//
//  MacawAgendaUITests.swift
//  MacawAgendaUITests
//
//  Created by tranquangson on 6/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest

class MacawAgendaUITests: XCTestCase {
    var app: XCUIApplication!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        self.app = XCUIApplication()
        self.app.launchEnvironment = ["isUITest": ""]
        self.app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sleep(1)
    }
}

extension MacawAgendaUITests {
    
    func goBack() {
        wait(interval: 1.0)
        let window = app.windows.element(boundBy: 0)
        window.coordinate(withNormalizedOffset: .zero).withOffset(CGVector(dx: 40, dy: 30)).tap()
        wait(interval: 1.5)
    }
    
    func waitForElementToAppear(_ element: XCUIElement, timeout: TimeInterval = 2,  file: String = #file, line: Int = #line) {
        let existsPredicate = NSPredicate(format: "exists == true")
        
        expectation(for: existsPredicate,
                    evaluatedWith: element,
                    handler: nil)
        
        waitForExpectations(timeout: timeout) { (error) -> Void in
            if (error != nil) {
                let message = "Failed to find \(element) after \(timeout) seconds."
                self.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
            }
        }
    }
    
    func wait(interval: TimeInterval) {
        RunLoop.current.run(until: Date().addingTimeInterval(interval))
    }
    
}

extension MacawAgendaUITests {
    func goToControlsView() {
        
        
        let tableView = app.tables.element(boundBy: 0)
        
        waitForElementToAppear(tableView)
        
        tableView.cells.allElementsBoundByIndex[5].tap()
    }
}

extension XCUIElement {
    func clearText() {
        let backspace = "\u{8}"
        let backspaces = Array((self.value as? String) ?? "").map { _ in backspace }
        self.typeText(backspaces.joined(separator: ""))
    }
    
    func typeSlow(text: String) {
        for i in text {
            self.typeText(String(i))
        }
    }
}

