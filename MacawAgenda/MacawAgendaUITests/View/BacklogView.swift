//
//  BacklogView.swift
//  MacawAgendaUITests
//
//  Created by tranquangson on 7/8/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest

extension MacawAgendaUITests {
    func testSearchTaskInBacklog() {
//        let elementsQuery = app.scrollViews.otherElements
//        let elementsQuery = app
        
        wait(interval: 6.0)
        //
        app.buttons[AccessiblityId.BTN_BACKLOG].tap()
        //
        let icMenuFilterButton = app.buttons[AccessiblityId.BTN_SEARCH]
        icMenuFilterButton.tap()
        //
        let searchField = app.searchFields["Search"]
        searchField.tap()
        
        searchField.typeText("xxx")
        
        waitForElementToAppear(app.tables.allElementsBoundByIndex[0].cells.element(boundBy: 2), timeout: 10.0)
        
        let cellCount = app.tables.cells.count
        XCTAssertTrue(cellCount > 0)
        
        
        app.buttons["Search"].tap()
        
        wait(interval: 1.0)
        
        let firstCell = app.tables.cells.element(boundBy: 2)
        XCTAssertTrue(firstCell.exists)
        firstCell.tap()
        
        wait(interval: 3.0)
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
    }
    
    func testFilterTaskInBacklog() {
//        let elementsQuery = app.scrollViews.otherElements
        //
        wait(interval: 6.0)
        //
        app.buttons[AccessiblityId.BTN_BACKLOG].tap()
        //
        let icMenuFilterButton = app.buttons[AccessiblityId.BTN_FILTER]
        icMenuFilterButton.tap()
        //
        let filterBySheet = app.sheets[AccessiblityId.SHEET_FILTER]
        filterBySheet.buttons[AccessiblityId.ACTION_SHEET_OTHER + "_1"].tap()
        icMenuFilterButton.tap()
        filterBySheet.buttons[AccessiblityId.ACTION_SHEET_OTHER + "_2"].tap()
        icMenuFilterButton.tap()
                filterBySheet.buttons[AccessiblityId.ACTION_SHEET_OTHER + "_3"].tap()
        icMenuFilterButton.tap()
        filterBySheet.buttons[AccessiblityId.ACTION_SHEET_OTHER + "_0"].tap()
        
        wait(interval: 5.0)
        
        let firstCell = app.tables.cells.element(boundBy: 5)
        XCTAssertTrue(firstCell.exists)
        firstCell.tap()
        
        wait(interval: 3.0)
  
        
    }
    
}
