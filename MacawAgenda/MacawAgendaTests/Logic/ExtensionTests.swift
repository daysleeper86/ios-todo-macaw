//
//  ExtensionTests.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/8/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest
@testable import MacawAgenda

class ExtensionTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

//Color extension
extension ExtensionTests {
    func testHexaColor() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //Arrange
        let oRed : CGFloat = 239.0
        let oGreen : CGFloat = 239.0
        let oBlue : CGFloat = 244.0
        let oAlpha : CGFloat = 1.0
        
        //Action
        let hexaCode = "#EFEFF4"
        let color = UIColor(hexString: hexaCode)
        
        var red: CGFloat = 0
        var green : CGFloat = 0
        var blue : CGFloat = 0
        var alpha : CGFloat = 0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        let iRed = red * 255
        let iGreen = green * 255
        let iBlue = blue * 255
        let iAlpha = alpha
        
        // Assert
        XCTAssertEqual(iRed,oRed,"Not equal")
        XCTAssertEqual(iGreen,oGreen,"Not equal")
        XCTAssertEqual(iBlue,oBlue,"Not equal")
        XCTAssertEqual(iAlpha,oAlpha,"Not equal")
    }
}

//UIImageView extension
extension ExtensionTests {
    func testLoadDefaultUser() {
        let userAvatar = UIImage(named: LocalImage.UserImage.Avatar)
        XCTAssertNotNil(userAvatar,"resource is nil. Are you remove it?")
    }
}

//NSDate extension
extension ExtensionTests {
    func testDateVar() {
        XCTAssertNotNil(Date.dateFormatter)
        XCTAssertNotNil(Date().yesterday)
        XCTAssertNotNil(Date().dayBefore)
        XCTAssertNotNil(Date().noon)
    }
    
    func testAllFormatFromDate() {
        //Arrange
        let resultDay = "01"
        let resultDay1 = "12"
        
        let resultMonth = NSLocalizedString("last_mess_january", comment: "")
        let resultMonth1 = NSLocalizedString("last_mess_febrary", comment: "")
        
        let resultYear = "1970"
        let resultYear1 = "1970"
        
        let resultDayMonthYear = resultMonth + " " + resultDay + ", " + resultYear
        let resultDayMonthYear1 = resultMonth1 + " " + resultDay1 + ", " + resultYear1
        
        let resultIntegerFormatted : Int = 19700101
        let resultIntegerFormatted1 : Int = 19700212
        
        //Action
        let date : Date = Date(timeIntervalSince1970: 0)

        let numDayInterval : Double = 42
        let numHourInterval : Double = numDayInterval * 24
        
        let twentyDayLater = TimeInterval(60 * 60 * 24 * numDayInterval)
        let date1 : Date = Date(timeIntervalSince1970: twentyDayLater)
        
        
        let day = date.dayString()
        let month = date.monthString()
        let dayMonthYear = date.dayMonthYearString()
        
        let day1 = date1.dayString()
        let month1 = date1.monthString()
        let dayMonthYear1 = date1.dayMonthYearString()
        
        let betweensDay : Double = Double(date.daysBetweenDateAndNow(nowDate: date1))
        let betweensHour : Double = Double(date.hoursBetweenDateAndNow(nowDate: date1))
        
        let integerFormatted = date.formatInt()
        let integerFormatted1 = date1.formatInt()
        
        // Assert
        XCTAssertEqual(resultDay, day,"not equal")
        XCTAssertEqual(resultDay1, day1,"not equal")
        XCTAssertEqual(resultMonth, month,"not equal")
        XCTAssertEqual(resultMonth1, month1,"not equal")
        XCTAssertEqual(resultDayMonthYear, dayMonthYear,"not equal")
        XCTAssertEqual(resultDayMonthYear1, dayMonthYear1,"not equal")
        XCTAssertEqual(resultIntegerFormatted, integerFormatted,"not equal")
        XCTAssertEqual(resultIntegerFormatted1, integerFormatted1,"not equal")
        XCTAssertEqual(numDayInterval, betweensDay,"not equal")
        XCTAssertEqual(numHourInterval, betweensHour,"not equal")
    }
    
    func testDayHourMinuteFromDate() {
        let day : Int = 19
        let hour : Int = 16
        let minute : Int = 41
        let strDateTime = "2019/07/\(day) \(hour):\(minute)"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let someDateTime = formatter.date(from: strDateTime)
        //
        XCTAssertEqual(someDateTime?.day() ?? 0, day,"not equal")
        XCTAssertEqual(someDateTime?.hour() ?? 0, hour,"not equal")
        XCTAssertEqual(someDateTime?.minute() ?? 0, minute,"not equal")
        
        XCTAssertEqual(someDateTime?.getTime(format: "a") ?? "", "PM","not equal")
        XCTAssertEqual(someDateTime?.getTime(format: "HH:mm") ?? "", "\(hour):\(minute)","not equal")
        XCTAssertEqual(someDateTime?.timeStampHourMinute() ?? "", "\(hour):\(minute)" + " " + "PM","not equal")
        
        XCTAssertEqual(someDateTime?.dayWeek(isFullText: true) ?? "", NSLocalizedString("last_mess_friday", comment: ""),"not equal")
        
        
        let showMinute : Bool = true
        
        var result_timestamp : String = ""
        let timeStamp: String = someDateTime?.timeStampForChattingHeader(showMinute: showMinute) ?? ""
        if(someDateTime?.isToday() ?? false == true){
            result_timestamp = NSLocalizedString("history_view_today_time",comment:"")
        }
        else if(someDateTime?.isYesterday() ?? false == true){
            result_timestamp = NSLocalizedString("history_view_yesterday_time",comment:"")
        }
        else if(someDateTime?.isWeek() ?? false == true){
            result_timestamp = someDateTime?.dayWeek(isFullText:true) ?? ""
        }
        else{
            result_timestamp = someDateTime?.dayMonthYearString() ?? ""
        }
        if(showMinute == true){
            let strMinute = someDateTime?.timeStampHourMinute() ?? ""
            result_timestamp = result_timestamp + " " + strMinute
        }
        XCTAssertEqual(timeStamp, result_timestamp,"not equal")
    }
    
    func testSameTodayYesterdayDate() {
        //
        XCTAssertEqual(Date().isSameDay(Date()), true,"not equal")
        
        //
        XCTAssertEqual(Date().isToday(), true,"not equal")
        
        //
        XCTAssertEqual(Date().yesterday.isYesterday(), true,"not equal")
        XCTAssertNotEqual(Date().isYesterday(), true,"equal")
        
        //
        XCTAssertEqual(Date().yesterday.isWeek(), true,"not equal")
        
        //
        let numDayInterval : Double = 6
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents([.era, .year , .month , .day], from: Date())
        let today = calendar.date(from: components)
        let sixdate = today?.addingTimeInterval(-86400.0 * numDayInterval)
        if(sixdate != nil){
            XCTAssertEqual(sixdate!.isWeek(), true,"not equal")
        }
    }
}

//UIImageView extension
extension ExtensionTests {
    func testMD5Value() {
        //Arrange
        let originString = "tranquangson"
        let md5Hash = "5b5c579d60eecc5c859debf6992e050b"
        //Action
        let encryptedHash = originString.md5Value()
        // Assert
        XCTAssertEqual(md5Hash, encryptedHash,"not equal")
    }
    
    func testImageExtension() {
        //Arrange
        let originalString_1 = "123abc"
        let originalString_2 = "jpg"
        let originalString_3 = "jpeg"
        let originalString_4 = "ico"
        let originalString_5 = "png"
        let originalString_6 = "pdf"
        let originalString_7 = "doc"
        // Act
        let isValidation1 = originalString_1.isImageExtension()
        let isValidation2 = originalString_2.isImageExtension()
        let isValidation3 = originalString_3.isImageExtension()
        let isValidation4 = originalString_4.isImageExtension()
        let isValidation5 = originalString_5.isImageExtension()
        let isValidation6 = originalString_6.isImageExtension()
        let isValidation7 = originalString_7.isImageExtension()
        // Assert
        XCTAssertFalse(isValidation1)
        XCTAssertTrue(isValidation2)
        XCTAssertTrue(isValidation3)
        XCTAssertTrue(isValidation4)
        XCTAssertTrue(isValidation5)
        XCTAssertFalse(isValidation6)
        XCTAssertFalse(isValidation7)
    }
    
    func testIconForFile() {
        let pdfIcon = LocalImage.FileIcon.pdf
        let docIcon = LocalImage.FileIcon.doc
        let excelIcon = LocalImage.FileIcon.excel
        let pptIcon = LocalImage.FileIcon.ppt
        let textIcon = LocalImage.FileIcon.txt
        
        //Arrange
        let originalString_1 = "123abc.xlsx"
        let originalString_2 = "sontran.doc"
        let originalString_3 = "longcao.txt"
        let originalString_4 = "khuyen.xls"
        let originalString_5 = "1.ppt"
        let originalString_6 = "tailieu.pdf"
        let originalString_7 = "2.docx"
        // Act
        let icon1 = originalString_1.getIconForFile()
        let icon2 = originalString_2.getIconForFile()
        let icon3 = originalString_3.getIconForFile()
        let icon4 = originalString_4.getIconForFile()
        let icon5 = originalString_5.getIconForFile()
        let icon6 = originalString_6.getIconForFile()
        let icon7 = originalString_7.getIconForFile()
        // Assert
        XCTAssertEqual(icon1, excelIcon,"not equal")
        XCTAssertEqual(icon2, docIcon,"not equal")
        XCTAssertEqual(icon3, textIcon,"not equal")
        XCTAssertEqual(icon4, excelIcon,"not equal")
        XCTAssertEqual(icon5, pptIcon,"not equal")
        XCTAssertEqual(icon6, pdfIcon,"not equal")
        XCTAssertEqual(icon7, docIcon,"not equal")
    }
    
    func testSizeForText() {
        let latoRegular = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 16)
        let maxWidth1 : CGFloat = 320
        let widthSmall1 : CGFloat = 280
        let widthLarge1 : CGFloat = 285
        let widthSmall2 : CGFloat = 104
        let widthLarge2 : CGFloat = 108
        let originalString_1 = "@vitxinhdep archived the channel #binh-duong-du-hi. Tran dai quang son day ne"
        let originalString_2 = "Tran quang son"
        // Act
        let sizeName1 : CGSize = originalString_1.getTextSizeWithString(maxWidth1,latoRegular)
        let sizeName2 : CGSize = originalString_2.getTextSizeWithString(maxWidth1,latoRegular)
        // Assert
        XCTAssertLessThanOrEqual(widthSmall1, sizeName1.width,"small than")
        XCTAssertLessThanOrEqual(sizeName1.width, widthLarge1,"large than")
        XCTAssertLessThanOrEqual(widthSmall2, sizeName2.width,"small than")
        XCTAssertLessThanOrEqual(sizeName2.width, widthLarge2,"large than")
        
    }
    
}

