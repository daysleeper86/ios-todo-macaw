//
//  ResourceTests.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/8/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest
@testable import MacawAgenda

class ResourceTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

//Image resource
extension ResourceTests {
    func testLoadDefaultImages() {
        let userAvatar = UIImage(named: LocalImage.UserImage.Avatar)
        let imagePlaceHolder = UIImage(named: LocalImage.ImagePlaceHolder)
        
        XCTAssertNotNil(userAvatar,"resource is nil. Are you remove it?")
        XCTAssertNotNil(imagePlaceHolder,"resource is nil. Are you remove it?")
    }
}

//Font resource
extension ResourceTests {
    func testLoadDefaultFonts() {
        let latoRegular = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 16)
        let latoBold = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        let latoItalic = UIFont(name: FontNames.Lato.MULI_ITALIC, size: 16)
        let latoBoldItalic = UIFont(name: FontNames.Lato.MULI_BOLD_ITALIC, size: 16)
        let latoBlack = UIFont(name: FontNames.Lato.MULI_BLACK, size: 16)
        let latoLight = UIFont(name: FontNames.Lato.MULI_LIGHT, size: 16)
        
        XCTAssertNotNil(latoRegular,"resource is nil. Are you remove it?")
        XCTAssertNotNil(latoBold,"resource is nil. Are you remove it?")
        XCTAssertNotNil(latoItalic,"resource is nil. Are you remove it?")
        XCTAssertNotNil(latoBoldItalic,"resource is nil. Are you remove it?")
        XCTAssertNotNil(latoBlack,"resource is nil. Are you remove it?")
        XCTAssertNotNil(latoLight,"resource is nil. Are you remove it?")
    }
}

//Font resource
extension ResourceTests {
    func testLocalization() {
        //Keys
        let key_filter_by = "filter_by"
        let key_all_talks = "all_talks"
        let key_my_talks = "my_talks"
        let key_priority_talks = "priority_talks"
        let key_completed_talks = "completed_talks"
        
        let value_filter_by = NSLocalizedString(key_filter_by, comment: "")
        let value_all_talks = NSLocalizedString(key_all_talks, comment: "")
        let value_my_talks = NSLocalizedString(key_my_talks, comment: "")
        let value_priority_talks = NSLocalizedString(key_priority_talks, comment: "")
        let value_completed_talks = NSLocalizedString(key_completed_talks, comment: "")
        
        XCTAssertNotEqual(key_filter_by, value_filter_by,"Value with key is not found")
        XCTAssertNotEqual(key_all_talks, value_all_talks,"Value with key is not found")
        XCTAssertNotEqual(key_my_talks, value_my_talks,"Value with key is not found")
        XCTAssertNotEqual(key_priority_talks, value_priority_talks,"Value with key is not found")
        XCTAssertNotEqual(key_completed_talks, value_completed_talks,"Value with key is not found")
        
    }
}

