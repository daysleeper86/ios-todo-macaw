//
//  UtilTests.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/8/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest
@testable import MacawAgenda

class UtilTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidEmail() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Arrange
        let originalString_1 = "paintbsd@gmail.let"
        let originalString_2 = "paintbsd.let"
        // Act
        let isValidation1 = Util.isValidEmail(originalString_1)
        let isValidation2 = Util.isValidEmail(originalString_2)
        // Assert
        XCTAssertTrue(isValidation1)
        XCTAssertFalse(isValidation2)
    }
    
    func testValidTaskNameSearch() {
        let originalString_1 = "#123"
        let originalString_2 = "#abc"
        let originalString_3 = "son tran"
        let originalString_4 = "#123123123123123123123123123123123123123123123123123123123123123123123123123123123123"
        let originalString_5 = "#123a"
        let originalString_6 = "#1234567890"
        // Act
        let isValidation1 = Util.isValidTaskNameSearch(originalString_1)
        let isValidation2 = Util.isValidTaskNameSearch(originalString_2)
        let isValidation3 = Util.isValidTaskNameSearch(originalString_3)
        let isValidation4 = Util.isValidTaskNameSearch(originalString_4)
        let isValidation5 = Util.isValidTaskNameSearch(originalString_5)
        let isValidation6 = Util.isValidTaskNameSearch(originalString_6)
        // Assert
        XCTAssertTrue(isValidation1)
        XCTAssertFalse(isValidation2)
        XCTAssertFalse(isValidation3)
        XCTAssertFalse(isValidation4)
        XCTAssertFalse(isValidation5)
        XCTAssertTrue(isValidation6)
    }
    
    func testValidNameAZ() {
        let originalString_1 = "123abc"
        let originalString_2 = "ab@c"
        let originalString_3 = "sontran"
        let originalString_4 = "123"
        // Act
        let isValidation1 = Util.isValidNameAZ(originalString_1)
        let isValidation2 = Util.isValidNameAZ(originalString_2)
        let isValidation3 = Util.isValidNameAZ(originalString_3)
        let isValidation4 = Util.isValidNameAZ(originalString_4)
        // Assert
        XCTAssertFalse(isValidation1)
        XCTAssertFalse(isValidation2)
        XCTAssertTrue(isValidation3)
        XCTAssertFalse(isValidation4)
    }
    
    func testValidName() {
        let originalString_1 = "123abc"
        let originalString_2 = "ab@c"
        let originalString_3 = "sontran"
        let originalString_4 = "123"
        let originalString_5 = "sontransontransontransontransontransontransontransontransontran"
        // Act
        let isValidation1 = Util.validateName(originalString_1)
        let isValidation2 = Util.validateName(originalString_2)
        let isValidation3 = Util.validateName(originalString_3)
        let isValidation4 = Util.validateName(originalString_4)
        let isValidation5 = Util.validateName(originalString_5)
        // Assert
        XCTAssertFalse(isValidation1.isValid)
        XCTAssertFalse(isValidation2.isValid)
        XCTAssertTrue(isValidation3.isValid)
        XCTAssertFalse(isValidation4.isValid)
        XCTAssertFalse(isValidation5.isValid)
    }
    
    func testValidateProjectName(){
        let originalString_1 = "123abc#$%^#jfyf"
        let originalString_2 = ""
        let originalString_3 = "sontransontransontransontran"
        let originalString_4 = "1232342@2342@gamil.com"
        let originalString_5 = "sontransontransontransontransontransontransontransontransontransontransontransontransontransontransontransontransontransontran"
        // Act
        let isValidation1 = Util.validateProjectName(originalString_1)
        let isValidation2 = Util.validateProjectName(originalString_2)
        let isValidation3 = Util.validateProjectName(originalString_3)
        let isValidation4 = Util.validateProjectName(originalString_4)
        let isValidation5 = Util.validateProjectName(originalString_5)
        // Assert
        XCTAssertFalse(isValidation1.isValid)
        XCTAssertFalse(isValidation2.isValid)
        XCTAssertTrue(isValidation3.isValid)
        XCTAssertFalse(isValidation4.isValid)
        XCTAssertFalse(isValidation5.isValid)
    }
    
    func testValidateMessageContent(){
        let originalString_1 = "123abc#$%^#jfyf"
        let originalString_2 = ""
        let originalString_3 = "sontransontransontransontran"
        let originalString_4 = "1232342@2342@gamil.com"
        let originalString_5 = "sontransontransontransontransontransontransontransontransontransontransontransontransontransontransontsontransontransontransontransontransontransontransontransontransontransontransontransontransontransontsontransontransontransontransontransontransontransontransontransontransontransontransontransontransontsontransontransontransontransontransontransontransontransontransontransontransontransontransontransontsontransontransontransontransontransontransontransontransontransontransontransontransontransontransont"
        // Act
        let isValidation1 = Util.validateMessageContent(originalString_1)
        let isValidation2 = Util.validateMessageContent(originalString_2)
        let isValidation3 = Util.validateMessageContent(originalString_3)
        let isValidation4 = Util.validateMessageContent(originalString_4)
        let isValidation5 = Util.validateMessageContent(originalString_5)
        // Assert
        XCTAssertFalse(isValidation1.isValid)
        XCTAssertFalse(isValidation2.isValid)
        XCTAssertTrue(isValidation3.isValid)
        XCTAssertFalse(isValidation4.isValid)
        XCTAssertFalse(isValidation5.isValid)
    }
    
    func testValidPassword() {
        let originalString_1 = "123abc"
        let originalString_2 = "12345678"
        let originalString_3 = "sontran1@#%@#$2345678@#%@#$@#%@#$"
        let originalString_4 = "123a@#%"
        let originalString_5 = "sontran12345678a@#%@#$@@#$tran1231dfsdfsdfsdfxva32423423423423423423"
        // Act
        let isValidation1 = Util.validatePassword(originalString_1)
        let isValidation2 = Util.validatePassword(originalString_2)
        let isValidation3 = Util.validatePassword(originalString_3)
        let isValidation4 = Util.validatePassword(originalString_4)
        let isValidation5 = Util.validatePassword(originalString_5)
        // Assert
        XCTAssertFalse(isValidation1.isValid)
        XCTAssertTrue(isValidation2.isValid)
        XCTAssertTrue(isValidation3.isValid)
        XCTAssertFalse(isValidation4.isValid)
        XCTAssertFalse(isValidation5.isValid)
    }
    
    func testValidateNewAndConfirmPassword(){
        let originalNewPass_1 = "123abc"
        let originalConfirmNewPass_1 = "123abc3"
        let originalNewPass_2 = "123abc321@ham"
        let originalConfirmNewPass_2 = "123abc321@ham"
        // Act
        let isValidation1 = Util.validateNewAndConfirmPassword(originalNewPass_1, originalConfirmNewPass_1)
        let isValidation2 = Util.validateNewAndConfirmPassword(originalNewPass_2, originalConfirmNewPass_2)
        // Assert
        XCTAssertFalse(isValidation1.isValid)
        XCTAssertTrue(isValidation2.isValid)
    }
    
    func testValidateContent(){
        let originalString_1 = "123abc"
        let originalString_2 = ""
        let originalString_3 = "sontran1@#%@#$2345678@#%@#$@#%@#$"
        // Act
        let isValidation1 = Util.validateContent(originalString_1)
        let isValidation2 = Util.validateContent(originalString_2)
        let isValidation3 = Util.validateContent(originalString_3)
        // Assert
        XCTAssertTrue(isValidation1.isValid)
        XCTAssertFalse(isValidation2.isValid)
        XCTAssertTrue(isValidation3.isValid)
    }
    
    func testDocumentPath() {
        let failPath = ""
        let truePath = "/Documents"
        let documentPath = Util.getDocumentDirectoryPath()
        //
        XCTAssertNotEqual(documentPath, failPath, "Failed to get document directory path")
        XCTAssertTrue(documentPath.contains(truePath))
    }
    
    func testLocalPathFromFolder() {
        let folderName = AppURL.APIDomains.Folder_User_Chatting
        //
        let failPath = ""
        let fullFailPath = failPath + folderName
        //
        let truePath = "/Documents"
        let fullTruePath = truePath + "/" + folderName
        //
        let localPath = Util.getLocalPathFromFolder(folderName)
        //
        XCTAssertNotEqual(localPath, fullFailPath, "Failed to get document directory path")
        XCTAssertTrue(localPath.contains(fullTruePath))
    }
    
    func testURLStringFromPath() {
        //
        let httpString = "http"
        let apiDevString = AppURL.APIDomains.ServerAPI
        //
        let originalString_1 = "zing.vn"
        
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let originalString_2 = "https://beta3.macaw-dev.ga/avatar/" + userId + ".jpg"
        let url1 = Util.getURLStringFromFile(originalString_1)
        let url2 = Util.getURLStringFromFile(originalString_2)
        //
        XCTAssertTrue(url1.contains(httpString))
        XCTAssertTrue(url1.contains(apiDevString))
        //
        XCTAssertTrue(url2.contains(httpString))
    }
    
    func testLoadImageFromLocal(){
        let name = "image"
        let image = Util.loadImageFromLocal(name)
        //
        XCTAssertNil(image)
    }
    
    func testFormatByteToOther() {
        let original_value1 : Double = 1234
        let original_value2 : Double = 70000
        let original_value3 : Double = 70
        let original_value4 : Double = 8
        let original_value5 : Double = 421134
        let original_value6 : Double = 42113434
        let original_value7 : Double = 1232354213
        //
        let value1 = Util.formatByteToOther(original_value1)
        let value2 = Util.formatByteToOther(original_value2)
        let value3 = Util.formatByteToOther(original_value3)
        let value4 = Util.formatByteToOther(original_value4)
        let value5 = Util.formatByteToOther(original_value5)
        let value6 = Util.formatByteToOther(original_value6)
        let value7 = Util.formatByteToOther(original_value7)
        
        XCTAssertEqual(value1, "1.21 KB", "Fail convert")
        XCTAssertEqual(value2, "68.36 KB", "Fail convert")
        XCTAssertEqual(value3, "70.00 bytes", "Fail convert")
        XCTAssertEqual(value4, "8.00 bytes", "Fail convert")
        XCTAssertEqual(value5, "411.26 KB", "Fail convert")
        XCTAssertEqual(value6, "40.16 MB", "Fail convert")
        XCTAssertEqual(value7, "1.15 GB", "Fail convert")
    }
    
    func testRandomKey() {
        let maxLength = Constant.MaxLength.RandomKey
        let randomKey = Util.getRandomKey()
        //
        XCTAssertEqual(maxLength,randomKey.count,"Email length")
    }
    
    func testParseStringContainArray() {
        let original_value1 : String = "1,2,3,4,6"
        let original_value2 : String = "1"
        let original_value3 : String = ""
        let value1 : Int = Util.parseStringContainArray(original_value1).count
        let value2 : Int = Util.parseStringContainArray(original_value2).count
        let value3 : Int = Util.parseStringContainArray(original_value3).count
        
        XCTAssertTrue(value1 == 5)
        XCTAssertTrue(value2 == 1)
        XCTAssertTrue(value3 == 1)
    }
    
    func testCheckStringContainArray() {
        let original_value1 : String = "1,2,3,4,6"
        let original_value2 : String = "1"
        let original_value3 : String = ""
        let value1 : Bool = Util.checkStringContainArray(original_value1)
        let value2 : Bool = Util.checkStringContainArray(original_value2)
        let value3 : Bool = Util.checkStringContainArray(original_value3)
        
        XCTAssertTrue(value1 == true)
        XCTAssertTrue(value2 == false)
        XCTAssertTrue(value3 == false)
    }
    
    func testUserToString() {
        let userid1 = "111"
        let userid2 = "222"
        let userid3 = "333"
        
        let user1 = User(userId: userid1, dataType: .normal)
        let user2 = User(userId: userid2, dataType: .normal)
        let user3 = User(userId: userid3, dataType: .normal)
        let users123 = [user1,user2,user3]
        let value_users123 = Util.userToString(users: users123)
        
        //
        let result_users123 = "\(userid1),\(userid2),\(userid3)"
        let fail_users123 = "\(userid1)\(userid2)\(userid3)"
        
        XCTAssertEqual(value_users123, result_users123, "Fail convert")
        XCTAssertNotEqual(value_users123, fail_users123, "Fail convert")
    }
    
    
    func testPerformanceExample() {
         // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            
        }
    }

}
