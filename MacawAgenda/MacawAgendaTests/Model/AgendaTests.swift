//
//  AgendaTests.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/9/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest
import FirebaseFirestore
import Firebase

@testable import MacawAgenda

class AgendaTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testQueryAgendaByDuedate() {
        if ((Auth.auth().currentUser?.uid) != nil){
            //Arrange
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            let projectId = "5RAA5FPJBazfPL69P"
            let duedate = Date()
            
            let longRunningExpectation = expectation(description: "QueryAgendaInFireStore")
            var queyError: Error?
            var arrayAgenda : [Agenda] = []
            
            //Action
            let agendaController = AgendaController.sharedAPI
            let query = agendaController.Query_AgendaByDuedate(userId, projectId, duedate)
            query.getDocuments() { (querySnapshot, err) in
                if let err = err {
                    queyError = err
                } else {
                    for document in querySnapshot!.documents {
                        let agenda = Agenda(jsonDict: document.data())
                        if(agenda.valid() == true){
                            arrayAgenda.append(agenda)
                        }
                    }
                    
                }
                longRunningExpectation.fulfill()
            }
            //Assert
            waitForExpectations(timeout: 5) { expectationError in
                XCTAssertNil(expectationError, expectationError!.localizedDescription)
                XCTAssertNil(queyError)
            }
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
