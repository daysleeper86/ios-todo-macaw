//
//  TaskTests.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/9/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest
import FirebaseFirestore
import Firebase

@testable import MacawAgenda

class TaskTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testMockTaskData() {
        //Arrange
        var arrayTask : [Task] = []
        //Action
        let mockTasks = MockTaskQuerySnapshot.jsonResponse
        for mockITem in mockTasks{
            let task = Task(jsonDict: mockITem)
            if(task.valid()){
                arrayTask.append(task)
            }
        }
        //Assert
        XCTAssertEqual(mockTasks.count, arrayTask.count, "Not equal")
    }
    
    func testQueryAllTaskData() {
        if ((Auth.auth().currentUser?.uid) != nil){
            //Arrange
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            let search = "task"
            let duedate : Date? = nil
            
            let longRunningExpectation = expectation(description: "QueryTaskInFireStore")
            var queyError: Error?
            var arrayTask : [Task] = []
            
            //Action
            let taskController = TaskController.sharedAPI
            let query = taskController.Query_AllTask(userId, projectId, search, duedate)
            query.getDocuments() { (querySnapshot, err) in
                if let err = err {
                    queyError = err
                } else {
                    for document in querySnapshot!.documents {
                        let task = Task(jsonDict: document.data())
                        if(task.valid() == true){
                            arrayTask.append(task)
                        }
                    }
                }
                longRunningExpectation.fulfill()
            }
            //Assert
            waitForExpectations(timeout: 10) { expectationError in
                XCTAssertNil(expectationError, expectationError!.localizedDescription)
                XCTAssertNil(queyError)
                XCTAssertTrue(arrayTask.count > 0)
            }
        }
    }
    
    func testQueryTaskFilterData() {
        if ((Auth.auth().currentUser?.uid) != nil){
            //Arrange
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            
            let longRunningExpectation = expectation(description: "QueryTaskInFireStore")
            var queyError: Error?
            var arrayTask : [Task] = []
            
            //Action
            let taskController = TaskController.sharedAPI
            let query = taskController.Query_TaskFilter(userId, projectId, .myTask)
            query.getDocuments() { (querySnapshot, err) in
                if let err = err {
                    queyError = err
                } else {
                    for document in querySnapshot!.documents {
                        let task = Task(jsonDict: document.data())
                        if(task.valid() == true){
                            arrayTask.append(task)
                        }
                    }
                }
                longRunningExpectation.fulfill()
            }
            //Assert
            waitForExpectations(timeout: 5) { expectationError in
                XCTAssertNil(expectationError, expectationError!.localizedDescription)
                XCTAssertNil(queyError)
                XCTAssertTrue(arrayTask.count > 0)
            }
        }
    }
    
    func testValidateTaskData() {
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
