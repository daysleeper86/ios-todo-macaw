//
//  MockQuerySnapshot.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/9/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

struct MockTaskQuerySnapshot{
    static let jsonResponse : [[String : Any]] = [
        [
            "_id" : "hr7DzLhKMeKxnQJFW",
            "name" : "Backlog / Plan: bổ sung Staggering Lists Transitions khi chọn labels / search",
            "tasknumber" : "424",
            "assignee" : ["hr7DzLhKMeKxnQJFW"],
            "duedate" : 1562728139,
            "users" : [
                [
                    "_id" : "cFQWyDevnpm35Xdt6",
                    "name" : "phuong88",
                    "username" : "nobitis88@gmail.com"
                ],
                [
                    "_id" : "hr7DzLhKMeKxnQJFW",
                    "name" : "khuyen@macaw-app.com",
                    "username" : "khuyen@macaw-app.com"
                ]
            ],
            "completed" : 0
            
        ],
        [
            "_id" : "4fu7GMReH6jZKBYhW",
            "name" : "Research - Hooked: Investment in Macaw",
            "tasknumber" : "93",
            "assignee" : ["Xgdo82DsfC56BFi3L"],
            "duedate" : 1562234544,
            "users" : [
                [
                    "_id" : "Xgdo82DsfC56BFi3L",
                    "name" : "Luu Dao",
                    "username" : "loudp.macaw@gmail.com"
                ],
                [
                    "_id" : "QQpf6FoFyK6vXKjmT",
                    "name" : "long.cao@macaw-app.com",
                    "username" : "long.cao@macaw-app.com"
                ]
            ],
            "priority" : 1,
        ],
        [
            "_id" : "6FtjtB8AqowhdCeAK",
            "name" : "Notifications: Check rule gửi notification đến member",
            "tasknumber" : "402",
            "assignee" : ["QQpf6FoFyK6vXKjmT"],
            "duedate" : 0,
            "users" : [
                [
                    "_id" : "Xgdo82DsfC56BFi3L",
                    "name" : "Luu Dao",
                    "username" : "loudp.macaw@gmail.com"
                ],
                [
                    "_id" : "hr7DzLhKMeKxnQJFW",
                    "name" : "khuyen@macaw-app.com",
                    "username" : "khuyen@macaw-app.com"
                ]
            ],
            "priority" : 0,
        ],
        [
            "_id" : "zf7ABQ77CnuKPcGSP",
            "name" : "BD - Task Detail - Task - Discussion List",
            "tasknumber" : "377",
            "assignee" : ["zf7ABQ77CnuKPcGSP"],
            "duedate" : 0,
            "users" : [
                [
                    "_id" : "zf7ABQ77CnuKPcGSP",
                    "name" : "paint.bsd@gmail.com",
                    "username" : "paint.bsd@gmail.com"
                ],
                [
                    "_id" : "Xgdo82DsfC56BFi3L",
                    "name" : "Luu Dao",
                    "username" : "loudp.macaw@gmail.com"
                ]
            ],
            "priority" : 1,
        ],
        [
            "_id" : "mn8mxwoBLjhpHqoci",
            "name" : "task 205",
            "tasknumber" : "206",
            "assignee" : ["zf7ABQ77CnuKPcGSP"],
            "duedate" : 0,
            "users" : [
                [
                    "_id" : "cFQWyDevnpm35Xdt6",
                    "name" : "phuong88",
                    "username" : "nobitis88@gmail.com"
                ],
                [
                    "_id" : "hr7DzLhKMeKxnQJFW",
                    "name" : "khuyen@macaw-app.com",
                    "username" : "khuyen@macaw-app.com"
                ]
            ],
            "priority" : 1,
        ],
        [
            "_id" : "7bMPcn8jMkL3reH5q",
            "name" : "Flow signup from landing page and upgrade to pro",
            "tasknumber" : "453",
            "assignee" : ["hr7DzLhKMeKxnQJFW"],
            "duedate" : 1562730425,
            "users" : [
                [
                    "_id" : "mn8mxwoBLjhpHqoci",
                    "name" : "phuong@macaw-app.com",
                    "username" : "phuong@macaw-app.com"
                ],
                [
                    "_id" : "QQpf6FoFyK6vXKjmT",
                    "name" : "cao long",
                    "username" : "long.cao@macaw-app.com"
                ]
            ],
            "priority" : 0,
        ]
    ]
}


