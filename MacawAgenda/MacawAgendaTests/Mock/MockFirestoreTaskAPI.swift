//
//  MockFirestoreTaskAPI.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift

class MockFirestoreTaskAPI : FirestoreTaskAPI{
    
    let mockTasks : [[String : Any]]
    init(mockTasks: [[String : Any]]) {
        self.mockTasks = mockTasks
    }
    
    func getAllTasks(projectId: String, userId: String, search: String,sort: Int, duedate : Date?) -> Observable<[Task]>{
        var arrayTask = TaskService.mockDataToTask(mockData: mockTasks)
        //
        if(sort == Constant.TaskFilter.TaskID.allTask.rawValue){
            return TaskService.filterSearch(search: search, arrayTask: arrayTask)
        }
        else{
            if(arrayTask.count == 0){
                arrayTask.append(TaskViewModel.emptyTask)
            }
            //
            return Observable.just(arrayTask)
        }
    }
    
    
    func getTaskByIds(projectId: String, userId: String, ids: [String],findType : Constant.FindType,byMe: Bool,needEmptyData: Bool) -> Observable<[Task]>{
        let arrayTask = TaskService.mockDataToTask(mockData: mockTasks)
        return TaskService.filterWithIdByAgenda(ids: ids, findType: findType, byMe: byMe, userMe: userId, arrayTask: arrayTask, needEmptyData: needEmptyData)
    }
    
    func getTaskById(taskId: String) -> Observable<[Task]> {
        return Observable.just([])
    }
    
    //Subcrible method - Not override
    func subscribleAllTasks(projectId: String, userId: String, search: String, sort: Int, duedate : Date?) -> Observable<[Task]>{
        return Observable.just([])
    }
    
    func subscribleTaskByIds(projectId: String, userId: String, ids: [String], findType : Constant.FindType, byMe: Bool,needEmptyData: Bool,needSortByTimeStamp: Bool) -> Observable<[Task]>{
        return Observable.just([])
    }
    
    func subscribleTaskById(taskId: String) -> Observable<[Task]> {
        return Observable.just([])
    }
       
       
}
