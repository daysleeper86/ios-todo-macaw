//
//  TaskViewModelTests.swift
//  MacawAgendaTests
//
//  Created by tranquangson on 7/9/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import XCTest
import RxCocoa
import RxSwift
import RxTest
import RxBlocking

@testable import MacawAgenda
let resolution: TimeInterval = 1 // seconds

class TaskViewModelTests: XCTestCase {
    var viewModel: TaskViewModel!
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBacklog() {
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        //
        scheduler = TestScheduler(initialClock: 0, resolution: resolution)
        
        let tasks = scheduler.createObserver(Int.self)
        let taskController : FirestoreTaskAPI = MockFirestoreTaskAPI(mockTasks: MockTaskQuerySnapshot.jsonResponse)
    
        let search = scheduler.createHotObservable([.next (10, "son"),
                                        .next(20, "123"),
                                        .next(30, "task")])
        let subjectFilter = scheduler.createHotObservable([.next (10, Constant.TaskFilter.TaskID.myTask.rawValue),
                                                     .next(25, Constant.TaskFilter.TaskID.priorityTask.rawValue),
                                                     .next(35, Constant.TaskFilter.TaskID.allTask.rawValue)])

        
        viewModel = TaskViewModel(
            inputSearchAndSort: (
                search: search.asObservable(),
                sort: subjectFilter.asObservable(),
                taskController: taskController,
                dataSource : BehaviorSubject<[Task]>(value: []).asObservable(),
                userId: userId,
                projectId: projectId,
                duedate: nil
            )
        )
//
        viewModel!.tasks?
            .flatMapLatest{tasks -> Observable<Int> in
                return Observable.just(tasks.count)
            }
            .bind(to: tasks)
            .disposed(by: disposeBag)
        
        
        scheduler.start()
        
//        viewModel!.tasks?.toBlocking(timeout: 10).first()
        
        let resultEvent : [Recorded<Event<Int>>]  = [
            .next(10, 5),
            .next(10, 6),
            .next(20, 5),
            .next(20, 6),
            .next(25, 5),
            .next(25, 6),
            .next(30, 5),
            .next(30, 6),
            .next(35, 5),
            .next(35, 2)
        ]
        
        XCTAssertEqual(tasks.events,resultEvent)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
