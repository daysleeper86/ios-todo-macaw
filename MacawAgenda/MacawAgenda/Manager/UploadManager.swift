//
//  DownloadManager.swift
//  MacawAgenda
//
//  Created by tranquangson on 8/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import Queuer
import FirebaseStorage
import Moya
import RxSwift

class UploadManager {
    static let sharedSingleton = UploadManager()
    //
    let uploadQueue = Queuer(name: "uploadQueue")
    var updatedtasks: Observable<[Task]>?
    let disposeBag = DisposeBag()
    //
    private init() {
        
    }
    
    func addFile(file: File, projectId: String){
        let reachability = Reachability()!
        if(reachability.isReachable == false){
            DispatchQueue.main.async {
                let dict = NSDictionary(objects: [file], forKeys: [ModelDataKeyString.COLLECTION_FILE as NSCopying])
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_FAIL), object: dict)
            }
        }
        else{
            let uploadOperation = SynchronousOperation(name: file.fileId, executionBlock: {[weak self] _ in
                // Upload data
                if let imgUpload = Util.loadImageFromLocal(file.local_link){
                    //
                    let imgData = imgUpload.jpegData(compressionQuality: CGFloat(1.0))
                    //
                    let fileSize : Double = Double(imgData!.count)
                    if(file.size != fileSize){
                        
                    }
                    let strStoredLocation = "\(AppURL.APIDomains.FireStorageFileFolder)/\(projectId)/\(file.taskId)/\(file.name)"
                    // Create the file metadata
                    let metadata = StorageMetadata()
                    metadata.contentType = AppURL.APIDomains.FireStorageImageType
                    //
                    let storageRef = Storage.storage().reference()
                    let riversRef = storageRef.child(strStoredLocation)
                    
                    let uploadTask = riversRef.putData(imgData!, metadata: metadata, completion: { (storage, error) in
                        if (error != nil) {
                            LoggerApp.logError("Error upload image file Firestore: " + error!.localizedDescription)
                            
                            //
                            DispatchQueue.main.async {
                                let dict = NSDictionary(objects: [file], forKeys: [ModelDataKeyString.COLLECTION_FILE as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_FAIL), object: dict)
                            }
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                file.progressUpload = 1.0
                                let dict = NSDictionary(objects: [file], forKeys: [ModelDataKeyString.COLLECTION_FILE as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_SUCCESSFULLY), object: dict)
                                //
                                let dictFile = Util.paramsForSendImageFileUpload(file: file,projectId: projectId, updatedURL: strStoredLocation)
                                self?.sendImageFileUploadToAPIServer(projectId: projectId,taskId:file.taskId, fileId: file.fileId, dictFile: dictFile, completion: { isSave in
                                })
                            }
                        }
                    })
                    uploadTask.observe(.progress, handler: { snapshot in
                        let progress = Double(snapshot.progress!.completedUnitCount)
                            / Double(snapshot.progress!.totalUnitCount)
                        file.progressUpload = progress
                        let dict = NSDictionary(objects: [file], forKeys: [ModelDataKeyString.COLLECTION_FILE as NSCopying])
                        // A progress event occurred
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOADING_PROGRESS), object: dict)
                    })
                    uploadTask.observe(.failure) { snapshot in
                        if let error = snapshot.error as NSError? {
                            var strMessage = ""
                            switch (StorageErrorCode(rawValue: error.code)!) {
                            case .objectNotFound:
                                // File doesn't exist
                                strMessage = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                                break
                            case .unauthorized:
                                // User doesn't have permission to access file
                                strMessage = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                                break
                                /* ... */
                            case .unknown:
                                // Unknown error occurred, inspect the server response
                                let reachability = Reachability()!
                                if(reachability.isReachable == false){
                                    
                                    strMessage = NSLocalizedString("invalid_offline_network", comment: "")
                                }
                                else{
                                    strMessage = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                                }
                                break
                            default:
                                // A separate error occurred. This is a good place to retry the upload.
                                strMessage = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                                break
                            }
                        }
                    }
                    //
                }
            })
            addOperation(synchronousOperation: uploadOperation)
        }
    }
    
    func addAvatar(imageAvatar: UIImage, userId: String){
        let uploadOperation = SynchronousOperation { _ in
            let imgData = imageAvatar.jpegData(compressionQuality: CGFloat(1.0))
            let strStoredLocation = "\(AppURL.APIDomains.FireStorageAvatarFolder)/\(userId)"
            // Create the file metadata
            let metadata = StorageMetadata()
            metadata.contentType = AppURL.APIDomains.FireStorageImageType
            
            let storageRef = Storage.storage().reference()
            let riversRef = storageRef.child(strStoredLocation)
            let uploadTask = riversRef.putData(imgData!, metadata: metadata, completion: { (storage, error) in
                if (error != nil) {
                    LoggerApp.logError("Error upload image avatar Firestore: " + error!.localizedDescription)
                    //
                    DispatchQueue.main.async {
                        let dict = NSDictionary(objects: [userId], forKeys: [SettingString.UserId as NSCopying])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_FAIL_AVATAR), object: dict)
                    }
                    
                }
                else{
                    DispatchQueue.main.async {
                        let dict = NSDictionary(objects: [userId], forKeys: [SettingString.UserId as NSCopying])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_SUCCESSFULLY_AVATAR), object: dict)
                    }
                }
            })
            uploadTask.observe(.progress, handler: { snapshot in
                let progress = Double(snapshot.progress!.completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                let dict = NSDictionary(objects: [userId,progress], forKeys: [SettingString.UserId as NSCopying, SettingString.Progress as NSCopying])
                // A progress event occurred
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_PROCESSING_AVATAR), object: dict)
            })
            uploadTask.observe(.failure) { snapshot in
                if let error = snapshot.error as NSError? {
                    switch (StorageErrorCode(rawValue: error.code)!) {
                    case .objectNotFound:
                        // File doesn't exist
                        break
                    case .unauthorized:
                        // User doesn't have permission to access file
                        break
                    case .cancelled:
                        // User canceled the upload
                        break
                        
                        /* ... */
                        
                    case .unknown:
                        // Unknown error occurred, inspect the server response
                        break
                    default:
                        // A separate error occurred. This is a good place to retry the upload.
                        break
                    }
                }
            }
        }
        addOperation(synchronousOperation: uploadOperation)
    }
    
    func addOperation(synchronousOperation : SynchronousOperation){
        uploadQueue.addOperation(synchronousOperation)
    }
    
    func cancelWithId(uploadOperationId: String){
        for operation in uploadQueue.operations{
            if(operation.name == uploadOperationId){
                operation.cancel()
            }
        }
    }
    
    func cancelAll(){
        uploadQueue.cancelAll()
    }
    
    func pause(){
        uploadQueue.pause()
    }
    
    func resume(){
        uploadQueue.resume()
    }
    
    func sendImageFileUploadToAPIServer(projectId: String,taskId: String, fileId: String,dictFile : [String:Any], completion: @escaping (_ isSave: Bool) -> ()){
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let fileAPIController = FileAPIController(provider: accountProvider)
        //
        let fileViewModel = FileAPIViewModel(
            inputSendFileUpload: (
                projectId: projectId,
                taskId: BehaviorSubject<String>(value: taskId),
                fileId: fileId,
                dictFile: dictFile,
                loginTaps: BehaviorSubject<Void>(value: ()),
                fileAPIController: fileAPIController
            )
        )
        //
        fileViewModel.processingIn?
            .subscribe(onNext: { loading  in
                let dict = NSDictionary(objects: [fileId,loading], forKeys: [SettingString.FileId as NSCopying,SettingString.Progress as NSCopying])
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOADING_API_PROGRESS), object: dict)
            })
            .disposed(by: disposeBag)
        //
        fileViewModel.processFinised?.subscribe(onNext: { loginResult in
            switch loginResult {
            case .failed( _):
                completion(false)
                let dict = NSDictionary(objects: [fileId], forKeys: [SettingString.FileId as NSCopying])
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_API_FAIL), object: dict)
            
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            completion(true)
                            DispatchQueue.main.async {
                                let message = NSLocalizedString("upload_file_successfully", comment: "")
                                let dict = NSDictionary(objects: [fileId,message], forKeys: [SettingString.FileId as NSCopying,SettingString.Message as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_API_SUCCESSFULLY), object: dict)
                            }
                        }
                        else{
                            completion(false)
                            DispatchQueue.main.async {
                                let message = NSLocalizedString("upload_file_unsuccessfully", comment: "")
                                let dict = NSDictionary(objects: [fileId,message], forKeys: [SettingString.FileId as NSCopying,SettingString.Message as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_API_FAIL), object: dict)
                            }
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
}


