//
//  DownloadManager.swift
//  MacawAgenda
//
//  Created by tranquangson on 8/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import Queuer
import SDWebImage

class DownloadManager {
    static let sharedSingleton = DownloadManager()
    //
    let downloadQueue = Queuer(name: "downloadQueue")
    
    private init() {
        
    }
    
    func addDownloadFile(file: File){
        let downloadOperation = ConcurrentOperation { _ in
            /// Your task here
            let url = URL(string: file.url)
            let manager = SDWebImageManager.shared
            _ = manager.loadImage(with: url, options: SDWebImageOptions.init(rawValue: 0), progress: {
                (receivedSize, expectedSize, targetURL) in
                if (expectedSize > 0) {
                    let progress = Double(receivedSize) / Double(expectedSize)
                    file.progressDownload = progress
                    let dict = NSDictionary(objects: [file], forKeys: [ModelDataKeyString.COLLECTION_FILE as NSCopying])
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DOWNLOADING_PROGRESS), object: dict)
                }
            }, completed: {
                 (image, data, error, cacheType, finished, imageURL) in
                if (error != nil) {
                    LoggerApp.logError("SDWebImage failed to download image: " + error!.localizedDescription)
                }
            })
        }
        addOperation(concurrentOperation: downloadOperation)
    }
    
    func addOperation(concurrentOperation : ConcurrentOperation){
        downloadQueue.addOperation(concurrentOperation)
    }
    
    func cancelAll(){
        downloadQueue.cancelAll()
    }
    
    func pause(){
        downloadQueue.pause()
    }
    
    func resume(){
        downloadQueue.resume()
    }
}


