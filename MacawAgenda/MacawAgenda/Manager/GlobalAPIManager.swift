//
//  GlobalAPIManager.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya

class GlobalAPIManager {
    static let sharedSingleton = GlobalAPIManager()
    //
    let disposeBag = DisposeBag()
    //
    private init() {
    }
    
    func updateUserActiveState(state: Bool){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        if(userId.count > 0){
            //
            if let notificationStatus = AppSettings.sharedSingleton.account?.notificationStatus{
                if(notificationStatus ==  true){
                    let token = AppSettings.sharedSingleton.account?.token ?? ""
                    let authPlugin = AccessTokenPlugin { token }
                    let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
                    let accountController = AccountAPIController(provider: accountProvider)
                    
                    let accountViewModel = AccountAPIViewModel(
                        inputUpdateProfileActiveState: (
                            userId: BehaviorSubject<String>(value: userId),
                            state: state,
                            controller: accountController
                        )
                    )
                    //
                    accountViewModel.signedIn?.subscribe(onNext: { loginResult in
                        switch loginResult {
                        case .failed(let output):
                            if(AppDelegate().sharedInstance().alertSessionExpiredIfNeed(code: output.code) == false){
                                
                            }
                            
                        case .ok(let output):
                            if let reponseAPI = output as? ResponseAPI{
                                if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                                    if(reponseAPI.code == 200){
                                        
                                    }
                                    else{
                                        
                                    }
                                    //
                                }
                            }
                        }
                    }).disposed(by: disposeBag)
                }
            }
            
        }
    }
    
    func updateSettings(emailStatus: Bool, notificationStatus: Bool, deviceToken: String){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        if(userId.count > 0){
            //
            let token = AppSettings.sharedSingleton.account?.token ?? ""
            let authPlugin = AccessTokenPlugin { token }
            let projectAccountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
            let accountController = AccountAPIController(provider: projectAccountProvider)
            let accountViewModel = AccountAPIViewModel(
                inputUpdateSettingUser: (
                    userId: userId,
                    emailStatus: BehaviorSubject(value: emailStatus).asObservable(),
                    notificationStatus: BehaviorSubject(value: notificationStatus).asObservable(),
                    notificationToken:BehaviorSubject(value: deviceToken).asObservable(),
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    controller: accountController
                )
            )
            //
            accountViewModel.signedIn?.subscribe(onNext: {loginResult in
                switch loginResult {
                case .failed(let output):
                    if(AppDelegate().sharedInstance().alertSessionExpiredIfNeed(code: output.code) == false){
                    }
                    
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                            if(reponseAPI.code == 200){
                            }
                            else{
                            }
                            //
                        }
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
    func updateDefaultProjectChanged(_ projectId: String, completion: @escaping (_ isSave: Bool) -> ()){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        if(userId.count > 0 && projectId.count > 0){
            let subjectValidProjectUpdated : PublishSubject<String> = PublishSubject()
            //
            let projectController = ProjectController.sharedAPI
            let projectViewModel = ProjectViewModel(
                inputNeedProjects: (
                    projectId: BehaviorSubject(value: projectId).asObservable(),
                    projectController: projectController,
                    needSubscrible:false
                )
            )
            projectViewModel.projects?
                .subscribe(onNext:  { projects in
                    if(projects.count > 0){
                        let firstProject = projects[0]
                        if(firstProject.dataType == .normal){
                            if(firstProject.valid() == true){
                                var isExits : Bool = false
                                for user in firstProject.users{
                                    if(user.userId == userId){
                                        subjectValidProjectUpdated.onNext(projectId)
                                        isExits = true
                                        break
                                    }
                                }
                                if(isExits == false){
                                    completion(false)
                                }
                            }
                            
                        }
                        else if(firstProject.dataType == .empty){
                            completion(false)
                        }
                    }
                })
                .disposed(by: disposeBag)
            
            //
            let token = AppSettings.sharedSingleton.account?.token ?? ""
            let authPlugin = AccessTokenPlugin { token }
            let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
            let accountController = AccountAPIController(provider: accountProvider)
            
            let accountViewModel = AccountAPIViewModel(
                inputUpdateProfileDefaultProject: (
                    userId: userId,
                    projectId: subjectValidProjectUpdated.asObservable(),
                    controller: accountController
                )
            )
            //
            accountViewModel.signedIn?.subscribe(onNext: {loginResult in
                switch loginResult {
                case .failed(let output):
                    if(AppDelegate().sharedInstance().alertSessionExpiredIfNeed(code: output.code) == false){
                    }
                    completion(false)
                    
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                            if(reponseAPI.code == 200){
                                completion(true)
                            }
                            else{
                                completion(false)
                            }
                            
                        }
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
    func readNotification(notificationData: [String:Any]){
        if let projectId = notificationData[SettingString.ProjectId] as? String, let notificationId = notificationData[SettingString.NotificationId] as? String{
        if(projectId.count > 0){
            //
            let token = AppSettings.sharedSingleton.account?.token ?? ""
            let authPlugin = AccessTokenPlugin { token }
            let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
            let notificationAPIController = NotificationAPIController(provider: projectProvider)
            let notificationViewModel = NotificationAPIViewModel(
                inputReadNotification: (
                    projectId: projectId,
                    notificationId: notificationId,
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    notificationAPIController: notificationAPIController
                )
            )
            //
            notificationViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
                switch loginResult {
                case .failed(let output):
                    let appDelegate = AppDelegate().sharedInstance()
                    if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                        
                    }
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                            if(reponseAPI.code == 200){
                            }
                            else{
                                
                            }
                        }
                    }
                }
            }).disposed(by: disposeBag)
        }
        }
    }
}

