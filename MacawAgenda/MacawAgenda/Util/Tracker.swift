//
//  Tracker.swift
//  MacawAgenda
//
//  Created by tranquangson on 9/9/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import Analytics

class Tracker{
    //
    static func analyticTrack(event: String, properties: [String : Any]){
        SEGAnalytics.shared().track(event, properties: properties)
    }
    
    static func analyticAlias(newId: String){
        SEGAnalytics.shared().alias(newId)
    }
    
    static func analyticIdentify(userId: String, traits: [String : Any]){
        SEGAnalytics.shared().identify(userId, traits: traits)
    }
    
    //Track need actions
    static func trackUserSignUp(account: Account, fromEmail: Bool){
        var type_signup = ""
        if(fromEmail == true){
            type_signup = "email"
        }
        else{
            type_signup = "google"
        }
        //Track identify
        let properties = ["type": "organic",
                          "uid" : account.userId,
                          "name" : account.name,
                          "email" : account.email,
                          "username" : account.userName,
                          "type_signup" : type_signup]
        Tracker.analyticIdentify(userId: account.userId, traits: properties)
        //Track event
        Tracker.analyticTrack(event: "User Signed Up - iOS", properties: properties)
        
    }
    
    static func trackUserLogin(account: Account, fromEmail: Bool){
        var type_signup = ""
        if(fromEmail == true){
            type_signup = "email"
        }
        else{
            type_signup = "google"
        }
        //Track identify
        let properties = ["uid" : account.userId,
                          "username" : account.userName,
                          "type_signup" : type_signup]
        Tracker.analyticIdentify(userId: account.userId, traits: properties)
        //Track event
        Tracker.analyticTrack(event: "User Signed In - iOS", properties: properties)
    }
    
    static func trackTeamCreated(project: Project){
        let properties : [String : Any] = ["type": "organic",
                                           "team_id" : project.projectId,
                                           "team_name" : project.name,
                                           "team_type" : project.teamType,
                                           "team_size" : project.teamSize,
                                           "team_owner_id" : project.owned.userId]
        //Track event
        Tracker.analyticTrack(event: "Team Created - iOS", properties: properties)
        
    }
    
    static func trackOpenBacklog(userId: String){
        if(userId.count > 0){
            let properties : [String : Any] = ["userid": userId]
            //Track event
            Tracker.analyticTrack(event: "Backlog: Open - iOS", properties: properties)
        }
    }
    
    static func trackOpenPlan(projectId: String, userId: String){
        if(userId.count > 0){
            let properties : [String : Any] = ["pid": projectId, "userid": userId]
            //Track event
            Tracker.analyticTrack(event: "Plan: Open - iOS", properties: properties)
        }
    }
    
    static func trackOpenCollaborate(projectId: String, userId: String){
        if(projectId.count > 0 && userId.count > 0){
            let properties : [String : Any] = ["pid": projectId, "userid": userId]
            //Track event
            Tracker.analyticTrack(event: "Collaborate: Open - iOS", properties: properties)
        }
    }
    
    static func trackTaskCreatedInPlan(projectId: String, userId: String, taskName: String){
        if(projectId.count > 0 && userId.count > 0){
            let properties : [String : Any] =
                ["pid": projectId,
                 "userid": userId,
                 "task_name": taskName]
            //Track event
            Tracker.analyticTrack(event: "Task: Created In Plan - iOS", properties: properties)
        }
    }
    
    static func trackTaskCreatedInBacklog(projectId: String, userId: String, taskName: String){
        let properties : [String : Any] =
            ["pid": projectId,
             "userid": userId,
             "task_name": taskName]
        //Track event
        Tracker.analyticTrack(event: "Task: Created In Backlog - iOS", properties: properties)
    }
    
    static func trackFinishTaskInFocus(taskid: String, userId: String, wip: Bool){
        let properties : [String : Any] =
            ["taskid": taskid,
             "userid": userId,
             "wip": wip]
        //Track event
        Tracker.analyticTrack(event: "Finish Task: In Focus - iOS", properties: properties)
    }
    
    static func trackFinishTaskInCollaborate(taskid: String, userId: String, wip: Bool){
        let properties : [String : Any] =
            ["taskid": taskid,
             "userid": userId,
             "wip": wip]
        //Track event
        Tracker.analyticTrack(event: "Finish Task: In Collaborate - iOS", properties: properties)
    }
    
    static func trackInviteSend(userid: String,name: String,projectId: String, email: String){
        let properties : [String : Any] =
            ["invitee_uid": userid,
             "invitee_email": email,
             "invitee_name": name,
             "invitee_role": "member",
             "project": projectId]
        //Track event
        Tracker.analyticTrack(event: "Invite Sent - iOS", properties: properties)
    }
    
    static func trackRemoveTask(userid: String,projectId: String){
        let properties : [String : Any] =
            ["pid": projectId,
             "userid": userid]
        //Track event
        Tracker.analyticTrack(event: "Plan: Remove Task - iOS", properties: properties)
    }
}
