//
//  Util.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/12/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import UIKit
import Moya
import RxSwift
import GoogleSignIn
import FirebaseAuth


class Util{
    
    static func isValidEmail(_ email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    static func isValidTaskNameSearch(_ search:String) -> Bool {
        let taskNameRegEx = "#+[0-9]{1,64}"
        let taskNameTest = NSPredicate(format:"SELF MATCHES %@", taskNameRegEx)
        return taskNameTest.evaluate(with: search)
    }
    
    static func validateName(_ name: String) -> ValidationResult {
        if name.isEmpty {
            return .empty
        }
        let maxLenght = Constant.MaxLength.Name
        if(name.count < maxLenght){
            return .ok(message: "Valid name")
        }
        return .failed(message: NSLocalizedString("name", comment: "") + " " + NSLocalizedString("contains_at_least_max_characters_prefix", comment: "") + " " + "\(maxLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
    }
    
    static func validateUserName(_ name: String) -> ValidationResult {
        if name.isEmpty {
            return .empty
        }
        let maxLenght = Constant.MaxLength.Name
        let minLenght = Constant.MinLength.Name
        if(name.count >= minLenght && name.count < maxLenght){
            return .ok(message: "Valid name")
        }
        else{
            if(name.count < minLenght){
                return .failed(message: NSLocalizedString("name", comment: "") + " " + NSLocalizedString("contains_at_least_min_characters_prefix", comment: "") + " " + "\(minLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
            }
        }
        return .failed(message: NSLocalizedString("name", comment: "") + " " + NSLocalizedString("contains_at_least_max_characters_prefix", comment: "") + " " + "\(maxLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
    }
    
    static func validateProjectName(_ name: String) -> ValidationResult {
        if name.isEmpty {
            return .empty
        }
        
        let maxLenght = Constant.MaxLength.Name
        let minLenght = Constant.MinLength.Name
        if(name.count >= minLenght && name.count < maxLenght){
            return .ok(message: "Valid name")
        }
        else{
            if(name.count < minLenght){
                return .failed(message: NSLocalizedString("project_name", comment: "") + " " + NSLocalizedString("contains_at_least_min_characters_prefix", comment: "") + " " + "\(minLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
            }
        }
        return .failed(message: NSLocalizedString("project_name", comment: "") + " " + NSLocalizedString("contains_at_least_min_characters_prefix", comment: "") + " " + "\(maxLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
    }
    
    static func validateTaskName(_ name: String) -> ValidationResult {
        if name.isEmpty {
            return .empty
        }
        
        let maxLenght = Constant.MaxLength.Task
        let minLenght = Constant.MinLength.Task
        if(name.count >= minLenght && name.count <= maxLenght){
            return .ok(message: "Valid task name")
        }
        return .failed(message: NSLocalizedString("task_name", comment: "") + " " + NSLocalizedString("contains_at_least_min_characters_prefix", comment: "") + " " + "\(maxLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
    }
    
    static func validateMessageContent(_ content: String) -> ValidationResult {
        if content.isEmpty {
            return .empty
        }
        
        let maxLenght = Constant.MaxLength.Message
        let minLenght = Constant.MinLength.Message
        if(content.count >= minLenght && content.count < maxLenght){
            return .ok(message: "Valid name")
        }
        return .failed(message: NSLocalizedString("message_content", comment: "") + " " + NSLocalizedString("contains_at_least_min_characters_prefix", comment: "") + " " + "\(maxLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
    }
    
    static func validatePassword(_ password: String) -> ValidationResult {
        let minLenght = Constant.MinLength.Password
        if(password.count < minLenght){
            return .failed(message: NSLocalizedString("password_contains_at_least_min_characters", comment: ""))
        }
        let maxLenght = Constant.MaxLength.Password
        if(password.count > maxLenght){
            return .failed(message: NSLocalizedString("password_contains_at_least_max_characters_prefix", comment: "") + " " + "\(maxLenght)" + " " + NSLocalizedString("contains_at_least_max_characters_subfix", comment: ""))
        }
        return .ok(message: "")
    }
    
    static func validateNewAndConfirmPassword(_ newPassword: String, _ confirmNewPass: String) -> ValidationResult {
        if(newPassword == confirmNewPass){
            return .ok(message: "Same password")
        }
        return .failed(message: NSLocalizedString("password_confirmation_does_not_match", comment: ""))
    }
    
    static func validateContent(_ content: String) -> ValidationResult {
        if(content.count > 0){
            return .ok(message: "Valid content")
        }
        return .failed(message: "Empty content")
    }
    
    static func isValidNameAZ(_ name: String) -> Bool {
        let allowedCharacterSet = NSCharacterSet (charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
        let typedCharacterSet = CharacterSet(charactersIn: name)
        let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
        return alphabet
    }
    
    
    static func getDocumentDirectoryPath() -> String{
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last ?? ""
    }
    
    static func  getLocalPathFromFolder(_ strFolder: String) -> String{
        let pathOfDocuments : String = Util.getDocumentDirectoryPath()
        let fullPath = pathOfDocuments + "/" + strFolder
        return fullPath
    }
    
    static func loadImageFromLocal(_ name: String) -> UIImage?{
        var image : UIImage?
        let localPath : String = Util.getLocalPathFromFolder(AppURL.APIDomains.Folder_User_Chatting)
        let imgPath : String = localPath + name
        //
        let fileManager : FileManager = FileManager.default
        if(fileManager.fileExists(atPath: imgPath)){
            image = UIImage(contentsOfFile: imgPath)!
        }
        return image
    }
    
    static func saveImage(image: UIImage,name: String,folder: String, completion: @escaping (_ isSave: Bool) -> ()){
        DispatchQueue.global().async {
            autoreleasepool{
                let localPath = Util.getLocalPathFromFolder(folder)
                if(!FileManager.default.fileExists(atPath: localPath)){
                    do{
                        try FileManager.default.createDirectory(atPath: localPath, withIntermediateDirectories: false, attributes: nil)
                    }
                    catch{
                        completion(false)
                    }
                }
                let fullPath = localPath + name
                let imgData = image.jpegData(compressionQuality: CGFloat(1.0))
                let saved = FileManager.default.createFile(atPath: fullPath, contents: imgData, attributes: nil)
                _ = addSkipBackupAttributeToItemAtURL(url: URL(fileURLWithPath: localPath))
                completion(saved)
            }
        }
    }
    
    static func addSkipBackupAttributeToItemAtURL(url: URL?) -> Bool
    {
        var success : Bool = false
        guard var strongURL = url else { return false}
        // iOS version >= 5.1
        assert(FileManager.default.fileExists(atPath: strongURL.path))
        
        do{
            var resourceValues = URLResourceValues()
            resourceValues.isExcludedFromBackup = true
            try strongURL.setResourceValues(resourceValues)
            
            success = true
        }
        catch let exception{
            print("Error excluding " + strongURL.lastPathComponent + " from backup " + exception.localizedDescription)
        }
        return success
    }
    
    static func imageIsNotNull(_ image: UIImage?) -> Bool{
        if(image == nil){
            return false
        }
        
        let cgref = image!.cgImage
        let cim = image!.ciImage
        return !(cim == nil && cgref == nil)
    }
    
    static func formatFileToName(fileId: String, taskId: String, userId: String) -> String{
        let strSpecial = "___"
        let strFormat = taskId + strSpecial + userId + strSpecial + fileId
        return strFormat
    }
    
    static func formatURLUserAvatar(_ userId: String) -> String{
        let strURL = AppURL.APIDomains.ImageURL_Avatar + userId + ".jpg"
        return strURL
    }

    static func formatDefaultType(strName : String) -> String{
        let strType = "jpg"
        let strFormat = strName + "." + strType
        return strFormat;
    }
    
    static func getURLStringFromFile(_ filePath: String) -> String{
        var strImageLink : String
        if(filePath.range(of: "http") == nil){
            strImageLink = AppURL.APIDomains.ServerAPI + filePath
        }
        else{
            strImageLink = filePath
        }
        strImageLink = strImageLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        return strImageLink
    }
    
    static func scaleAndRotateImage(image: UIImage) -> UIImage? {
        let kMaxResolution: CGFloat = 640
        let imgRef = image.cgImage
        guard let strongImgRef = imgRef else { return nil }
        let width = CGFloat(strongImgRef.width)
        let height = CGFloat(strongImgRef.height)
        
        var transform = CGAffineTransform.identity
        var bounds = CGRect(x: 0, y: 0, width: width, height: height)
        if (width > kMaxResolution || height > kMaxResolution) {
            let ratio = CGFloat(width/height)
            if (ratio > 1.0) {
                bounds.size.width = CGFloat(kMaxResolution)
                bounds.size.height = CGFloat(roundf(Float(bounds.size.width / ratio)))
            }
            else{
                bounds.size.height = CGFloat(kMaxResolution)
                bounds.size.width = CGFloat(roundf(Float(bounds.size.height * ratio)))
            }
            
        }
        let scaleRatio : CGFloat = bounds.size.width / width
        let imageSize = CGSize(width: width, height: height)
        var boundHeight : CGFloat = 0
        let orient = image.imageOrientation
        switch orient {
        case .up: //EXIF = 1
            transform = .identity
        case .upMirrored://EXIF = 2
            transform = transform.translatedBy(x: imageSize.width, y: 0.0)
            transform = transform.scaledBy(x: -1.0, y: 1.0)
        case .down: //EXIF = 3
            transform = transform.translatedBy(x: imageSize.width, y: imageSize.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        case .downMirrored: //EXIF = 4
            transform = transform.translatedBy(x: 0.0, y: imageSize.height)
            transform = transform.scaledBy(x: 1.0, y: -1.0)
        case .leftMirrored: //EXIF = 5
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = transform.translatedBy(x: imageSize.height, y: imageSize.width)
            transform = transform.scaledBy(x: -1.0, y: 1.0)
            transform = transform.rotated(by: 3.0 * CGFloat(Double.pi) / 2.0)
        case .left: //EXIF = 6
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = transform.translatedBy(x: 0.0, y: imageSize.width)
            transform = transform.rotated(by: 3.0 * CGFloat(Double.pi) / 2.0)
        case .rightMirrored: //EXIF = 7
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = transform.scaledBy(x: -1.0, y: 1.0)
            transform = transform.rotated(by: CGFloat(Double.pi) / 2.0)
        case .right: //EXIF = 8
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = transform.translatedBy(x: imageSize.height, y: 0.0)
            transform = transform.rotated(by: CGFloat(Double.pi) / 2.0)
        default:
            let exception = NSException(name:NSExceptionName(rawValue: "Scale And Rotate Image"), reason:"Invalid image orientation.", userInfo:nil)
            exception.raise()
        }
        UIGraphicsBeginImageContext(bounds.size)
        let context = UIGraphicsGetCurrentContext()
        if(orient == .right || orient == .left){
            context?.scaleBy(x: -scaleRatio, y: scaleRatio)
            context?.translateBy(x: -height, y: 0.0)
        }
        else{
            context?.scaleBy(x: scaleRatio, y: -scaleRatio)
            context?.translateBy(x: 0.0, y: -height)
        }
        context?.concatenate(transform)
        UIGraphicsGetCurrentContext()?.draw(strongImgRef, in: CGRect(x: 0.0, y: 0.0, width: width, height: height))
        let imageCopy = UIGraphicsGetImageFromCurrentImageContext()
        return imageCopy
    }
    
    static func saveImageToPhotoAlbum(_ image: UIImage){
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }


    static func parseURLWithToken(_ strURL: String) -> URLRequest?{
        var resultRequest : URLRequest?
        //
        let url = URL(string: strURL)
        if(url != nil){
            resultRequest = URLRequest(url: url!)
            resultRequest?.setValue("", forHTTPHeaderField: "token")
        }
        return resultRequest
    }
    
    static func parseURLImageFireStore(_ path: String) -> String{
        let strURL = AppURL.APIDomains.FireStorageImageURL + path + AppURL.APIDomains.FireStorageImageMediaType
        return strURL
    }
    
    static func parseURLAvatarImageFireStore(_ userId: String) -> String{
        let path = "avatars/" + userId
        let strURL = AppURL.APIDomains.FireStorageImageURL + path.URLEscapedString + AppURL.APIDomains.FireStorageImageMediaType
        return strURL
    }
    
    static func formatByteToOther(_ imageSize: Double) -> String{
        var multiplyFactor : Int = 0
        let tokens = ["bytes","KB","MB","GB","TB","PB", "EB", "ZB", "YB"]
        var smallImageSize = imageSize
        while (smallImageSize > 1024) {
            smallImageSize /= 1024
            multiplyFactor += 1
        }
        return String(format: "%4.2f %@", Float(smallImageSize),tokens[multiplyFactor])
    }
    
    static func contentTypeForImageData(_ imgData: Data) -> String{
        let ext : String
        switch (imgData[0]) {
        case 0xFF:
            
            ext = "jpg"
            
        case 0x89:
            
            ext = "png"
        case 0x47:
            
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "" //unknown
        }
        return ext
    }

    
    static func getRandomKey() -> String{
        let len : Int = Constant.MaxLength.RandomKey
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<len).map{ _ in letters.randomElement()! })
    }
    
    static func fromJSONFile(_ fileName: String) -> Data {
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else {
            fatalError("Invalid path for json file")
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            fatalError("Invalid data from json file")
        }
        return data
    }
    
    static func parseStringContainArray(_ object: String) -> [String] {
        let objectItems = object.components(separatedBy: ",")
        return objectItems
    }
    
    static func checkStringContainArray(_ object: String) -> Bool {
        let objectItems = Util.parseStringContainArray(object)
        return objectItems.count > 1
    }
    
    static func userToString(users : [User]) -> String{
        var ids : String = ""
        if(users.count > 0){
            for user in users {
                let userId = user.userId
                if(ids == ""){
                    ids = userId
                }
                else{
                    ids = ids + "," + userId
                }
            }
        }
        return ids
    }
    
    static func openSettingApp(){
        let url = URL(string: UIApplication.openSettingsURLString)
        if(url != nil){
            UIApplication.shared.openURL(url!)
        }
    }
    
//    static func migrateDatabase(){
//        var configuration = Realm.Configuration.defaultConfiguration
//        let fileURL = FileManager.default
//            .containerURL(forSecurityApplicationGroupIdentifier: "group.com.macaw.todo")!
//            .appendingPathComponent("Library/Caches/default.realm")
//        configuration.fileURL = fileURL
//        // Set the new schema version. This must be greater than the previously used
//        // version (if you've never set a schema version before, the version is 0).
//        configuration.schemaVersion = 1
//
//        // Set the block which will be called automatically when opening a Realm with a
//        // schema version lower than the one set above
//        configuration.migrationBlock = {(_ migration: Migration, _ oldSchemaVersion: UInt64) in
//            // We haven’t migrated anything yet, so oldSchemaVersion == 0
//            if (oldSchemaVersion < 1) {
//                // Nothing to do!
//                // Realm will automatically detect new properties and removed properties
//                // And will update the schema on disk automatically
//            }
//        }
//        configuration.shouldCompactOnLaunch = {(totalBytes: Int, usedBytes: Int) -> Bool in
//            // totalBytes refers to the size of the file on disk in bytes (data + free space)
//            // usedBytes refers to the number of bytes used by data in the file
//
//            // Compact if the file is over 100MB in size and less than 50% 'used'
//            let oneHundredMB = 100 * 1024 * 1024
//            let shouldCompactOnLaunch : Bool = (totalBytes > oneHundredMB) && (Float(usedBytes) / Float(totalBytes)) < Float(0.5)
//            return shouldCompactOnLaunch
//        }
//
//        do{
//            try Realm.init(configuration: configuration)
//        }
//        catch{
//            // handle error compacting or opening Realm
//        }
//        // Tell Realm to use this new configuration object for the default Realm
//        Realm.Configuration.defaultConfiguration = configuration
//        // Now that we've told Realm how to handle the schema change, opening the file
//        // will automatically perform the migration
//        //[RLMRealm defaultRealm];
//
//    }
    
    //Utils
    static func findIndexOfHour(range9to16: [Int], finishdate : Date) -> Int{
        let hour = finishdate.hour()
        let minute = finishdate.minute()
        //Check first
        let firstItem = range9to16[0]
        if(hour < firstItem){
            return -1
        }
        
        let laststItem = range9to16[range9to16.count-1]
        if(hour > laststItem){
            return range9to16.count
        }
        
        var index : Int = -1
        for i in 0..<range9to16.count{
            let rangeItem = range9to16[i]
            if(rangeItem == hour){
                if(minute > 0){
                    index = i + 1
                }
                else{
                    index = i
                }
                break
            }
        }
        
        return index
    }
    
    static func countTotalTask(value9to16: [Int], to : Int) -> Int{
        var total : Int = 0
        for i in 0...to{
            let rangeItem = value9to16[i]
            total = total + rangeItem
        }
        return total
    }
    
    static func getColorCode(value: Int) -> String{
        var strCode = ""
        switch value {
        case 1:
            strCode = AppColor.RandomColors.COLOR_1
        case 2:
            strCode = AppColor.RandomColors.COLOR_2
        case 3:
            strCode = AppColor.RandomColors.COLOR_3
        case 4:
            strCode = AppColor.RandomColors.COLOR_4
        case 5:
            strCode = AppColor.RandomColors.COLOR_5
        case 6:
            strCode = AppColor.RandomColors.COLOR_6
        case 7:
            strCode = AppColor.RandomColors.COLOR_7
        case 8:
            strCode = AppColor.RandomColors.COLOR_8
        case 9:
            strCode = AppColor.RandomColors.COLOR_9
        default:
            strCode = AppColor.RandomColors.COLOR_10
        }
        return strCode
    }
    
    static func getColor(value: Int) -> UIColor{
        let newValue = value % 10
        //
        let strCode = Util.getColorCode(value: newValue)
        //
        let color = UIColor(hexString: strCode)
        //
        return color
    }
    
    static func getColorFromString(string : String) -> UIColor{
        if(string.count > 0){
            let arrChar = string.utf8CString
            let char1 = arrChar[0]
            let x : Int = Int(char1)
            return Util.getColor(value: x)
        }
        return Util.getColor(value: 0)
    }

    static func shortname2Char(name: String) -> String{
        var result = ""
        let array = name.components(separatedBy: " ")
        let n = array.count
        if(n > 1){
            var string1 = array[0]
            var string2 = array[n-1]
            
            if(string1.count > 0){
                string1 = string1.subString(from: 0, to: 0)
            }
            if(string2.count > 0){
                string2 = string2.subString(from: 0, to: 0)
            }
            //
            if(string1.count > 0){
                result = string1
                if(string2.count > 0){
                    result = result + string2
                }
            }
            else if(string2.count > 0){
                result = string2
            }
        }
        else{
            result = name.subString(from: 0, to: 1)
        }
        return result
    }
    
    static func shortname1Char(name: String) -> String{
        let result = name.subString(from: 0, to: 0)
        return result
    }
    
    static func parseErrorResult(errorResult: Error?) -> LoginResult{
        var strMessage = NSLocalizedString("something_went_wrong", comment: "")
        var statusCode : Int = AppURL.APIStatusCode.INTERNAL_SERVER_ERROR.rawValue
        if let error = errorResult{
            if let moyaError = error as? MoyaError{
                if let response = moyaError.response{
                    statusCode = response.statusCode
                }
                else{
                    if let errorItem = error as NSError?{
                        statusCode = errorItem.code
                    }
                }
            }
            else{
                if let errorItem = error as NSError?{
                    statusCode = errorItem.code
                }
            }
            //
            if statusCode == AppURL.APIStatusCode.MOYA_REQUEST_TIMEOUT.rawValue{
                strMessage = NSLocalizedString("invalid_connect_to_server", comment: "")
            }
            else if statusCode == AppURL.APIStatusCode.NOT_FOUND.rawValue {
                strMessage = NSLocalizedString("invalid_offline_network", comment: "")
            }
            else if statusCode == AppURL.APIStatusCode.INTERNAL_SERVER_ERROR.rawValue {
                strMessage = NSLocalizedString("something_went_wrong", comment: "")
            }
            else if statusCode == AppURL.APIStatusCode.UNAUTHORIZED.rawValue {
                strMessage = NSLocalizedString("the_session_user_has_expired", comment: "")
            }
            else if statusCode == AppURL.APIStatusCode.TOO_MANY_REQUESTS.rawValue {
                strMessage = ""
            }
            else{
                strMessage = NSLocalizedString("invalid_offline_network", comment: "")
            }
        }
        return LoginResult.failed(output: (statusCode,strMessage))
    }
    
    static func parseAuthenFireStoreResult(authDataResult: AuthDataResult?, errorResult: Error?) -> LoginResult{
        if let authResult = authDataResult{
            let user = authResult.user
            let userId = user.uid
            let email = user.email
            let displayName = user.displayName
            let providerID = user.providerID
            let photoURL = user.photoURL?.absoluteString
            let phoneNumber = user.phoneNumber
            //
            var userData : [String : Any] = ["provider": providerID,
                                             "uid": userId]
            if(email?.count ?? 0 > 0){
                userData["email"] = email!
            }
            if(displayName?.count ?? 0 > 0){
                userData["name"] = displayName!
            }
            if(photoURL?.count ?? 0 > 0){
                userData["avatar"] = photoURL!
            }
            if(phoneNumber?.count ?? 0 > 0){
                userData["phone"] = phoneNumber!
            }
            //
            let accountInfo = Account(jsonDict: userData)
            return LoginResult.ok(output: accountInfo)
        }
        else{
            var strMessage = NSLocalizedString("something_went_wrong", comment: "")
            var statusCode : Int = AppURL.APIStatusCode.INTERNAL_SERVER_ERROR.rawValue
            if let error = errorResult{
                if let errorItem = error as NSError?{
                    statusCode = errorItem.code
                    if let errCode = AuthErrorCode(rawValue: statusCode) {
                        if(errCode == .userNotFound){
                            strMessage = NSLocalizedString("invalid_email_or_password", comment: "")
                        }
                        else if(errCode == .networkError){
                            strMessage = NSLocalizedString("invalid_offline_network", comment: "")
                        }
                    }
                }
            }
            return LoginResult.failed(output: (statusCode,strMessage))
        }
    }
    
    static func dateForRFC3339DateTimeString(rfc3339DateTimeString: String) -> Date? {
        let rfc3339DateFormatter = DateFormatter()
        rfc3339DateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
        rfc3339DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
   
        // Convert the RFC 3339 date time string to an NSDate.
        let result = rfc3339DateFormatter.date(from: rfc3339DateTimeString)
        return result
    }
    
    static func resetImageData(_ userId: String){
        //Clear image cache
        let url = Util.parseURLAvatarImageFireStore(userId)
        UIImageView.clearCache(for: url)
        
    }
    
    static func getFilterTitle(filter : Int) -> String{
        let keyValue = Constant.keyValue
        if(filter == Constant.TaskFilter.TaskID.allTask.rawValue){
            return Constant.TaskFilter.allTalk[keyValue] as! String
        }
        else if(filter == Constant.TaskFilter.TaskID.myTask.rawValue){
            return Constant.TaskFilter.myTask[keyValue] as! String
        }
        else if(filter == Constant.TaskFilter.TaskID.priorityTask.rawValue){
            return Constant.TaskFilter.priorityTask[keyValue] as! String
        }
        else{
            return Constant.TaskFilter.completedTask[keyValue] as! String
        }
    }
    

    static func checkPermissionEditTask(_ ownedTaskId: String) -> Bool{
        var isEditPermission : Bool = false
        //
        let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectOwnedId = AppSettings.sharedSingleton.project?.owned.userId ?? ""
        if(myUserId.count > 0){
            if(myUserId == projectOwnedId || myUserId == ownedTaskId){
                isEditPermission = true
            }
        }
        if(isEditPermission == false){
            let orangnizationOwnedId = AppSettings.sharedSingleton.orangnization?.owner.userId ?? ""
            if(myUserId.count > 0 && orangnizationOwnedId.count > 0){
                if(myUserId == orangnizationOwnedId){
                    isEditPermission = true
                }
            }
        }
        //
        return isEditPermission
    }
    
    static func paramsForSendImageFileUpload(file: File,projectId: String, updatedURL: String) -> [String:Any]{
        guard let userID = AppSettings.sharedSingleton.account?.userId else {
            return [:]
        }
        //
        let fileId = file.fileId
        let fileName = file.name
        let GOOLGE_FIREBASE_STOGRADE = "GoogleFireBase:Uploads"
        let linkFile = "/ufs/" + GOOLGE_FIREBASE_STOGRADE + fileId + fileName
        //Encoding
        let dictSize : [String:Any] = ["size":["width":file.width,"height":file.height]]
        let dictFile : [String:Any] = ["_id":fileId,"name":fileName,"store":GOOLGE_FIREBASE_STOGRADE,
                                       "userId":userID,"pid":projectId,"path":linkFile,
                                      "associate":[file.taskId], "type":AppURL.APIDomains.FireStorageImageType,"size":file.size,"identify":dictSize,"FireBaseStorage":["path":updatedURL]]
        return dictFile
    }
    
    static func formatMessageHTMLContent(content: String) -> String{
        let fontSizeNormal : String = "14"
        let contentHTML =
            "<html>" +
            "<head>" +
            "<meta name=\"viewport\" content=\"initial-scale=1, maximum-scale=1\">" +
            "<style type=\"text/css\">" +
            "@font-face { font-family: 'LatoRegular';src: url('Lato-Regular.ttf')} " +
            "@font-face { font-family: 'LatoItalic';src: url('Lato-Italic.ttf')} " +
            "@font-face { font-family: 'LatoBold';src: url('Lato-Bold.ttf')} " +
            "body { font-family: 'LatoRegular';font-size: " + fontSizeNormal + "px; } " +
            "strong { font-family: 'LatoBold' } " +
            "em { font-family: 'LatoItalic' } " +
            ".mention {color: #0080FF;font-size: 15px;font-weight: bold} " +
            "</style>" +
            "</head>" +
            "<body>" +
            content +
            "</body>" +
        "</html>"
        return contentHTML
    }
    
    static func signOutCurrentAccount(){
        do {
            try Auth.auth().signOut()
            GIDSignIn.sharedInstance()?.signOut()
            //
            AppSettings.sharedSingleton.saveAccount(nil)
            AppSettings.sharedSingleton.saveProject(nil)
            AppSettings.sharedSingleton.saveOrangnization(nil)
        } catch _ {
        }
    }
    
    static func subTextBySearchKeyword(strOriginText: String,strKeyword: String) -> String{
        //
        var strContent = strOriginText
        //
        if let rangeFounded = strOriginText.range(of: strKeyword, options: .widthInsensitive){
            let strFirstText = strContent[...rangeFounded.lowerBound]
            let strLastText = strContent[rangeFounded.lowerBound...]
            
            //
            if(strFirstText.count > 0){
                //
                let SPACE_CHARACTER : String = " "
                //
                let maxSub : Int = 5
                let space = SPACE_CHARACTER
                
                var strSubFirstText : String = ""
                let arraySpace = strFirstText.components (separatedBy: space)
                if(arraySpace.count > maxSub){
                    let arraySubSpace = arraySpace[arraySpace.count - maxSub...maxSub]
                    strSubFirstText = arraySubSpace.joined(separator: space)
                    strSubFirstText = "... " + strSubFirstText
                }
                else{
                    strSubFirstText = arraySpace.joined(separator: space)
                    strSubFirstText = "... " + strSubFirstText
                }
                let strLastString = arraySpace[arraySpace.count-1]
                if(strLastString.count == 0){
                    strContent = strSubFirstText + strLastText
                }
                else{
                    strContent = strSubFirstText + space + strLastText
                }
            }
        }
        
        
        return strContent
    }
    
    static func setAttributeStringForSearch(lbText: UILabel, withSearch strKeyword: String, fontSearch: UIFont, colorSearch: UIColor, fontNormal: UIFont, colorNormal: UIColor){
        if let strText = lbText.text{
            if(strText.count > 0){
                let attributedString = NSMutableAttributedString(string: strText)
                if(strKeyword.count > 0){
                    let arrayRange : [Range<String.Index>] = strText.ranges(of: strKeyword)
                    for range in arrayRange {
                        let foundRange = NSRange(range, in: strText)
                        //
                        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: colorSearch, range: foundRange)
                        attributedString.addAttribute(NSAttributedString.Key.font, value: fontSearch, range: foundRange)
                    }
                }
                else{
                    let foundRange = NSRange(location: 0, length: strText.count)
                    attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: colorNormal, range: foundRange)
                    attributedString.addAttribute(NSAttributedString.Key.font, value: fontNormal, range: foundRange)
                }
                //
                lbText.attributedText = attributedString
            }
        }
    }
    
    static func getUserIdFrom(email: String) -> String {
        var userId = ""
        if let currentUsers = AppSettings.sharedSingleton.orangnization?.users{
            if(currentUsers.count > 0){
                for user in currentUsers {
                    let emailItem = user.email
                    let usernameItem = user.username
                    if(emailItem == email || usernameItem == email){
                        userId = user.userId
                        break;
                    }
                }
            }
        }
        //
        return userId
    }
    
    
    
    
    
    static func getPushNotificationType(remoteNotificationData : [String:Any]) -> Constant.PushNotificationDataType{
        var dataType : Constant.PushNotificationDataType = .emptyNotification
        //
        if let notificationType = remoteNotificationData[SettingString.NotificationType] as? String{
            if(notificationType == NotificationTypeKeyString.NotificationType_CommentAgenda){
                dataType = .newCommentAgenda
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_SunmitAgenda || notificationType == NotificationTypeKeyString.NotificationType_DoneAgenda || notificationType == NotificationTypeKeyString.NotificationType_CompletedAllTasksAgenda){
                dataType = .planTask
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_AddMessage || notificationType == NotificationTypeKeyString.NotificationType_commentTask ||
                notificationType == NotificationTypeKeyString.NotificationType_Mention){
                //
                if(notificationType == NotificationTypeKeyString.NotificationType_Mention){
                    if let roomType = remoteNotificationData[SettingString.RoomType] as? String{
                        if(roomType ==  Constant.RoomTypes.task){
                            dataType = .newCommentTask
                        }
                        else{
                            dataType = .newCommentAgenda
                        }
                    }
                    else{
                        dataType = .newCommentTask
                    }
                }
                else{
                    dataType = .newCommentTask
                }
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_AddAssignee ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskPriority ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskUnpriority ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskDuedate ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskDescription ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskLabel){
                dataType = .assignTask
            }
        }
        return dataType
    }
    
    static func parseDataFromPushNotification(remoteNotificationData : [String:Any]) -> [String:Any]{
        var result : [String:Any] = [:]
        //
        var notificationId = ""
        var projectDataId = ""
        var roomDataId = ""
        var roomDataName = ""
        var roomDataType = ""
        var messageDataId = ""
        var userDataId = ""
        var dateTimestamp = Date()
        if let project = remoteNotificationData["project"] as? [String: Any]{
            if let projectId = project["_id"] as? String{
                projectDataId = projectId
            }
        }
//        if let room = remoteNotificationData["room"] as? [String: Any]{
        if let room = remoteNotificationData["sub"] as? [String: Any]{
            if let roomId = room["rid"] as? String{
                roomDataId = roomId
            }
            if let roomName = room["name"] as? String{
                roomDataName = roomName
            }
            if let roomType = room["t"] as? String{
                roomDataType = roomType
            }
        }
        else if let room = remoteNotificationData["room"] as? [String: Any]{
            if let roomId = room["rid"] as? String{
                roomDataId = roomId
            }
            if let roomName = room["name"] as? String{
                roomDataName = roomName
            }
            if let roomType = room["t"] as? String{
                roomDataType = roomType
            }
        }
        //
        if let message = remoteNotificationData["msg"] as? [String: Any]{
            if let messageId = message["_id"] as? String{
                messageDataId = messageId
            }
            if let user = message["u"] as? [String: Any]{
                if let userId = user["_id"] as? String{
                    userDataId = userId
                }
            }
            if let timestamp = message["ts"] as? String{
                dateTimestamp = Util.dateForRFC3339DateTimeString(rfc3339DateTimeString: timestamp) ?? Date()
            }
        }
        if let notificationDataId = remoteNotificationData[SettingString.NotificationId] as? String{
            notificationId = notificationDataId
        }
        //
        if let notificationType = remoteNotificationData[SettingString.NotificationType] as? String{
            result[SettingString.NotificationId] = notificationId
            result[SettingString.NotificationType] = notificationType
            if(notificationType == NotificationTypeKeyString.NotificationType_CommentAgenda){
                result[SettingString.ProjectId] = projectDataId
                result[SettingString.AgendaId] = roomDataId
                result[SettingString.Date] = dateTimestamp
                //
                result[SettingString.MessageId] = messageDataId
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_AddMessage || notificationType == NotificationTypeKeyString.NotificationType_commentTask){
                result[SettingString.ProjectId] = projectDataId
                result[SettingString.RoomId] = roomDataId
                result[SettingString.RoomName] = roomDataName
                result[SettingString.Date] = dateTimestamp
                //
                result[SettingString.MessageId] = messageDataId
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_Mention){
                result[SettingString.ProjectId] = projectDataId
                result[SettingString.RoomId] = roomDataId
                result[SettingString.RoomName] = roomDataName
                result[SettingString.RoomType] = roomDataType
                result[SettingString.Date] = dateTimestamp
                //
                result[SettingString.MessageId] = messageDataId
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_DoneAgenda || notificationType == NotificationTypeKeyString.NotificationType_CompletedAllTasksAgenda || notificationType == NotificationTypeKeyString.NotificationType_SunmitAgenda){
                result[SettingString.ProjectId] = projectDataId
                result[SettingString.AgendaId] = roomDataId
                result[SettingString.UserId] = userDataId
                result[SettingString.Date] = dateTimestamp
            }
            else if(notificationType == NotificationTypeKeyString.NotificationType_AddAssignee ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskPriority ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskUnpriority ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskDuedate ||
                notificationType == NotificationTypeKeyString.NotificationType_TaskDescription){
                result[SettingString.ProjectId] = projectDataId
                result[SettingString.RoomId] = roomDataId
                result[SettingString.RoomName] = roomDataName
                result[SettingString.UserId] = userDataId
                result[SettingString.Date] = dateTimestamp
                //
            }
            else{
                
            }
        }
        return result
    }
    
    static func checkPushNotificationOtherProject(remoteNotificationData : [String:Any]) -> Bool{
        var isOtherProject : Bool = false
        if let projectDataId = remoteNotificationData[SettingString.ProjectId] as? String{
            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            if(projectDataId != projectId){
                isOtherProject = true
            }
        }
        return isOtherProject
    }
    
    static func parseNotificationDataToPushNotification(notificationData: NotificationData) -> [String:Any]{
        var result : [String:Any] = [:]
        result[SettingString.NotificationType] = notificationData.type
        result[SettingString.NotificationId] = notificationData.notificationId
        result[SettingString.ProjectId] = notificationData.projectId
        if(notificationData.type == NotificationTypeKeyString.NotificationType_CommentAgenda){
            result[SettingString.ProjectId] = notificationData.projectId
            result[SettingString.AgendaId] = notificationData.room.roomId
            result[SettingString.Date] = notificationData.createdDate
            //
            result[SettingString.MessageId] = notificationData.message.messageId
        }
        else if(notificationData.type == NotificationTypeKeyString.NotificationType_AddMessage || notificationData.type == NotificationTypeKeyString.NotificationType_commentTask){
            result[SettingString.ProjectId] = notificationData.projectId
            result[SettingString.RoomId] = notificationData.room.roomId
            result[SettingString.RoomName] = notificationData.room.name
            result[SettingString.Date] = notificationData.createdDate
            //
            result[SettingString.MessageId] = notificationData.message.messageId
        }
        else if(notificationData.type == NotificationTypeKeyString.NotificationType_Mention){
            result[SettingString.ProjectId] = notificationData.projectId
            result[SettingString.RoomId] = notificationData.room.roomId
            result[SettingString.RoomName] = notificationData.room.name
            result[SettingString.RoomType] = notificationData.room.roomType
            result[SettingString.Date] = notificationData.createdDate
            //
            result[SettingString.MessageId] = notificationData.message.messageId
        }
        else if(notificationData.type == NotificationTypeKeyString.NotificationType_DoneAgenda || notificationData.type == NotificationTypeKeyString.NotificationType_CompletedAllTasksAgenda || notificationData.type == NotificationTypeKeyString.NotificationType_SunmitAgenda){
            result[SettingString.ProjectId] = notificationData.projectId
            result[SettingString.AgendaId] = notificationData.room.roomId
            result[SettingString.UserId] = notificationData.message.user.userId
            result[SettingString.Date] = notificationData.createdDate
        }
        else if(notificationData.type == NotificationTypeKeyString.NotificationType_AddAssignee ||
                       notificationData.type == NotificationTypeKeyString.NotificationType_TaskPriority ||
                       notificationData.type == NotificationTypeKeyString.NotificationType_TaskUnpriority ||
                       notificationData.type == NotificationTypeKeyString.NotificationType_TaskDuedate ||
                       notificationData.type == NotificationTypeKeyString.NotificationType_TaskDescription || notificationData.type == NotificationTypeKeyString.NotificationType_TaskLabel){
            result[SettingString.ProjectId] = notificationData.projectId
            result[SettingString.RoomId] = notificationData.room.roomId
            result[SettingString.RoomName] = notificationData.room.name
            result[SettingString.Date] = notificationData.createdDate
            result[SettingString.UserId] = notificationData.message.user.userId
            //
            result[SettingString.MessageId] = notificationData.message.messageId
        }
        else{
            
        }
        
        return result
        
    }
}

class ThrottleHelper {
    // helper function to delay whatever's in the callback
    class func throttle(seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
    }
}

