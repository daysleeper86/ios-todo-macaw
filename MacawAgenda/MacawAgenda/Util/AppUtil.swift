//
//  UtilApp.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/31/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

class AppUtil{
    static func resetCountingChat(badgeNumber: Int, isShow: Bool){
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        let rootViewController = appDelegate.window?.rootViewController
        if let navigationRootView = rootViewController as? ExampleNavigationController{
            let maxDisplay : Int = 99
            var strBadge : String = ""
            if (badgeNumber > 0) {
                strBadge = String(badgeNumber)
                if (badgeNumber > maxDisplay) {
                    strBadge = "99+"
                }
            }
            //
            DispatchQueue.main.async {
                if (navigationRootView.viewControllers.count > 0){
                    let viewController = navigationRootView.viewControllers.last
                    if let tabBarController = viewController as? ESTabBarController{
                        let tbNotification : UITabBarItem?  = tabBarController.tabBar.items?[AppDevice.TabbarIndex.Notifications]
                        if (isShow) {
                            if(strBadge.count > 0){
                                tbNotification?.badgeValue = strBadge
                                if (badgeNumber > maxDisplay) {
                                    UIApplication.shared.applicationIconBadgeNumber = maxDisplay
                                }
                                else{
                                    UIApplication.shared.applicationIconBadgeNumber = badgeNumber
                                }
                            }
                            else{
                                tbNotification?.badgeValue = nil
                                UIApplication.shared.applicationIconBadgeNumber = 0
                            }
                        }
                        else{
                            tbNotification?.badgeValue = nil
                            UIApplication.shared.applicationIconBadgeNumber = 0
                        }
                    }
                }
                
            }
        }
    }
    
    static func getSelectedTabbar() -> UIViewController?{
        var navigationController : UIViewController?
        if let strongNavigationController = AppDelegate().sharedInstance().navigationController{
            if(strongNavigationController.viewControllers.count > 0){
                let firstViewController = strongNavigationController.viewControllers[0]
                if let tabBarController = firstViewController as? ESTabBarController{
                    if let selectedNavigationController = tabBarController.selectedViewController{
                        navigationController = selectedNavigationController
                    }
                    
                }
            }
        }
        return navigationController;
    }
    
    static func checkNavigationControllerIsTabbar() -> UITabBarController?{
        var resultTabBarController : UITabBarController?
        if let strongNavigationController = AppDelegate().sharedInstance().navigationController{
            if(strongNavigationController.viewControllers.count > 0){
                let firstViewController = strongNavigationController.viewControllers[0]
                if let tabBarController = firstViewController as? ESTabBarController{
                    resultTabBarController = tabBarController
                }
            }
        }
        return resultTabBarController
    }
    
    static func getTopViewController() -> UIViewController?{
        var viewController : UIViewController?
        if let selectedViewController = AppUtil.getSelectedTabbar(){
            if let navigationController = selectedViewController.navigationController{
                let n = navigationController.viewControllers.count
                if(n > 0){
                    viewController = navigationController.viewControllers[n-1]
                }
            }
        }
        return viewController
    }
}
