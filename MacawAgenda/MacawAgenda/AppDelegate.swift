//
//  AppDelegate.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FirebaseStorage
import FirebaseAuth
import FirebaseFirestore
import UserNotifications
import Analytics
import XCGLogger

let log: XCGLogger = _xcode_workaround_log

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController : UINavigationController?
    
    var backgroundTask : UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    var isGoToPush: Bool = false
    var isEnterBackground: Bool = false
    var remoteNotificationData : [String:Any]?
    var currentProject : Project?
    //Views
    lazy var bannerInternetConnection : UIView = {
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        //
        let customView = UIView(frame: CGRect(x: 0, y: hHeaderView, width: AppDevice.ScreenComponent.Width, height: 44.0))
        customView.backgroundColor = AppColor.NormalColors.GRAY_COLOR
        //
        let lbTitle = UILabel(frame: CGRect(x: 0, y: 0, width: customView.frame.size.width, height: customView.frame.size.height))
        lbTitle.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitle.text = NSLocalizedString("can_not_connect_internet_connection", comment: "")
        lbTitle.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitle.textAlignment = .center
        customView.addSubview(lbTitle)
        //
        let wButton = customView.frame.size.height
        let xButton = customView.frame.size.width - wButton - 8
        let btnRetry = UIButton.init(type: .custom)
        btnRetry.accessibilityIdentifier = AccessiblityId.BTN_BACKLOG
        btnRetry.frame = CGRect(x: xButton, y: 0, width: wButton, height: wButton)
        btnRetry.setImage(UIImage(named: "ic_refresh"), for: .normal)
        btnRetry.addTarget(self, action: #selector(retryAction), for: .touchUpInside)
        customView.addSubview(btnRetry)
        
        return customView
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //
        UIApplication.shared.beginReceivingRemoteControlEvents()
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            // Fallback on earlier versions
            UIApplication.shared.statusBarStyle = .default
        }
        
        UINavigationBar.appearance().tintColor = AppColor.NormalColors.WHITE_COLOR
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Storage.storage().maxUploadRetryTime = 2000
        
        setupOfflineFireStorage()
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = AppURL.ExternalKey.GoogleSignIn
        //Enable Cache System
        let sharedCache = URLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 32 * 1024 * 1024, diskPath: nil)
        URLCache.shared = sharedCache
        UIApplication.shared.setMinimumBackgroundFetchInterval(60)
        //
        Timer.scheduledTimer(timeInterval: TimeInterval(5.0), target: self, selector: #selector(calculateNextNumber), userInfo: nil, repeats: true)
        
        setupBackgroudTask()
        //
        setupAnalytic()
        //
        if (launchOptions != nil) {
            // For remote Notification
            if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as! [String : Any]? {
                remoteNotificationData = remoteNotification
            }
        }
        
        setupMainView()
        
        return true
    }
    
    @objc func calculateNextNumber()
    {
        if (UIApplication.shared.applicationState == .active){
            print("App is backgrounded - Remain: \(UIApplication.shared.backgroundTimeRemaining) seconds")
        }
    }
    
    func setupBackgroudTask(){
        self.backgroundTask = UIBackgroundTaskIdentifier.invalid
        self.backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            print("Background handler called. Not running background tasks anymore.")
            UIApplication.shared.endBackgroundTask(self.backgroundTask)
            self.backgroundTask = UIBackgroundTaskIdentifier.invalid
        })
    }
    
    func setupAnalytic(){
        let configuration = SEGAnalyticsConfiguration(writeKey: AppURL.ExternalKey.AnalyticKey)
        configuration.trackApplicationLifecycleEvents = true // Enable this to record certain application events automatically!
        configuration.recordScreenViews = false // Enable this to record screen views automatically!
        SEGAnalytics.setup(with: configuration)
    }
    
    func setupMainView(){
        let userID = Auth.auth().currentUser?.uid
        let account = AppSettings.sharedSingleton.account
        
        if (account != nil){
            if(userID != nil){
                let project = AppSettings.sharedSingleton.project
                if (project != nil){
                    setupWelcomeView()
                }
                else{
                    setupOnboardingView()
                }
            }
            else{
                let project = AppSettings.sharedSingleton.project
                if (project != nil){
                    setupWelcomeView()
                }
                else{
                    setupTeamInfoView()
                }
            }
        }
        else{
            setupOnboardingView()
        }
    }
    
    func setupOnboardingView(){
        if (self.window == nil)
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        }
        
        let onBoardingView = OnBoardingAppViewController(nibName: nil, bundle: nil)
        navigationController = UINavigationController(rootViewController: onBoardingView)
        navigationController!.navigationBar.isTranslucent = false
        //Init window
        self.window?.rootViewController = navigationController;
        self.window?.makeKeyAndVisible();
    }
    
//    func setupTeamAgendaView(){
//        if (self.window == nil)
//        {
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            self.window?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
//        }
//
//        let teamAgenda = TeamAgendaViewController(nibName: nil, bundle: nil)
//        navigationController = UINavigationController(rootViewController: teamAgenda)
//        navigationController!.navigationBar.isTranslucent = false
//        //Init window
//        self.window?.rootViewController = navigationController;
//        self.window?.makeKeyAndVisible();
//    }
    
    func setupWelcomeView(){
        if (self.window == nil)
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        }
        
        let welcomeView = WelcomeLoadingViewController(nibName: nil, bundle: nil)
        navigationController = UINavigationController(rootViewController: welcomeView)
        navigationController!.navigationBar.isTranslucent = false
        //Init window
        self.window?.rootViewController = navigationController;
        self.window?.makeKeyAndVisible();
    }
    
    func setupTeamInfoView(){
        if (self.window == nil)
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        }
        
        let teamInfoView = TeamInformationViewController(nibName: nil, bundle: nil)
        navigationController = UINavigationController(rootViewController: teamInfoView)
        navigationController!.navigationBar.isTranslucent = false
        //Init window
        self.window?.rootViewController = navigationController;
        self.window?.makeKeyAndVisible();
    }
    
    func setupTeamAgendaView(){
        if (self.window == nil)
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        }
        
        navigationController = MainViewController.customIrregularityStyle(delegate:nil)
//        navigationController!.navigationBar.isTranslucent = false
        //Init window
        self.window?.rootViewController = navigationController;
        self.window?.makeKeyAndVisible();
        
    }
    
    func alertSessionExpiredIfNeed(code: Int) -> Bool{
        if(code == AppURL.APIStatusCode.UNAUTHORIZED.rawValue){
            let message = NSLocalizedString("the_session_user_has_expired", comment: "")
            var toastStyle = ToastStyle()
            toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
            toastStyle.cornerRadius = 4.0
            toastStyle.messageAlignment = .center
            toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
            
            self.window?.makeToast(message, duration: 3.0, position: .top, style: toastStyle, completion: { didTap in
                self.setupOnboardingView()
            })
            return true
        }
        else if(code == AppURL.APIStatusCode.TOO_MANY_REQUESTS.rawValue){
            return true
        }
        return false
    }
    
    func makeSystemToast(message: String){
        var toastStyle = ToastStyle()
        toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
        toastStyle.cornerRadius = 4.0
        toastStyle.messageAlignment = .center
        toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
        
        self.window?.makeToast(message, duration: 3.0, position: .top, style: toastStyle, completion: { didTap in
            
        })
    }
    
    func setupOfflineFireStorage(){
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        settings.cacheSizeBytes = FirestoreCacheSizeUnlimited
        // Enable offline data persistence
        let db = Firestore.firestore()
        db.settings = settings
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { settings in
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    //Notification permission
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) {[weak self] // 2
                granted, error in
                guard granted else { return }
                self?.getNotificationSettings()
            }
        }
        else{
            DispatchQueue.main.async {
                //Settings
                let settings = UIUserNotificationSettings(types: [.alert,.badge,.sound], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
                UIApplication.shared.registerForRemoteNotifications()
                //
                //Cache
                UIApplication.shared.beginReceivingRemoteControlEvents()
            }
        }
    }
    
    func alertNoInternetConntect(reachable : Bool){
        if let strongWindow = self.window{
            if(reachable == true){
                UIView.transition(with: strongWindow, duration: 0.25, options: [.transitionCrossDissolve], animations: {[weak self] in
                    self?.bannerInternetConnection.removeFromSuperview()
                }, completion: nil)
            }
            else{
                UIView.transition(with: strongWindow, duration: 0.25, options: [.transitionCrossDissolve], animations: {[weak self] in
                    if let strongBanner = self?.bannerInternetConnection{
                        self?.window?.addSubview(strongBanner)
                    }
                    
                    }, completion: nil)
                
            }
        }
        
    }
    
    func clearPushNotificationData(){
        self.isGoToPush = false
        self.remoteNotificationData = nil
    }
    
    @objc func retryAction(){
        //
        DispatchQueue.main.async {
            if let strongNavigationController = self.navigationController{
                if(strongNavigationController.viewControllers.count > 0){
                    let firstViewController = strongNavigationController.viewControllers[0]
                    if let welcomeLoadingViewController = firstViewController as? WelcomeLoadingViewController {
                        //do something if it's an instance of that class
                        self.alertNoInternetConntect(reachable: true)
                        welcomeLoadingViewController.retryConnection()
                        
                    }
                    else{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_RETRY_NETWORK), object: nil)
                    }
                }
            }
        }
    }
    
    func storeRemoteNotificationData(notificationData: [String : Any]){
        self.remoteNotificationData = notificationData
        //
        if (self.isGoToPush == true){
            return;
        }
        self.isGoToPush = true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        //
        if let dictUserInfo = userInfo as? [String : Any]{
            let formatNotificationData = Util.parseDataFromPushNotification(remoteNotificationData: dictUserInfo)
            storeRemoteNotificationData(notificationData: formatNotificationData)
        }
        else{ return }
        //
        if (UIApplication.shared.applicationState == .inactive || UIApplication.shared.applicationState == .background) {
            LoggerApp.logInfo("Notification received by running app");
            //
            if let strongNotificationData = self.remoteNotificationData{
                //
                GlobalAPIManager.sharedSingleton.readNotification(notificationData: strongNotificationData)
                //
                if(Util.checkPushNotificationOtherProject(remoteNotificationData: strongNotificationData) == true){
                    setupWelcomeView()
                }
                else{
                    //
                    if let remoteNotificationData = self.remoteNotificationData{
                        let dataType = Util.getPushNotificationType(remoteNotificationData: remoteNotificationData)
                        if(dataType != .emptyNotification){
                            selectTeamAgendaTabbar()
                        }
                    }

                }
            }

        }
        else {
            LoggerApp.logInfo("App opened from Notification");
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        
    }
    
    func alertNotificationInApp(title: String?,attributeString: NSMutableAttributedString?, message: String?,data: NotificationData?){
        let notiData = HDNotificationData(
            iconImage: UIImage(named: "AppIcon"),
            appTitle: "Notification".uppercased(),
            title: title,
            attributedString: attributeString,
            message: message,
            time: "now",
            data: data)
        let _ = HDNotificationView.show(data: notiData, onTap: {[weak self] in
            if let strongNotificationData = notiData.data{
                self?.processNotificationAllProject(notification: strongNotificationData)
            }
        }, onDidDismiss: nil)
    }
    
    //This is the two delegate method to get the notification in iOS 10..
    //First for foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options:UNNotificationPresentationOptions) -> Void)
    {
        print("Handle push from foreground")
        // custom code to handle push while app is in the foreground
        print("\(notification.request.content.userInfo)")
    }
    
    //Second for background and close
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response:UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
    }
    
    func showChatViewWithMessage(projectId: String, messageId: String) -> Bool{
        let viewController = AppUtil.getTopViewController()
        if let teamAgendaViewController = viewController as? TeamAgendaViewController{
            let currentProjectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            if (currentProjectId.count > 0){
                if(currentProjectId == projectId){
                    
                }
                else{
                    
                }
            }
            if(teamAgendaViewController.checkActivityPresenting() == true){
                return true
            }
            else{
//                alertNotificationInApp(title: "This is a sample of HDNotificationView ⏰", message: "This area that you can input some message to notify to user 🔔")
//                teamAgendaViewController.discussionAction()
            }
        }
        else{
            if let strongTabbarController = AppUtil.checkNavigationControllerIsTabbar(){
                if let navController = AppUtil.getSelectedTabbar(){
                    navController.navigationController?.popToRootViewController(animated: false)
                    strongTabbarController.selectedIndex = 0
                    //
                    if let strongViewController = strongTabbarController.viewControllers{
                        if(strongViewController.count > 0){
                            if let teamAgendaViewController = strongViewController[0] as? TeamAgendaViewController{
                                teamAgendaViewController.discussionAction()
                            }
                        }
                    }
                }
            }
        }
//        if([viewController isKindOfClass:[ChattingViewController class]])
//        {
//            //
//            ChattingViewController *chattingViewController = (ChattingViewController *)viewController;
//            BOOL isExist = [chattingViewController gotoConversationFromMessage:message];
//            if(isExist == NO){
//                [chattingViewController.navigationController popToRootViewControllerAnimated:YES];
//                //
//                ConversationViewController *conversationViewController = (ConversationViewController *)mainViewController.messageNavigationController.viewControllers[0];
//                [conversationViewController openRoomWithMessage:message];
//            }
//            return YES;
//        }
//        else{
//            if(self.window.rootViewController == mainViewController){
//                UINavigationController* navController = [Utils getSelectedTabbar];
//                [navController popToRootViewControllerAnimated:NO];
//                //
//                mainViewController.selectedViewController = mainViewController.messageNavigationController;
//                //
//                ConversationViewController *conversationViewController = (ConversationViewController *)mainViewController.messageNavigationController.viewControllers[0];
//                [conversationViewController openRoomWithMessage:message];
//                //
//                return YES;
//            }
//            else{
//                self.messageGoToPush = message;
//            }
//        }
        return true
    }
    
    func selectBacklogTabbar(){
        if let strongTabbarController = AppUtil.checkNavigationControllerIsTabbar(){
            if let navController = AppUtil.getSelectedTabbar(){
                navController.navigationController?.popToRootViewController(animated: false)
                strongTabbarController.selectedIndex = AppDevice.TabbarIndex.Backlog
                //
                if let strongViewController = strongTabbarController.viewControllers{
                    if(strongViewController.count == AppDevice.TabbarCount){
                        if let backlogViewController = strongViewController[AppDevice.TabbarIndex.Backlog] as? BacklogViewController{
                            if (backlogViewController.isViewLoaded && ((backlogViewController.view?.window) != nil)) {
                                // viewController is visible
                                backlogViewController.viewDidAppear(false)
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    func selectTeamAgendaTabbar(){
        if let strongTabbarController = AppUtil.checkNavigationControllerIsTabbar(){
            if let navController = AppUtil.getSelectedTabbar(){
                navController.navigationController?.popToRootViewController(animated: false)
                strongTabbarController.selectedIndex = AppDevice.TabbarIndex.Collaborate
                //
                if let strongViewController = strongTabbarController.viewControllers{
                    if(strongViewController.count == AppDevice.TabbarCount){
                        if let teamAgendaViewController = strongViewController[AppDevice.TabbarIndex.Collaborate] as? TeamAgendaViewController{
                            //
                            if (teamAgendaViewController.isViewLoaded && ((teamAgendaViewController.view?.window) != nil)) {
                                // viewController is visible
                                teamAgendaViewController.viewDidAppear(false)
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    func processNotificationAllProject(notification: NotificationData){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        //
        let notificationData = Util.parseNotificationDataToPushNotification(notificationData: notification)
        GlobalAPIManager.sharedSingleton.readNotification(notificationData: notificationData)
        //
        if(notification.projectId == projectId){
            AppDelegate().sharedInstance().processNotificationCurrentProject(notification: notification)
        }
        else{
            AppDelegate().sharedInstance().storeRemoteNotificationData(notificationData: notificationData)
            //
            GlobalAPIManager.sharedSingleton.updateDefaultProjectChanged(notification.projectId, completion: {[weak self] isSave in
                if(isSave == false){
                    //
                    self?.makeSystemToast(message: NSLocalizedString("you_can_not_access_this_project_in_app", comment: ""))
                    //
                    self?.clearPushNotificationData()
                }
            })
        }
    }
    
    func processNotificationCurrentProject(notification: NotificationData){
        let notificationData = Util.parseNotificationDataToPushNotification(notificationData: notification)
        storeRemoteNotificationData(notificationData: notificationData)
        navigationForPushNotification(notification: notification,notificationType: notification.type)
    }
    
    func navigationForPushNotification(notification: NotificationData,notificationType: String){
        if(notificationType == NotificationTypeKeyString.NotificationType_Mention){
            if(notification.room.roomType == Constant.RoomTypes.agenda){
                selectTeamAgendaTabbar()
            }
            else{
                selectBacklogTabbar()
            }
        }
        else if(notificationType == NotificationTypeKeyString.NotificationType_AddMessage || notificationType == NotificationTypeKeyString.NotificationType_commentTask || notificationType == NotificationTypeKeyString.NotificationType_SunmitAgenda || notificationType == NotificationTypeKeyString.NotificationType_DoneAgenda || notificationType == NotificationTypeKeyString.NotificationType_CompletedAllTasksAgenda || notificationType == NotificationTypeKeyString.NotificationType_CommentAgenda){
            selectTeamAgendaTabbar()
        }
        else if(notificationType == NotificationTypeKeyString.NotificationType_AddAssignee ||
            notificationType == NotificationTypeKeyString.NotificationType_TaskPriority ||
            notificationType == NotificationTypeKeyString.NotificationType_TaskUnpriority ||
            notificationType == NotificationTypeKeyString.NotificationType_TaskDuedate ||
            notificationType == NotificationTypeKeyString.NotificationType_TaskDescription ||
            notificationType == NotificationTypeKeyString.NotificationType_TaskLabel){
            selectBacklogTabbar()
        }
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types.isEmpty == false{
            application.registerForRemoteNotifications()
            //store to local (notification setting)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        AppSettings.sharedSingleton.saveMiscAccount(nil, name: nil, password: nil, deviceToken: token)
        //
        let dict = NSDictionary(objects: [token], forKeys: [Constant.keyValue as NSCopying])
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_RECEIVE_DEVICE_TOKEN), object: dict)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        isEnterBackground = true
        GlobalAPIManager.sharedSingleton.updateUserActiveState(state: isEnterBackground)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        setupBackgroudTask()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        isEnterBackground = false
        if AppUtil.checkNavigationControllerIsTabbar() != nil{
            GlobalAPIManager.sharedSingleton.updateUserActiveState(state: isEnterBackground)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        isEnterBackground = true
        GlobalAPIManager.sharedSingleton.updateUserActiveState(state: isEnterBackground)
    }

    // MARK: - AppDelegate Singleton
    func sharedInstance () -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    let cacheDirectory: URL = {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
}



