//
//  TeamAgendaUserItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/2/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TeamAgendaUserItemViewCell: UICollectionViewCell {
    
    //Views
    var backgroundViewCell : UIView?
    var selectedView : UIView?
    var imvAvatar : UIImageView?
    var lbTitleName : UILabel?
    //Data
    static let fontForName : UIFont! = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
    static let maxWidthForName :  CGFloat = 1000
    static let pSmallAvatar : CGFloat = 8
    static let wSmallAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
    static let hSmallAvatar : CGFloat = wSmallAvatar
    static let xSmallAvatar : CGFloat = pSmallAvatar
    static let pTitleName : CGFloat = 8
    
    var user : User?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.SubViewFrame.UserHorizontalItemWidth
        let hBackgroundView : CGFloat = AppDevice.SubViewFrame.UserHorizontalItemHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hAvatar : CGFloat = wAvatar
        
        let hSelectedView : CGFloat = hAvatar
        let wSelectedView : CGFloat = 0
        let ySelectedView : CGFloat = (hBackgroundView-hAvatar)/2
        
        let xAvatar : CGFloat = 8
        let yAvatar : CGFloat = 0
        
        selectedView = UIView(frame: CGRect(x: xBackgroundView, y: ySelectedView, width: wSelectedView, height: hSelectedView))
        selectedView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        selectedView?.layer.cornerRadius = selectedView!.frame.size.height/2
        selectedView?.layer.masksToBounds = true
        backgroundViewCell?.addSubview(selectedView!)
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = imvAvatar!.frame.size.height/2
        imvAvatar?.layer.masksToBounds = true
        selectedView?.addSubview(imvAvatar!)
        
        // Title name
        let xTitleName : CGFloat = TeamAgendaUserItemViewCell.xSmallAvatar + TeamAgendaUserItemViewCell.wSmallAvatar + TeamAgendaUserItemViewCell.pTitleName
        let yTitleName : CGFloat = 0
        let wTitleName : CGFloat = selectedView!.frame.size.width - xTitleName - TeamAgendaUserItemViewCell.pTitleName
        let hTitleName : CGFloat = selectedView!.frame.size.height
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.backgroundColor = .clear
        lbTitleName?.textAlignment = .center
        lbTitleName?.numberOfLines = 1
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = TeamAgendaUserItemViewCell.fontForName
        selectedView?.addSubview(lbTitleName!)
    }
    
    func setValueForCell(user: User){
        self.user = user
        //
        stateForAvatar(user)
        //
    }
    
    func showText(_ showText: Bool){
        if(showText ==  true){
            guard let strongUser = self.user else { return }
            lbTitleName?.isHidden = false
            lbTitleName?.text = strongUser.getUserName()
            //
            let ySmallAvatar : CGFloat = (selectedView!.frame.size.height - TeamAgendaUserItemViewCell.hSmallAvatar) / 2
            imvAvatar?.frame = CGRect(x: TeamAgendaUserItemViewCell.xSmallAvatar, y: ySmallAvatar, width: TeamAgendaUserItemViewCell.wSmallAvatar, height: TeamAgendaUserItemViewCell.hSmallAvatar)
            imvAvatar?.layer.cornerRadius = imvAvatar!.frame.size.height/2
            
            //
            let xTitleName : CGFloat = TeamAgendaUserItemViewCell.xSmallAvatar + TeamAgendaUserItemViewCell.wSmallAvatar + TeamAgendaUserItemViewCell.pTitleName
            let sizeName : CGSize = lbTitleName!.text!.getTextSizeWithString(TeamAgendaUserItemViewCell.maxWidthForName,lbTitleName!.font)
            
            lbTitleName?.frame.origin.x = xTitleName
            lbTitleName?.frame.size.width = sizeName.width
             
            selectedView?.frame.size.width = lbTitleName!.frame.origin.x+lbTitleName!.frame.size.width+TeamAgendaUserItemViewCell.pTitleName
        }
        else{
            lbTitleName?.isHidden = true
            //
            let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
            let hAvatar : CGFloat = wAvatar
            let xAvatar : CGFloat = 0
            let yAvatar : CGFloat = 0
            
            selectedView?.frame.size.width = wAvatar
            
            imvAvatar?.frame = CGRect(x: xAvatar, y: yAvatar , width: wAvatar, height: hAvatar)
            imvAvatar?.layer.cornerRadius = imvAvatar!.frame.size.height/2
        }
        //For reset label font if need
        if (imvAvatar?.image) != nil{}
        else{
            guard let strongUser = self.user else { return }
            let strUserName = strongUser.getUserName()
            imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
        }
        
    }
    
    func stateForAvatar(_ user : User){
        //Cancel current request
        imvAvatar?.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID.count > 0){
            let strURL = Util.parseURLAvatarImageFireStore(strUserID)
            imvAvatar?.sd_setImage(with: URL(string: strURL), placeholderImage: nil, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongSelf = self, let strongObject = strongSelf.user else { return }
                if let imageValue = image{
                    //Current userId
                    let strCurrentUserID = strongObject.userId
                    if(strCurrentUserID == strUserID){
                        if(imageValue.size.width > 0){
                            strongSelf.imvAvatar?.backgroundColor = .clear
                            strongSelf.imvAvatar?.removeLabelTextIfNeed()
                        }
                        else{
                            let strUserName = strongObject.getUserName()
                            strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                }
                else{
                    //Current userId
                    let strCurrentUserID = strongObject.userId
                    if(strCurrentUserID == strUserID){
                        let strUserName = strongObject.getUserName()
                        strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
            })
        }
        else{
            let strUserName = user.getUserName()
            imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
        }
    }
    

}
