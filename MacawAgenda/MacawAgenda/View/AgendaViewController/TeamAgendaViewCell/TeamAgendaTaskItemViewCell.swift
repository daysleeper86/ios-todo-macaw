//
//  TeamAgendaTaskItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TeamAgendaTaskItemViewCell: UITableViewCell {
    //Views
    var backgroundViewCell : UIView!
    var btnCheckbox : UIButton!
    var lbTitleName : UILabel!
    //Data
    var task : Task?
    var itemWidth : CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.WHITE_COLOR
    }
    
    func initView(itemWidth : CGFloat,contentPadding: CGFloat){
        if(self.itemWidth != itemWidth){
            self.itemWidth = itemWidth
            // Background View
            let xBackgroundView : CGFloat = 0
            let yBackgroundView : CGFloat = 0
            let wBackgroundView : CGFloat = itemWidth
            let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            self.contentView.addSubview(backgroundViewCell)
            
            // Checkbox
            let wCheckbox : CGFloat = 24
            let hCheckbox : CGFloat = wCheckbox
            let xCheckbox : CGFloat = contentPadding
            let yCheckbox : CGFloat = (hBackgroundView-hCheckbox)/2
            
            btnCheckbox = UIButton.init(type: .custom)
            btnCheckbox.frame = CGRect(x: xCheckbox, y: yCheckbox, width: wCheckbox, height: hCheckbox)
            backgroundViewCell.addSubview(btnCheckbox)
            
            // Title name
            let pLeftTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let pRightTitleName : CGFloat = contentPadding
            let xTitleName : CGFloat = xCheckbox + wCheckbox + pLeftTitleName
            let wTitleName : CGFloat = wBackgroundView - xTitleName - pRightTitleName
            let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
            let yTitleName : CGFloat = (hBackgroundView-hTitleName)/2
            
            lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.textAlignment = .left
            lbTitleName.numberOfLines = 1
            lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            backgroundViewCell.addSubview(lbTitleName)
        }
        
    }
    
    func resetValue_Cell(){
        lbTitleName.text = ""
        btnCheckbox.setImage(nil, for: .normal)
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        lbTitleName.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        btnCheckbox.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        lbTitleName.startShimmering()
        btnCheckbox.startShimmering()
    }
    
    func stopShimmering_Cell(){
        let isShimmering = lbTitleName.isShimmering()
        if(isShimmering == true){
            //
            lbTitleName.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            btnCheckbox.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            
            lbTitleName.stopShimmering()
            btnCheckbox.stopShimmering()
        }
    }
    
    func startProcessingFinishTask(startAnimationRow: Bool){
        if(startAnimationRow == true){
            backgroundViewCell.startShimmering()
        }
        btnCheckbox.isUserInteractionEnabled = false
    }
    
    func stopProcessingFinishTask(){
        if(backgroundViewCell.isShimmering() == true){
            backgroundViewCell.stopShimmering()
            btnCheckbox.isUserInteractionEnabled = true
        }
    }
    
    func setValueForCell(task: Task, isCanFinished: Bool){
        if(task.name.count > 0){
            //
            stopShimmering_Cell()
            //
            self.task = task
            //
            stateForFinishButton()
            //
            let name = "#" + task.taskNumber + " - " + task.name
            stateForTitleName(name, task)
            //
            if(isCanFinished == true){
                self.btnCheckbox.alpha = 1.0
                self.btnCheckbox.isUserInteractionEnabled = true
            }
            else{
                self.btnCheckbox.alpha = 0.4
                self.btnCheckbox.isUserInteractionEnabled = false
            }
        }
        else{
            self.btnCheckbox.alpha = 1.0
            startShimmering_Cell()
        }
        //
        stopProcessingFinishTask()
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell.backgroundColor = color
    }
    
    func disableCell(){
        
    }
    
    func stateForFinishButton(){
        if let strongTask = self.task{
            if(strongTask.completed == false){
                self.btnCheckbox.setImage(UIImage(named: "ic_uncheck"), for: .normal)
            }
            else{
                self.btnCheckbox.setImage(UIImage(named: "ic_list_checked"), for: .normal)
            }
        }
    }
    
    func stateForTitleName(_ name : String, _ task : Task){
        self.lbTitleName.text = name
        let attributedString = NSMutableAttributedString(string: name)
        //
        if(task.completed == false){
            attributedString.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0, name.count))
            if #available(iOS 13.0, *) {
                attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: AppColor.NormalColors.CLEAR_COLOR, range: NSRange(location: 0, length: name.count))
            }
            self.lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
        }
        else{
            attributedString.addAttribute(NSAttributedString.Key.font, value: self.lbTitleName.font ?? UIFont.systemFont(ofSize: self.lbTitleName.font.pointSize), range: NSRange(location: 0, length: name.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: name.count))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.thick.rawValue, range: NSRange(location: 0, length: name.count))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: name.count))
        }
        self.lbTitleName.attributedText = attributedString
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
