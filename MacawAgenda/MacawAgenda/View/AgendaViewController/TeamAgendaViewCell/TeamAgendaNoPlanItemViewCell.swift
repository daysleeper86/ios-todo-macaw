//
//  TeamAgendaNoPlanItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import SkeletonView

class TeamAgendaNoPlanItemViewCell: UITableViewCell {
    
    //Views
    var backgroundViewCell : UIView!
    var lbTitleName : UILabel!
    var itemWidth : CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    func initView(itemWidth : CGFloat){
        if(self.itemWidth != itemWidth){
            self.itemWidth = itemWidth
            // Background View
            let xBackgroundView : CGFloat = 0
            let yBackgroundView : CGFloat = 0
            let wBackgroundView : CGFloat = itemWidth
            let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            self.addSubview(backgroundViewCell)
            
            
            // Title name
            let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let xTitleName : CGFloat = pTitleName
            let yTitleName : CGFloat = 0
            let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
            let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight * 2 + pTitleName
            
            lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.textAlignment = .left
            lbTitleName.numberOfLines = 0
            lbTitleName.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            lbTitleName.isSkeletonable = true
            backgroundViewCell.addSubview(lbTitleName)
        }
    }
    
    func resetValue_Cell(){
        lbTitleName.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        lbTitleName.showAnimatedSkeleton(usingColor: AppColor.MicsColors.SHIMMER_COLOR, animation: nil)
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = lbTitleName?.isSkeletonActive{
            if(isShimmering == true){
                //
                lbTitleName.stopSkeletonAnimation()
                lbTitleName.hideSkeleton()
            }
        }
    }
    
    func setValueForCell(agenda: Agenda,height: CGFloat){
        if(backgroundViewCell.frame.size.height != height){
            backgroundViewCell.frame.size.height = height
        }
        //
        let name = agenda.owned.getUserName()
        if(name.count > 0){
            stopShimmering_Cell()
            //
            let empty = " "
            var noPlan = NSLocalizedString("has_no_plan", comment: "")
            if(agenda.date.isToday()){
                noPlan = noPlan + " " + NSLocalizedString("today", comment: "").lowercased()
            }
            else{
                noPlan = noPlan + " " + NSLocalizedString("yesterday", comment: "").lowercased()
            }
            let textName  = name + empty + noPlan
            //
            let boldFont = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
            let sizeTitleName : CGSize = textName.getTextSizeWithString(lbTitleName.frame.size.width,boldFont)
            let hTitleName : CGFloat = sizeTitleName.height
            let yTitleName : CGFloat = (backgroundViewCell.frame.size.height-hTitleName)/2
            lbTitleName.frame.origin.y = yTitleName
            lbTitleName.frame.size.height = hTitleName
            //
            let attributedString = NSMutableAttributedString(string: textName)
            attributedString.addAttribute(NSAttributedString.Key.font, value: boldFont ?? UIFont.boldSystemFont(ofSize: boldFont!.pointSize), range: NSRange(location: 0, length: name.count))
            self.lbTitleName.attributedText = attributedString
            //
            if(hTitleName > AppDevice.SubViewFrame.TitleHeight){
                self.lbTitleName.textAlignment = .left
            }
            else{
                self.lbTitleName.textAlignment = .center
            }
            //
        }
        else{
            startShimmering_Cell()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
