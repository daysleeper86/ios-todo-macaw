//
//  TeamAgendaTaskNotActionItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Material

class TeamAgendaTaskNotActionItemViewCell: SwipeTableViewCell {
    //Views
    var backgroundViewCell : UIView?
    var spinnerView: NVActivityIndicatorView?
    var errorButton: UIButton?
    var lbTitleName : UILabel?
    //Data
    var task : Task?
    var itemWidth : CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.WHITE_COLOR
    }
    
    func initView(itemWidth : CGFloat,contentPadding: CGFloat){
        self.itemWidth = itemWidth
        if(backgroundViewCell == nil){
            // Background View
            let xBackgroundView : CGFloat = 0
            let yBackgroundView : CGFloat = 0
            let wBackgroundView : CGFloat = itemWidth
            let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            self.contentView.addSubview(backgroundViewCell!)
            
            let pSpinnerView : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let wSpinnerView : CGFloat = 24.0
            let hSpinnerView : CGFloat = wSpinnerView
            let ySpinnerView : CGFloat = (hBackgroundView - hSpinnerView ) / 2
            let xSpinnerView : CGFloat = wBackgroundView - pSpinnerView - wSpinnerView
            let spinnerFrame = CGRect(x: xSpinnerView, y: ySpinnerView, width: wSpinnerView, height: hSpinnerView)
            spinnerView = NVActivityIndicatorView(frame: spinnerFrame,
                                                  type: .ballClipRotate)
            spinnerView?.isHidden = true
            backgroundViewCell?.addSubview(spinnerView!)
            
            errorButton = UIButton(type: .custom)
//            errorButton?.pulseColor = .white
            errorButton?.frame = spinnerFrame
            errorButton?.setImage(UIImage(named: "ic_error_failed"), for: .normal)
            errorButton?.isHidden = true
            backgroundViewCell?.addSubview(errorButton!)
            
            // Title name
            let xTitleName : CGFloat = contentPadding
            let wTitleName : CGFloat = wBackgroundView - ( xTitleName * 2)
            let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
            let yTitleName : CGFloat = (hBackgroundView-hTitleName)/2
            
            lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName?.textAlignment = .left
            lbTitleName?.numberOfLines = 1
            lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            backgroundViewCell?.addSubview(lbTitleName!)
        }
        
    }
    
    func resetValue_Cell(){
        lbTitleName?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        lbTitleName?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = lbTitleName?.isShimmering(){
            if(isShimmering == true){
                //
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                lbTitleName?.stopShimmering()
            }
        }
    }
    
    func startProcessing(color: UIColor,startAnimationRow: Bool){
        //
        errorButton?.isHidden = true
        if(startAnimationRow == true){
            backgroundViewCell?.startShimmering()
        }
        //
        spinnerView?.isHidden = false
        spinnerView?.color = color
        spinnerView?.startAnimating()
        //
        if let strongSpinnerView = spinnerView, let strongTitleName = lbTitleName{
            let wTitleName : CGFloat = strongSpinnerView.frame.origin.x - strongTitleName.frame.origin.x - MemberItemViewCell.pTitleName
            strongTitleName.frame.size.width = wTitleName
        }
        
    }
    
    func stopProcessing(){
        if(spinnerView?.isHidden == false){
            backgroundViewCell?.stopShimmering()
            //
            spinnerView?.isHidden = true
            spinnerView?.stopAnimating()
            //
            if let strongBackgroundViewCell = backgroundViewCell, let strongTitleName = lbTitleName{
                let wTitleName : CGFloat = strongBackgroundViewCell.frame.size.width - strongTitleName.frame.origin.x - MemberItemViewCell.pTitleName
                strongTitleName.frame.size.width = wTitleName
            }
        }
    }
    
    func setValueForCell(task: Task){
        if(task.dataType != .holder){
            stopShimmering_Cell()
            //
            self.task = task
            //
            var name = ""
            if(task.taskNumber.count > 0){
                name = "#" + task.taskNumber + " - " + task.name
            }
            else{
                name = task.name
            }
            stateForTitleName(name, task)
            stateForErrorView()
            //
            if(task.dataType == .processing){
                self.lbTitleName?.startShimmering()
            }
            else{
                self.lbTitleName?.stopShimmering()
            }
        }
        else{
            startShimmering_Cell()
        }
        //
        stopProcessing()
    }
    
    func stateForErrorView(){
        if(self.task?.errorType == false){
            errorButton?.isHidden = true
            //
            if let strongBackgroundViewCell = backgroundViewCell, let strongTitleName = lbTitleName{
                let wTitleName : CGFloat = strongBackgroundViewCell.frame.size.width - strongTitleName.frame.origin.x - MemberItemViewCell.pTitleName
                strongTitleName.frame.size.width = wTitleName
            }
        }
        else{
            errorButton?.isHidden = false
            //
            if let strongSpinnerView = spinnerView, let strongTitleName = lbTitleName{
                let wTitleName : CGFloat = strongSpinnerView.frame.origin.x - strongTitleName.frame.origin.x - MemberItemViewCell.pTitleName
                strongTitleName.frame.size.width = wTitleName
            }
        }
    }
    
    func setDarkModeView(_ darkMode: Bool, titleColor: UIColor){
        var bgColor : UIColor!
        if(darkMode == true){
            bgColor = AppColor.MicsColors.DARK_MENU_COLOR
        }
        else{
            bgColor = AppColor.NormalColors.WHITE_COLOR
        }
        setBackGroundColor(color: bgColor)
        lbTitleName?.textColor = titleColor
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    func stateForTitleName(_ name : String, _ task : Task){
        self.lbTitleName?.text = name
        //
        let attributedString = NSMutableAttributedString(string: name)
        //
        if(task.completed == false){
            attributedString.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0, name.count))
            if #available(iOS 13.0, *) {
                attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: AppColor.NormalColors.CLEAR_COLOR, range: NSRange(location: 0, length: name.count))
            }
            self.lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        }
        else{
            attributedString.addAttribute(NSAttributedString.Key.font, value: self.lbTitleName!.font ?? UIFont.systemFont(ofSize: self.lbTitleName!.font.pointSize), range: NSRange(location: 0, length: name.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: name.count))
            
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.thick.rawValue, range: NSRange(location: 0, length: name.count))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: name.count))
        }
        self.lbTitleName?.attributedText = attributedString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
