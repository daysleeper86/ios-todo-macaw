//
//  TeamAgendaItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import SDWebImage
import AnimatableReload
import SkeletonView
import Material

protocol TeamAgendaItemViewCellDelegate : NSObject {
    func teamAgendaItemViewCellTaskItemSelected(_ teamAgendaItemViewCell: TeamAgendaItemViewCell, didSelectRowAt: Int, task : Task)
    func teamAgendaItemViewCellEdited(_ teamAgendaItemViewCell: TeamAgendaItemViewCell)
    func teamAgendaItemViewCellTaskItemFinishChanged(_ teamAgendaItemViewCell: TeamAgendaItemViewCell, didSelectRowAt: Int, task : Task, taskRowIndex: Int)
}

class TeamAgendaItemViewCell: UICollectionViewCell,UITableViewDataSource,UITableViewDelegate {
    //Views
    var backgroundViewCell : UIView!
    var headerView : UIView!
    var imvAvatar : FAShimmerImageView!
    var lbTitleName : FAShimmerLabelView!
    var lbTitleSubName : FAShimmerLabelView!
    var tbTalks : UITableView!
    var btnEditPlanning : UIButton!
    var taskFooterProgressView : TaskProgressView!
    //Data
    var itemWidth : CGFloat = 0
    var itemHeight : CGFloat = 0
    let taskTeamAgendaCellIdentifier : String = "taskTeamAgendaCellIdentifier"
    let noPlanTeamAgendaCellIdentifier : String = "noPlanTeamAgendaCellIdentifier"
    let wipTeamAgendaCellIdentifier : String = "wipTeamAgendaCellIdentifier"
    var agenda = AgendaService.createEmptyAgenda()
    var wip : Bool = false
    var wipShowOther : Bool = false
    var isCanFinish : Bool = false
    var itemIndex : Int = 0
    //
    static let pLeftHeaderView : CGFloat = AppDevice.ScreenComponent.ItemPadding
    //Event
    weak var delegate: TeamAgendaItemViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect,itemWidth: CGFloat,itemHeight: CGFloat,taskInteractionEnabled: Bool) {
        self.init(frame: frame)
        //
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(itemWidth: CGFloat,itemHeight: CGFloat,taskInteractionEnabled: Bool){
        //
        if(self.itemWidth != itemWidth){
            self.itemWidth = itemWidth
            self.itemHeight = itemHeight
            // Background View
            let xBackgroundView : CGFloat = 2
            let yBackgroundView : CGFloat = 2
            let wBackgroundView : CGFloat = self.itemWidth-(xBackgroundView*2)
            let hBackgroundView : CGFloat = self.itemHeight-(yBackgroundView*2)
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell.backgroundColor = AppColor.NormalColors.WHITE_COLOR
            backgroundViewCell.layer.cornerRadius = 4
            backgroundViewCell.layer.shadowColor = UIColor(red: 0.09, green: 0.17, blue: 0.3, alpha: 0.1).cgColor
            backgroundViewCell.layer.shadowOpacity = 1
            backgroundViewCell.layer.shadowOffset = CGSize.zero
            backgroundViewCell.layer.shadowRadius = 2
            self.addSubview(backgroundViewCell)
            
            let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
            let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
            
            let xHeaderView : CGFloat = TeamAgendaItemViewCell.pLeftHeaderView
            let yHeaderView : CGFloat = 0
            let wHeaderView : CGFloat = wBackgroundView - (xHeaderView * 2)
            let hHeaderView : CGFloat = wAvatar + (pAvatar * 2)
            headerView = UIView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
            backgroundViewCell.addSubview(headerView)
            
            //Action Button
            let pEditPlanning : CGFloat = 8
            let wEditPlanning : CGFloat = 36
            let hEditPlanning = wEditPlanning
            let xEditPlanning = wBackgroundView - wEditPlanning - pEditPlanning
            let yEditPlanning : CGFloat = pEditPlanning
            
            btnEditPlanning = IconButton.init(type: .custom)
            btnEditPlanning.frame = CGRect(x: xEditPlanning, y: yEditPlanning, width: wEditPlanning, height: hEditPlanning)
            btnEditPlanning.setImage(UIImage(named: "ic_edit_planning"), for: .normal)
            btnEditPlanning.isHidden = true
            btnEditPlanning.addTarget(self, action: #selector(editAction), for: .touchUpInside)
            backgroundViewCell.addSubview(btnEditPlanning)
            
            // Avatar
            let hAvatar : CGFloat = wAvatar
            let xAvatar : CGFloat = 0
            let yAvatar : CGFloat = (hHeaderView-hAvatar)/2
            
            imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
            imvAvatar.layer.cornerRadius = hAvatar/2
            imvAvatar.layer.masksToBounds = true
            imvAvatar.image = LocalImage.UserPlaceHolder
            imvAvatar.sd_imageIndicator = SDWebImageActivityIndicator.white
            headerView.addSubview(imvAvatar)
            
            // Title name
            let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
            let xTitleName : CGFloat = xAvatar + wAvatar + pAvatar
            let yTitleName : CGFloat = (hHeaderView-(hTitleName*2))/2
            let wTitleName : CGFloat = (btnEditPlanning.isHidden ? headerView.frame.size.width : (btnEditPlanning.frame.origin.x - TeamAgendaItemViewCell.pLeftHeaderView)) - xTitleName
            
            lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.textAlignment = .left
            lbTitleName.numberOfLines = 0
            lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
            lbTitleName.isSkeletonable = true
            headerView.addSubview(lbTitleName)
            
            let yTitleSubName : CGFloat = yTitleName + hTitleName
            
            lbTitleSubName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleSubName, width: wTitleName, height: hTitleName))
            lbTitleSubName.textAlignment = .left
            lbTitleSubName.numberOfLines = 0
            lbTitleSubName.textColor = AppColor.NormalColors.LIGHT_GRAY_COLOR
            lbTitleSubName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 12)
            lbTitleSubName.isSkeletonable = true
            headerView.addSubview(lbTitleSubName)
            
            //Footer
            let wFooter : CGFloat = backgroundViewCell.frame.size.width
            let hFootter : CGFloat = TaskProgressView.hHeader
            let yFootter : CGFloat = backgroundViewCell.frame.size.height - hFootter
            taskFooterProgressView = TaskProgressView(frame: CGRect(x: 0, y: yFootter, width: wFooter, height: hFootter))
            taskFooterProgressView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            taskFooterProgressView.isHidden = true
            backgroundViewCell.addSubview(taskFooterProgressView)
            
            let pTable : CGFloat = 0
            let xTable : CGFloat = pTable
            let yTable : CGFloat = headerView.frame.origin.y + headerView.frame.size.height
            let wTable : CGFloat = backgroundViewCell.frame.size.width - pTable - xTable
            let hTable : CGFloat = backgroundViewCell.frame.size.height - hFootter - yTable
            
            tbTalks = UITableView(frame: CGRect(x: xTable, y: yTable, width: wTable, height: hTable))
            tbTalks.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            tbTalks.separatorColor = AppColor.NormalColors.CLEAR_COLOR
            tbTalks.dataSource = self
            tbTalks.delegate = self
            tbTalks.isUserInteractionEnabled = taskInteractionEnabled
            tbTalks.register(TeamAgendaTaskItemViewCell.self, forCellReuseIdentifier: taskTeamAgendaCellIdentifier)
            tbTalks.register(TeamAgendaNoPlanItemViewCell.self, forCellReuseIdentifier: noPlanTeamAgendaCellIdentifier)
            tbTalks.register(TeamAgendaWIPItemViewCell.self, forCellReuseIdentifier: wipTeamAgendaCellIdentifier)
            tbTalks.contentSize.width = 100
            backgroundViewCell.addSubview(tbTalks)
            
            
        }
    }
    
    func resetValue_Cell(){
        imvAvatar.image = nil
        lbTitleName.text = ""
        lbTitleSubName.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        imvAvatar.startShimmering()
        lbTitleName.showAnimatedSkeleton(usingColor: AppColor.MicsColors.SHIMMER_COLOR, animation: nil)
        lbTitleSubName.showAnimatedSkeleton(usingColor: AppColor.MicsColors.SHIMMER_COLOR, animation: nil)
    }
    
    func stopShimmering_Cell(){
        let isShimmering = imvAvatar.isShimmering()
        if(isShimmering == true){
            //
            imvAvatar.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            lbTitleName.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            lbTitleSubName.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            
            imvAvatar.stopShimmering()
            lbTitleName.stopSkeletonAnimation()
            lbTitleSubName.stopSkeletonAnimation()
            lbTitleName.hideSkeleton()
            lbTitleSubName.hideSkeleton()
        }
    }
    
    func setValueForCell(agenda: Agenda,isEdited: Bool, isCanFinish: Bool){
        self.agenda = agenda
        self.isCanFinish = isCanFinish
        //
        if(self.agenda.owned.dataType == .normal){
            stopShimmering_Cell()
            //
            stateForAvatar(self.agenda.owned)
            //
            lbTitleName.text = self.agenda.owned.getUserName()
        }
        else{
            startShimmering_Cell()
        }
        //
        if(self.agenda.owned.dataType == .normal){
            var strTitleTask = NSLocalizedString("task", comment:"")
            if(self.agenda.tasks.count > 1){
                strTitleTask = strTitleTask + "s"
            }
            lbTitleSubName.text = String(self.agenda.tasks.count) + " " + strTitleTask
        }
        //
        self.wip = self.agenda.checkWIPTask()
        //
        stateForProgressFooter()
//        AnimatableReload.reload(tableView: tbTalks!, animationDirection: "up")
        tbTalks.reloadData()
        //
        stateForEditButton(isEdited)
    }
    
    
    func stateForAvatar(_ user : User){
        //Cancel current request
        imvAvatar.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID.count > 0){
            imvAvatar.sd_cancelCurrentImageLoad()
            //
            let strURL = Util.parseURLAvatarImageFireStore(strUserID)
            imvAvatar.sd_setImage(with: URL(string: strURL), placeholderImage: nil, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongSelf = self else { return }
                if let imageValue = image{
                    //Current userId
                    let strCurrentUserID = strongSelf.agenda.owned.userId
                    if(strCurrentUserID == strUserID){
                        if(imageValue.size.width > 0){
                            strongSelf.imvAvatar.backgroundColor = .clear
                            strongSelf.imvAvatar.removeLabelTextIfNeed()
                        }
                        else{
                            let strUserName = strongSelf.agenda.owned.getUserName()
                            strongSelf.imvAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                }
                else{
                    //Current userId
                    let strCurrentUserID = strongSelf.agenda.owned.userId
                    if(strCurrentUserID == strUserID){
                        let strUserName = strongSelf.agenda.owned.getUserName()
                        strongSelf.imvAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
            })
        }
        else{
            let strUserName = user.name
            imvAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
        }
    }
    
    
    func stateForEditButton(_ isEdit : Bool){
        btnEditPlanning.isHidden = !isEdit
        //
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = imvAvatar.frame.origin.x + imvAvatar.frame.size.width + pAvatar
        let wTitleName : CGFloat = (btnEditPlanning.isHidden ? headerView.frame.size.width : (btnEditPlanning.frame.origin.x - TeamAgendaItemViewCell.pLeftHeaderView)) - xTitleName
        //
        lbTitleName.frame.size.width = wTitleName
        lbTitleSubName.frame.size.width = wTitleName
    }
    
    func stateForProgressFooter(){
        var hFootter : CGFloat = 0
        if(self.wip == true){
            taskFooterProgressView.isHidden = false
            taskFooterProgressView.setValueForView(completed: TaskService.countNumComplete(arrayTask: self.agenda.tasks), numTask: self.agenda.tasks.count)
            //
            hFootter = taskFooterProgressView.frame.size.height
        }
        else{
            taskFooterProgressView.isHidden = true
        }
        let yTable : CGFloat = headerView.frame.origin.y + headerView.frame.size.height
        let hTable : CGFloat = backgroundViewCell.frame.size.height - hFootter - yTable
        //
        if(tbTalks.frame.size.height != hTable){
            tbTalks.frame.size.height = hTable
        }
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat{
        if(self.agenda.tasks.count == 0 || self.wip == true){
            return 0
        }
        //
        return TaskProgressView.hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(self.agenda.tasks.count == 0 || self.wip == true){
            return nil
        }
        
        let wHeader : CGFloat = tableView.frame.size.width
        let hHeader : CGFloat = TaskProgressView.hHeader
        
        //Header
        let taskProgressView = TaskProgressView(frame: CGRect(x: 0, y: 0, width: wHeader, height: hHeader))
        taskProgressView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //
        taskProgressView.setValueForView(completed: TaskService.countNumComplete(arrayTask: self.agenda.tasks), numTask: self.agenda.tasks.count)
        //
        return taskProgressView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numRow : Int = 0
        if(self.agenda.tasks.count == 0){
            numRow = 1
        }
        else{
            //
            if(self.wip == true){
                if(self.wipShowOther == false){
                    numRow = 1
                }
                else{
                    numRow = self.agenda.tasks.count
                }
            }
            else{
                numRow = self.agenda.tasks.count
            }
        }
        return numRow
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var heightRow : CGFloat = 0
        if(self.agenda.tasks.count == 0){
            heightRow = tableView.frame.size.height
        }
        else{
            if(self.wip == true){
                if(self.wipShowOther == false){
                    heightRow = tableView.frame.size.height
                }
                else{
                    let task = self.agenda.tasks[indexPath.row]
                    //Only display first WIP
                    if(indexPath.row == 0 && task.wip == true){
                        heightRow = AppDevice.TableViewRowSize.TaskWIPHeight
                    }
                    else{
                        heightRow = AppDevice.TableViewRowSize.TaskHeight
                    }
                }
            }
            else{
                heightRow = AppDevice.TableViewRowSize.TaskHeight
            }
        }
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.agenda.tasks.count == 0){
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  noPlanTeamAgendaCellIdentifier, for: indexPath) as? TeamAgendaNoPlanItemViewCell else {
                return TeamAgendaNoPlanItemViewCell()
            }
            cell.initView(itemWidth: backgroundViewCell.frame.size.width)
            //
            cell.selectionStyle = .none
            cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            //
            cell.setValueForCell(agenda: self.agenda,height: tableView.frame.size.height)
            //
            return cell
            
        }
        else{
            let task = self.agenda.tasks[indexPath.row]
            //Only display first WIP
            if(indexPath.row == 0 && task.wip == true){
                guard let cell = tableView.dequeueReusableCell(withIdentifier:  wipTeamAgendaCellIdentifier, for: indexPath) as? TeamAgendaWIPItemViewCell else {
                    return TeamAgendaWIPItemViewCell()
                }
                cell.initView(itemWidth: backgroundViewCell.frame.size.width)
                //
                cell.selectionStyle = .none
                cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                //
                let task = self.agenda.getWIPTask()
                cell.setValueForCell(task: task!, height: tableView.frame.size.height)
                //
                return cell
            }
            else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier:  taskTeamAgendaCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
                    return TeamAgendaTaskItemViewCell()
                }
                 
                let itemWidth = backgroundViewCell.frame.size.width
                let contentPadding = AppDevice.ScreenComponent.ItemPadding
                
                cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                //
                cell.selectionStyle = .none
                cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                //
                cell.setValueForCell(task: task,isCanFinished: self.isCanFinish)
                //
                cell.btnCheckbox?.tag = indexPath.row
                cell.btnCheckbox?.addTarget(self, action: #selector(checkboxAction(sender:)), for: .touchUpInside)
                //
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if(self.agenda.tasks.count > 0){
            let task = self.agenda.tasks[indexPath.row]
            if(task.valid() == true){
                //
                self.delegate?.teamAgendaItemViewCellTaskItemSelected(self, didSelectRowAt: indexPath.row, task: task)
            }
        }
    }
    
    @objc func editAction(){
        self.delegate?.teamAgendaItemViewCellEdited(self)
    }
    
    @objc func checkboxAction(sender: UIButton){
        let index = sender.tag
        let task = self.agenda.tasks[index]
        if(task.valid() == true){
            //
            self.delegate?.teamAgendaItemViewCellTaskItemFinishChanged(self, didSelectRowAt: self.itemIndex, task: task, taskRowIndex: index)
        }
    }
    
}
