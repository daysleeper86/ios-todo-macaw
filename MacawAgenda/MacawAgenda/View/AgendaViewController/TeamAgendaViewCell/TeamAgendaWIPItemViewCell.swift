//
//  TeamAgendaWIPItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TeamAgendaWIPItemViewCell: UITableViewCell {
    
    //Views
    var backgroundViewCell : UIView!
    var lbTitleName : UILabel!
    var lbTitleSubName : UILabel!
    //Data
    var task : Task?
    var itemWidth : CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    func initView(itemWidth : CGFloat){
        if(self.itemWidth != itemWidth){
            self.itemWidth = itemWidth
            // Background View
            let xBackgroundView : CGFloat = 0
            let yBackgroundView : CGFloat = 0
            let wBackgroundView : CGFloat = itemWidth
            let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            self.addSubview(backgroundViewCell)
            
            
            // Title name
            let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let xTitleName : CGFloat = pTitleName
            let yTitleName : CGFloat = 0
            let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
            let hTitleName : CGFloat = AppDevice.SubViewFrame.LargeTitleHeight * 2
            
            lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.textAlignment = .left
            lbTitleName.numberOfLines = 0
            lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
            backgroundViewCell.addSubview(lbTitleName)
            
            // SubTitle name
            let pSubTitleName : CGFloat = 4
            let xSubTitleName : CGFloat = xTitleName
            let ySubTitleName : CGFloat = yTitleName + hTitleName + pSubTitleName
            let wSubTitleName : CGFloat = wTitleName
            let hSubTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
            
            lbTitleSubName = UILabel(frame: CGRect(x: xSubTitleName, y: ySubTitleName, width: wSubTitleName, height: hSubTitleName))
            lbTitleSubName.textAlignment = .left
            lbTitleSubName.numberOfLines = 0
            lbTitleSubName.textColor = AppColor.NormalColors.BLUE_COLOR
            lbTitleSubName.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
            lbTitleSubName.text = NSLocalizedString("work_in_progress", comment: "").lowercased()
            backgroundViewCell.addSubview(lbTitleSubName)
        }
    }
    
    func setValueForCell(task: Task, height: CGFloat){
        self.task = task
        //
        let name = "#" + task.taskNumber + " - " + task.name
        stateForTitleName(name)
        //
        if(backgroundViewCell.frame.size.height != height){
            backgroundViewCell.frame.size.height = height
        }
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell.backgroundColor = color
    }
    
    func stateForTitleName(_ name : String){
        self.lbTitleName.text = name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
