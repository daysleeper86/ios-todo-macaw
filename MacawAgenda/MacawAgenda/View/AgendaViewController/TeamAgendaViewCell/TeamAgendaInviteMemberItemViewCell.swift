//
//  TeamAgendaNoPlanItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material
import DTGradientButton

class TeamAgendaInviteMemberItemViewCell: UICollectionViewCell {
    
    //Views
    var backgroundViewCell : UIView?
    var bgScaleView : UIView?
    var imvAvatar : UIImageView?
    var lbTitleName : UILabel?
    var lbTitleDescription : UILabel?
    var btnInvite : RaisedButton?
    //Data
    var itemWidth : CGFloat = 0
    var itemHeight : CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initView(itemWidth : CGFloat,itemHeight : CGFloat, isAvatar : Bool){
        if(backgroundViewCell == nil){
            self.itemWidth = itemWidth
            self.itemHeight = itemHeight
            // Background View
            let xBackgroundView : CGFloat = 2
            let yBackgroundView : CGFloat = 2
            let wBackgroundView : CGFloat = self.itemWidth-(xBackgroundView*2)
            let hBackgroundView : CGFloat = self.itemHeight-(yBackgroundView*2)
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
            backgroundViewCell?.layer.cornerRadius = 4
            backgroundViewCell?.layer.shadowColor = UIColor(red: 0.09, green: 0.17, blue: 0.3, alpha: 0.1).cgColor
            backgroundViewCell?.layer.shadowOpacity = 1
            backgroundViewCell?.layer.shadowOffset = CGSize.zero
            backgroundViewCell?.layer.shadowRadius = 2
            self.addSubview(backgroundViewCell!)
            
            bgScaleView = UIView(frame: .zero)
            bgScaleView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            backgroundViewCell?.addSubview(bgScaleView!)
            
            var wScaleView : CGFloat = 0
            var yScreen : CGFloat = 0
            if(isAvatar == true){
                yScreen = 0
                wScaleView = Constant.CollectionItemAgenda.Width
                //Avatar
                let yAvatar : CGFloat = 0
                let wAvatar : CGFloat = 180
                let hAvatar : CGFloat = 136
                let xAvatar : CGFloat = (wScaleView-wAvatar)/2
                let pBottomAvatar : CGFloat = 24
                
                imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
                imvAvatar?.image = UIImage(named: "bg_invite_email")
                bgScaleView?.addSubview(imvAvatar!)
                
                yScreen = imvAvatar!.frame.origin.y + imvAvatar!.frame.size.height + pBottomAvatar
                
            }
            else{
                yScreen = AppDevice.ScreenComponent.ItemPadding
                wScaleView = wBackgroundView
            }
            
            //Title
            let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let xTitleName : CGFloat = pTitleName
            let yTitleName : CGFloat = yScreen
            let wTitleName : CGFloat = wScaleView - (xTitleName * 2)
            let hTitleName : CGFloat = 20
            let pBottomTitleName : CGFloat = 5
            
            lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName?.textAlignment = .left
            lbTitleName?.numberOfLines = 0
            lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
            lbTitleName?.text = NSLocalizedString("invite_member", comment: "")
            bgScaleView?.addSubview(lbTitleName!)
            
            yScreen += hTitleName
            yScreen += pBottomTitleName
            
            // Title name
            let pTitleDescription : CGFloat = pTitleName
            let xTitleDescription : CGFloat = pTitleDescription
            let yTitleDescription : CGFloat = yScreen
            let wTitleDescription : CGFloat = wScaleView - (xTitleDescription * 2)
            let hTitleDescription : CGFloat = 56
            let pBottomTitleDescription : CGFloat = 24
            
            lbTitleDescription = UILabel(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
            lbTitleDescription?.textAlignment = .left
            lbTitleDescription?.numberOfLines = 0
            lbTitleDescription?.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
            lbTitleDescription?.text = NSLocalizedString("invite_people_to_collaborate", comment: "")
            bgScaleView?.addSubview(lbTitleDescription!)
            
            yScreen += hTitleDescription
            yScreen += pBottomTitleDescription
            
            let pButtonInvite : CGFloat = pTitleDescription
            let xButtonInvite : CGFloat = pButtonInvite
            let yButtonInvite : CGFloat = yScreen
            let wButtonInvite : CGFloat = 80
            let hButtonInvite : CGFloat = 36
            
            btnInvite = RaisedButton.init(type: .custom)
            btnInvite?.pulseColor = .white
            btnInvite?.frame = CGRect(x: xButtonInvite, y: yButtonInvite, width: wButtonInvite, height: hButtonInvite)
            btnInvite?.titleLabel?.textAlignment = .center
            btnInvite?.layer.cornerRadius = 4.0
            btnInvite?.layer.masksToBounds = true
            let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
            btnInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
            btnInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
            btnInvite?.setTitle(NSLocalizedString("invite", comment:"").uppercased(), for: .normal)
            btnInvite?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
            btnInvite?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
            btnInvite?.titleLabel?.textAlignment = .right
            bgScaleView?.addSubview(btnInvite!)
            
            if(isAvatar == true){
                let xScaleView : CGFloat = (self.itemWidth-wScaleView)/2
                let hScaleView : CGFloat = btnInvite!.frame.origin.y + btnInvite!.frame.size.height
                let yScaleView : CGFloat = (self.itemHeight-hScaleView)/2
                bgScaleView?.frame = CGRect(x: xScaleView, y: yScaleView, width: wScaleView, height: hScaleView)
            }
            else{
                bgScaleView?.frame = CGRect(x: 0, y: 0, width: wBackgroundView, height: hBackgroundView)
            }
        }
    }
}
