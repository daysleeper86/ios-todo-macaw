//
//  TeamAgendaViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Charts
import Hero
import Moya
import AnimatedCollectionViewLayout
import NotificationBannerSwift
import Material
import DTGradientButton

class CustomBannerColors: BannerColorsProtocol {
    
    internal func color(for style: BannerStyle) -> UIColor {
        switch style {
        case .danger:   return UIColor(red:0.90, green:0.31, blue:0.26, alpha:1.00)
        case .info:     return UIColor(red:0.23, green:0.60, blue:0.85, alpha:1.00)
        case .none:     return UIColor.black
        case .success:  return UIColor(red:0.22, green:0.80, blue:0.46, alpha:1.00)
        case .warning:  return UIColor(red:1.00, green:0.66, blue:0.16, alpha:1.00)
        }
    }
    
}

class TeamAgendaViewController: BaseViewController {
    
    //Views
    var navigationView : CustomNavigationView?
    var contentScrollView : UIScrollView?
    var keyboardBounds : CGRect?
//    var imvAvatar : UIImageView?
//    var btnNotification : IconButton?
//    var lbNumNotification : UILabel?
    var btnDiscussion : IconButton?
    var lbTitleDate : UILabel?
    var dateSwitch : Suitchi?
    var tfEmailID : UITextField?
    var lbErrorEmailAdress : UILabel?
    var tbTeamAgenda : UICollectionView?
    private var activityViewNavigation : DTOverlayController?
    //View controller
    var detailView : TeamAgendaDetailViewController?
    //Internet connection
    let reachabilityInternetConnection = Reachability()!

    //Chart
    var chartView: LineChartView?
    lazy var chartSet1 : LineChartDataSet = {
        let charSet = LineChartDataSet(entries: nil, label: NSLocalizedString("finished_tasks", comment: ""))
        charSet.drawIconsEnabled = false
        charSet.setColor(UIColor(red: CGFloat(0.0)/255, green: CGFloat(128.0)/255, blue: CGFloat(255.0)/255, alpha: 1.0))
        charSet.setCircleColor(UIColor(red: CGFloat(0.0)/255, green: CGFloat(128.0)/255, blue: CGFloat(255.0)/255, alpha: 1.0))
        charSet.lineWidth = 1
        charSet.circleRadius = 3
        charSet.drawCircleHoleEnabled = true
        charSet.valueFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 10) ?? .systemFont(ofSize: 10)
        charSet.formLineWidth = 2
        charSet.valueFormatter = ChartXValueFormatter()
        charSet.formSize = 35
        //Fill
        let gradientColors = [UIColor(red: CGFloat(255.0)/255, green: CGFloat(255.0)/255, blue: CGFloat(255.0)/255, alpha: 0.6).cgColor,
                              UIColor(red: CGFloat(0.0)/255, green: CGFloat(128.0)/255, blue: CGFloat(255.0)/255, alpha: 1.0).cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        charSet.fillAlpha = 1
        charSet.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        charSet.drawFilledEnabled = true
        return charSet
    }()
    lazy var chartSet2 : LineChartDataSet = {
        let charSet = LineChartDataSet(entries: nil, label: NSLocalizedString("guideline", comment: ""))
        charSet.drawIconsEnabled = false
        charSet.lineDashLengths = [8, 2.5]
        charSet.setColor(UIColor(red: CGFloat(160.0)/255, green: CGFloat(184.0)/255, blue: CGFloat(76.0)/255, alpha: 1.0))
        charSet.circleRadius = 0
        charSet.drawCircleHoleEnabled = false
        charSet.lineWidth = 1
        charSet.formLineDashLengths = [5, 2.5]
        charSet.formLineWidth = 2
        charSet.valueFormatter = ChartXValueFormatter()
        charSet.formSize = 35
        return charSet
    }()
    //Data
    static let teamAgendaCellIdentifier : String = "teamAgendaCellIdentifier"
    static let inviteMemberTeamAgendaCellIdentifier : String = "inviteMemberTeamAgendaCellIdentifier"
//    var yScreen_Scroll : CGFloat = 0
    var duration : CGFloat = 0
    let maxinumRow : Int = 9
    var firstLoading : Bool = false
//    var needShakeNotification : Bool = false
//    var pauseShakeNotification : Bool = false
    var arrayTeamAgenda : [Agenda] = []
    
    //ViewModel
    var orangnizationViewModel : OrangnizationViewModel?
    var projectViewModel : ProjectViewModel?
    var agendaViewModel : AgendaViewModel?
    var taskViewModel : TaskViewModel?
    var notificationViewModel : NotificationViewModel?
    var subjectProjectId : BehaviorSubject<String>?
    var subjectUserId : BehaviorSubject<String>?
    var dateAgenda = AppSettings.sharedSingleton.agenda_today?.date ?? Date()
    var subjectDate = BehaviorSubject<Date>(value: AppSettings.sharedSingleton.agenda_today?.date ?? Date())
    var subjectInviteMember = BehaviorSubject<Bool>(value: false)
    
    lazy var btnFocusMode : FABButton = {
        let pButtonFocusMode : CGFloat = 24
        let wButtonFocusMode : CGFloat = 72
        let yButtonFocusMode : CGFloat = AppDevice.ScreenComponent.Height - AppDevice.ScreenComponent.TabbarHeight - wButtonFocusMode - pButtonFocusMode
        let xButtonFocusMode : CGFloat = AppDevice.ScreenComponent.Width - wButtonFocusMode - pButtonFocusMode
        //Focus mode button
        let btnFocusMode = FABButton.init(type: .custom)
        btnFocusMode.accessibilityIdentifier = AccessiblityId.BTN_FOCUS_MODE
        btnFocusMode.frame = CGRect(x: xButtonFocusMode, y: yButtonFocusMode, width: wButtonFocusMode, height: wButtonFocusMode)
        btnFocusMode.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        btnFocusMode.titleLabel?.textAlignment = .center
        btnFocusMode.titleLabel?.numberOfLines = 2
        btnFocusMode.setTitle(NSLocalizedString("focus_mode", comment: ""), for: .normal)
        btnFocusMode.addTarget(self, action: #selector(focusModeAction), for: .touchUpInside)
        btnFocusMode.pulseColor = .white
        //
        let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
        btnFocusMode.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
        btnFocusMode.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
        
        btnFocusMode.layer.cornerRadius = wButtonFocusMode/2
        btnFocusMode.layer.masksToBounds = true
        //
        btnFocusMode.layer.shadowColor = UIColor(red: 0.09, green: 0.17, blue: 0.3, alpha: 0.4).cgColor
        btnFocusMode.layer.shadowOffset = CGSize(width: 0.0, height: 8.0)
        btnFocusMode.layer.shadowRadius = 16;
        btnFocusMode.layer.shadowOpacity = 1;
        return btnFocusMode
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        addNotification()
        // Do any additional setup after loading the view.
        initView()
        //
        setDefaultValue()
        //
        bindToViewModel()
        //Network Connection
        observeInternetConnection()
    }
    
    
    func trackOpenCollaborate(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        Tracker.trackOpenCollaborate(projectId: projectId, userId: userId)
    }
    
    func observeInternetConnection(){
        reachabilityInternetConnection.whenReachable = {reachability in
            DispatchQueue.main.async {
                AppDelegate().sharedInstance().alertNoInternetConntect(reachable: true)
            }
        }
        reachabilityInternetConnection.whenUnreachable = { _ in
            DispatchQueue.main.async {
                AppDelegate().sharedInstance().alertNoInternetConntect(reachable: false)
            }
        }
        do {
            try reachabilityInternetConnection.startNotifier()
            if(reachabilityInternetConnection.isReachable == true){
                DispatchQueue.main.async {
                    AppDelegate().sharedInstance().alertNoInternetConntect(reachable: true)
                }
            }
        } catch {
        }
    }
    
    func checkCurrentConnection(){
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        appDelegate.alertNoInternetConntect(reachable: reachabilityInternetConnection.isReachable)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //
        if(firstLoading == false){
            firstLoading = true
            loadUnreadNotification()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //
        trackOpenCollaborate()
        //
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
//        appDelegate.
//        appDelegate.alertNotificationInApp()
        //
        if(appDelegate.isGoToPush == true){
            if let remoteNotificationData = appDelegate.remoteNotificationData{
                let dataType = Util.getPushNotificationType(remoteNotificationData: remoteNotificationData)
                if(dataType != .emptyNotification){
                    if(dataType == .newCommentAgenda){
                        if let agendaDate = remoteNotificationData[SettingString.Date] as? Date{
                            self.openDiscussion(date: agendaDate)
                        }
                    }
                    else if(dataType == .newCommentTask){
                        if let roomId = remoteNotificationData[SettingString.RoomId] as? String{
                            AppDelegate().sharedInstance().selectBacklogTabbar()
                        }
                    }
                    else if(dataType == .planTask){
                        var kIndex = -1
                        //
                        if let agendaId = remoteNotificationData[SettingString.AgendaId] as? String{
                            if(agendaId.count > 0){
                                for i in 0..<self.arrayTeamAgenda.count{
                                    let agenda = self.arrayTeamAgenda[i]
                                    if(agenda.agendaId == agendaId){
                                        kIndex = i
                                        break
                                    }
                                }
                            }
                        }
                        if(kIndex == -1){
                            if let userId = remoteNotificationData[SettingString.UserId] as? String{
                                for i in 0..<self.arrayTeamAgenda.count{
                                    let agenda = self.arrayTeamAgenda[i]
                                    if(agenda.owned.userId == userId){
                                        kIndex = i
                                        break
                                    }
                                }
                            }
                        }
                        
                        if(kIndex >= 0 && kIndex < self.arrayTeamAgenda.count){
                            DispatchQueue.main.async {[weak self] in
                                if let strongCollection = self?.tbTeamAgenda{
                                    let indexPath = IndexPath(row: kIndex, section: 0)
                                    strongCollection.selectItem(at: indexPath, animated: true, scrollPosition: .left)
                                    self?.collectionView(strongCollection, didSelectItemAt: indexPath)
                                }
                            }
                            //
                            AppDelegate().sharedInstance().clearPushNotificationData()
                        }
                    }
                    
                    else if(dataType == .assignTask){
                        if let roomId = remoteNotificationData[SettingString.RoomId] as? String{
                            AppDelegate().sharedInstance().selectBacklogTabbar()
                        }
                    }
                }
            }
            
        }
        //Start shake notification when view become active
//        pauseShakeNotification = false
//        if(needShakeNotification == true){
//            shakeViews(isShake: true)
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //Stop shake notification when view become active
        if(activityViewNavigation != nil){
            activityViewNavigation = nil
        }
        //
        
//        pauseShakeNotification = true
//        if(needShakeNotification == true){
//            shakeViews(isShake: false)
//        }
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Action Button
//        let pButtonAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        let wButtonAvatar : CGFloat = hContentHeaderView
//        let hButtonAvatar = wButtonAvatar
//        let xButtonAvatar = navigationView!.frame.size.width - wButtonAvatar - pButtonAvatar
//        let yButtonAvatar : CGFloat = yContentHeaderView
//
//        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
//        let hAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconHeight
//        let xAvatar = xButtonAvatar + (wButtonAvatar - wAvatar) / 2
//        let yAvatar = yButtonAvatar + (hButtonAvatar - hAvatar) / 2
//
//        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
//        imvAvatar?.layer.cornerRadius = hAvatar/2
//        imvAvatar?.layer.masksToBounds = true
//        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
//        imvAvatar?.isAccessibilityElement = true
//        imvAvatar?.accessibilityIdentifier = AccessiblityId.IMG_PROFILE
//        navigationView?.addSubview(imvAvatar!)
//
//
//        let btnAvatar = IconButton.init(type: .custom)
//        btnAvatar.accessibilityIdentifier = AccessiblityId.BTN_PROFILE
//        btnAvatar.frame = CGRect(x: xButtonAvatar, y: yButtonAvatar, width: wButtonAvatar, height: hButtonAvatar)
//        btnAvatar.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
//        btnAvatar.titleLabel?.textAlignment = .right
//        btnAvatar.addTarget(self, action: #selector(profileAction), for: .touchUpInside)
//        navigationView?.addSubview(btnAvatar)
//
//        //Action Button
//        let yBacklog : CGFloat = yContentHeaderView
//        let wButton : CGFloat = hContentHeaderView
//        let hButton : CGFloat = wButton
//        let xBacklog : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
//
//        let btnBacklog = IconButton.init(type: .custom)
//        btnBacklog.accessibilityIdentifier = AccessiblityId.BTN_BACKLOG
//        btnBacklog.frame = CGRect(x: xBacklog, y: yBacklog, width: wButton, height: hButton)
//        btnBacklog.setImage(UIImage(named: "ic_menu_backlog"), for: .normal)
//        btnBacklog.addTarget(self, action: #selector(backlogAction), for: .touchUpInside)
//        navigationView?.addSubview(btnBacklog)
//
//        let xNotification : CGFloat = xButtonAvatar - wButton
//        let yNotification : CGFloat = yContentHeaderView
//
//        btnNotification = IconButton.init(type: .custom)
//        btnNotification?.accessibilityIdentifier = AccessiblityId.BTN_NOTIFICATION
//        btnNotification?.frame = CGRect(x: xNotification, y: yNotification, width: wButton, height: hButton)
//        btnNotification?.setImage(UIImage(named: "ic_menu_notification"), for: .normal)
//        btnNotification?.addTarget(self, action: #selector(notificationAction), for: .touchUpInside)
//        navigationView?.addSubview(btnNotification!)
//
//        let wNumNotification : CGFloat = 22
//        let hNumNotification : CGFloat = wNumNotification
//        let xNumNotification : CGFloat = xNotification
//        let yNumNotification : CGFloat = yNotification
//
//        lbNumNotification = UILabel(frame: CGRect(x: xNumNotification, y: yNumNotification, width: wNumNotification, height: hNumNotification))
//        lbNumNotification?.accessibilityIdentifier = AccessiblityId.LB_NOTIFICATION
//        lbNumNotification?.backgroundColor = AppColor.NormalColors.LIGHT_RED_COLOR
//        lbNumNotification?.textAlignment = .center
//        lbNumNotification?.textColor = AppColor.NormalColors.WHITE_COLOR
//        lbNumNotification?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 12)
//        lbNumNotification?.text = NSLocalizedString("8", comment: "")
//        lbNumNotification?.layer.cornerRadius = hNumNotification/2
//        lbNumNotification?.layer.masksToBounds = true
//        navigationView?.addSubview(lbNumNotification!)
        //
//        let xBacklog : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        
        let pDiscussion : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wDiscussion : CGFloat = hContentHeaderView
        let hDiscussion : CGFloat = wDiscussion
        let xDiscussion = navigationView!.frame.size.width - wDiscussion - pDiscussion
        let yDiscussion : CGFloat = yContentHeaderView
        
        btnDiscussion = IconButton.init(type: .custom)
        btnDiscussion?.accessibilityIdentifier = AccessiblityId.BTN_DISCUSSION
        btnDiscussion?.frame = CGRect(x: xDiscussion, y: yDiscussion, width: wDiscussion, height: hDiscussion)
        btnDiscussion?.setImage(UIImage(named: "ic_menu_discussion"), for: .normal)
        btnDiscussion?.addTarget(self, action: #selector(discussionAction), for: .touchUpInside)
        navigationView?.addSubview(btnDiscussion!)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = xDiscussion - xTitleHeader - pTitleHeader
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .left
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 32)
        lbTitleHeader.text = NSLocalizedString("collaborate", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        navigationView?.showHideTitleNavigation(isShow: true)
        navigationView?.showHideNavigationLine(isHidden: true)
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let yScrollView : CGFloat = yHeader
        let hScrollView : CGFloat = AppDevice.ScreenComponent.Height - AppDevice.ScreenComponent.TabbarHeight - yScrollView
        contentScrollView = UIScrollView(frame: CGRect(x: 0, y: yScrollView, width: AppDevice.ScreenComponent.Width, height: hScrollView))
        contentScrollView?.accessibilityIdentifier = AccessiblityId.SCROLLVIEW_TEAM_AGENDA
        contentScrollView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(contentScrollView!)
        
        //Content View
        var yScreenView : CGFloat = 10
        
        //Title Screen
//        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        let xTitleScreen : CGFloat = pTitleScreen
//        let yTitleScreen : CGFloat = yScreenView
//        let wTitleScreen : CGFloat = self.view.frame.size.width - (xTitleScreen * 2)
//        let hTitleScreen : CGFloat = 40
//        let pBottomTitleScreen : CGFloat = 8
//
//        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
//        lbTitleScreen.textAlignment = .left
//        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
//        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
//        lbTitleScreen.text = NSLocalizedString("collaborate", comment: "")
//        contentScrollView?.addSubview(lbTitleScreen)
//
//        yScreenView += hTitleScreen
//        yScreenView += pBottomTitleScreen
        
        //Date Title
        let pTitleDate : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleDate : CGFloat = pTitleDate
        let yTitleDate : CGFloat = yScreenView
        let wTitleDate : CGFloat = self.view.frame.size.width - (xTitleDate * 2)
        let hTitleDate : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let pBottomTitleDate : CGFloat = 16
        let pBottomWhiteView : CGFloat = 10
        
        lbTitleDate = UILabel(frame: CGRect(x: xTitleDate, y: yTitleDate, width: wTitleDate, height: hTitleDate))
        lbTitleDate?.isAccessibilityElement = true
        lbTitleDate?.accessibilityIdentifier = AccessiblityId.LB_DATE
        lbTitleDate?.textAlignment = .left
        lbTitleDate?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleDate?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        lbTitleDate?.text = NSLocalizedString("May 07, 2019", comment: "")
        contentScrollView?.addSubview(lbTitleDate!)
        
        yScreenView += hTitleDate
        yScreenView += pBottomWhiteView
        
        let grayView : UIView = UIView(frame: CGRect(x: 0, y: yScreenView, width: contentScrollView!.frame.size.width, height: 0))
        grayView.backgroundColor = AppColor.NormalColors.BG_VIEW_COLOR
        contentScrollView?.addSubview(grayView)
        
        yScreenView += pBottomTitleDate
        
        
        //Chart View
        let pViewChart : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xViewChart : CGFloat = pViewChart
        let yViewChart : CGFloat = yScreenView
        let wViewChart : CGFloat = self.view.frame.size.width - (xViewChart * 2)
        let hViewChart : CGFloat = 36
        let pBottomViewChart : CGFloat = 16
        
        let vChart = UIView(frame: CGRect(x: xViewChart, y: yViewChart, width: wViewChart, height: hViewChart))
        contentScrollView?.addSubview(vChart)
        
        yScreenView += hViewChart
        yScreenView += pBottomViewChart
        
        //Title Chart
        let hTitleChart : CGFloat = hViewChart
        let wTitleChart : CGFloat = 180
        let yTitleChart : CGFloat = 0
        let xTitleChart : CGFloat = 0
        
        let lbTitleChart : UILabel = UILabel(frame: CGRect(x: xTitleChart, y: yTitleChart, width: wTitleChart, height: hTitleChart))
        lbTitleChart.isAccessibilityElement = true
        lbTitleChart.accessibilityIdentifier = AccessiblityId.LB_TITLE_CHART
        lbTitleChart.textAlignment = .left
        lbTitleChart.textColor = AppColor.NormalColors.DARK_COLOR
        lbTitleChart.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleChart.text = NSLocalizedString("BURN-UP", comment: "")
        vChart.addSubview(lbTitleChart)
        
        //Switch
        let hSwitch : CGFloat = hViewChart
        let wSwitch : CGFloat = 180
        let ySwitch : CGFloat = 0
        let xSwitch : CGFloat = vChart.frame.size.width - wSwitch
        
        dateSwitch = Suitchi(frame: CGRect(x: xSwitch, y: ySwitch, width: wSwitch, height: hSwitch),
                                   onSwitchedBlock: {[weak self] () -> () in
                                    self?.dateSwitch?.accessibilityLabel = AccessiblityId.BTN_TODAY
                                    self?.changeDate(date: AppSettings.sharedSingleton.agenda_today?.date ?? Date())
        }) {[weak self] () -> () in
            self?.dateSwitch?.accessibilityLabel = AccessiblityId.BTN_YESTERDAY
            self?.changeDate(date: AppSettings.sharedSingleton.agenda_yesterday?.date ?? (AppSettings.sharedSingleton.agenda_today?.date.yesterday ?? Date().yesterday))
        }
        dateSwitch?.isAccessibilityElement = true
        dateSwitch?.accessibilityIdentifier = AccessiblityId.SWITCH_DATE
        dateSwitch?.colorBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        dateSwitch?.offLabel = NSLocalizedString("yesterday", comment: "")
        dateSwitch?.onLabel = NSLocalizedString("today", comment: "")
        vChart.addSubview(dateSwitch!)
        
        let pBurnChart : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xBurnChart : CGFloat = pBurnChart
        let yBurnChart : CGFloat = yScreenView
        let wBurnChart : CGFloat = self.view.frame.size.width - (xViewChart * 2)
        let hBurnChart : CGFloat = wBurnChart*3/4 + 20 + 20
        let pBottomBurnChart : CGFloat = 16
        
        chartView = LineChartView(frame: CGRect(x: xBurnChart, y: yBurnChart, width: wBurnChart, height: hBurnChart))
        chartView?.accessibilityIdentifier = AccessiblityId.CHART_TEAM_AGENDA
        chartView?.bounds = CGRect(x: 0, y: 0, width: wBurnChart, height: hBurnChart)
        chartView?.noDataFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? NSUIFont.systemFont(ofSize: 14)
        chartView?.noDataTextColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        chartView?.dragEnabled = false
        chartView?.pinchZoomEnabled = false
        chartView?.scaleXEnabled = false
        chartView?.scaleYEnabled = false
        chartView?.doubleTapToZoomEnabled = false
        contentScrollView?.addSubview(chartView!)
        
//        chartView?.delegate = self
        chartView?.chartDescription?.enabled = false
        chartView?.dragEnabled = true
        chartView?.setScaleEnabled(true)
        chartView?.pinchZoomEnabled = true
        
        // x-axis limit line
        chartView?.xAxis.labelPosition = .bottom
        chartView?.xAxis.avoidFirstLastClippingEnabled = true
        chartView?.xAxis.drawGridLinesEnabled = true
        chartView?.xAxis.granularity = 1.0
        chartView?.xAxis.labelCount = 9
        chartView?.xAxis.gridLineDashLengths = [2, 2]
        chartView?.xAxis.axisMinimum = 9
        chartView?.xAxis.axisMaximum = 18
        chartView?.xAxis.valueFormatter = self
        //
        let fmt = NumberFormatter()
        fmt.numberStyle = .decimal
        fmt.maximumFractionDigits = 0
        fmt.groupingSeparator = ","
        fmt.decimalSeparator = "."
        chartView?.xAxis.valueFormatter = DefaultAxisValueFormatter.init(formatter: fmt)
        //
        // y-axis limit line
        chartView?.leftAxis.labelCount = maxinumRow
        chartView?.leftAxis.gridLineDashLengths = [2, 2]
        
        chartView?.rightAxis.enabled = false
        
        //Legend
        chartView?.legend.form = .line
        chartView?.legend.xEntrySpace = 15
        chartView?.legend.formToTextSpace = 15
        chartView?.legend.horizontalAlignment = .center
        chartView?.legend.verticalAlignment = .bottom
        chartView?.legend.orientation = .horizontal
        chartView?.legend.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12) ?? .systemFont(ofSize: 12)
        
        //Marker
        let marker = BalloonMarker(color: UIColor(hexString: "#1F2532"),
                                   font: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12) ?? .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView?.marker = marker
        
        
        yScreenView += hBurnChart
        yScreenView += pBottomBurnChart
        
        //Title Agenda
        let pTitleAgenda : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleAgenda : CGFloat = pTitleAgenda
        let yTitleAgenda : CGFloat = yScreenView
        let wTitleAgenda : CGFloat = self.view.frame.size.width - (xTitleAgenda * 2)
        let hTitleAgenda : CGFloat = 20
        let pBottomTitleAgenda : CGFloat = 16
        
        let lbTitleAgenda : UILabel = UILabel(frame: CGRect(x: xTitleAgenda, y: yTitleAgenda, width: wTitleAgenda, height: hTitleAgenda))
        lbTitleAgenda.textAlignment = .left
        lbTitleAgenda.textColor = AppColor.NormalColors.DARK_COLOR
        lbTitleAgenda.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitleAgenda.text = NSLocalizedString("agenda", comment: "").uppercased()
        contentScrollView?.addSubview(lbTitleAgenda)
        
        yScreenView += hTitleAgenda
        yScreenView += pBottomTitleAgenda
        
        //Collection team agenda
        let wCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.Width
        let hCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.Height
        
        let pCollectionAgenda : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xCollectionAgenda : CGFloat = pCollectionAgenda
        let yCollectionAgenda : CGFloat = yScreenView
        let wCollectionAgenda : CGFloat = self.view.frame.size.width - (xCollectionAgenda * 2)
        let hCollectionAgenda : CGFloat = hCollectionItemAgenda
        let pBottomCollectionAgenda : CGFloat = 16
        
        let flowLayout_Horizontal = AnimatedCollectionViewLayout()
        flowLayout_Horizontal.animator = ParallaxAttributesAnimator()
        flowLayout_Horizontal.scrollDirection = .horizontal
        flowLayout_Horizontal.itemSize = CGSize(width: wCollectionItemAgenda, height: hCollectionItemAgenda)
        flowLayout_Horizontal.minimumInteritemSpacing = 10.0
        
        tbTeamAgenda = UICollectionView(frame:  CGRect(x: xCollectionAgenda, y: yCollectionAgenda, width: wCollectionAgenda, height: hCollectionAgenda), collectionViewLayout: flowLayout_Horizontal)
        tbTeamAgenda?.accessibilityIdentifier = AccessiblityId.LIST_TEAM_AGENDA
        tbTeamAgenda?.dataSource = self
        tbTeamAgenda?.delegate = self
        tbTeamAgenda?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeamAgenda?.isOpaque = false
        tbTeamAgenda?.showsHorizontalScrollIndicator = false
        tbTeamAgenda?.register (TeamAgendaItemViewCell.self, forCellWithReuseIdentifier: TeamAgendaViewController.teamAgendaCellIdentifier)
        tbTeamAgenda?.register(TeamAgendaInviteMemberItemViewCell.self, forCellWithReuseIdentifier: TeamAgendaViewController.inviteMemberTeamAgendaCellIdentifier)
        contentScrollView?.addSubview(tbTeamAgenda!)
        
        yScreenView += hCollectionAgenda
        yScreenView += pBottomCollectionAgenda
        
        //Mics Update
//        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        grayView.frame.size.height = yScreenView - grayView.frame.origin.y
        contentScrollView?.contentSize = CGSize(width: contentScrollView!.frame.size.width, height:yScreenView)
        //
//        contentScrollView?.rx.contentOffset.subscribe {[weak self] in
//            let yOffset = $0.element?.y ?? 0
//            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
//            var isShow : Bool = false
//            if(yOffset > yScreen_Scroll){
//                isShow = true
//            }
//            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
//            }.disposed(by: disposeBag)
    }
    
    func initView(){
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        //Floating button
        showHideFloatingButton(isShow: true)
    }
    
    func setDefaultValue(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        //
//        loadAvatar()
        loadUnreadNotification()
        //
        subjectProjectId = BehaviorSubject<String>(value: projectId)
        changeDate(date: self.dateAgenda)
        //
        subjectUserId = BehaviorSubject<String>(value: userId)
    }
    
    func loadUnreadNotification(){
        let numberUnread = AppSettings.sharedSingleton.account?.numberUnread ?? 0
        AppUtil.resetCountingChat(badgeNumber: numberUnread, isShow: true)
//        if(numberUnread > 0){
//            needShakeNotification = true
//            lbNumNotification?.isHidden = false
//            //
//            let maxUnread = 99
//            var strNumberUnread = "99"
//            if(numberUnread < maxUnread){
//                strNumberUnread = "\(numberUnread)"
//            }
//            else{
//                strNumberUnread = "\(maxUnread)"
//            }
//            lbNumNotification?.text = strNumberUnread
//        }
//        else{
//            lbNumNotification?.isHidden = true
//        }
    }
    
//    func loadAvatar(){
//        DispatchQueue.main.async {[weak self] in
//            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
//            let userName = AppSettings.sharedSingleton.account?.name ?? ""
//            var user = User(userId: userId, dataType: .normal)
//            user.name = userName
//            //
//            self?.imvAvatar?.loadAvatarForUser(user: user)
//        }
//    }
    
    func changeDate(date: Date){
        startShimmering_View()
        //
        self.dateAgenda = date
        self.subjectDate.onNext(date)
        lbTitleDate?.text = date.dayMonthYearString()
    }
    
    func shakeViews(isShake : Bool){
//        if(isShake == true){
//            let interval : CGFloat = 5.0
//            btnNotification?.shake(with: SCShakeOptions(rawValue: SCShakeOptionsDirectionRotate.rawValue | SCShakeOptionsForceInterpolationExpDown.rawValue | SCShakeOptionsAutoreverse.rawValue), force: 0.15, duration: 1, iterationDuration: 0.03, completionHandler: { [weak self] in
//                if(interval > 0){
//                    self?.perform(#selector(self?.performAnimation_Notification), with: nil, afterDelay: TimeInterval(interval))
//                }
//            })
//        }
//        else{
//            btnNotification?.endShake()
//        }
    }
    
    @objc func performAnimation_Notification(){
//        if(needShakeNotification == true && pauseShakeNotification == false){
//            shakeViews(isShake: true)
//        }
    }
    
    func showHideFloatingButton(isShow : Bool){
        if(isShow == true){
            self.view.addSubview(self.btnFocusMode)
        }
        else{
            self.btnFocusMode.removeFromSuperview()
        }
    }
    
    
    func calculateChartData(agendas : [Agenda]?) {
        guard let strongAgendas = agendas, let strongChartView = chartView else { return }
        if(strongAgendas.count > 0){
            //Async
            DispatchQueue.main.async {[weak self] in
                guard let strongSelf = self else { return }
                // chartView.xAxis
                let leftAxis = strongChartView.leftAxis
                
                //Minimum tasks
                leftAxis.axisMinimum = 0
                
                //Maximum tasks
                var axisMaximum : Int = 0
                for agendaItem in strongAgendas{
                    axisMaximum = axisMaximum + agendaItem.tasks.count
                }
                leftAxis.axisMaximum = Double(axisMaximum)
                
                //Label count
                var needForce : Bool = false
                var labelCount : Int = 0
                if(axisMaximum == 0 || axisMaximum > strongSelf.maxinumRow){
                    labelCount = strongSelf.maxinumRow
                }
                else{
                    labelCount = axisMaximum
                    if(labelCount == 1){
                        needForce = true
                    }
                }
                leftAxis.setLabelCount(labelCount, force: needForce)
                
                
                let range9to16 = [9,10,11,12,13,14,15,16,17,18]
                var value9to16 = [0,0,0,0,0,0,0,0,0,0]
                var hasFinishedTask : Bool = false
                //Guideline data
                
                for agendaItem in strongAgendas{
                    for taskItem in agendaItem.tasks{
                        if(taskItem.completed == true && taskItem.finishdate != nil){
                            //
                            if(hasFinishedTask == false){
                                hasFinishedTask = true
                            }
                            //
                            let index = Util.findIndexOfHour(range9to16: range9to16, finishdate: taskItem.finishdate!)
                            if(index == -1){
                                let newIndex = 0
                                let value = value9to16[newIndex] + 1
                                value9to16[newIndex] = value
                            }
                            else if(index == range9to16.count){
                                let newIndex = range9to16.count-1
                                let value = value9to16[newIndex] + 1
                                value9to16[newIndex] = value
                            }
                            else{
                                var newIndex = index + 1
                                if(newIndex == range9to16.count){
                                    newIndex = range9to16.count-1
                                }
                                let value = value9to16[newIndex] + 1
                                value9to16[newIndex] = value
                            }
                        }
                    }
                }
                
                //Values 1
                var values1 : [ChartDataEntry] = []
                var index : Int = 0
                if(strongSelf.dateAgenda.day() != AppSettings.sharedSingleton.agenda_today?.date.day()){
                    index = range9to16.count
                }
                else{
                    let utcOffset = AppSettings.sharedSingleton.project?.utcOffset ?? 0
                    let utcDate = Date().dateForUTC(utcOffset:  utcOffset)
                    index = Util.findIndexOfHour(range9to16: range9to16, finishdate: utcDate)
                }
                if(index == -1){
                    let newIndex = 0
                    let value = value9to16[newIndex]
                    let dataEntry = ChartDataEntry(x: Double(range9to16[newIndex]), y: Double(value), icon: nil)
                    values1.append(dataEntry)
                }
                else{
                    for i in 0..<range9to16.count{
                        if(i <= index){
                            let value = range9to16[i]
                            let totalTask = Util.countTotalTask(value9to16: value9to16, to: i)
                            let dataEntry = ChartDataEntry(x: Double(value), y: Double(totalTask), icon: nil)
                            values1.append(dataEntry)
                        }
                    }
                }
                
                //Values 2 - Remaining task
                var values2 : [ChartDataEntry] = []
                let index0 = 0
                let range0 = range9to16[index0]
//                let value0 = value9to16[index0]
                let value0 = 0
                let dataEntry0 = ChartDataEntry(x: Double(range0), y: Double(value0), icon: nil)
                let indexN = range9to16.count - 1
                let rangeN = range9to16[indexN]
                let dataEntryN = ChartDataEntry(x: Double(rangeN), y: Double(axisMaximum), icon: nil)
                values2.append(dataEntry0)
                values2.append(dataEntryN)
                
                //Reset chartSet
                strongSelf.chartSet1.replaceEntries(values1)
                strongSelf.chartSet2.replaceEntries(values2)
                
                let dataSet = LineChartData(dataSets: [strongSelf.chartSet2,strongSelf.chartSet1])
                dataSet.setDrawValues(false)
                let noZeroFormatter = NumberFormatter()
                noZeroFormatter.zeroSymbol = ""
                dataSet.setValueFormatter(DefaultValueFormatter.init(formatter: noZeroFormatter))
                //
                strongChartView.data = dataSet
                strongChartView.animate(xAxisDuration: 2.5)
            }
        }
    }
    
    deinit {
        removeNotification()
    }
}

extension TeamAgendaViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let wCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.Width
        let hCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.Height
        return CGSize(width: wCollectionItemAgenda, height: hCollectionItemAgenda)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayTeamAgenda.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let element = self.arrayTeamAgenda[indexPath.row]
        if(element.dataType == .other){
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  TeamAgendaViewController.inviteMemberTeamAgendaCellIdentifier, for: indexPath) as? TeamAgendaInviteMemberItemViewCell else {
                return TeamAgendaInviteMemberItemViewCell()
            }
            cell.initView(itemWidth: Constant.CollectionItemAgenda.Width, itemHeight: Constant.CollectionItemAgenda.Height, isAvatar:false)
            cell.btnInvite?.addTarget(self, action: #selector(inviteAction), for: .touchUpInside)
            
            return cell
            
        }
        else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  TeamAgendaViewController.teamAgendaCellIdentifier, for: indexPath) as? TeamAgendaItemViewCell else {
                return TeamAgendaItemViewCell()
            }
            cell.initView(itemWidth: Constant.CollectionItemAgenda.Width, itemHeight: Constant.CollectionItemAgenda.Height, taskInteractionEnabled: false)
            cell.wipShowOther = false
            cell.itemIndex = indexPath.row
            //
            cell.setValueForCell(agenda: element,isEdited: false, isCanFinish: false)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        let heroId = "cell\(indexPath.row)"
        self.tbTeamAgenda?.hero.id = heroId
        //
        let rowIndex = indexPath.row
        self.detailView = TeamAgendaDetailViewController(nibName: nil, bundle: nil)
        self.detailView?.arrayTeamAgenda = self.arrayTeamAgenda
        self.detailView?.dateAgenda = self.dateAgenda
        self.detailView?.selectedIndex = rowIndex
        self.detailView?.modalPresentationStyle = .overCurrentContext
        self.detailView?.hero.isEnabled = true
        self.detailView?.tbTeamAgenda.hero.id = heroId
        self.detailView?.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        
        self.navigationController?.present(detailView!, animated: true, completion: nil)
        
        //Callback
        self.detailView?.closeCallback = {[weak self] (close) -> Void in
            if(close == true){
                self?.detailView = nil
                //
                self?.checkCurrentConnection()
            }
        }
    }
    
}

class ChartXValueFormatter : NSObject, IValueFormatter {
    
    // This method is  called when a value (from labels inside the chart) is formatted before being drawn.
    func stringForValue(_ value: Double,
                        entry: ChartDataEntry,
                        dataSetIndex: Int,
                        viewPortHandler: ViewPortHandler?) -> String {
        var digitWithoutFractionValues = String(format: "%.0f", value)
        if(value == 0 && dataSetIndex == 0){
            digitWithoutFractionValues = " "
        }
        return digitWithoutFractionValues
    }
    
}

extension TeamAgendaViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return String(value)
    }
}

//Shimming view for reload view
extension TeamAgendaViewController {
    func resetValue_View(){
        guard let strongChartView = chartView else { return }
        strongChartView.data = nil
        let leftAxis = strongChartView.leftAxis
        leftAxis.axisMaximum = Double(10)
    }
    
    func startShimmering_View(){
        resetValue_View()
        //Chart
        chartView?.noDataText = NSLocalizedString("loading", comment: "")
        chartView?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        chartView?.startShimmering()
    }
    
    func stopShimmering_View(){
        let isShimmering = chartView?.isShimmering()
        if(isShimmering == true){
            //Chart
            chartView?.noDataText = NSLocalizedString("no_chart_data_available", comment: "")
            chartView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            chartView?.stopShimmering()
        }
        
    }
}

//Notification event
extension TeamAgendaViewController {
    
    func addNotification(){
        addNotification_RetryNetwork()
        addNotification_UpdateProfile()
        addNotification_UpdateReadNotification()
        addNotification_NewNotification()
    }
    
    func addNotification_RetryNetwork(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retryNetwork(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_RETRY_NETWORK),
                                               object: nil)
    }
    
    @objc func retryNetwork(notification: Notification) {
        do {
            try reachabilityInternetConnection.startNotifier()
        } catch {
        }
    }
    
    func addNotification_UpdateProfile(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProfile(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_USER_INFO),
                                               object: nil)
    }
    
    @objc func updateProfile(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProfileUpdateInfoType{
                if(updatedType == .kProfileUpdateAvatar){
//                    loadAvatar()
                    //
//                    let userId = AppSettings.sharedSingleton.account?.userId ?? ""
//                    forceChangeAvatarForAllVisibleCell(userId: userId)
                }
            }
        }
    }
    
    func forceChangeAvatarForAllVisibleCell(userId : String){
        let itemPaths = tbTeamAgenda!.indexPathsForVisibleItems
        for itemPath in itemPaths{
            let cell = tbTeamAgenda!.cellForItem(at: itemPath)
            if let agendaCell = cell as? TeamAgendaItemViewCell{
                if(agendaCell.agenda.owned.userId == userId){
                    agendaCell.stateForAvatar(agendaCell.agenda.owned)
                }
            }
        }
    }
    
    func addNotification_UpdateReadNotification(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateReadNotification(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_NUMBER_READ_NOTIFICATION),
                                               object: nil)
    }
    
    @objc func updateReadNotification(notification: Notification) {
        loadUnreadNotification()
    }
    
    func addNotification_NewNotification() {
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(notifcation_UpdateNewNotifcation(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_NEW_REALTIME_DATA_NOTIFICATION),
                                               object: nil)
    }
    
    @objc func notifcation_UpdateNewNotifcation(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let newNotification = dictionary[ModelDataKeyString.COLLECTION_NOTIFICATION] as? NotificationData{
                //My user
                let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
                let myUserName = AppSettings.sharedSingleton.account?.name ?? ""
                var myUser = User(userId: myUserId, dataType: .normal)
                myUser.name = myUserName
                //
                let title = ""
                
//                let strDate = notification.createdDate.timeStampForChattingHeader(showMinute: true)
                let message = ""
                //
                let attributedString = NotificationService.formatAttributedString(notification: newNotification,user: myUser)
                //
                AppDelegate().sharedInstance().alertNotificationInApp(title: title,attributeString: attributedString, message: message,data: newNotification)
            }
        }
    }
    
//    func addNotification_UpdateUserTeamInfo(){
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(teamAgenda_UpdateInfo(notification:)),
//                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TEAM_USER_INFO),
//                                               object: nil)
//    }
    
    //Notification for task detail
//    @objc func teamAgenda_UpdateInfo(notification: Notification) {
//        if let dictionary = notification.object as? NSDictionary{
//            if let userProcessing = dictionary[ModelDataKeyString.COLLECTION_USER] as? User{
//                var needChanged : Int = -1
//                for i in 0..<arrayTeamAgenda.count{
//                    var agenda = arrayTeamAgenda[i]
//                    if(agenda.owned.userId == userProcessing.userId){
//                        agenda.owned = userProcessing
//                        arrayTeamAgenda[i] = agenda
//                        //
//                        needChanged = i
//                        break
//                    }
//                }
//                //
//                if(needChanged >= 0){
//                    let indexPath = IndexPath(row: needChanged, section: 0)
//                    tbTeamAgenda?.reloadItems(at: [indexPath])
//                }
//            }
//        }
//    }
//
    
    
    func removeNotification(){
        NotificationCenter.default.removeObserver(self)
    }
}

//Action - Goto other views
extension TeamAgendaViewController {
    @objc func backlogAction(){
        //
        DispatchQueue.main.async {
            let backlogView  = BacklogViewController(nibName: nil, bundle: nil)
            self.navigationController?.pushViewController(backlogView, animated: true)
        }
    }
    
    @objc func discussionAction(){
        openDiscussion(date: self.dateAgenda)
    }
    
    @objc func openDiscussion(date: Date){
        DispatchQueue.main.async {[weak self] in
            guard let strongSelf = self else { return }
            let activityView  = ActivityViewController(nibName: nil, bundle: nil)
            activityView.currentDate = date
            strongSelf.activityViewNavigation = DTOverlayController(viewController: activityView,viewNotPan: [activityView.tbActivity,activityView.containerInputChatView])
            strongSelf.activityViewNavigation?.overlayHeight = .dynamic(AppDevice.ScreenComponent.OverlayHeight)
            strongSelf.activityViewNavigation?.handleVerticalSpace = AppDevice.ScreenComponent.OverlayHandler
            
            strongSelf.navigationController?.present(strongSelf.activityViewNavigation!, animated: true, completion: nil)
        }
    }
    
    func checkActivityPresenting() -> Bool{
        var activityPresenting: Bool = false
        if let strongActivityViewNavigation = self.activityViewNavigation{
            if(strongActivityViewNavigation.presentingViewController != nil){
                activityPresenting = true
            }
        }
        return activityPresenting
    }
    
    @objc func notificationAction(){
        DispatchQueue.main.async {
            let notificationView  = NotificationViewController(nibName: nil, bundle: nil)
            notificationView.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.navigationController?.present(notificationView, animated: true, completion: nil)
        }
    }
    
    @objc func focusModeAction(){
        DispatchQueue.main.async {
            let focusModeView  = FocusModeViewController(nibName: nil, bundle: nil)
            self.navigationController?.pushViewController(focusModeView, animated: true)
        }
    }
    
    @objc func profileAction(){
        DispatchQueue.main.async {
            let settingView  = SettingViewController(nibName: nil, bundle: nil)
            self.navigationController?.pushViewController(settingView, animated: true)
        }
    }
    
    @objc func inviteAction(){
        DispatchQueue.main.async {
            let inviteMemberView = InviteMemberViewController(nibName: nil, bundle: nil)
            inviteMemberView.viewType = .update
            inviteMemberView.navigationType = .present
            //
            let navigationController = UINavigationController(rootViewController: inviteMemberView)
            navigationController.modalPresentationStyle = .fullScreen //or .overFullScreen
            self.navigationController?.present(navigationController, animated: true, completion: nil)
        }
    }
}

//Binding data to view model
extension TeamAgendaViewController {
    func bindToViewModel(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        //
        let orangnizationController = OrangnizationController.sharedAPI
        self.orangnizationViewModel = OrangnizationViewModel(
            inputOrangnizationByUserId: (
                userId: Observable.just(userId),
                orangnizationController: orangnizationController
            )
        )
        
        self.orangnizationViewModel?.orangnizations?
            .subscribe(onNext:  { orangnizations in
                if(orangnizations.count > 0){
                    let firstorangnizations =  orangnizations[0]
                    if(firstorangnizations.valid() == true){
                        AppSettings.sharedSingleton.saveOrangnization(firstorangnizations)
                        //
                        let dict = NSDictionary(objects: [firstorangnizations], forKeys: [ModelDataKeyString.COLLECTION_ORANGNIZATION as NSCopying])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_ORANGNIZATION_INFO), object: dict)
                        //
                    }
                }
            
        }).disposed(by: disposeBag)
        //
        let projectController = ProjectController.sharedAPI
        let agendaController = AgendaController.sharedAPI
        let taskController = TaskController.sharedAPI
        let userController = UserController.sharedAPI
        self.projectViewModel = ProjectViewModel(
            inputNeedProjects: (
                projectId: subjectProjectId?.asObservable(),
                projectController: projectController,
                needSubscrible:true
            )
        )
        
        self.projectViewModel?.projects?
            .subscribe(onNext:  { projects in
                if(projects.count > 0){
                    let firstProject = projects[0]
                    if(firstProject.dataType == .normal){
                        if(firstProject.valid() == true){
                            //
                            AppDelegate().sharedInstance().currentProject = firstProject
                            //
                            var updatedProjectType : Constant.ProjectUpdateInfoType = .kProjecUpdateNone
                            var updatedProjectOwnerType : Constant.ProjectUpdateInfoType = .kProjecUpdateNone
                            var updatedUserType : Constant.ProjectUpdateInfoType = .kProjecUpdateNone
                            if let currentProject = AppSettings.sharedSingleton.project{
                                if(firstProject.name != currentProject.name || firstProject.projectUserPlanType != currentProject.projectUserPlanType){
                                    updatedProjectType = .kProjecUpdateInfo
                                }
                                if(firstProject.owned.userId != currentProject.owned.userId){
                                    updatedProjectOwnerType = .kProjecUpdateOwner
                                }
                                if(firstProject.projectUserPlanType != currentProject.projectUserPlanType){
                                    updatedUserType = .kProjecUpdatePlanningType
                                }
                                //
                                if(updatedProjectType != .kProjecUpdateNone || updatedUserType != .kProjecUpdateNone || updatedProjectOwnerType != .kProjecUpdateNone){
                                    //
                                    DispatchQueue.main.async {
                                        AppSettings.sharedSingleton.saveProject(firstProject)
                                        //
                                        if(updatedProjectType != .kProjecUpdateNone){
                                            let dict = NSDictionary(objects: [updatedProjectType], forKeys: [Constant.keyValue as NSCopying])
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_INFO), object: dict)
                                        }
                                        if(updatedUserType != .kProjecUpdateNone){
                                            let dict = NSDictionary(objects: [updatedUserType], forKeys: [Constant.keyValue as NSCopying])
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_PLANNING_TYPE), object: dict)
                                        }
                                        if(updatedProjectOwnerType != .kProjecUpdateNone){
                                            let dict = NSDictionary(objects: [updatedProjectOwnerType], forKeys: [Constant.keyValue as NSCopying])
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_OWNER_TYPE), object: dict)
                                        }
                                    }
                                }
                            }
                            if(updatedProjectType == .kProjecUpdateNone && updatedUserType == .kProjecUpdateNone){
                                AppSettings.sharedSingleton.saveProject(firstProject)
                            }
                        }
                        
                    }
                    else if(firstProject.dataType == .empty){
//                        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
//                        appDelegate.setupMainView()
                    }
                }
            })
            .disposed(by: disposeBag)
        
        let usersId =
            self.projectViewModel!.projects!
//                .distinctUntilChanged { (lhs, rhs) -> Bool in
//                    let distinct = AgendaService.checkAgendaInfoBetweenTwoArray(lhs: lhs, rhs: rhs)
//                    return distinct
//                }
                .flatMapLatest { projects -> Observable<[User]> in
                    var ids : [User] = []
                    if(projects.count > 0){
                        let firstProject = projects[0]
                        if(firstProject.dataType == .normal){
                            ids =  projects[0].users
                        }
                    }
                    return Observable.just(ids)
                }.share(replay: 1)
        
        usersId.subscribe({[weak self] user  in
            guard let strongUsers = user.element else { return }
            //Check if only me, add Invite member cell
            if(strongUsers.count == 1){
                let firstUser = strongUsers[0]
                if(firstUser.valid() == true){
                    if(firstUser.userId == userId){
                        self?.subjectInviteMember.onNext(true)
                    }
                    else{
                        self?.subjectInviteMember.onNext(false)
                    }
                }
            }
            else{
                self?.subjectInviteMember.onNext(false)
            }
            //Check user contain me, If empty, user can remove from project
            if(strongUsers.count > 0){
                var existMe : Bool = false
                for user in strongUsers{
                    if(user.userId == userId){
                        existMe = true
                        break
                    }
                }
                //If not found
                if(existMe == false){
//                    let appDelegate : AppDelegate = AppDelegate().sharedInstance()
//                    appDelegate.setupMainView()
                }
            }
        })
            .disposed(by: disposeBag)
        
        let usersList =
            usersId
                .flatMapLatest { usersId1 -> Observable<[User]> in
                    var listUsers : [Observable<User>] = []
                    //Sub
                    for user in usersId1{
                        if(user.valid() == true){
                            let userViewModel = UserViewModel(
                                inputUserInfo: (
                                    userId: Observable.just(user.userId),
                                    userController: userController
                                )
                            )
                            if let users = userViewModel.user{
                                listUsers.append(users)
                            }
                        }
                    }
                    
                    return Observable.combineLatest(listUsers)
                    
        }
        
        usersList
            .subscribe({[weak self] usersList1  in
                guard let strongUsers = usersList1.element else { return }
                //
                var validAll : Bool = true
                for user in strongUsers {
                    if(user.valid() == false){
                        validAll = false
                    }
                }
                //
                if(validAll == true){
                    DispatchQueue.main.async {
                        AppSettings.sharedSingleton.saveUserToTeamInfoIfNeed(strongUsers)
                        //
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TEAM_USER_INFO), object: nil)
                        //
                        self?.tbTeamAgenda?.reloadData()
                    }
                }
            })
            .disposed(by: disposeBag)
        
        self.agendaViewModel = AgendaViewModel(
            inputUserTeam: (
                agendaController: agendaController,
                userId: usersId,
                projectId:projectId,
                duedate: subjectDate.asObservable(),
                sortUserId: userId,
                otherEmpty:true
            )
        )
        
        let agendaData = self.agendaViewModel?.agendas?
            .share(replay:1)
        
        agendaData?
            .subscribe({ agendas  in
                guard let strongAgendas = agendas.element else { return }
                var agendaTaskInfo : [[String:Any]] = []
                var needSaveTodayAgendaTask : Bool = false
                for agenda in strongAgendas {
                    if(agenda.valid() == true && agenda.date.isToday() == true && agenda.tasks.count > 0){
                        //
                        needSaveTodayAgendaTask = true
                        //
                        var agendaTaskIds : [String:Any] = [:]
                        var taskIds : [String] = []
                        for task in agenda.tasks {
                            taskIds.append(task.taskId)
                        }
                        agendaTaskIds[Constant.keyId] = agenda.agendaId
                        agendaTaskIds[Constant.keyValue] = taskIds
                        //
                        agendaTaskInfo.append(agendaTaskIds)
                    }
                }
                //
                if(needSaveTodayAgendaTask == true){
                    AppSettings.sharedSingleton.saveTaskAgendaIds(agendaTaskInfo)
                }
            })
            .disposed(by: disposeBag)
        
        //
        let tasksInfo =
            agendaData!
            .distinctUntilChanged { (lhs, rhs) -> Bool in
                let distinct = AgendaService.checkAgendaInfoBetweenTwoArray(lhs: lhs, rhs: rhs)
                return distinct
            }
                .flatMapLatest { agendas -> Observable<[[String : Any]]> in
                    var tasksInfo1 : [[String : Any]] = []
                    for agenda in agendas {
                        var ids : [String] = []
                        for task in agenda.tasks {
                            ids.append(task.taskId)
                        }
                        let userId = agenda.owned.userId
                        //
                        let taskInfo : [String : Any] = [
                            "taskId": ids,
                            "userId": userId,
                            "findType": Constant.FindType.findIn,
                        ]
                        tasksInfo1.append(taskInfo)
                    }
                    return Observable.just(tasksInfo1)
        }
        
        let tasksList =
            tasksInfo
                .flatMapLatest { tasksInfo1 -> Observable<[[Task]]> in
                    var listTasks : [Observable<[Task]>] = []
                    for taskInfo in tasksInfo1 {
                        let taskViewModel1 : TaskViewModel
                        let tasksID = taskInfo["taskId"] as? [String] ?? []
                        if(tasksID.count > 0){
                            if(tasksID.count == 1){
                                let taskId = tasksID[0]
                                taskViewModel1 = TaskViewModel(
                                    inputSingleTaskId: (
                                        taskID:taskId,
                                        loginTaps:BehaviorSubject<Void>(value:()).asObservable(),
                                        taskController: taskController
                                    )
                                )
                            }
                            else{
                                taskViewModel1 = TaskViewModel(
                                    inputTaskAgenda: (
                                        taskInfo:Observable.just(taskInfo),
                                        taskController: taskController,
                                        dataSource : Observable.just([]),
                                        projectId: projectId,
                                        findOther:false,
                                        needEmptyData:true
                                    )
                                )
                            }
                        }
                        else{
                            taskViewModel1 = TaskViewModel(
                                inputTaskAgenda: (
                                    taskInfo:Observable.just(taskInfo),
                                    taskController: taskController,
                                    dataSource : Observable.just([]),
                                    projectId: projectId,
                                    findOther:false,
                                    needEmptyData:true
                                )
                            )
                        }
                        
                        if let tasks = taskViewModel1.tasks{
                            listTasks.append(tasks)
                        }
                    }
                    return Observable.combineLatest(listTasks)
        }
        
        let agendaAndTasks = Observable
            .combineLatest(agendaData!, tasksList)  { (agenda: $0, task: $1) }
            .flatMapLatest{ agendaTask -> Observable<[Agenda]> in
                return AgendaService.combineTaskOfAgenda(arrayTask: agendaTask.task, arrayAgenda: agendaTask.agenda)
        }
        
        let agendaInviteAndTasks = self.subjectInviteMember
            .flatMapLatest{ inviteMemeber -> Observable<[Agenda]> in
                if(inviteMemeber == true){
                    let inviteAgenda = Agenda(agendaId: "", dataType: .other)
                    return Observable
                        .combineLatest(agendaAndTasks, Observable.just(inviteAgenda))  { (agendas: $0, inviteAgenda: $1) }
                        .flatMapLatest{ agendaInviteTask -> Observable<[Agenda]> in
                            var agendaInviteTaskCombine : [Agenda] = []
                            for agenda in agendaInviteTask.agendas{
                                agendaInviteTaskCombine.append(agenda)
                            }
                            agendaInviteTaskCombine.append(agendaInviteTask.inviteAgenda)
                            return Observable.just(agendaInviteTaskCombine)
                    }
                }
                else{
                    return agendaAndTasks
                }
            }
            .share(replay: 1)
        
        agendaInviteAndTasks.subscribe({ [weak self] agendaTask  in
            if let strongAgendaTasks = agendaTask.element{
                self?.arrayTeamAgenda = strongAgendaTasks
                //
                if(strongAgendaTasks.count > 0){
                    let firstAgenda = strongAgendaTasks[0]
                    if(firstAgenda.dataType != .holder){
                        self?.stopShimmering_View()
                        //
                        self?.calculateChartData(agendas: self?.arrayTeamAgenda)
                    }
                }
            }
            else{
                self?.arrayTeamAgenda = []
            }
            //
            self?.tbTeamAgenda?.reloadData()
            //
            self?.detailView?.arrayTeamAgenda = self?.arrayTeamAgenda ?? []
        })
            .disposed(by: disposeBag)
        
        //Realtime for subcrible user info
        let userViewModel = UserViewModel(
            inputMyAccount: (
                userId: subjectUserId?.asObservable(),
                subscrible: true,
                userController: userController
            )
        )
        userViewModel.account?.subscribe({ accountInfo  in
            if let account = accountInfo.element{
                if(account.valid() == true){
                    //
                    var hasChangeTeam : Bool = false
                    if let currentProject = AppSettings.sharedSingleton.project{
                        if(account.defaultProject != currentProject.projectId){
                            hasChangeTeam = true
                            //
                            let appDelegate : AppDelegate = AppDelegate().sharedInstance()
                            appDelegate.setupMainView()
                        }
                    }
                    //
                    if(hasChangeTeam == false){
                        var updatedType : Constant.ProfileUpdateInfoType = .kProfileUpdateNone
                        if let currentAccount = AppSettings.sharedSingleton.account{
                            if(account.lastChangedAvatar != currentAccount.lastChangedAvatar){
                                updatedType = .kProfileUpdateAvatar
                                if(account.name != currentAccount.name){
                                    updatedType = .kProfileUpdateAll
                                }
                            }
                            else{
                                if(account.name != currentAccount.name){
                                    updatedType = .kProfileUpdateInfo
                                }
                            }
                            //
                            if(account.emailStatus != currentAccount.emailStatus || account.notificationStatus != currentAccount.notificationStatus){
                                AppSettings.sharedSingleton.saveSettingUser(emailStatus: account.emailStatus, notificationStatus: account.notificationStatus)
                                //
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_SETTING_NOTIFICATION), object: nil)
                            }
                            //
                            if(updatedType != .kProfileUpdateNone){
                                if(updatedType == .kProfileUpdateAvatar || updatedType == .kProfileUpdateAll){
                                    Util.resetImageData(userId)
                                }
                                //
                                DispatchQueue.main.async {
                                    AppSettings.sharedSingleton.saveMiscAccount(nil,name: account.name,password: account.password,deviceToken: nil)
                                    //
                                    let dict = NSDictionary(objects: [updatedType], forKeys: [Constant.keyValue as NSCopying])
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_USER_INFO), object: dict)
                                }
                            }
                        }
                        if(updatedType == .kProfileUpdateNone){
                            AppSettings.sharedSingleton.saveMiscAccount(nil,name: account.name,password: account.password,deviceToken: nil)
                        }
                    }
                }
            }
        })
            .disposed(by: disposeBag)
        
        if let strongSubjectUserId = subjectUserId{
            let notificationController = NotificationController.sharedAPI
            self.notificationViewModel = NotificationViewModel(
                input: (
                    projectId:projectId,
                    userId: strongSubjectUserId.asObservable(),
                    notificationController: notificationController
                )
            )
            
            
            self.notificationViewModel?.notifications?.subscribe(onNext:  {notifications in
                //
                NotificationStored.sharedSingleton.storeNotifications(notifications)
                //
                DispatchQueue.main.async {
                    let dict = NSDictionary(objects: [notifications], forKeys: [ModelDataKeyString.COLLECTION_NOTIFICATION as NSCopying])
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_NEW_DATA_NOTIFICATION), object: dict)
                }
            })
                .disposed(by: disposeBag)
        }
        
    }
}

