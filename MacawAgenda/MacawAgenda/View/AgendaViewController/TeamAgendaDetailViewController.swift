//
//  TeamAgendaDetailViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import Material
import AnimatedCollectionViewLayout

class TeamAgendaDetailViewController: BaseViewController {
    //Views
    var tbTeamAgenda = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var viewUserhorizontal : UserHorizontalView?
    var planningView : TeamAgendaPlanningViewController?
    
    //Data
    let teamAgendaLargeCellIdentifier : String = "teamAgendaLargeCellIdentifier"
    let teamAgendaInviteMemberLargeCellIdentifier : String = "teamAgendaInviteMemberLargeCellIdentifier"
    var selectedIndex : Int = 0
    var arrayTeamAgenda : [Agenda] = [] {
        didSet {
            // Update the view.
            setDataView()
        }
    }
    var dateAgenda = Date()
    //Callback
    var closeCallback: ((_ isClose: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
    }
    
    func addNotification(){
        addNotification_UpdateProject()
    }
    
    func addNotification_UpdateProject(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProject(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_OWNER_TYPE),
                                               object: nil)
    }
    
    @objc func updateProject(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProjectUpdateInfoType{
                if(updatedType == .kProjecUpdateOwner){
                    tbTeamAgenda.reloadData()
                }
            }
        }
    }
    
    func removeNotification(){
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Update the view.
        scrollToIndex()
    }
    
    func initView(){
        self.view.isOpaque = false
        self.view.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        
        //Background View
        let bgView = UIScrollView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: AppDevice.ScreenComponent.Height))
        bgView.backgroundColor = UIColor(hexString: "#1F2532").withAlphaComponent(0.9)
        self.view.addSubview(bgView)
        
        var yHeader : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hHeader : CGFloat = 44
        let pBottomHeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let headerView : UIView = UIView(frame: CGRect(x: 0, y: yHeader, width: bgView.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        bgView.addSubview(headerView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hClose = hHeader
        let xClose = headerView.frame.size.width - hClose - pClose
        let yClose : CGFloat = 0
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: hClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_tf_white"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        headerView.addSubview(btnClose)
        
        yHeader += hHeader
        yHeader += pBottomHeader
        //
        let hViewTeam : CGFloat = 40
        let pViewTeam : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.Width
        let hCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.Height
        let pCollectionAgenda : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xCollectionAgenda : CGFloat = pCollectionAgenda
        let yCollectionAgenda : CGFloat = yHeader
        let wCollectionAgenda : CGFloat = bgView.frame.size.width - (pCollectionAgenda * 2)
//        let pBottomCollectionAgenda : CGFloat = 80
        let pBottomCollectionAgenda : CGFloat = 0
        let hCollectionAgenda : CGFloat = bgView.frame.size.height - yCollectionAgenda - pBottomCollectionAgenda - (hViewTeam + pViewTeam * 2)
        
        
        let flowLayout_Horizontal = AnimatedCollectionViewLayout()
        flowLayout_Horizontal.animator = ParallaxAttributesAnimator()
        flowLayout_Horizontal.scrollDirection = .horizontal
        flowLayout_Horizontal.itemSize = CGSize(width: wCollectionItemAgenda, height: hCollectionItemAgenda)
        flowLayout_Horizontal.minimumInteritemSpacing = 10.0
        
        //
        tbTeamAgenda.frame = CGRect(x: xCollectionAgenda, y: yCollectionAgenda, width: wCollectionAgenda, height: hCollectionAgenda)
        tbTeamAgenda.collectionViewLayout = flowLayout_Horizontal
        tbTeamAgenda.delegate = self
        tbTeamAgenda.dataSource = self
        tbTeamAgenda.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeamAgenda.isOpaque = false
        tbTeamAgenda.showsHorizontalScrollIndicator = false
        tbTeamAgenda.isPagingEnabled = true
        tbTeamAgenda.register(TeamAgendaItemViewCell.self, forCellWithReuseIdentifier: teamAgendaLargeCellIdentifier)
        tbTeamAgenda.register(TeamAgendaInviteMemberItemViewCell.self, forCellWithReuseIdentifier: teamAgendaInviteMemberLargeCellIdentifier)
        bgView.addSubview(tbTeamAgenda)
        
        yHeader += hCollectionAgenda
        yHeader += pBottomCollectionAgenda
        
        let xUserhorizontal : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wUserhorizontal : CGFloat = AppDevice.ScreenComponent.Width - (xUserhorizontal * 2)
        let hUserhorizontal : CGFloat = AppDevice.SubViewFrame.UserHorizontalItemHeight
        let yUserhorizontal : CGFloat = bgView.frame.size.height - hUserhorizontal
        viewUserhorizontal = UserHorizontalView(frame: CGRect(x: xUserhorizontal, y: yUserhorizontal, width: wUserhorizontal, height: hUserhorizontal))
        viewUserhorizontal?.delegate = self
        bgView.addSubview(viewUserhorizontal!)
        //
        updateUserTeamAgenda()
    }
    
    
    func setDataView(){
        tbTeamAgenda.reloadData()
        //
        updateUserTeamAgenda()
        //
        checkPlanningViewShowing()
    }
    
    func scrollToIndex(){
        tbTeamAgenda.scrollToItem(at:IndexPath(row: selectedIndex, section: 0), at: .left, animated: true)
        //
        handleUserHorizontalViewScroll(index: selectedIndex)
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: {
            self.closeCallback?(true)
        })
        
    }
    
    func checkPermission(_ agendaOwner: String) -> Bool{
        var isEditPermission : Bool = false
        //
        let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectOwnedId = AppSettings.sharedSingleton.project?.owned.userId ?? ""
        if(myUserId.count > 0 && (myUserId == projectOwnedId || myUserId == agendaOwner)){
            isEditPermission = true
        }
        if(isEditPermission == false){
            let orangnizationOwnedId = AppSettings.sharedSingleton.orangnization?.owner.userId ?? ""
            if(myUserId.count > 0 && orangnizationOwnedId.count > 0){
                if(myUserId == orangnizationOwnedId){
                    isEditPermission = true
                }
            }
        }
        return isEditPermission
    }
    
    //
    func checkEdited(_ agenda : Agenda) -> Bool{
        var isEdited : Bool = false
        let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectOwnedId = AppSettings.sharedSingleton.project?.owned.userId
        //
        if let dateTodayAgenda = AppSettings.sharedSingleton.agenda_today?.date{
            if(agenda.date.isSameDay(dateTodayAgenda)){
                //
                if(myUserId.count > 0){
                    if(myUserId == agenda.owned.userId){
                        isEdited = true
                    }
                    else if(myUserId == projectOwnedId){
                        if(agenda.tasks.count > 0){
                            let firstTask = agenda.tasks[0]
                            if(firstTask.dataType == .normal){
                                isEdited = true
                            }
                        }
                    }
                }
            }
        }
        return isEdited
    }
    
    func checkPlanningViewShowing(){
        if(planningView != nil){
            for agenda in arrayTeamAgenda{
                if(agenda.agendaId.count > 0 && agenda.agendaId == planningView!.agenda.agendaId){
                    planningView?.agenda = agenda
                    break
                }
            }
        }
    }
    
    deinit {
        // Release all resources
        // perform the deinitialization
        closeCallback = nil
        //
        removeNotification()
    }
}

extension TeamAgendaDetailViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let wCollectionItemAgenda : CGFloat = Constant.CollectionItemAgenda.LargeWidth
        let hCollectionItemAgenda : CGFloat = tbTeamAgenda.frame.size.height
        return CGSize(width: wCollectionItemAgenda, height: hCollectionItemAgenda)
    }
    //
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayTeamAgenda.count;
    }
    //
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let element = arrayTeamAgenda[indexPath.row]
        //Padding bottom for under view, but can scroll item
        let pBottomCollectionAgenda : CGFloat = 80
        let itemHeight = collectionView.frame.size.height - pBottomCollectionAgenda
        //
        if(element.dataType == .other){
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  teamAgendaInviteMemberLargeCellIdentifier, for: indexPath) as? TeamAgendaInviteMemberItemViewCell else {
                return TeamAgendaInviteMemberItemViewCell()
            }
            //
            cell.initView(itemWidth: Constant.CollectionItemAgenda.LargeWidth, itemHeight: itemHeight, isAvatar:true)
            cell.btnInvite?.addTarget(self, action: #selector(inviteAction), for: .touchUpInside)
            
            return cell
            
        }
        else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  teamAgendaLargeCellIdentifier, for: indexPath) as? TeamAgendaItemViewCell else {
                return TeamAgendaItemViewCell()
            }
            cell.delegate = self
            cell.initView(itemWidth: Constant.CollectionItemAgenda.LargeWidth, itemHeight: itemHeight, taskInteractionEnabled: true)
            cell.wipShowOther = true
            cell.itemIndex = indexPath.row
            //Check permission
            let agenda = arrayTeamAgenda[indexPath.row]
            let isEdited = checkEdited(agenda)
            let isCanFinish = checkPermission(agenda.owned.userId)
            cell.setValueForCell(agenda: agenda,isEdited: isEdited, isCanFinish:isCanFinish)
            return cell
        }
    }
    //
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    }
}

extension TeamAgendaDetailViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //
        targetContentOffset.pointee = scrollView.contentOffset
        var factor: CGFloat = 0.9
        if velocity.x < 0 {
            factor = -factor
        }
        var visibleIndex = Int(scrollView.contentOffset.x/Constant.CollectionItemAgenda.LargeWidth + factor)
        if(abs(visibleIndex-selectedIndex) > 1){
            if(selectedIndex > visibleIndex){
                visibleIndex = selectedIndex - 1
            }
            else if(selectedIndex < visibleIndex){
                visibleIndex = selectedIndex + 1
            }
        }
        //
        guard let strongUserHorizontalView = viewUserhorizontal else { return }
        if(visibleIndex >= 0 && visibleIndex < strongUserHorizontalView.arrayUsers.count){
            let indexPath = IndexPath(row: visibleIndex, section: 0)
            tbTeamAgenda.scrollToItem(at: indexPath, at: .left, animated: true)
            
            if(visibleIndex != selectedIndex){
                selectedIndex = visibleIndex
                handleUserHorizontalViewScroll(index: selectedIndex)
            }
        }
        else if(visibleIndex == strongUserHorizontalView.arrayUsers.count){
            if(tbTeamAgenda.numberOfItems(inSection: 0) > strongUserHorizontalView.arrayUsers.count){
                let indexPath = IndexPath(row: visibleIndex, section: 0)
                tbTeamAgenda.scrollToItem(at: indexPath, at: .left, animated: true)
            }
        }
    }
    
}

extension TeamAgendaDetailViewController: UserHorizontalViewDelegate {
    func updateUserTeamAgenda(){
        var users : [User] = []
        for agenda in arrayTeamAgenda{
            if(agenda.owned.userId.count > 0){
                users.append(agenda.owned)
            }
        }
        viewUserhorizontal?.reloadData(users: users)
        if(selectedIndex >= users.count){
            selectedIndex = 0
        }
        handleUserHorizontalViewScroll(index: selectedIndex)
    }
    
    func handleUserHorizontalViewScroll(index: Int){
        guard let strongUserHorizontalView = viewUserhorizontal else { return }
        var newIndex = index
        if(index >= strongUserHorizontalView.arrayUsers.count){
            newIndex = newIndex - 1
        }
        viewUserhorizontal?.scrollToIndex(index: newIndex)
    }
    
    func horizontalView(_ horizontalView: UserHorizontalView, clickedAt buttonIndex: Int){
        if(buttonIndex != selectedIndex){
            selectedIndex = buttonIndex
            tbTeamAgenda.scrollToItem(at:IndexPath(row: selectedIndex, section: 0), at: .left, animated: true)
        }
    }
}

extension TeamAgendaDetailViewController: TeamAgendaItemViewCellDelegate {
    func teamAgendaItemViewCellTaskItemSelected(_ teamAgendaItemViewCell: TeamAgendaItemViewCell, didSelectRowAt: Int, task: Task) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            let heroId = "cell\(didSelectRowAt)"
            teamAgendaItemViewCell.contentView.hero.id = heroId
            //
            let taskDetailView = TaskDetailViewController(nibName: nil, bundle: nil)
            taskDetailView.taskDetailData = task
            taskDetailView.hero.isEnabled = true
            taskDetailView.tbBacklog.hero.id = heroId
            taskDetailView.modalPresentationStyle = .fullScreen //or .overFullScreen
            
            strongSelf.present(taskDetailView, animated: true, completion: nil)
        }
    }
    
    
    func teamAgendaItemViewCellEdited(_ teamAgendaItemViewCell: TeamAgendaItemViewCell) {
        let heroId = "cell\(2)"
        teamAgendaItemViewCell.btnEditPlanning?.hero.id = heroId
        //
        planningView = TeamAgendaPlanningViewController(nibName: nil, bundle: nil)
        planningView?.viewType = .update
        planningView?.userId = teamAgendaItemViewCell.agenda.owned.userId
        planningView?.hero.isEnabled = true
        planningView?.tbBacklog.hero.id = heroId
        if(planningView != nil){
            present(planningView!, animated: true, completion: nil)
        }
        planningView?.agenda = teamAgendaItemViewCell.agenda
        //Callback
        planningView?.closeCallback = {[weak self] (close) -> Void in
            if(close == true){
                self?.planningView = nil
            }
        }
    }
    
    func teamAgendaItemViewCellTaskItemFinishChanged(_ teamAgendaItemViewCell: TeamAgendaItemViewCell, didSelectRowAt: Int, task : Task, taskRowIndex: Int){
        if let agendaCell = self.tbTeamAgenda.cellForItem(at: IndexPath(row: didSelectRowAt, section: 0)) as? TeamAgendaItemViewCell{
            if let taskItemCell = agendaCell.tbTalks?.cellForRow(at: IndexPath(row: taskRowIndex, section: 0)) as? TeamAgendaTaskItemViewCell{
                taskItemCell.startProcessingFinishTask(startAnimationRow: true)
                //
                self.updateTaskFinishChanged(task: task)
            }
        }
    }
}

//Action - Goto other views
extension TeamAgendaDetailViewController {
    @objc func inviteAction(){
        let inviteMemberView = InviteMemberViewController(nibName: nil, bundle: nil)
        inviteMemberView.viewType = .update
        inviteMemberView.navigationType = .present
        //
        let navigationController = UINavigationController(rootViewController: inviteMemberView)
        navigationController.modalPresentationStyle = .fullScreen //or .overFullScreen
        
        self.present(navigationController, animated: true, completion: nil)
    }
}

extension TeamAgendaDetailViewController {
    func trackFinishTaskInCollaborate(taskId: String,fromCollaborate : Bool){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        if(fromCollaborate == true){
            Tracker.trackFinishTaskInCollaborate(taskid: taskId, userId: userId, wip: true)
        }
        else{
            Tracker.trackFinishTaskInFocus(taskid: taskId, userId: userId, wip: true)
        }
    }
}

//Binding data to view model
extension TeamAgendaDetailViewController {
    func updateTaskFinishChanged(task: Task){
        let agendaTaskId : String = task.checkIsTodayAgenda()
        //
        let taskId = task.taskId
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        var taskAPIViewModel : TaskAPIViewModel?
        // Update task finish
        if(agendaTaskId.count > 0){
            let finished = task.completed == true ? Constant.TaskStatus.kTaskStatusUncompleted.rawValue : Constant.TaskStatus.kTaskStatusCompleted.rawValue
            // Update task finish
            taskAPIViewModel = TaskAPIViewModel(
                inputUpdateTaskAgendaStatus: (
                    taskId: taskId,
                    projectId: projectId,
                    agendaId: agendaTaskId,
                    status: BehaviorSubject<Int>(value: finished),
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    controller: taskController
                )
            )
        }
        else{
            let finished = !task.completed
            //
            taskAPIViewModel = TaskAPIViewModel(
                inputUpdateTaskFinished: (
                    taskId: taskId,
                    projectId: projectId,
                    finished: BehaviorSubject(value: finished).asObservable(),
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    controller: taskController
                )
            )
        }
        taskAPIViewModel?.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                        }
                        //
                        self?.trackFinishTaskInCollaborate(taskId: reponseAPI.data, fromCollaborate: true)
                    }
                    //
                    self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func stopProcessingForAllVisibleCell(){
        let itemPaths = tbTeamAgenda.indexPathsForVisibleItems
        for itemPath in itemPaths{
            let cell = tbTeamAgenda.cellForItem(at: itemPath)
            if let agendaCell = cell as? TeamAgendaItemViewCell{
                let cellTablePaths = agendaCell.tbTalks?.indexPathsForVisibleRows
                if let strongPaths = cellTablePaths{
                    for path in strongPaths{
                        if let taskItemCell = agendaCell.tbTalks?.cellForRow(at: path) as? TeamAgendaTaskItemViewCell{
                            taskItemCell.startProcessingFinishTask(startAnimationRow: true)
                        }
                    }
                }
            }
        }
    }
    
    func getVisibleCellFromAgendaTask(taskId: String) -> TeamAgendaTaskItemViewCell?{
        var visibleCell : TeamAgendaTaskItemViewCell?
        //
        let itemPaths = tbTeamAgenda.indexPathsForVisibleItems
        for itemPath in itemPaths{
            let cell = tbTeamAgenda.cellForItem(at: itemPath)
            if let agendaCell = cell as? TeamAgendaItemViewCell{
                let cellTablePaths = agendaCell.tbTalks?.indexPathsForVisibleRows
                if let strongPaths = cellTablePaths{
                    for path in strongPaths{
                        if let taskItemCell = agendaCell.tbTalks?.cellForRow(at: path) as? TeamAgendaTaskItemViewCell{
                            if(taskItemCell.task?.taskId == taskId){
                                visibleCell = taskItemCell
                                break
                            }
                        }
                    }
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromAgendaTask(taskId: taskId){
            visibleCell.stopProcessingFinishTask()
        }
    }

}
