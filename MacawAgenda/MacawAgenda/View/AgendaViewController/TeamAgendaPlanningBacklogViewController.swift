//
//  TeamAgendaPlanningBacklogViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/5/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import RxDataSources
import Material

class TeamAgendaPlanningBacklogViewController: BaseViewController {
    //External props
    var userId : String = ""
    var agendaId : String = ""
    //Views
    var navigationView : CustomNavigationView?
    var lbTitleScreen : UILabel?
    var lbTitleHeader : UILabel?
    var tbBacklog =  UITableView(frame: .zero)
    //Data
    var yScreen_Scroll : CGFloat = 0
    var agendaViewModel : AgendaViewModel?
    var taskViewModel : TaskViewModel?
    //Constants
    static let taskCellIdentifier : String = "taskCellIdentifier"
    static let emptyTaskCellIdentifier : String = "emptyTaskCellIdentifier"
    var subjectUser = BehaviorSubject<String>(value: "")
    var subjectDate = BehaviorSubject<Date>(value: AppSettings.sharedSingleton.agenda_today?.date ?? Date())
    //Datasource
    var dataSource: RxTableViewSectionedReloadDataSource<TaskSection>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        setDefaultValue()
        //
        bindToViewModel()
    }
    
    func setDefaultValue(){
        subjectUser = BehaviorSubject<String>(value: userId)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose = pClose
        let wClose = hContentHeaderView
        let hClose = wClose
        let yClose : CGFloat = yContentHeaderView
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: hClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView?.addSubview(btnClose)
        
        //Title Screen
        let pTitleHeader : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = wHeaderView - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        lbTitleHeader = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader?.textAlignment = .left
        lbTitleHeader?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        navigationView?.titleView(titleView: lbTitleHeader!)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = AppDevice.ScreenComponent.Width
        let hTableBacklog : CGFloat = AppDevice.ScreenComponent.Height - yTableBacklog
        tbBacklog.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbBacklog.register(TeamAgendaTaskNotActionItemViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.taskNotActionCellIdentifier)
        self.view.addSubview(tbBacklog)
        
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableBacklog
        let tbBacklogHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: 0))
        tbBacklogHeaderView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 4.0
        
        lbTitleScreen = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen?.textAlignment = .left
        lbTitleScreen?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        tbBacklogHeaderView.addSubview(lbTitleScreen!)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //
        let xTitleNote : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yTitleNote : CGFloat = yViewScreen
        let wTitleNote : CGFloat = wHeaderTableView - (xTitleNote * 2)
        let hTitleNote : CGFloat = 48
        let hBottomTitleNote : CGFloat = 0
        
        let lbTitleNote = UILabel(frame: CGRect(x: xTitleNote, y: yTitleNote, width: wTitleNote, height: hTitleNote))
        lbTitleNote.backgroundColor = AppColor.MicsColors.FILE_MESSAGE_BORDER_COLOR
        lbTitleNote.layer.cornerRadius = 4.0
        lbTitleNote.layer.masksToBounds = true
        lbTitleNote.textAlignment = .left
        lbTitleNote.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleNote.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitleNote.text = "    " + NSLocalizedString("swipe_left_task_to_add_to_agenda", comment: "")
        tbBacklogHeaderView.addSubview(lbTitleNote)
        
        yViewScreen += hTitleNote
        yViewScreen += hBottomTitleNote
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen!.frame.origin.y + lbTitleScreen!.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbBacklogHeaderView.frame.size.height = hHeaderTableView
        tbBacklog.tableHeaderView = tbBacklogHeaderView
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: {
        })
    }
}

extension TeamAgendaPlanningBacklogViewController : UITableViewDelegate,SwipeTableViewCellDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionData = self.dataSource?[section] else { return nil }
        
        
        let wHeader : CGFloat = tableView.frame.size.width
        let hHeader : CGFloat = 48
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: wHeader, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //
        let pTopTitleName : CGFloat = 12
        let xTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wTitleName : CGFloat = wHeader - (xTitleName * 2)
        let hTitleName : CGFloat = 36
        
        let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: pTopTitleName, width: wTitleName, height: hTitleName))
        lbTitleName.text = sectionData.header
        lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleName.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        headerView.addSubview(lbTitleName)
        //
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let heightRow = AppDevice.TableViewRowSize.TaskHeight
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 12 + 36
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let workAction = SwipeAction(style: .default, title: NSLocalizedString("add_to_my_agenda", comment: "")) {[weak self] action, indexPath in
            // handle action by updating model with deletion
            guard let strongSelf = self else { return }
            if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR, startAnimationRow: true)
                //
                taskCell.hideSwipe(animated: true)
                //
                strongSelf.updatePlanningTask(taskId: task.taskId)
            }
        }
        workAction.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        workAction.textColor = AppColor.NormalColors.WHITE_COLOR
        workAction.backgroundColor = AppColor.NormalColors.BLUE_COLOR
        
        return [workAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 170
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.MicsColors.DISABLE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.NormalColors.WHITE_COLOR)
    }
    
}

//Binding data to view model
extension TeamAgendaPlanningBacklogViewController {
    
    func updatePlanningTask(taskId: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let agendaController = AgendaAPIController(provider: accountProvider)
        //
        let agendaViewModel = AgendaAPIViewModel(
            inputUpdateAgendaAddOrRemoveTask: (
                projectId: projectId,
                addOrRemove: 1,
                agendaId: BehaviorSubject<String>(value: agendaId),
                taskId: BehaviorSubject<String>(value: taskId),
                loginTaps: BehaviorSubject<Void>(value: ()),
                agendaAPIController: agendaController
            )
        )
        //
        
        agendaViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            DispatchQueue.main.async {
//                                self?.view.makeToast(NSLocalizedString("add_task_to_agenda_successfully", comment: ""), duration: 1.5, position: .center, completion: { didTap in
//                                })
//                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self?.makeToast(message:NSLocalizedString("add_task_to_agenda_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                            }
                        }
                        //
                        self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func bindToViewModel(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        func getOnlyIdFromTask(talks : [Task]){
            var ids : [String] = []
            for i in 0..<talks.count{
                let task = talks[i]
                ids.append(task.taskId)
            }
        }
        
        
        let agendaController = AgendaController.sharedAPI
        let taskController = TaskController.sharedAPI
        
        self.agendaViewModel = AgendaViewModel(
            inputUserId: (
                agendaController: agendaController,
                userId: subjectUser.asObservable(),
                projectId: Observable.just(projectId),
                duedate: subjectDate.asObservable(),
                needSubscrible: true
            )
        )
        
        let agendaData = self.agendaViewModel!.agendas!.share(replay: 1)
        let taskInfo =
            agendaData
                .flatMapLatest { agendas -> Observable<[String : Any]> in
                    var taskInfo : [String : Any] = [:]
                    if(agendas.count > 0){
                        let agenda = agendas[0]
                        if(agenda.empty() == true){
                            let userId = agenda.owned.userId
                            //
                            taskInfo = [
                                "isEmpty": true,
                                "clearEmptyData": true,
                                "taskId": [],
                                "userId": userId,
                                "findType": Constant.FindType.findOut,
                            ]
                        }
                        else{
                            var ids : [String] = []
                            for task in agenda.tasks {
                                ids.append(task.taskId)
                            }
                            let userId = agenda.owned.userId
                            //
                            taskInfo = [
                                "taskId": ids,
                                "userId": userId,
                                "findType": Constant.FindType.findOut,
                            ]
                            //
                            if(ids.count == 0){
                                taskInfo["clearEmptyData"] = true
                            }
                        }
                    }
                    return Observable.just(taskInfo)
        }
        
        self.taskViewModel = TaskViewModel(
            inputTaskAgenda: (
                taskInfo: taskInfo,
                taskController: taskController,
                dataSource : Observable.just([]),
                projectId: projectId,
                findOther: true,
                needEmptyData:false
            )
        )
        
        let tasksData = self.taskViewModel!.tasks!.share(replay: 1)
        let otherTasksData = self.taskViewModel!.otherTasks!
            .flatMapLatest{ otherTasks -> Observable<[Task]> in
                if(TaskService.checkHolderTask(tasks: otherTasks)){
                    return Observable.just(otherTasks)
                }
                else{
                    var dataTask : [Task] = []
                    for task in otherTasks{
                        if(task.checkIsTodayAgenda().count == 0){
                            dataTask.append(task)
                        }
                    }
                    return Observable.just(dataTask)
                }
            }
            .share(replay: 1)
        let tasksAndSection = Observable
            .combineLatest(tasksData, otherTasksData)  { (assigneeTask: $0, otherTask: $1) }
            .distinctUntilChanged {
                (old: (assigneeTask: [Task], otherTask: [Task]),
                new: (assigneeTask: [Task], otherTask: [Task])) in
                var distinct = false
                if(old.assigneeTask.count == new.assigneeTask.count && old.otherTask.count == new.otherTask.count){
                    if(TaskService.checkHolderTask(tasks: new.assigneeTask) && TaskService.checkHolderTask(tasks: new.otherTask)){
                        distinct = true
                    }
                }
                return distinct
            }
            .flatMapLatest{ task in
                return Observable.just(
                    [
                        TaskSection(header: NSLocalizedString("my_assigned_tasks", comment: "").uppercased(), items: task.assigneeTask),
                        TaskSection(header: NSLocalizedString("others", comment: "").uppercased(), items: task.otherTask)
                    ])
        }
        .share(replay: 1)
        
        tasksAndSection.subscribe({ [weak self] tasksSection  in
            var numberTask : Int = 0
            if let strongTaskSections = tasksSection.element{
                for tasksSectionItem in strongTaskSections{
                    for task in tasksSectionItem.items{
                        if(task.valid()){
                            numberTask += 1
                        }
                    }
                }
            }
            var title = NSLocalizedString("backlog", comment: "")
            if(numberTask > 0){
                title = title + " (" + String(numberTask) + ")"
            }
            self?.lbTitleScreen?.text = title
            self?.lbTitleHeader?.text = self?.lbTitleScreen!.text
        })
            .disposed(by: disposeBag)
        
        //Datasource
        let dataSource = RxTableViewSectionedReloadDataSource<TaskSection>(
            configureCell: {(_ ,table, indexPath, element) in
                guard let cell = table.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.taskNotActionCellIdentifier, for: indexPath) as? TeamAgendaTaskNotActionItemViewCell else {
                    return TeamAgendaTaskNotActionItemViewCell()
                }
                //
                let itemWidth = table.frame.size.width
                let contentPadding = AppDevice.ScreenComponent.ItemPadding
                cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                cell.selectionStyle = .none
                //
                cell.delegate = self
                
                cell.setValueForCell(task: element)
                
                return cell
        },
            titleForHeaderInSection: { dataSource, sectionIndex in
                return dataSource.sectionModels[sectionIndex].header
        }
        )
        
        self.dataSource = dataSource
        
        tasksAndSection
            .bind(to: tbBacklog.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        
        tbBacklog.rx
            .setDelegate(self)
            .disposed(by : disposeBag)
        
        tbBacklog.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
            }.disposed(by: disposeBag)
    }
    
    func getVisibleCellFromTask(taskId: String) -> TeamAgendaTaskNotActionItemViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : TeamAgendaTaskNotActionItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromTask(taskId: taskId){
            visibleCell.stopProcessing()
        }
    }
    
    func stopProcessingForAllVisibleCell(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                taskCell.stopProcessing()
            }
        }
    }
    
}
