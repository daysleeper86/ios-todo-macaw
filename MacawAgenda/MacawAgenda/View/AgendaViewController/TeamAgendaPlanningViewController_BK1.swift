//
//  TeamAgendaPlanningViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import AnimatableReload
import Material

class TeamAgendaPlanningViewController: BaseViewController {
    //External props
    var viewType : Constant.ViewTypeAddUpdateInsert = .add
    var userId : String = ""
    var agendaId : String = ""
    //Views
    var navigationView : CustomNavigationView?
    var lbTitleScreen : UILabel?
    var lbTitleHeader : UILabel?
    var lbTitleDate : UILabel?
    var tbBacklog =  UITableView(frame: .zero)
    var inputTaskInfoView : InputTaskInfoView?
    var headerAddTaskView : TaskAddNewView?
    var btnDonePlanning = RaisedButton.init(type: .custom)
    //Data
    let taskCellIdentifier : String = "taskCellIdentifier"
    let emptyTaskCellIdentifier : String = "emptyTaskCellIdentifier"
    var processingTask : Task?
    var yScreen_Scroll : CGFloat = 0
    //
    var agenda : Agenda = AgendaService.createEmptyAgenda() {
        didSet {
            agendaId = self.agenda.agendaId
            //
            subjectCurrentTasks.onNext(self.agenda.tasks)
            // Update the view.
            setDataView()
        }
    }
    //ViewModel
    var agendaViewModel : AgendaViewModel?
    var taskViewModel : TaskViewModel?
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    var subjectDate = BehaviorSubject<Date>(value: Date())
    var subjectHasTask = BehaviorSubject<Bool>(value: false)
    //Subject New task Id
    var subjectAgendaID : PublishSubject<String> = PublishSubject()
    var subjectTaskNewID : BehaviorSubject<String> = BehaviorSubject(value: "")
    var subjectTaskNewIDEventStared : PublishSubject<Void> = PublishSubject()
    //
    var subjectCurrentTasks = BehaviorSubject<[Task]>(value: [])
    //
    var uploadingTasks : [Task] = []
    var subjectUploadingTasks = BehaviorSubject<[Task]>(value: [])
    
    //Callback
    var closeCallback: ((_ isClose: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
        bindToViewModel()
        //
        trackOpenPlan()
    }
    
    func trackOpenPlan(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        Tracker.trackOpenPlan(projectId: projectId, userId: userId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Action Button
        let pButtonBacklog : CGFloat = AppDevice.ScreenComponent.RightButtonPadding
        let wButtonBacklog : CGFloat = hContentHeaderView
        let hButtonBacklog = wButtonBacklog
        let xButtonBacklog = navigationView!.frame.size.width - wButtonBacklog - pButtonBacklog
        let yButtonAvatar : CGFloat = yContentHeaderView
        
        let btnBacklog = IconButton.init(type: .custom)
        btnBacklog.frame = CGRect(x: xButtonBacklog, y: yButtonAvatar, width: wButtonBacklog, height: hButtonBacklog)
        btnBacklog.setImage(UIImage(named: "ic_add_backlog"), for: .normal)
        btnBacklog.setImage(UIImage(named: "ic_add_backlog"), for: .highlighted)
        btnBacklog.addTarget(self, action: #selector(backlogAction), for: .touchUpInside)
        navigationView?.addSubview(btnBacklog)
        
        if(self.viewType == .update){
            //Close Button
            let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
            let xClose = pClose
            let wClose = hContentHeaderView
            let hClose = wClose
            let yClose : CGFloat = yContentHeaderView
            
            let btnClose : UIButton = IconButton.init(type: .custom)
            btnClose.frame = CGRect(x: xClose, y: yClose, width: hClose, height: hClose)
            btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
            btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
            navigationView?.addSubview(btnClose)
            
            //Title header
            let pTitleHeader : CGFloat = 8
            let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
            let yTitleHeader : CGFloat = yContentHeaderView
            let wTitleHeader : CGFloat = xButtonBacklog - xTitleHeader - pTitleHeader
            let hTitleHeader : CGFloat = hContentHeaderView
            
            let lbTitleHeader = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
            lbTitleHeader.textAlignment = .left
            lbTitleHeader.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
            lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            lbTitleHeader.text = NSLocalizedString("all_changes_auto_saved", comment: "")
            navigationView?.addSubview(lbTitleHeader)
        }
        else{
            //Title Screen
            let pTitleHeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let xTitleHeader : CGFloat = pTitleHeader
            let yTitleHeader : CGFloat = yContentHeaderView
            let wTitleHeader : CGFloat = xButtonBacklog - xTitleHeader
            let hTitleHeader : CGFloat = hContentHeaderView
            
            lbTitleHeader = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
            lbTitleHeader?.textAlignment = .left
            lbTitleHeader?.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleHeader?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
            navigationView?.titleView(titleView: lbTitleHeader!)
        }
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        var pButtonDoneIfVisible : CGFloat = 0
        if(self.viewType == .add){
            let wButtonDone : CGFloat = 300
            let hButtonDone : CGFloat = AppDevice.ScreenComponent.ButtonHeight
            let pButtonDone : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let yButtonDone : CGFloat = AppDevice.ScreenComponent.Height - hButtonDone - pButtonDone
            let xButtonDone : CGFloat = (AppDevice.ScreenComponent.Width-wButtonDone)/2
            btnDonePlanning.frame = CGRect(x: xButtonDone, y: yButtonDone, width: wButtonDone, height: hButtonDone)
            btnDonePlanning.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
            btnDonePlanning.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
            btnDonePlanning.titleLabel?.textAlignment = .center
            btnDonePlanning.layer.cornerRadius = 4.0
            btnDonePlanning.layer.masksToBounds = true
            btnDonePlanning.backgroundColor = AppColor.NormalColors.BLUE_COLOR
            btnDonePlanning.setTitle(NSLocalizedString("done_planing", comment:"").uppercased(), for: .normal)
            btnDonePlanning.pulseColor = .white
            self.view.addSubview(btnDonePlanning)
            //
            pButtonDoneIfVisible = hButtonDone + (pButtonDone * 2)
        }
        
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = AppDevice.ScreenComponent.Width
        let hTableBacklog : CGFloat = AppDevice.ScreenComponent.Height - yTableBacklog - pButtonDoneIfVisible
        tbBacklog.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbBacklog.register(TeamAgendaTaskNotActionItemViewCell.self, forCellReuseIdentifier: FocusModeEditorViewController.taskNotActionCellIdentifier)
        self.view.addSubview(tbBacklog)
        //
        if(self.viewType == .update){
//            tbBacklog.dataSource = self
//            tbBacklog.delegate =  self
        }
        else{
            
        }
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        
    }
    
    func setHeaderView(){
        let hAddTaskView : CGFloat = AppDevice.TableViewRowSize.AddNewTaskHeight
        let pBottomAddTaskView : CGFloat = 0
        //
        if(headerAddTaskView == nil){
            let xAddTaskView : CGFloat = 0
            let yAddTaskView : CGFloat = 0
            let wAddTaskView : CGFloat = AppDevice.ScreenComponent.Width
            
            headerAddTaskView = TaskAddNewView(frame: CGRect(x: xAddTaskView, y: yAddTaskView, width: wAddTaskView, height: hAddTaskView))
            headerAddTaskView?.setValueForTitle(title: NSLocalizedString("create_a_task_for_yourself", comment: ""))
            //Tap Add Task Event
            let btnAddTaskView = RaisedButton.init(type: .custom)
            btnAddTaskView.frame = CGRect(x: 0, y: 0, width: wAddTaskView, height: hAddTaskView)
            btnAddTaskView.pulseColor = AppColor.MicsColors.LINE_COLOR
            btnAddTaskView.backgroundColor = .clear
            btnAddTaskView.addTarget(self, action: #selector(didTapAddNewTask), for: .touchUpInside)
            headerAddTaskView?.addSubview(btnAddTaskView)
        }
        //
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = tbBacklog.frame.size.width
        
        let tbBacklogHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: 0))
        tbBacklogHeaderView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = AppDevice.ScreenComponent.Width - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 4.0
        
        lbTitleScreen = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen?.textAlignment = .left
        lbTitleScreen?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        tbBacklogHeaderView.addSubview(lbTitleScreen!)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //Date Title
        let pTitleDate : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleDate : CGFloat = pTitleDate
        let yTitleDate : CGFloat = yViewScreen
        let wTitleDate : CGFloat = AppDevice.ScreenComponent.Width - (xTitleDate * 2)
        let hTitleDate : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let pBottomTitleDate : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        
        lbTitleDate = UILabel(frame: CGRect(x: xTitleDate, y: yTitleDate, width: wTitleDate, height: hTitleDate))
        lbTitleDate?.textAlignment = .left
        lbTitleDate?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleDate?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        tbBacklogHeaderView.addSubview(lbTitleDate!)
        
        yViewScreen += hTitleDate
        yViewScreen += pBottomTitleDate
        
        
        //
        if(agenda.tasks.count == 0){
            let xEmptyLabel : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let wEmptyLabel : CGFloat = AppDevice.ScreenComponent.Width - (xEmptyLabel * 2)
            let hEmptyLabel : CGFloat = AppDevice.SubViewFrame.TitleHeight
            let yEmptyLabel : CGFloat = yViewScreen
            let pBottomEmptyLabel : CGFloat = 8
            let lbEmpty = UILabel(frame: CGRect(x: xEmptyLabel, y: yEmptyLabel, width: wEmptyLabel, height: hEmptyLabel))
            lbEmpty.text = NSLocalizedString("add_task_to_your_agenda_to_begin_new_word_day", comment: "")
            lbEmpty.textColor = AppColor.NormalColors.BLACK_COLOR
            lbEmpty.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
            tbBacklogHeaderView.addSubview(lbEmpty)
            
            yViewScreen += hEmptyLabel
            yViewScreen += pBottomEmptyLabel
        }
        
        headerAddTaskView?.frame.origin.y = yViewScreen
        tbBacklogHeaderView.addSubview(headerAddTaskView!)
        
        yViewScreen += hAddTaskView
        yViewScreen += pBottomAddTaskView
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen!.frame.origin.y + lbTitleScreen!.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbBacklogHeaderView.frame.size.height = hHeaderTableView
        tbBacklog.tableHeaderView = tbBacklogHeaderView
    }
    
    func setDataView(){
        setHeaderView()
        //
        tbBacklog.reloadData()
//        AnimatableReload.reload(tableView: tbBacklog, animationDirection: "up")
        //
        setDefaultValue()
    }
    
    
    func setDefaultValue(){
        var titleScreen = ""
        if(self.viewType == .add){
            titleScreen = NSLocalizedString("plan_your_day", comment: "")
        }
        else{
            titleScreen = NSLocalizedString("your_plan_today", comment: "")
//            titleScreen = NSLocalizedString("today", comment: "") + "'s " + NSLocalizedString("agenda", comment: "")
        }
        lbTitleScreen?.text = titleScreen
        lbTitleHeader?.text = lbTitleScreen!.text
        //
        let utcDate = Date().dateForUTC(utcOffset:  AppSettings.sharedSingleton.account?.utcOffset ?? 0)
        lbTitleDate?.text = utcDate.dayMonthYearString()
    }
    
    
    @objc func didTapAddNewTask(){
        let content = ""
        showAddTaskView(content: content,viewType:  viewType)
    }
    
    deinit {
        // Release all resources
        // perform the deinitialization
        removeNotification_Keyboard()
    }
}

extension TeamAgendaPlanningViewController : InputTaskInfoViewDelegate{
    //
    func showAddTaskView(content : String, viewType: Constant.ViewTypeAddUpdateInsert){
        if(inputTaskInfoView == nil){
            inputTaskInfoView = InputTaskInfoView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: AppDevice.ScreenComponent.Height))
            inputTaskInfoView?.delegate = self
        }
        inputTaskInfoView?.tag = viewType.rawValue
        //
        self.view.window?.addSubview(inputTaskInfoView!)
        inputTaskInfoView?.setContent(content:content)
        inputTaskInfoView?.becomeResponder()
    }
    
    func inputTaskInfoViewAdd(_ inputTaskInfoView: InputTaskInfoView, addNew withName: String) {
        let task = Task(taskId: "1", name: withName, dataType: .processing)
        //
        uploadingTasks.insert(task, at: 0)
        subjectUploadingTasks.onNext(uploadingTasks)
        //
        addNewTaskAndUpdateAgenda(name: withName)
    }
    
    func inputTaskInfoViewUpdate(_ inputTaskInfoView: InputTaskInfoView, update withName: String, taskId: String){
        updateTask(taskId: taskId, name: withName)
    }
    
    func inputTaskInfoViewCancel(_ inputTaskInfoView: InputTaskInfoView, tapCancel isSave: Bool) {
        stopProcessingForAllVisibleCell()
    }
}

//Notification event
extension TeamAgendaPlanningViewController {
    func addNotification(){
        addNotification_Keyboard()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double{
                //
                guard let strongInputTaskInfoView = inputTaskInfoView else { return }
                if(strongInputTaskInfoView.isFirstResponder() == true){
                    var bindUpdateTask = ""
                    if(Constant.ViewTypeAddUpdateInsert(rawValue: strongInputTaskInfoView.tag) == .update){
                        bindUpdateTask = processingTask?.taskId ?? ""
                    }
                    strongInputTaskInfoView.showPicker(keyboardBounds: keyboardBounds, duration: NSNumber(value: duration), isDismiss: true, bindAddTask:true,bindAddAgenda: agendaId,bindUpdateTask: bindUpdateTask)
                }
            }
            
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension TeamAgendaPlanningViewController : UITableViewDelegate,SwipeTableViewCellDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var heightRow : CGFloat = 0
        heightRow = AppDevice.TableViewRowSize.TaskHeight
        return heightRow
    }
    
    //
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        let strSpace = "  "
        if(orientation == .left){
            //
            let strTitleEdit = NSLocalizedString("edit_task", comment: "") + strSpace
            let editAction = SwipeAction(style: .default, title: strTitleEdit) {[weak self] action, indexPath in
                // handle action by updating model with deletion
                guard let strongSelf = self else { return }
                if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                    taskCell.startProcessing(color: AppColor.NormalColors.LIGHT_ORANGE_COLOR)
                    //
                    taskCell.hideSwipe(animated: true)
                    //
                    strongSelf.processingTask = task
                    //
                    let content = task.name
                    strongSelf.showAddTaskView(content: content,viewType: Constant.ViewTypeAddUpdateInsert.update)
                }
            }
            // customize the action appearance
            editAction.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
            editAction.textColor = AppColor.NormalColors.WHITE_COLOR
            editAction.backgroundColor = AppColor.NormalColors.LIGHT_ORANGE_COLOR
            editAction.image = UIImage(named: "ic_edit_task")
            editAction.imageTitleOrientation = .horizontal
            
            return [editAction]
        }
        else{
            let strTitleRemove = NSLocalizedString("remove_task", comment: "") + strSpace
            let removeAction = SwipeAction(style: .default, title: strTitleRemove) {[weak self] action, indexPath in
                // handle action by updating model with deletion
                guard let strongSelf = self else { return }
                if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                    taskCell.startProcessing(color: AppColor.NormalColors.LIGHT_RED_COLOR)
                    //
                    taskCell.hideSwipe(animated: true)
                    //
                    strongSelf.processingTask = task
                    strongSelf.removePlanningTask(taskId: task.taskId)
                }
            }
            // customize the action appearance
            removeAction.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
            removeAction.textColor = AppColor.NormalColors.WHITE_COLOR
            removeAction.backgroundColor = AppColor.NormalColors.LIGHT_RED_COLOR
            removeAction.image = UIImage(named: "ic_remove_task")
            removeAction.imageTitleOrientation = .horizontal
            
            return [removeAction]
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        //Minium
        var minimumButtonWidth : CGFloat = 0
        if(orientation == .left){
            minimumButtonWidth = 120
        }
        else{
            minimumButtonWidth = 140
        }
        options.minimumButtonWidth = minimumButtonWidth
        //
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.MicsColors.DISABLE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.NormalColors.WHITE_COLOR)
    }
}

//Binding data to view model
extension TeamAgendaPlanningViewController {
    func trackOpenCreateTask(taskName: String, fromBacklog: Bool){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        if(fromBacklog == true){
            Tracker.trackTaskCreatedInBacklog(projectId: projectId, userId: userId, taskName: taskName)
        }
        else{
            Tracker.trackTaskCreatedInPlan(projectId: projectId, userId: userId, taskName: taskName)
        }
    }
    
    func removePlanningTask(taskId: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let agendaController = AgendaAPIController(provider: accountProvider)
        //
        let agendaViewModel = AgendaAPIViewModel(
            inputUpdateAgendaAddOrRemoveTask: (
                projectId: projectId,
                addOrRemove: 0,
                agendaId: BehaviorSubject<String>(value: agendaId),
                taskId: BehaviorSubject<String>(value: taskId),
                loginTaps: BehaviorSubject<Void>(value: ()),
                agendaAPIController: agendaController
            )
        )
        //
        
        agendaViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            DispatchQueue.main.async {
                                self?.view.makeToast(NSLocalizedString("remove_task_to_agenda_successfully", comment: ""), duration: 1.5, position: .center, completion: { didTap in
                                })
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self?.view.makeToast(NSLocalizedString("remove_task_to_agenda_unsuccessfully", comment: ""), duration: 1.5, position: .center, completion: { didTap in
                                })
                            }
                        }
                        //
                        self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func updateTask(taskId: String, name: String){
        // Update task name
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let viewModelName = TaskAPIViewModel(
            inputUpdateTaskName: (
                taskId: taskId,
                projectId: projectId,
                name: BehaviorSubject<String>(value: name),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: taskController
            )
        )
        
        viewModelName.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
                self?.stopProcessingForAllVisibleCell()
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        //
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 0.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 0.5, position: .center)
                        }
                        //
                        self?.stopProcessingForAllVisibleCell()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func addNewTaskAndUpdateAgenda(name: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let viewModelCreate = TaskAPIViewModel(
            inputCreateTask: (
                projectId: projectId,
                name: BehaviorSubject<String>(value: name),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: taskController
            )
        )
        
        viewModelCreate.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            guard let strongSelf = self else { return }
                            if reponseAPI.data.count > 0{
                                let taskID = reponseAPI.data
                                self?.subjectAgendaID.onNext(strongSelf.agendaId)
                                self?.subjectTaskNewID.onNext(taskID)
                                self?.subjectTaskNewIDEventStared.onNext(())
                                //
                                strongSelf.trackOpenCreateTask(taskName: name, fromBacklog: false)
                            }
                        }
                        else{
                            var message = NSLocalizedString("add_task_unsuccessfully", comment: "")
                            if(reponseAPI.code != 0){
                                if(reponseAPI.message.count > 0){
                                    message = reponseAPI.message
                                }
                            }
                            self?.view.makeToast(message, duration: 0.5, position: .center)
                        }
                        //
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
        //
        let agendaController = AgendaAPIController(provider: accountProvider)
        //
        let agendaViewModel = AgendaAPIViewModel(
            inputUpdateAgendaAddOrRemoveTask: (
                projectId: projectId,
                addOrRemove: 1,
                agendaId: subjectAgendaID.asObservable(),
                taskId: subjectTaskNewID.asObservable(),
                loginTaps: subjectTaskNewIDEventStared.asObservable(),
                agendaAPIController: agendaController
            )
        )
        
        agendaViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                    self?.subjectTaskNewID.onNext("")
                    self?.stopProcessingNewTaskForAllVisibleCell()
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                self?.view.makeToast(NSLocalizedString("add_task_to_agenda_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                            }
                        }
                        //
                    }
                    self?.subjectTaskNewID.onNext("")
                    self?.stopProcessingNewTaskForAllVisibleCell()
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func bindToViewModel(){
        var tasksData : Observable<[Task]>?
        if(self.viewType == .add){
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            
            func getOnlyIdFromTask(talks : [Task]){
                var ids : [String] = []
                for i in 0..<talks.count{
                    let task = talks[i]
                    ids.append(task.taskId)
                }
            }
            
            let agendaController = AgendaController.sharedAPI
            let taskController = TaskController.sharedAPI
            
            self.agendaViewModel = AgendaViewModel(
                inputUserId: (
                    agendaController: agendaController,
                    userId: subjectUser.asObservable(),
                    projectId:projectId,
                    duedate: subjectDate.asObservable(),
                    needSubscrible: true
                )
            )
            
            let taskInfo =
                self.agendaViewModel!.agendas!
                    .flatMapLatest { agendas -> Observable<[String : Any]> in
                        var taskInfo : [String : Any] = [:]
                        if(agendas.count > 0){
                            let agenda = agendas[0]
                            if(agenda.empty() == true){
                                taskInfo = [
                                    "isEmpty": true,
                                    "clearEmptyData": true,
                                    "taskId": [],
                                    "userId": userId,
                                    "findType": Constant.FindType.findIn,
                                ]
                            }
                            else{
                                var ids : [String] = []
                                for task in agenda.tasks {
                                    ids.append(task.taskId)
                                }
                                let userId = agenda.owned.userId
                                //
                                taskInfo = [
                                    "taskId": ids,
                                    "userId": userId,
                                    "findType": Constant.FindType.findIn,
                                ]
                                //
                                if(ids.count == 0){
                                    taskInfo["isEmpty"] = true
                                    taskInfo["clearEmptyData"] = true
                                }
                            }
                        }
                        return Observable.just(taskInfo)
            }
            
            
            self.taskViewModel = TaskViewModel(
                inputTaskAgenda: (
                    taskInfo: taskInfo,
                    taskController: taskController,
                    dataSource : subjectUploadingTasks.asObservable(),
                    projectId: projectId,
                    findOther: false,
                    needEmptyData:true
                )
            )
            
            tasksData = self.taskViewModel?.tasks?.share(replay: 1)
            
            let token = AppSettings.sharedSingleton.account?.token ?? ""
            let authPlugin = AccessTokenPlugin { token }
            let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
            let agendaAPIController = AgendaAPIController(provider: accountProvider)
            //
            let observableTap = btnDonePlanning.rx.tap.asObservable()
            let agendaAPIViewModel = AgendaAPIViewModel(
                inputUpdateAgendaDonePlanning: (
                    projectId: projectId,
                    agendaId: agendaId,
                    hasTask: subjectHasTask.asObservable(),
                    status: 1,
                    loginTaps: observableTap,
                    agendaAPIController: agendaAPIController
                )
            )
            
            agendaAPIViewModel.processEnabled?.subscribe(onNext: { [weak self] valid  in
                self?.btnDonePlanning.isEnabled = valid
                if(valid == true){
                    self?.btnDonePlanning.backgroundColor = AppColor.NormalColors.BLUE_COLOR
                    self?.btnDonePlanning.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
                }
                else{
                    self?.btnDonePlanning.backgroundColor = AppColor.MicsColors.DISABLE_COLOR
                    self?.btnDonePlanning.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
                }
            })
                .disposed(by: disposeBag)
            
            agendaAPIViewModel.signingIn?
                .subscribe(onNext: {[weak self] loading  in
                    if(loading == true){
                        self?.view.isUserInteractionEnabled = false
                    }
                    else{
                        self?.view.isUserInteractionEnabled = true
                    }
                })
                .disposed(by: disposeBag)
            
            agendaAPIViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
                switch loginResult {
                case .failed(let output):
                    let appDelegate = AppDelegate().sharedInstance()
                    if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                        self?.view.makeToast(output.message, duration: 3.0, position: .center)
                        //
                        self?.view.isUserInteractionEnabled = true
                    }
                    
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                            if(reponseAPI.code == 200){
                                DispatchQueue.main.async {
                                    self?.doneAction()
                                }
                            }
                            else{
                                let message = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                                self?.view.makeToast(message, duration: 3.0, position: .center)
                            }
                            //
                            self?.view.isUserInteractionEnabled = true
                        }
                    }
                }
            }).disposed(by: disposeBag)
            
            //Get today task of today agenda
            let todayAgendaViewModel = AgendaViewModel(
                inputProjectInfo: (
                    agendaController: agendaController,
                    projectId:projectId,
                    duedate: subjectDate.asObservable()
                )
            )
            
            todayAgendaViewModel.agendas?
                .subscribe({ agendas  in
                    guard let strongAgendas = agendas.element else { return }
                    var agendaTaskInfo : [[String:Any]] = []
                    var needSaveTodayAgendaTask : Bool = false
                    for agenda in strongAgendas {
                        if(agenda.valid() == true && agenda.date.isToday() == true && agenda.tasks.count > 0){
                            //
                            needSaveTodayAgendaTask = true
                            //
                            var agendaTaskIds : [String:Any] = [:]
                            var taskIds : [String] = []
                            for task in agenda.tasks {
                                taskIds.append(task.taskId)
                            }
                            agendaTaskIds[Constant.keyId] = agenda.agendaId
                            agendaTaskIds[Constant.keyValue] = taskIds
                            //
                            agendaTaskInfo.append(agendaTaskIds)
                        }
                    }
                    //
                    if(needSaveTodayAgendaTask == true){
                        AppSettings.sharedSingleton.saveTaskAgendaIds(agendaTaskInfo)
                    }
                })
                .disposed(by: disposeBag)
            
            //
            tbBacklog.rx.contentOffset.subscribe { [weak self] in
                let yOffset = $0.element?.y ?? 0
                let yScreen_Scroll = self?.yScreen_Scroll ?? 0
                var isShow : Bool = false
                if(yOffset > yScreen_Scroll){
                    isShow = true
                }
                self?.navigationView?.showHideTitleNavigation(isShow: isShow)
                }.disposed(by: disposeBag)
        }
        else{
            tasksData =
                Observable.combineLatest(subjectUploadingTasks.asObservable(), subjectCurrentTasks.asObservable()) { (dataSource: $0, dataDest: $1) }
                    .distinctUntilChanged { (old: (dataSource: [Task], dataDest: [Task]),
                        new: (dataSource: [Task], dataDest: [Task])) in
                        return (TaskService.checkHolderTask(tasks: old.dataDest) == true
                            && TaskService.checkHolderTask(tasks: new.dataDest) ==  true)
                    }
                    .flatMapLatest { dataSource, dataDest -> Observable<[Task]> in
                        if(dataSource.count == 0){
                            return Observable.just(dataDest)
                        }
                        else{
                            var resultSource : [Task]
                            if(TaskService.checkEmptyTask(tasks: dataDest) == false){
                                resultSource = dataSource + dataDest
                            }
                            else{
                                resultSource = dataSource
                            }
                            return Observable.just(resultSource)
                        }
            }
        }
        
        tasksData?.bind(to: tbBacklog.rx.items){[weak self] (tableView, index, element) -> UITableViewCell in
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewController.taskNotActionCellIdentifier, for: IndexPath.init(row: index, section: 0)) as? TeamAgendaTaskNotActionItemViewCell else {
                return TeamAgendaTaskNotActionItemViewCell()
            }
            //
            let itemWidth = tableView.frame.size.width
            let contentPadding = AppDevice.ScreenComponent.ItemPadding
            cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
            cell.selectionStyle = .none
            //
            cell.delegate = self
            //
            let task = element
            cell.setValueForCell(task: task)
            return cell
            
            }.disposed(by: disposeBag)
        
        tbBacklog.rx
            .setDelegate(self)
            .disposed(by : disposeBag)
        
        tbBacklog.rx
            .modelSelected(Task.self)
            .subscribe(onNext:  { value in
                let taskDetailView = TaskDetailViewController(nibName: nil, bundle: nil)
                taskDetailView.taskDetailData = value
                self.navigationController?.pushViewController(taskDetailView, animated: true)
            })
            .disposed(by: disposeBag)
        
        tasksData?
            .subscribe(onNext:  {[weak self] tasks in
                var hasTask : Bool = false
                if(TaskService.checkHolderTask(tasks: tasks) == false){
                    if(tasks.count > 0){
                        hasTask = true
                    }
                }
                self?.subjectHasTask.onNext(hasTask)
            })
            .disposed(by: disposeBag)
    }
    
    func getVisibleCellFromTask(taskId: String) -> TeamAgendaTaskNotActionItemViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : TeamAgendaTaskNotActionItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromTask(taskId: taskId){
            visibleCell.stopProcessing()
        }
    }
    
    func stopProcessingForAllVisibleCell(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                taskCell.stopProcessing()
            }
        }
    }
    
    func stopProcessingNewTaskForAllVisibleCell(){
        uploadingTasks.removeAll()
        subjectUploadingTasks.onNext(uploadingTasks)
    }
}

//Action - Goto other views
extension TeamAgendaPlanningViewController{
    @objc func backlogAction(sender:UIButton){
        let heroId = "cell\(3)"
        sender.hero.id = heroId
        //
        let planningBacklogView = TeamAgendaPlanningBacklogViewController(nibName: nil, bundle: nil)
        planningBacklogView.userId = userId
        planningBacklogView.agendaId = agendaId
        planningBacklogView.hero.isEnabled = true
        planningBacklogView.tbBacklog.hero.id = heroId
        //
        present(planningBacklogView, animated: true, completion: nil)
        
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: {
            
        })
        
    }
    
    @objc func doneAction(){
        self.dismiss(animated: true, completion: {
            self.closeCallback?(true)
            //
            let appDelegate : AppDelegate = AppDelegate().sharedInstance()
            appDelegate.setupTeamAgendaView()
        })
    }
}
