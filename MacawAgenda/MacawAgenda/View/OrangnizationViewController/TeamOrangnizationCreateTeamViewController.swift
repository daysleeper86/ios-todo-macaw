//
//  TeamOrangnizationCreateTeamViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/3/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material

protocol TeamOrangnizationCreateTeamViewControllerDelegate : NSObject {
    func teamOrangnizationCreateTeamViewControllerSave(_ teamOrangnizationCreateTeamViewController: TeamOrangnizationCreateTeamViewController, doneWith user: User, andInfo name: String)
}

class TeamOrangnizationCreateTeamViewController: BaseViewController {
    //Views
    private var tbInfo : UITableView = UITableView(frame: .zero)
    private var btnSave : UIButton?
    private var lbNameLeader : UILabel?
    private var imvAvatarLeader : UIImageView?
    private var tfTeamName : LPlaceholderTextView = LPlaceholderTextView(frame: .zero)
    
    //External Data & props
    var leader : User = UserService.createEmptyUser(){
        didSet {
            updateTeamLeader(user: leader)
        }
    }
    var arrayUsers : [User] = [] {
        didSet {
            // Update the view.
            if(arrayUsers.count > 0){
                if(leader.dataType == .empty){
                    leader = arrayUsers[0]
                }
            }
            setDataView()
        }
    }
    
    //Datas
    private let arraySettings : [[String : Any]] = [
    [Constant.keyId:Constant.ProfileMenu.kProfileMenuTeamName,Constant.keyValue:NSLocalizedString("team_name", comment: "").uppercased()],
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuTeamLeader,Constant.keyValue:NSLocalizedString("team_leader", comment: "")],
    ]
    
    //Events
    weak var delegate: TeamOrangnizationCreateTeamViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    func setDataView(){
        tbInfo.reloadData()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnSave = UIButton.init(type: .custom)
        btnSave?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnSave?.setTitle(NSLocalizedString("create", comment:"").uppercased(), for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnSave?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnSave?.titleLabel?.textAlignment = .right
        btnSave?.contentHorizontalAlignment = .right
        btnSave?.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        navigationView.addSubview(btnSave!)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("create_team", comment: "")
        navigationView.titleView(titleView: lbTitleHeader)
        navigationView.showHideTitleNavigation(isShow: true)
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = AppDevice.ScreenComponent.Height - yTableMember
        tbInfo.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbInfo.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbInfo.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbInfo.dataSource = self
        tbInfo.delegate = self
        tbInfo.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbInfo.register(MemberItemViewCell.self, forCellReuseIdentifier: InviteMemberViewController.memberCellIdentifier)
        self.view.addSubview(tbInfo)
        
        let wHeaderTableView : CGFloat = wTableMember
        let hHeaderTableView : CGFloat = 36
        let tbMemberHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: hHeaderTableView))
        tbMemberHeaderView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tbInfo.tableHeaderView = tbMemberHeaderView
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveAction(){
        self.delegate?.teamOrangnizationCreateTeamViewControllerSave(self, doneWith: leader, andInfo: tfTeamName.text)
        self.closeAction()
    }
    
    func bindToViewModel(){
        //
        let validatedName = tfTeamName.rx.text.orEmpty.asObservable()
            .map { name in
                return Util.validateProjectName(name)
        }
        validatedName
            .subscribe(onNext: { [weak self] valid  in
                self?.btnSave?.isUserInteractionEnabled = valid.isValid
                if(valid.isValid == true){
                    self?.btnSave?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
                }
                else{
                    self?.btnSave?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
                }
            })
            .disposed(by: disposeBag)
        
        
        //Tap Background Event
        let tapBackground = UITapGestureRecognizer()
        tapBackground.rx.event
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
        view.addGestureRecognizer(tapBackground)
    }
    
    func updateTeamLeader(user: User){
        imvAvatarLeader?.loadAvatarForUser(user: leader)
        lbNameLeader?.text = user.name
    }

}

extension TeamOrangnizationCreateTeamViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let cellHeight : CGFloat = AppDevice.TableViewRowSize.InputFieldHeight
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        //
        let profile = arraySettings[indexPath.row]
        let strName = profile[Constant.keyValue] as? String ?? ""
        let key = profile[Constant.keyId] as? Constant.ProfileMenu ?? .kProfileMenuName
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = tableView.frame.size.width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.InputFieldHeight
        
        let backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        cell.addSubview(backgroundViewCell)
        
        var yCellScreen : CGFloat = 24
        
        //Title Screen
        let xTitleName : CGFloat = 0
        let yTitleName : CGFloat = yCellScreen
        let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
        let hTitleName : CGFloat = 20
        let hBottomTitleName : CGFloat = 4.0
        
        let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName.textAlignment = .left
        lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 12)
        lbTitleName.text = strName.uppercased()
        backgroundViewCell.addSubview(lbTitleName)
        
        yCellScreen += hTitleName
        yCellScreen += hBottomTitleName
        
        //Button option
        let xViewSelection : CGFloat = 0
        let yViewSelection : CGFloat = yCellScreen
        let wViewSelection : CGFloat = wBackgroundView - (xTitleName * 2)
        let hViewSelection : CGFloat = 48
        let hViewButtonSelection : CGFloat = 0.0
        
        let viewSelection = UIView(frame: CGRect(x: xViewSelection, y: yViewSelection, width: wViewSelection, height: hViewSelection))
        viewSelection.layer.cornerRadius = 4.0
        viewSelection.layer.borderWidth = 1.0
        viewSelection.layer.borderColor = AppColor.MicsColors.LINE_VIEW_NORMAL.cgColor
        backgroundViewCell.addSubview(viewSelection)
        
        yCellScreen += hViewSelection
        yCellScreen += hViewButtonSelection
        
        if(key == .kProfileMenuTeamLeader){
            let pImageSelection : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let hImageSelection : CGFloat = 24
            let wImageSelection : CGFloat = hImageSelection
            let xImageSelection : CGFloat = viewSelection.frame.size.width - pImageSelection - wImageSelection
            let yImageSelection : CGFloat = (viewSelection.frame.size.height - hImageSelection) / 2
            
            let imvSelection = UIImageView(frame: CGRect(x: xImageSelection, y: yImageSelection, width: wImageSelection, height: hImageSelection))
            imvSelection.image = UIImage(named: "ic_select_option_next")
            viewSelection.addSubview(imvSelection)
            
            let pImvAvatarLeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let wImvAvatarLeader : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
            let hImvAvatarLeader : CGFloat = wImvAvatarLeader
            let xImvAvatarLeader : CGFloat = pImvAvatarLeader
            let yImvAvatarLeader : CGFloat = (hViewSelection-hImvAvatarLeader)/2
            
            imvAvatarLeader = UIImageView(frame: CGRect(x: xImvAvatarLeader, y: yImvAvatarLeader, width: wImvAvatarLeader, height: hImvAvatarLeader))
            imvAvatarLeader?.layer.cornerRadius = hImvAvatarLeader/2
            imvAvatarLeader?.layer.masksToBounds = true
            viewSelection.addSubview(imvAvatarLeader!)
            imvAvatarLeader?.loadAvatarForUser(user: leader)
            
            let pLabelLeaderName : CGFloat = AppDevice.ScreenComponent.ItemPadding / 2
            let hLabelLeaderName : CGFloat = 32
            let xLabelLeaderName : CGFloat = xImvAvatarLeader + wImvAvatarLeader + pLabelLeaderName
            let yLabelLeaderName : CGFloat = (viewSelection.frame.size.height - hLabelLeaderName) / 2
            let wLabelLeaderName : CGFloat = xImageSelection - pLabelLeaderName - xLabelLeaderName
            
            lbNameLeader = UILabel(frame: CGRect(x: xLabelLeaderName, y: yLabelLeaderName, width: wLabelLeaderName, height: hLabelLeaderName))
            lbNameLeader?.accessibilityIdentifier = AccessiblityId.LB_TEAMTYPE
            lbNameLeader?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            lbNameLeader?.textColor = AppColor.NormalColors.BLACK_COLOR
            viewSelection.addSubview(lbNameLeader!)
            
            let strTeamType = leader.name
            lbNameLeader?.text = strTeamType
            
            //Button option
            let xButtonSelection : CGFloat = 0
            let yButtonSelection : CGFloat = 0
            let wButtonSelection : CGFloat = viewSelection.frame.size.width
            let hButtonSelection : CGFloat = viewSelection.frame.size.height
            
            let btnSelection = RaisedButton.init(type: .custom)
            btnSelection.accessibilityIdentifier = AccessiblityId.BTN_SELECTTEAMTYPE
            btnSelection.backgroundColor = .clear
            btnSelection.pulseColor = AppColor.MicsColors.LINE_COLOR
            btnSelection.frame = CGRect(x: xButtonSelection, y: yButtonSelection, width: wButtonSelection, height: hButtonSelection)
            btnSelection.addTarget(self, action: #selector(selectionTeamLeaderAction), for: .touchUpInside)
            viewSelection.addSubview(btnSelection)
        }
        else if(key == .kProfileMenuTeamName){
            let hTextFieldName : CGFloat = 32
            let xTextFieldName : CGFloat = 10
            let yTextFieldName : CGFloat = (viewSelection.frame.size.height - hTextFieldName) / 2
            let wTextFieldName : CGFloat = viewSelection.frame.size.width - (xTextFieldName * 2)
            
            tfTeamName.frame = CGRect(x: xTextFieldName, y: yTextFieldName, width: wTextFieldName, height: hTextFieldName)
            tfTeamName.delegate = self
            tfTeamName.accessibilityIdentifier = AccessiblityId.TF_TEAMNAME
            tfTeamName.returnKeyType = .done
            tfTeamName.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            tfTeamName.textColor = AppColor.NormalColors.BLACK_COLOR
            tfTeamName.textContainer.maximumNumberOfLines = 1
            tfTeamName.textContainer.lineBreakMode = .byTruncatingTail
            tfTeamName.placeholderText = NSLocalizedString("enter_your_team_name", comment: "")
            tfTeamName.placeholderColor = UIColor(hexString: "#b2bac0")
            viewSelection.addSubview(tfTeamName)
            //
        }
        return cell
    }
    
    
}

extension TeamOrangnizationCreateTeamViewController : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


extension TeamOrangnizationCreateTeamViewController : TeamOrangnizationMemberSelectedViewControllerDelegate{
    @objc func selectionTeamLeaderAction(){
        let teamOrangnizationSelected  = TeamOrangnizationMemberSelectedViewController(nibName: nil, bundle: nil)
        teamOrangnizationSelected.arrayUsers = self.arrayUsers
        var selected : Int = 0
        for i in 0 ..< self.arrayUsers.count{
            let user = self.arrayUsers[i]
            if(user.userId == leader.userId){
                selected = i
                break
            }
        }
        teamOrangnizationSelected.selected = selected
        teamOrangnizationSelected.delegate = self
        let overlayController = DTOverlayController(viewController: teamOrangnizationSelected,viewNotPan: [teamOrangnizationSelected.tbTeam])
        overlayController.overlayHeight = .dynamic(AppDevice.ScreenComponent.OverlayHeight1)
        overlayController.handleVerticalSpace = AppDevice.ScreenComponent.OverlayHandler
        overlayController.isHandleHidden = true
        self.present(overlayController, animated: true, completion: nil)
    }
    
    //Delegate
    func teamOrangnizationMemberSelectedViewControllerChange(_ teamOrangnizationMemberSelectedViewController: TeamOrangnizationMemberSelectedViewController, doneWith user: User){
        var shouldUpdated : Bool = false
        let userId = user.userId
        if(leader.userId != userId){
            shouldUpdated = true
        }
        //
        if(shouldUpdated == true){
            leader = user
        }
    }
}
