//
//  TeamOrangnizationTeamViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material
import RxSwift

protocol TeamOrangnizationTeamViewControllerDelegate : NSObject {
    func teamOrangnizationTeamViewControllerSelectItem(_ teamOrangnizationTeamViewController: TeamOrangnizationTeamViewController, doneWith project: Project, selectAt index:Int)
}

class TeamOrangnizationTeamViewController: BaseViewController {
    //
    var tbTeam = UITableView(frame: .zero)
    var hViewPager : CGFloat = 0
    var navigationView : CustomNavigationView?
    //Data
    var projectId : String = AppSettings.sharedSingleton.project?.projectId ?? ""
    var arrayProjects : [Project] = [] {
        didSet {
            // Update the view.
            setDataView()
        }
    }
    
    //Events
    weak var delegate: TeamOrangnizationTeamViewControllerDelegate?
    
    //ViewModel
    var projectViewModel : ProjectViewModel?
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
    }
    
    func setDataView(){
        tbTeam.reloadData()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = 0
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView?.addSubview(btnClose)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView!.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleHeader.text = NSLocalizedString("change_team", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func createContentView(yHeader :  CGFloat){
        //
        var hBackgroundView : CGFloat = 0
        if(hViewPager > 0){
            hBackgroundView = hViewPager
        }
        else{
            hBackgroundView = AppDevice.ScreenComponent.Height - yHeader
        }
        
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = hBackgroundView - yTableMember
        tbTeam.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbTeam.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeam.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeam.dataSource = self
        tbTeam.delegate = self
        tbTeam.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbTeam.register(TeamOrangnizationTeamItemViewCell.self, forCellReuseIdentifier: TeamOrangnizationTeamSelectedViewController.teamOrangnizationItemViewCell)
        self.view.addSubview(tbTeam)
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
//        let yScreen : CGFloat = createHeaderView()
        let yScreen : CGFloat = 0
        createContentView(yHeader: yScreen)
    }
   
    @objc func manageAction(sender: UIButton){
        let index = sender.tag
        let project = arrayProjects[index]
        
        DispatchQueue.main.async {
            let actionSheet = UIAlertController(title: nil, message: project.name, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("edit_team",comment:""), style: .default, handler: {[weak self] (action) in
                //
                
            }))
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel",comment:""), style: .cancel, handler: {[weak self] (action) in
                
            }))
            self.present(actionSheet, animated: true)
        }
    }
}

extension TeamOrangnizationTeamViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayProjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamOrangnizationTeamSelectedViewController.teamOrangnizationItemViewCell, for: indexPath) as? TeamOrangnizationTeamItemViewCell else {
            return TeamOrangnizationTeamItemViewCell()
        }
        let element = self.arrayProjects[indexPath.row]
        //
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        cell.setValueForCell(project: element, selected: false, manage: true)
        //
        cell.btnManage?.tag = indexPath.row
        cell.btnManage?.addTarget(self, action: #selector(manageAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let heightRow : CGFloat = AppDevice.ScreenComponent.ItemPadding + AppDevice.SubViewFrame.AvatarIconMediumWidth + AppDevice.ScreenComponent.ItemPadding
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //
        let element = self.arrayProjects[indexPath.row]
        //
        self.delegate?.teamOrangnizationTeamViewControllerSelectItem(self, doneWith: element, selectAt: indexPath.row)
    }

}

