//
//  TeamOrangnizationMemberSelectedViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material
import RxSwift

protocol TeamOrangnizationMemberSelectedViewControllerDelegate : NSObject {
    func teamOrangnizationMemberSelectedViewControllerChange(_ teamOrangnizationMemberSelectedViewController: TeamOrangnizationMemberSelectedViewController, doneWith user: User)
}

class TeamOrangnizationMemberSelectedViewController: BaseViewController {
    //External props
    var arrayUsers : [User] = []
    var selected : Int = 0
    
    //Views
    var tbTeam = UITableView(frame: .zero)
    var navigationView : CustomNavigationView?
    
    //Data
    static let teamOrangnizationItemViewCell : String = "teamOrangnizationItemViewCell"
    var projectId : String = AppSettings.sharedSingleton.project?.projectId ?? ""
    
    //ViewModel
    var projectViewModel : ProjectViewModel?
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    
    //Events
    weak var delegate: TeamOrangnizationMemberSelectedViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = 0
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView?.addSubview(btnClose)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView!.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleHeader.text = NSLocalizedString("choose_team_leader", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func createContentView(yHeader :  CGFloat){
        let hBackgroundView : CGFloat = AppDevice.ScreenComponent.Height * AppDevice.ScreenComponent.OverlayHeight1 - AppDevice.ScreenComponent.OverlayHandler
        
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = hBackgroundView - yTableMember
        tbTeam.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbTeam.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeam.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeam.dataSource = self
        tbTeam.delegate = self
        tbTeam.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbTeam.register(MemberAssigneeItemViewCell.self, forCellReuseIdentifier: TaskAssigneeSelectedViewController.memberAssigneeCellIdentifier)
        self.view.addSubview(tbTeam)
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
}

extension TeamOrangnizationMemberSelectedViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  TaskAssigneeSelectedViewController.memberAssigneeCellIdentifier, for: indexPath) as? MemberAssigneeItemViewCell else {
            return MemberAssigneeItemViewCell()
        }
        let element = arrayUsers[indexPath.row]
        //
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        var selected : Bool = false
        if(self.selected == indexPath.row){
            selected = true
        }
        cell.setValueForCell(user: element,selected: selected,search: "")
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let heightRow : CGFloat = AppDevice.TableViewRowSize.MemberHeight
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //
        let element = self.arrayUsers[indexPath.row]
        //
        if(indexPath.row != selected){
            self.closeAction()
            //
            self.delegate?.teamOrangnizationMemberSelectedViewControllerChange(self, doneWith: element)
        }
    }
    
}

