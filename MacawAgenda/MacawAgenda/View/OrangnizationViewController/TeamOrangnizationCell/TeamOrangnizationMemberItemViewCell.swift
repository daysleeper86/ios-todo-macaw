//
//  TeamOrangnizationMemberItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TeamOrangnizationMemberItemViewCell: UITableViewCell {
    //Views
    private var backgroundViewCell : UIView?
    private var imvAvatar : FAShimmerImageView?
    private var lbTitleName : FAShimmerLabelView?
    private var lbTitleSubName : FAShimmerLabelView?
    //Data
    private var user = UserService.createEmptyUser()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        initView()
    }
    
    func initView(){
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        //
        let paddingRow : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let heightRow : CGFloat = paddingRow + wAvatar + paddingRow
        // Background View
        let xBackgroundView : CGFloat = 8
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = heightRow
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        backgroundViewCell?.layer.cornerRadius = 4.0
        backgroundViewCell?.layer.masksToBounds = true
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = pAvatar
        let yAvatar : CGFloat = (hBackgroundView-hAvatar)/2
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = hAvatar / 2
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.image = LocalImage.UserPlaceHolder
        backgroundViewCell?.addSubview(imvAvatar!)
        
        //Image selected
        let wImageNext : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
        let hImageNext : CGFloat = wImageNext
        let xImageNext : CGFloat = wBackgroundView - wImageNext
        let yImageNext : CGFloat = (hBackgroundView-hImageNext)/2
        
        let imvSelected = FAShimmerImageView(frame: CGRect(x: xImageNext, y: yImageNext, width: wImageNext, height: hImageNext))
        imvSelected.image = UIImage(named:"ic_next_row")
        backgroundViewCell?.addSubview(imvSelected)
        
        // Title name
        let pTitleName : CGFloat = pAvatar
        let hTitleName : CGFloat = AppDevice.SubViewFrame.LargeTitleHeight
        let xTitleName : CGFloat = xAvatar + wAvatar + pAvatar
//        let yTitleName : CGFloat = (hBackgroundView-hTitleName)/2
        let yTitleName : CGFloat = yAvatar - 4
        let wTitleName : CGFloat = xImageNext - xTitleName - pTitleName
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        backgroundViewCell?.addSubview(lbTitleName!)
        
        // Title name
        let hTitleSubName : CGFloat = hTitleName
        let xTitleSubName : CGFloat = xTitleName
        let yTitleSubName : CGFloat = yTitleName + hTitleName
        let wTitleSubName : CGFloat = wTitleName
        
        lbTitleSubName = FAShimmerLabelView(frame: CGRect(x: xTitleSubName, y: yTitleSubName, width: wTitleSubName, height: hTitleSubName))
        lbTitleSubName?.textAlignment = .left
        lbTitleSubName?.numberOfLines = 0
        lbTitleSubName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleSubName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        backgroundViewCell?.addSubview(lbTitleSubName!)
        
        let hLineView : CGFloat = 0.6
        let xlineView : CGFloat = xTitleName
        let ylineView : CGFloat = heightRow - hLineView
        let wlineView : CGFloat = wBackgroundView - xlineView
        
        let lineView = UILabel(frame: CGRect(x: xlineView, y: ylineView, width: wlineView, height: hLineView))
        lineView.backgroundColor = AppColor.MicsColors.LINE_COLOR
        backgroundViewCell?.addSubview(lineView)
    }
    
    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        imvAvatar?.startShimmering()
        lbTitleName?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopShimmering()

            }
        }
    }
    
    func setBackGroundColor(_ color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    func setValueForCell(user: User){
        self.user = user
        //
        if(self.user.dataType == .normal){
            stopShimmering_Cell()
            //
            stateForAvatar(self.user)
            //
            stateForTitleName(self.user.name,info: self.user.info)
        }
        else{
            startShimmering_Cell()
        }
    }
    
    func stateForTitleName(_ name : String, info: String){
        lbTitleName?.text = name
        lbTitleSubName?.text = info
    }
    
    func stateForAvatar(_ user : User){
        //Cancel current request
        imvAvatar?.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID == Constant.keyHolderUnassignedId){
            imvAvatar?.image = UIImage(named: "ic_no_assignee")
        }
        else{
            let strUserID = user.userId
            if(strUserID.count > 0){
                imvAvatar?.sd_cancelCurrentImageLoad()
                //
                let strURL = Util.parseURLAvatarImageFireStore(strUserID)
                imvAvatar?.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                    guard let strongSelf = self else { return }
                    if let imageValue = image{
                        //Current userId
                        let strCurrentUserID = strongSelf.user.userId
                        if(strCurrentUserID == strUserID){
                            if(imageValue.size.width > 0){
                                strongSelf.imvAvatar?.backgroundColor = .clear
                                strongSelf.imvAvatar?.removeLabelTextIfNeed()
                            }
                            else{
                                let strUserName = strongSelf.user.name
                                strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                            }
                        }
                    }
                    else{
                        //Current userId
                        let strCurrentUserID = strongSelf.user.userId
                        if(strCurrentUserID == strUserID){
                            let strUserName = strongSelf.user.name
                            strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                })
            }
            else{
                let strUserName = user.name
                imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
            }
        }
    }
}
