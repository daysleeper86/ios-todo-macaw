//
//  SettingItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material

class TeamOrangnizationProfileCell: UITableViewCell {
    
    //Views
    private var backgroundViewCell : UIView?
    
    private var backgroundOrangnizationView : UIView?
    private var imvOrangnizationAvatar : UIImageView?
    private var lbOrangnizationName : UILabel?
    private var lbOrangnizationPlan : UILabel?
    var btnManage : IconButton?
    var btnHeaderView : RaisedButton?
    private var backgroundTeamView : UIView?
    private var imvTeamAvatar : UIImageView?
    private var lbTeamName : UILabel?
    var btnChange : UIButton?
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
    }
    
    func initView(orangnization: Orangnization?, project: Project?){
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.SettingHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.addSubview(backgroundViewCell!)
        
        let xlineView1 : CGFloat = 0
        let ylineView1 : CGFloat = AppDevice.SubViewFrame.OrangnizationCellPadding
        let wlineView1 : CGFloat = backgroundViewCell!.frame.size.width - (xlineView1 * 2)
        let hLineView1 : CGFloat = 1.0
        let pBottomLineView1 : CGFloat = AppDevice.SubViewFrame.OrangnizationCellPadding
        //
        var yScreenView : CGFloat = ylineView1
        
        
        let lineView1 = UILabel(frame: CGRect(x: xlineView1, y: ylineView1, width: wlineView1, height: hLineView1))
        lineView1.addDashedBorder()
        backgroundViewCell?.addSubview(lineView1)
        
        yScreenView += lineView1.frame.size.height
        yScreenView += pBottomLineView1
        
        
        if(orangnization != nil){
            if(orangnization!.orangnizationId.count > 0){
            let xbackgroundOrangnizationView : CGFloat = xlineView1
            let ybackgroundOrangnizationView : CGFloat = yScreenView
            let wbackgroundOrangnizationView : CGFloat = wlineView1
            let hbackgroundOrangnizationView : CGFloat = AppDevice.SubViewFrame.OrangnizationIconWidth
            let pBottombackgroundOrangnizationView : CGFloat = AppDevice.ScreenComponent.ItemPadding
            //
            backgroundOrangnizationView = UIView(frame: CGRect(x: xbackgroundOrangnizationView, y: ybackgroundOrangnizationView, width: wbackgroundOrangnizationView, height: hbackgroundOrangnizationView))
            backgroundOrangnizationView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            backgroundViewCell?.addSubview(backgroundOrangnizationView!)
            
            yScreenView += backgroundOrangnizationView!.frame.size.height
            yScreenView += pBottombackgroundOrangnizationView
            
            // Avatar
            let pOrangnizationAvatar : CGFloat = 0
            let wOrangnizationAvatar : CGFloat = AppDevice.SubViewFrame.OrangnizationIconWidth
            let hOrangnizationAvatar : CGFloat = wOrangnizationAvatar
            let xOrangnizationAvatar : CGFloat = pOrangnizationAvatar
            let yOrangnizationAvatar : CGFloat = (hbackgroundOrangnizationView-hOrangnizationAvatar)/2
            
            imvOrangnizationAvatar = UIImageView(frame: CGRect(x: xOrangnizationAvatar, y: yOrangnizationAvatar, width: wOrangnizationAvatar, height: hOrangnizationAvatar))
            imvOrangnizationAvatar?.layer.cornerRadius = 4.0
            imvOrangnizationAvatar?.layer.masksToBounds = true
            backgroundOrangnizationView?.addSubview(imvOrangnizationAvatar!)
        
            loadAvatarForOrangnization(orangnization: orangnization!)
            
            // Title name
            let pOrangnizationName : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let xOrangnizationName : CGFloat = xOrangnizationAvatar + wOrangnizationAvatar + pOrangnizationName
            let yOrangnizationName : CGFloat = 0
            let wOrangnizationName : CGFloat = wbackgroundOrangnizationView - xOrangnizationName
            let hOrangnizationName : CGFloat = 28
            
            lbOrangnizationName = UILabel(frame: CGRect(x: xOrangnizationName, y: yOrangnizationName, width: wOrangnizationName, height: hOrangnizationName))
            lbOrangnizationName?.textAlignment = .left
            lbOrangnizationName?.textColor = AppColor.NormalColors.BLACK_COLOR
            lbOrangnizationName?.text = orangnization!.orangnizationName
            lbOrangnizationName?.numberOfLines = 1
            lbOrangnizationName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
            backgroundOrangnizationView?.addSubview(lbOrangnizationName!)
            
            let xOrangnizationPro : CGFloat = xOrangnizationName
            let yOrangnizationPro : CGFloat = yOrangnizationName + hOrangnizationName
            let wOrangnizationPro : CGFloat = wOrangnizationName
            let hOrangnizationPro : CGFloat = 20
            
            lbOrangnizationPlan = UILabel(frame: CGRect(x: xOrangnizationPro, y: yOrangnizationPro, width: wOrangnizationPro, height: hOrangnizationPro))
            lbOrangnizationPlan?.textAlignment = .left
            lbOrangnizationPlan?.textColor = AppColor.NormalColors.GRAY_1_COLOR
            lbOrangnizationPlan?.text = NSLocalizedString("pro_plan", comment: "")
            lbOrangnizationPlan?.numberOfLines = 1
            lbOrangnizationPlan?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            backgroundOrangnizationView?.addSubview(lbOrangnizationPlan!)
            
            //Icon
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            let owner = orangnization!.owner
            if(owner.userId == userId){
                let pIconDropdown : CGFloat = 0
                let wIconDropdown : CGFloat = 36
                let hIconDropdown : CGFloat = wIconDropdown
                let xIconDropdown : CGFloat = wbackgroundOrangnizationView - wIconDropdown - pIconDropdown
                let yIconDropdown : CGFloat = yOrangnizationAvatar + ((hOrangnizationAvatar - hIconDropdown) / 2)
                
                btnManage = IconButton.init(type: .custom)
                btnManage?.accessibilityIdentifier = AccessiblityId.BTN_MANAGE
                btnManage?.frame = CGRect(x: xIconDropdown, y: yIconDropdown, width: wIconDropdown, height: hIconDropdown)
                btnManage?.setImage(UIImage(named: "ic_dropdown_manage"), for: .normal)
                backgroundOrangnizationView?.addSubview(btnManage!)
                
                //Tap Background Event
                btnHeaderView = RaisedButton.init(type: .custom)
                btnHeaderView?.accessibilityIdentifier = AccessiblityId.BTN_TEAM_MANAGE
                btnHeaderView?.frame = CGRect(x: 0, y: 0, width: wbackgroundOrangnizationView, height: hbackgroundOrangnizationView)
                btnHeaderView?.pulseColor = AppColor.MicsColors.LINE_COLOR
                btnHeaderView?.backgroundColor = .clear
                backgroundOrangnizationView?.addSubview(btnHeaderView!)
            }
            }
            
        }
        
        let xbackgroundTeamView : CGFloat = xlineView1
        let ybackgroundTeamView : CGFloat = yScreenView
        let wbackgroundTeamView : CGFloat = wlineView1
        let hbackgroundTeamView : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let pBottombackgroundTeamView : CGFloat = AppDevice.SubViewFrame.OrangnizationCellPadding
        
        backgroundTeamView = UIView(frame: CGRect(x: xbackgroundTeamView, y: ybackgroundTeamView, width: wbackgroundTeamView, height: hbackgroundTeamView))
        backgroundTeamView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        backgroundViewCell?.addSubview(backgroundTeamView!)
        
        yScreenView += backgroundTeamView!.frame.size.height
        yScreenView += pBottombackgroundTeamView
        
        
        // Avatar
        let pTeamAvatar : CGFloat = 0
        let wTeamAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
        let hTeamAvatar : CGFloat = wTeamAvatar
        let xTeamAvatar : CGFloat = pTeamAvatar
        let yTeamAvatar : CGFloat = (hbackgroundTeamView-hTeamAvatar)/2
        
        imvTeamAvatar = UIImageView(frame: CGRect(x: xTeamAvatar, y: yTeamAvatar, width: wTeamAvatar, height: hTeamAvatar))
        imvTeamAvatar?.layer.cornerRadius = 4.0
        imvTeamAvatar?.layer.masksToBounds = true
        backgroundTeamView?.addSubview(imvTeamAvatar!)
        
        //
        let projectId = project?.projectId ?? ""
        let projectName = project?.name ?? ""
        imvTeamAvatar?.setLabelTextForAvatar_Large(image: nil, text: projectName, identify: projectId)
        
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = 0
        let hAction = hbackgroundTeamView
        let yAction : CGFloat = 0
        let xAction = wbackgroundTeamView - wAction - pAction
        
        btnChange = UIButton.init(type: .custom)
        btnChange?.accessibilityIdentifier = AccessiblityId.BTN_CHANGE
        btnChange?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnChange?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnChange?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnChange?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnChange?.titleLabel?.textAlignment = .right
        btnChange?.contentHorizontalAlignment = .right
        backgroundTeamView?.addSubview(btnChange!)
        
        var canChanged : Bool = false
        if(orangnization != nil){
            if(orangnization!.orangnizationId.count > 0){
                canChanged = true
//                let userId = AppSettings.sharedSingleton.account?.userId ?? ""
//                let owner = orangnization!.owner
//                if(owner.userId == userId){
//                    canChanged = true
//                }
            }
        }
//        let canChanged : Bool = true
        if(canChanged == false){
            btnChange?.setImage(UIImage(named: "ic_item_selected_clear"), for: .normal)
            btnChange?.isUserInteractionEnabled = false
        }
        else{
            btnChange?.setTitle(NSLocalizedString("change", comment:""), for: .normal)
            btnChange?.isUserInteractionEnabled = true
        }
//        btnChange?.setTitle(NSLocalizedString("change", comment:""), for: .normal)
//        btnChange?.isUserInteractionEnabled = true
        
        // Title name
        let pTeamName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTeamName : CGFloat = xTeamAvatar + wTeamAvatar + pTeamName
        let yTeamName : CGFloat = 0
        let wTeamName : CGFloat = xAction - xTeamName - pTeamName
        let hTeamName : CGFloat = hBackgroundView
        
        lbTeamName = UILabel(frame: CGRect(x: xTeamName, y: yTeamName, width: wTeamName, height: hTeamName))
        lbTeamName?.textAlignment = .left
        lbTeamName?.text = projectName
        lbTeamName?.numberOfLines = 1
        lbTeamName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        backgroundTeamView?.addSubview(lbTeamName!)
        
        let xlineView2 : CGFloat = xlineView1
        let ylineView2 : CGFloat = yScreenView
        let wlineView2 : CGFloat = wlineView1
        let hLineView2 : CGFloat = hLineView1
        let pBottomLineView2 : CGFloat = 1.0
        
        let lineView2 = UILabel(frame: CGRect(x: xlineView2, y: ylineView2, width: wlineView2, height: hLineView2))
        lineView2.addDashedBorder()
        backgroundViewCell?.addSubview(lineView2)
        
        yScreenView += lineView2.frame.size.height
        yScreenView += pBottomLineView2
        
        backgroundViewCell?.frame.size.height = yScreenView
    }
    
    func startShimmering_Cell() {
        backgroundViewCell?.startShimmering()
    }
    
    func stopShimmering_Cell() {
        backgroundViewCell?.stopShimmering()
    }
    
    func loadAvatarForOrangnization(orangnization: Orangnization){
        //
        imvOrangnizationAvatar?.loadAvatarForOrangnization(orangnization: orangnization)
    }

}
