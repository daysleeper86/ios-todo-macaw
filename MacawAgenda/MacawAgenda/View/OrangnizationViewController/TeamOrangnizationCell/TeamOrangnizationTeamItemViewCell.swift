//
//  TeamOrangnizationTeamItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material

class TeamOrangnizationTeamItemViewCell: UITableViewCell {
    //Views
    private var backgroundViewCell : UIView?
    private var imvAvatar : FAShimmerImageView?
    private var lbTitleName : FAShimmerLabelView?
    private var lbTitleSubName : FAShimmerLabelView?
    private var imvSelected : FAShimmerImageView?
    var btnManage : IconButton?
    //Data
    private var project = ProjectService.createEmptyProject()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        initView()
    }
    
    func initView(){
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconMediumWidth
        //
        let paddingRow : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let heightRow : CGFloat = paddingRow + wAvatar + paddingRow
        // Background View
        let xBackgroundView : CGFloat = 8
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = heightRow
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        backgroundViewCell?.layer.cornerRadius = 4.0
        backgroundViewCell?.layer.masksToBounds = true
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = pAvatar
        let yAvatar : CGFloat = (hBackgroundView-hAvatar)/2
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = 4.0
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.image = LocalImage.UserPlaceHolder
        backgroundViewCell?.addSubview(imvAvatar!)
        
        //Image selected
        let pImageSelected : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wImageSelected : CGFloat = 13
        let hImageSelected : CGFloat = 8
        let xImageSelected : CGFloat = wBackgroundView - wImageSelected - pImageSelected
        let yImageSelected : CGFloat = (hBackgroundView-hImageSelected)/2
        
        imvSelected = FAShimmerImageView(frame: CGRect(x: xImageSelected, y: yImageSelected, width: wImageSelected, height: hImageSelected))
        backgroundViewCell?.addSubview(imvSelected!)
        
//        let pButtonManage : CGFloat = 8
//        let wButtonManage : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
//        let hButtonManage : CGFloat = wButtonManage
//        let xButtonManage : CGFloat = wBackgroundView - wButtonManage - pButtonManage
//        let yButtonManage : CGFloat = (hBackgroundView-hButtonManage)/2
        
//        btnManage = IconButton.init(type: .custom)
//        btnManage?.frame = CGRect(x: xButtonManage, y: yButtonManage, width: wButtonManage, height: hButtonManage)
//        btnManage?.setImage(UIImage(named: "ic_menu_gray"), for: .normal)
//        btnManage?.isHidden = true
//        backgroundViewCell?.addSubview(btnManage!)
        
        
        // Title name
        let pTitleName : CGFloat = pAvatar
        let hTitleName : CGFloat = AppDevice.SubViewFrame.LargeTitleHeight
        let xTitleName : CGFloat = xAvatar + wAvatar + pAvatar
//        let yTitleName : CGFloat = (hBackgroundView-hTitleName)/2
        let yTitleName : CGFloat = yAvatar
        let wTitleName : CGFloat = xImageSelected - xTitleName - pTitleName
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        backgroundViewCell?.addSubview(lbTitleName!)
        
        // Title name
        let hTitleSubName : CGFloat = hTitleName
        let xTitleSubName : CGFloat = xTitleName
        let yTitleSubName : CGFloat = yTitleName + hTitleName
        let wTitleSubName : CGFloat = wTitleName
        
        lbTitleSubName = FAShimmerLabelView(frame: CGRect(x: xTitleSubName, y: yTitleSubName, width: wTitleSubName, height: hTitleSubName))
        lbTitleSubName?.textAlignment = .left
        lbTitleSubName?.numberOfLines = 0
        lbTitleSubName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleSubName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        backgroundViewCell?.addSubview(lbTitleSubName!)
        
        let hLineView : CGFloat = 0.6
        let xlineView : CGFloat = xTitleName
        let ylineView : CGFloat = heightRow - hLineView
        let wlineView : CGFloat = wBackgroundView - xlineView
        
        let lineView = UILabel(frame: CGRect(x: xlineView, y: ylineView, width: wlineView, height: hLineView))
        lineView.backgroundColor = AppColor.MicsColors.LINE_COLOR
        backgroundViewCell?.addSubview(lineView)
    }
    
    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
        imvSelected?.image = nil
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleSubName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR

        imvSelected?.isHidden = true
        
        imvAvatar?.startShimmering()
        lbTitleName?.startShimmering()
        lbTitleSubName?.startShimmering()
        
        //Frame
        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wTitleName : CGFloat = backgroundViewCell!.frame.size.width - lbTitleName!.frame.origin.x - pTitleName
        lbTitleName?.frame.size.width = wTitleName
        lbTitleSubName?.frame.size.width = wTitleName
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleSubName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                imvSelected?.isHidden = false
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopShimmering()
                lbTitleSubName?.stopShimmering()
                
                //Frame
                let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
                let wTitleName : CGFloat = imvSelected!.frame.origin.x - lbTitleName!.frame.origin.x - pTitleName
                lbTitleName?.frame.size.width = wTitleName
                lbTitleSubName?.frame.size.width = wTitleName
            }
        }
    }
    
    func setBackGroundColor(_ color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    func setValueForCell(project: Project, selected: Bool, manage: Bool){
        self.project = project
        //
        if(self.project.dataType != .holder){
            stopShimmering_Cell()
            //
            let projectName = self.project.name
            let projectInfo = String(self.project.users.count) + " " + NSLocalizedString("members", comment: "").lowercased()
            //
            stateForAvatar(projectName)
            //
            stateForTitleName(projectName, info: projectInfo)
            //
            imvSelected?.isHidden = !selected
            //
            btnManage?.isHidden = !manage
            //
            if(project.dataType == .processing){
                self.imvAvatar?.startShimmering()
                self.lbTitleName?.startShimmering()
                self.lbTitleSubName?.startShimmering()
            }
            else{
                self.imvAvatar?.stopShimmering()
                self.lbTitleName?.stopShimmering()
                self.lbTitleSubName?.stopShimmering()
            }
            //
            if(selected == true){
                imvSelected?.image = UIImage(named: "ic_item_selected_clear")
                setBackGroundColor(AppColor.MicsColors.BLUE_LIGHT_1_COLOR)
            }
            else{
                setBackGroundColor(AppColor.NormalColors.WHITE_COLOR)
            }
        }
            
        else{
            startShimmering_Cell()
        }
    }
    
    func stateForTitleName(_ name : String, info :  String){
        lbTitleName?.text = name
        lbTitleSubName?.text = info
    }
    
    func hideAvatar(isHide: Bool){
        imvAvatar?.isHidden = isHide
    }
    
    func stateForAvatar(_ name : String){
        //
        imvAvatar?.setLabelTextForAvatar_Large(image: nil, text: name, identify: name)
    }
}
