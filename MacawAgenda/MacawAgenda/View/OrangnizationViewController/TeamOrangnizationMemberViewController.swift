//
//  TeamOrangnizationTeamViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material
import RxSwift

protocol TeamOrangnizationMemberViewControllerDelegate : NSObject {
    func teamOrangnizationMemberViewControllerSelectItem(_ teamOrangnizationMemberViewController: TeamOrangnizationMemberViewController, doneWith user: User, selectAt index:Int)
}

class TeamOrangnizationMemberViewController: BaseViewController {
    
    //Views
    var tbTeam = UITableView(frame: .zero)
    var hViewPager : CGFloat = 0
    var navigationView : CustomNavigationView?
    
    //Data
    static let teamOrangnizationMemberItemViewCell : String = "teamOrangnizationMemberItemViewCell"
    var projectId : String = AppSettings.sharedSingleton.project?.projectId ?? ""
    var orangnization : Orangnization = OrangnizationService.createEmptyOrangnization()
    var arrayProjects : [Project] = [] {
        didSet {
            // Update the view.
            setDataView()
        }
    }
    var arrayUsers : [User] = []
    
    //ViewModel
    var projectViewModel : ProjectViewModel?
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    
    //Events
    weak var delegate: TeamOrangnizationMemberViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        // Update the view.
        setDataView()
    }
    
    func setDataView(){
        self.orangnization = AppSettings.sharedSingleton.orangnization ?? OrangnizationService.createEmptyOrangnization()
        //
        arrayUsers.removeAll()
        for var user in self.orangnization.users{
            let userId = user.userId
            var strUserProject = ""
            for project in self.arrayProjects{
                if(project.checkExist(userId: userId)){
                    let projectName = "# " + project.name
                    if(strUserProject.count == 0){
                        strUserProject = projectName
                    }
                    else{
                        strUserProject = strUserProject + " | " + projectName
                    }
                }
            }
            //
            user.info = strUserProject
            arrayUsers.append(user)
        }
        tbTeam.reloadData()
    }
    
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = 0
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView?.addSubview(btnClose)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView!.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleHeader.text = NSLocalizedString("change_team", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
        //
    }
    
    func createContentView(yHeader :  CGFloat){
        //
        var hBackgroundView : CGFloat = 0
        if(hViewPager > 0){
            hBackgroundView = hViewPager
        }
        else{
            hBackgroundView = AppDevice.ScreenComponent.Height - yHeader
        }
        
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = hBackgroundView - yTableMember
        tbTeam.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbTeam.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeam.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeam.dataSource = self
        tbTeam.delegate = self
        tbTeam.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbTeam.register(TeamOrangnizationMemberItemViewCell.self, forCellReuseIdentifier: TeamOrangnizationMemberViewController.teamOrangnizationMemberItemViewCell)
        self.view.addSubview(tbTeam)
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
//        let yScreen : CGFloat = createHeaderView()
        let yScreen : CGFloat = 0
        createContentView(yHeader: yScreen)
    }
    
    
    func bindToViewModel(){
    }
}

extension TeamOrangnizationMemberViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamOrangnizationMemberViewController.teamOrangnizationMemberItemViewCell, for: indexPath) as? TeamOrangnizationMemberItemViewCell else {
            return TeamOrangnizationMemberItemViewCell()
        }
        let element = arrayUsers[indexPath.row]
        //
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        cell.setValueForCell(user: element)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let heightRow : CGFloat = AppDevice.ScreenComponent.ItemPadding + AppDevice.SubViewFrame.AvatarIconWidth + AppDevice.ScreenComponent.ItemPadding
        return heightRow
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //
        let element = self.arrayUsers[indexPath.row]
        //
        self.delegate?.teamOrangnizationMemberViewControllerSelectItem(self, doneWith: element, selectAt: indexPath.row)
    }
}

