//
//  TeamOrangnizationProfileViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Firebase
import RxFirebaseAuthentication
import FirebaseAuth
import Material
import Moya
import RxSwift

class TeamOrangnizationProfileViewController: BaseViewController {
    
    //External props
    var user = UserService.createEmptyUser()
    var arrayProject : [Project] = [] {
        didSet {
            // Update the view.
            setDataView()
        }
    }
    //Views
    var tbSettings : UITableView?
    var imvAvatar : UIImageView?
    var lbTitleName : UILabel?
    var lbTitleEmail : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setDataView(){
        tbSettings?.reloadData()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pBack : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xBack : CGFloat = pBack
        let yBack : CGFloat = yContentHeaderView
        let hButton : CGFloat = hContentHeaderView
        let wButton : CGFloat = hButton
        
        let btnBack = IconButton.init(type: .custom)
        btnBack.frame = CGRect(x: xBack, y: yBack, width: wButton, height: hButton)
        btnBack.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnBack.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationView.addSubview(btnBack)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        let bgView = UIView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        bgView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(bgView)
        
        let yViewScreen : CGFloat = 0
        let xTableSetting : CGFloat = 0
        let yTableSetting : CGFloat = yViewScreen
        let wTableSetting : CGFloat = bgView.frame.size.width
        let hTableSetting : CGFloat = bgView.frame.size.height - yTableSetting
        tbSettings = UITableView(frame: CGRect(x: xTableSetting, y: yTableSetting, width: wTableSetting, height: hTableSetting), style: .grouped)
        tbSettings?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.dataSource = self
        tbSettings?.delegate = self
        tbSettings?.register(TeamOrangnizationTeamItemViewCell.self, forCellReuseIdentifier: TeamOrangnizationTeamSelectedViewController.teamOrangnizationItemViewCell)
        bgView.addSubview(tbSettings!)
    }
    
    func initView(){
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadUserInfo(){
        loadAvatar()
        loadInfo()
    }
    
    func loadAvatar(){
        imvAvatar?.loadAvatarForUser(user: self.user)
    }
    
    func loadInfo(){
        let userName = self.user.name.count > 0 ? self.user.name : ""
        let userEmail = self.user.email.count > 0 ? self.user.email : ""
        lbTitleName?.text = userName
        lbTitleEmail?.text = userEmail
    }
    
    
}

extension TeamOrangnizationProfileViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let hHeader : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderPaddingTop + AppDevice.SubViewFrame.SettingProfileHeaderHeight + AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom + AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom + AppDevice.ScreenComponent.NormalHeight
        return hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let xHeader : CGFloat = 0
        let yHeader : CGFloat = 0
        let wHeader : CGFloat = tableView.frame.size.width - (xHeader * 2)
        let hHeader : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderPaddingTop + AppDevice.SubViewFrame.SettingProfileHeaderHeight + AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom + AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom + AppDevice.ScreenComponent.NormalHeight
        
        let headerView = UIView(frame: CGRect(x: xHeader, y: yHeader, width: wHeader, height: hHeader))
        headerView.isUserInteractionEnabled = true
        headerView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        
        let xUserView : CGFloat = 24
        let yUserView : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderPaddingTop
        let wUserView : CGFloat = headerView.frame.size.width - (xUserView * 2)
        let hUserView : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderHeight
        let pBottomUserView : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom
        //        let pBottomUserView : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom
        
        let userView = UIView(frame: CGRect(x: xUserView, y: yUserView, width: wUserView, height: hUserView))
        userView.isUserInteractionEnabled = false
        headerView.addSubview(userView)
        
        let xLineView : CGFloat = xUserView
        let wLineView : CGFloat = wUserView
        let hLineView : CGFloat = 1.0
        let yLineView : CGFloat = userView.frame.origin.y + userView.frame.size.height + hLineView + pBottomUserView
        let pBottomLineView : CGFloat = AppDevice.SubViewFrame.SettingProfileHeaderPaddingBottom
        let lineView = UILabel(frame: CGRect(x: xLineView, y: yLineView, width: wLineView, height: hLineView))
        lineView.addDashedBorder()
        headerView.addSubview(lineView)
        
        
        
        let xAvatar : CGFloat = 0
        let yAvatar : CGFloat = 0
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconLargeLargeWidth
        let hAvatar : CGFloat = wAvatar
        let pBottomAvatar : CGFloat = 24
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.isAccessibilityElement = true
        imvAvatar?.accessibilityIdentifier = AccessiblityId.IMG_USERAVATAR
        imvAvatar?.layer.cornerRadius = 4.0
        imvAvatar?.contentMode = .scaleAspectFill
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        userView.addSubview(imvAvatar!)
        
        //Title Screen
        //        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = xAvatar
        let yTitleName : CGFloat = yAvatar + hAvatar + pBottomAvatar
        let wTitleName : CGFloat = userView.frame.size.width - xTitleName
        let hTitleName : CGFloat = 32
        let pBottomTitleName : CGFloat = 4
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.accessibilityIdentifier = AccessiblityId.LB_USERNAME
        lbTitleName?.textAlignment = .left
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        userView.addSubview(lbTitleName!)
        
        //Title Screen
        let xTitleEmail : CGFloat = xAvatar
        let yTitleEmail : CGFloat = yTitleName + hTitleName + pBottomTitleName
        let wTitleEmail : CGFloat = wTitleName
        let hTitleEmail : CGFloat = 20
        
        lbTitleEmail = UILabel(frame: CGRect(x: xTitleEmail, y: yTitleEmail, width: wTitleEmail, height: hTitleEmail))
        lbTitleEmail?.accessibilityIdentifier = AccessiblityId.LB_EMAIL
        lbTitleEmail?.textAlignment = .left
        lbTitleEmail?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleEmail?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        userView.addSubview(lbTitleEmail!)
        
        //
        let xTitleJoined : CGFloat = xUserView
        let yTitleJoined : CGFloat = yLineView + hLineView + pBottomLineView
        let wTitleJoined : CGFloat = wUserView
        let hTitleJoined : CGFloat = 24
        
        let lbTitleJoined = UILabel(frame: CGRect(x: xTitleJoined, y: yTitleJoined, width: wTitleJoined, height: hTitleJoined))
        lbTitleJoined.textAlignment = .left
        lbTitleJoined.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleJoined.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleJoined.text = NSLocalizedString("joined_team", comment: "")
        headerView.addSubview(lbTitleJoined)
        
        //
        loadUserInfo()
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayProject.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heightRow : CGFloat = AppDevice.ScreenComponent.ItemPadding + AppDevice.SubViewFrame.AvatarIconMediumWidth + AppDevice.ScreenComponent.ItemPadding
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamOrangnizationTeamSelectedViewController.teamOrangnizationItemViewCell, for: indexPath) as? TeamOrangnizationTeamItemViewCell else {
            return TeamOrangnizationTeamItemViewCell()
        }
        let element = self.arrayProject[indexPath.row]
        //
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        cell.setValueForCell(project: element, selected: false, manage: true)
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

