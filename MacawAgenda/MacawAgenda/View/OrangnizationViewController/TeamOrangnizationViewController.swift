//
//  TeamOrangnizationViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 9/27/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Tabman
import Pageboy
import Material
import RxSwift
import Moya

class TeamOrangnizationViewController: BaseViewController {
    //Views
    var navigationView : CustomNavigationView?
    var contentScrollView : UIScrollView?
    var btnAddTeam : UIButton?
    var viewPager : WormTabStrip?
    private var viewControllers : [BaseViewController] = []
    private let teamOrangnizationTeamViewController = TeamOrangnizationTeamViewController(nibName: nil, bundle: nil)
    private let teamOrangnizationMemberViewController = TeamOrangnizationMemberViewController(nibName: nil, bundle: nil)
    private var teamOrangnizationProfileViewController : TeamOrangnizationProfileViewController?
    private var teamOrangnizationCreateTeamViewController : TeamOrangnizationCreateTeamViewController?
    
    //Data
    private var titles = [NSLocalizedString("teams", comment: ""), NSLocalizedString("members", comment: "")]
    private var yScreen_Scroll : CGFloat = 0
    private var arrayProjects : [Project] = []
    
    //View Model
    private var projectViewModel : ProjectViewModel?
    private var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    private var uploadingProjects : [Project] = []
    private var subjectUploadingProjects = BehaviorSubject<[Project]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Check last viewcontroller in navigation is TeamOrangnizationProfileViewController. Release it
        if(teamOrangnizationProfileViewController != nil){
            if let strongNavigationController = self.navigationController{
                let viewControllers = strongNavigationController.viewControllers
                if(viewControllers.count > 0){
                    let lastViewControllers = viewControllers[viewControllers.count - 1]
                    if lastViewControllers is TeamOrangnizationProfileViewController{
                        
                    } else{
                        teamOrangnizationProfileViewController = nil
                    }
                }
            }
            
        }
    }
    
    //Init datas
    func initData(hViewPager: CGFloat){
        teamOrangnizationTeamViewController.delegate = self
        teamOrangnizationTeamViewController.hViewPager = hViewPager
        //
        teamOrangnizationMemberViewController.delegate = self
        teamOrangnizationMemberViewController.hViewPager = hViewPager
        
        viewControllers.append(teamOrangnizationTeamViewController)
        viewControllers.append(teamOrangnizationMemberViewController)
    }
    
    //Init views
    func initView(){
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Action Button
        let yBacklog : CGFloat = yContentHeaderView
        let wButton : CGFloat = hContentHeaderView
        let hButton : CGFloat = wButton
        let xBacklog : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        
        let btnBacklog = IconButton.init(type: .custom)
        btnBacklog.accessibilityIdentifier = AccessiblityId.BTN_BACKLOG
        btnBacklog.frame = CGRect(x: xBacklog, y: yBacklog, width: wButton, height: hButton)
        btnBacklog.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnBacklog.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationView?.addSubview(btnBacklog)
        
        let pAddTeam : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xAddTeam : CGFloat = wHeaderView - wButton - pAddTeam
        let yAddTeam : CGFloat = yContentHeaderView
        
        btnAddTeam = IconButton.init(type: .custom)
        btnAddTeam?.accessibilityIdentifier = AccessiblityId.BTN_NOTIFICATION
        btnAddTeam?.frame = CGRect(x: xAddTeam, y: yAddTeam, width: wButton, height: hButton)
        btnAddTeam?.setImage(UIImage(named: "ic_add_blue"), for: .normal)
        btnAddTeam?.addTarget(self, action: #selector(addTeamAction), for: .touchUpInside)
        navigationView?.addSubview(btnAddTeam!)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xBacklog + wButton + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = xAddTeam - xTitleHeader
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        navigationView?.titleView(titleView: lbTitleHeader)
        navigationView?.showHideTitleNavigation(isShow: true)
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        let orangnizationName = AppSettings.sharedSingleton.orangnization?.orangnizationName ?? ""
        lbTitleHeader.text = orangnizationName
        
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScrollView : CGFloat = yHeader
        let hScrollView : CGFloat = AppDevice.ScreenComponent.Height - yScrollView
        contentScrollView = UIScrollView(frame: CGRect(x: 0, y: yScrollView, width: AppDevice.ScreenComponent.Width, height: hScrollView))
        contentScrollView?.accessibilityIdentifier = AccessiblityId.SCROLLVIEW_TEAM_ORANGNIZATION
        contentScrollView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(contentScrollView!)
        
        //Content View
        var yScreenView : CGFloat = 0
        
        let kHeightOfTopScrollView : CGFloat = 40
        let pTopViewPager : CGFloat = AppDevice.ScreenComponent.NormalHeight - kHeightOfTopScrollView
        yScreenView += pTopViewPager
        
        let hViewPager : CGFloat = hScrollView - yScreenView
        //
        let hViewContentPager : CGFloat = hViewPager - kHeightOfTopScrollView
        initData(hViewPager: hViewContentPager)
        //
        let frame =  CGRect(x: 0, y: yScreenView, width: self.view.frame.size.width, height: hViewPager)
        viewPager = WormTabStrip(frame: frame)
        viewPager?.isJustified = false
        viewPager?.eyStyle.kHeightOfTopScrollView = kHeightOfTopScrollView
        contentScrollView!.addSubview(viewPager!) //IMPORTANT!
        viewPager?.delegate = self
        viewPager?.eyStyle.wormStyel = .LINE
        viewPager?.eyStyle.topScrollViewBackgroundColor = .white
        viewPager?.eyStyle.contentScrollViewBackgroundColor = .white
        
        viewPager?.eyStyle.isWormEnable = true
        viewPager?.eyStyle.spacingBetweenTabs = 15
        viewPager?.eyStyle.dividerBackgroundColor = AppColor.MicsColors.LINE_VIEW_NORMAL.withAlphaComponent(0.5)
        viewPager?.eyStyle.WormColor = AppColor.NormalColors.BLUE_COLOR
        viewPager?.eyStyle.kHeightOfDivider = 1
        viewPager?.eyStyle.tabItemDefaultColor = AppColor.NormalColors.BLACK_COLOR
        viewPager?.eyStyle.tabItemSelectedColor = AppColor.NormalColors.BLUE_COLOR
        viewPager?.eyStyle.tabItemDefaultFont = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14)
        viewPager?.eyStyle.tabItemSelectedFont = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14)
        //default selected tab
        viewPager?.currentTabIndex = 0
        //center the selected tab
        viewPager?.shouldCenterSelectedWorm = false
        viewPager?.buildUI()
        
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addTeamAction(){
        teamOrangnizationCreateTeamViewController = TeamOrangnizationCreateTeamViewController(nibName: nil, bundle: nil)
        if let orangnization = AppSettings.sharedSingleton.orangnization{
            teamOrangnizationCreateTeamViewController?.leader = orangnization.owner
            teamOrangnizationCreateTeamViewController?.arrayUsers = orangnization.users
        }
        else{
            if let account = AppSettings.sharedSingleton.account{
                var user = User(userId: account.userId, dataType: .normal)
                user.name = account.name
                //
                teamOrangnizationCreateTeamViewController?.leader = user
            }
            teamOrangnizationCreateTeamViewController?.arrayUsers = []
        }
        teamOrangnizationCreateTeamViewController?.delegate = self
        teamOrangnizationCreateTeamViewController!.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.navigationController?.present(teamOrangnizationCreateTeamViewController!, animated: true, completion: nil)
    }
    
    func bindToViewModel(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let orangnizationId = AppSettings.sharedSingleton.orangnization?.orangnizationId ?? ""
        //
        let projectController = ProjectController.sharedAPI
        self.projectViewModel = ProjectViewModel(
            inputUserNeedProjects: (
                userId: Observable.just(userId),
                orangnizationId: orangnizationId,
                projectController: projectController,
                dataSource : subjectUploadingProjects.asObservable()
            )
        )
        
        self.projectViewModel?.projects?
            .subscribe(onNext:  {[weak self] projects in
                guard let strongSelf = self else { return }
                var newProjects = projects
                if(newProjects.count == 1){
                    let project = newProjects[0]
                    if(project.dataType == .empty){
                        newProjects.removeAll()
                        if let strongCurrentProject = AppDelegate().sharedInstance().currentProject{
                            newProjects.append(strongCurrentProject)
                        }
                    }
                }
                self?.arrayProjects = newProjects
                self?.teamOrangnizationTeamViewController.arrayProjects = newProjects
                self?.teamOrangnizationMemberViewController.arrayProjects = newProjects
                
                if let strongProfileViewController = strongSelf.teamOrangnizationProfileViewController{
                    strongProfileViewController.arrayProject = strongSelf.filterProjectByUser(user: strongProfileViewController.user)
                }
            }).disposed(by: disposeBag)
        
        //
        
    }
}


extension TeamOrangnizationViewController : WormTabStripDelegate{
    func WTSNumberOfTabs() -> Int {
        return viewControllers.count
    }
    
    func WTSViewOfTab(index: Int) -> UIView {
        return viewControllers[index].view
    }
    
    func WTSTitleForTab(index: Int) -> String {
        let title = titles[index]
        return title
    }
    
    func WTSReachedLeftEdge(panParam: UIPanGestureRecognizer) {
        
    }
    
    func WTSReachedRightEdge(panParam: UIPanGestureRecognizer) {
        
    }
}

extension TeamOrangnizationViewController : TMBarDataSource{
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let title = titles[index]
        return TMBarItem(title: title)
    }
}

extension TeamOrangnizationViewController : PageboyViewControllerDataSource{
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
}

extension TeamOrangnizationViewController : TeamOrangnizationTeamViewControllerDelegate{
    func teamOrangnizationTeamViewControllerSelectItem(_ teamOrangnizationTeamViewController: TeamOrangnizationTeamViewController, doneWith project: Project, selectAt index: Int) {
        
    }
    
}

extension TeamOrangnizationViewController : TeamOrangnizationMemberViewControllerDelegate{
    func teamOrangnizationMemberViewControllerSelectItem(_ teamOrangnizationMemberViewController: TeamOrangnizationMemberViewController, doneWith user: User, selectAt index: Int) {
        //
        teamOrangnizationProfileViewController = TeamOrangnizationProfileViewController(nibName: nil, bundle: nil)
        teamOrangnizationProfileViewController?.user = user
        teamOrangnizationProfileViewController?.arrayProject = filterProjectByUser(user: user)
        self.navigationController?.pushViewController(teamOrangnizationProfileViewController!, animated: true)
    }
    
    func filterProjectByUser(user: User) -> [Project]{
        var arraySubProject : [Project] = []
        let userId = user.userId
        for project in self.arrayProjects{
            if(project.checkExist(userId: userId)){
                arraySubProject.append(project)
            }
        }
        return arraySubProject
    }
}

extension TeamOrangnizationViewController : TeamOrangnizationCreateTeamViewControllerDelegate{
    
    func stopProcessingNewProjectForAllVisibleCell(){
        uploadingProjects.removeAll()
        subjectUploadingProjects.onNext(uploadingProjects)
    }
    
    func createTeam(teamLeader: String, projectName: String){
        let orangnizationId = AppSettings.sharedSingleton.orangnization?.orangnizationId ?? ""
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let projectController = ProjectAPIController(provider: projectProvider)
        let projectCreatedViewModel = ProjectAPIViewModel(
            inputCreateOrangnizationProject: (
                orangnizationId: orangnizationId,
                name: BehaviorSubject<String>(value: projectName),
                teamLeader: BehaviorSubject<String>(value: teamLeader),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: projectController
            )
        )
        
        
        projectCreatedViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    //
                    self?.stopProcessingNewProjectForAllVisibleCell()
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                        self?.makeToast(message:NSLocalizedString("create_team_successfully", comment: ""), duration: 0.5, position: .top)
                        }
                        else{
                            var message = NSLocalizedString("create_team_unsuccessfully", comment: "")
                            if(reponseAPI.code != 0){
                                if(reponseAPI.message.count > 0){
                                    message = reponseAPI.message
                                }
                            }
                            self?.makeToast(message:message, duration: 0.5, position: .top)
                        }
                    }
                    //
                    self?.stopProcessingNewProjectForAllVisibleCell()
                }
            }
        }).disposed(by: disposeBag)
        
    }
    
    func teamOrangnizationCreateTeamViewControllerSave(_ teamOrangnizationCreateTeamViewController: TeamOrangnizationCreateTeamViewController, doneWith user: User, andInfo name: String){
        let project = Project(projectId: "1", name: name, dataType: .processing)
        //
        uploadingProjects.insert(project, at: 0)
        subjectUploadingProjects.onNext(uploadingProjects)
        //
        createTeam(teamLeader: user.userId, projectName:name)
        //
        self.teamOrangnizationCreateTeamViewController = nil
    }
}
