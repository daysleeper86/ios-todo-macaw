//
//  InputEmailViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Material

class InputEmailViewController: BaseViewController,UITextFieldDelegate {

    //View
    var keyboardBounds : CGRect?
    var btnContinue : UIButton?
    var tfInputEmail : FloatingLabelTextFieldView?
    var lbErrorEmailAdress : UILabel?
    //Data
    var duration : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.accessibilityIdentifier = AccessiblityId.BTN_BACK
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 100
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnContinue = UIButton.init(type: .custom)
        btnContinue?.accessibilityIdentifier = AccessiblityId.BTN_INPUTEMAIL
        btnContinue?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnContinue?.setTitle(NSLocalizedString("continue", comment:"").uppercased(), for: .normal)
        btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnContinue?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnContinue?.titleLabel?.textAlignment = .right
        btnContinue?.contentHorizontalAlignment = .right
        navigationView.addSubview(btnContinue!)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        let bgView = UIScrollView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        self.view.addSubview(bgView)
        
       

        
        var yScreenView : CGFloat = 0
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yScreenView
        let wTitleScreen : CGFloat = wBGView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = 40
        let pBottomTitleScreen : CGFloat = 72
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("continue_with_email", comment: "")
        bgView.addSubview(lbTitleScreen)
        
        yScreenView += hTitleScreen
        yScreenView += pBottomTitleScreen
        
        let heightTextField : CGFloat = FloatingLabelTextFieldView.heightTextFieldView
        //
        let xInputEmail : CGFloat = 0
        let yInputEmail : CGFloat = yScreenView
        let wInputEmail : CGFloat = bgView.frame.size.width
        let hInputEmail : CGFloat = heightTextField
        let pBottomInputEmail : CGFloat = 8
        
        tfInputEmail = FloatingLabelTextFieldView(frame: CGRect(x: xInputEmail, y: yInputEmail, width: wInputEmail, height: hInputEmail))
        tfInputEmail?.textField?.accessibilityIdentifier = AccessiblityId.TF_EMAIL
        tfInputEmail?.textField?.autocapitalizationType = .none
        tfInputEmail?.textField?.placeholder = NSLocalizedString("enter_your_email_address", comment: "")
        tfInputEmail?.textField?.title = NSLocalizedString("word_email_address", comment: "").uppercased()
        tfInputEmail?.textField?.keyboardType = .emailAddress
        bgView.addSubview(tfInputEmail!)
        
        yScreenView += hInputEmail
        yScreenView += pBottomInputEmail
        
        //Label error email
        let pErrorEmailAdress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xErrorEmailAdress : CGFloat = pErrorEmailAdress
        let yErrorEmailAdress : CGFloat = yScreenView
        let wErrorEmailAdress : CGFloat = wBGView - (xErrorEmailAdress * 2)
        let hErrorEmailAdress : CGFloat = AppDevice.SubViewFrame.TitleHeight

        lbErrorEmailAdress = UILabel(frame: CGRect(x: xErrorEmailAdress, y: yErrorEmailAdress, width: wErrorEmailAdress, height: hErrorEmailAdress))
        lbErrorEmailAdress?.accessibilityIdentifier = AccessiblityId.LB_ERROR_EMAIL
        lbErrorEmailAdress?.textAlignment = .left
        lbErrorEmailAdress?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
        lbErrorEmailAdress?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        bgView.addSubview(lbErrorEmailAdress!)
        
        yScreenView += hErrorEmailAdress
        
        
        //
        tfInputEmail?.textField?.becomeFirstResponder()
        
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func gotoLoginView(){
        if let email = tfInputEmail?.textField?.text{
            let loginEmailView  = LoginEmailViewController(nibName: nil, bundle: nil)
            loginEmailView.email = email.lowercased()
            self.navigationController?.pushViewController(loginEmailView, animated: true)
        }
    }
    
    func gotoSignUpView(){
        if let email = tfInputEmail?.textField?.text{
            let signupEmailView  = SignUpEmailViewController(nibName: nil, bundle: nil)
            signupEmailView.email = email.lowercased()
            self.navigationController?.pushViewController(signupEmailView, animated: true)
        }
    }
    
    private func bindToViewModel() {
        let provider = MoyaProvider<AppAPI>(manager: DefaultAlamofireManager.sharedManager,
                                            plugins: [NetworkLoggerPlugin(verbose: true)])
        let accountController = AccountAPIController(provider: provider)
        let viewModel = AccountViewModel(
            inputEmailInput: (
                email: tfInputEmail!.textField!.rx.text.orEmpty.asObservable(),
                loginTaps: btnContinue!.rx.tap.asObservable(),
                controller: accountController
            )
        )
        // bind results
        viewModel.validatedEmail?
            .subscribe(onNext: { [weak self] valid  in
                var strError = ""
                var color = AppColor.NormalColors.BLUE_COLOR
                if(valid == false){
                    guard let strongSelf = self, let text = strongSelf.tfInputEmail?.textField?.text else { return }
                    if(text.count > 0){
                        strError = NSLocalizedString("please_use_a_valid_email", comment: "")
                        color = AppColor.NormalColors.LIGHT_RED_COLOR
                    }
                }
                self?.lbErrorEmailAdress?.text = strError
                self?.tfInputEmail?.textField?.selectedLineColor = color
                
            })
            .disposed(by: disposeBag)
        
        viewModel.loginEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.btnContinue?.isEnabled = valid
                if(valid == true){
                    self?.btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
                }
                else{
                    self?.btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.signingIn
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.startLoading(message: NSLocalizedString("authenticating", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        //
        viewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                self?.makeToast(message:output.message, duration: 3.0, position: .top)
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPICheckEmail{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 0){
                            self?.gotoSignUpView()
                        }
                        else{
                            self?.gotoLoginView()
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        //Tap Background Event
        let tapBackground = UITapGestureRecognizer()
        tapBackground.rx.event
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
        view.addGestureRecognizer(tapBackground)
        
    }

}

