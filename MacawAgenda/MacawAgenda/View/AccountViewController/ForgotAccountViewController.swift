//
//  LoginEmailViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/18/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Material
import DTGradientButton

class ForgotAccountViewController: BaseViewController,UITextFieldDelegate {

    //View
    var keyboardBounds : CGRect?
    var btnResetAccount : RaisedButton?
    var email : String?
    var tfInputEmail : FloatingLabelTextFieldView?
    var lbErrorEmailAdress : UILabel?
    //Data
    var duration : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        let bgView = UIScrollView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        self.view.addSubview(bgView)
        
        var yScreenView : CGFloat = 0
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yScreenView
        let wTitleScreen : CGFloat = wBGView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = 40
        let pBottomTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("forgot_your_password", comment: "")
        bgView.addSubview(lbTitleScreen)
        
        yScreenView += hTitleScreen
        yScreenView += pBottomTitleScreen
        
        //Title Email
        let pTitleEmailAdress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleEmailAdress : CGFloat = pTitleEmailAdress
        let yTitleEmailAdress : CGFloat = yScreenView
        let wTitleEmailAdress : CGFloat = wBGView - (xTitleEmailAdress * 2)
        let hTitleEmailAdress : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let pBottomTitleEmailAdress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        let lbTitleEmailAdress : UILabel = UILabel(frame: CGRect(x: xTitleEmailAdress, y: yTitleEmailAdress, width: wTitleEmailAdress, height: hTitleEmailAdress))
        lbTitleEmailAdress.textAlignment = .left
        lbTitleEmailAdress.numberOfLines = 0
        lbTitleEmailAdress.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleEmailAdress.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitleEmailAdress.text = NSLocalizedString("enter_your_email_address_below", comment: "")
        bgView.addSubview(lbTitleEmailAdress)
        
        yScreenView += hTitleEmailAdress
        yScreenView += pBottomTitleEmailAdress
        
        let heightTextField : CGFloat = FloatingLabelTextFieldView.heightTextFieldView
        //
        let xInputEmail : CGFloat = 0
        let yInputEmail : CGFloat = yScreenView
        let wInputEmail : CGFloat = bgView.frame.size.width
        let hInputEmail : CGFloat = heightTextField
        let pBottomInputEmail : CGFloat = 8
        
        tfInputEmail = FloatingLabelTextFieldView(frame: CGRect(x: xInputEmail, y: yInputEmail, width: wInputEmail, height: hInputEmail))
        tfInputEmail?.textField?.accessibilityIdentifier = AccessiblityId.TF_EMAIL
        tfInputEmail?.textField?.autocapitalizationType = .none
        tfInputEmail?.textField?.placeholder = NSLocalizedString("enter_your_email_address", comment: "")
        tfInputEmail?.textField?.title = NSLocalizedString("word_email_address", comment: "").uppercased()
        tfInputEmail?.textField?.keyboardType = .emailAddress
        bgView.addSubview(tfInputEmail!)
        //Set data
        tfInputEmail?.textField?.text = self.email
        
        yScreenView += hInputEmail
        yScreenView += pBottomInputEmail
        
        //Label error email
        let pErrorEmailAdress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xErrorEmailAdress : CGFloat = pErrorEmailAdress
        let yErrorEmailAdress : CGFloat = yScreenView
        let wErrorEmailAdress : CGFloat = wBGView - (xErrorEmailAdress * 2)
        let hErrorEmailAdress : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let hBottomErrorEmailAdress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        lbErrorEmailAdress = UILabel(frame: CGRect(x: xErrorEmailAdress, y: yErrorEmailAdress, width: wErrorEmailAdress, height: hErrorEmailAdress))
        lbErrorEmailAdress?.accessibilityIdentifier = AccessiblityId.LB_ERROR_EMAIL
        lbErrorEmailAdress?.textAlignment = .left
        lbErrorEmailAdress?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
        lbErrorEmailAdress?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        bgView.addSubview(lbErrorEmailAdress!)
        
        yScreenView += hErrorEmailAdress
        yScreenView += hBottomErrorEmailAdress
        
        let yButtonReset : CGFloat = yScreenView
        let wButtonReset :CGFloat = 300
        let hButtonReset :CGFloat = AppDevice.ScreenComponent.ButtonHeight
        let xButtonReset :CGFloat = (wBGView - wButtonReset ) / 2
        
        btnResetAccount = RaisedButton.init(type: .custom)
        btnResetAccount?.accessibilityIdentifier = AccessiblityId.BTN_RESET
        btnResetAccount?.pulseColor = .white
        btnResetAccount?.frame = CGRect(x: xButtonReset, y: yButtonReset, width: wButtonReset, height: hButtonReset)
        btnResetAccount?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        btnResetAccount?.titleLabel?.textAlignment = .center
        btnResetAccount?.setTitle(NSLocalizedString("send_reset_password_email", comment: "").uppercased(), for: .normal)
        btnResetAccount?.layer.cornerRadius = 4.0
        btnResetAccount?.layer.masksToBounds = true
        bgView.addSubview(btnResetAccount!)
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Tap Background Event
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackground))
        self.view.addGestureRecognizer(tapGesture)
        
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        
    }
    
    @objc func didTapBackground(){
        tfInputEmail?.textField?.resignFirstResponder()
    }

    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func resetAction(){
        let emailFinishedView = EmailFinishedViewController(nibName: nil, bundle: nil)
        emailFinishedView.email = tfInputEmail?.textField?.text ?? ""
        self.navigationController?.pushViewController(emailFinishedView, animated: true)
    }
    
    func bindToViewModel(){
        //
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
        let userController = UserAPIController(provider: projectProvider)
        let observableTap = btnResetAccount!.rx.tap.asObservable()
        let accountViewModel = UserAPIViewModel(
            inputForgetPass: (
                email: tfInputEmail!.textField!.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                userAPIController: userController
            )
        )
        // bind results to  {
        accountViewModel.validatedEmail?
            .subscribe(onNext: { [weak self] valid  in
                var strError = ""
                var color = AppColor.NormalColors.BLUE_COLOR
                if(valid == false){
                    strError = NSLocalizedString("please_use_a_valid_email", comment: "")
                    color = AppColor.NormalColors.LIGHT_RED_COLOR
                }
                self?.lbErrorEmailAdress?.text = strError
                self?.tfInputEmail?.textField?.selectedLineColor = color
            })
            .disposed(by: disposeBag)
        
        // bind results to  {
        accountViewModel.loginEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.btnResetAccount?.isEnabled = valid
                if(valid == true){
                    let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
                    self?.btnResetAccount?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
                    self?.btnResetAccount?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
                    self?.btnResetAccount?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
                }
                else{
                    let gradientColor = [UIColor(red: CGFloat(248.0)/255, green: CGFloat(248.0)/255, blue: CGFloat(248.0)/255, alpha: 1.0), UIColor(red: CGFloat(225.0)/255, green: CGFloat(234.0)/255, blue: CGFloat(238.0)/255, alpha: 1.0)]
                    self?.btnResetAccount?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.normal)
                    self?.btnResetAccount?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.highlighted)
                    self?.btnResetAccount?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
                }
            })
            .disposed(by: disposeBag)
        //
        accountViewModel.signingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.startLoading(message: NSLocalizedString("processing", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        //
        //
        accountViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                self?.makeToast(message:output.message, duration: 3.0, position: .top)
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.resetAction()
                        }
                        else{
                            let message = NSLocalizedString("send_request_for_reset_password_unsuccessfully", comment: "")
                            self?.makeToast(message:message, duration: 3.0, position: .top)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
