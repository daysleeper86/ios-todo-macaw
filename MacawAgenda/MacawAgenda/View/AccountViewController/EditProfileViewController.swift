//
//  EditProfileViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Photos
import Moya
import RxSwift
import KYCircularProgress
import NVActivityIndicatorView
import Material
import DTGradientButton

class EditProfileViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    //View
    var keyboardBounds : CGRect?
    var btnContinue : UIButton?
    var tbSettings : UITableView?
    var imvAvatar : UIImageView?
    var loadingIndicator : NVActivityIndicatorView?
//    lazy var loadingView : RSLoadingView = {
//        let loadingView = RSLoadingView()
//        loadingView.dimBackgroundColor = UIColor.black.withAlphaComponent(0.15)
//        return loadingView
//    }()
    // Background View
    var tfName = FloatingLabelTextFieldView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: FloatingLabelTextFieldView.heightTextFieldView))
    var tfEmail : FloatingLabelTextFieldView?
    var tfTeam = FloatingLabelTextFieldView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: FloatingLabelTextFieldView.heightTextFieldView))
    //Data
    let arraySettings : [[String : Any]] = [
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuName,Constant.keyValue:NSLocalizedString("your_name", comment: "")],
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuEmail,Constant.keyValue:NSLocalizedString("work_email_address", comment: "")],
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuTeamName,Constant.keyValue:NSLocalizedString("team_name", comment: "")],
    ]
    var imgAvatar : UIImage?
    //Can edit
    var isEditPermission : Bool = false
    //
    var subjectProjectName = BehaviorSubject<String>(value: AppSettings.sharedSingleton.project?.name ?? "")
    var subjectUserName = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.name ?? "")
    //
    var messageError : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        checkPermission()
        //
        initView()
        //
        bindToViewModel()
    }

    
    func addNotification() {
        //
        addNotification_UpdateProfile()
        addNotification_UpdateProject()
        addNotification_Upload()
    }
    
    func addNotification_UpdateProfile(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProfile(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_USER_INFO),
                                               object: nil)
    }
    
    @objc func updateProfile(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProfileUpdateInfoType{
                if(updatedType == .kProfileUpdateAvatar){
                    startStopIndicator(false)
                    loadAvatar()
                }
                else if(updatedType == .kProfileUpdateInfo){
                    reloadUserName()
                }
                else if(updatedType == .kProfileUpdateAll){
                    startStopIndicator(false)
                    loadAvatar()
                    reloadUserName()
                }
            }
        }
    }
    
    func addNotification_UpdateProject(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProject(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_INFO),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProject(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_OWNER_TYPE),
                                               object: nil)
    }
    
    @objc func updateProject(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProjectUpdateInfoType{
                if(updatedType == .kProjecUpdateInfo){
                    reloadProjectName()
                }
                else if(updatedType == .kProjecUpdateOwner){
                    checkPermission()
                    reloadProjectName()
                }
            }
        }
    }
    
    func addNotification_Upload() {
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadAvatar_Fail(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_FAIL_AVATAR),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadAvatar_Successful(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_SUCCESSFULLY_AVATAR),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadAvatar_Processing(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_PROCESSING_AVATAR),
                                               object: nil)
    }
    
    @objc func uploadAvatar_Fail(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if (dictionary[SettingString.UserId] as? String) != nil{
                startStopIndicator(false)
            }
        }
    }
    
    @objc func uploadAvatar_Successful(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if (dictionary[SettingString.UserId] as? String) != nil{
                //
                let userId = AppSettings.sharedSingleton.account?.userId ?? ""
                updateAvatarChanged(userId)
            }
        }
    }
    
    @objc func uploadAvatar_Processing(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if (dictionary[SettingString.UserId] as? String) != nil{
                if let progress = dictionary[SettingString.Progress] as? Double{
                    //
                    startStopIndicator(true)
                }
            }
        }
    }
    
    func removeAllNotification(){
        NotificationCenter.default.removeObserver(self)
    }
    
    func startStopIndicator(_ start: Bool){
        if(start == true){
            loadingIndicator?.isHidden = false
            loadingIndicator?.startAnimating()
        }
        else{
            loadingIndicator?.isHidden = true
            loadingIndicator?.stopAnimating()
        }
    }
    
    
    
    
    func startIncicator(){
//        guard let parentView = tbSettings else { return }
//        self.loadingView.show(on: parentView)
        startLoading(message: NSLocalizedString("updating", comment: ""))
    }
    
    func stopIncicator(){
//        self.loadingView.hide()
        stopLoading()
    }
    

    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnContinue = UIButton.init(type: .custom)
        btnContinue?.accessibilityIdentifier = AccessiblityId.BTN_SAVE
        btnContinue?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnContinue?.setTitle(NSLocalizedString("save", comment:"").uppercased(), for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnContinue?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnContinue?.titleLabel?.textAlignment = .right
        btnContinue?.contentHorizontalAlignment = .right
        navigationView.addSubview(btnContinue!)
        
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("profile", comment: "")
        navigationView.titleView(titleView: lbTitleHeader)
        navigationView.showHideTitleNavigation(isShow: true)
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableSetting : CGFloat = 0
        let yTableSetting : CGFloat = yHeader
        let wTableSetting : CGFloat = AppDevice.ScreenComponent.Width
        let hTableSetting : CGFloat = AppDevice.ScreenComponent.Height - yTableSetting
        
        tbSettings = UITableView(frame: CGRect(x: xTableSetting, y: yTableSetting, width: wTableSetting, height: hTableSetting))
        tbSettings?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.dataSource = self
        tbSettings?.delegate = self
        self.view.addSubview(tbSettings!)
        
    }
    
    func resignAllTextField(){
        if(tfName.textField?.isFirstResponder == true){
            
        }
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Tap Background Event
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackground))
        self.view.addGestureRecognizer(tapGesture)
        
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    func stopUpdatingAvatar(){
        
    }
    
    func updateAvatarChanged(_ userId: String){
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let accountController = AccountAPIController(provider: accountProvider)
        
        let accountViewModel = AccountAPIViewModel(
            inputUpdateProfileAvatar: (
                userId: userId,
                changedAvatar: BehaviorSubject<Bool>(value: true),
                controller: accountController
            )
        )
        
        //
        accountViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopUpdatingAvatar()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.makeToast(message:NSLocalizedString("update_avatar_successfully", comment: ""), duration: 3.0, position: .top)
                            //Clear temp data image
                            self?.imgAvatar = nil
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("update_avatar_unsuccessfully", comment: ""), duration: 3.0, position: .top)
                        }
                        //
                        self?.stopUpdatingAvatar()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func bindToViewModel(){
        //
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        
        let projectAccountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let projectController = ProjectAPIController(provider: projectAccountProvider)
        let accountController = AccountAPIController(provider: projectAccountProvider)
        
        let observableTap = btnContinue!.rx.tap.asObservable()
        let projectViewModel = ProjectAPIViewModel(
            inputUpdateProject: (
                projectId: projectId,
                oldName: subjectProjectName.asObservable(),
                name: tfTeam.textField!.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                controller: projectController
            )
        )
        
        projectViewModel.validatedName?.subscribe(onNext: { [weak self] valid  in
            guard let strongSelf = self,let textField = strongSelf.tfTeam.textField else { return }
            switch valid {
            case .failed(let message):
                textField.errorMessage = message
            case .empty:
                textField.errorMessage = NSLocalizedString("the_project_name_is_required", comment: "")
            case .ok(_),
                 .validating:
                textField.errorMessage = ""
            }
        })
            .disposed(by: disposeBag)
        
        let accountViewModel = AccountAPIViewModel(
            inputUpdateProfileName: (
                userId: userId,
                oldName: subjectUserName.asObservable(),
                name: tfName.textField!.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                controller: accountController
            )
        )
        
        accountViewModel.validatedName?.subscribe(onNext: { [weak self] valid  in
            guard let strongSelf = self,let textField = strongSelf.tfName.textField else { return }
            switch valid {
            case .failed(let message):
                textField.errorMessage = message
            case .empty:
                textField.errorMessage = NSLocalizedString("the_name_is_required", comment: "")
            case .ok(_),
                 .validating:
                textField.errorMessage = ""
            }
        })
            .disposed(by: disposeBag)
        
        let loginEnabled = Observable.combineLatest(
            projectViewModel.processEnabled!,
            accountViewModel.loginEnabled!,
            projectViewModel.validatedName!,
            accountViewModel.validatedName!)
            { projectLoginEnabled,accountLoginEnabled,
                projectValidatedName,accountValidatedName -> Bool in
                if(projectLoginEnabled || accountLoginEnabled){
                    var projectValided = false
                    switch projectValidatedName {
                        case .ok(_):
                            projectValided = true
                        default:
                            break
                    }
                    var accountValided = false
                    switch accountValidatedName {
                    case .ok(_):
                        accountValided = true
                    default:
                        break
                    }
                    if(projectValided && accountValided){
                        return true
                    }
                }
                return false
            }
            .distinctUntilChanged()
        
        loginEnabled
            .subscribe(onNext: { [weak self] valid  in
                self?.enableDoneButton(valid)
            })
            .disposed(by: disposeBag)
        
        
        let signingIn = Observable.combineLatest(
            projectViewModel.processingIn!,
            accountViewModel.signingIn)
        { projectSigningIn,accountSigningIn in
            projectSigningIn ||
            accountSigningIn
            }
            .distinctUntilChanged()
        
        signingIn
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    self?.didTapBackground()
                    strongSelf.startLoading(message: NSLocalizedString("updating", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        //
        let signedIn = Observable
            .combineLatest(projectViewModel.processFinished!, accountViewModel.signedIn!) { (projectSignedIn: $0, accountSignedIn: $1) }
            .share(replay: 1)
        signedIn
            .subscribe(onNext: {[weak self] dataSignedIn  in
                switch dataSignedIn.projectSignedIn {
                case .failed(let output):
                    let appDelegate = AppDelegate().sharedInstance()
                    if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                        self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    }
                    
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        guard let strongSelf = self else { return }
                        if(reponseAPI.code == 200){
                            strongSelf.messageError = NSLocalizedString("update_profile_successfully", comment: "")
                            //
//                            if let projectName = strongSelf.tfTeam.textField?.text{
//                                AppSettings.sharedSingleton.saveNameProject(projectName)
//                            }
                        }
                        else{
                            strongSelf.messageError = NSLocalizedString("update_profile_unsuccessfully", comment: "")
                        }
                    }
                }
                //
                switch dataSignedIn.accountSignedIn {
                case .failed(let output):
                    let appDelegate = AppDelegate().sharedInstance()
                    if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                        self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    }
                    
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        guard let strongSelf = self else { return }
                        if(reponseAPI.code == 200){
                            strongSelf.messageError = NSLocalizedString("update_profile_successfully", comment: "")
                            //
//                            if let userName = strongSelf.tfName.textField?.text{
//                                AppSettings.sharedSingleton.saveNameUser(userName)
//                            }
                        }
                        else{
                            strongSelf.messageError = NSLocalizedString("update_profile_unsuccessfully", comment: "")
                        }
                    }
                }
            })
            .disposed(by: disposeBag)
        
        let finishedSignedIn = signedIn
            .flatMapLatest{ dataSignedIn -> Observable<Bool> in
                if(dataSignedIn.projectSignedIn == dataSignedIn.accountSignedIn){
                    var successProjectSignedIn = false
                    switch dataSignedIn.projectSignedIn {
                    case .ok(let output):
                        if (output as? ResponseAPI) == nil{successProjectSignedIn = true}
                    default:
                        break
                    }
                    //
                    var successAccountSignedIn = false
                    switch dataSignedIn.accountSignedIn {
                    case .ok(let output):
                        if (output as? ResponseAPI) == nil{successAccountSignedIn = true}
                    default:
                        break
                    }
                    //
                    if(successProjectSignedIn && successAccountSignedIn){return Observable.just(true)}
                }
                return Observable.just(false)
            }
            .distinctUntilChanged()
        
        //
        finishedSignedIn
            .subscribe(onNext: {[weak self] finish  in
                guard let strongSelf = self else { return }
                if(finish == true){
                    if(strongSelf.messageError.count > 0){
                        strongSelf.makeToast(message:strongSelf.messageError, duration: 3.0, position: .top)
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    @objc func didTapBackground(){
        if(self.tfName.textField?.isFirstResponder == true){
            self.tfName.textField?.resignFirstResponder()
        }
        else if(self.tfEmail?.textField?.isFirstResponder == true){
            self.tfEmail?.textField?.resignFirstResponder()
        }
        else if(self.tfTeam.textField?.isFirstResponder == true){
            self.tfTeam.textField?.resignFirstResponder()
        }
    }
    
    @objc func closeAction(){
        self.isEditing = false
        //
        var isDiscard : Bool = false
        if(imgAvatar != nil){
            isDiscard = true
        }
        //
        if(isDiscard == false){
            let strName = self.tfName.textField?.text
            let userName = AppSettings.sharedSingleton.account?.name ?? ""
            if(strName != userName){
                isDiscard = true
            }
            else{
                let strTeam = self.tfTeam.textField?.text
                let userTeamName = AppSettings.sharedSingleton.project?.name ?? ""
                if(strTeam != userTeamName){
                    isDiscard = true
                }
            }
        }
        //
        if(isDiscard == true){
            let ac = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            ac.view.tag = Constant.AlertType.kAlertTypeConfirm.rawValue
            ac.addAction(UIAlertAction(title: NSLocalizedString("discard", comment: ""), style: .destructive, handler: {
            [weak self] (action) in
            self?.navigationController?.popViewController(animated: true)
            }))
            ac.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel))
            present(ac, animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func enableDoneButton(_ valid: Bool){
        btnContinue?.isEnabled = valid
        if(valid == true){
            btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
        }
        else{
            btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let hHeader : CGFloat = AppDevice.SubViewFrame.EditUserHeaderPaddingTop + AppDevice.SubViewFrame.EditUserHeaderHeight + AppDevice.SubViewFrame.EditUserHeaderPaddingTop
        return hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let xHeader : CGFloat = 0
        let yHeader : CGFloat = 0
        let wHeader : CGFloat = tableView.frame.size.width - (xHeader * 2)
        let hHeader : CGFloat = AppDevice.SubViewFrame.EditUserHeaderPaddingTop + AppDevice.SubViewFrame.EditUserHeaderHeight + AppDevice.SubViewFrame.EditUserHeaderPaddingTop
        
        let headerView = UIView(frame: CGRect(x: xHeader, y: yHeader, width: wHeader, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let xUserView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yUserView : CGFloat = AppDevice.SubViewFrame.EditUserHeaderPaddingTop
        let wUserView : CGFloat = headerView.frame.size.width - (xUserView * 2)
        let hUserView : CGFloat = AppDevice.SubViewFrame.EditUserHeaderHeight
        let userView = UIView(frame: CGRect(x: xUserView, y: yUserView, width: wUserView, height: hUserView))
        headerView.isUserInteractionEnabled = true
        headerView.addSubview(userView)
        
        let hAvatar : CGFloat = hUserView
        let wAvatar : CGFloat = hAvatar
        let xAvatar : CGFloat = 0
        let yAvatar : CGFloat = 0
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.contentMode = .scaleAspectFill
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        userView.addSubview(imvAvatar!)
        
        //Default library line width = 8, if set lineWidth = 4 => padding = 4 => What the library?
        let lineWidthLoadingIndicator : CGFloat = 2.0
        let hLoadingIndicator : CGFloat = wAvatar + (lineWidthLoadingIndicator * 2)
        let wLoadingIndicator : CGFloat = hAvatar + (lineWidthLoadingIndicator * 2)
        let xLoadingIndicator : CGFloat = xUserView - lineWidthLoadingIndicator
        let yLoadingIndicator : CGFloat = yUserView + yAvatar - lineWidthLoadingIndicator
        // Loading indicator
        loadingIndicator = NVActivityIndicatorView(frame: CGRect(x: xLoadingIndicator, y: yLoadingIndicator, width: wLoadingIndicator, height: hLoadingIndicator),
                                              type: .circleStrokeSpin)
        loadingIndicator?.color = AppColor.NormalColors.BLUE_COLOR
        loadingIndicator?.isHidden = true
        headerView.addSubview(loadingIndicator!)
        //Title Screen
        let pChangeAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xChangeAvatar : CGFloat = xAvatar + wAvatar + pChangeAvatar
        let wChangeAvatar : CGFloat = 160
        let hChangeAvatar : CGFloat = AppDevice.ScreenComponent.ButtonHeight
        let yChangeAvatar : CGFloat = (userView.frame.size.height - hChangeAvatar ) / 2
        
        
        let btnChangeAvatar = RaisedButton.init(type: .custom)
        btnChangeAvatar.pulseColor = .white
        btnChangeAvatar.frame = CGRect(x: xChangeAvatar, y: yChangeAvatar, width: wChangeAvatar, height: hChangeAvatar)
        btnChangeAvatar.addTarget(self, action: #selector(changeAvatarAction), for: .touchUpInside)
        btnChangeAvatar.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
        btnChangeAvatar.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        btnChangeAvatar.titleLabel?.textAlignment = .center
        btnChangeAvatar.layer.cornerRadius = 4.0
        btnChangeAvatar.layer.masksToBounds = true
        let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
        btnChangeAvatar.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
        btnChangeAvatar.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
        btnChangeAvatar.setTitle(NSLocalizedString("change_avatar", comment:"").uppercased(), for: .normal)
        userView.addSubview(btnChangeAvatar)
        
        //
        setDefaultValue()
        
        return headerView
        
    }
    
    func setDefaultValue(){
        //
        loadAvatar()
    }
    
    func loadAvatar(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let userName = AppSettings.sharedSingleton.account?.name ?? ""
        var user = User(userId: userId, dataType: .normal)
        user.name = userName
        //
        imvAvatar?.loadAvatarForUser(user: user)
    }
    
    func reloadUserName(){
        let userName = AppSettings.sharedSingleton.account?.name ?? ""
        self.tfName.textField?.text = userName
        self.tfName.textField?.sendActions(for: .valueChanged)
        self.subjectUserName.onNext(userName)
    }
    
    func checkPermission(){
        //
        let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectOwnedId = AppSettings.sharedSingleton.project?.owned.userId ?? ""
        
        var edited : Bool = false
        if(myUserId.count > 0 && projectOwnedId.count > 0){
            if(myUserId == projectOwnedId){
                edited = true
            }
        }
        if(edited == false){
            let orangnizationOwnedId = AppSettings.sharedSingleton.orangnization?.owner.userId ?? ""
            if(myUserId.count > 0 && orangnizationOwnedId.count > 0){
                if(myUserId == orangnizationOwnedId){
                    edited = true
                }
            }
        }
        //
        isEditPermission = edited
    }
    
    func reloadProjectName(){
        let userTeam = AppSettings.sharedSingleton.project?.name ?? ""
        self.tfTeam.textField?.text = userTeam
        //
        if(isEditPermission == true){
            self.tfTeam.isUserInteractionEnabled = true
//            self.tfTeam.alpha = 1.0
            self.tfTeam.textField?.sendActions(for: .valueChanged)
            self.subjectProjectName.onNext(userTeam)
        }
        else{
            self.tfTeam.isUserInteractionEnabled = false
//            self.tfTeam.alpha = 0.4
            self.tfTeam.textField?.sendActions(for: .valueChanged)
        }
    }
    
    @objc func changeAvatarAction(){
        requestAlbumPhoto()
    }
    
    func uploadImageWhenChoosen(image: UIImage?){
        if let strongImage = image{
            imvAvatar?.setLabelTextForAvatar(image: strongImage, text: "", identify: "")
            imgAvatar = strongImage
            //
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            if(imgAvatar != nil){
                UploadManager.sharedSingleton.addAvatar(imageAvatar: imgAvatar!, userId: userId)
            }
        }
    }

    
    func requestAlbumPhoto(){
        //Check permission for access photo library
        let status : PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if (status == .notDetermined){
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ status in
                let nnvc = EWPhotoPickerViewController(photoDelegate: self)
                nnvc.modalPresentationStyle = .fullScreen //or .overFullScreen
                
                self.present(nnvc, animated: true, completion: nil)
            })
        }
        else{
            let nnvc = EWPhotoPickerViewController(photoDelegate: self)
            nnvc.modalPresentationStyle = .fullScreen //or .overFullScreen
            
            self.present(nnvc, animated: true, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FloatingLabelTextFieldView.heightTextFieldView + FloatingLabelTextFieldView.paddingTopTextField
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        //
        let profile = arraySettings[indexPath.row]
        let strName = profile[Constant.keyValue] as? String ?? ""
        let key = profile[Constant.keyId] as? Constant.ProfileMenu ?? .kProfileMenuName
        
        let hTextFieldFrame : CGFloat = FloatingLabelTextFieldView.heightTextFieldView
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = tableView.frame.size.width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = hTextFieldFrame + FloatingLabelTextFieldView.paddingTopTextField
        
        let xTextFieldFrame : CGFloat = 0
        let yTextFieldFrame : CGFloat = 0
        let wTextFieldFrame : CGFloat = wBackgroundView
        
        
        let backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        cell.addSubview(backgroundViewCell)
        
        if(key == .kProfileMenuName){
            self.tfName.textField?.clearButtonMode = .whileEditing
            self.tfName.textField?.placeholder = NSLocalizedString("enter_your_name", comment:"")
            self.tfName.textField?.title = strName.uppercased()
            backgroundViewCell.addSubview(self.tfName)
            //Set data
            reloadUserName()
            
        
        }
        else if(key == .kProfileMenuEmail){
            if(self.tfEmail == nil){
                self.tfEmail = FloatingLabelTextFieldView(frame: CGRect(x: xTextFieldFrame, y: yTextFieldFrame, width: wTextFieldFrame, height: hTextFieldFrame))
                self.tfEmail?.isUserInteractionEnabled = false
                self.tfEmail?.textField?.alpha = 0.7
                self.tfEmail?.textField?.placeholder = NSLocalizedString("enter_email_address", comment:"")
                self.tfEmail?.textField?.title = strName.uppercased()
                //Set data
                let userEmail = AppSettings.sharedSingleton.account?.email ?? ""
                self.tfEmail?.textField?.text = userEmail
            }
            if(self.tfEmail != nil){
                backgroundViewCell.addSubview(self.tfEmail!)
            }
        }
        else if(key == .kProfileMenuTeamName){
            self.tfTeam.textField?.clearButtonMode = .whileEditing
            self.tfTeam.textField?.placeholder = NSLocalizedString("enter_team_name", comment:"")
            self.tfTeam.textField?.title = strName.uppercased()
            //Set data
            reloadProjectName()
            backgroundViewCell.addSubview(self.tfTeam)
        }
        
        return cell
    }
    
    deinit {
        removeAllNotification()
    }

}

extension EditProfileViewController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == self.tfName || textField == self.tfTeam){
            if var text = textField.text{
                if (string == "\n") {
                    textField.resignFirstResponder()
                    return false
                }
                if (text.count + (string.count - range.length) > Constant.MaxLength.Name) {
                    let lengthOfReplacementText = 4 - text.count + range.length
                    
                    if (lengthOfReplacementText < 0) {
                        return false
                    }
                    
                    let newText = text.subString(from: 0, to: lengthOfReplacementText)
                    let strFirst = text.subString(from: 0, to: range.location)
                    let strLast = text.subString(from: range.location, to: text.count - 1)
                    
                    text = strFirst + newText + strLast
                    textField.text = text;
                    
                    return false
                }
            }
        }
        //
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        if(textField == self.tfName.textField){
            enableDoneButton(false)
        }
        return true
    }
}

extension EditProfileViewController: EWImageCropperDelegate {
    func imageCropper(_ cropperViewController: UIViewController, didFinished editImg: UIImage, sourceType source: UIImagePickerController.SourceType) {
        cropperViewController.navigationController?.dismiss(animated: true, completion: nil)
        ///对选取并编辑后的图片直接使用
        uploadImageWhenChoosen(image: editImg)
    }
}
