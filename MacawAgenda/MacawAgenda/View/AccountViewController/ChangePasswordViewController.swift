//
//  ChangePasswordViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Material

class ChangePasswordViewController: BaseViewController {
    //View
    var keyboardBounds : CGRect?
    var btnContinue : UIButton?
    var tfCurrentPass : FloatingLabelTextFieldView?
    var tfNewPass : FloatingLabelTextFieldView?
    var tfNewConfirmNewPass : FloatingLabelTextFieldView?
    var lbErrorCurrentPass : UILabel?
    var lbErrorNewPass : UILabel?
    var lbErrorConfirmNewPassass : UILabel?
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnContinue = UIButton.init(type: .custom)
        btnContinue?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnContinue?.setTitle(NSLocalizedString("save", comment:"").uppercased(), for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnContinue?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnContinue?.titleLabel?.textAlignment = .right
        btnContinue?.contentHorizontalAlignment = .right
        btnContinue?.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        navigationView.addSubview(btnContinue!)
        
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("change_password", comment: "")
        navigationView.titleView(titleView: lbTitleHeader)
        navigationView.showHideTitleNavigation(isShow: true)
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        let bgView = UIScrollView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        self.view.addSubview(bgView)
        
        var yScreenView : CGFloat = 36
        
        let heightTextField : CGFloat = FloatingLabelTextFieldView.heightTextFieldView
        let paddingBottomTextField : CGFloat = 8
        
        let paddingErrorLabel : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let heightErrorLabel : CGFloat = AppDevice.SubViewFrame.TitleHeight
        
        //If user login with Social, check the first change password
        if(AppSettings.sharedSingleton.account?.password.count ?? 0 > 0){
            //
            let xCurrentPass : CGFloat = 0
            let yCurrentPass : CGFloat = yScreenView
            let wCurrentPass : CGFloat = bgView.frame.size.width
            let hCurrentPass : CGFloat = heightTextField
            
            tfCurrentPass = FloatingLabelTextFieldView(frame: CGRect(x: xCurrentPass, y: yCurrentPass, width: wCurrentPass, height: hCurrentPass))
            tfCurrentPass?.textField?.placeholder = NSLocalizedString("enter_current_password", comment: "")
            tfCurrentPass?.textField?.title = NSLocalizedString("current_password", comment: "").uppercased()
            tfCurrentPass?.secureTextEntry = true
            bgView.addSubview(tfCurrentPass!)
            
            yScreenView += hCurrentPass
            yScreenView += paddingBottomTextField
            
            //Label error email
            let xErrorCurrentPass : CGFloat = paddingErrorLabel
            let yErrorCurrentPass : CGFloat = yScreenView
            let wErrorCurrentPass : CGFloat = wBGView - (xErrorCurrentPass * 2)
            let hErrorCurrentPass : CGFloat = heightErrorLabel
            let pBottomErrorCurrentPass : CGFloat = 0
            
            lbErrorCurrentPass = UILabel(frame: CGRect(x: xErrorCurrentPass, y: yErrorCurrentPass, width: wErrorCurrentPass, height: hErrorCurrentPass))
            lbErrorCurrentPass?.textAlignment = .left
            lbErrorCurrentPass?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
            lbErrorCurrentPass?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
            bgView.addSubview(lbErrorCurrentPass!)
            
            yScreenView += hErrorCurrentPass
            yScreenView += pBottomErrorCurrentPass
        }
        
        //New Password
        let xNewPass : CGFloat = 0
        let yNewPass : CGFloat = yScreenView
        let wNewPass : CGFloat = bgView.frame.size.width
        let hNewPass : CGFloat = heightTextField
        
        tfNewPass = FloatingLabelTextFieldView(frame: CGRect(x: xNewPass, y: yNewPass, width: wNewPass, height: hNewPass))
        tfNewPass?.textField?.placeholder = NSLocalizedString("enter_new_password", comment: "")
        tfNewPass?.textField?.title = NSLocalizedString("new_password", comment: "").uppercased()
        tfNewPass?.secureTextEntry = true
        bgView.addSubview(tfNewPass!)
        
        yScreenView += hNewPass
        yScreenView += paddingBottomTextField
        
        //Label error email
        let xErrorNewPass : CGFloat = paddingErrorLabel
        let yErrorNewPass : CGFloat = yScreenView
        let wErrorNewPass : CGFloat = wBGView - (xErrorNewPass * 2)
        let hErrorNewPass : CGFloat = heightErrorLabel
        let pBottomErrorNewPass : CGFloat = 0
        
        lbErrorNewPass = UILabel(frame: CGRect(x: xErrorNewPass, y: yErrorNewPass, width: wErrorNewPass, height: hErrorNewPass))
        lbErrorNewPass?.textAlignment = .left
        lbErrorNewPass?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
        lbErrorNewPass?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        bgView.addSubview(lbErrorNewPass!)
        
        yScreenView += hErrorNewPass
        yScreenView += pBottomErrorNewPass
        
        //Confirm Password
        let xConfirmNewPass : CGFloat = 0
        let yConfirmNewPass : CGFloat = yScreenView
        let wConfirmNewPass : CGFloat = bgView.frame.size.width
        let hConfirmNewPass : CGFloat = heightTextField
        
        tfNewConfirmNewPass = FloatingLabelTextFieldView(frame: CGRect(x: xConfirmNewPass, y: yConfirmNewPass, width: wConfirmNewPass, height: hConfirmNewPass))
        tfNewConfirmNewPass?.textField?.placeholder = NSLocalizedString("re_enter_new_password", comment: "")
        tfNewConfirmNewPass?.textField?.title = NSLocalizedString("confirm_new_password", comment: "").uppercased()
        tfNewConfirmNewPass?.secureTextEntry = true
        bgView.addSubview(tfNewConfirmNewPass!)
        
        //
        yScreenView += hConfirmNewPass
        yScreenView += paddingBottomTextField
        
        //Label error email
        let xErrorConfirmNewPass : CGFloat = paddingErrorLabel
        let yErrorConfirmNewPass : CGFloat = yScreenView
        let wErrorConfirmNewPass : CGFloat = wBGView - (xErrorConfirmNewPass * 2)
        let hErrorConfirmNewPass : CGFloat = heightErrorLabel
        let pBottomErrorConfirmNewPass : CGFloat = 0
        
        lbErrorConfirmNewPassass = UILabel(frame: CGRect(x: xErrorConfirmNewPass, y: yErrorConfirmNewPass, width: wErrorConfirmNewPass, height: hErrorConfirmNewPass))
        lbErrorConfirmNewPassass?.textAlignment = .left
        lbErrorConfirmNewPassass?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
        lbErrorConfirmNewPassass?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        bgView.addSubview(lbErrorConfirmNewPassass!)
        
        yScreenView += hErrorConfirmNewPass
        yScreenView += pBottomErrorConfirmNewPass
        
        bgView.contentSize.height = yScreenView
    }
    
    //
    func resignAllTextField(){
        if(tfCurrentPass?.textField?.isFirstResponder == true){
            tfCurrentPass?.textField?.resignFirstResponder()
        }
        else if(tfNewPass?.textField?.isFirstResponder == true){
            tfNewPass?.textField?.resignFirstResponder()
        }
        if(tfCurrentPass?.textField?.isFirstResponder == true){
            tfCurrentPass?.textField?.resignFirstResponder()
        }
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        
    }
    
    func bindToViewModel(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let accountController = AccountAPIController(provider: accountProvider)
        //
        let viewModel = AccountAPIViewModel(
            inputChangePass: (
                userId: userId,
                password: tfCurrentPass?.textField?.rx.text.orEmpty.asObservable(),
                newPassword:tfNewPass!.textField!.rx.text.orEmpty.asObservable(),
                confirmNewPassword:tfNewConfirmNewPass!.textField!.rx.text.orEmpty.asObservable(),
                loginTaps: btnContinue!.rx.tap.asObservable(),
                controller: accountController
            )
        )
        
        viewModel.validatedPassword?.subscribe(onNext: { [weak self] valid  in
            self?.lbErrorCurrentPass?.isHidden = valid.isValid
            switch valid {
            case .failed(let message):
                guard let strongSelf = self, let text = strongSelf.tfCurrentPass?.textField?.text else { return }
                if(text.count > 0){
                    strongSelf.lbErrorCurrentPass?.text = message
                    let color = AppColor.NormalColors.LIGHT_RED_COLOR
                    strongSelf.tfCurrentPass?.textField?.selectedLineColor = color
                }
            case .ok(_),
                 .empty,
                 .validating:
                self?.lbErrorCurrentPass?.text = ""
                let color = AppColor.NormalColors.BLUE_COLOR
                self?.tfCurrentPass?.textField?.selectedLineColor = color
            }
        })
            .disposed(by: disposeBag)
        
        viewModel.validatedNewPassword?.subscribe(onNext: { [weak self] valid  in
            self?.lbErrorNewPass?.isHidden = valid.isValid
            switch valid {
            case .failed(let message):
                guard let strongSelf = self, let text = strongSelf.tfNewPass?.textField?.text else { return }
                if(text.count > 0){
                    strongSelf.lbErrorNewPass?.text = message
                    let color = AppColor.NormalColors.LIGHT_RED_COLOR
                    strongSelf.tfNewPass?.textField?.selectedLineColor = color
                }
            case .ok(_),
                 .empty,
                 .validating:
                self?.lbErrorNewPass?.text = ""
                let color = AppColor.NormalColors.BLUE_COLOR
                self?.tfNewPass?.textField?.selectedLineColor = color
            }
        })
            .disposed(by: disposeBag)
        
        viewModel.validatedConfirmPassword?.subscribe(onNext: { [weak self] valid  in
            self?.lbErrorConfirmNewPassass?.isHidden = valid.isValid
            switch valid {
            case .failed(let message):
                guard let strongSelf = self, let text = strongSelf.tfNewConfirmNewPass?.textField?.text else { return }
                if(text.count > 0){
                    strongSelf.lbErrorConfirmNewPassass?.text = message
                    let color = AppColor.NormalColors.LIGHT_RED_COLOR
                    strongSelf.tfNewConfirmNewPass?.textField?.selectedLineColor = color
                }
            case .ok(_),
                 .empty,
                 .validating:
                self?.lbErrorConfirmNewPassass?.text = ""
                let color = AppColor.NormalColors.BLUE_COLOR
                self?.tfNewConfirmNewPass?.textField?.selectedLineColor = color
            }
        })
            .disposed(by: disposeBag)
        
        viewModel.loginEnabled?.subscribe(onNext: { [weak self] valid  in
            self?.btnContinue?.isEnabled = valid
            if(valid == true){
                self?.btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
            }
            else{
                self?.btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
            }
        })
            .disposed(by: disposeBag)
        
        viewModel.signingIn
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.resignAllTextField()
                    strongSelf.startLoading(message: NSLocalizedString("authenticating", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.btnContinue?.isUserInteractionEnabled = false
                            //
                            let message = NSLocalizedString("change_password_successfully", comment: "")
                            AppDelegate().sharedInstance().makeSystemToast(message: message)
                            //
                            self?.closeAction()
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("change_password_unsuccessfully", comment: ""), duration: 3.0, position: .top)
                        }
                        //
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        //Tap Background Event
        let tapBackground = UITapGestureRecognizer()
        tapBackground.rx.event
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
        view.addGestureRecognizer(tapBackground)
        
    }
    
    @objc func didTapBackground(){
        
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func continueAction(){
        
    }

    

}
