//
//  SignUpEmailViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Material


class SignUpEmailViewController: BaseViewController {

    //Views
    var keyboardBounds : CGRect?
    var btnContinue : UIButton?
    var email : String?
    var tfInputName : FloatingLabelTextFieldView?
    var tfInputPass : FloatingLabelTextFieldView?
    
    var lbErrorName : UILabel?
    var lbErrorPassword : UILabel?
    //Data
    var duration : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.accessibilityIdentifier = AccessiblityId.BTN_BACK
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 150
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnContinue = UIButton.init(type: .custom)
        btnContinue?.accessibilityIdentifier = AccessiblityId.BTN_SIGNUP
        btnContinue?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnContinue?.setTitle(NSLocalizedString("create_account", comment:"").uppercased(), for: .normal)
        btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnContinue?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnContinue?.titleLabel?.textAlignment = .right
        btnContinue?.contentHorizontalAlignment = .right
        navigationView.addSubview(btnContinue!)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        let bgView = UIScrollView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        self.view.addSubview(bgView)
        
        var yScreenView : CGFloat = 0
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yScreenView
        let wTitleScreen : CGFloat = wBGView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = 40
        let pBottomTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("create_a_account", comment: "")
        bgView.addSubview(lbTitleScreen)
        
        yScreenView += hTitleScreen
        yScreenView += pBottomTitleScreen
        
        //Title Email
        let pTitleEmailAdress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleEmailAdress : CGFloat = pTitleEmailAdress
        let yTitleEmailAdress : CGFloat = yScreenView
        let wTitleEmailAdress : CGFloat = wBGView - (xTitleEmailAdress * 2)
        let hTitleEmailAdress : CGFloat = 24
        let pBottomTitleEmailAdress : CGFloat = 24
        
        let lbTitleEmailAdress : UILabel = UILabel(frame: CGRect(x: xTitleEmailAdress, y: yTitleEmailAdress, width: wTitleEmailAdress, height: hTitleEmailAdress))
        lbTitleEmailAdress.accessibilityIdentifier = AccessiblityId.LB_EMAIL
        lbTitleEmailAdress.textAlignment = .left
        lbTitleEmailAdress.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleEmailAdress.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 10)
        bgView.addSubview(lbTitleEmailAdress)
        
        if let email = self.email{
            let strTitleMail = NSLocalizedString("registered_email", comment: "")
            let strTextMail =  email
            let strMail = strTitleMail + " " + strTextMail;
            lbTitleEmailAdress.text = strMail;
            //
            
            let attributedString = NSMutableAttributedString(string: lbTitleEmailAdress.text!)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 12) ?? UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold), range: NSRange(location: 0, length: strTitleMail.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: CGFloat(149.0)/255, green: CGFloat(149.0)/255, blue: CGFloat(149.0)/255, alpha: 1), range: NSRange(location: 0, length: strTitleMail.count))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD_ITALIC, size: 14) ?? UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold), range: NSRange(location: strTitleMail.count+1, length: strTextMail.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: strTitleMail.count+1, length: strTextMail.count))
            
            lbTitleEmailAdress.attributedText = attributedString
        }
        
        yScreenView += hTitleEmailAdress
        yScreenView += pBottomTitleEmailAdress
        
        let heightTextField : CGFloat = FloatingLabelTextFieldView.heightTextFieldView
        //
        let xInputName : CGFloat = 0
        let yInputName : CGFloat = yScreenView
        let wInputName : CGFloat = bgView.frame.size.width
        let hInputName : CGFloat = heightTextField
        let pBottomInputName : CGFloat = 8
        
        tfInputName = FloatingLabelTextFieldView(frame: CGRect(x: xInputName, y: yInputName, width: wInputName, height: hInputName))
        tfInputName?.textField?.accessibilityIdentifier = AccessiblityId.TF_NAME
        tfInputName?.textField?.placeholder = NSLocalizedString("enter_your_name", comment: "")
        tfInputName?.textField?.title = NSLocalizedString("your_name", comment: "").uppercased()
        bgView.addSubview(tfInputName!)
        
        yScreenView += hInputName
        yScreenView += pBottomInputName
        
        //Label error email
        let pErrorName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xErrorName : CGFloat = pErrorName
        let yErrorName : CGFloat = yScreenView
        let wErrorName : CGFloat = wBGView - (xErrorName * 2)
        let hErrorName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let pBottomErrorName : CGFloat = 0
        
        lbErrorName = UILabel(frame: CGRect(x: xErrorName, y: yErrorName, width: wErrorName, height: hErrorName))
        lbErrorName?.accessibilityIdentifier = AccessiblityId.LB_ERROR_NAME
        lbErrorName?.textAlignment = .left
        lbErrorName?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
        lbErrorName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        lbErrorName?.text = NSLocalizedString("please_use_a_valid_name", comment: "")
        bgView.addSubview(lbErrorName!)
    
        yScreenView += hErrorName
        yScreenView += pBottomErrorName
        
        let xInputPass : CGFloat = 0
        let yInputPass : CGFloat = yScreenView
        let wInputPass : CGFloat = bgView.frame.size.width
        let hInputPass : CGFloat = heightTextField
        let pBottomInputPass : CGFloat = 8
        
        tfInputPass = FloatingLabelTextFieldView(frame: CGRect(x: xInputPass, y: yInputPass, width: wInputPass, height: hInputPass))
        tfInputPass?.textField?.accessibilityIdentifier = AccessiblityId.TF_PASSSWORD
        tfInputPass?.textField?.placeholder = NSLocalizedString("at_least_8_character", comment: "")
        tfInputPass?.textField?.title = NSLocalizedString("password", comment: "")
        tfInputPass?.secureTextEntry = true
        bgView.addSubview(tfInputPass!)
        
        yScreenView += hInputPass
        yScreenView += pBottomInputPass
        
        //Label error email
        let pErrorPass : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xErrorPass : CGFloat = pErrorPass
        let yErrorPass : CGFloat = yScreenView
        let wErrorPass : CGFloat = wBGView - (xErrorPass * 2)
        let hErrorPass : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let pBottomErrorPass : CGFloat = 0
        
        lbErrorPassword = UILabel(frame: CGRect(x: xErrorPass, y: yErrorPass, width: wErrorPass, height: hErrorPass))
        lbErrorPassword?.accessibilityIdentifier = AccessiblityId.LB_ERROR_PASSSWORD
        lbErrorPassword?.textAlignment = .left
        lbErrorPassword?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
        lbErrorPassword?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        bgView.addSubview(lbErrorPassword!)
        
        yScreenView += hErrorPass
        yScreenView += pBottomErrorPass
        
        //
        tfInputName?.textField?.becomeFirstResponder()
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func continueAction(){
        
    }
    
    func bindToViewModel(){
        let provider = MoyaProvider<AppAPI>()
        let accountController = AccountAPIController(provider: provider)
        let observableTap = btnContinue!.rx.tap.asObservable()
        let accountViewModel = AccountViewModel(
            inputSignUpEmail: (
                email: .just(self.email!),
                name:tfInputName!.textField!.rx.text.orEmpty.asObservable(),
                password:tfInputPass!.textField!.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                controller: accountController
            )
        )
        
         
        accountViewModel.validatedName?.subscribe(onNext: { [weak self] valid  in
            self?.lbErrorName?.isHidden = valid.isValid
            switch valid {
            case .failed(let message):
                self?.lbErrorName?.text = message
                //
                let color = AppColor.NormalColors.LIGHT_RED_COLOR
                self?.tfInputName?.textField?.selectedLineColor = color
            case .ok(_),
                 .empty,
                 .validating:
                self?.lbErrorName?.text = ""
                //
                let color = AppColor.NormalColors.BLUE_COLOR
                self?.tfInputName?.textField?.selectedLineColor = color
            }
        })
            .disposed(by: disposeBag)
        
        accountViewModel.validatedPassword?.subscribe(onNext: { [weak self] valid  in
            self?.lbErrorPassword?.isHidden = valid.isValid
            switch valid {
            case .failed(let message):
                guard let strongSelf = self, let text = strongSelf.tfInputPass?.textField?.text else { return }
                if(text.count > 0){
                    self?.lbErrorPassword?.text = message
                    //
                    let color = AppColor.NormalColors.LIGHT_RED_COLOR
                    self?.tfInputPass?.textField?.selectedLineColor = color
                }
                
            case .ok(_),
                 .empty,
                 .validating:
                self?.lbErrorPassword?.text = ""
                //
                let color = AppColor.NormalColors.BLUE_COLOR
                self?.tfInputPass?.textField?.selectedLineColor = color
            }
            
        })
            .disposed(by: disposeBag)
        
        // bind results to  {
        accountViewModel.loginEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.btnContinue?.isEnabled = valid
                if(valid == true){
                    self?.btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
                }
                else{
                    self?.btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
                }
            })
            .disposed(by: disposeBag)
        
        accountViewModel.signingIn
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.startLoading(message: NSLocalizedString("authenticating", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        accountViewModel.signupInMyServer?
            .subscribe(onNext: { [weak self] loginResult in
                switch loginResult {
                case .failed(let output):
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                case .ok(let output):
                    if let strongAccount = output as? Account{
                        AppSettings.sharedSingleton.saveAccount(strongAccount)
                        //
                        Tracker.trackUserSignUp(account: strongAccount,fromEmail: true)
                        //
                        if(strongAccount.defaultProject.count > 0){
                            let defaultProject = Project(projectId: strongAccount.defaultProject, name: "", dataType: .normal) //Design team
                            AppSettings.sharedSingleton.saveProject(defaultProject)
                            //
                            guard let strongSelf = self else { return }
                            strongSelf.launchWelcomeAction()
                        }
                        else{
                            guard let strongSelf = self else { return }
                            strongSelf.launchTeamInfoAction()
                        }
                        
                    }
                }
            }).disposed(by: disposeBag)
        
        //Tap Background Event
        let tapBackground = UITapGestureRecognizer()
        tapBackground.rx.event
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
        view.addGestureRecognizer(tapBackground)
        
    }
    
    func launchWelcomeAction(){
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        appDelegate.setupWelcomeView()
    }
    
    func launchTeamInfoAction(){
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        appDelegate.setupTeamInfoView()
    }
    
}
