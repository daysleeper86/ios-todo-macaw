//
//  TeamInformationViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/26/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Material

class TeamInformationViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    //Views
    var navigationView : CustomNavigationView?
    var tbMemberHeaderView : UIView?
    var tbMember : UITableView = UITableView(frame: .zero)
    var btnContinue : UIButton?
    var lbTeamType : UILabel?
    var tfTeamName : LPlaceholderTextView = LPlaceholderTextView(frame: .zero)
    var lbTeamSize : UILabel?
    //Data
    var subjectAccountInfo : PublishSubject<Account> = PublishSubject()
    var subjectEventStared : PublishSubject<Void> = PublishSubject()
    //
    var yScreen_Scroll : CGFloat = 0
    static let teamInfoCellIdentifier : String = "teamInfoCellIdentifier"
    let arraySettings : [[String : Any]] = [
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuTeamType,Constant.keyValue:NSLocalizedString("what_type_of_team", comment: "")],
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuTeamName,Constant.keyValue:NSLocalizedString("what_your_team_name", comment: "")],
        [Constant.keyId:Constant.ProfileMenu.kProfileMenuTeamSize,Constant.keyValue:NSLocalizedString("how_big_is_your_team", comment: "")],
    ]
    //Options
    let arrayTeamType : [String] = [NSLocalizedString("select_an_option", comment: ""),
                                    "Engineering","Product","Design","IT","Marketting",
                                    "Operations","HR","Other"]
    let arrayTeamSize : [String] = ["Small Team (1 - 5)","Medium Team (6 - 15)","Large Team (> 15)"]
    var iFilter_TeamType : Int = 0
    var iFilter_TeamSize : Int = 0
    var subjectFilterTeamType = BehaviorSubject<String>(value: "")
    var subjectFilterTeamSize = BehaviorSubject<Int>(value: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Action Button
        let wAction : CGFloat = 150
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView!.frame.size.width - wAction - pAction
        
        btnContinue = UIButton.init(type: .custom)
        btnContinue?.accessibilityIdentifier = AccessiblityId.BTN_CREATETEAM
        btnContinue?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnContinue?.setTitle(NSLocalizedString("create_team", comment:"").uppercased(), for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnContinue?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
//        btnContinue!.addTarget(self, action: #selector(createTeamAction), for: .touchUpInside)
        btnContinue?.titleLabel?.textAlignment = .right
        btnContinue?.contentHorizontalAlignment = .right
        navigationView?.addSubview(btnContinue!)
        
        //Title Screen
        let pTitleHeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleHeader : CGFloat = pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = xAction - xTitleHeader
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .left
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("about_your_team", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = AppDevice.ScreenComponent.Height - yTableMember
        tbMember.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbMember.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbMember.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbMember.dataSource = self
        tbMember.delegate = self
        tbMember.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbMember.register(MemberItemViewCell.self, forCellReuseIdentifier: InviteMemberViewController.memberCellIdentifier)
        self.view.addSubview(tbMember)
        
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableMember
        tbMemberHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: 0))
        tbMemberHeaderView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 0.0
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("tell_us_about_your_team", comment: "")
        tbMemberHeaderView?.addSubview(lbTitleScreen)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //Sub Title Screen
        let xSubTitleScreen : CGFloat = xTitleScreen
        let ySubTitleScreen : CGFloat = yViewScreen
        let wSubTitleScreen : CGFloat = wTitleScreen
        let hSubTitleScreen : CGFloat = 24
        let hBottomSubTitleScreen : CGFloat = 16
        
        let lbSubTitleScreen : UILabel = UILabel(frame: CGRect(x: xSubTitleScreen, y: ySubTitleScreen, width: wSubTitleScreen, height: hSubTitleScreen))
        lbSubTitleScreen.textAlignment = .left
        lbSubTitleScreen.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbSubTitleScreen.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbSubTitleScreen.text = NSLocalizedString("provide_us_more_details_about_tour_team", comment: "")
        tbMemberHeaderView?.addSubview(lbSubTitleScreen)
        
        yViewScreen += hSubTitleScreen
        yViewScreen += hBottomSubTitleScreen
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbMemberHeaderView?.frame.size.height = hHeaderTableView
        tbMember.tableHeaderView = tbMemberHeaderView!
    }

    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func continueAction(){
        
    }
    
    func bindToViewModel(){
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let projectController = ProjectAPIController(provider: projectProvider)
        let observableTap = btnContinue!.rx.tap.asObservable()
        let projectViewModel = ProjectAPIViewModel(
            inputCreateProject: (
                name: tfTeamName.rx.text.orEmpty.asObservable(),
                teamType: subjectFilterTeamType.asObserver(),
                teamSize: subjectFilterTeamSize.asObserver(),
                loginTaps: observableTap,
                controller: projectController
            )
        )
        
        projectViewModel.processEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.btnContinue?.isEnabled = valid
                if(valid == true){
                    self?.btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
                }
                else{
                    self?.btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
                }
            })
            .disposed(by: disposeBag)
            
            
        projectViewModel.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.startLoading(message: NSLocalizedString("updating", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        //
        projectViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                }
            
            case .ok(let output):
                if let project = output as? Project{
                    if(project.valid() == true){
                        AppSettings.sharedSingleton.saveProject(project)
                        //
                        Tracker.trackTeamCreated(project: project)
                        //
                        ThrottleHelper.throttle(seconds: 0.5, completion: {[weak self] in
                            self?.createTeamAction()
                        })
                    }
                    else{
                        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
                        appDelegate.setupOnboardingView()
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        //Tap Background Event
        let tapBackground = UITapGestureRecognizer()
        tapBackground.rx.event
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
        view.addGestureRecognizer(tapBackground)
        
        tbMember.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
            }.disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let cellHeight : CGFloat = AppDevice.TableViewRowSize.InputFieldHeight
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        //
        let profile = arraySettings[indexPath.row]
        let strName = profile[Constant.keyValue] as? String ?? ""
        let key = profile[Constant.keyId] as? Constant.ProfileMenu ?? .kProfileMenuName
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = tableView.frame.size.width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.InputFieldHeight
        
        let backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        cell.addSubview(backgroundViewCell)
        
        var yCellScreen : CGFloat = 24
        
        //Title Screen
        let xTitleName : CGFloat = 0
        let yTitleName : CGFloat = yCellScreen
        let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
        let hTitleName : CGFloat = 20
        let hBottomTitleName : CGFloat = 4.0
        
        let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName.textAlignment = .left
        lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 12)
        lbTitleName.text = strName.uppercased()
        backgroundViewCell.addSubview(lbTitleName)
        
        yCellScreen += hTitleName
        yCellScreen += hBottomTitleName
        
        //Button option
        let xViewSelection : CGFloat = 0
        let yViewSelection : CGFloat = yCellScreen
        let wViewSelection : CGFloat = wBackgroundView - (xTitleName * 2)
        let hViewSelection : CGFloat = 48
        let hViewButtonSelection : CGFloat = 0.0
        
        let viewSelection = UIView(frame: CGRect(x: xViewSelection, y: yViewSelection, width: wViewSelection, height: hViewSelection))
        viewSelection.layer.cornerRadius = 4.0
        viewSelection.layer.borderWidth = 1.0
        viewSelection.layer.borderColor = AppColor.MicsColors.LINE_VIEW_NORMAL.cgColor
        backgroundViewCell.addSubview(viewSelection)
        
        yCellScreen += hViewSelection
        yCellScreen += hViewButtonSelection
        
        if(key == .kProfileMenuTeamType){
            let pImageSelection : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let hImageSelection : CGFloat = 24
            let wImageSelection : CGFloat = hImageSelection
            let xImageSelection : CGFloat = viewSelection.frame.size.width - pImageSelection - wImageSelection
            let yImageSelection : CGFloat = (viewSelection.frame.size.height - hImageSelection) / 2
            
            let imvSelection = UIImageView(frame: CGRect(x: xImageSelection, y: yImageSelection, width: wImageSelection, height: hImageSelection))
            imvSelection.image = UIImage(named: "ic_select_option")
            viewSelection.addSubview(imvSelection)
            
            let pLabelTeamType : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let hLabelTeamType : CGFloat = 32
            let xLabelTeamType : CGFloat = pLabelTeamType
            let yLabelTeamType : CGFloat = (viewSelection.frame.size.height - hLabelTeamType) / 2
            let wLabelTeamType : CGFloat = xImageSelection - pLabelTeamType - xLabelTeamType
            
            lbTeamType = UILabel(frame: CGRect(x: xLabelTeamType, y: yLabelTeamType, width: wLabelTeamType, height: hLabelTeamType))
            lbTeamType?.accessibilityIdentifier = AccessiblityId.LB_TEAMTYPE
            lbTeamType?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            lbTeamType?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
            viewSelection.addSubview(lbTeamType!)
            //
            let strTeamType = arrayTeamType[iFilter_TeamType]
            lbTeamType?.text = strTeamType
            
            //Button option
            let xButtonSelection : CGFloat = 0
            let yButtonSelection : CGFloat = 0
            let wButtonSelection : CGFloat = viewSelection.frame.size.width
            let hButtonSelection : CGFloat = viewSelection.frame.size.height
            
            let btnSelection = RaisedButton.init(type: .custom)
            btnSelection.accessibilityIdentifier = AccessiblityId.BTN_SELECTTEAMTYPE
            btnSelection.backgroundColor = .clear
            btnSelection.pulseColor = AppColor.MicsColors.LINE_COLOR
            btnSelection.frame = CGRect(x: xButtonSelection, y: yButtonSelection, width: wButtonSelection, height: hButtonSelection)
            btnSelection.addTarget(self, action: #selector(selectionTeamTypeAction), for: .touchUpInside)
            viewSelection.addSubview(btnSelection)
        }
        else if(key == .kProfileMenuTeamName){
            let hTextFieldName : CGFloat = 32
            let xTextFieldName : CGFloat = 10
            let yTextFieldName : CGFloat = (viewSelection.frame.size.height - hTextFieldName) / 2
            let wTextFieldName : CGFloat = viewSelection.frame.size.width - (xTextFieldName * 2)
            
            tfTeamName.frame = CGRect(x: xTextFieldName, y: yTextFieldName, width: wTextFieldName, height: hTextFieldName)
            tfTeamName.accessibilityIdentifier = AccessiblityId.TF_TEAMNAME
            tfTeamName.returnKeyType = .done
            tfTeamName.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            tfTeamName.textColor = AppColor.NormalColors.BLACK_COLOR
            tfTeamName.textContainer.maximumNumberOfLines = 1
            tfTeamName.textContainer.lineBreakMode = .byTruncatingTail
            tfTeamName.delegate = self
            tfTeamName.placeholderText = NSLocalizedString("enter_your_team_name", comment: "")
            tfTeamName.placeholderColor = UIColor(hexString: "#b2bac0")
            viewSelection.addSubview(tfTeamName)
            //
            if let projectName = AppSettings.sharedSingleton.project?.name{
                tfTeamName.text = projectName
            }
            else{
                if let userName = AppSettings.sharedSingleton.account?.name{
                    tfTeamName.text = userName + "'s team"
                }
            }
        }
        else if(key == .kProfileMenuTeamSize){
            let pImageSelection : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let hImageSelection : CGFloat = 24
            let wImageSelection : CGFloat = hImageSelection
            let xImageSelection : CGFloat = viewSelection.frame.size.width - pImageSelection - wImageSelection
            let yImageSelection : CGFloat = (viewSelection.frame.size.height - hImageSelection) / 2
            
            let imvSelection = UIImageView(frame: CGRect(x: xImageSelection, y: yImageSelection, width: wImageSelection, height: hImageSelection))
            imvSelection.image = UIImage(named: "ic_select_option")
            viewSelection.addSubview(imvSelection)
            
            let pLabelTeamType : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let hLabelTeamType : CGFloat = 32
            let xLabelTeamType : CGFloat = pLabelTeamType
            let yLabelTeamType : CGFloat = (viewSelection.frame.size.height - hLabelTeamType) / 2
            let wLabelTeamType : CGFloat = xImageSelection - pLabelTeamType - xLabelTeamType
            
            lbTeamSize = UILabel(frame: CGRect(x: xLabelTeamType, y: yLabelTeamType, width: wLabelTeamType, height: hLabelTeamType))
            lbTeamSize?.accessibilityIdentifier = AccessiblityId.LB_TEAMSIZE
            lbTeamSize?.text = NSLocalizedString("select_an_option", comment: "")
            lbTeamSize?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            lbTeamSize?.textColor = AppColor.NormalColors.BLACK_COLOR
            viewSelection.addSubview(lbTeamSize!)
            //
            let strTeamSize = arrayTeamSize[iFilter_TeamSize]
            lbTeamSize?.text = strTeamSize
            
            //Button option
            let xButtonSelection : CGFloat = 0
            let yButtonSelection : CGFloat = 0
            let wButtonSelection : CGFloat = viewSelection.frame.size.width
            let hButtonSelection : CGFloat = viewSelection.frame.size.height
            
            let btnSelection = RaisedButton.init(type: .custom)
            btnSelection.accessibilityIdentifier = AccessiblityId.BTN_SELECTTEAMSIZE
            btnSelection.backgroundColor = .clear
            btnSelection.pulseColor = AppColor.MicsColors.LINE_COLOR
            btnSelection.frame = CGRect(x: xButtonSelection, y: yButtonSelection, width: wButtonSelection, height: hButtonSelection)
            btnSelection.addTarget(self, action: #selector(selectionTeamSizeAction), for: .touchUpInside)
            viewSelection.addSubview(btnSelection)
        }
        
        return cell
    }
    
    func createTeamAction(){
        let inviteMemberView = InviteMemberViewController(nibName: nil, bundle: nil)
        inviteMemberView.viewType = .add
        inviteMemberView.navigationType = .push
        self.navigationController?.pushViewController(inviteMemberView, animated: true)
    }

}

extension TeamInformationViewController : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension TeamInformationViewController : JCActionSheetDelegate{
    @objc func selectionTeamTypeAction(){
        let actionSheet = JCActionSheet(title: NSLocalizedString("what_type_of_team", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("cancel", comment: ""), cancelTextColor: AppColor.NormalColors.BLACK_COLOR, destructiveButtonTitle: nil, otherButtonTitles:  arrayTeamType, textColor: AppColor.NormalColors.BLUE_COLOR, checkedButtonIndex: iFilter_TeamType, checkedButtonTextColor: AppColor.NormalColors.BLUE_COLOR)
        actionSheet.view.tag = Constant.ProfileMenu.kProfileMenuTeamType.rawValue
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func selectionTeamSizeAction(){
        let actionSheet = JCActionSheet(title: NSLocalizedString("how_big_is_your_team", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("cancel", comment: ""), cancelTextColor: AppColor.NormalColors.BLACK_COLOR, destructiveButtonTitle: nil, otherButtonTitles:  arrayTeamSize, textColor: AppColor.NormalColors.BLUE_COLOR, checkedButtonIndex: iFilter_TeamSize, checkedButtonTextColor: AppColor.NormalColors.BLUE_COLOR)
        actionSheet.view.tag = Constant.ProfileMenu.kProfileMenuTeamSize.rawValue
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    //Delegate
    func actionSheet(_ actionSheet: JCActionSheet, clickedButtonAt buttonIndex: Int) {
        if(actionSheet.view.tag == Constant.ProfileMenu.kProfileMenuTeamType.rawValue){
            if(buttonIndex != iFilter_TeamType){
                iFilter_TeamType = buttonIndex
                //
                updateTeamType()
            }
        }
        else{
            if(buttonIndex != iFilter_TeamSize){
                iFilter_TeamSize = buttonIndex
                //
                updateTeamSize()
            }
        }
        
    }
    
    func actionSheetCancel(_ actionSheet: JCActionSheet) {
        //do something with cancel action
    }
    
    func actionSheetDetructive(_ actionSheet: JCActionSheet) {
        //do something with detructive action
    }
    
    
    func updateTeamType(){
        let strTeamType = arrayTeamType[iFilter_TeamType]
        lbTeamType?.text = strTeamType
        //
        if(iFilter_TeamType == 0){
            self.subjectFilterTeamType.onNext("")
            lbTeamType?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        }
        else{
            self.subjectFilterTeamType.onNext(strTeamType)
            lbTeamType?.textColor = AppColor.NormalColors.BLACK_COLOR
        }
    }
    
    func updateTeamSize(){
        let strTeamSize = arrayTeamSize[iFilter_TeamSize]
        lbTeamSize?.text = strTeamSize
        //
        self.subjectFilterTeamSize.onNext(iFilter_TeamSize)
    }
}
