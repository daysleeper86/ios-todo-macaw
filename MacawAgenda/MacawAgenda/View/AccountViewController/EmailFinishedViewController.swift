//
//  EmailFinishedViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/26/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Nantes
import RxSwift
import Moya

class EmailFinishedViewController: BaseViewController,NantesLabelDelegate {
    //Views
    var bgScaleView : UIView = UIView(frame: .zero)
    var lbTitleDescription : NantesLabel?
    //Data
    var email : String?
    var time_for_origin_resend : Int = 60
    var time_for_resend : Int = 0
    let time_for_repeat : Int = 1
    var timer_resend : AATimer?
    //
    let GoToLoginView : String = "gotoLogin"
    let GoToResend : String = "gotoResend"
    //
    var subjectEmail : PublishSubject<String> = PublishSubject()
    var subjectResendEventStared : PublishSubject<Void> = PublishSubject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        initTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        invalidTimer()
    }
    
    func initTimer(){
        time_for_resend = time_for_origin_resend
        timer_resend = AATimer(interval: TimeInterval(time_for_repeat), repeats: true, triggerOnInit: true, onTick: {[weak self] in
            self?.autoPlay()
        })
        timer_resend?.start()
    }
    
    func invalidTimer(){
        timer_resend?.stop()
        timer_resend = nil
    }
    
    @objc func autoPlay(){
        if(time_for_resend>0){
            time_for_resend = time_for_resend - 1
        }
        else{
            invalidTimer()
        }
        //
        let strTitleEmailSendTo = NSLocalizedString("an_email_has_been_sent_to_your_email_address", comment: "")
        let strTitleCheckInbox =  NSLocalizedString("please_check_your_inbox_and_follow", comment: "")
        let strEmail = self.email ?? ""
        let strBreak = "\n\n\n"
        let strSpace = " "
        let strMessageResend = NSLocalizedString("didnt_receive_the_password_reset_email", comment: "")
        //
        var strResend = NSLocalizedString("resend", comment: "")
        if(time_for_resend > 0){
            strResend = strResend + " (" + String(time_for_resend) + ")"
        }
        let strEmailSendToCheckInbox = strTitleEmailSendTo + strSpace + strEmail + strSpace + strTitleCheckInbox + strBreak + strMessageResend + strSpace + strResend
        lbTitleDescription?.text = strEmailSendToCheckInbox
        //
        let attributedStringDescription = NSMutableAttributedString(string: lbTitleDescription!.text!)
        //Email attribute
        attributedStringDescription.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold), range: NSRange(location: strTitleEmailSendTo.count+strSpace.count, length: strEmail.count))
        attributedStringDescription.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: strTitleEmailSendTo.count+strSpace.count, length: strEmail.count))
        //Resend
        lbTitleDescription?.attributedText = attributedStringDescription
        if let url = URL(string: GoToResend){
            lbTitleDescription?.addLink(to: url, withRange: NSRange(location: strEmailSendToCheckInbox.count-strResend.count, length: strResend.count))
        }
        
    }
    
    func initView(){
        //Background View
        let yScreenView : CGFloat = 0
        let wScreenView : CGFloat = AppDevice.ScreenComponent.Width
        let hScreenView : CGFloat = AppDevice.ScreenComponent.Height - yScreenView
        
        let bgView = UIView(frame: CGRect(x: 0, y: yScreenView, width: wScreenView, height: hScreenView))
        bgView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(bgView)
        //
        bgScaleView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        bgView.addSubview(bgScaleView)
        
        let xScaleView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wScaleView : CGFloat = bgView.frame.size.width - (xScaleView * 2)
        var yScreen : CGFloat = 0
        
        //Avatar
        let yAvatar : CGFloat = yScreen
        let wAvatar : CGFloat = 48
        let hAvatar : CGFloat = 48
        let xAvatar : CGFloat = (wScaleView-wAvatar)/2
        let pBottomAvatar : CGFloat = 24
        
        let imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar.image = UIImage(named: "bg_done_send_email")
        bgScaleView.addSubview(imvAvatar)
        
        yScreen += hAvatar
        yScreen += pBottomAvatar
        
        //Title
        let xTitleName : CGFloat = 0
        let yTitleName : CGFloat = yScreen
        let wTitleName : CGFloat = wScaleView - (xTitleName * 2)
        let hTitleName : CGFloat = 20
        let pBottomTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName.textAlignment = .center
        lbTitleName.numberOfLines = 0
        lbTitleName.textColor = AppColor.NormalColors.BLUE_COLOR
        lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleName.text = NSLocalizedString("password_reset_email_sent", comment: "")
        bgScaleView.addSubview(lbTitleName)
        
        yScreen += hTitleName
        yScreen += pBottomTitleName
        
        // Title name
        let xTitleDescription : CGFloat = xTitleName
        let yTitleDescription : CGFloat = yScreen
        let wTitleDescription : CGFloat = wTitleName
        let hTitleDescription : CGFloat = 126
        let pBottomTitleDescription : CGFloat = 24
        
        lbTitleDescription = NantesLabel(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
        lbTitleDescription?.accessibilityIdentifier = AccessiblityId.LB_CONTROL_RESEND
        lbTitleDescription?.linkAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.NormalColors.BLUE_COLOR,
            NSAttributedString.Key.font: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        ]
        lbTitleDescription?.enabledTextCheckingTypes = [.date]
        lbTitleDescription?.delegate = self // NantesLabelDelegate
        lbTitleDescription?.textAlignment = .center
        lbTitleDescription?.numberOfLines = 0
        lbTitleDescription?.textColor = AppColor.MicsColors.CHATTING_TITLE_NAME_COLOR
        lbTitleDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        bgScaleView.addSubview(lbTitleDescription!)
        
        //
        let strTitleEmailSendTo = NSLocalizedString("an_email_has_been_sent_to_your_email_address", comment: "")
        let strTitleCheckInbox =  NSLocalizedString("please_check_your_inbox_and_follow", comment: "")
        let strEmail = self.email ?? ""
        let strBreak = "\n\n\n"
        let strSpace = " "
        let strMessageResend = NSLocalizedString("didnt_receive_the_password_reset_email", comment: "")
        //
        let strResend = NSLocalizedString("resend", comment: "") + " (" + String(time_for_origin_resend) + ")"
        let strEmailSendToCheckInbox = strTitleEmailSendTo + strSpace + strEmail + strSpace + strTitleCheckInbox + strBreak + strMessageResend + strSpace + strResend
        lbTitleDescription?.text = strEmailSendToCheckInbox
        //
        let attributedStringDescription = NSMutableAttributedString(string: lbTitleDescription!.text!)
        //Email attribute
        attributedStringDescription.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold), range: NSRange(location: strTitleEmailSendTo.count+strSpace.count, length: strEmail.count))
        attributedStringDescription.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: strTitleEmailSendTo.count+strSpace.count, length: strEmail.count))
        //Resend
        lbTitleDescription?.attributedText = attributedStringDescription
        if let url = URL(string: GoToResend){
            lbTitleDescription?.addLink(to: url, withRange: NSRange(location: strEmailSendToCheckInbox.count-strResend.count, length: strResend.count))
        }
        
        yScreen += hTitleDescription
        yScreen += pBottomTitleDescription
        
        let hScaleView : CGFloat = lbTitleDescription!.frame.origin.y + lbTitleDescription!.frame.size.height
        let yScaleView : CGFloat = (bgView.frame.size.height-hScaleView)/2
        bgScaleView.frame = CGRect(x: xScaleView, y: yScaleView, width: wScaleView, height: hScaleView)
        
        
        let wTitleGotoLogin : CGFloat = 180
        let hTitleGotoLogin : CGFloat = 26
        let xTitleGotoLogin : CGFloat = (wScreenView - wTitleGotoLogin ) / 2
        let pBottomTitleGotoLogin : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
        let yTitleGotoLogin : CGFloat = hScreenView - pBottomTitleGotoLogin - hTitleGotoLogin
        
        let lbitleGotoLogin = NantesLabel(frame: CGRect(x: xTitleGotoLogin, y: yTitleGotoLogin, width: wTitleGotoLogin, height: hTitleGotoLogin))
        lbitleGotoLogin.accessibilityIdentifier = AccessiblityId.LB_CONTROL_NAVIGATE
        lbitleGotoLogin.linkAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.NormalColors.BLUE_COLOR,
            NSAttributedString.Key.font: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        ]
        lbitleGotoLogin.delegate = self // NantesLabelDelegate
        lbitleGotoLogin.textAlignment = .center
        lbitleGotoLogin.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        bgView.addSubview(lbitleGotoLogin)

        let strTitleBackTo = NSLocalizedString("back_to", comment: "")
        let strTitleLogin =  NSLocalizedString("log_in", comment: "").uppercased()
        let strBackToLoginIn = strTitleBackTo + " " + strTitleLogin
        lbitleGotoLogin.text = strBackToLoginIn
        //
        let attributedStringLogin = NSMutableAttributedString(string: lbitleGotoLogin.text!)
        attributedStringLogin.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: strTitleBackTo.count))
        attributedStringLogin.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: strTitleBackTo.count))
        lbitleGotoLogin.attributedText = attributedStringLogin
        //
        if let url = URL(string: GoToLoginView){
            lbitleGotoLogin.addLink(to: url, withRange: NSRange(location: strTitleBackTo.count+1, length: strTitleLogin.count))
        }
    }
    
    // Link handling
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        if(link.absoluteString == GoToLoginView){
            gotoLoginView()
        }
        else if(link.absoluteString == GoToResend){
            if(time_for_resend == 0){
                subjectEmail.onNext(self.email!)
                subjectResendEventStared.onNext(())
            }
        }
    }
    
    func gotoLoginView(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func gotoInputEmailView(){
        if let viewControllers = self.navigationController?.viewControllers{
            for viewController in viewControllers{
                if ((viewController as? LoginEmailViewController) != nil){
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func bindToViewModel(){
        //
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
        let userController = UserAPIController(provider: projectProvider)
        let accountViewModel = UserAPIViewModel(
            inputForgetPass: (
                email: subjectEmail.asObservable(),
                loginTaps: subjectResendEventStared.asObservable(),
                userAPIController: userController
            )
        )
        
        // bind results to  {
        accountViewModel.loginEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.lbTitleDescription?.isEnabled = valid
//                if(valid == true){
//                    self?.lbTitleDescription?.startShimmering()
//                }
//                else{
//                    self?.lbTitleDescription?.stopShimmering()
//                }
            })
            .disposed(by: disposeBag)
        //
        accountViewModel.signingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    self?.lbTitleDescription?.startShimmering()
                    strongSelf.startLoading(message: NSLocalizedString("processing", comment: ""))
                }
                else{
                    self?.lbTitleDescription?.stopShimmering()
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        //
        //
        accountViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                self?.makeToast(message:output.message, duration: 3.0, position: .top)
                //
                self?.initTimer()
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            let message = NSLocalizedString("resend_reset_password_successfully", comment: "")
                            self?.makeToast(message:message, duration: 2.0, position: .top)
                        }
                        else{
                            let message = NSLocalizedString("send_request_for_reset_password_unsuccessfully", comment: "")
                            self?.makeToast(message:message, duration: 2.0, position: .top)
                        }
                        //
                        self?.initTimer()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
}
