//
//  OnBoardingAppViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import SCPageControl
import GoogleSignIn
import RxSwift
import Moya
import Material
import DTGradientButton

class OnBoardingAppViewController: BaseViewController,UIScrollViewDelegate,GIDSignInDelegate,GIDSignInUIDelegate {
    struct OnBoardingImage{
        static let OnBoarding1 = "onboarding_firstchat.png"
        static let OnBoarding2 = "onboarding_business.png"
        static let OnBoarding3 = "onboarding_welcome.png"
    }
    struct OnBoardingTitle{
        static let OnBoarding1 = NSLocalizedString("on_boarding_title_welcome", comment:"")
        static let OnBoarding2 = NSLocalizedString("on_boarding_title_business", comment:"")
        static let OnBoarding3 = NSLocalizedString("on_boarding_title_firstchat", comment:"")
    }
    struct OnBoardingDescription{
        static let OnBoarding1 = NSLocalizedString("on_boarding_content_welcome", comment:"")
        static let OnBoarding2 = NSLocalizedString("on_boarding_content_business", comment:"")
        static let OnBoarding3 = NSLocalizedString("on_boarding_content_firstchat", comment:"")
    }
    
    //View
    var srvContent : UIScrollView!
    var pageControl : SCPageControlView!
    
    //Data
    let time_for_autoplay : Int = 3
    var timer_autoplay : AATimer?
    var arrayAvatar : [String]!
    var arrayTitle : [String]!
    var arrayDescription : [String]!
    var currentPage : Int = 0
    
    var subjectGoogleSignInInfo : PublishSubject<[String:Any]> = PublishSubject()
    var subjectEventStared : PublishSubject<Void> = PublishSubject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Logout if need (For case user remove app, keychain not removed)
        Util.signOutCurrentAccount()
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance()?.delegate = self
        //
        initData()
        //
        initView()
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //
        initTimer()
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        if #available(iOS 13.0, *) {
//            return .darkContent
//        } else {
//            // Fallback on earlier versions
//            return .default
//        }
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        invalidTimer()
    }
    
    func initData(){
        arrayAvatar = [OnBoardingImage.OnBoarding1,OnBoardingImage.OnBoarding2,OnBoardingImage.OnBoarding3]
        arrayTitle = [OnBoardingTitle.OnBoarding1,OnBoardingTitle.OnBoarding2,OnBoardingTitle.OnBoarding3]
        arrayDescription = [OnBoardingDescription.OnBoarding1,OnBoardingDescription.OnBoarding2,OnBoardingDescription.OnBoarding3]
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //
        let numItem = arrayAvatar.count
        
        let yScrollView = AppDevice.ScreenComponent.StatusHeight
        let wScrollView = AppDevice.ScreenComponent.Width
        let hScrollView = AppDevice.ScreenComponent.Height * 3 / 4
        
        //ScollView
        srvContent = UIScrollView(frame: CGRect(x: 0, y: yScrollView, width: wScrollView, height: hScrollView))
        srvContent.accessibilityIdentifier = AccessiblityId.SCROLLVIEW_CONTENT
        srvContent.delegate = self
        srvContent.isOpaque = false
        srvContent.showsVerticalScrollIndicator = false
        srvContent.showsHorizontalScrollIndicator = false
        srvContent.isPagingEnabled = true
        srvContent.contentSize = CGSize(width: srvContent.frame.size.width * CGFloat(numItem), height: 0)
        self.view.addSubview(srvContent)
        
        //Item
        var yPageScroll : CGFloat = 0
        let wItem = srvContent.frame.size.width
        let yAvatar = AppDevice.ScreenComponent.Height/5/2
        let xAvatar : CGFloat = 60.0
        let wAvatar = srvContent.frame.size.width-(xAvatar*2)
        
        let hTitle : CGFloat = 35
        let pTitle : CGFloat = 35.0
        let wTitle = AppDevice.ScreenComponent.Width-(pTitle*2)
        
        let hDescription : CGFloat = 40.0
        let pDescription : CGFloat = 8.0
        
        for i in 0..<numItem{
            let imvAvatar : UIImageView = UIImageView(frame: CGRect(x: CGFloat(i)*wItem+(xAvatar), y: yAvatar, width: wAvatar, height: wAvatar))
            srvContent.addSubview(imvAvatar)
            
            //Title
            let lbTitle : UILabel = UILabel(frame: CGRect(x: CGFloat(i)*wItem+(AppDevice.ScreenComponent.Width-wTitle)/2, y: imvAvatar.frame.origin.y + imvAvatar.frame.size.height, width: wTitle, height: hTitle))
            lbTitle.textAlignment = .left
            lbTitle.textColor = UIColor(red: CGFloat(38.0)/255, green: CGFloat(53.0)/255, blue: CGFloat(70.0)/255, alpha: 1.0)
            lbTitle.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
            srvContent.addSubview(lbTitle)
            
            //Description
            let lbDescription : UILabel = UILabel(frame: CGRect(x: lbTitle.frame.origin.x, y: lbTitle.frame.origin.y + lbTitle.frame.size.height + pDescription, width: lbTitle.frame.size.width, height: hDescription))
            lbDescription.textAlignment = .left
            lbDescription.textColor = AppColor.MicsColors.CHATTING_TITLE_NAME_COLOR
            lbDescription.numberOfLines = 0
            lbDescription.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            srvContent.addSubview(lbDescription)
            
            //Set values
            let strAvatar = arrayAvatar[i]
            imvAvatar.image = UIImage(named: strAvatar)
            //
            let strTitle = arrayTitle[i]
            lbTitle.text = strTitle
            //
            let strDescription = arrayDescription[i]
            lbDescription.text = strDescription
            
            //
            if(yPageScroll == 0){
                yPageScroll = lbDescription.frame.origin.y + lbDescription.frame.size.height + 20
            }
        }
        
        srvContent.frame.size.height = yPageScroll
    
        //PageControl
        let wPageControl : CGFloat = 80
        let hPageControl : CGFloat = 20
        let pPageControl : CGFloat = 15
        
        pageControl = SCPageControlView(frame: CGRect(x: (srvContent.frame.size.width-wPageControl)/2, y: srvContent.frame.origin.y + srvContent.frame.size.height + pPageControl, width: wPageControl, height: hPageControl))
        pageControl.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        pageControl.tintColor = AppColor.NormalColors.BLUE_COLOR
        pageControl.layer.cornerRadius = pageControl.frame.size.height/2
        pageControl.layer.masksToBounds = true
        pageControl.set_view(numItem, current: 0, current_color: AppColor.NormalColors.BLUE_COLOR, disable_color: AppColor.NormalColors.LIGHT_GRAY_COLOR)
        self.view.addSubview(pageControl)
        
        //
        let wButton :CGFloat = 300
        let hButton :CGFloat = AppDevice.ScreenComponent.ButtonHeight
        let pButton :CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xButton :CGFloat = (AppDevice.ScreenComponent.Width - wButton ) / 2
        let yButtonLoginEmail :CGFloat = pageControl.frame.origin.y + pageControl.frame.size.height + pButton
        
        let btnLoginEmail = RaisedButton.init(type: .custom)
        btnLoginEmail.accessibilityIdentifier = AccessiblityId.BTN_LOGIN_EMAIL
        btnLoginEmail.pulseColor = .white
        btnLoginEmail.frame = CGRect(x: xButton, y: yButtonLoginEmail, width: wButton, height: hButton)
        btnLoginEmail.addTarget(self, action: #selector(loginEmailAction), for: .touchUpInside)
        btnLoginEmail.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
        btnLoginEmail.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        btnLoginEmail.titleLabel?.textAlignment = .center
        btnLoginEmail.layer.cornerRadius = 4.0
        btnLoginEmail.layer.masksToBounds = true
        let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
        btnLoginEmail.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
        btnLoginEmail.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
        btnLoginEmail.setImage(UIImage(named: "ic_invite_email_white"), for: .normal)
        btnLoginEmail.setImage(UIImage(named: "ic_invite_email_white"), for: .highlighted)
        btnLoginEmail.setTitle(NSLocalizedString("continue_with_email", comment:"").uppercased(), for: .normal)
        btnLoginEmail.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -80, bottom: 0.0, right:0.0)
        btnLoginEmail.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -40, bottom: 0.0, right:0.0)
        btnLoginEmail.layer.shadowColor = UIColor(red: 0.15, green: 0.2, blue: 0.22, alpha: 0.12).cgColor
        btnLoginEmail.layer.shadowOpacity = 1
        btnLoginEmail.layer.shadowRadius = 4
        btnLoginEmail.layer.shadowOffset = CGSize(width: 0, height: 2)
        btnLoginEmail.layer.compositingFilter = "multiplyBlendMode"
        self.view.addSubview(btnLoginEmail)
        
        let btnLoginGoogle = RaisedButton.init(type: .custom)
        btnLoginGoogle.accessibilityIdentifier = AccessiblityId.BTN_LOGIN_GOOGLE
        btnLoginGoogle.pulseColor = AppColor.NormalColors.LIGHT_BLUE_COLOR
        btnLoginGoogle.frame = CGRect(x: btnLoginEmail.frame.origin.x, y: btnLoginEmail.frame.origin.y + btnLoginEmail.frame.size.height + pButton, width: btnLoginEmail.frame.size.width, height: btnLoginEmail.frame.size.height)
        btnLoginGoogle.addTarget(self, action: #selector(signinGoogleAction), for: .touchUpInside)
        btnLoginGoogle.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
        btnLoginGoogle.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        btnLoginGoogle.titleLabel?.textAlignment = .center
        btnLoginGoogle.layer.cornerRadius = 4.0
        btnLoginGoogle.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        btnLoginGoogle.setImage(UIImage(named: "ic_google"), for: .normal)
        btnLoginGoogle.setImage(UIImage(named: "ic_google"), for: .normal)
        btnLoginGoogle.setTitle(NSLocalizedString("continue_with_google", comment:"").uppercased(), for: .normal)
        btnLoginGoogle.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -62, bottom: 0.0, right:0.0)
        btnLoginGoogle.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -25, bottom: 0.0, right:0.0)
        btnLoginGoogle.layer.shadowColor = UIColor(red: 0.15, green: 0.2, blue: 0.22, alpha: 0.12).cgColor
        btnLoginGoogle.layer.shadowOpacity = 1
        btnLoginGoogle.layer.shadowRadius = 4
        btnLoginGoogle.layer.shadowOffset = CGSize(width: 0, height: 2)
        btnLoginGoogle.layer.compositingFilter = "multiplyBlendMode"
        self.view.addSubview(btnLoginGoogle)
        
    }
    
    func bindToViewModel(){
        let provider = MoyaProvider<AppAPI>(manager: DefaultAlamofireManager.sharedManager,
                                            plugins: [NetworkLoggerPlugin(verbose: true)])
        let accountController = AccountAPIController(provider: provider)
        
        let viewModel = AccountViewModel(
            inputGoogleSignIn: (
                loginInfos: self.subjectGoogleSignInInfo.asObserver(),
                loginTaps: self.subjectEventStared.asObservable(),
                controller: accountController
            )
        )
        
        viewModel.signingIn
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.startLoading(message: NSLocalizedString("authenticating", comment: ""))
                }
                else{
                    strongSelf.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                self?.makeToast(message:output.message, duration: 3.0, position: .top)
            case .ok(let output):
                if let strongAccount = output as? Account{
                    guard let strongSelf = self else { return }
                    strongSelf.invalidTimer()
                    //
                    AppSettings.sharedSingleton.saveAccount(strongAccount)
                    //
                    if(strongAccount.defaultProject.count > 0){
                        //
                        Tracker.trackUserLogin(account: strongAccount,fromEmail: false)
                        //
                        let defaultProject = Project(projectId: strongAccount.defaultProject, name: "", dataType: .normal) //Design team
                        AppSettings.sharedSingleton.saveProject(defaultProject)
                        //
                        strongSelf.launchWelcomeAction()
                    }
                    else{
                        //
                        Tracker.trackUserSignUp(account: strongAccount,fromEmail: false)
                        //
                        strongSelf.launchTeamInfoAction()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func launchWelcomeAction(){
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        appDelegate.setupWelcomeView()
    }
    
    func launchTeamInfoAction(){
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        appDelegate.setupTeamInfoView()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let pageWidth : CGFloat = scrollView.frame.size.width
        let fractionalPage : CGFloat = scrollView.contentOffset.x / pageWidth
        let page : Int = lroundf(Float(fractionalPage))
        //
        currentPage = page
        //
        pageControl.scroll_did(scrollView)
    }
    
    func initTimer(){
        timer_autoplay = AATimer(interval: TimeInterval(time_for_autoplay), repeats: true, triggerOnInit: true, onTick: {[weak self] in
            self?.autoPlay()
        })
        timer_autoplay?.start()
    }
    
    @objc func autoPlay(){
        let numItem = arrayAvatar.count
        if(currentPage + 1 == numItem){
            currentPage = 0
        }
        else{
            currentPage = currentPage + 1
        }
        srvContent.setContentOffset(CGPoint(x: CGFloat(currentPage) * srvContent.frame.size.width,y: 0), animated: true)
    }
    
    func invalidTimer(){
        timer_autoplay?.stop()
        timer_autoplay = nil
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen //or .overFullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser?, withError error: Error?) {
        // Perform any operations on signed in user here.
        // For client-side use only!

        if let userId = user?.userID{
            var dictInfo : [String:Any] = [:]
            dictInfo[UserInfoKeyString.MY_USERID_KEY] = userId
            //
            let givenName = user?.profile.givenName ?? ""
            dictInfo[UserInfoKeyString.MY_NAME_KEY] = givenName
            //
            let email = user?.profile.email ?? ""
            dictInfo[UserInfoKeyString.MY_EMAIL_KEY] = email
            //
            var urlImage : String = ""
            if(user?.profile.hasImage == true){
                let size : UInt = 150
                let url = user?.profile.imageURL(withDimension: size)
                urlImage = url?.absoluteString ?? ""
            }
            dictInfo[UserInfoKeyString.MY_URL_AVATAR_KEY] = urlImage
            //Token
            let accessToken = user?.authentication.accessToken ?? ""
            dictInfo[UserInfoKeyString.MY_TOKEN_KEY_API] = accessToken
            //
            self.subjectGoogleSignInInfo.onNext(dictInfo)
            self.subjectEventStared.onNext(())
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser?, withError error: Error?) {
        // Perform any operations when the user disconnects from app here.
    }
    
    @objc func loginEmailAction(){
        let inputEmailViewController : InputEmailViewController = InputEmailViewController(nibName: nil, bundle: nil)
        self.navigationController?.pushViewController(inputEmailViewController, animated: true)
    }
    
    @objc func signinGoogleAction(){
//        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @objc func signoutGoogleAction(){
        GIDSignIn.sharedInstance()?.signOut()
    }
    
}
