//
//  WelcomeViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Firebase
import FirebaseAuth
import Material
import DTGradientButton
import NVActivityIndicatorView

class WelcomeLoadingViewController: BaseViewController {
    //Views
    var bgScaleView : UIView?
    var lbUserName : UILabel?
    var viewContent : UIView?
    var viewAvatar : UIImageView?
//    var viewAnimation : VDSCircleAnimation?
    var viewAnimation : NVActivityIndicatorView?
    var timer_autoplay : Timer?
    let time_for_autoplay : Int = 5
    //Data
    var agendaViewModel : AgendaViewModel?
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    var subjectDate : PublishSubject<Date> = PublishSubject()
    var subjectProject : PublishSubject<String> = PublishSubject()
    var subjectCreateAgendaEventStared = BehaviorSubject<Void>(value:())
    //
    var subjectReSignInInfo : PublishSubject<Account> = PublishSubject()
    var subjectEventStared : PublishSubject<Void> = PublishSubject()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initLoading()
        //
        let userID = Auth.auth().currentUser?.uid
        if(userID != nil){
            bindToViewModel()
        }
        else{
            bindToReLoginViewModel()
            //
            if let account = AppSettings.sharedSingleton.account{
                self.subjectReSignInInfo.onNext(account)
                self.subjectEventStared.onNext(())
            }
        }
    }
    
    //
    func startPlanning(_ agenda: Agenda){
        viewAnimation?.stopAnimating()
        //
        let heroId = "cell\(2)"
        viewAnimation?.hero.id = heroId
        //
        let welcomePlanning = WelcomePlanningViewController(nibName: nil, bundle: nil)
        welcomePlanning.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        welcomePlanning.hero.isEnabled = true
        welcomePlanning.imvAvatar.hero.id = heroId
        welcomePlanning.lbUserName.hero.id = heroId
        welcomePlanning.btnPlanning.hero.id = heroId
        welcomePlanning.agenda = agenda
        //
        self.present(welcomePlanning, animated: true, completion: nil)
    }
    
    func startTeamAgenda(){
        AppDelegate().sharedInstance().setupTeamAgendaView()
    }
    
    func restartLoadingViewWhenError(){
        AppDelegate().sharedInstance().setupWelcomeView()
    }
    
    func initLoading(){
        //Background View
        let yScreenView : CGFloat = 0
        let wScreenView : CGFloat = AppDevice.ScreenComponent.Width
        let hScreenView : CGFloat = AppDevice.ScreenComponent.Height - yScreenView
        let bgView = UIView(frame: CGRect(x: 0, y: yScreenView, width: wScreenView, height: hScreenView))
        bgView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(bgView)
        
        // Animation view
        let xViewAvatar : CGFloat = 0
        let yViewAvatar : CGFloat = 0
        let wViewAvatar : CGFloat = 64
        let hViewAvatar : CGFloat = wViewAvatar
        
        let xViewAvatar_Off : CGFloat = wViewAvatar / 2
        let yViewAvatar_Off : CGFloat = hViewAvatar
        let wViewAvatar_Off : CGFloat = 0
        let hViewAvatar_Off : CGFloat = 0
        
        let wViewAnimation : CGFloat = 40
        let hViewAnimation : CGFloat = wViewAnimation
        let pViewAnimation : CGFloat = 36
        
        let wViewContent : CGFloat = wViewAvatar
        let hViewContent : CGFloat = hViewAvatar + pViewAnimation + hViewAnimation
        let xViewContent : CGFloat = (wScreenView - wViewContent ) / 2
        let yViewContent : CGFloat = (hScreenView - hViewContent ) / 2
        
        viewContent = UIView(frame: CGRect(x: xViewContent, y: yViewContent, width: wViewContent, height: hViewContent))
        self.view.addSubview(viewContent!)
        
        viewAvatar = UIImageView(frame: CGRect(x: xViewAvatar_Off, y: yViewAvatar_Off, width: wViewAvatar_Off, height: hViewAvatar_Off))
        viewAvatar?.image = UIImage(named: "AppIcon")
        viewAvatar?.layer.cornerRadius = 10
        viewAvatar?.layer.masksToBounds = true
        viewContent?.addSubview(viewAvatar!)
        
        let xViewAnimation : CGFloat = (wViewContent - wViewAnimation ) / 2
        let yViewAnimation : CGFloat = yViewAvatar + hViewAvatar + hViewAnimation
        
        viewAnimation = NVActivityIndicatorView(frame: CGRect(x: xViewAnimation, y: yViewAnimation, width: wViewAnimation, height: hViewAnimation),
                                      type: .ballClipRotate)
//        viewAnimation.width
        viewAnimation?.color = AppColor.NormalColors.GRAY_COLOR
//        viewAnimation = VDSCircleAnimation(frame: CGRect(x: xViewAnimation, y: yViewAnimation, width: wViewAnimation, height: hViewAnimation))
        viewAnimation?.startAnimating()
        viewContent?.addSubview(viewAnimation!)
        
        UIView.animate(withDuration: 0.7, animations: {[weak self] in
            self?.viewAvatar?.frame = CGRect(x: xViewAvatar, y: yViewAvatar, width: wViewAvatar, height: hViewAvatar)
        })
        
//        viewContent?.transform = CGAffineTransform(scaleX: 0.2 , y: 0.2)
//        let originalTransform = viewContent!.transform
//        let scaledTransform = originalTransform.scaledBy(x: 1.0, y: 1.0)
//        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
//        UIView.animate(withDuration: 0.7, animations: {[weak self] in
//            self?.viewContent?.transform = scaledAndTranslatedTransform
//        })
        //
//        viewContent?.alpha = 0
//        let animationDuration : CGFloat = 0.1
//        viewContent?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        UIView.animate(withDuration: TimeInterval(animationDuration), animations: { [weak self] in
//            self?.viewContent?.alpha = 1
//            self?.viewContent?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//        })
        
    }
    
    func startAnimationIcon(){
        
    }
    
    func retryConnection(){
        subjectCreateAgendaEventStared.onNext(())
        //
        DispatchQueue.main.async {[weak self] in
            if let strongViewAnimation = self?.viewAnimation{
                strongViewAnimation.startAnimating()
                self?.viewContent?.addSubview(strongViewAnimation)
            }
        }
    }
    
    func stopAnimationIcon(){
        AppDelegate().sharedInstance().alertNoInternetConntect(reachable: false)
        //
        viewAnimation?.stopAnimating()
        viewAnimation?.removeFromSuperview()
    }

    func clearNotificationDataAndRestartApp (){
        let appDelegate = AppDelegate().sharedInstance()
        appDelegate.remoteNotificationData = nil
        //
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        if(projectId.count > 0){
            self.subjectProject.onNext(projectId)
            self.subjectCreateAgendaEventStared.onNext(())
        }
        else{
            appDelegate.setupMainView()
        }
    }
    
    
    func bindToReLoginViewModel(){
        let viewModel = AccountViewModel(
            inputReLoginFireStore: (
                loginInfos: self.subjectReSignInInfo.asObserver(),
                loginTaps: self.subjectEventStared.asObservable()
            )
        )
        viewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed( _):
                let appDelegate : AppDelegate = AppDelegate().sharedInstance()
                appDelegate.setupOnboardingView()
            case .ok(let output):
                if (output as? Account) != nil{
                    guard let strongSelf = self else { return }
                    if let myProject = AppSettings.sharedSingleton.project{
                        //
                        if(myProject.projectId.count > 0){
                            //
                            strongSelf.bindToViewModel()
                        }
                        else{
                            let appDelegate : AppDelegate = AppDelegate().sharedInstance()
                            appDelegate.setupOnboardingView()
                        }
                    }
                    else{
                        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
                        appDelegate.setupOnboardingView()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func bindToViewModel(){
        let userController = UserController.sharedAPI
        let userViewModel = UserViewModel(
            inputMyAccount: (
                userId: subjectUser.asObservable(),
                subscrible: false,
                userController: userController
            )
        )
        
        userViewModel.account?.subscribe({[weak self] accountInfo  in
            if let account = accountInfo.element{
                if(account.valid() == true){
                    if let currentProject = AppSettings.sharedSingleton.project{
                        if(account.defaultProject != currentProject.projectId){
                            AppSettings.sharedSingleton.saveIdProject(account.defaultProject)
                        }
                    }
                    //
                    var changeProjectId = ""
                    var changeProject = false
                    if let remoteNotificationData = AppDelegate().sharedInstance().remoteNotificationData{
                        if let projectId = remoteNotificationData[SettingString.ProjectId] as? String{
                            if(account.defaultProject != projectId){
                                changeProject = true
                                changeProjectId = projectId
                            }
                        }
                    }
                    //
                    if(account.hasSetting == false){
                        let emailStatus = true
                        let notificationStatus = true
                        let deviceToken = AppSettings.sharedSingleton.account?.deviceToken ?? ""
                        GlobalAPIManager.sharedSingleton.updateSettings(emailStatus: emailStatus, notificationStatus: notificationStatus, deviceToken: deviceToken)
                    }
                    //
                    if(changeProject == false){
                        if(account.defaultProject.count == 0){
                            AppSettings.sharedSingleton.saveProject(nil)
                            AppDelegate().sharedInstance().setupTeamInfoView()
                        }
                        else{
                            self?.subjectProject.onNext(account.defaultProject)
                            self?.subjectCreateAgendaEventStared.onNext(())
                        }
                    }
                    else{
                        GlobalAPIManager.sharedSingleton.updateDefaultProjectChanged(changeProjectId, completion: {[weak self] isSave in
                            if(isSave == true){
                                AppSettings.sharedSingleton.saveIdProject(changeProjectId)
                                self?.subjectProject.onNext(changeProjectId)
                                self?.subjectCreateAgendaEventStared.onNext(())
                            }
                            else{
                                AppDelegate().sharedInstance().makeSystemToast(message: NSLocalizedString("you_can_not_access_this_project_open_app", comment: ""))
                                self?.clearNotificationDataAndRestartApp()
                            }
                        })
                    }
                }
                else{
                    AppDelegate().sharedInstance().setupOnboardingView()
                }
            }
        }).disposed(by: disposeBag)
        
        let agendaController = AgendaController.sharedAPI
        self.agendaViewModel = AgendaViewModel(
            inputUserId: (
                agendaController: agendaController,
                userId: subjectUser.asObservable(),
                projectId:subjectProject.asObservable(),
                duedate: subjectDate.asObservable(),
                needSubscrible: false
            )
        )
        
        let notificationController = NotificationController.sharedAPI
        let notificationViewModel = NotificationViewModel(
            inputGetCountNotification: (
                projectId:subjectProject.asObservable(),
                userId: subjectUser.asObservable(),
                notificationController: notificationController
            )
        )
        
        let agendaNotificationAndUser = Observable
            .combineLatest(self.agendaViewModel!.agendas!, notificationViewModel.counter!)  { (agendas: $0, counter: $1) }
        
        agendaNotificationAndUser.subscribe(onNext: {[weak self] agendaNotis  in
            if(agendaNotis.agendas.count > 0 && agendaNotis.counter.dataType != .holder){
                let firstAgenda = agendaNotis.agendas[0]
                if(firstAgenda.dataType != .holder){
                    //
                    if(agendaNotis.counter.dataType == .normal){
                        let count = agendaNotis.counter.count
                        AppSettings.sharedSingleton.saveMiscAccount(count,name: nil,password: nil,deviceToken: nil)
                    }
                    //
                    if(firstAgenda.tasks.count > 0){
                        if(firstAgenda.status == 0){
                            self?.startPlanning(firstAgenda)
                        }
                        else{
                            self?.startTeamAgenda()
                        }
                    }
                    else{
                        if(firstAgenda.dataType == .empty){
                            AppDelegate().sharedInstance().alertSessionExpiredIfNeed(code: AppURL.APIStatusCode.UNAUTHORIZED.rawValue)
                        }
                        else{
                            var clearNotification = true
                            if let remoteNotificationData = AppDelegate().sharedInstance().remoteNotificationData{
                                if let agendaId = remoteNotificationData[SettingString.RoomId] as? String{
                                    if(agendaId == firstAgenda.agendaId){
                                        clearNotification = false
                                    }
                                    else{
                                        if(agendaNotis.agendas.count > 1){
                                            let secondAgenda = agendaNotis.agendas[1]
                                            if(secondAgenda.dataType == .normal){
                                                if(agendaId == secondAgenda.agendaId){
                                                    clearNotification = false
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //
                            if(clearNotification == true){
                                AppDelegate().sharedInstance().remoteNotificationData = nil
                            }
                            //
                            self?.startPlanning(firstAgenda)
                        }
                    }
                }
            }
            })
            .disposed(by: disposeBag)
        
        
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let agendaProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let agendaAPIController = AgendaAPIController(provider: agendaProvider)
        let agendaViewModel = AgendaAPIViewModel(
            inputCreateAgenda: (
                projectId: subjectProject.asObservable(),
                loginTaps: subjectCreateAgendaEventStared.asObserver(),
                agendaAPIController: agendaAPIController
            )
        )
        //
        agendaViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.stopAnimationIcon()
//                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPIAgendas{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            if(reponseAPI.agenda_today.dataType == .normal){
                                DispatchQueue.main.async {
                                    let agenda_today = reponseAPI.agenda_today
                                    AppSettings.sharedSingleton.saveTodayAgenda(agenda_today)
                                    AppSettings.sharedSingleton.saveYesterdayAgenda(reponseAPI.agenda_yesterday)
                                    //
                                    if(agenda_today.tasks.count > 0){
                                        self?.subjectDate.onNext(agenda_today.date)
                                    }
                                    else{
                                        if(agenda_today.dataType == .empty){
                                            AppDelegate().sharedInstance().alertSessionExpiredIfNeed(code: AppURL.APIStatusCode.UNAUTHORIZED.rawValue)
                                        }
                                        else{
                                            self?.subjectDate.onNext(agenda_today.date)
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            AppDelegate().sharedInstance().alertSessionExpiredIfNeed(code: AppURL.APIStatusCode.UNAUTHORIZED.rawValue)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    deinit {
        // Release all resources - perform the deinitialization
        subjectUser.on(.completed)
        subjectDate.on(.completed)
        subjectCreateAgendaEventStared.on(.completed)
    }
}

class WelcomePlanningViewController : BaseViewController {
    //Views
    var bgScaleView : UIView = UIView(frame: .zero)
    var imvAvatar : UIImageView = UIImageView(frame: .zero)
    var lbUserName : UILabel = UILabel(frame: .zero)
    var btnPlanning = RaisedButton.init(type: .custom)
    var agenda : Agenda = Agenda(agendaId: "", dataType: .empty)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        setDefaultValue()
    }
    
    func initView(){
        //Background View
        let yScreenView : CGFloat = 0
        let wScreenView : CGFloat = AppDevice.ScreenComponent.Width
        let hScreenView : CGFloat = AppDevice.ScreenComponent.Height - yScreenView
        let bgView = UIView(frame: CGRect(x: 0, y: yScreenView, width: wScreenView, height: hScreenView))
        bgView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(bgView)
        
        //
        bgScaleView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        bgView.addSubview(bgScaleView)
        
        let xScaleView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wScaleView : CGFloat = bgView.frame.size.width - (xScaleView * 2)
        var yScreen : CGFloat = 0
        
        //Username
        let xUserName : CGFloat = 0
        let yUserName : CGFloat = 0
        let wUserName : CGFloat = wScaleView - (xUserName * 2)
        let hUserName : CGFloat = 24
        let pBottomUserName : CGFloat = 8
        
        lbUserName.frame = CGRect(x: xUserName, y: yUserName, width: wUserName, height: hUserName)
        lbUserName.accessibilityIdentifier = AccessiblityId.LB_USERNAME
        lbUserName.textAlignment = .center
        lbUserName.numberOfLines = 0
        lbUserName.textColor = AppColor.NormalColors.BLACK_COLOR
        lbUserName.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 16)
        lbUserName.hero.isEnabled = true
        lbUserName.hero.modifiers = [.fade, .scale(0.5)]
        bgScaleView.addSubview(lbUserName)
        
        yScreen = lbUserName.frame.origin.y + lbUserName.frame.size.height + pBottomUserName
        
        //Welcome
        let xWelcome : CGFloat = xUserName
        let yWelcome : CGFloat = yScreen
        let wWelcome : CGFloat = wUserName
        let hWelcome : CGFloat = hUserName
        let pBottomWelcome : CGFloat = 61
        
        let lbWelcome = UILabel(frame: CGRect(x: xWelcome, y: yWelcome, width: wWelcome, height: hWelcome))
        lbWelcome.textAlignment = .center
        lbWelcome.numberOfLines = 0
        lbWelcome.textColor = AppColor.NormalColors.BLACK_COLOR
        lbWelcome.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbWelcome.text = NSLocalizedString("welcome_to_macaw", comment: "")
        bgScaleView.addSubview(lbWelcome)
        
        yScreen += hWelcome
        yScreen += pBottomWelcome
        
        //Avatar
        let yAvatar : CGFloat = yScreen
        let wAvatar : CGFloat = 240
        let hAvatar : CGFloat = 180
        let xAvatar : CGFloat = (wScaleView-wAvatar)/2
        let pBottomAvatar : CGFloat = 24
        
        imvAvatar.frame = CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar)
        imvAvatar.image = UIImage(named: "bg_welcome_planning")
        bgScaleView.addSubview(imvAvatar)
        
        yScreen += hAvatar
        yScreen += pBottomAvatar
        
        //Title
        let xTitleName : CGFloat = xUserName
        let yTitleName : CGFloat = yScreen
        let wTitleName : CGFloat = wUserName
        let hTitleName : CGFloat = 20
        let pBottomTitleName : CGFloat = 5
        
        let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName.textAlignment = .center
        lbTitleName.numberOfLines = 0
        lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleName.text = NSLocalizedString("starting_your_work_day_right", comment: "")
        bgScaleView.addSubview(lbTitleName)
        
        yScreen += hTitleName
        yScreen += pBottomTitleName
        
        // Title name
        let xTitleDescription : CGFloat = xTitleName
        let yTitleDescription : CGFloat = yScreen
        let wTitleDescription : CGFloat = wTitleName
        let hTitleDescription : CGFloat = 56
        let pBottomTitleDescription : CGFloat = 24
        
        let lbTitleDescription = UILabel(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
        lbTitleDescription.textAlignment = .center
        lbTitleDescription.numberOfLines = 0
        lbTitleDescription.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleDescription.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitleDescription.text = NSLocalizedString("beginning_your_new_great_day", comment: "")
        bgScaleView.addSubview(lbTitleDescription)
        
        yScreen += hTitleDescription
        yScreen += pBottomTitleDescription
        
        let wButtonInvite : CGFloat = 160
        let xButtonInvite : CGFloat = (wScaleView - wButtonInvite)/2
        let yButtonInvite : CGFloat = yScreen
        
        let hButtonInvite : CGFloat = AppDevice.ScreenComponent.ButtonHeight
        btnPlanning.pulseColor = .white
        btnPlanning.accessibilityIdentifier = AccessiblityId.BTN_PLANNING
        btnPlanning.frame = CGRect(x: xButtonInvite, y: yButtonInvite, width: wButtonInvite, height: hButtonInvite)
        btnPlanning.titleLabel?.textAlignment = .center
        btnPlanning.layer.cornerRadius = 4.0
        btnPlanning.layer.masksToBounds = true
        btnPlanning.hero.isEnabled = true
        btnPlanning.hero.modifiers = [.fade, .scale(0.5)]
        let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
        btnPlanning.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
        btnPlanning.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
        btnPlanning.setTitle(NSLocalizedString("start_planning", comment:"").uppercased(), for: .normal)
        btnPlanning.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
        btnPlanning.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnPlanning.titleLabel?.textAlignment = .right
        btnPlanning.addTarget(self, action: #selector(planningAction), for: .touchUpInside)
        bgScaleView.addSubview(btnPlanning)
        
        let hScaleView : CGFloat = btnPlanning.frame.origin.y + btnPlanning.frame.size.height
        let yScaleView : CGFloat = (bgView.frame.size.height-hScaleView)/2
        bgScaleView.frame = CGRect(x: xScaleView, y: yScaleView, width: wScaleView, height: hScaleView)
    }
    
    func initLoading(){
        let wScreenView : CGFloat = AppDevice.ScreenComponent.Width
        let hScreenView : CGFloat = AppDevice.ScreenComponent.Height
        
        // Animation view
        let wViewAnimation : CGFloat = 100
        let hViewAnimation : CGFloat = wViewAnimation
        let xViewAnimation : CGFloat = (wScreenView - wViewAnimation ) / 2
        let yViewAnimation : CGFloat = (hScreenView - hViewAnimation ) / 2
        
        let viewAnimation = VDSCircleAnimation(frame: CGRect(x: xViewAnimation, y: yViewAnimation, width: wViewAnimation, height: hViewAnimation))
        self.view.addSubview(viewAnimation)
        
    }
    
    func setDefaultValue(){
        let userName = AppSettings.sharedSingleton.account?.name
        lbUserName.text = NSLocalizedString("hi", comment: "") + (userName ?? "You")
    }
    
    @objc func planningAction(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        //
        let heroId = "cell\(2)"
        btnPlanning.hero.id = heroId
        //
        let planningView = TeamAgendaPlanningViewController(nibName: nil, bundle: nil)
        planningView.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        planningView.viewType = .add
        planningView.userId = userId
        planningView.agenda = self.agenda
        planningView.hero.isEnabled = true
        planningView.btnDonePlanning.hero.id = heroId
        hero.replaceViewController(with: planningView)
        
    }
}

