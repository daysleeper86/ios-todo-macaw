//
//  FileBrowserViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 8/5/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Photos

class FileBrowserViewController: BaseViewController,EWImageCropperDelegate,SwipeTableViewCellDelegate,UITableViewDelegate {
    
    
    //External props
    var taskDetailData : Task = Task()
    
    //View
    var navigationView : CustomNavigationView?
    var tbFilesHeaderView : UIView?
    var tbFiles : UITableView = UITableView(frame: .zero)
    //Data
    var yScreen_Scroll : CGFloat = 0
    var fileViewModel : FileViewModel?
    var subjectTaskId : BehaviorSubject<String>?
    var subjectUploadFile = BehaviorSubject<[File]>(value: [])
    let attachmentCellIdentifier : String = "AttachmentCellIdentifier"
    let attachmentProgressingCellIdentifier : String = "AttachmentProgressingCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initData()
        //
        initView()
        //
        bindToViewModel()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func initData(){
        subjectTaskId = BehaviorSubject<String>(value: taskDetailData.taskId)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = UIButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView?.addSubview(btnClose)
        
        //Action Button
        let pAddFile : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wAddFile : CGFloat = hContentHeaderView
        let hAddFile = wAddFile
        let xAddFile = navigationView!.frame.size.width - wAddFile - pAddFile
        let yAddFile : CGFloat = yContentHeaderView
        
        let btnAddFile = UIButton.init(type: .custom)
        btnAddFile.frame = CGRect(x: xAddFile, y: yAddFile, width: wAddFile, height: hAddFile)
        btnAddFile.setImage(UIImage(named: "ic_add_file"), for: .normal)
        btnAddFile.addTarget(self, action: #selector(addFileAction(sender:)), for: .touchUpInside)
        navigationView?.addSubview(btnAddFile)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView!.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("attachment", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableNotification : CGFloat = 0
        let yTableNotification : CGFloat = yHeader
        let wTableNotification : CGFloat = AppDevice.ScreenComponent.Width
        let hTableNotification : CGFloat = AppDevice.ScreenComponent.Height - yTableNotification
        tbFiles.frame = CGRect(x: xTableNotification, y: yTableNotification, width: wTableNotification, height: hTableNotification)
        tbFiles.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbFiles.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbFiles.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbFiles.register(FileItemViewCell.self, forCellReuseIdentifier: self.attachmentCellIdentifier)
        tbFiles.register(FileProgressingItemViewCell.self, forCellReuseIdentifier: self.attachmentProgressingCellIdentifier)
        self.view.addSubview(tbFiles)
        
        
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableNotification
        tbFilesHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wTableNotification, height: 0))
        tbFilesHeaderView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = 0
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 10.0
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("attachment", comment: "")
        tbFilesHeaderView?.addSubview(lbTitleScreen)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbFilesHeaderView?.frame.size.height = hHeaderTableView
        tbFiles.tableHeaderView = tbFilesHeaderView!
    }

    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addFileAction(sender: UIButton){
        requestAlbumPhoto(view: sender)
    }
    
    func presentPickerPhoto(_ view: UIView?){
        let photoViewController = EWPhotoCollectionViewController()
        photoViewController.delegate = self
        photoViewController.needCrop = false
        photoViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        
        //
        if(view != nil){
            let heroId = "openPhotoPicker"
            view!.hero.id = heroId
            //
            photoViewController.hero.isEnabled = true
            photoViewController.collectionView.hero.id = heroId
        }
        //
        let navigationController = UINavigationController(rootViewController: photoViewController)
        navigationController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func requestAlbumPhoto(view: UIView?){
        //Check permission for access photo library
        let status : PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if (status == .notDetermined){
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({[weak self] status in
                guard let strongSelf = self else { return }
                strongSelf.presentPickerPhoto(view)
            })
        }
        else{
            presentPickerPhoto(view)
        }
    }
    
    func bindToViewModel(){
        guard let strongSubjectTaskId = subjectTaskId else { return }
        
        let fileController = FileController.sharedAPI
        self.fileViewModel = FileViewModel(
            input: (
                taskId: strongSubjectTaskId.asObservable(),
                dataSource: subjectUploadFile,
                fileController: fileController
            )
        )
        
        self.fileViewModel?.files?.bind(to: tbFiles.rx.items){[weak self] (table, index, element) -> UITableViewCell in
            guard let strongSelf = self else { return UITableViewCell()}
            //
//            element.progressUpload = 0.5
            if(element.progressUpload > 0 && element.progressUpload < 1){
                guard let cell = table.dequeueReusableCell(withIdentifier:   strongSelf.attachmentProgressingCellIdentifier, for: IndexPath.init(row: index, section: 0)) as? FileProgressingItemViewCell else {
                    return FileProgressingItemViewCell()
                }
                cell.selectionStyle = .none
                cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                cell.setValueForCell_File(file: element)
                return cell
            }
            else{
                guard let cell = table.dequeueReusableCell(withIdentifier:  strongSelf.attachmentCellIdentifier, for: IndexPath.init(row: index, section: 0)) as? FileItemViewCell else {
                return FileItemViewCell()
                }
                cell.selectionStyle = .none
                cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                cell.delegate = strongSelf
                //
                cell.setValueForCell_File(file: element)
                return cell
            }
            
            }.disposed(by: disposeBag)
        
        tbFiles.rx
            .modelSelected(File.self)
            .subscribe(onNext: { value in
                
            })
            .disposed(by: disposeBag)
        
        tbFiles.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
            }.disposed(by: disposeBag)
        
        tbFiles.rx
            .setDelegate(self)
            .disposed(by : disposeBag)
    }
    
    // MARK: EWImageCropperDelegate
    func imageCropper(_ cropperViewController: UIViewController, didFinished editImg: UIImage, sourceType source: UIImagePickerController.SourceType) {
        
    }
    
    // MARK: SwipeTableViewCellDelegate
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let strSpace = "  "
        
        let strTitleDelete = NSLocalizedString("delete", comment: "") + strSpace
        let deleteAction = SwipeAction(style: .default, title: strTitleDelete) { action, indexPath in
            // handle action by updating model with deletion
        }
        // customize the action appearance
        deleteAction.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        deleteAction.textColor = AppColor.NormalColors.WHITE_COLOR
        deleteAction.backgroundColor = AppColor.NormalColors.LIGHT_RED_COLOR
        deleteAction.image = UIImage(named: "ic_file_delete")
        deleteAction.imageTitleOrientation = .horizontal
        deleteAction.paddingBottom = AppDevice.ScreenComponent.ItemPadding
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 100
        options.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //        options.heig
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? FileItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.MicsColors.DISABLE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? FileItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.NormalColors.WHITE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return AppDevice.TableViewRowSize.FileHeight + AppDevice.ScreenComponent.ItemPadding
    }
    
}
