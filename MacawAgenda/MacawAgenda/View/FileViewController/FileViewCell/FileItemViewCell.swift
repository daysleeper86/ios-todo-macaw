//
//  FileItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class FileItemViewCell: SwipeTableViewCell {
    
    //Views
    var backgroundViewCell : UIView?
    var imvAvatar : FAShimmerImageView?
    var lbTitleName : FAShimmerLabelView?
    var lbTextDescription : FAShimmerLabelView?
    
    //Data
    var fileDocument : File?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
        lbTextDescription?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTextDescription?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        imvAvatar?.startShimmering()
        lbTitleName?.startShimmering()
        lbTextDescription?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTextDescription?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopShimmering()
                lbTextDescription?.stopShimmering()
            }
        }
    }
    
    func startProcessing(){
        backgroundViewCell?.startShimmering()
    }
    
    func stopProcessing(){
        backgroundViewCell?.stopShimmering()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.FileHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        backgroundViewCell?.layer.cornerRadius = 4.0
        backgroundViewCell?.layer.masksToBounds = true
        backgroundViewCell?.layer.borderColor = AppColor.MicsColors.FILE_MESSAGE_BORDER_COLOR.cgColor
        backgroundViewCell?.layer.borderWidth = 1.0
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconHeight
        let xAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconX
        let yAvatar : CGFloat = (backgroundViewCell!.frame.size.height-AppDevice.SubViewFrame.AvatarIconHeight)/2
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.clipsToBounds = true
        imvAvatar?.contentMode = .scaleAspectFill
        imvAvatar?.layer.cornerRadius = 3.0
        imvAvatar?.layer.borderColor = AppColor.MicsColors.LINE_VIEW_NORMAL.cgColor
        imvAvatar?.layer.borderWidth = 0.6
        imvAvatar?.layer.masksToBounds = true
        backgroundViewCell?.addSubview(imvAvatar!)
        
        // Title name
        let pTitleName : CGFloat = xAvatar
        let xTitleName : CGFloat = imvAvatar!.frame.origin.x+imvAvatar!.frame.size.width+pTitleName;
        let yTitleName : CGFloat = imvAvatar!.frame.origin.y-3
        let wTitleName : CGFloat = backgroundViewCell!.frame.size.width - xTitleName - pTitleName
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.MicsColors.DARK_LIGHT_MENU_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15)
        backgroundViewCell?.addSubview(lbTitleName!)
        
        
        // Title duedate
        let xTitleDescription : CGFloat = lbTitleName!.frame.origin.x
        let yTitleDescription : CGFloat = lbTitleName!.frame.origin.y+lbTitleName!.frame.size.height+2
        let wTitleDescription : CGFloat = lbTitleName!.frame.size.width
        let hTitleDescription : CGFloat = lbTitleName!.frame.size.height
        
        lbTextDescription = FAShimmerLabelView(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
        lbTextDescription?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTextDescription?.textAlignment = .left
        lbTextDescription?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTextDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        backgroundViewCell?.addSubview(lbTextDescription!)
        
        //
        
    }
    
    func setValueForCell_File(file: File){
        if(file.dataType == .normal){
            //
            stopShimmering_Cell()
            //
            self.fileDocument = file
            //
            if(file.file_type.isImageExtension()){
                if(file.local_link.count > 0){
                    var image : UIImage? = Util.loadImageFromLocal(file.local_link)
                    if(image == nil){
                        image = LocalImage.FilePlaceHolder
                    }
                    imvAvatar?.image = image
                }
                else{
                    if(file.pathURL.count>0){
                        if let url = URL(string: file.pathURL){
                            imvAvatar?.sd_setImage(with: url, placeholderImage: LocalImage.FilePlaceHolder)
                        }
                        else{
                            imvAvatar?.image = LocalImage.FilePlaceHolder
                        }
                    }
                    else{
                        imvAvatar?.image = LocalImage.FilePlaceHolder
                    }
                }
            }
            else{
                let strIcon = file.file_type.getIconForFile()
                imvAvatar?.image = UIImage(named: strIcon)
            }
            
            let strName = file.name
            lbTitleName?.text = strName
            
            var strFilesize : String
            if(file.size == 0){
                strFilesize = NSLocalizedString("calculating_size", comment:"");
            }
            else{
                strFilesize = Util.formatByteToOther(file.size)
            }
            //
            let strDate = file.uploadedAt.dayMonthYearString()
            let strFormatDescription = String(format: "%@ • %@", strFilesize,strDate)
            lbTextDescription?.text = strFormatDescription
        }
        else{
            startShimmering_Cell()
        }
        stopProcessing()
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    @objc func menuAction(){
        
    }
}
