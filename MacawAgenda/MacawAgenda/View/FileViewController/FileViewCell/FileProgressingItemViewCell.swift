//
//  FileItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import GTProgressBar

class FileProgressingItemViewCell: SwipeTableViewCell {
    
    //Views
    var backgroundViewCell : UIView?
    var imvAvatar : UIImageView?
    var lbTitleName : UILabel?
    var btnMenu : UIButton?
    var lbTextDescription : UILabel?
    var progressBar : GTProgressBar?
    
    //Data
    var fileDocument : File?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.FileHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        backgroundViewCell?.layer.cornerRadius = 4.0
        backgroundViewCell?.layer.masksToBounds = true
        backgroundViewCell?.layer.borderColor = AppColor.MicsColors.LINE_COLOR.cgColor
        backgroundViewCell?.layer.borderWidth = 1.0
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconHeight
        let xAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconX
        let yAvatar : CGFloat = (backgroundViewCell!.frame.size.height-AppDevice.SubViewFrame.AvatarIconHeight)/2
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.clipsToBounds = true
        imvAvatar?.contentMode = .scaleAspectFill
        imvAvatar?.layer.cornerRadius = 3.0
        imvAvatar?.layer.borderColor = AppColor.MicsColors.LINE_VIEW_NORMAL.cgColor
        imvAvatar?.layer.borderWidth = 0.6
        imvAvatar?.layer.masksToBounds = true
        backgroundViewCell?.addSubview(imvAvatar!)
        
        let hMenuButton : CGFloat = 16
        let pMenuButton : CGFloat = 12
        let xMenuButton : CGFloat = backgroundViewCell!.frame.size.width-hMenuButton-pMenuButton
        let yMenuButton : CGFloat = imvAvatar!.frame.origin.y-3
        
        // Title name
        let pTitleName : CGFloat = xAvatar
        let xTitleName : CGFloat = imvAvatar!.frame.origin.x+imvAvatar!.frame.size.width+pTitleName;
        let yTitleName : CGFloat = yMenuButton
        let wTitleName : CGFloat = backgroundViewCell!.frame.size.width - xTitleName - pTitleName - hMenuButton - pMenuButton
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15)
        backgroundViewCell?.addSubview(lbTitleName!)
        
        // Title duedate
        let xTitleDescription : CGFloat = lbTitleName!.frame.origin.x
        let yTitleDescription : CGFloat = lbTitleName!.frame.origin.y+lbTitleName!.frame.size.height+2
        let wTitleDescription : CGFloat = lbTitleName!.frame.size.width
        let hTitleDescription : CGFloat = lbTitleName!.frame.size.height
        
        lbTextDescription = UILabel(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
        lbTextDescription?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTextDescription?.textAlignment = .left
        lbTextDescription?.textColor = AppColor.NormalColors.RED_COLOR
        lbTextDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        lbTextDescription?.isUserInteractionEnabled = true
        lbTextDescription?.isHidden = true
        lbTextDescription?.text = NSLocalizedString("failed_to_upload_retry", comment: "")
        backgroundViewCell?.addSubview(lbTextDescription!)
        
        //
        btnMenu = FAShimmerButtonView.init(type: .custom)
        btnMenu?.frame = CGRect(x: xMenuButton, y: yMenuButton, width: hMenuButton, height: hMenuButton)
        btnMenu?.setImage(UIImage(named: "ic_close_tf_black"), for: .normal)
        backgroundViewCell?.addSubview(btnMenu!)
        
        // Title duedate
        let hProgressing : CGFloat = 4.0
        let xProgressing : CGFloat = lbTitleName!.frame.origin.x
        let yProgressing : CGFloat = yAvatar + hAvatar - hProgressing
        let wProgressing : CGFloat = backgroundViewCell!.frame.size.width - xTitleName - pMenuButton
        
        progressBar = GTProgressBar(frame: CGRect(x: xProgressing, y: yProgressing, width: wProgressing, height: hProgressing))
        progressBar?.orientation = GTProgressBarOrientation.horizontal
        progressBar?.displayLabel = false
        progressBar?.barBorderColor = AppColor.MicsColors.BLUE_LIGHT_2_COLOR
        progressBar?.barFillColor = AppColor.MicsColors.BLUE_LIGHT_2_COLOR
        progressBar?.barBackgroundColor = AppColor.NormalColors.GRAY_COLOR
        progressBar?.barBorderWidth = 0
        progressBar?.barFillInset = 0
        progressBar?.progress = 0
        backgroundViewCell?.addSubview(progressBar!)
        
    }
    
    func startProcessing(){
        backgroundViewCell?.startShimmering()
    }
    
    func stopProcessing(){
        backgroundViewCell?.stopShimmering()
    }
    
    func setValueForCell_File(file: File){
        fileDocument = file
        //
        if(file.local_link.count > 0){
            var image : UIImage? = Util.loadImageFromLocal(file.local_link)
            if(image == nil){
                image = LocalImage.FilePlaceHolder
            }
            imvAvatar?.image = image
        }
        else{
            if(file.pathURL.count>0){
                if let url = URL(string: file.pathURL){
                    imvAvatar?.sd_setImage(with: url, placeholderImage: LocalImage.FilePlaceHolder)
                }
                else{
                    imvAvatar?.image = LocalImage.FilePlaceHolder
                }
            }
            else{
                imvAvatar?.image = LocalImage.FilePlaceHolder
            }
        }
        
        let strName = file.name
        lbTitleName?.text = strName
        //
        showLoadingIndicator_Upload()
    }
    
    func showLoadingIndicator_Upload(){
        guard let strongFile = fileDocument else { return }
        progressBar?.progress = CGFloat(strongFile.progressUpload)
    }
    
    func setStateForCellWhenUploading(){
        guard let strongFile = fileDocument else { return }
        if(strongFile.dataType == .failed){
            backgroundViewCell?.layer.borderColor = AppColor.NormalColors.LIGHT_RED_COLOR.cgColor
            lbTextDescription?.isHidden = false
            progressBar?.isHidden = true
        }
        else{
            backgroundViewCell?.layer.borderColor = AppColor.MicsColors.LINE_COLOR.cgColor
            lbTextDescription?.isHidden = true
            progressBar?.isHidden = false
        }
    }
    
}
