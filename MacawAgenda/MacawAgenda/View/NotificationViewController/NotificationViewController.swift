//
//  BacklogViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import RxDataSources
import Material

class NotificationViewController: BaseViewController {
    //External props
    var userId : String = ""
    
    //View
    private var navigationView : CustomNavigationView?
    private var tbNotificationHeaderView : UIView?
    private var btnReadAll : IconButton?
    private var tbNotification : UITableView = UITableView(frame: .zero)
    
    //Data
    static let notificationCellIdentifier : String = "notificationCellIdentifier"
    static let emptyNotificationCellIdentifier : String = "emptyNotificationCellIdentifier"
    var yScreen_Scroll : CGFloat = 0
    var arrayNotifications : [NotificationData] = []
    
    //ViewModel
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    
    //Datasource
//    let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, NotificationData>>(
//        configureCell: {(_ ,table, indexPath, element) -> UITableViewCell in
//            if element.dataType == .empty {
//                guard let cell = table.dequeueReusableCell(withIdentifier:  NotificationViewController.emptyNotificationCellIdentifier, for: indexPath) as? NotificationEmptyViewCell else {
//                    return NotificationEmptyViewCell()
//                }
//                return cell
//            }else{
//                guard let cell = table.dequeueReusableCell(withIdentifier:  NotificationViewController.notificationCellIdentifier, for: indexPath) as? NotificationItemViewCell else {
//                    return NotificationItemViewCell()
//                }
//                //
//                cell.selectionStyle = .none
//                cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
//                //My user
//                let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
//                let myUserName = AppSettings.sharedSingleton.account?.name ?? ""
//                var myUser = User(userId: myUserId, dataType: .normal)
//                myUser.name = myUserName
//                //
//                cell.setValueForCell(notification: element,user: myUser)
//                return cell
//            }
//
//    },
//        titleForHeaderInSection: { dataSource, sectionIndex in
//            return dataSource[sectionIndex].model
//        }
//    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initData()
        //
        initView()
        //
        bindToViewModel()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Action Button
        let pReadAll : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wReadAll : CGFloat = hContentHeaderView
        let hReadAll = wReadAll
        let xReadAll = navigationView!.frame.size.width - wReadAll - pReadAll
        let yReadAll : CGFloat = yContentHeaderView
        
        btnReadAll = IconButton.init(type: .custom)
        btnReadAll?.frame = CGRect(x: xReadAll, y: yReadAll, width: wReadAll, height: hReadAll)
        btnReadAll?.setImage(UIImage(named: "ic_read_all"), for: .normal)
        btnReadAll?.isUserInteractionEnabled = false
        navigationView?.addSubview(btnReadAll!)
        
        //Title Screen
        let pTitleHeader : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xTitleHeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = xReadAll - xTitleHeader - pTitleHeader
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("notifications", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableNotification : CGFloat = 0
        let yTableNotification : CGFloat = yHeader
        let wTableNotification : CGFloat = AppDevice.ScreenComponent.Width
        let hTableNotification : CGFloat = AppDevice.ScreenComponent.Height - AppDevice.ScreenComponent.TabbarHeight - yTableNotification
        tbNotification.frame = CGRect(x: xTableNotification, y: yTableNotification, width: wTableNotification, height: hTableNotification)
        tbNotification.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbNotification.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbNotification.dataSource = self
        tbNotification.delegate = self
        tbNotification.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbNotification.register(NotificationItemViewCell.self, forCellReuseIdentifier: NotificationViewController.notificationCellIdentifier)
        tbNotification.register(NotificationEmptyViewCell.self, forCellReuseIdentifier: NotificationViewController.emptyNotificationCellIdentifier)
        self.view.addSubview(tbNotification)
        
        
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableNotification
        tbNotificationHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wTableNotification, height: 0))
        tbNotificationHeaderView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = 0
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 10.0

        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("notifications", comment: "")
        tbNotificationHeaderView?.addSubview(lbTitleScreen)

        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbNotificationHeaderView?.frame.size.height = hHeaderTableView
        tbNotification.tableHeaderView = tbNotificationHeaderView!
    }
    
    func initData(){
        arrayNotifications = NotificationStored.sharedSingleton.notifications
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func readNotification(notification: NotificationData){
        let projectNotificationId = notification.projectId
//        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        if(projectNotificationId.count > 0){
//            > 0 && projectNotificationId == projectId){
            let notificationId = notification.notificationId
            //
            let token = AppSettings.sharedSingleton.account?.token ?? ""
            let authPlugin = AccessTokenPlugin { token }
            let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
            let notificationAPIController = NotificationAPIController(provider: projectProvider)
            let notificationViewModel = NotificationAPIViewModel(
                inputReadNotification: (
                    projectId: projectNotificationId,
                    notificationId: notificationId,
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    notificationAPIController: notificationAPIController
                )
            )
            //
            notificationViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
                switch loginResult {
                case .failed(let output):
                    let appDelegate = AppDelegate().sharedInstance()
                    if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                        self?.makeToast(message: output.message, duration: 3.0, position: .top)
                    }
                case .ok(let output):
                    if let reponseAPI = output as? ResponseAPI{
                        if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                            if(reponseAPI.code == 200){
                            }
                            else{
                                
                            }
                        }
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
    func bindToViewModel(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""

        let notificationController = NotificationController.sharedAPI
        
        let counterNotificationViewModel = NotificationViewModel(
            inputSubscribleCountNotification: (
                projectId:projectId,
                userId: subjectUser.asObservable(),
                notificationController: notificationController
            )
        )
        counterNotificationViewModel.counter?.subscribe({counter  in
            guard let strongElement = counter.element else { return }
            let count = strongElement.count
            AppSettings.sharedSingleton.saveMiscAccount(count,name: nil,password: nil,deviceToken: nil)
            //
            if(count == 0){
                self.btnReadAll?.isUserInteractionEnabled = false
            }
            else{
                self.btnReadAll?.isUserInteractionEnabled = true
            }
            //
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_NUMBER_READ_NOTIFICATION), object: nil)
        })
            .disposed(by: disposeBag)
        
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let notificationAPIController = NotificationAPIController(provider: projectProvider)
        let observableTap = btnReadAll!.rx.tap.asObservable()
        let notificationViewModel = NotificationAPIViewModel(
            inputReadAllNotification: (
                projectId: projectId,
                loginTaps: observableTap,
                notificationAPIController: notificationAPIController
            )
        )
        //
        notificationViewModel.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.btnReadAll?.isUserInteractionEnabled = false
                    self?.btnReadAll?.startShimmering()
                }
                else{
                    self?.btnReadAll?.isUserInteractionEnabled = true
                    self?.btnReadAll?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        //
        notificationViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message: output.message, duration: 3.0, position: .top)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.makeToast(message: NSLocalizedString("update_all_notification_successfully", comment: ""), duration: 1.0, position: .top)
                            //
                            let count = 0
                            AppSettings.sharedSingleton.saveMiscAccount(count,name: nil,password: nil,deviceToken: nil)
                        }
                        else{
                            self?.makeToast(message: NSLocalizedString("update_all_notification_unsuccessfully", comment: ""), duration: 1.0, position: .top)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
//        tbNotification.rx
//            .setDelegate(self)
//            .disposed(by : disposeBag)
        
        tbNotification.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
        }.disposed(by: disposeBag)
    }
}

extension NotificationViewController: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNotifications.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight : CGFloat = 0
//        let dataSection = dataSource[indexPath.section]
        let element = arrayNotifications[indexPath.row]
        if element.dataType == .empty {
            cellHeight = tableView.frame.size.height
        }
        else{
            cellHeight = AppDevice.TableViewRowSize.NotificationHeight
        }
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let element = arrayNotifications[indexPath.row]
        if element.dataType == .empty {
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  NotificationViewController.emptyNotificationCellIdentifier, for: indexPath) as? NotificationEmptyViewCell else {
                return NotificationEmptyViewCell()
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  NotificationViewController.notificationCellIdentifier, for: indexPath) as? NotificationItemViewCell else {
                return NotificationItemViewCell()
            }
            //
            cell.selectionStyle = .none
            cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            //My user
            let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
            let myUserName = AppSettings.sharedSingleton.account?.name ?? ""
            var myUser = User(userId: myUserId, dataType: .normal)
            myUser.name = myUserName
            //
            cell.setValueForCell(notification: element,user: myUser)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let notification = arrayNotifications[indexPath.row]
        if(notification.isRead == false){
            self.readNotification(notification: notification)
        }
        //
        AppDelegate().sharedInstance().processNotificationAllProject(notification: notification)
    }
}

//Notification event
extension NotificationViewController {
    
    func addNotification(){
        addNotification_UpdateDataNotification()
    }
    
    
    func addNotification_UpdateDataNotification(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UpdateDataNotification(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_NEW_DATA_NOTIFICATION),
                                               object: nil)
    }
    
    @objc func UpdateDataNotification(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let notifications = dictionary[ModelDataKeyString.COLLECTION_NOTIFICATION] as? [NotificationData]{
                if(notifications.count > 0){
                    self.btnReadAll?.isUserInteractionEnabled = true
                }
                self.arrayNotifications = notifications
                self.tbNotification.reloadData()
            }
        }
    }
}
