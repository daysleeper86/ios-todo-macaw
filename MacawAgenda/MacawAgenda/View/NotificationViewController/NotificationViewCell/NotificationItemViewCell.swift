//
//  NotificationItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/22/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import SkeletonView

class NotificationItemViewCell: UITableViewCell {
    //Views
    private var backgroundViewCell : UIView?
    private var imvAvatar : FAShimmerImageView?
    private var lbTitleName : FAShimmerLabelView?
    private var lbTime : FAShimmerLabelView?
    private var lineView : UIView?
    private var cycleView : UIView?
    
    //Data
    var notification : NotificationData?
    //Static
    static let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.NotificationHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        let pCycle : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wCycle : CGFloat = 12
        let hCycle : CGFloat = wCycle
        let yCycle : CGFloat = pCycle
        let xCycle : CGFloat = wBackgroundView - pCycle - wCycle
        
        cycleView  = UIView(frame: CGRect(x: xCycle, y: yCycle, width: wCycle, height: hCycle))
        cycleView?.layer.backgroundColor = AppColor.NormalColors.BLUE_COLOR.cgColor
        cycleView?.layer.cornerRadius = cycleView!.frame.size.height/2
        cycleView?.layer.masksToBounds = true
        cycleView?.isHidden = true
        backgroundViewCell?.addSubview(cycleView!)
        
        // Avatar
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xAvatar : CGFloat = pAvatar
        let yAvatar : CGFloat = pAvatar
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hAvatar : CGFloat = wAvatar
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.layer.masksToBounds = true
        backgroundViewCell?.addSubview(imvAvatar!)
        
        // Title name
        let xTitleName : CGFloat = xAvatar + wAvatar + NotificationItemViewCell.pTitleName
        let wTitleName : CGFloat = xCycle - xTitleName - NotificationItemViewCell.pTitleName
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight * 2
        let yTitleName : CGFloat = yAvatar
        let pBottomTitleName : CGFloat = 8
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        backgroundViewCell?.addSubview(lbTitleName!)
        
        // Title time
        let xTitleTime : CGFloat = xTitleName
        let wTitleTime : CGFloat = wTitleName
        let hTitleTime : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let yTitleTime : CGFloat = yTitleName + hTitleName + pBottomTitleName
        
        lbTime = FAShimmerLabelView(frame: CGRect(x: xTitleTime, y: yTitleTime, width: wTitleTime, height: hTitleTime))
        lbTime?.textAlignment = .left
        lbTime?.numberOfLines = 1
        lbTime?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTime?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        backgroundViewCell?.addSubview(lbTime!)
        
        //Line view
        let xLineView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wLineView : CGFloat = wBackgroundView - (xLineView * 2)
        let hLineView : CGFloat = 1.0
        lineView = UIView(frame: CGRect(x: xLineView, y: hBackgroundView-hLineView, width: wLineView, height: hLineView))
        lineView?.backgroundColor = AppColor.MicsColors.LINE_COLOR
        backgroundViewCell?.addSubview(lineView!)
    }

    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
        lbTime?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTime?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        cycleView?.isHidden = true
        
        imvAvatar?.startShimmering()
        lbTitleName?.startShimmering()
        lbTime?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTime?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopShimmering()
                lbTime?.stopShimmering()
            }
        }
    }
    
    func showHideCycle(isShow : Bool){
        cycleView?.isHidden = !isShow
    }
    
    func setValueForCell(notification: NotificationData,user: User){
        if(notification.notificationId.count > 1){
            stopShimmering_Cell()
            //
            self.notification = notification
            //
            if(notification.isRead == true){
                backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
                lineView?.backgroundColor = AppColor.MicsColors.DISABLE_COLOR
                //
                showHideCycle(isShow : false)
            }
            else{
                backgroundViewCell?.backgroundColor = AppColor.MicsColors.BLUE_LIGHT_1_COLOR
                lineView?.backgroundColor = AppColor.MicsColors.BLUE_LIGHT_COLOR;
                //
                showHideCycle(isShow : true)
            }
            //
            let attributedString = NotificationService.formatAttributedString(notification: notification,user: user)
            self.lbTitleName?.attributedText = attributedString
            //
            let strDate = notification.createdDate.timeStampForChattingHeader(showMinute: true)
            lbTime?.text = strDate
            //
            stateForAvatar(notification.message.user.userId)
        }
        else{
            startShimmering_Cell()
        }
    }
    
    func stateForAvatar(_ strUserId : String){
        if(strUserId.count > 0){
            imvAvatar?.sd_cancelCurrentImageLoad()
            //
            let strURL = Util.parseURLAvatarImageFireStore(strUserId)
            imvAvatar?.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongNotification = self?.notification else { return }
                if let imageValue = image{
                    //Current userId
                    let strCurrentUserID = strongNotification.message.user.userId
                    if(strCurrentUserID == strUserId){
                        if(imageValue.size.width > 0){
                            self?.imvAvatar?.backgroundColor = .clear
                            self?.imvAvatar?.removeLabelTextIfNeed()
                        }
                        else{
                            let strUserName = strongNotification.message.user.name
                            self?.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                }
                else{
                    //Current userId
                    let strCurrentUserID = strongNotification.message.user.userId
                    if(strCurrentUserID == strUserId){
                        let strUserName = strongNotification.message.user.name
                        self?.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
            })
        }
        else{
            imvAvatar?.image = LocalImage.UserPlaceHolder
        }
    }
    
}
