//
//  TaskEmptyViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/3/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class NotificationEmptyViewCell: UITableViewCell {
    //Views
    private var backgroundViewCell : UIView?
    private var imvAvatar : UIImageView?
    private var lbTitleName : UILabel?
    private var lbTitleDescription : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        var yScreen : CGFloat = 60
        
        // Avatar
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconLargeWidth
        let hAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconLargeHeight
        let xAvatar : CGFloat = (wBackgroundView - wAvatar ) / 2
        let yAvatar : CGFloat = yScreen
        let pBottomAvatar : CGFloat = 24
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.image = UIImage(named: "ic_empty_notification")
        backgroundViewCell!.addSubview(imvAvatar!)
        
        yScreen += hAvatar
        yScreen += pBottomAvatar
        
        // Title name
        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = pTitleName
        let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
        let hTitleName : CGFloat = 24
        let yTitleName : CGFloat = yScreen
        let pBottomTitleName : CGFloat = 8
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .center
        lbTitleName?.textColor = AppColor.MicsColors.DARK_LIGHT_ITEM_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleName?.text = NSLocalizedString("no_notifications_yet", comment: "")
        backgroundViewCell?.addSubview(lbTitleName!)
        
        yScreen += hTitleName
        yScreen += pBottomTitleName
        
        // Title description
        let xTitleDescription : CGFloat = xTitleName
        let wTitleDescription : CGFloat = wTitleName
        let hTitleDescription : CGFloat = 44
        let yTitleDescription : CGFloat = yScreen
        
        lbTitleDescription = UILabel(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
        lbTitleDescription?.textAlignment = .center
        lbTitleDescription?.numberOfLines = 0
        lbTitleDescription?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitleDescription?.text = NSLocalizedString("you_will_be_notified_here_about_any_changes", comment: "")
        backgroundViewCell?.addSubview(lbTitleDescription!)
        
        //Resize background cell wrap content
        backgroundViewCell?.frame.size.height = lbTitleDescription!.frame.origin.y + lbTitleDescription!.frame.size.height + pTitleName
    }
}
