//
//  MessageItemInfoViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class MessageItemInfoViewCell: UITableViewCell {
    
    //Views
    var lbTitleInfo : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        let pTitleInfo : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleInfo : CGFloat = pTitleInfo
        let yTitleInfo : CGFloat = 0
        let wTitleInfo : CGFloat = AppDevice.ScreenComponent.Width - (pTitleInfo * 2)
        let hTitleInfo : CGFloat = AppDevice.SubViewFrame.TitleHeight
        
        lbTitleInfo = UILabel(frame: CGRect(x: xTitleInfo, y: yTitleInfo, width: wTitleInfo, height: hTitleInfo))
        lbTitleInfo?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitleInfo?.numberOfLines = 0
        self.addSubview(lbTitleInfo!)
        
    }
    
    func setValueForCell_NoneComment(message : Message){
        lbTitleInfo?.textAlignment = .left
        lbTitleInfo?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleInfo?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        //
        lbTitleInfo?.text = message.content;
        //
        var height : CGFloat = message.messageHeight
        height += Constant.ChattingView.Padding_Info_Chatting_Cell + Constant.ChattingView.Padding_Info_Chatting_Cell
        lbTitleInfo?.frame.size.height = height
        
    }
    
//    func getMessageContent() -> String{
//        return self.getMessageContent()
//    }
    
     override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
