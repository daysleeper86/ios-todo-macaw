//
//  BaseMessageTableViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class BaseMessageTableViewCell: UITableViewCell {

    var message : Message?
    lazy var likeButton : UIButton = {
        let button : UIButton = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        return button
    }()
    
    
    
    func reloadSpinActionCell(isLike : Bool){
        if(isLike == true){
            self.likeButton.setImage(UIImage(named: LocalImage.SpinIcon.On), for: .normal)
        }
        else{
             self.likeButton.setImage(UIImage(named: LocalImage.SpinIcon.Off), for: .normal)
        }
    }
    
    
    @objc func likeAction(){
    }
    

}
