//
//  ReceiverBaseMessageTableViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class ReceiverBaseMessageTableViewCell: BaseMessageTableViewCell {

    let isOutgoing : Bool = false
    var avatarImageView : FAShimmerImageView = {
        let pImage : CGFloat = Constant.ChattingView.Padding_Chatting_Cell
        let wImage : CGFloat = AppDevice.SubViewFrame.AvatarIconMediumWidth
        
        let imageView = FAShimmerImageView(frame: CGRect(x: pImage, y: pImage, width: wImage, height: wImage))
        imageView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.layer.masksToBounds = true

        return imageView
    }()
    
    lazy var nameLabel : FAShimmerLabelView = {
        let pTitle : CGFloat = Constant.ChattingView.Padding_Chatting_Cell
        let xTitle : CGFloat = self.avatarImageView.frame.origin.x + self.avatarImageView.frame.size.width + pTitle
        let yTitle : CGFloat = self.avatarImageView.frame.origin.y
        let wTitle : CGFloat = Constant.ChattingView.MaxWidth - pTitle - xTitle
        let hTitle : CGFloat = AppDevice.SubViewFrame.TitleHeight
        
        let lbTitle = FAShimmerLabelView(frame: CGRect(x: xTitle, y: yTitle, width: wTitle, height: hTitle))
        lbTitle.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitle.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        lbTitle.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitle.textAlignment = .left
        
        return lbTitle
    }()

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        self.addSubview(self.nameLabel)
        self.addSubview(self.avatarImageView)
    }
    
    func setMessage(message : Message, messagePosition : Constant.MessagePosition, searchOption : String){
        self.message = message
    }
    
    func checkShowAvatar(messagePosition : Constant.MessagePosition) -> Bool{
        return true
    }
    
     
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
