//
//  MessageItemTextViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import WebKit

class MessageItemTextViewCell: ReceiverBaseMessageTableViewCell {

    //Views
    var lbContent : UILabel?
    var wvContent : WKWebView?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
    }
    
    
    override func initView(){
        super.initView()
        
        let pContent : CGFloat = 0
        let xContent : CGFloat = self.nameLabel.frame.origin.x
        let yContent : CGFloat = self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + pContent
        let wContent : CGFloat = Constant.ChattingView.Buble_TextView_Avatar_MaxWidth
        let hContent : CGFloat = Constant.ChattingView.Buble_TextView_Avatar_MaxHeight
        
        let paddingLeftWebview :  CGFloat = 8.0
        let xPaddingLeftWebviewContent : CGFloat = xContent - paddingLeftWebview
        let wPaddingLeftWebviewContent : CGFloat = wContent + (paddingLeftWebview * 2)
        
        let webCfg : WKWebViewConfiguration = WKWebViewConfiguration()
        wvContent = WKWebView(frame: CGRect(x: xPaddingLeftWebviewContent, y: yContent, width: wPaddingLeftWebviewContent, height: hContent), configuration: webCfg)
        wvContent?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        wvContent?.scrollView.isScrollEnabled = false
        wvContent?.scrollView.bounces = false
        self.addSubview(wvContent!)
        
        let hLabelContent : CGFloat = hContent / 2
        let yLabelContent : CGFloat = yContent + ((hContent -  hLabelContent) / 2)
        let xLabelContent : CGFloat = xContent
        let wLabelContent : CGFloat = wContent
        lbContent = UILabel(frame: CGRect(x: xLabelContent, y: yLabelContent, width: wLabelContent, height: hLabelContent))
        self.addSubview(lbContent!)
    }
    
    func resetValue_Cell(){
        self.avatarImageView.image = nil
        self.nameLabel.text = ""
        self.lbContent?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        lbContent?.isHidden = false
        wvContent?.isHidden = true
        //
        self.avatarImageView.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        self.nameLabel.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbContent?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        self.avatarImageView.startShimmering()
        self.nameLabel.startShimmering()
        lbContent?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = lbContent?.isShimmering(){
            if(isShimmering == true){
                //
                lbContent?.isHidden = true
                wvContent?.isHidden = false
                //
                self.avatarImageView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                self.nameLabel.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbContent?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                self.avatarImageView.stopShimmering()
                self.nameLabel.stopShimmering()
                lbContent?.stopShimmering()
            }
        }
    }
    
    func setValueForCell(message : Message){
        if(message.dataType == .normal){
            self.message = message
            //
            stopShimmering_Cell()
    
            let bundleUrl = URL(fileURLWithPath: Bundle.main.bundlePath)
            wvContent?.loadHTMLString(message.content, baseURL: bundleUrl)
            //
            stateForTitle(message.user.name,message.createdAt)
            //
            stateForAvatar(message.user)
        }
        else{
            startShimmering_Cell()
        }
    }
    
    func stateForAvatar(_ user : User){
        //Cancel current request
        self.avatarImageView.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID.count > 0){
            self.avatarImageView.sd_cancelCurrentImageLoad()
            //
            let strURL = Util.parseURLAvatarImageFireStore(strUserID)
            self.avatarImageView.sd_setImage(with: URL(string: strURL), placeholderImage: nil, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongSelf = self, let strongObject = strongSelf.message else { return }
                if let imageValue = image{
                    //Current userId
                    let strCurrentUserID = strongObject.user.userId
                    if(strCurrentUserID == strUserID){
                        if(imageValue.size.width > 0){
                            strongSelf.avatarImageView.backgroundColor = .clear
                            strongSelf.avatarImageView.removeLabelTextIfNeed()
                        }
                        else{
                            let strUserName = strongObject.user.name
                            strongSelf.avatarImageView.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                }
                else{
                    //Current userId
                    let strCurrentUserID = strongObject.user.userId
                    if(strCurrentUserID == strUserID){
                        let strUserName = strongObject.user.name
                        strongSelf.avatarImageView.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
            })
        }
        else{
            let strUserName = user.name
            self.avatarImageView.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
        }

    }
    
    func stateForTitle(_ strUserName : String, _ date : Date){
        let strDate = "• " + date.timeStampForChattingHeader(showMinute: true)
        let strNameDate = strUserName + " " + strDate
        self.nameLabel.text = strNameDate
        //
        var lenghtUserName : Int = 0
        var lenghtNameDate : Int = 0
        if(strUserName.containsEmoji == false){
            lenghtUserName = strUserName.count
            lenghtNameDate = strNameDate.count
        }
        else{
            lenghtUserName = strUserName.utf16.count
            lenghtNameDate = strNameDate.utf16.count
        }
        //
        let attributedString = NSMutableAttributedString(string: strNameDate)
        attributedString.addAttribute(NSAttributedString.Key.font, value: self.nameLabel.font ?? UIFont.boldSystemFont(ofSize: self.nameLabel.font.pointSize), range: NSRange(location: 0, length: lenghtUserName))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: 0, length: lenghtUserName))
        
        //
        let sizeFont : CGFloat = 12
        let dateFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: sizeFont)
        attributedString.addAttribute(NSAttributedString.Key.font, value: dateFont ?? UIFont.systemFont(ofSize: sizeFont), range: NSRange(location: lenghtNameDate-strDate.count, length: strDate.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: lenghtNameDate-strDate.count, length: strDate.count))
        
        self.nameLabel.attributedText = attributedString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
