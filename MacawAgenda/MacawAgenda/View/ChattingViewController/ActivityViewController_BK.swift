//
//  BacklogViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import FirebaseFirestore
import RxSwift
import RxDataSources
import Moya

class ActivityViewController: BaseViewController,UITableViewDelegate,CustomTextViewDelegate,UIWebViewDelegate {
    //External props
    var currentDate : Date?
    //View
    var backgroundView : UIView?
    var tbActivity : UITableView = UITableView(frame: .zero)
    //Data
    var oeMentions:OEMentions!
    static let receiverTextCellIdentitifer : String = "ReceiverTextCellIdentitifer"
    static let emptyActivityCellIdentifier : String = "emptyActivityCellIdentifier"
    static let DefaultHeightWebView : CGFloat = Constant.ChattingView.Buble_TextView_Avatar_MaxHeight
    //ViewModel
    var roomViewModel : RoomViewModel?
    var messageViewModel : MessageViewModel?
    var contentHeights : [CGFloat] = []
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    //Datasource
    var dataSource: RxTableViewSectionedReloadDataSource<MessageSection>?
    
    //Chatting
    var isLockSendButton : Bool =  false
    // For Chat text, image
    lazy var inputTextView : CustomTextView = {
        let hInputTextView : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let xInputTextView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wInputTextView : CGFloat =  self.defaultInputView.frame.size.width - xInputTextView - self.sendTextButton.frame.size.width
        let yInputTextView : CGFloat = 0
        
        let inputTextView = CustomTextView(frame: CGRect(x: xInputTextView, y: yInputTextView, width: wInputTextView, height: hInputTextView))
        inputTextView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        inputTextView.clipsToBounds = true
        inputTextView.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        inputTextView.isScrollable = false
        inputTextView.isEditable = true
        inputTextView.maxNumberOfLines = 5
        inputTextView.delegate = self
        inputTextView.textColor = UIColor(hexString: "#545d64")
        inputTextView.placeholderColor = UIColor(hexString: "#b2bac0")
        inputTextView.returnKeyType = .default
        inputTextView.placeholder = NSLocalizedString("placehoder_text_input", comment: "")
        inputTextView.center = CGPoint(x: inputTextView.center.x, y: self.defaultInputView.frame.size.height / 2)
        return inputTextView
    }()
    
    lazy var sendTextButton : UIButton = {
        let hSendTextButton : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let wSendTextButton : CGFloat = hSendTextButton
        let xSendTextButton : CGFloat = self.defaultInputView.frame.size.width - wSendTextButton
        let ySendTextButton : CGFloat = 0
        
        let sendTextButton = UIButton(frame: CGRect(x: xSendTextButton, y: ySendTextButton, width: wSendTextButton, height: hSendTextButton))
        sendTextButton.setImage(UIImage(named: LocalImage.ChattingImage.Send.NotSelected), for: .normal)
        sendTextButton.setImage(UIImage(named: LocalImage.ChattingImage.Send.Selected), for: .selected)
        sendTextButton.contentMode = .scaleAspectFill
        sendTextButton.isEnabled = false
        
        return sendTextButton
    }()
    
    lazy var defaultInputView : UIView = {
        let xDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wDefaultInputView : CGFloat = self.containerInputChatView.frame.size.width - (xDefaultInputView * 2)
        let hDefaultInputView : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        
        let defaultInputView = UIView(frame: CGRect(x: xDefaultInputView, y: yDefaultInputView, width: wDefaultInputView, height: hDefaultInputView))
        defaultInputView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        defaultInputView.layer.borderColor = AppColor.NormalColors.GRAY_COLOR.cgColor
        defaultInputView.layer.borderWidth = 1.0
        defaultInputView.layer.cornerRadius = 4.0
        defaultInputView.isOpaque = false
        defaultInputView.isHidden = false
        
        
        return defaultInputView
    }()
    
    lazy var containerInputChatView : UIView = {
        let height = Constant.ChattingView.Height_Input_Container_Message
        let containerInputChatView = UIView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: height))
        containerInputChatView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //
        let lineView_Horizontal = UIView(frame: CGRect(x: 0, y: 0, width: containerInputChatView.frame.size.width, height: 1.0))
        lineView_Horizontal.backgroundColor = AppColor.MicsColors.LINE_COLOR
        lineView_Horizontal.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        lineView_Horizontal.layer.shadowOpacity = 1
        lineView_Horizontal.layer.shadowRadius = 4
        lineView_Horizontal.layer.shadowOffset = CGSize(width: 0, height: -1)
        containerInputChatView.addSubview(lineView_Horizontal)
        
        
        return containerInputChatView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
        bindToViewModel()
    }
    
    func addNotification(){
        addNotification_Keyboard()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int{
                
                if((self.inputTextView.isFirstResponder() == true)){
                    let containerFrame = self.containerInputChatView.frame
                    let yContainerInputChatView = backgroundView!.frame.size.height - (keyboardBounds.size.height + containerFrame.size.height)
                
                    // Animations settings
                    UIView.beginAnimations("Change frame over keyboard container", context: nil)
                    UIView.setAnimationBeginsFromCurrentState(true)
                    UIView.setAnimationDuration(duration)
                    UIView.setAnimationCurve(UIView.AnimationCurve(rawValue: curve) ?? .easeInOut)
                
                    // Set views with new info
                    self.containerInputChatView.frame.origin.y = yContainerInputChatView
                
                    // Commit animations
                    UIView.commitAnimations()
                }
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int{
            
            let containerFrame = self.containerInputChatView.frame
            let yContainerInputChatView = backgroundView!.frame.size.height - containerFrame.size.height
            
            // Animations settings
            UIView.beginAnimations("Change frame over keyboard container", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(duration)
            UIView.setAnimationCurve(UIView.AnimationCurve(rawValue: curve) ?? .easeInOut)
            
            // Set views with new info
            self.containerInputChatView.frame.origin.y = yContainerInputChatView
            
            // Commit animations
            UIView.commitAnimations()
        }
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupInputMessage(yInput : CGFloat){
        self.containerInputChatView.addSubview(self.defaultInputView)
        self.defaultInputView.addSubview(self.sendTextButton)
        self.defaultInputView.addSubview(self.inputTextView)
        //
        self.containerInputChatView.frame.origin.y = yInput
    }
    
    func initView(){
        
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Background View
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.ScreenComponent.Height * AppDevice.ScreenComponent.OverlayHeight - AppDevice.ScreenComponent.OverlayHandler
        
        backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: wBackgroundView, height: hBackgroundView))
        backgroundView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.view.addSubview(backgroundView!)
        
        let yScreen : CGFloat = 0
        //Header View
        var yHeader = yScreen
        let hHeader : CGFloat = 44
        let pBottomHeader : CGFloat = 10
        
        let headerView : UIView = UIView(frame: CGRect(x: 0, y: yHeader, width: backgroundView!.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        backgroundView?.addSubview(headerView)
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = 0
        let wTitleScreen : CGFloat = headerView.frame.size.width - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = headerView.frame.size.height
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleScreen.text = NSLocalizedString("discussion", comment: "").uppercased()
        headerView.addSubview(lbTitleScreen)
        
        yHeader += hHeader
        yHeader += pBottomHeader
        
        let hContainerInputChatView : CGFloat = Constant.ChattingView.Height_Input_Container_Message
        let yContainerInputChatView = backgroundView!.frame.size.height - hContainerInputChatView
        
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = AppDevice.ScreenComponent.Width
        let hTableBacklog : CGFloat = backgroundView!.frame.size.height - yTableBacklog - hContainerInputChatView
        tbActivity.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbActivity.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbActivity.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbActivity.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbActivity.register(MessageItemTextViewCell.self, forCellReuseIdentifier: ActivityViewController.receiverTextCellIdentitifer)
        tbActivity.register(ActivityEmptyViewCell.self, forCellReuseIdentifier: ActivityViewController.emptyActivityCellIdentifier)
        backgroundView?.addSubview(tbActivity)
        
        //Footer input
        
        setupInputMessage(yInput : yContainerInputChatView)
        backgroundView?.addSubview(self.containerInputChatView)
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func bindToViewModel(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        let roomController = RoomController.sharedAPI
        self.roomViewModel = RoomViewModel(
            input: (
                projectId: projectId,
                userId:subjectUser,
                date: currentDate!,
                roomController: roomController
            )
        )
        
        
        let roomId =
            self.roomViewModel!.rooms!
                .flatMapLatest { rooms -> Observable<Room> in
                    var room = RoomService.createEmptyRoom()
                    if(rooms.count > 0){
                        room =  rooms[0]
                    }
                    return Observable.just(room)
        }
        
        let messageController = MessageController.sharedAPI
        self.messageViewModel = MessageViewModel(
            inputNeedAgendaRoomId: (
                agendaRoomId: roomId,
                messageController: messageController
            )
        )
        
        self.messageViewModel?.messages?.subscribe(onNext: {[weak self] messages in
            guard let strongSelf = self else { return }
            var heights : [CGFloat] = []
            for _ in messages{
                heights.append(ActivityViewController.DefaultHeightWebView)
            }
            strongSelf.contentHeights = heights
        }).disposed(by: disposeBag)
        
        let messagesAndSection = self.messageViewModel?.messages?
            .flatMapLatest{ messages -> Observable<[MessageSection]> in
                return Observable.just(
                    [
                        MessageSection(header: "", items: messages)
                    ])
        }
        
        //Datasource
        let dataSource = RxTableViewSectionedReloadDataSource<MessageSection>(
            configureCell: {[weak self] (_ ,table, indexPath, element) in
                if element.dataType == .empty {
                    guard let cell = table.dequeueReusableCell(withIdentifier:  ActivityViewController.emptyActivityCellIdentifier, for: indexPath) as? ActivityEmptyViewCell else {
                        return ActivityEmptyViewCell()
                    }
                    return cell
                }else{
                    guard let strongSelf = self, let cell = table.dequeueReusableCell(withIdentifier:  ActivityViewController.receiverTextCellIdentitifer, for: indexPath) as? MessageItemTextViewCell else {
                        return MessageItemTextViewCell()
                    }
                    
                    cell.selectionStyle = .none
                    cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                    //
                    if(element.valid() == true){
                        let htmlHeight = strongSelf.contentHeights[indexPath.row]
                        cell.wvContent?.tag = indexPath.row
                        cell.wvContent?.delegate = self
                        cell.wvContent?.frame.size.height = htmlHeight
                    }
                    else{
                        cell.wvContent?.delegate = self
                    }
                    //
                    cell.setValueForCell(message: element)
                    return cell
                }
                
            },
            titleForHeaderInSection: { dataSource, sectionIndex in
                return dataSource.sectionModels[sectionIndex].header
            }
        )
        
        self.dataSource = dataSource
        
        messagesAndSection?
            .bind(to: tbActivity.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        tbActivity.rx
            .modelSelected(Message.self)
            .subscribe(onNext:  {value in
                
            })
            .disposed(by: disposeBag)
        
        tbActivity.rx
            .setDelegate(self)
            .disposed(by : disposeBag)
        
        //Send message
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let messageAPIController = MessageAPIController(provider: projectProvider)
        let observableTap = self.sendTextButton.rx.tap.asObservable()
        let messageViewModel = MessageAPIViewModel(
            inputSendMessageRoomAgenda: (
                projectId: projectId,
                roomId: roomId,
                content: self.inputTextView.textInput.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                messageAPIController: messageAPIController
            )
        )
        // bind results to  {
        messageViewModel.processEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.stateForSendButton(valid)
            })
            .disposed(by: disposeBag)
        //
        messageViewModel.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.inputTextView.textInput?.startShimmering()
                }
                else{
                    self?.inputTextView.textInput?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        //
        messageViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                    self?.inputTextView.textInput?.stopShimmering()
                    self?.inputTextView.textInput?.text = ""
                    self?.inputTextView.resignFirstResponder()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            let message = NSLocalizedString("send_message_content_successfully", comment: "")
//                            self?.view.makeToast(message, duration: 3.0, position: .center)
                        }
                        else{
                            let message = NSLocalizedString("send_message_content_unsuccessfully", comment: "")
                            self?.view.makeToast(message, duration: 3.0, position: .center)
                        }
                        self?.inputTextView.textInput?.stopShimmering()
                        self?.inputTextView.textInput?.text = ""
                        self?.inputTextView.resignFirstResponder()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        if (contentHeights[webView.tag] != ActivityViewController.DefaultHeightWebView){ return }
        
        var height : CGFloat = 0
        guard let value = webView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight") else { return }
        if let number = NumberFormatter().number(from: value) {
            height = CGFloat(truncating: number)
        }
        
        if (contentHeights[webView.tag] == height){ return }
        
        contentHeights[webView.tag] = height
        tbActivity.reloadRows(at: [IndexPath(row: webView.tag, section: 0)], with: .fade)
    }
    
    func filterResultsToOriginal(){
        
    }
    
    func filterResultsWithSearchTerm(_ searchText : String){
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight : CGFloat = 0
        guard let sectionData = self.dataSource?[indexPath.section] else { return cellHeight }
        let element = sectionData.items[indexPath.row]
        if element.dataType == .empty {
            cellHeight = tableView.frame.size.height
        }
        else{
            var webviewHeight : CGFloat = 0
            if element.dataType == .holder {
                webviewHeight = ActivityViewController.DefaultHeightWebView
            }
            else{
                webviewHeight = contentHeights[indexPath.row]
            }
            cellHeight = webviewHeight + Constant.ChattingView.Padding_Chatting_Cell + AppDevice.SubViewFrame.TitleHeight + Constant.ChattingView.Padding_Chatting_Cell
        }
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //        let task = arrayTask[indexPath.row]
    }
    
    func growing(_ growingTextView: CustomTextView!, willChangeHeight height: Float) {
        //
        if(growingTextView == self.inputTextView){
            let newHeight : CGFloat = CGFloat(height)
            let diff : CGFloat = abs(growingTextView.frame.size.height - newHeight)
            if (self.inputTextView.frame.size.height < newHeight){
                self.containerInputChatView.frame.origin.y -= diff
                self.containerInputChatView.frame.size.height += diff
                //
                tbActivity.contentSize.height += diff
            }
            else{
                self.containerInputChatView.frame.origin.y += diff
                self.containerInputChatView.frame.size.height -= diff
                //
                tbActivity.contentSize.height -= diff
            }
            let yDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
            self.defaultInputView.frame.size.height = self.containerInputChatView.frame.size.height - (yDefaultInputView * 2)
            self.sendTextButton.center = CGPoint(x: self.sendTextButton.center.x, y: self.defaultInputView.frame.size.height / 2)
        }
    }
    
    
    func growingTextViewDidChange(_ growingTextView: CustomTextView!) {
        if(growingTextView == self.inputTextView){
            let messageString = self.inputTextView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            if(isLockSendButton == false){
                let lenght = messageString.count
                if(lenght < Constant.MinLength.Message){
                    stateForSendButton(false)
                }
                else{
                    stateForSendButton(true)
                }
                
            }
        }
    }
    
    func stateForSendButton(_ enable: Bool){
        if(enable == false){
            self.sendTextButton.isUserInteractionEnabled = false
            self.sendTextButton.isSelected = false
            self.sendTextButton.isEnabled = false
        }
        else{
            self.sendTextButton.isUserInteractionEnabled = true
            self.sendTextButton.isSelected = true
            self.sendTextButton.isEnabled = true
        }
    }
    
    func growing(_ growingTextView: CustomTextView!, shouldChangeTextIn range: NSRange, replacementText text: String!) -> Bool {
        if(growingTextView == self.inputTextView){
            return self.inputTextView.text.count + (text.count - range.length) <= Constant.MaxLength.Message;
        }
        return true
    }
    
    
    
    deinit {
        // Release all resources - perform the deinitialization
        removeNotification_Keyboard()
    }
    
}
