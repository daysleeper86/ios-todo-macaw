//
//  BacklogViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import FirebaseFirestore
import RxSwift
import RxDataSources
import Moya
import WebKit

class ActivityViewController: BaseViewController{
    //External props
    var currentDate : Date?
    //View
    var backgroundView : UIView?
    var keyboardBounds : CGRect?
    var isShowKeyboard : Bool = false
    var tbActivity : UITableView = UITableView(frame: .zero)
    //Data
    var oeMentions:OEMentions!
    static let receiverTextCellIdentitifer : String = "ReceiverTextCellIdentitifer"
    static let emptyActivityCellIdentifier : String = "emptyActivityCellIdentifier"
    static let DefaultHeightWebView : CGFloat = Constant.ChattingView.Buble_TextView_Avatar_MaxHeight
    //ViewModel
    var roomViewModel : RoomViewModel?
    var messageViewModel : MessageViewModel?
    var contentHeights : [CGFloat] = []
    var arrayMessages : [Message] = []
    var subjectContent = BehaviorSubject<String>(value: "")
    var subjectMention = BehaviorSubject<[String]>(value: [])
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    //Datasource
//    var dataSource: RxTableViewSectionedReloadDataSource<MessageSection>?
    
    //Chatting
    var isLockSendButton : Bool =  false
    // For Chat text, image
    
    lazy var webviewDescription : WKWebView = {
        let hInputTextView : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let xInputTextView : CGFloat = self.mentionButton.frame.origin.x + self.mentionButton.frame.size.width
        let wInputTextView : CGFloat =  self.defaultInputView.frame.size.width - xInputTextView - self.sendTextButton.frame.size.width
        let yInputTextView : CGFloat = 0
        
        let webCfg : WKWebViewConfiguration = WKWebViewConfiguration()
        let userController : WKUserContentController = WKUserContentController()
        userController.add(self, name: "popupHandler")
        userController.add(self, name: "attributeHandler")
        userController.add(self, name: "contentHeight")
        webCfg.userContentController = userController
        let webviewDescription = WKWebView(frame: CGRect(x: xInputTextView, y: yInputTextView, width: wInputTextView, height: hInputTextView), configuration: webCfg)
        webviewDescription.navigationDelegate = self
        //        webviewDescription?.configuration = webCfg
        webviewDescription.tag = DescriptionInfo.tag
        webviewDescription.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        webviewDescription.scrollView.contentInset = .zero
        //
        if let filePath = Bundle.main.url(forResource: "test_son13", withExtension: "htm") {
            let request = URLRequest(url: filePath)
            webviewDescription.load(request)
        }
        //        webviewDescription?.delegate = self
        return webviewDescription
    }()
    
    var menuHeaderPopover : MLKMenuPopover?
    //
    
    lazy var sendTextButton : UIButton = {
        let hSendTextButton : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let wSendTextButton : CGFloat = hSendTextButton
        let xSendTextButton : CGFloat = self.defaultInputView.frame.size.width - wSendTextButton
        let ySendTextButton : CGFloat = 0
        
        let sendTextButton = UIButton(frame: CGRect(x: xSendTextButton, y: ySendTextButton, width: wSendTextButton, height: hSendTextButton))
        sendTextButton.setImage(UIImage(named: LocalImage.ChattingImage.Send.NotSelected), for: .normal)
        sendTextButton.setImage(UIImage(named: LocalImage.ChattingImage.Send.Selected), for: .selected)
        sendTextButton.contentMode = .scaleAspectFill
        sendTextButton.isEnabled = false
        
        return sendTextButton
    }()
    
    lazy var mentionButton : UIButton = {
        let hMentionButton : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let wMentionButton : CGFloat = hMentionButton
        let xMentionButton : CGFloat = 0
        let yMentionButton : CGFloat = 0
        
        let mentionButton = UIButton(frame: CGRect(x: xMentionButton, y: yMentionButton, width: wMentionButton, height: hMentionButton))
        mentionButton.setImage(UIImage(named: LocalImage.ChattingImage.Mention.NotSelected), for: .normal)
        mentionButton.setImage(UIImage(named: LocalImage.ChattingImage.Mention.Selected), for: .selected)
        mentionButton.addTarget(self, action: #selector(mentionAction), for: .touchUpInside)
        mentionButton.contentMode = .scaleAspectFill
        //
        return mentionButton
    }()
    
    lazy var defaultInputView : UIView = {
        let xDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wDefaultInputView : CGFloat = self.containerInputChatView.frame.size.width - (xDefaultInputView * 2)
        let hDefaultInputView : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        
        let defaultInputView = UIView(frame: CGRect(x: xDefaultInputView, y: yDefaultInputView, width: wDefaultInputView, height: hDefaultInputView))
        defaultInputView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        defaultInputView.layer.borderColor = AppColor.NormalColors.GRAY_COLOR.cgColor
        defaultInputView.layer.borderWidth = 1.0
        defaultInputView.layer.cornerRadius = 4.0
        defaultInputView.isOpaque = false
        defaultInputView.isHidden = false
        
        
        return defaultInputView
    }()
    
    lazy var containerInputChatView : UIView = {
        let height = Constant.ChattingView.Height_Input_Container_Message
        let containerInputChatView = UIView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: height))
        containerInputChatView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //
        let lineView_Horizontal = UIView(frame: CGRect(x: 0, y: 0, width: containerInputChatView.frame.size.width, height: 1.0))
        lineView_Horizontal.backgroundColor = AppColor.MicsColors.LINE_COLOR
        lineView_Horizontal.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        lineView_Horizontal.layer.shadowOpacity = 1
        lineView_Horizontal.layer.shadowRadius = 4
        lineView_Horizontal.layer.shadowOffset = CGSize(width: 0, height: -1)
        containerInputChatView.addSubview(lineView_Horizontal)
        
        
        return containerInputChatView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
        bindToViewModel()
    }
    
    func addNotification(){
        addNotification_Keyboard()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        isShowKeyboard = true
        //
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.keyboardBounds = keyboardBounds
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int{
                
                let containerFrame = self.containerInputChatView.frame
                let yContainerInputChatView = backgroundView!.frame.size.height - (keyboardBounds.size.height + containerFrame.size.height)
                
                // Animations settings
                UIView.beginAnimations("Change frame over keyboard container", context: nil)
                UIView.setAnimationBeginsFromCurrentState(true)
                UIView.setAnimationDuration(duration)
                UIView.setAnimationCurve(UIView.AnimationCurve(rawValue: curve) ?? .easeInOut)
                
                // Set views with new info
                self.containerInputChatView.frame.origin.y = yContainerInputChatView
                self.view.addSubview(self.containerInputChatView)
                stateForMentionButton(true)
                // Commit animations
                UIView.commitAnimations()
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        isShowKeyboard = false
        //
        if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int{
            
            let containerFrame = self.containerInputChatView.frame
            let yContainerInputChatView = backgroundView!.frame.size.height - containerFrame.size.height
            
            // Animations settings
            UIView.beginAnimations("Change frame over keyboard container", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(duration)
            UIView.setAnimationCurve(UIView.AnimationCurve(rawValue: curve) ?? .easeInOut)
            
            // Set views with new info
            self.containerInputChatView.frame.origin.y = yContainerInputChatView
            self.view.addSubview(self.containerInputChatView)
            stateForMentionButton(false)
            // Commit animations
            UIView.commitAnimations()
        }
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupInputMessage(yInput : CGFloat){
        self.containerInputChatView.addSubview(self.defaultInputView)
        self.defaultInputView.addSubview(self.mentionButton)
        self.defaultInputView.addSubview(self.sendTextButton)
        self.defaultInputView.addSubview(self.webviewDescription)
        //
        self.containerInputChatView.frame.origin.y = yInput
        //
        stateForMentionButton(false)
    }
    
    func initView(){
        
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Background View
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.ScreenComponent.Height * AppDevice.ScreenComponent.OverlayHeight - AppDevice.ScreenComponent.OverlayHandler
        
        backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: wBackgroundView, height: hBackgroundView))
        backgroundView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.view.addSubview(backgroundView!)
        
        let yScreen : CGFloat = 0
        //Header View
        var yHeader = yScreen
        let hHeader : CGFloat = 44
        let pBottomHeader : CGFloat = 10
        
        let headerView : UIView = UIView(frame: CGRect(x: 0, y: yHeader, width: backgroundView!.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        backgroundView?.addSubview(headerView)
        
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = 0
        let wTitleScreen : CGFloat = headerView.frame.size.width - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = headerView.frame.size.height
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitleScreen.text = NSLocalizedString("discussion", comment: "").uppercased()
        headerView.addSubview(lbTitleScreen)
        
        yHeader += hHeader
        yHeader += pBottomHeader
        
        let hContainerInputChatView : CGFloat = Constant.ChattingView.Height_Input_Container_Message
        let yContainerInputChatView = backgroundView!.frame.size.height - hContainerInputChatView
        
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = AppDevice.ScreenComponent.Width
        let hTableBacklog : CGFloat = backgroundView!.frame.size.height - yTableBacklog - hContainerInputChatView
        tbActivity.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbActivity.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbActivity.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbActivity.dataSource = self
        tbActivity.delegate = self
        tbActivity.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbActivity.register(MessageItemTextViewCell.self, forCellReuseIdentifier: ActivityViewController.receiverTextCellIdentitifer)
        tbActivity.register(ActivityEmptyViewCell.self, forCellReuseIdentifier: ActivityViewController.emptyActivityCellIdentifier)
        backgroundView?.addSubview(tbActivity)
        
        //Footer input
        
        setupInputMessage(yInput : yContainerInputChatView)
//        backgroundView?.addSubview(self.containerInputChatView)
        self.view.addSubview(self.containerInputChatView)
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkIndexNotifcation() -> String{
        var notificationMessageID = ""
        let appDelegate = AppDelegate().sharedInstance()
        if(appDelegate.isGoToPush == true){
            if let remoteNotificationData = appDelegate.remoteNotificationData{
                if let messageId = remoteNotificationData[SettingString.MessageId] as? String{
                    notificationMessageID = messageId
                }
            }
        }
        return notificationMessageID
    }
    
    func bindToViewModel(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        let roomController = RoomController.sharedAPI
        self.roomViewModel = RoomViewModel(
            input: (
                projectId: projectId,
                userId:subjectUser,
                date: currentDate!,
                roomController: roomController
            )
        )
        
        let roomId =
            self.roomViewModel!.rooms!
                .flatMapLatest { rooms -> Observable<Room> in
                    var room = RoomService.createEmptyRoom()
                    if(rooms.count > 0){
                        room =  rooms[0]
                    }
                    return Observable.just(room)
        }
        
        let messageController = MessageController.sharedAPI
        self.messageViewModel = MessageViewModel(
            inputNeedAgendaRoomId: (
                agendaRoomId: roomId,
                messageController: messageController
            )
        )
        
        self.messageViewModel?.messages?.subscribe(onNext: {[weak self] messages in
            guard let strongSelf = self else { return }
            var heights : [CGFloat] = []
            //
            let notificationId = strongSelf.checkIndexNotifcation()
            var gotoNotificationIndex : Int = -1
            if(notificationId.count == 0){
                gotoNotificationIndex = -1
            }
            //
            for i in 0..<messages.count{
                let message = messages[i]
                heights.append(ActivityViewController.DefaultHeightWebView)
                //
                if(notificationId.count > 0){
                    if(message.messageId == notificationId){
                        gotoNotificationIndex = i
                    }
                }
            }
            //
            AppDelegate().sharedInstance().clearPushNotificationData()
            //
            strongSelf.contentHeights = heights
            strongSelf.arrayMessages = messages
            strongSelf.tbActivity.reloadData()
            //
            if(gotoNotificationIndex != -1 && gotoNotificationIndex < strongSelf.arrayMessages.count){
                let indexPath = IndexPath(row: gotoNotificationIndex, section: 0)
                strongSelf.tbActivity.scrollToRow(at: indexPath, at: .middle, animated: true)
            }
            //
        }).disposed(by: disposeBag)
        
//        let messagesAndSection = self.messageViewModel?.messages?
//            .flatMapLatest{ messages -> Observable<[MessageSection]> in
//                return Observable.just(
//                    [
//                        MessageSection(header: "", items: messages)
//                    ])
//        }
        
        //Datasource
//        let dataSource = RxTableViewSectionedReloadDataSource<MessageSection>(
//            configureCell: {[weak self] (_ ,table, indexPath, element) in
//                if element.dataType == .empty {
//                    guard let cell = table.dequeueReusableCell(withIdentifier:  ActivityViewController.emptyActivityCellIdentifier, for: indexPath) as? ActivityEmptyViewCell else {
//                        return ActivityEmptyViewCell()
//                    }
//                    cell.selectionStyle = .none
//                    return cell
//                }else{
//                    guard let strongSelf = self, let cell = table.dequeueReusableCell(withIdentifier:  ActivityViewController.receiverTextCellIdentitifer, for: indexPath) as? MessageItemTextViewCell else {
//                        return MessageItemTextViewCell()
//                    }
//
//                    cell.selectionStyle = .none
//                    cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
//                    //
//                    if(element.valid() == true){
//                        let htmlHeight = strongSelf.contentHeights[indexPath.row]
//                        cell.wvContent?.tag = indexPath.row
//                        cell.wvContent?.delegate = self
//                        cell.wvContent?.frame.size.height = htmlHeight
//                    }
//                    else{
//                        cell.wvContent?.delegate = self
//                    }
//                    //
//                    cell.setValueForCell(message: element)
//                    return cell
//                }
//
//            },
//            titleForHeaderInSection: { dataSource, sectionIndex in
//                return dataSource.sectionModels[sectionIndex].header
//            }
//        )
//
//        self.dataSource = dataSource
        
//        messagesAndSection?
//            .bind(to: tbActivity.rx.items(dataSource: dataSource))
//            .disposed(by: disposeBag)
        
//        tbActivity.rx
//            .modelSelected(Message.self)
//            .subscribe(onNext:  {value in
//
//            })
//            .disposed(by: disposeBag)
        
//        tbActivity.rx
//            .setDelegate(self)
//            .disposed(by : disposeBag)
        
        //Send message
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let messageAPIController = MessageAPIController(provider: projectProvider)
        let observableTap = self.sendTextButton.rx.tap.asObservable()
        let messageViewModel = MessageAPIViewModel(
            inputSendMessageRoomAgenda: (
                projectId: projectId,
                roomId: roomId,
                mentions: subjectMention.asObservable(),
                content: subjectContent.asObservable(),
                loginTaps: observableTap,
                messageAPIController: messageAPIController
            )
        )
        // bind results to  {
        messageViewModel.processEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.stateForSendButton(valid)
            })
            .disposed(by: disposeBag)
        //
        messageViewModel.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.webviewDescription.startShimmering()
                }
                else{
                    self?.webviewDescription.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        //
        messageViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    //
                    self?.containerInputChatView.stopShimmering()
                    self?.clearContent()
                    self?.dismissKeyboard()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            let message = NSLocalizedString("send_message_content_successfully", comment: "")
//                            self?.view.makeToast(message, duration: 3.0, position: .center)
                        }
                        else{
                            let message = NSLocalizedString("send_message_content_unsuccessfully", comment: "")
                            self?.makeToast(message:message, duration: 3.0, position: .top)
                        }
                        //
                        self?.containerInputChatView.stopShimmering()
                        self?.clearContent()
                        self?.dismissKeyboard()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    
    func filterResultsToOriginal(){
        
    }
    
    func filterResultsWithSearchTerm(_ searchText : String){
        
    }
    
    @objc func mentionAction(){
        //
        let scriptSource = "triggerSuggestion();"
        webviewDescription.evaluateJavaScript(scriptSource) {(result, error) in
            if result != nil {
            }
        }
    }
    
    func stateForSendButton(_ enable: Bool){
        if(enable == false){
            self.sendTextButton.isUserInteractionEnabled = false
            self.sendTextButton.isSelected = false
            self.sendTextButton.isEnabled = false
        }
        else{
            self.sendTextButton.isUserInteractionEnabled = true
            self.sendTextButton.isSelected = true
            self.sendTextButton.isEnabled = true
        }
    }
    
    func stateForMentionButton(_ enable: Bool){
        if(enable == false){
            self.mentionButton.isUserInteractionEnabled = false
            self.mentionButton.isSelected = false
            self.mentionButton.isEnabled = false
        }
        else{
            self.mentionButton.isUserInteractionEnabled = true
            self.mentionButton.isSelected = true
            self.mentionButton.isEnabled = true
        }
    }
    
    
    deinit {
        // Release all resources - perform the deinitialization
        removeNotification_Keyboard()
    }
    
}


extension ActivityViewController : WKScriptMessageHandler,WKNavigationDelegate {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if(message.name == "popupHandler") {
            if let dict = message.body as? NSDictionary {
                if let status = dict["status"] as? Int,let message = dict["message"] as? String{
                    if(status == 1){
                        showDropdownView(fromDirection: .top, withText: message)
                    }
                    else{
                        hideDropdownView()
                    }
                }
            }
        }
        else if(message.name == "attributeHandler") {
            if let dict = message.body as? NSDictionary {
//                updateToolBarWithButtonName(dictAttribute: dict)
            }
        }
        else if(message.name == "contentHeight") {
            if let dict = message.body as? NSDictionary {
                //                updateToolBarWithButtonName(dictAttribute: dict)
                let maxHeight : Float = 120
                let minHeight : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
                if let height = dict["key"] as? Float{
//                    height = height + (paddingHeight * 2)
                    let currentHeight : Float = Float(self.webviewDescription.frame.size.height)
                    if(height != currentHeight && height <= maxHeight){
                        var newHeight = CGFloat(height)
                        if(newHeight < minHeight){
                            newHeight = minHeight
                        }
                        let diff : CGFloat = abs(self.webviewDescription.frame.size.height - newHeight)
                        if (self.webviewDescription.frame.size.height < newHeight){
                            self.containerInputChatView.frame.origin.y -= diff
                            self.containerInputChatView.frame.size.height += diff
                            //
                            tbActivity.contentSize.height += diff
                        }
                        else{
                            self.containerInputChatView.frame.origin.y += diff
                            self.containerInputChatView.frame.size.height -= diff
                            //
                            tbActivity.contentSize.height -= diff
                        }
                        self.webviewDescription.frame.size.height = newHeight
                        let yDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
                        self.defaultInputView.frame.size.height = self.containerInputChatView.frame.size.height - (yDefaultInputView * 2)
                        self.sendTextButton.center = CGPoint(x: self.sendTextButton.center.x, y: self.defaultInputView.frame.size.height / 2)
                        self.mentionButton.center = CGPoint(x: self.mentionButton.center.x, y: self.defaultInputView.frame.size.height / 2)
                    }
                }
                //
                let scriptSourceMention = "getMentions();"
                webviewDescription.evaluateJavaScript(scriptSourceMention) {[weak self] (result, error) in
                    if result != nil {
                        if let arrMention = result as? [String]{
                            self?.subjectMention.onNext(arrMention)
                        }
                        else{
                            self?.subjectMention.onNext([])
                        }
                    }
                    else{
                        self?.subjectMention.onNext([])
                    }
                }
                let scriptSourceContent = "getHTMLContent1();"
                webviewDescription.evaluateJavaScript(scriptSourceContent) {[weak self] (result, error) in
                    if result != nil {
                        if let strContent = result as? String{
                            //
                            let textContent = strContent.withoutHtmlTags
                            let lenght = textContent.count
                            if(lenght < Constant.MinLength.Message){
                                self?.stateForSendButton(false)
                                //
                                self?.subjectContent.onNext("")
                            }
                            else{
                                self?.stateForSendButton(true)
                                //
                                self?.subjectContent.onNext(strContent)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        guard navigationAction.navigationType == .other || navigationAction.navigationType == .reload  else {
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
    
    //Finished loading WKWebView
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if(webView.tag == DescriptionInfo.tag){
            setHTML(html : "")
            //
            let scriptSource = "getContentScroll();"
            webviewDescription.evaluateJavaScript(scriptSource) {(result, error) in
                if result != nil {
                    
                }
            }
        }
        else{
            let tag = webView.tag
            if(tag < contentHeights.count){
                if (contentHeights[tag] != ActivityViewController.DefaultHeightWebView){ return }

                let scriptSource = "document.body.scrollHeight"
                webView.evaluateJavaScript(scriptSource) {[weak self] (result, error) in
                    if result != nil {
                        if let strContent = result as? Float{
                            let height : CGFloat = CGFloat(strContent)
                            //
                            guard let strongSelf = self else { return }
                            //
                            if(tag < strongSelf.contentHeights.count){
                                if (strongSelf.contentHeights[tag] == height){ return }
                                
                                strongSelf.contentHeights[tag] = height
                                strongSelf.tbActivity.reloadRows(at: [IndexPath(row: tag, section: 0)], with: .fade)
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    @objc func setHTML(html : String){
        let escapedForJSON = html.replacingOccurrences(of: "\"", with: "\\\"")
        let scriptSource = "setHTMLContent1(\"" + escapedForJSON + "\");"
        webviewDescription.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
            }
        }
    }
    
    func mentionUser(user : User){
        let scriptSource = "selectUserMention(\"" + user.userId + "\",\"" + user.getUserName() + "\");"
        webviewDescription.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
            }
        }
    }
    
    func dismissKeyboard(){
        let scriptSource = "document.activeElement.blur();"
        webviewDescription.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                
            }
        }
    }
    
    
    func clearContent(){
        let scriptSource = "clearContent1();"
        webviewDescription.evaluateJavaScript(scriptSource) {[weak self] (result, error) in
            if result != nil {
                
            }
        }
        self.subjectContent.onNext("")
    }
//    func getContentScroll(){
//        let scriptSource = "getContentScroll();"
//        webviewDescription.evaluateJavaScript(scriptSource) { (result, error) in
//            if result != nil {
//                print(result)
//            }
//        }
//    }
}

extension ActivityViewController : MLKMenuPopoverPopoverDelegate{
    func showDropdownView(fromDirection direction: LMDropdownViewDirection, withText text: String) {
        //
        if let userTeam = AppSettings.sharedSingleton.userTeam{
            //
            var filterUserTeam : [User] = []
            for user in userTeam{
                if(text.count == 0){
                    filterUserTeam.append(user)
                }
                else{
                    if(user.name.range(of: text, options:.caseInsensitive) != nil){
                        filterUserTeam.append(user)
                    }
                }
            }
            //
            if(filterUserTeam.count == 0){
                filterUserTeam.append(UserService.createEmptyUser())
            }
            //
            let paddingTopBottom : CGFloat = 8
            let maxRow : CGFloat = 3
            let heightRow : CGFloat = AppDevice.TableViewRowSize.MemberHeight
            var hMenu : CGFloat = heightRow * (filterUserTeam.count > Int(maxRow) ? maxRow : CGFloat(filterUserTeam.count)) + (paddingTopBottom*2)
//            let yMenu : CGFloat = self.containerInputChatView.frame.origin.y - self.containerInputChatView.frame.size.height - hMenu
            var yMenu : CGFloat = AppDevice.ScreenComponent.Height - (self.keyboardBounds?.size.height ?? 0) - self.containerInputChatView.frame.size.height - hMenu - paddingTopBottom
            if(yMenu == 0){
                yMenu = 0
                hMenu = AppDevice.ScreenComponent.Height - (self.keyboardBounds?.size.height ?? 0) - self.containerInputChatView.frame.size.height - paddingTopBottom
            }
            let xMenu : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let wMenu : CGFloat = AppDevice.ScreenComponent.Width - (xMenu * 2)
            
            
            let rect = CGRect(x: xMenu, y: yMenu, width: wMenu, height: hMenu)
            if(menuHeaderPopover == nil){
                menuHeaderPopover = MLKMenuPopover(frame: rect)
                //                menuHeaderPopover?.backgroundColor = .red
                menuHeaderPopover?.delegate = self
                menuHeaderPopover?.initView(menuItems:filterUserTeam,search: text)
            }
            else{
                //
                var needChangeFrame : Bool = false
                if(menuHeaderPopover?.menuItems?.count != filterUserTeam.count){
                    needChangeFrame = true
                }
                //
                if(needChangeFrame == true){
                    menuHeaderPopover?.changeFrame(rect: rect)
                }
                //
                menuHeaderPopover?.menuItems = filterUserTeam
                menuHeaderPopover?.search = text
                menuHeaderPopover?.menuItemsTableView?.reloadData()
                //
            }
            //
            if(menuHeaderPopover!.isShowing == false){
                if let strongWindow = self.view.window{
                    menuHeaderPopover!.showInView(view: strongWindow)
                    //
                    self.containerInputChatView.frame.origin.y = AppDevice.ScreenComponent.Height - (self.keyboardBounds?.size.height ?? 0) - self.containerInputChatView.frame.size.height
                    strongWindow.addSubview(self.containerInputChatView)
                    strongWindow.bringSubviewToFront(self.containerInputChatView)
                }
            }
            else{
                //                menuHeaderPopover!.hide()
            }
        }
    }
    
    func hideDropdownView(){
        menuHeaderPopover?.hide()
    }
    
    func mlkMenuPopoverPopoverSelected(_ MLKMenuPopover: MLKMenuPopover, didSelectMenuItemAtIndex: Int,item : User) {
        if(item.dataType == .normal){
            self.mentionUser(user: item)
        }
    }
    
    func mlkMenuPopoverPopoverAutoHide(_ MLKMenuPopover: MLKMenuPopover){
        //
        if(self.isShowKeyboard == true){
            let containerFrame = self.containerInputChatView.frame
            let yContainerInputChatView = backgroundView!.frame.size.height - ((self.keyboardBounds?.size.height ?? 0) + containerFrame.size.height)
            self.containerInputChatView.frame.origin.y = yContainerInputChatView
            self.view.addSubview(self.containerInputChatView)
        }
        else{
            let containerFrame = self.containerInputChatView.frame
            let yContainerInputChatView = backgroundView!.frame.size.height - containerFrame.size.height
            // Set views with new info
            self.containerInputChatView.frame.origin.y = yContainerInputChatView
            self.view.addSubview(self.containerInputChatView)
        }
        
        //
        menuHeaderPopover = nil
    }
}

extension ActivityViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let element = arrayMessages[indexPath.row]
        //
        if element.dataType == .empty {
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  ActivityViewController.emptyActivityCellIdentifier, for: indexPath) as? ActivityEmptyViewCell else {
                return ActivityEmptyViewCell()
            }
            cell.selectionStyle = .none
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  ActivityViewController.receiverTextCellIdentitifer, for: indexPath) as? MessageItemTextViewCell else {
                return MessageItemTextViewCell()
            }
            
            cell.selectionStyle = .none
            cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            //
            if(element.valid() == true){
                let htmlHeight = contentHeights[indexPath.row]
                cell.wvContent?.tag = indexPath.row
                cell.wvContent?.navigationDelegate = self
                cell.wvContent?.frame.size.height = htmlHeight
            }
            else{
                cell.wvContent?.navigationDelegate = nil
            }
            //
            cell.setValueForCell(message: element)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight : CGFloat = 0
        let element = arrayMessages[indexPath.row]
        if element.dataType == .empty {
            cellHeight = tableView.frame.size.height
        }
        else{
            var webviewHeight : CGFloat = 0
            if element.dataType == .holder {
                webviewHeight = ActivityViewController.DefaultHeightWebView
            }
            else{
                webviewHeight = contentHeights[indexPath.row]
            }
            cellHeight = webviewHeight + Constant.ChattingView.Padding_Chatting_Cell + AppDevice.SubViewFrame.TitleHeight + Constant.ChattingView.Padding_Chatting_Cell
        }
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //        let task = arrayTask[indexPath.row]
    }
}

