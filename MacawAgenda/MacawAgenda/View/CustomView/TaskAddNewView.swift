//
//  TaskAddNewView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/2/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TaskAddNewView: UIView {
    
    //Views
    private var imvAvatar : UIImageView?
    private var lbTitleName : UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        let wAvatar : CGFloat = 14
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = pAvatar + 5
        let yAvatar : CGFloat = (self.frame.size.height-hAvatar)/2
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.image = UIImage(named: "ic_add_blue")
        self.addSubview(imvAvatar!)
        
        // Title name
        let pTitleName : CGFloat = 8 + 10
        let hTitleName : CGFloat = self.frame.size.height
        let xTitleName : CGFloat = xAvatar + wAvatar + pTitleName
        let yTitleName : CGFloat = 0
        let wTitleName : CGFloat = self.frame.size.width - xTitleName - pTitleName
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.NormalColors.BLUE_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        self.addSubview(lbTitleName!)
    }
    
    func setValueForTitle(title: String){
        lbTitleName?.text = title
    }

}
