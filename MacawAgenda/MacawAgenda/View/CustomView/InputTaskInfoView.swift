//
//  InputTaskInfoView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/2/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Moya
import RxSwift

protocol InputTaskInfoViewDelegate : NSObject {
    func inputTaskInfoViewAdd(_ inputTaskInfoView: InputTaskInfoView, addNew withName: String)
    func inputTaskInfoViewUpdate(_ inputTaskInfoView: InputTaskInfoView, update withName: String, taskId: String)
    func inputTaskInfoViewCancel(_ inputTaskInfoView: InputTaskInfoView, tapCancel isSave: Bool)
}


class InputTaskInfoView: UIView,UITextViewDelegate {
    
    //Views
    private var blackView : UIView?
    private var contentView : UIView?
    private var tvCheckListName : LPlaceholderTextView?
    private var btnSave : UIButton?
    private var btnCancel : UIButton?
    //Data
    private var nDuration : NSNumber?
    private var isAutoDismissCancel: Bool = false
    private var isBindAddTask : Bool = false
    private var isBindAddAgenda : String = ""
    private var isBindUpdateTask : String = ""
    //Viewmodel
    private var viewModelName : TaskAPIViewModel?
    private var viewModelCreate : TaskAPIViewModel?
    //Events
    weak var delegate: InputTaskInfoViewDelegate?
    let disposeBag = DisposeBag()
    
    //Subject New task Id
    private var subjectAgendaID : PublishSubject<String> = PublishSubject()
    private var subjectTaskNewID : BehaviorSubject<String> = BehaviorSubject(value: "")
    private var subjectTaskNewIDEventStared : PublishSubject<Void> = PublishSubject()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = 0
        
        blackView = UIView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        blackView?.accessibilityIdentifier = AccessiblityId.VIEW_OVERPLAY
        blackView?.backgroundColor = AppColor.MicsColors.DARK_MENU_COLOR
        blackView?.alpha = 0.5
        self.addSubview(blackView!)
        
        //Tap Background Event
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackground))
        blackView?.addGestureRecognizer(tapGesture)
        //
        let xContentView : CGFloat = 0
        let hContentView : CGFloat = 120
        let wContentView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentView : CGFloat = AppDevice.ScreenComponent.Height
        
        contentView = UIView(frame: CGRect(x: xContentView, y: yContentView, width: wContentView, height: hContentView))
        contentView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.addSubview(contentView!)
        
        //
        let padding : CGFloat = 16
        let hButton : CGFloat = 20
        let wButtonSave : CGFloat = 45
        let wButtonCancel : CGFloat = 65
        
        let xButtonSave : CGFloat = contentView!.frame.size.width-wButtonSave-padding
        let yButtonSave : CGFloat = contentView!.frame.size.height-hButton-padding
        
        btnSave = UIButton.init(type: .custom)
        btnSave?.accessibilityIdentifier = AccessiblityId.BTN_SAVE
        btnSave?.frame = CGRect(x: xButtonSave, y: yButtonSave, width: wButtonSave, height: hButton)
        btnSave?.setTitle(NSLocalizedString("save", comment: "").uppercased(), for: .normal)
        btnSave?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnSave?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnSave?.contentHorizontalAlignment = .center
        contentView?.addSubview(btnSave!)
        //
        stateForSaveButton(false)
        
        let xButtonCancel  : CGFloat = btnSave!.frame.origin.x-wButtonCancel-padding
        let yButtonCancel  : CGFloat = yButtonSave
        
        btnCancel = UIButton.init(type: .custom)
        btnCancel?.accessibilityIdentifier = AccessiblityId.BTN_CANCEL
        btnCancel?.frame = CGRect(x: xButtonCancel, y: yButtonCancel, width: wButtonCancel, height: hButton)
        btnCancel?.setTitle(NSLocalizedString("cancel", comment: "").uppercased(), for: .normal)
        btnCancel?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnCancel?.setTitleColor(AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, for: .normal)
        btnCancel?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnCancel?.contentHorizontalAlignment = .center
        btnCancel?.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        contentView?.addSubview(btnCancel!)
        
        //
        let xTextView : CGFloat = padding
        let yTextView : CGFloat = padding
        let wTextView : CGFloat = contentView!.frame.size.width - ( xTextView * 2 )
        let hTextView : CGFloat = btnSave!.frame.origin.y - ( yTextView * 2)
        
        tvCheckListName = LPlaceholderTextView(frame: CGRect(x: xTextView, y: yTextView, width: wTextView, height: hTextView))
        tvCheckListName?.accessibilityIdentifier = AccessiblityId.TF_TASKNAME
        tvCheckListName?.placeholderColor = AppColor.NormalColors.GRAY_COLOR
        tvCheckListName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        tvCheckListName?.textColor = AppColor.NormalColors.BLACK_COLOR
        tvCheckListName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tvCheckListName?.isOpaque = false
        tvCheckListName?.delegate = self
        tvCheckListName?.returnKeyType = .done
        tvCheckListName?.placeholderText = NSLocalizedString("new_task", comment:"")
        contentView?.addSubview(tvCheckListName!)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(textView == tvCheckListName){
            if (text == "\n") {
                //
                if(btnSave?.isEnabled == true){
                    saveAction()
                }
                //
                return false
            }
            return textView.text.count + (text.count - range.length) <= Constant.MaxLength.Task
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        var enableSave : Bool = false
        if(Util.validateTaskName(textView.text).isValid){
            enableSave = true
        }
        stateForSaveButton(enableSave)
    }
    
    func showPicker(keyboardBounds : CGRect, duration: NSNumber, isDismiss:Bool, bindAddTask: Bool, bindAddAgenda: String,bindUpdateTask: String){
        isBindAddTask = bindAddTask
        isBindAddAgenda = bindAddAgenda
        isBindUpdateTask = bindUpdateTask
        //
        btnSave?.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        //
        isAutoDismissCancel = isDismiss
        //
        nDuration = duration
        let fDuration :  Double = nDuration?.doubleValue ?? 0
        //
        blackView?.alpha = 0.5
        blackView?.isHidden = false
        //
        var containerFrame  = contentView!.frame;
        let yFrame = self.frame.size.height - (keyboardBounds.size.height + containerFrame.size.height);
        if(yFrame != containerFrame.origin.y){
            containerFrame.origin.y = yFrame
            
            // Animations settings
            UIView.animate(withDuration: TimeInterval(fDuration), delay: 0.0, options: [.curveEaseOut], animations: {
                self.contentView?.frame = containerFrame
                self.contentView?.isHidden = false
            }, completion: nil)
            
        }
    }
    @objc func didTapBackground(){
        if(isAutoDismissCancel == true){
            hideInput()
        }
    }
    
    @objc func hideInput(){
        if (tvCheckListName != nil && tvCheckListName?.isFirstResponder ?? false) {
            tvCheckListName?.resignFirstResponder()
        }
        if(contentView != nil && contentView!.isHidden == false){
            hidePicker()
        }
    }
    
    @objc func saveAction(){
        if(isAutoDismissCancel == true){
            hideInput()
        }
        //
        if(isBindUpdateTask.count > 0){
            let taskId = isBindUpdateTask
            self.delegate?.inputTaskInfoViewUpdate(self, update: tvCheckListName!.text, taskId: taskId)
        }
        else{
            self.delegate?.inputTaskInfoViewAdd(self, addNew: tvCheckListName!.text)
        }
    }
    
    @objc func closeAction(){
        self.hideInput()
        //
        self.delegate?.inputTaskInfoViewCancel(self, tapCancel: false)
    }
    
    func hidePicker(){
        var containerFrame = contentView!.frame
        let yFrame  = self.frame.size.height
        if(yFrame != containerFrame.origin.y){
            containerFrame.origin.y = yFrame;
            //
            let fDuration : Double = nDuration?.doubleValue ?? 0.25
            //
            blackView?.alpha = 0.5
            blackView?.isHidden = false
            // Animations settings
            UIView.animate(withDuration: TimeInterval(fDuration), delay: 0.0, options: [.curveEaseOut], animations: {
                self.contentView?.frame = containerFrame;
                self.blackView?.alpha = 0
            }, completion: { finished in
                self.contentView?.isHidden = true
                self.blackView?.isHidden = true
                self.removeFromSuperview()
            })
            
        }
    }
    func becomeResponder(){
        tvCheckListName?.becomeFirstResponder()
    }
    func resignResponder(){
        tvCheckListName?.resignFirstResponder()
    }
    func isFirstResponder() -> Bool{
        return tvCheckListName?.isFirstResponder ?? false
    }
    func setContent(content : String){
        tvCheckListName?.text = content
    }
    func setDimBG(dim : Bool){
        if(dim == true){
            blackView?.backgroundColor = AppColor.MicsColors.DARK_MENU_COLOR
            blackView?.alpha = 0.5
        }
        else{
            blackView?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            blackView?.alpha = 1.0
        }
    }
    
    func setTitleForSubmitButton(title : String){
        let padding : CGFloat = 16
        let hButton : CGFloat = 20
        let wButtonSave : CGFloat = 45
        let wButtonCancel : CGFloat = 65
        let xButtonSave : CGFloat = contentView!.frame.size.width-wButtonSave-padding
        let yButtonSave : CGFloat = contentView!.frame.size.height-hButton-padding
        
        btnSave?.frame = CGRect(x: xButtonSave, y: yButtonSave, width: wButtonSave, height: hButton)
        btnSave?.setTitle(title, for: .normal)
        //
        let xButtonCancel  : CGFloat = btnSave!.frame.origin.x-wButtonCancel-padding
        let yButtonCancel  : CGFloat = yButtonSave
        btnCancel?.frame = CGRect(x: xButtonCancel, y: yButtonCancel, width: wButtonCancel, height: hButton)
    }
    func addPaddingLine(){
        let hLine : CGFloat = 1.0
        let lineView = UIView(frame: CGRect(x: 0, y: 0, width: contentView!.frame.size.width, height: hLine))
        lineView.backgroundColor = AppColor.MicsColors.LINE_COLOR
        contentView?.addSubview(lineView)
    }
    func setPlaceHolder(placeHolder : String){
        tvCheckListName?.placeholderText = placeHolder
    }
    
    func stateForSaveButton(_ enable: Bool){
        self.btnSave?.isEnabled = enable
        if(enable == true){
            self.btnSave?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
        }
        else{
            self.btnSave?.setTitleColor(AppColor.NormalColors.LIGHT_GRAY_COLOR, for:.normal)
        }
        tvCheckListName?.enablesReturnKeyAutomatically = enable
    }
    
    func stateForCancelButton(_ enable: Bool){
        self.btnCancel?.isEnabled = enable
    }
    
    func trackOpenCreateTask(taskName: String, fromBacklog: Bool){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        if(fromBacklog == true){
            Tracker.trackTaskCreatedInBacklog(projectId: projectId, userId: userId, taskName: taskName)
        }
        else{
            Tracker.trackTaskCreatedInPlan(projectId: projectId, userId: userId, taskName: taskName)
        }
    }
    
    //
    func bindToViewModel(){
        //
        if(isBindUpdateTask.count > 0){
            let taskId = isBindUpdateTask
            if(viewModelName == nil){
                // Update task name
                let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
                let token = AppSettings.sharedSingleton.account?.token ?? ""
                let authPlugin = AccessTokenPlugin { token }
                let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
                let taskController = TaskAPIController(provider: accountProvider)
                //
                viewModelName = TaskAPIViewModel(
                    inputUpdateTaskName: (
                        taskId: taskId,
                        projectId: projectId,
                        name: tvCheckListName!.rx.text.orEmpty.asObservable(),
                        loginTaps: btnSave!.rx.tap.asObservable(),
                        controller: taskController
                    )
                )
                
                viewModelName?.processEnabled?.subscribe(onNext: { [weak self] valid  in
                    self?.stateForSaveButton(valid)
                })
                    .disposed(by: disposeBag)
                
                viewModelName?.processingIn?
                    .subscribe(onNext: {[weak self] loading  in
                        guard let strongSelf = self else { return }
                        if(loading == true){
                            strongSelf.tvCheckListName?.startShimmering()
                        }
                        else{
                            strongSelf.tvCheckListName?.stopShimmering()
                        }
                    })
                    .disposed(by: disposeBag)
                
                viewModelName?.processFinished?.subscribe(onNext: { [weak self] loginResult in
                    switch loginResult {
                    case .failed(let output):
                        let appDelegate = AppDelegate().sharedInstance()
                        if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                            let message = output.message
                            var toastStyle = ToastStyle()
                            toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                            toastStyle.cornerRadius = 4.0
                            toastStyle.messageAlignment = .center
                            toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                            self?.makeToast(message, duration: 3.0, position: .top, style: toastStyle)
                        }
                    case .ok(let output):
                        if let reponseAPI = output as? ResponseAPI{
                            if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                                //
                                if(reponseAPI.code == 200){
                                    let message = NSLocalizedString("update_task_successfully", comment: "")
                                    var toastStyle = ToastStyle()
                                    toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                                    toastStyle.cornerRadius = 4.0
                                    toastStyle.messageAlignment = .center
                                    toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                                    self?.makeToast(message, duration: 0.5, position: .top, style: toastStyle,completion: { didTap in
                                        self?.hideInput()
                                    })
                                }
                                else{
                                    let message = NSLocalizedString("update_task_unsuccessfully", comment: "")
                                    var toastStyle = ToastStyle()
                                    toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                                    toastStyle.cornerRadius = 4.0
                                    toastStyle.messageAlignment = .center
                                    toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                                    
                                    self?.makeToast(message, duration: 0.5, position: .top, style: toastStyle, completion: { didTap in
                                        self?.hideInput()
                                    })
                                }
                                //
                            }
                        }
                    }
                }).disposed(by: disposeBag)
            }
        }
        else{
            if(viewModelCreate == nil){
                let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
                let token = AppSettings.sharedSingleton.account?.token ?? ""
                let authPlugin = AccessTokenPlugin { token }
                let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
                let taskController = TaskAPIController(provider: accountProvider)
                //
                viewModelCreate = TaskAPIViewModel(
                    inputCreateTask: (
                        projectId: projectId,
                        name: tvCheckListName!.rx.text.orEmpty.asObservable(),
                        loginTaps: btnSave!.rx.tap.debounce(0.2, scheduler: MainScheduler.instance).asObservable(),
                        controller: taskController
                    )
                )
                
                let processEnabledData = viewModelCreate!.processEnabled!.share()
                let enableButton = Observable.combineLatest(
                    processEnabledData,
                    subjectTaskNewID.asObservable())
                { processEnable, subjectTaskNewID in
                    processEnable &&
                        subjectTaskNewID.count == 0
                    }
                    .distinctUntilChanged()
                
                enableButton.subscribe(onNext: { [weak self] valid  in
                    self?.stateForSaveButton(valid)
                })
                    .disposed(by: disposeBag)
                
                viewModelCreate?.processingIn?
                    .subscribe(onNext: {[weak self] loading  in
                        guard let strongSelf = self else { return }
                        if(loading == true){
                            strongSelf.tvCheckListName?.startShimmering()
                        }
                        else{
                            strongSelf.tvCheckListName?.stopShimmering()
                        }
                        
                    })
                    .disposed(by: disposeBag)
                
                viewModelCreate?.processFinished?.subscribe(onNext: { [weak self] loginResult in
                    switch loginResult {
                    case .failed(let output):
                        let appDelegate = AppDelegate().sharedInstance()
                        if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                            let message = output.message
                            var toastStyle = ToastStyle()
                            toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                            toastStyle.cornerRadius = 4.0
                            toastStyle.messageAlignment = .center
                            toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                            self?.makeToast(message, duration: 3.0, position: .top, style: toastStyle)
                        }
                    case .ok(let output):
                        if let reponseAPI = output as? ResponseAPI{
                            if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                                if(reponseAPI.code == 200){
                                    guard let strongSelf = self else { return }
                                    //
                                    let name = self?.tvCheckListName?.text ?? ""
                                    //
                                    if(strongSelf.isBindAddAgenda.count > 0){
                                        if reponseAPI.data.count > 0{
                                            let taskID = reponseAPI.data
                                            self?.subjectAgendaID.onNext(strongSelf.isBindAddAgenda)
                                            self?.subjectTaskNewID.onNext(taskID)
                                            self?.subjectTaskNewIDEventStared.onNext(())
                                            //
                                            self?.trackOpenCreateTask(taskName: name, fromBacklog: false)
                                        }
                                    }
                                    else{
                                        if reponseAPI.data.count > 0{
                                            let taskID = reponseAPI.data
                                            self?.subjectTaskNewID.onNext(taskID)
                                            //
                                            self?.setContent(content: "")
                                        }
                                        let message = NSLocalizedString("add_task_successfully", comment: "")
                                        var toastStyle = ToastStyle()
                                        toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                                        toastStyle.cornerRadius = 4.0
                                        toastStyle.messageAlignment = .center
                                        toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                                        self?.makeToast(message, duration: 0.5, position: .center, style: toastStyle,completion: { didTap in
                                            //
                                            self?.trackOpenCreateTask(taskName: name, fromBacklog: true)
                                            //
                                            self?.hideInput()
                                            //
                                            let taskID = ""
                                            self?.subjectTaskNewID.onNext(taskID)
                                        })
                                    }
                                }
                                else{
                                    var message = NSLocalizedString("add_task_unsuccessfully", comment: "")
                                    if(reponseAPI.code != 0){
                                        if(reponseAPI.message.count > 0){
                                            message = reponseAPI.message
                                        }
                                    }
                                    var toastStyle = ToastStyle()
                                    toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                                    toastStyle.cornerRadius = 4.0
                                    toastStyle.messageAlignment = .center
                                    toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                                    self?.makeToast(message, duration: 0.5, position: .center, style: toastStyle, completion: { didTap in
                                        self?.hideInput()
                                    })
                                }
                                //
                            }
                        }
                    }
                }).disposed(by: disposeBag)
                
                //
                if(isBindAddAgenda.count > 0){
                    let agendaController = AgendaAPIController(provider: accountProvider)
                    //
                    let agendaViewModel = AgendaAPIViewModel(
                        inputUpdateAgendaAddOrRemoveTask: (
                            projectId: projectId,
                            addOrRemove: 1,
                            agendaId: subjectAgendaID.asObservable(),
                            taskId: subjectTaskNewID.asObservable(),
                            loginTaps: subjectTaskNewIDEventStared.asObservable(),
                            agendaAPIController: agendaController
                        )
                    )
                    //
                    agendaViewModel.processEnabled?.subscribe(onNext: { [weak self] valid  in
                        self?.stateForCancelButton(valid)
                    })
                        .disposed(by: disposeBag)
                    
                    agendaViewModel.signingIn?
                        .subscribe(onNext: {[weak self] loading  in
                            guard let strongSelf = self else { return }
                            if(loading == true){
                                strongSelf.tvCheckListName?.startShimmering()
                            }
                            else{
                                strongSelf.tvCheckListName?.stopShimmering()
                            }
                        })
                        .disposed(by: disposeBag)
                    
                    agendaViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
                        switch loginResult {
                        case .failed(let output):
                            let appDelegate = AppDelegate().sharedInstance()
                            if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                                let message = output.message
                                var toastStyle = ToastStyle()
                                toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                                toastStyle.cornerRadius = 4.0
                                toastStyle.messageAlignment = .center
                                toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                                self?.makeToast(message, duration: 3.0, position: .center, style: toastStyle, completion: { didTap in
                                    self?.hideInput()
                                    //
                                    self?.subjectTaskNewID.onNext("")
                                })
                            }
                        case .ok(let output):
                            if let reponseAPI = output as? ResponseAPI{
                                if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                                    if(reponseAPI.code == 200){
                                        DispatchQueue.main.async {
                                            self?.hideInput()
                                        }
                                    }
                                    else{
                                        DispatchQueue.main.async {
                                            let message = NSLocalizedString("add_task_to_agenda_unsuccessfully", comment: "")
                                            var toastStyle = ToastStyle()
                                            toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
                                            toastStyle.cornerRadius = 4.0
                                            toastStyle.messageAlignment = .center
                                            toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
                                            self?.makeToast(message, duration: 1.5, position: .center, style: toastStyle, completion: { didTap in
                                                self?.hideInput()
                                            })
                                        }
                                    }
                                    //
                                }
                                self?.subjectTaskNewID.onNext("")
                            }
                        }
                    }).disposed(by: disposeBag)
                }
            }
        }
    }
    
}
