//
//  DatePickerView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/29/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

protocol InputDatePickerViewDelegate : NSObject {
    func inputDatePickerViewWithRemoveAction(_ inputDatePickerView: InputDatePickerView)
    func inputDatePickerViewWithDoneAction(_ inputDatePickerView: InputDatePickerView, date: Date)
}

class InputDatePickerView: UIView {
    //Views
    private var blackView : UIView?
    private var contentView : UIView?
    private var datePicker : UIDatePicker?
    private var btnRemove : UIButton?
    private var btnDone : UIButton?
    //Events
    weak var delegate: InputDatePickerViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = 0
        
        blackView = UIView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        blackView?.backgroundColor = AppColor.MicsColors.DARK_MENU_COLOR
        blackView?.alpha = 0.5
        self.addSubview(blackView!)
        
        //Tap Background Event
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackground))
        blackView?.addGestureRecognizer(tapGesture)
        
        //
        let xContentView : CGFloat = 0
        let hContentView : CGFloat = 271
        let wContentView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentView : CGFloat = AppDevice.ScreenComponent.Height
        
        contentView = UIView(frame: CGRect(x: xContentView, y: yContentView, width: wContentView, height: hContentView))
        contentView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.addSubview(contentView!)
        
        let xToolbarView : CGFloat = 0
        let yToolbarView : CGFloat = 0
        let wToolbarView : CGFloat = wContentView
        let hToolbarView : CGFloat = AppDevice.ScreenComponent.ToolbarHeight
        let toolbarView = UIView(frame: CGRect(x: xToolbarView, y: yToolbarView, width: wToolbarView, height: hToolbarView))
        toolbarView.backgroundColor = AppColor.NormalColors.BG_VIEW_COLOR
        contentView?.addSubview(toolbarView)
        
        //
        let wButton : CGFloat = 80
        let hButton : CGFloat = toolbarView.frame.size.height
        
        let xButtonRemove : CGFloat = 0
        let yButtonRemove : CGFloat = 0
        btnRemove = UIButton.init(type: .custom)
        btnRemove?.frame = CGRect(x: xButtonRemove, y: yButtonRemove, width: wButton, height: hButton)
        btnRemove?.setTitle(NSLocalizedString("cancel", comment: "").uppercased(), for: .normal)
        btnRemove?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnRemove?.setTitleColor(AppColor.NormalColors.LIGHT_RED_COLOR, for: .normal)
        btnRemove?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnRemove?.contentHorizontalAlignment = .center
        btnRemove?.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        toolbarView.addSubview(btnRemove!)
        
        let xButtonDone : CGFloat = toolbarView.frame.size.width-wButton
        let yButtonDone : CGFloat = 0
        btnDone = UIButton.init(type: .custom)
        btnDone?.frame = CGRect(x: xButtonDone, y: yButtonDone, width: wButton, height: hButton)
        btnDone?.setTitle(NSLocalizedString("save", comment: "").uppercased(), for: .normal)
        btnDone?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnDone?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnDone?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnDone?.contentHorizontalAlignment = .center
        btnDone?.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        toolbarView.addSubview(btnDone!)
        
        let hDatePicker : CGFloat = 217
        let wDatePicker : CGFloat = wContentView
        let xDatePicker : CGFloat = 0
        let yDatePicker : CGFloat = contentView!.frame.size.height-hDatePicker
        datePicker = UIDatePicker(frame: CGRect(x: xDatePicker, y: yDatePicker, width: wDatePicker, height: hDatePicker))
        datePicker?.setValue(UIColor.black, forKey: "textColor")
        datePicker?.datePickerMode = .date
        datePicker?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        datePicker?.date = Date()
        datePicker?.addTarget(self, action: #selector(labelChange), for: .valueChanged)
        contentView?.addSubview(datePicker!)
    }
    
    @objc func removeAction(){
        self.delegate?.inputDatePickerViewWithRemoveAction(self)
    }
    
    @objc func doneAction(){
        self.delegate?.inputDatePickerViewWithDoneAction(self, date: datePicker!.date)
    }
    
    @objc func labelChange(){
        let df = DateFormatter()
        df.dateStyle = .medium
    }
    
    @objc func didTapBackground(){
        if(contentView != nil && contentView!.isHidden == false){
            hidePicker()
        }
    }
    
    func setDate(date : Date){
        datePicker?.setDate(date, animated: true)
    }
    func showPicker(){
        //
        blackView?.alpha = 0.5
        blackView?.isHidden = false
        //
        var containerFrame  = contentView!.frame;
        let yFrame = self.frame.size.height - containerFrame.size.height
        if(yFrame != containerFrame.origin.y){
            containerFrame.origin.y = yFrame
            
            // Animations settings
            UIView.animate(withDuration: TimeInterval(0.25), delay: 0.0, options: [.curveEaseOut], animations: {
                self.contentView?.frame = containerFrame
                self.contentView?.isHidden = false
            }, completion: nil)
            
        }
    }
    func hidePicker(){
        var containerFrame = contentView!.frame
        let yFrame  = self.frame.size.height
        if(yFrame != containerFrame.origin.y){
            containerFrame.origin.y = yFrame;
            //
            blackView?.alpha = 0.5
            blackView?.isHidden = false
            // Animations settings
            UIView.animate(withDuration: TimeInterval(0.25), delay: 0.0, options: [.curveEaseOut], animations: {
                self.contentView?.frame = containerFrame;
                self.blackView?.alpha = 0
            }, completion: { finished in
                self.contentView?.isHidden = true
                self.blackView?.isHidden = true
                self.removeFromSuperview()
            })
            
        }
    }
}
