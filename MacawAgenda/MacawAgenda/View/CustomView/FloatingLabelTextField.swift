//
//  FloatingLabelTextField.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import SkyFloatingLabelTextField

class FloatingLabelTextField: SkyFloatingLabelTextField{
    //Views
    private var btnShowHide : UIButton?
    
    /**
     Initializes the control
     - parameter frame the frame of the control
     */
    override public init(frame: CGRect) {
        super.init(frame: frame)
        //
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUp(){
        self.placeholderColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        self.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 16)
        self.placeholderFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 16)
        self.lineColor = AppColor.MicsColors.LINE_VIEW_NORMAL
        self.selectedLineColor = AppColor.NormalColors.BLUE_COLOR
        self.titleColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        self.titleFont = UIFont(name: FontNames.Lato.MULI_BOLD, size: 10) ?? UIFont.boldSystemFont(ofSize: 10)
        self.selectedTitleColor = AppColor.NormalColors.BLUE_COLOR
        
    }
    
    func updateShowHidePasswordView(secureTextEntry : Bool){
        self.isSecureTextEntry = secureTextEntry
        //
        if(secureTextEntry == false){
            self.rightView = nil
        }
        else{
            if(self.rightView == nil){
                //
                let wRightView : CGFloat = 50
                let hRightView : CGFloat = self.frame.size.height
                let rightView = UIView(frame: CGRect(x: 0, y: 0, width: wRightView, height: hRightView))
                rightView.isUserInteractionEnabled = true
                self.rightViewMode = .always
                self.rightView = rightView
                
                let xShowHide : CGFloat = 0
                let yShowHide : CGFloat = self.titleHeight()
                let wShowHide : CGFloat = wRightView
                let hShowHide : CGFloat = hRightView - yShowHide
                btnShowHide = UIButton.init(type: .custom)
                btnShowHide?.frame = CGRect(x: xShowHide, y: yShowHide, width: wShowHide, height: hShowHide)
                btnShowHide?.setTitle(NSLocalizedString("show", comment:"").uppercased(), for: .normal)
                btnShowHide?.setTitleColor(AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, for: .normal)
                btnShowHide?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
                btnShowHide?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 12)
                btnShowHide?.titleLabel?.textAlignment = .right
                btnShowHide?.contentHorizontalAlignment = .right
                btnShowHide?.addTarget(self, action: #selector(showHideAction), for: .touchUpInside)
                rightView.addSubview(btnShowHide!)
            }
        }
    }
    
    @objc func showHideAction(){
        self.isSecureTextEntry = !self.isSecureTextEntry
        var strTitle = ""
        if(self.isSecureTextEntry == true){
            strTitle = NSLocalizedString("show", comment:"")
        }
        else{
            strTitle = NSLocalizedString("hide", comment:"")
        }
        btnShowHide?.setTitle(strTitle.uppercased(), for: .normal)
    }
    
}
