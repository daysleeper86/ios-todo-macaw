//
//  FloatingLabelTextFieldView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/26/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class FloatingLabelTextFieldView: UIView,UITextFieldDelegate {
    //Open var
    open var secureTextEntry: Bool = false {
        didSet {
            textField?.updateShowHidePasswordView(secureTextEntry: secureTextEntry)
            if(secureTextEntry == true){
                textField?.delegate = self
            }
            else{
                textField?.delegate = nil
            }
        }
    }
    
    static let paddingLeftTextField : CGFloat = AppDevice.ScreenComponent.ItemPadding
    static let paddingTopTextField : CGFloat = AppDevice.SubViewFrame.FloatingLabelTextFieldTopBottomPadding
    static let heightTextField : CGFloat = AppDevice.SubViewFrame.FloatingLabelTextFieldHeight
    static let heightTextFieldView : CGFloat = paddingTopTextField + heightTextField
    var textField : FloatingLabelTextField?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        //
        let xTextField : CGFloat = FloatingLabelTextFieldView.paddingLeftTextField
        let yTextField : CGFloat = FloatingLabelTextFieldView.paddingTopTextField
        let wTextField : CGFloat = self.frame.size.width - (xTextField * 2)
        let hTextField : CGFloat = FloatingLabelTextFieldView.heightTextField
        
        textField = FloatingLabelTextField(frame: CGRect(x: xTextField, y: yTextField, width: wTextField, height: hTextField))
        self.addSubview(textField!)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let start: UITextPosition = textField.position(from: textField.beginningOfDocument, offset: range.location),
            let end: UITextPosition = textField.position(from:start, offset: range.length),
            let textRange: UITextRange = textField.textRange(from: start, to: end){
            textField.replace(textRange, withText: string)
        }
        return false
    }
    
    
}
