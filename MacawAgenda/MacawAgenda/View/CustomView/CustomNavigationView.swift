//
//  CustomNavigationView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class CustomNavigationView: UIView {
    //Views
    static let hLineNavigation : CGFloat = 1.0
    private var viewLineNavigation : UIView?
    private var lbTitleNavigation : UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        viewLineNavigation = UIView(frame: CGRect(x: 0, y: self.frame.size.height-CustomNavigationView.hLineNavigation, width: self.frame.size.width, height: CustomNavigationView.hLineNavigation))
        viewLineNavigation?.backgroundColor = AppColor.MicsColors.LINE_COLOR
        viewLineNavigation?.isHidden = true
        self.addSubview(viewLineNavigation!)
    }

    func showHideTitleNavigation(isShow : Bool){
        lbTitleNavigation?.isHidden = !isShow
        viewLineNavigation?.isHidden = !isShow
    }
    
    func titleView(titleView : UILabel){
        lbTitleNavigation = titleView
        self.addSubview(titleView)
    }
    
    func showHideNavigationLine(isHidden : Bool){
        viewLineNavigation?.isHidden = isHidden
    }
}
