//
//  UserHorizontalView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/2/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit


protocol UserHorizontalViewDelegate : NSObject {
    func horizontalView(_ horizontalView: UserHorizontalView, clickedAt buttonIndex: Int)
}

class UserHorizontalView: UIView,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    private var tbTeamAgenda : UICollectionView?
    private let userAvatarCellIdentifier : String = "userAvatarCellIdentifier"
    private var selectedItem : Int = 0
    var arrayUsers : [User] = []
    //
    weak var delegate: UserHorizontalViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    func reloadData(users: [User]){
        self.arrayUsers = users
        tbTeamAgenda?.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        //Collection team agenda
        let wCollectionItemAgenda : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hCollectionItemAgenda : CGFloat = self.frame.size.height
        
        let xCollectionAgenda : CGFloat = 0
        let yCollectionAgenda : CGFloat = 0
        let wCollectionAgenda : CGFloat = self.frame.size.width
        let hCollectionAgenda : CGFloat = self.frame.size.height
        
        let flowLayout_Horizontal = UICollectionViewFlowLayout()
        flowLayout_Horizontal.scrollDirection = .horizontal
        flowLayout_Horizontal.itemSize = CGSize(width: wCollectionItemAgenda, height: hCollectionItemAgenda)
        flowLayout_Horizontal.minimumInteritemSpacing = 10.0
        
        tbTeamAgenda = UICollectionView(frame:  CGRect(x: xCollectionAgenda, y: yCollectionAgenda, width: wCollectionAgenda, height: hCollectionAgenda), collectionViewLayout: flowLayout_Horizontal)
        tbTeamAgenda?.delegate = self
        tbTeamAgenda?.dataSource = self
        tbTeamAgenda?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbTeamAgenda?.isOpaque = false
        tbTeamAgenda?.showsHorizontalScrollIndicator = false
        tbTeamAgenda?.register(TeamAgendaUserItemViewCell.self, forCellWithReuseIdentifier: userAvatarCellIdentifier)
        self.addSubview(tbTeamAgenda!)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayUsers.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  userAvatarCellIdentifier, for: indexPath) as? TeamAgendaUserItemViewCell else {
            return TeamAgendaUserItemViewCell()
        }
        let user = arrayUsers[indexPath.row]
        let showText = indexPath.row == selectedItem ? true : false
        cell.setValueForCell(user:user)
        cell.showText(showText)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        var wCollectionItemAgenda : CGFloat
        if(indexPath.row == selectedItem){
            let user = arrayUsers[indexPath.row]
            //Calculate size width
            let sizeName : CGSize = user.getUserName().getTextSizeWithString(TeamAgendaUserItemViewCell.maxWidthForName,TeamAgendaUserItemViewCell.fontForName)
            //Size cell
            wCollectionItemAgenda = TeamAgendaUserItemViewCell.xSmallAvatar + TeamAgendaUserItemViewCell.wSmallAvatar + TeamAgendaUserItemViewCell.pTitleName+sizeName.width+TeamAgendaUserItemViewCell.pTitleName
        }
        else{
            wCollectionItemAgenda = AppDevice.SubViewFrame.UserHorizontalItemWidth
        }
        let hCollectionItemAgenda : CGFloat = AppDevice.SubViewFrame.UserHorizontalItemHeight
        return CGSize(width: wCollectionItemAgenda, height: hCollectionItemAgenda)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if(indexPath.row != selectedItem){
            // Animate
            doSelected(previous: selectedItem, current: indexPath.row)
            //
            self.delegate?.horizontalView(self,clickedAt:selectedItem)
        }
    }
    
    func scrollToIndex(index : Int){
        doSelected(previous: selectedItem, current: index)
    }
    
    func doSelected(previous: Int, current: Int){
        guard let strongCollection = tbTeamAgenda else { return }
        //
        strongCollection.collectionViewLayout.invalidateLayout()
        
        //previous
        let previousIndexPath = IndexPath(row: previous, section: 0)
        if let previousCell = strongCollection.cellForItem(at: previousIndexPath){
            UIView.transition(with: previousCell, duration: 0.3, options: .curveLinear, animations: {
                previousCell.frame.size.width = AppDevice.SubViewFrame.UserHorizontalItemWidth
                //
                if let previousUserCell = previousCell as? TeamAgendaUserItemViewCell{
                    let showText = false
                    previousUserCell.showText(showText)
                }
            }, completion: nil)
        }
        
        //Current
        let currentIndexPath = IndexPath(row: current, section: 0)
        if let currentCell = strongCollection.cellForItem(at: currentIndexPath){
            //Calculate size width
            let user = arrayUsers[selectedItem]
            let sizeName : CGSize = user.getUserName().getTextSizeWithString(TeamAgendaUserItemViewCell.maxWidthForName,TeamAgendaUserItemViewCell.fontForName)
            let wCollectionItemAgenda : CGFloat = TeamAgendaUserItemViewCell.xSmallAvatar + TeamAgendaUserItemViewCell.wSmallAvatar + TeamAgendaUserItemViewCell.pTitleName+sizeName.width+TeamAgendaUserItemViewCell.pTitleName
            UIView.transition(with: currentCell, duration: 0.3, options: .curveLinear, animations: {
                currentCell.frame.size.width = wCollectionItemAgenda
                if let currentUserCell = currentCell as? TeamAgendaUserItemViewCell{
                    let showText = true
                    currentUserCell.showText(showText)
                }
            }, completion: { complete in
                //
                UIView.animate(withDuration: 0.3, animations: {
                    strongCollection.scrollToItem(at: IndexPath(row: current, section: 0), at: .centeredHorizontally, animated: false)
                })
            })
        }
        else{
            //
            UIView.animate(withDuration: 0.3, animations: {
                strongCollection.scrollToItem(at: IndexPath(row: current, section: 0), at: .centeredHorizontally, animated: false)
            })
        }
        
        //
        selectedItem = current
        
    }
}
