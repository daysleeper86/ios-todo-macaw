//
//  TaskProgressView.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/1/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TaskProgressView: UIView {
    //Views
    private var lbTitleProgress : FAShimmerLabelView?
    private var vLineFull : FAShimmerView?
    private var vLineProgress : UIView?
    
    //Data
    static let hTitleProgress : CGFloat = 21
    static let hLineProgress : CGFloat = 4
    static let pTopLineProgress : CGFloat = 3
    static let pHeader : CGFloat = 8
    static let hHeader : CGFloat = pHeader + pTopLineProgress + hTitleProgress + hLineProgress + pHeader
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        let wHeader : CGFloat = self.frame.size.width
        
        // Title name
        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = pTitleName
        let yTitleName : CGFloat = TaskProgressView.pHeader
        let wTitleName : CGFloat = wHeader - (xTitleName * 2)
        let hTitleName : CGFloat = TaskProgressView.hTitleProgress
        
        lbTitleProgress = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleProgress?.textAlignment = .left
        lbTitleProgress?.numberOfLines = 0
        lbTitleProgress?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleProgress?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        self.addSubview(lbTitleProgress!)
        
        let xLineProgress : CGFloat = xTitleName
        let wLineProgress : CGFloat = wTitleName
        let yLineProgress : CGFloat = yTitleName + hTitleName + TaskProgressView.pTopLineProgress
        
        vLineFull = FAShimmerView(frame: CGRect(x: xLineProgress, y: yLineProgress, width: wLineProgress, height: TaskProgressView.hLineProgress))
        vLineFull?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        vLineFull?.layer.cornerRadius = TaskProgressView.hLineProgress/2
        vLineFull?.layer.masksToBounds = true
        self.addSubview(vLineFull!)
        
        vLineProgress = UIView(frame: CGRect(x: vLineFull!.frame.origin.x, y: vLineFull!.frame.origin.y, width: 0, height: vLineFull!.frame.size.height))
        vLineProgress?.backgroundColor = UIColor.green
        vLineProgress?.layer.cornerRadius = TaskProgressView.hLineProgress/2
        vLineProgress?.layer.masksToBounds = true
        self.addSubview(vLineProgress!)
    }
    
    func resetValue_Cell(){
        lbTitleProgress?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        lbTitleProgress?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        vLineFull?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        vLineProgress?.isHidden = true
        
        lbTitleProgress?.startShimmering()
        vLineFull?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = lbTitleProgress?.isShimmering(){
            if(isShimmering == true){
                //
                lbTitleProgress?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                vLineFull?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                vLineProgress?.isHidden = false
                
                lbTitleProgress?.stopShimmering()
                vLineFull?.stopShimmering()
            }
        }
    }

    func setValueForView(completed : Int, numTask : Int){
        if(numTask > 0){
            stopShimmering_Cell()
            //
            lbTitleProgress?.text = String(completed) + "/" + String(numTask) + " " + NSLocalizedString("completed_talks", comment: "").lowercased()
            //
            let progress : CGFloat = CGFloat(completed)/CGFloat(numTask)
            //
            vLineProgress?.frame.size.width = vLineFull!.frame.size.width * progress
            //
            if(progress > 0){
                if(progress == 1){
//                    vLineProgress?.backgroundColor = AppColor.NormalColors.BLUE_COLOR
                    vLineProgress?.backgroundColor = AppColor.NormalColors.BLUE_COLOR
                }
                else{
                    vLineProgress?.backgroundColor = AppColor.NormalColors.BLUE_COLOR
//                    vLineProgress?.backgroundColor = AppColor.NormalColors.GREEN_COLOR
                }
            }
        }
        else{
            startShimmering_Cell()
        }
    }
}
