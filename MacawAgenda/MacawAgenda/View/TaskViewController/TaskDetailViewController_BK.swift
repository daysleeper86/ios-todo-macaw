//
//  TaskDetailViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/21/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Moya
import Photos
import Material
import Toast_Swift

struct DescriptionInfo {
    static let maxLine : CGFloat = 5
    static let font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
    static let tag = 111111
}


class TaskDetailViewController: BaseViewController {
    //View
    var btnMenuSetting : UIButton?
    var btnPriority : UIButton?
    var btnFinishCheckList : UIButton?
    var lbTitleName : UILabel?
    var lbTextTime : UILabel?
    var labelDescription : UILabel?
    var webviewDescription : UIWebView?
    var imvAvatarAssignee : UIImageView?
    var lbTitleAssign : UILabel?
    var tbBacklog = UITableView(frame: .zero, style: .grouped )
    
    var inputDatePickerView : InputDatePickerView?
    var inputTaskInfoView : InputTaskInfoView?
    //Data
    var contentHeights : [CGFloat] = []
    var fileViewModel : FileViewModel?
    var messageViewModel : MessageViewModel?
    var taskDetailData : Task = Task()
    var subjectTaskId : BehaviorSubject<String>?
    var files : [File] = []
    var agendaTaskId : String = ""
    //Uploading
    var uploadingFiles : [File] = []
    var subjectUploadingFile = BehaviorSubject<[File]>(value: [])
    var yFrameAvatar_Scroll : CGFloat = 0
    var isDeleted = false
    var isFromBacklog = false
    //
    let attachmentCellIdentifier : String = "AttachmentCellIdentifier"
    let attachmentProgressingCellIdentifier : String = "AttachmentProgressingCellIdentifier"
    static let receiverInfoCellIdentitifer : String = "receiverInfoCellIdentitifer"
    static let titleDiscussion = NSLocalizedString("discussion", comment:"")
    static let titleAttachment = NSLocalizedString("attachment", comment:"").uppercased()
    var duration : CGFloat = 0
    //Chatting
    var keyboardBounds : CGRect = .zero
    var lastContentSizeKeyboard : CGFloat = 0
    //Datasource
    var dataSource: RxTableViewSectionedReloadDataSource<MultipleSectionModel>?
    //Can edit
    var isEditPermission : Bool = false
    
    //Subjects Name
    var subjectTaskName : PublishSubject<String> = PublishSubject()
    var subjectTaskNameEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Finished
    var subjectTaskFinished : PublishSubject<Bool> = PublishSubject()
    var subjectTaskFinishedEventStared : PublishSubject<Void> = PublishSubject()
    var subjectTaskAgendaFinished : PublishSubject<Int> = PublishSubject()
    var subjectTaskAgendaFinishedEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Priority
    var subjectTaskPriority : PublishSubject<Bool> = PublishSubject()
    var subjectTaskPriorityEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Description
    var subjectTaskDescription : PublishSubject<String> = PublishSubject()
    var subjectTaskDescriptionEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Assignee
    var subjectTaskAssignee : PublishSubject<String> = PublishSubject()
    var subjectTaskAssigneeEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Duedate
    var subjectTaskDuedate : PublishSubject<Date> = PublishSubject()
    var subjectTaskDuedateEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Follower
    var subjectTaskFollower : PublishSubject<String> = PublishSubject()
    var subjectAddOrRemoveFollower : PublishSubject<Int> = PublishSubject()
    var subjectTaskFollowerEventStared : PublishSubject<Void> = PublishSubject()
    //Subject Deleted
    var subjectTaskDeleted : PublishSubject<Bool> = PublishSubject()
    var subjectTaskDeletedEventStared : PublishSubject<Void> = PublishSubject()
    
    lazy var inputTextView : CustomTextView = {
        let hInputTextView : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let xInputTextView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wInputTextView : CGFloat =  self.defaultInputView.frame.size.width - xInputTextView - self.sendTextButton.frame.size.width
        let yInputTextView : CGFloat = 0
        
        let inputTextView = CustomTextView(frame: CGRect(x: xInputTextView, y: yInputTextView, width: wInputTextView, height: hInputTextView))
        inputTextView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        inputTextView.clipsToBounds = true
        inputTextView.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        inputTextView.isScrollable = false
        inputTextView.isEditable = true
        inputTextView.maxNumberOfLines = 5
        inputTextView.delegate = self
        inputTextView.textColor = UIColor(hexString: "#545d64")
        inputTextView.placeholderColor = UIColor(hexString: "#b2bac0")
        inputTextView.returnKeyType = .default
        inputTextView.placeholder = NSLocalizedString("placehoder_text_input", comment: "")
        inputTextView.center = CGPoint(x: inputTextView.center.x, y: self.defaultInputView.frame.size.height / 2)
        return inputTextView
    }()
    
    lazy var sendTextButton : UIButton = {
        let hSendTextButton : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        let wSendTextButton : CGFloat = hSendTextButton
        let xSendTextButton : CGFloat = self.defaultInputView.frame.size.width - wSendTextButton
        let ySendTextButton : CGFloat = 0
        
        let sendTextButton = UIButton(frame: CGRect(x: xSendTextButton, y: ySendTextButton, width: wSendTextButton, height: hSendTextButton))
        sendTextButton.setImage(UIImage(named: LocalImage.ChattingImage.Send.NotSelected), for: .normal)
        sendTextButton.setImage(UIImage(named: LocalImage.ChattingImage.Send.Selected), for: .selected)
        sendTextButton.contentMode = .scaleAspectFill
        sendTextButton.isEnabled = false
        
        return sendTextButton
    }()
    
    lazy var defaultInputView : UIView = {
        let xDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wDefaultInputView : CGFloat = self.containerInputChatView.frame.size.width - (xDefaultInputView * 2)
        let hDefaultInputView : CGFloat = Constant.ChattingView.Height_Input_TextView_Message
        
        let defaultInputView = UIView(frame: CGRect(x: xDefaultInputView, y: yDefaultInputView, width: wDefaultInputView, height: hDefaultInputView))
        defaultInputView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        defaultInputView.layer.borderColor = AppColor.NormalColors.GRAY_COLOR.cgColor
        defaultInputView.layer.borderWidth = 1.0
        defaultInputView.layer.cornerRadius = 4.0
        defaultInputView.isOpaque = false
        defaultInputView.isHidden = false

        
        return defaultInputView
    }()
    
    lazy var containerInputChatView : UIView = {
        let height = Constant.ChattingView.Height_Input_Container_Message
        let containerInputChatView = UIView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: height))
        containerInputChatView.backgroundColor = AppColor.NormalColors.WHITE_COLOR

        
        return containerInputChatView
    }()
    
    //Datasource
//    let dataSource = TaskDetailViewController.dataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        checkPermission()
        //
        initView()
        //
        initData()
        //
        refreshData_Task()
        //
        bindToViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override public var preferredStatusBarUpdateAnimation: UIStatusBarAnimation{
        return .slide
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    func addNotification_Upload() {
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadFile_Fail(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_FAIL),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadFile_Successful(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_SUCCESSFULLY),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadFile_Processing(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOADING_PROGRESS),
                                               object: nil)
        //
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadFile_API_Fail(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_API_FAIL),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadFile_API_Successful(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOAD_API_SUCCESSFULLY),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(uploadFile_API_Processing(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPLOADING_API_PROGRESS),
                                               object: nil)
    }
    
    func addNotification_Task() {
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(taskDetail_UpdateTask(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TASK_ITEM),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(taskDetail_DeleteTask(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DELETE_TASK_ITEM),
                                               object: nil)
    }
    
    
    //Notification for uploading file
    @objc func uploadFile_Fail(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let fileProcessing = dictionary[ModelDataKeyString.COLLECTION_FILE] as? File{
                updateFailUpload(fileIdProcessing: fileProcessing.fileId)
            }
        }
    }
    
    @objc func uploadFile_Successful(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let fileProcessing = dictionary[ModelDataKeyString.COLLECTION_FILE] as? File{
                updateProgress(fileIdProcessing: fileProcessing.fileId)
            }
        }
    }
    
    @objc func uploadFile_Processing(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let fileProcessing = dictionary[ModelDataKeyString.COLLECTION_FILE] as? File{
                updateProgress(fileIdProcessing: fileProcessing.fileId)
            }
        }
    }
    
    @objc func uploadFile_API_Fail(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let fileId = dictionary[SettingString.FileId] as? String,
                let message = dictionary[SettingString.Message] as? String{
                updateFailAPI(fileIdProcessing: fileId)
                //
                self.view.makeToast(message, duration: 2.0, position: .center)
            }
        }
    }
    
    @objc func uploadFile_API_Successful(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let fileId = dictionary[SettingString.FileId] as? String,
                let message = dictionary[SettingString.Message] as? String{
                updateSuccessAPI(fileIdProcessing: fileId)
                //
                self.view.makeToast(message, duration: 2.0, position: .center)
            }
        }
    }
    
    @objc func uploadFile_API_Processing(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let fileId = dictionary[SettingString.FileId] as? String,
                let loading = dictionary[SettingString.Progress] as? Bool{
                if(loading == true){
                    updateAPIProgress(fileIdProcessing: fileId)
                }
            }
        }
    }
    
    //Notification for task detail
    @objc func taskDetail_UpdateTask(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let taskProcessing = dictionary[ModelDataKeyString.COLLECTION_TASK] as? Task{
                if(taskProcessing.taskId == taskDetailData.taskId){
                    if(taskProcessing.deactive == 1){
                        DispatchQueue.main.async {
                            let dict = NSDictionary(objects: [taskProcessing], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DELETE_TASK_ITEM), object: dict)
                        }
                    }
                    else{
                        if(taskProcessing.valid()){
                            self.taskDetailData = taskProcessing
                            //
                            checkPermission()
                            //
                            refreshData_Task()
                            //
                            reloadHeaderMenuView()
                        }
                    }
                }
                
            }
        }
    }
    
    
    @objc func taskDetail_DeleteTask(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let taskProcessing = dictionary[ModelDataKeyString.COLLECTION_TASK] as? Task{
                if(taskProcessing.valid() && taskProcessing.taskId == taskDetailData.taskId  && taskProcessing.deactive == 1){
                    isDeleted = true
                    //
                    let message = NSLocalizedString("task_has_been_removed_by_other_user", comment: "")
                    AppDelegate().sharedInstance().makeSystemToast(message: message)
                    //
                    closeAction()
                }
            }
        }
    }
    
    func getVisibleCellFromFile(fileIdProcessing: String) -> FileProgressingItemViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : FileProgressingItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let fileCell = cell as? FileProgressingItemViewCell{
                guard let strongFile = fileCell.fileDocument else { return nil}
                if(strongFile.fileId == fileIdProcessing){
                    visibleCell = fileCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func addNotification_UpdateUserTeamInfo(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(teamAgenda_UpdateInfo(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_SIGNLE_USER_INFO),
                                               object: nil)
    }
    
    //Notification for task detail
    @objc func teamAgenda_UpdateInfo(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let userProcessing = dictionary[ModelDataKeyString.COLLECTION_USER] as? User{
                if(taskDetailData.assignee == userProcessing.userId){
                    setInfoDisplayView_Task(self.taskDetailData)
                }
            }
        }
    }
    
    
    func updateProgress(fileIdProcessing: String){
        if let visibleCell = getVisibleCellFromFile(fileIdProcessing: fileIdProcessing){
            visibleCell.showLoadingIndicator_Upload()
        }
    }
    
    func updateFailUpload(fileIdProcessing: String){
        if let visibleCell = getVisibleCellFromFile(fileIdProcessing: fileIdProcessing){
            guard let strongFile = visibleCell.fileDocument else { return }
            strongFile.dataType = .failed
            visibleCell.setStateForCellWhenUploading()
        }
    }
    
    func updateAPIProgress(fileIdProcessing: String){
        if let visibleCell = getVisibleCellFromFile(fileIdProcessing: fileIdProcessing){
            visibleCell.startProcessing()
        }
    }
    
    func updateFailAPI(fileIdProcessing: String){
        if let visibleCell = getVisibleCellFromFile(fileIdProcessing: fileIdProcessing){
            visibleCell.stopProcessing()
        }
    }
    
    func updateSuccessAPI(fileIdProcessing: String){
        if let visibleCell = getVisibleCellFromFile(fileIdProcessing: fileIdProcessing){
            uploadingFiles.remove(at: visibleCell.tag)
            subjectUploadingFile.onNext(uploadingFiles)
            //
            visibleCell.stopProcessing()
        }
    }
    
    func addNotification(){
        addNotification_Keyboard()
        addNotification_Upload()
        addNotification_Task()
        addNotification_UpdateUserTeamInfo()
    }
    
    func removeNotification(){
        NotificationCenter.default.removeObserver(self)
        removeNotification_Keyboard()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.keyboardBounds = keyboardBounds
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double{
                self.duration = CGFloat(duration)
                //
                if let inputTaskInfoView = inputTaskInfoView{
                    if((inputTaskInfoView.isFirstResponder() == true)){
                        inputTaskInfoView.showPicker(keyboardBounds: keyboardBounds, duration: NSNumber(value: duration), isDismiss: true, bindAddTask:false, bindAddAgenda: "",bindUpdateTask: "")
                    }
                }
            }
        }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
    }
    
    
    func initData(){
        subjectTaskId = BehaviorSubject<String>(value: taskDetailData.taskId)
    }
    
    func setupInputMessage(){
        self.containerInputChatView.addSubview(self.defaultInputView)
        self.defaultInputView.addSubview(self.sendTextButton)
        self.defaultInputView.addSubview(self.inputTextView)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let pMenuSetting : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let wMenuSetting : CGFloat = hContentHeaderView
        let hMenuSetting = wMenuSetting
        let xMenuSetting = wHeaderView - wMenuSetting - pMenuSetting
        let yMenuSetting : CGFloat = yContentHeaderView
        
        btnMenuSetting = IconButton.init(type: .custom)
        btnMenuSetting?.frame = CGRect(x: xMenuSetting, y: yMenuSetting, width: wMenuSetting, height: hMenuSetting)
        btnMenuSetting?.setImage(UIImage(named: "ic_menu_gray"), for: .normal)
        btnMenuSetting?.addTarget(self, action: #selector(settingAction), for: .touchUpInside)
        navigationView.addSubview(btnMenuSetting!)
        
        let wPriority : CGFloat = wMenuSetting
        let hPriority : CGFloat = wPriority
        let xPriority : CGFloat = xMenuSetting - wPriority
        let yPriority : CGFloat = yContentHeaderView
        
        btnPriority = IconButton.init(type: .custom)
        btnPriority?.frame = CGRect(x: xPriority, y: yPriority, width: wPriority, height: hPriority)
        btnPriority?.addTarget(self, action: #selector(priorityAction), for: .touchUpInside)
        navigationView.addSubview(btnPriority!)
        //
        reloadHeaderMenuView()
        
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = AppDevice.ScreenComponent.Width
        let hTableBacklog : CGFloat = AppDevice.ScreenComponent.Height - yTableBacklog
        tbBacklog.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
//        tbBacklog.keyboardDismissMode = .interactive
        tbBacklog.register(FileItemViewCell.self, forCellReuseIdentifier: self.attachmentCellIdentifier)
        tbBacklog.register(FileProgressingItemViewCell.self, forCellReuseIdentifier: self.attachmentProgressingCellIdentifier)
        tbBacklog.register(MessageItemInfoViewCell.self, forCellReuseIdentifier: TaskDetailViewController.receiverInfoCellIdentitifer)
        tbBacklog.register(MessageItemTextViewCell.self, forCellReuseIdentifier: ActivityViewController.receiverTextCellIdentitifer)
        self.view.addSubview(tbBacklog)
        //
        tbBacklog.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        //
        let tapGesture_TableView : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapTableView))
        tapGesture_TableView.delaysTouchesBegan = true
        tapGesture_TableView.cancelsTouchesInView = false
        tapGesture_TableView.delegate = self
        tbBacklog.addGestureRecognizer(tapGesture_TableView)
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if obj == tbBacklog && keyPath == "contentSize" {
                //do stuff here
                NSObject.cancelPreviousPerformRequests(withTarget: self)
                self.perform(#selector(changeContentSizeIfNeed), with: nil, afterDelay: 2)
            }
        }
    }
    
    @objc func changeContentSizeIfNeed() {
        guard let sectionFile = self.dataSource?[0],
                let sectionMessage = self.dataSource?[1],
                let footerView = tbBacklog.tableFooterView else { return }
        //
        var isHolder : Bool = false
        for sectionItem in sectionFile.items{
            switch sectionItem {
            case let .FileSectionItem(file):
                if(file.dataType == .holder){
                    isHolder = true
                }
            default:
                break
            }
            //
            break
        }
        if(isHolder == false){
            for sectionItem in sectionMessage.items{
                switch sectionItem {
                case let .MessageSectionItem(message):
                    if(message.dataType == .holder){
                        isHolder = true
                    }
                default:
                    break
                }
                //
                break
            }
        }
        
        if(isHolder == false){
            if(self.inputTextView.isFirstResponder()){
                if(self.keyboardBounds.size.height > 0){
                    if(tbBacklog.contentSize.height == footerView.frame.origin.y + footerView.frame.size.height){
                        tbBacklog.contentSize.height += self.keyboardBounds.size.height
                    }
                }

            }
        }
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        //Footer input
        setupInputMessage()
        //
        tbBacklog.tableFooterView = self.containerInputChatView
    }
    
    func setInfoDisplayView_Task(_ task : Task){
        let tableHeaderView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: tbBacklog.frame.size.width, height: 0))
        tableHeaderView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //
        let pFrame : CGFloat = AppDevice.ScreenComponent.ItemPadding
        var yFrame : CGFloat = 0
        
        let backgroundView_TitleHeader = UIView(frame: CGRect(x: 0, y: 0, width: tbBacklog.frame.size.width, height: 0))
        backgroundView_TitleHeader.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tableHeaderView.addSubview(backgroundView_TitleHeader)
        
        let pIconChecklist : CGFloat = pFrame
        let xIconChecklist : CGFloat = pIconChecklist
        let wIconChecklist : CGFloat = 24
        let hIconChecklist : CGFloat = wIconChecklist
        
        let pLabelTitleName : CGFloat = pFrame
        let xLabelTitleName : CGFloat = xIconChecklist + wIconChecklist + pLabelTitleName
        let wLabelTitleName : CGFloat = backgroundView_TitleHeader.frame.size.width - pLabelTitleName - xLabelTitleName
        let fontSizeTitle = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        let size = self.taskDetailData.name.getTextSizeWithString(wLabelTitleName, fontSizeTitle)
        //
        let maxTitleHeader : CGFloat = 71
        let hTitleHeader = size.height > maxTitleHeader ? maxTitleHeader : (size.height < 30 ? 44 : 71)
        backgroundView_TitleHeader.frame.size.height = hTitleHeader
        
        let yIconChecklist : CGFloat = ( backgroundView_TitleHeader.frame.size.height - hIconChecklist ) / 2
        btnFinishCheckList = IconButton.init(type: .custom)
        btnFinishCheckList?.frame = CGRect(x: xIconChecklist, y: yIconChecklist, width: wIconChecklist, height: hIconChecklist)
        btnFinishCheckList?.addTarget(self, action: #selector(didTapFinish), for: .touchUpInside)
        backgroundView_TitleHeader.addSubview(btnFinishCheckList!)
        
        //
        let hLabelTitleName : CGFloat = backgroundView_TitleHeader.frame.size.height
        lbTitleName = UILabel(frame: CGRect(x: xLabelTitleName, y: 0, width: wLabelTitleName, height: hLabelTitleName))
        lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleName?.textColor = AppColor.MicsColors.DARK_MENU_COLOR
        lbTitleName?.numberOfLines = 0
        lbTitleName?.isUserInteractionEnabled = true
        backgroundView_TitleHeader.addSubview(lbTitleName!)
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(didTapChangeName))
        doubleTap.numberOfTapsRequired = 2
        lbTitleName?.addGestureRecognizer(doubleTap)
        
        lbTitleName?.text = self.taskDetailData.name
        setStateForFinishChecklistUI(self.taskDetailData.completed)
        
        //
        yFrame += backgroundView_TitleHeader.frame.size.height
        yFrameAvatar_Scroll = yFrame
        
        let hBackGroundDuedate : CGFloat = 48
        let backgroundView_Duedate = UIView(frame: CGRect(x: 0, y: yFrame, width: tbBacklog.frame.size.width, height: hBackGroundDuedate))
        backgroundView_Duedate.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tableHeaderView.addSubview(backgroundView_Duedate)
        //
        let xIconTime : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wIconTime : CGFloat =  24.0
        let hIconTime : CGFloat =  wIconTime
        let yIconTime : CGFloat = (backgroundView_Duedate.frame.size.height - hIconTime) / 2
        let imvTime = UIImageView(frame: CGRect(x: xIconTime, y: yIconTime, width: wIconTime, height: hIconTime))
        imvTime.image = UIImage(named: "ic_duedate")
        backgroundView_Duedate.addSubview(imvTime)
        
        let xLabelTime : CGFloat = imvTime.frame.origin.x+imvTime.frame.size.width+pFrame
        let yLabelTime : CGFloat = 0
        let wLabelTime : CGFloat = backgroundView_Duedate.frame.size.width-xLabelTime-pFrame
        let hLabelTime : CGFloat = backgroundView_Duedate.frame.size.height
        lbTextTime = UILabel(frame: CGRect(x: xLabelTime, y: yLabelTime, width: wLabelTime, height: hLabelTime))
        lbTextTime?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTextTime?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15)
        lbTextTime?.textColor = AppColor.NormalColors.BLUE_COLOR
        backgroundView_Duedate.addSubview(lbTextTime!)
        
        reloadDueDateDataIfNeed()
        //
        yFrame += backgroundView_Duedate.frame.size.height
        
        let hBackGroundAssign : CGFloat = 48
        let backgroundView_Assign = UIView(frame: CGRect(x: 0, y: yFrame, width: tbBacklog.frame.size.width, height: hBackGroundAssign))
        backgroundView_Assign.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tableHeaderView.addSubview(backgroundView_Assign)
        
        //
        let yIconAssign : CGFloat = (backgroundView_Assign.frame.size.height-hIconTime) / 2
        imvAvatarAssignee = UIImageView(frame: CGRect(x: xIconTime, y: yIconAssign, width: wIconTime, height: hIconTime))
        imvAvatarAssignee?.layer.cornerRadius = imvAvatarAssignee!.frame.size.height/2
        imvAvatarAssignee?.layer.masksToBounds = true
        imvAvatarAssignee?.isUserInteractionEnabled = true
        backgroundView_Assign.addSubview(imvAvatarAssignee!)
        
        let xTitleAssign : CGFloat = imvAvatarAssignee!.frame.origin.x+imvAvatarAssignee!.frame.size.width+pFrame
        let wTitleAssign : CGFloat = backgroundView_Assign.frame.size.width-xLabelTime-pFrame
        let yTitleAssign : CGFloat = 0
        let hTitleAssign : CGFloat = backgroundView_Assign.frame.size.height
        lbTitleAssign = UILabel(frame: CGRect(x: xTitleAssign, y: yTitleAssign, width: wTitleAssign, height: hTitleAssign))
        lbTitleAssign?.isUserInteractionEnabled = true
        lbTitleAssign?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15)
        lbTitleAssign?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        backgroundView_Assign.addSubview(lbTitleAssign!)
        
        if(self.taskDetailData.assignee.count > 0){
            lbTitleAssign?.textColor = AppColor.NormalColors.BLACK_COLOR
            //
            lbTitleAssign?.text = self.taskDetailData.getAssigneeName()
            loadOwnedUserAvatar(imvAvatar: imvAvatarAssignee!, userID: self.taskDetailData.assignee, name: self.taskDetailData.getAssigneeName())
        }
        else{
            lbTitleAssign?.textColor = AppColor.NormalColors.BLUE_COLOR
            //
            lbTitleAssign?.text = NSLocalizedString("add_assignee", comment:"")
            imvAvatarAssignee?.image = UIImage(named: "ic_assign")
        }
        let tapGesture_Assignee : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapAssignee))
        lbTitleAssign?.addGestureRecognizer(tapGesture_Assignee)
        //
        yFrame += backgroundView_Assign.frame.size.height
        
        var strDescription = ""
        if(self.taskDetailData.description.count > 0){
            let fontSizeNormal : String = "14"
            strDescription =
                "<html>" +
                    "<head>" +
                    "<meta name=\"viewport\" content=\"initial-scale=1, maximum-scale=1\">" +
                    "<style type=\"text/css\">" +
                    "@font-face { font-family: 'LatoRegular';src: url('Lato-Regular.ttf')} " +
                    "@font-face { font-family: 'LatoItalic';src: url('Lato-Italic.ttf')} " +
                    "@font-face { font-family: 'LatoBold';src: url('Lato-Bold.ttf')} " +
                    "body { font-family: 'LatoRegular';font-size: " + fontSizeNormal + "px; } " +
                    "strong { font-family: 'LatoBold' } " +
                    "em { font-family: 'LatoItalic' } " +
                    ".mention {color: #0080FF;font-size: 15px;font-weight: bold} " +
                    "</style>" +
                    "</head>" +
                    "<body>" +
                    self.taskDetailData.description +
                    "</body>" +
            "</html>";
        }
        
        //Default height
        let hBackgroundDescription : CGFloat = 48
        //
        let backgroundView_Description = UIView(frame: CGRect(x: 0, y: yFrame, width: tbBacklog.frame.size.width, height: hBackgroundDescription))
        backgroundView_Description.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tableHeaderView.addSubview(backgroundView_Description)
        
        let yImvDescription : CGFloat = 12
        let imvDescription = UIImageView(frame: CGRect(x: xIconTime, y: yImvDescription, width: wIconTime, height: hIconTime))
        imvDescription.image = UIImage(named: "ic_description")
        backgroundView_Description.addSubview(imvDescription)
        
        //
        var xDescription : CGFloat = imvDescription.frame.origin.x + imvDescription.frame.size.width + pFrame
        var yDescription : CGFloat = 0
        var wDescription : CGFloat = backgroundView_Description.frame.size.width - xDescription - pFrame
        let hDescription : CGFloat = hBackgroundDescription
        
        //
        labelDescription = nil
        webviewDescription = nil
        if(strDescription.count == 0){
            labelDescription = UILabel(frame: CGRect(x: xDescription, y: yDescription, width: wDescription, height: hDescription))
            labelDescription?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            labelDescription?.textColor = AppColor.NormalColors.BLUE_COLOR
            labelDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15)
            labelDescription?.text = NSLocalizedString("add_task_description", comment:"")
            labelDescription?.numberOfLines = 0
            backgroundView_Description.addSubview(labelDescription!)
        }
        else{
            let paddingLeftWebview : CGFloat =  8.0
            yDescription = 6
            xDescription = xDescription - paddingLeftWebview
            wDescription = wDescription + (paddingLeftWebview * 2)
            webviewDescription = UIWebView(frame: CGRect(x: xDescription, y: yDescription, width: wDescription, height: hDescription))
            webviewDescription?.backgroundColor = AppColor.NormalColors.LIGHT_RED_COLOR
            webviewDescription?.tag = DescriptionInfo.tag
            webviewDescription?.delegate = self
            backgroundView_Description.addSubview(webviewDescription!)
            //
            let bundleUrl = URL(fileURLWithPath: Bundle.main.bundlePath)
            webviewDescription?.loadHTMLString(strDescription, baseURL: bundleUrl)
        }
        
        let isEdit : Bool = false
        if(isEdit == true){
            if(labelDescription != nil){
                let tapGesture_Description : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapDescription))
                labelDescription?.isUserInteractionEnabled = true
                labelDescription?.addGestureRecognizer(tapGesture_Description)
            }
            else{
                let tapGesture_Description : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapDescription))
                tapGesture_Description.numberOfTapsRequired = 2
                tapGesture_Description.delegate = self
                webviewDescription?.isUserInteractionEnabled = true
                webviewDescription?.addGestureRecognizer(tapGesture_Description)
            }
        }
        yFrame += backgroundView_Description.frame.size.height
        //
        tableHeaderView.frame.size.height =  yFrame
        tbBacklog.tableHeaderView = tableHeaderView
    }
    
    func reloadHeaderMenuView(){
        //Priority
        let priority = self.taskDetailData.priority
        if(priority == true){
            self.btnPriority?.setImage(UIImage(named: "ic_priority"), for: .normal)
        }
        else{
            self.btnPriority?.setImage(UIImage(named: "ic_unpriority"), for: .normal)
        }
        
    }
    
//    func reloadAssigneeUserInfo(user: User){
//        loadOwnedUserAvatar(imvAvatar: imvAvatarAssignee!, userID: self.taskDetailData.assignee, name: self.taskDetailData.getAssigneeName())
//
//    }
    
    func checkPermission(){
        //
//        isEditPermission = (Util.checkPermissionEditTask(self.taskDetailData.assignee) && self.taskDetailData.checkIsTodayAgenda().count > 0 && isFromBacklog == false)
        isEditPermission = (self.taskDetailData.assignee.count == 0 || (Util.checkPermissionEditTask(self.taskDetailData.assignee) && self.taskDetailData.checkIsTodayAgenda().count > 0))
    }
    
    func reloadAllMessageData_Task(_ task: Task){
        self.agendaTaskId = task.checkIsTodayAgenda()
    }
    
    func refreshData_Task(){
        setInfoDisplayView_Task(self.taskDetailData)
        //
        reloadAllMessageData_Task(self.taskDetailData)
        //
        disableAllForEdit(disable: !isEditPermission)
    }
    
    func setStateForFinishChecklistUI(_ finish : Bool) {
        if(finish == true){
            self.btnFinishCheckList?.setImage(UIImage(named: "ic_list_checked"), for: .normal)
        }
        else{
            self.btnFinishCheckList?.setImage(UIImage(named: "ic_uncheck"), for: .normal)
        }
    }
    
    func reloadDueDateDataIfNeed() {
        if(self.taskDetailData.duedate != nil){
            let strDate = self.taskDetailData.duedate?.dayMonthYearString()
            self.lbTextTime?.text = strDate
        }
        else{
            setDueDateIfNil(lbTime: self.lbTextTime!)
        }
        //
        self.lbTextTime?.isUserInteractionEnabled = true
        let tapGesture_Duedate : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapDueDate))
        self.lbTextTime?.addGestureRecognizer(tapGesture_Duedate)
    }
    
    func setDueDateIfNil(lbTime : UILabel){
        let string = NSLocalizedString("add_due_date", comment:"")
        lbTime.text = string
    }
    
    
    func loadOwnedUserAvatar(imvAvatar : UIImageView,userID : String?, name : String){
        if let strongUserId = userID{
            imvAvatar.setDefaultAvatarIfNeed()
            //
            var user = User(userId: strongUserId, dataType: .normal)
            user.name = name
            imvAvatar.loadAvatarForUser(user: user)
        }
    }
    
    func disableAllForEdit(disable: Bool){
        if(disable == true){
            btnFinishCheckList?.isUserInteractionEnabled = false
            btnFinishCheckList?.alpha = 0.4
            //
            btnPriority?.isUserInteractionEnabled = false
            btnPriority?.alpha = 0.4
            //
            lbTitleName?.isUserInteractionEnabled = false
            lbTitleName?.alpha = 0.4
            //
            lbTextTime?.isUserInteractionEnabled = false
            lbTextTime?.alpha = 0.4
            //
            lbTitleAssign?.isUserInteractionEnabled = false
            lbTitleAssign?.alpha = 0.4
            //
            labelDescription?.isUserInteractionEnabled = false
            labelDescription?.alpha = 0.4
        }
        else{
            btnFinishCheckList?.isUserInteractionEnabled = true
            btnFinishCheckList?.alpha = 1.0
            //
            btnPriority?.isUserInteractionEnabled = true
            btnPriority?.alpha = 1.0
            //
            lbTitleName?.isUserInteractionEnabled = true
            lbTitleName?.alpha = 1.0
            //
            lbTextTime?.isUserInteractionEnabled = true
            lbTextTime?.alpha = 1.0
            //
            lbTitleAssign?.isUserInteractionEnabled = true
            lbTitleAssign?.alpha = 1.0
            //
            labelDescription?.isUserInteractionEnabled = false
            labelDescription?.alpha = 0.4
        }
    }
    
    @objc func didTapTableView(){
        self.view?.endEditing(true)
    }
    
    @objc func didTapChangeName(){
        if(isEditPermission == true){
            let content = self.taskDetailData.name
            showAddTaskView(content: content)
        }
    }
    
    @objc func didTapFinish(){
        if(self.agendaTaskId.count > 0){
            let finished = self.taskDetailData.completed == true ? Constant.TaskStatus.kTaskStatusUncompleted.rawValue : Constant.TaskStatus.kTaskStatusCompleted.rawValue
            self.subjectTaskAgendaFinished.onNext(finished)
            self.subjectTaskAgendaFinishedEventStared.onNext(())
        }
        else{
            let updated = !self.taskDetailData.completed
            self.subjectTaskFinished.onNext(updated)
            self.subjectTaskFinishedEventStared.onNext(())
        }
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func priorityAction(){
        if(isEditPermission == true){
            let updated = !self.taskDetailData.priority
            self.subjectTaskPriority.onNext(updated)
            self.subjectTaskPriorityEventStared.onNext(())
        }
        
    }
    
    @objc func seemoreFile(){
        let fileBrowserViewController = FileBrowserViewController(nibName: nil, bundle: nil)
        fileBrowserViewController.taskDetailData = taskDetailData
        //
        let navigationController = UINavigationController(rootViewController: fileBrowserViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func settingAction(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        //
        var addOrRemove : Int = 0
        var strFollowing : String = ""
        if(self.taskDetailData.checkFollowing(myUserId: userId)){
            strFollowing = NSLocalizedString("unfollow_this_task", comment: "")
            addOrRemove = 0
        }
        else{
            strFollowing = NSLocalizedString("follow_this_task", comment: "")
            addOrRemove = 1
        }
        //
        let ac = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title:strFollowing, style: .default, handler: {[weak self] (action) in
            self?.subjectTaskFollower.onNext(userId)
            self?.subjectAddOrRemoveFollower.onNext(addOrRemove)
            self?.subjectTaskFollowerEventStared.onNext(())
        }))
        
        //
        if(isEditPermission == true){
            ac.addAction(UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .destructive, handler: {[weak self] (action) in
                // handle action by updating model with deletion
                guard let strongSelf = self else { return }
                //
                if(strongSelf.isEditPermission == true){
                    let title = NSLocalizedString("are_you_sure_you_want_to_delete",comment:"")
                    let name = strongSelf.taskDetailData.name
                    let strSpace = "\n"
                    let titleMessage  = title + strSpace + name
                    //
                    let attributedString = NSMutableAttributedString(string: titleMessage)
                    attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: titleMessage.count))
                    attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: title.count))
                    attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold), range: NSRange(location: title.count + strSpace.count, length: name.count))
                    
                    let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    actionSheet.setValue(attributedString, forKey: "attributedTitle")
                    actionSheet.addAction(UIAlertAction(title: NSLocalizedString("delete",comment:""), style: .destructive, handler: {[weak self] (action) in
                        //
                        self?.subjectTaskDeleted.onNext(true)
                        self?.subjectTaskDeletedEventStared.onNext(())
                    }))
                    actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel",comment:""), style: .cancel))
                    self?.present(actionSheet, animated: true)
                }
            }))
        }
        ac.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel))
        present(ac, animated: true)
    }
    
    @objc func cancelAction(sender: UIButton){
        let rowTag = sender.tag
        //Update UI
        let cell = tbBacklog.cellForRow(at: IndexPath(row: rowTag, section: 0))
        if let processingCell = cell as? FileProgressingItemViewCell{
            guard let strongFile = processingCell.fileDocument else { return}
            //Cancel
            UploadManager.sharedSingleton.cancelWithId(uploadOperationId: strongFile.fileId)
            //Update objects
            uploadingFiles.remove(at: rowTag)
            subjectUploadingFile.onNext(uploadingFiles)
        }
    }
    
    @objc func didTapAddNewAttachment(){
        requestAlbumPhoto(view: nil)
    }
    
    @objc func didTapRetryFile(gestureRecognizer: UITapGestureRecognizer){
        let sender = gestureRecognizer.view
        guard let strongSender = sender else { return }
        //
        let rowTag = strongSender.tag
        //Update UI
        let cell = tbBacklog.cellForRow(at: IndexPath(row: rowTag, section: 0))
        if let processingCell = cell as? FileProgressingItemViewCell{
            guard let strongFile = processingCell.fileDocument else { return }
            strongFile.progressUpload = 0.01
            strongFile.dataType = .processing
            
            //Reset cell
            processingCell.setStateForCellWhenUploading()
            
            //Reset progress
            processingCell.showLoadingIndicator_Upload()
            
            //Reupload
            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            UploadManager.sharedSingleton.addFile(file: strongFile, projectId: projectId)
        }
    }
    
    func trackFinishTaskInCollaborate(taskId: String,fromCollaborate : Bool){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        if(fromCollaborate == true){
            Tracker.trackFinishTaskInCollaborate(taskid: taskId, userId: userId, wip: true)
        }
        else{
            Tracker.trackFinishTaskInFocus(taskid: taskId, userId: userId, wip: true)
        }
    }
    
    func bindToViewModel(){
        guard let strongSubjectTaskId = subjectTaskId else { return }
        
        let fileController = FileController.sharedAPI
        self.fileViewModel = FileViewModel(
            input: (
                taskId: strongSubjectTaskId.asObservable(),
                dataSource: subjectUploadingFile.asObserver(),
                fileController: fileController
            )
        )
        //Subscrible for add upload file
        self.fileViewModel?.files?.subscribe(onNext: {[weak self] files in
            guard let strongSelf = self else { return }
            //
            strongSelf.files = files
        }).disposed(by: disposeBag)
        
        let messageController = MessageController.sharedAPI
        self.messageViewModel = MessageViewModel(
            inputNeedTaskId: (
                taskId: subjectTaskId?.asObservable(),
                messageController: messageController
            )
        )
        
        //Subscrible for height
        self.messageViewModel?.messages?.subscribe(onNext: {[weak self] messages in
            guard let strongSelf = self else { return }
            var heights : [CGFloat] = []
            for _ in messages{
                heights.append(ActivityViewController.DefaultHeightWebView)
            }
            strongSelf.contentHeights = heights
        }).disposed(by: disposeBag)
        
        let filesAndMessageSection = Observable
            .combineLatest(self.fileViewModel!.files!, self.messageViewModel!.messages!)  { (files: $0, messages: $1) }
            .distinctUntilChanged {
                (old: (files: [File], messages: [Message]),
                new: (files: [File], messages: [Message])) in
                var distinct = false
                if(old.files.count == new.files.count && old.messages.count == new.messages.count){
                    if(FileService.checkHolderFile(files: new.files) && MessageService.checkHolderMessage(messages: new.messages)){
                        distinct = true
                    }
                }
                return distinct
            }
            .flatMapLatest{ fileMessage -> Observable<[MultipleSectionModel]> in
                //Files
                var strTitleFile = NSLocalizedString("attachment", comment:"").uppercased()
                if(fileMessage.files.count > 0){
                    if(FileService.checkHolderFile(files: fileMessage.files) == false){
                        strTitleFile = strTitleFile + " (" + String(fileMessage.files.count) + ")"
                    }
                }
                var fileItemsections : [SectionItem] = []
                for file in fileMessage.files {
                    fileItemsections.append(.FileSectionItem(file: file))
                }
                //Messages
                var strTitleDiscussion = TaskDetailViewController.titleDiscussion
                if(fileMessage.messages.count > 0){
                    if(MessageService.checkHolderMessage(messages: fileMessage.messages) == false){
                        let firstMessage = fileMessage.messages[0]
                        if(firstMessage.dataType == .normal){
                            strTitleDiscussion = strTitleDiscussion + " (" + String(fileMessage.messages.count) + ")"
                        }
                    }
                }
                var messageItemsections : [SectionItem] = []
                for message in fileMessage.messages {
                    messageItemsections.append(.MessageSectionItem(message: message))
                }
                
                let sections: [MultipleSectionModel] = [
                    .FileSection(title: strTitleFile,
                                 items: fileItemsections),
                    .MessageSection(title: strTitleDiscussion,
                                    items: messageItemsections)
                ]
                
                return Observable.just(sections)
        }
        .share(replay: 1)
        
        //Datasource
        let dataSource = RxTableViewSectionedReloadDataSource<MultipleSectionModel>(
            configureCell: {[weak self] (dataSource, table, indexPath, _) in
                guard let strongSelf = self else { return UITableViewCell() }
                switch dataSource[indexPath] {
                case let .FileSectionItem(file):
                    if(file.dataType == .processing){
                        guard let cell = table.dequeueReusableCell(withIdentifier:   strongSelf.attachmentProgressingCellIdentifier, for: indexPath) as? FileProgressingItemViewCell else {
                            return FileProgressingItemViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                        cell.setValueForCell_File(file: file)
                        //
                        cell.btnMenu?.tag = indexPath.row
                        cell.btnMenu?.addTarget(self, action: #selector(strongSelf.cancelAction(sender:)), for: .touchUpInside)
                        //
                        cell.tag = indexPath.row
                        //Tap Retry Event
                        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(strongSelf.didTapRetryFile(gestureRecognizer:)))
                        cell.lbTextDescription?.addGestureRecognizer(tapGesture)
                        cell.lbTextDescription?.tag = indexPath.row
                        //
                        return cell
                    }
                    else{
                        guard let cell = table.dequeueReusableCell(withIdentifier:  strongSelf.attachmentCellIdentifier, for: indexPath) as? FileItemViewCell else {
                            return FileItemViewCell()
                        }
                    
                        cell.selectionStyle = .none
                        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
//                        cell.delegate = self
                        cell.setValueForCell_File(file: file)
                    
                        return cell
                    }
                case let .MessageSectionItem(message):
                    if(message.messageType == .kMessageTypeNoneComment) {
                        guard let cell = table.dequeueReusableCell(withIdentifier:  TaskDetailViewController.receiverInfoCellIdentitifer, for: indexPath) as? MessageItemInfoViewCell else {
                            return MessageItemInfoViewCell()
                        }
                        
                        cell.selectionStyle = .none
                        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                        cell.setValueForCell_NoneComment(message: message)
                        return cell;
                    }
                    else{
                        guard let cell = table.dequeueReusableCell(withIdentifier:  ActivityViewController.receiverTextCellIdentitifer, for: indexPath) as? MessageItemTextViewCell else {
                            return MessageItemTextViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                        //
                        if(message.valid() == true){
                            let htmlHeight = strongSelf.contentHeights[indexPath.row]
                            cell.wvContent?.tag = indexPath.row
                            cell.wvContent?.delegate = self
                            cell.wvContent?.frame.size.height = htmlHeight
                        }
                        else{
                            cell.wvContent?.delegate = nil
                        }
                        //
                        cell.setValueForCell(message: message)
                        return cell
                    }
                }
            },
            titleForHeaderInSection: { dataSource, index in
                let section = dataSource[index]
                return section.title
            }
        )
        
        self.dataSource = dataSource
        
        filesAndMessageSection
            .bind(to: tbBacklog.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        tbBacklog.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }
                //
                if(indexPath.section ==  0){
                    let file = strongSelf.files[indexPath.row]
                    if(file.valid() == true){
                        self?.showPhotoBrowser(index: indexPath.row)
                    }
                }
            })
            .disposed(by: disposeBag)
        
        tbBacklog.rx
            .setDelegate(self)
            .disposed(by : disposeBag)
        
        
        
        // Update task name
        let taskId = self.taskDetailData.taskId
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let viewModelName = TaskAPIViewModel(
            inputUpdateTaskName: (
                taskId: taskId,
                projectId: projectId,
                name: subjectTaskName.asObservable(),
                loginTaps: subjectTaskNameEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelName.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.lbTitleName?.isUserInteractionEnabled = false
                    strongSelf.lbTitleName?.startShimmering()
                }
                else{
                    strongSelf.lbTitleName?.isUserInteractionEnabled = true
                    strongSelf.lbTitleName?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        
        viewModelName.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        // Update task deleted
        let viewModelDeleted = TaskAPIViewModel(
            inputUpdateTaskDeleted: (
                taskId: taskId,
                projectId: projectId,
                deleted: subjectTaskDeleted.asObservable(),
                loginTaps: subjectTaskDeletedEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelDeleted.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.startLoading(message: NSLocalizedString("deleting", comment: ""))
                }
                else{
                    self?.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        
        viewModelDeleted.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
                            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
                            //
                            Tracker.trackRemoveTask(userid: userId, projectId: projectId)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
        // Update task finish
        let viewModelFinished = TaskAPIViewModel(
            inputUpdateTaskFinished: (
                taskId: taskId,
                projectId: projectId,
                finished: subjectTaskFinished.asObservable(),
                loginTaps: subjectTaskFinishedEventStared.asObservable(),
                controller: taskController
            )
        )
            
        viewModelFinished.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.btnFinishCheckList?.isUserInteractionEnabled = false
                    strongSelf.btnFinishCheckList?.startShimmering()
                }
                else{
                    strongSelf.btnFinishCheckList?.isUserInteractionEnabled = true
                    strongSelf.btnFinishCheckList?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelFinished.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                        self?.trackFinishTaskInCollaborate(taskId: reponseAPI.data, fromCollaborate: false)
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        //
        let viewModelAgendaFinished = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaTaskId,
                status: subjectTaskAgendaFinished.asObservable(),
                loginTaps: subjectTaskAgendaFinishedEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelAgendaFinished.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.btnFinishCheckList?.isUserInteractionEnabled = false
                    strongSelf.btnFinishCheckList?.startShimmering()
                }
                else{
                    strongSelf.btnFinishCheckList?.isUserInteractionEnabled = true
                    strongSelf.btnFinishCheckList?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelAgendaFinished.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                        self?.trackFinishTaskInCollaborate(taskId: reponseAPI.data, fromCollaborate: true)
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        // Update task Priority
        let viewModelPriority = TaskAPIViewModel(
            inputUpdateTaskPriority: (
                taskId: taskId,
                projectId: projectId,
                priority: subjectTaskPriority.asObservable(),
                loginTaps: subjectTaskPriorityEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelPriority.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.btnPriority?.isUserInteractionEnabled = false
                    strongSelf.btnPriority?.startShimmering()
                }
                else{
                    strongSelf.btnPriority?.isUserInteractionEnabled = true
                    strongSelf.btnPriority?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelPriority.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        // Update task Description
        let viewModelDescription = TaskAPIViewModel(
            inputUpdateTaskDescription: (
                taskId: taskId,
                projectId: projectId,
                description: subjectTaskDescription.asObservable(),
                loginTaps: subjectTaskDescriptionEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelDescription.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.labelDescription?.isUserInteractionEnabled = false
                    strongSelf.labelDescription?.startShimmering()
                    //
                    strongSelf.webviewDescription?.isUserInteractionEnabled = false
                    strongSelf.webviewDescription?.startShimmering()
                }
                else{
                    strongSelf.labelDescription?.isUserInteractionEnabled = true
                    strongSelf.labelDescription?.stopShimmering()
                    //
                    strongSelf.webviewDescription?.isUserInteractionEnabled = true
                    strongSelf.webviewDescription?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelDescription.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
        // Update task Description
        let viewModelAssignee = TaskAPIViewModel(
            inputUpdateTaskAssignee: (
                taskId: taskId,
                projectId: projectId,
                assignee: subjectTaskAssignee.asObservable(),
                loginTaps: subjectTaskAssigneeEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelAssignee.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.lbTitleAssign?.isUserInteractionEnabled = false
                    strongSelf.lbTitleAssign?.startShimmering()
                }
                else{
                    strongSelf.lbTitleAssign?.isUserInteractionEnabled = true
                    strongSelf.lbTitleAssign?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelAssignee.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        // Update task duedate
        let viewModelDuedate = TaskAPIViewModel(
            inputUpdateTaskDuedate: (
                taskId: taskId,
                projectId: projectId,
                duedate: subjectTaskDuedate.asObservable(),
                loginTaps: subjectTaskDuedateEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelDuedate.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.lbTextTime?.isUserInteractionEnabled = false
                    strongSelf.lbTextTime?.startShimmering()
                }
                else{
                    strongSelf.lbTextTime?.isUserInteractionEnabled = true
                    strongSelf.lbTextTime?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelDuedate.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        // Update task follower
        let viewModelFollower = TaskAPIViewModel(
            inputUpdateTaskFollower: (
                taskId: taskId,
                projectId: projectId,
                follower: subjectTaskFollower.asObservable(),
                addOrRemove: subjectAddOrRemoveFollower.asObservable(),
                loginTaps: subjectTaskFollowerEventStared.asObservable(),
                controller: taskController
            )
        )
        viewModelFollower.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                guard let strongSelf = self else { return }
                if(loading == true){
                    strongSelf.btnMenuSetting?.isUserInteractionEnabled = false
                    strongSelf.btnMenuSetting?.startShimmering()
                }
                else{
                    strongSelf.btnMenuSetting?.isUserInteractionEnabled = true
                    strongSelf.btnMenuSetting?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        viewModelFollower.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
        //Send message
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let messageAPIController = MessageAPIController(provider: projectProvider)
        let observableTap = self.sendTextButton.rx.tap.asObservable()
        let messageViewModel = MessageAPIViewModel(
            inputSendMessageRoomTask: (
                projectId: projectId,
                roomId: subjectTaskId?.asObservable(),
                content: self.inputTextView.textInput.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                messageAPIController: messageAPIController
            )
        )
        // bind results to  {
        messageViewModel.processEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.stateForSendButton(valid)
            })
            .disposed(by: disposeBag)
        //
        messageViewModel.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.inputTextView.textInput?.startShimmering()
                }
                else{
                    self?.inputTextView.textInput?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        //
        messageViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                    self?.inputTextView.textInput?.stopShimmering()
                    self?.inputTextView.textInput?.text = ""
                    self?.inputTextView.resignFirstResponder()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            let message = NSLocalizedString("send_message_content_successfully", comment: "")
//                            self?.view.makeToast(message, duration: 3.0, position: .center)
                        }
                        else{
                            let message = NSLocalizedString("send_message_content_unsuccessfully", comment: "")
                            self?.view.makeToast(message, duration: 3.0, position: .center)
                        }
                        self?.inputTextView.textInput?.stopShimmering()
                        self?.inputTextView.textInput?.text = ""
                        self?.inputTextView.resignFirstResponder()
                    }
                }
            }
        }).disposed(by: disposeBag)
        
    }
    
    func requestAlbumPhoto(view: UIView?){
        //Check permission for access photo library
        let status : PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if (status == .notDetermined){
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({[weak self] status in
                guard let strongSelf = self else { return }
                strongSelf.presentPickerPhoto(view)
            })
        }
        else{
            presentPickerPhoto(view)
        }
    }
    
    
    func scrollToBottom(){
        if(self.keyboardBounds.size.height > 0){
            var yOffset : CGFloat = 0
            if (tbBacklog.contentSize.height < tbBacklog.bounds.size.height) {
                yOffset = self.keyboardBounds.size.height
            }
            else{
                yOffset = tbBacklog.contentSize.height - self.keyboardBounds.size.height - 350
            }
            //
            tbBacklog.setContentOffset(CGPoint(x: 0, y: yOffset), animated: true)
        }
    }
    
    
    func sendImageToServer(dictImage: NSDictionary, file: File, projectId: String){
        if let imageData = dictImage["imageData"] as? UIImage{
            if let scaleImageData = Util.scaleAndRotateImage(image: imageData){
                let strPath = Util.formatFileToName(fileId: file.fileId, taskId: file.taskId, userId: file.user.userId)
                let local_link = Util.formatDefaultType(strName: strPath)
                file.local_link = local_link
                Util.saveImage(image: scaleImageData, name: file.local_link, folder: AppURL.APIDomains.Folder_User_Chatting, completion: { finished in
                    UploadManager.sharedSingleton.addFile(file: file, projectId: projectId)
                })
            }
        }
        
    }
    
    deinit {
        // Release all resources - perform the deinitialization
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        removeNotification()
    }
}

extension TaskDetailViewController : UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if let touchedView = touch.view, let webView = webviewDescription{
            if (touchedView.isDescendant(of: webView)){
                return true
            }
        }
        if let touchedView = touch.view, let tableHeaderView = tbBacklog.tableHeaderView{
            if (touchedView.isDescendant(of: tableHeaderView)){
                return true
            }
        }
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
}

extension TaskDetailViewController : InputDatePickerViewDelegate{
    @objc func didTapDueDate(){
        if(isEditPermission == true){
            if(inputDatePickerView == nil){
                inputDatePickerView = InputDatePickerView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: AppDevice.ScreenComponent.Height))
                inputDatePickerView?.delegate = self
            }
            self.view.window?.addSubview(inputDatePickerView!)
            var date : Date?
            if(self.taskDetailData.duedate != nil){
                date = self.taskDetailData.duedate
            }
            else{
                date = Date()
            }
            inputDatePickerView?.setDate(date: date!)
            inputDatePickerView?.showPicker()
        }
    }
    
    func inputDatePickerViewWithRemoveAction(_ inputDatePickerView: InputDatePickerView) {
        inputDatePickerView.hidePicker()
    }
    
    func inputDatePickerViewWithDoneAction(_ inputDatePickerView: InputDatePickerView, date: Date) {
        inputDatePickerView.hidePicker()
        //
        subjectTaskDuedate.onNext(date)
        subjectTaskDuedateEventStared.onNext(())
    }
}

extension TaskDetailViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let pTopFrame : CGFloat = 17
        let hFrame : CGFloat = 48
        let hHeader : CGFloat = pTopFrame + hFrame
        return hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionData = self.dataSource?[section] else { return nil }
        //
        let pTopHeaderFrame : CGFloat = 17
        let hHeaderFrame : CGFloat = 48
        let wHeaderFrame : CGFloat = tableView.frame.size.width
        //Header
        let headerView_Section = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderFrame, height: pTopHeaderFrame+hHeaderFrame))
        headerView_Section.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Title
        let pTitle : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitle : CGFloat = pTitle
        let yTitle : CGFloat = pTopHeaderFrame
        let wTitle : CGFloat = headerView_Section.frame.size.width
        let hTitle : CGFloat = hHeaderFrame
        
        let lbTitle : UILabel = UILabel(frame: CGRect(x: xTitle, y: yTitle, width: wTitle, height: hTitle))
        lbTitle.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitle.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        lbTitle.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitle.numberOfLines = 0
        headerView_Section.addSubview(lbTitle)
        lbTitle.text = sectionData.title
        
        //
        return headerView_Section
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var hHeader : CGFloat = 0
        if(section == 0){
            let pTopFrame : CGFloat = 8
            let hFrame : CGFloat = AppDevice.TableViewRowSize.AddNewTaskHeight
            hHeader = pTopFrame + hFrame
        }
        return hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var footerView : UIView?
        if(section == 0){
            let pTopFrame : CGFloat = 8
            let hFrame : CGFloat = AppDevice.TableViewRowSize.AddNewTaskHeight
            let hHeader = pTopFrame + hFrame
            //
            footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: hHeader))
            footerView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
            //
            let xAddTaskView : CGFloat = 0
            let yAddTaskView : CGFloat = 0
            let wAddTaskView : CGFloat = AppDevice.ScreenComponent.Width
            let hAddTaskView : CGFloat = AppDevice.TableViewRowSize.AddNewTaskHeight
            //
            let addTaskView = TaskAddNewView(frame: CGRect(x: xAddTaskView, y: yAddTaskView, width: wAddTaskView, height: hAddTaskView))
            addTaskView.setValueForTitle(title: NSLocalizedString("add_attachment", comment: ""))
            footerView?.addSubview(addTaskView)
            //Tap Add Task Event
            let btnAddTaskView = RaisedButton.init(type: .custom)
            btnAddTaskView.frame = CGRect(x: 0, y: 0, width: wAddTaskView, height: hAddTaskView)
            btnAddTaskView.pulseColor = AppColor.MicsColors.LINE_COLOR
            btnAddTaskView.backgroundColor = .clear
            btnAddTaskView.addTarget(self, action: #selector(didTapAddNewAttachment), for: .touchUpInside)
            addTaskView.addSubview(btnAddTaskView)
        }
        //
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight : CGFloat = 0
        guard let sectionData = self.dataSource?[indexPath] else { return cellHeight }
        switch sectionData {
        case .FileSectionItem(_):
            return AppDevice.TableViewRowSize.FileHeight + AppDevice.ScreenComponent.ItemPadding
        case let .MessageSectionItem(message):
            
            if(message.messageType == .kMessageTypeNoneComment) {
                cellHeight = message.messageHeight + Constant.ChattingView.Padding_Info_Chatting_Cell + Constant.ChattingView.Padding_Info_Chatting_Cell
            }
            else{
                let webviewHeight = contentHeights[indexPath.row]
                cellHeight = webviewHeight + Constant.ChattingView.Padding_Chatting_Cell + AppDevice.SubViewFrame.TitleHeight + Constant.ChattingView.Padding_Chatting_Cell
            }
            return cellHeight
        }
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        if(indexPath.section ==  0){
//            let file = files[indexPath.row]
//            if(file.valid() == true){
//                showPhotoBrowser(index: indexPath.row)
//            }
//        }
//    }
}

extension TaskDetailViewController : UIWebViewDelegate{
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        if(webView.tag == DescriptionInfo.tag){
            guard let value = webView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight") else { return }
            var height : CGFloat = 0
            if let number = NumberFormatter().number(from: value) {
                height = CGFloat(truncating: number)
            }
            webView.frame.size.height = height
            webView.isHidden = true
            //Update parent view
            guard let backgroundView_Description = webView.superview else { return }
            backgroundView_Description.frame.size.height = webView.frame.origin.y + webView.frame.size.height
            //
            guard let tableHeaderView = tbBacklog.tableHeaderView else { return }
            let newHeight = backgroundView_Description.frame.origin.y + backgroundView_Description.frame.size.height
            UIView.animate(withDuration: 0.4, animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.tbBacklog.beginUpdates()
                strongSelf.tbBacklog.tableHeaderView = tableHeaderView
                tableHeaderView.frame.size.height = newHeight
                webView.isHidden = false
                strongSelf.tbBacklog.endUpdates()
            })
        }
        else{
            if (contentHeights[webView.tag] != ActivityViewController.DefaultHeightWebView){ return }
            
            var height : CGFloat = 0
            guard let value = webView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight") else { return }
            if let number = NumberFormatter().number(from: value) {
                height = CGFloat(truncating: number)
            }
            
            if (contentHeights[webView.tag] == height){ return }
            
            contentHeights[webView.tag] = height
            tbBacklog.reloadRows(at: [IndexPath(row: webView.tag, section: 1)], with: .fade)
        }
        
    }
}

//extension TaskDetailViewController : UIGestureRecognizerDelegate{
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
//        return true
//    }
//}


extension TaskDetailViewController : CustomTextViewDelegate{
    func growingTextViewDidBeginEditing(_ growingTextView: CustomTextView!) {
        tbBacklog.contentSize.height += self.keyboardBounds.size.height
        //
        lastContentSizeKeyboard = tbBacklog.contentSize.height
        //
        scrollToBottom()
    }
    
    func growingTextViewDidEndEditing(_ growingTextView: CustomTextView!) {
        tbBacklog.contentSize.height -= self.keyboardBounds.size.height

    }
    
    
    func growing(_ growingTextView: CustomTextView!, willChangeHeight height: Float) {
        //
        if(growingTextView == self.inputTextView){
            let newHeight : CGFloat = CGFloat(height)
            let diff : CGFloat = abs(growingTextView.frame.size.height - newHeight)
            if (self.inputTextView.frame.size.height < newHeight){
                self.containerInputChatView.frame.size.height += diff
                //
                tbBacklog.contentSize.height += diff
            }
            else{
                self.containerInputChatView.frame.size.height -= diff
                //
                tbBacklog.contentSize.height -= diff
            }
            let yDefaultInputView : CGFloat = AppDevice.ScreenComponent.ItemPadding
            self.defaultInputView.frame.size.height = self.containerInputChatView.frame.size.height - (yDefaultInputView * 2)
            self.sendTextButton.center = CGPoint(x: self.sendTextButton.center.x, y: self.defaultInputView.frame.size.height / 2)
        }
    }
    
    
    func growingTextViewDidChange(_ growingTextView: CustomTextView!) {
        if(growingTextView == self.inputTextView){
            let messageString = self.inputTextView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            let lenght = messageString.count
            if(lenght < Constant.MinLength.Message){
                stateForSendButton(false)
            }
            else{
                stateForSendButton(true)
            }
        }
    }
    
    func stateForSendButton(_ enable: Bool){
        if(enable == false){
            self.sendTextButton.isUserInteractionEnabled = false
            self.sendTextButton.isSelected = false
            self.sendTextButton.isEnabled = false
        }
        else{
            self.sendTextButton.isUserInteractionEnabled = true
            self.sendTextButton.isSelected = true
            self.sendTextButton.isEnabled = true
        }
    }
    
    func growing(_ growingTextView: CustomTextView!, shouldChangeTextIn range: NSRange, replacementText text: String!) -> Bool {
        if(growingTextView == self.inputTextView){
            return self.inputTextView.text.count + (text.count - range.length) <= Constant.MaxLength.Message;
        }
        return true
    }
    
}

extension TaskDetailViewController : SwipeTableViewCellDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let strSpace = "  "
        
        let strTitleDelete = NSLocalizedString("delete", comment: "") + strSpace
        let deleteAction = SwipeAction(style: .default, title: strTitleDelete) { action, indexPath in
            // handle action by updating model with deletion
        }
        // customize the action appearance
        deleteAction.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        deleteAction.textColor = AppColor.NormalColors.WHITE_COLOR
        deleteAction.backgroundColor = AppColor.NormalColors.LIGHT_RED_COLOR
        deleteAction.image = UIImage(named: "ic_file_delete")
        deleteAction.imageTitleOrientation = .horizontal
        deleteAction.paddingBottom = AppDevice.ScreenComponent.ItemPadding
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 100
        options.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //        options.heig
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? FileItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.MicsColors.DISABLE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? FileItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.NormalColors.WHITE_COLOR)
    }
    
}

extension TaskDetailViewController : EWImageCropperDelegate{
    func presentPickerPhoto(_ view: UIView?){
        DispatchQueue.main.async {
            let photoViewController = EWPhotoCollectionViewController()
            photoViewController.delegate = self
            photoViewController.needCrop = false
            //
            if(view != nil){
                let heroId = "openPhotoPicker"
                view!.hero.id = heroId
                //
                photoViewController.hero.isEnabled = true
                photoViewController.collectionView.hero.id = heroId
            }
            //
            let navigationController = UINavigationController(rootViewController: photoViewController)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func imageCropper(_ cropperViewController: UIViewController, didFinished editImg: UIImage, sourceType source: UIImagePickerController.SourceType) {
        cropperViewController.navigationController?.dismiss(animated: true, completion: nil)
        ///对选取并编辑后的图片直接使用
        guard let imgData = editImg.jpegData(compressionQuality: CGFloat(1.0)) else { return }
        let fileSize : Double = Double(imgData.count)
        let maximumSizeMB = 10
        if(fileSize > Double(maximumSizeMB * 1024 * 1024)){
            let message = NSLocalizedString("photo_maximum_size_prefix", comment: "") + String(maximumSizeMB) + "\n" + NSLocalizedString("photo_maximum_size_subfix", comment: "")
            self.view.makeToast(message, duration: 4.0, position: .center)
        }
        else{
            let fileType = Util.contentTypeForImageData(imgData)
            let imageName = "image_take" + String(Int(Date().timeIntervalSince1970)) + "." + fileType
            
            let file = File(fileId: Util.getRandomKey(), name: imageName, dataType: .processing)
            file.size = fileSize;
            file.associate = taskDetailData.taskId
            file.taskId = taskDetailData.taskId
            file.file_type = fileType
            file.progressUpload = 0.01
            file.width = Double(editImg.size.width)
            file.height = Double(editImg.size.height)
            //
            let userId = AppSettings.sharedSingleton.account?.userId ?? ""
            let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
            
            let user = User(userId: userId, dataType: .normal)
            file.user = user
            
            //Update UI
            uploadingFiles.insert(file, at: 0)
            subjectUploadingFile.onNext(uploadingFiles)
            
            //Send file to queue
            let dictImage = NSDictionary(objects: [imageName,editImg,source], forKeys: [SettingString.ImageName as NSCopying,SettingString.ImageData as NSCopying,SettingString.ImageSource as NSCopying])
            sendImageToServer(dictImage: dictImage, file: file, projectId: projectId)
            
        }
    }
}

extension TaskDetailViewController : MWPhotoBrowserDelegate1{
    func photoBrowser(photoBrowser: MWPhotoBrowser1, filePhotoAtIndex: Int) -> File {
        return files[filePhotoAtIndex]
    }
    
    func numberOfPhotos(photoBrowser: MWPhotoBrowser1) -> Int {
        return files.count
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser1, photoAtIndex: Int) -> MWPhoto1 {
        let file = files[photoAtIndex]
        let url = URL(string: file.pathURL)
        guard let strongURL = url else { return MWPhoto1(image: UIImage(named: "MWPhotoBrowser.bundle/ImageError"))}
        let photo = MWPhoto1(url: strongURL)
        return photo
    }
    
    func showPhotoBrowser(index: Int){
        let photoBrowser = MWPhotoBrowser1(delegate: self)
        // Set options
        photoBrowser.displayActionButton = true // Show action button to allow sharing, copying, etc (defaults to YES)
        photoBrowser.displayNavArrows = true // Whether to display left and right nav arrows on toolbar (defaults to NO)
        photoBrowser.displaySelectionButtons = false // Whether selection buttons are shown on each image (defaults to NO)
        photoBrowser.zoomPhotosToFill = true// Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
        photoBrowser.alwaysShowControls = false // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
        photoBrowser.enableGrid = true // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
        photoBrowser.startOnGrid = false // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
        photoBrowser.autoPlayOnAppear = false // Auto-play first video
        
        // Customise selection images to change colours if required
        photoBrowser.customImageSelectedIconName = "ImageSelected.png"
        photoBrowser.customImageSelectedSmallIconName = "ImageSelectedSmall.png"
        
        // Present
        let navigationController = UINavigationController(rootViewController: photoBrowser)
        self.present(navigationController, animated: true, completion: nil)
        
        //
        
        // Manipulate
        photoBrowser.showNextPhotoAnimated(animated: false)
        photoBrowser.showPreviousPhotoAnimated(animated: false)
        
        //Optionally set the current visible photo before displaying
        photoBrowser.setCurrentPhotoIndex(index: index)
    }
}

extension TaskDetailViewController : InputTaskInfoViewDelegate{
    func showAddTaskView(content : String){
        if(inputTaskInfoView == nil){
            inputTaskInfoView = InputTaskInfoView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: AppDevice.ScreenComponent.Height))
            inputTaskInfoView?.delegate = self
        }
        //
        self.view.window?.addSubview(inputTaskInfoView!)
        inputTaskInfoView?.setContent(content:content)
        inputTaskInfoView?.becomeResponder()
    }
    
    func inputTaskInfoViewAdd(_ inputTaskInfoView: InputTaskInfoView, addNew withName: String) {
        //
        subjectTaskName.onNext(withName)
        subjectTaskNameEventStared.onNext(())
    }
    
    func inputTaskInfoViewUpdate(_ inputTaskInfoView: InputTaskInfoView, update withName: String, taskId: String){
        
    }
    
    func inputTaskInfoViewCancel(_ inputTaskInfoView: InputTaskInfoView, tapCancel isSave: Bool) {
        
    }
}

extension TaskDetailViewController : EditTaskDescriptionViewControllerDelegate{
    @objc func didTapDescription(){
        let editTaskDescriptionView = EditTaskDescriptionViewController(nibName: nil, bundle: nil)
        editTaskDescriptionView.task = self.taskDetailData
        editTaskDescriptionView.delegate = self
        self.present(editTaskDescriptionView, animated: true, completion:  nil)
    }
    
    //Delegate
    func ediTaskDescriptionViewControllerSave(_ ediTaskDescriptionViewController: EditTaskDescriptionViewController, doneWith description: String){
        //
        ediTaskDescriptionViewController.closeAction()
        //
        subjectTaskDescription.onNext(description)
        subjectTaskDescriptionEventStared.onNext(())
    }
    
    func ediTaskDescriptionViewControllerCancel(_ ediTaskDescriptionViewController: EditTaskDescriptionViewController, tapCancel isSave: Bool){
        
    }
}

extension TaskDetailViewController : AssigneeSelectedViewControllerDelegate{
    @objc func didTapAssignee(){
        if(isEditPermission == true){
            let taskAssigneeSelectedView = TaskAssigneeSelectedViewController(nibName: nil, bundle: nil)
            taskAssigneeSelectedView.task = self.taskDetailData
            taskAssigneeSelectedView.delegate = self
            self.present(taskAssigneeSelectedView, animated: true, completion:  nil)
        }
    }
    
    //Delegate
    func assigneeSelectedViewControllerSave(_ assigneeSelectedViewController: TaskAssigneeSelectedViewController, doneWith user: User){
        assigneeSelectedViewController.closeAction()
        //
        var shouldUpdated : Bool = false
        let userId = user.userId
        if(taskDetailData.assignee.count > 0){
            if(taskDetailData.assignee != userId){
                shouldUpdated = true
            }
        }
        else{
            if(userId != Constant.keyHolderUnassignedId){
                shouldUpdated = true
            }
        }
        //
        if(shouldUpdated == true){
            let agendaTaskId = taskDetailData.checkIsTodayAgenda()
            if(agendaTaskId.count > 0){
                let message = NSLocalizedString("current_task_has_plan_your_today", comment: "")
                //
                self.view.makeToast(message, duration: 2.0, position: .center)
            }
            else{
                subjectTaskAssignee.onNext(userId)
                subjectTaskAssigneeEventStared.onNext(())
            }
        }
    }
    
    func assigneeSelectedViewControllerCancel(_ assigneeSelectedViewController: TaskAssigneeSelectedViewController){
        
    }
}


enum MultipleSectionModel {
    case FileSection(title: String, items: [SectionItem])
    case MessageSection(title: String, items: [SectionItem])
}

enum SectionItem {
    case FileSectionItem(file: File)
    case MessageSectionItem(message: Message)
}

extension MultipleSectionModel: SectionModelType {
    typealias Item = SectionItem
    
    var items: [SectionItem] {
        switch  self {
        case .FileSection(title: _, items: let items):
            return items.map {$0}
        case .MessageSection(title: _, items: let items):
            return items.map {$0}
        }
    }
    
    init(original: MultipleSectionModel, items: [Item]) {
        switch original {
        case let .FileSection(title: title, items: _):
            self = .FileSection(title: title, items: items)
        case let .MessageSection(title, _):
            self = .MessageSection(title: title, items: items)
        }
    }
}

extension MultipleSectionModel {
    var title: String {
        switch self {
        case .FileSection(title: let title, items: _):
            return title
        case .MessageSection(title: let title, items: _):
            return title
        }
    }
}

