//
//  EditTaskDescriptionViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/29/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material
import WebKit

protocol EditTaskDescriptionViewControllerDelegate : NSObject {
    func ediTaskDescriptionViewControllerSave(_ ediTaskDescriptionViewController: EditTaskDescriptionViewController_WV, doneWith description: String)
    func ediTaskDescriptionViewControllerCancel(_ ediTaskDescriptionViewController: EditTaskDescriptionViewController_WV, tapCancel isSave: Bool)
}

class EditTaskDescriptionViewController_WV : BaseViewController, WKScriptMessageHandler,WKNavigationDelegate {

    //External props
    var task : Task?
    //Views
    var bgView : UIScrollView?
    var btnSave : UIButton?
    var webviewDescription : WKWebView?
    var menuHeaderPopover : MLKMenuPopover?
    //
    var toolBarScroll : UIScrollView?
    var toolbarHolder : UIView?
    var wToolbar : CGFloat = 0
    var btnBold : UIButton?
    var btnItalic : UIButton?
    var btnUnderline : UIButton?
    var btnUnorderedList : UIButton?
    var btnOrderedList : UIButton?
    
    //Events
    weak var delegate: EditTaskDescriptionViewControllerDelegate?
    //Events
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
//        setDefaultValue()
        //
        bindToViewModel()
    }
    
    func addNotification(){
        addNotification_Keyboard()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int{
                //
                if let strongtoolbarHolder = self.toolbarHolder{
                    //
                    let yTextView : CGFloat = webviewDescription!.frame.origin.y
                    let pTextView : CGFloat = AppDevice.ScreenComponent.ItemPadding
                    var containerFrame  = webviewDescription!.frame
                    let hFrame : CGFloat = bgView!.frame.size.height - yTextView - pTextView - keyboardBounds.size.height;
                    if(hFrame != containerFrame.size.height){
                        containerFrame.size.height = hFrame
                        // Animations settings
                        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, options: [.curveEaseOut], animations: {
                            self.webviewDescription?.frame = containerFrame
                        }, completion: nil)
                    }
                    //
                    let containerToolbarFrame = strongtoolbarHolder.frame
                    let yContainerInputChatView = self.view.frame.size.height - (keyboardBounds.size.height + containerToolbarFrame.size.height)
                    // Animations settings
                    UIView.beginAnimations("Change frame over keyboard container", context: nil)
                    UIView.setAnimationBeginsFromCurrentState(true)
                    UIView.setAnimationDuration(duration)
                    UIView.setAnimationCurve(UIView.AnimationCurve(rawValue: curve) ?? .easeInOut)
                    
                    // Set views with new info
                    strongtoolbarHolder.frame.origin.y = yContainerInputChatView
                    
                    // Commit animations
                    UIView.commitAnimations()
                    
                }
                
                
                
            }
            
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int{
                //
                if let strongtoolbarHolder = self.toolbarHolder{
                    let yTextView : CGFloat = webviewDescription!.frame.origin.y
                    let pTextView : CGFloat = AppDevice.ScreenComponent.ItemPadding
                    var containerFrame  = webviewDescription!.frame
                    let hFrame : CGFloat = bgView!.frame.size.height - yTextView - pTextView
                    if(hFrame != containerFrame.size.height){
                        containerFrame.size.height = hFrame
                        // Animations settings
                        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, options: [.curveEaseOut], animations: {
                            self.webviewDescription?.frame = containerFrame
                        }, completion: nil)
                    }
                    
                    //
                    let containerToolbarFrame = strongtoolbarHolder.frame
                    let yContainerInputChatView = self.view.frame.size.height - containerToolbarFrame.size.height
                    
                    // Animations settings
                    UIView.beginAnimations("Change frame over keyboard container", context: nil)
                    UIView.setAnimationBeginsFromCurrentState(true)
                    UIView.setAnimationDuration(duration)
                    UIView.setAnimationCurve(UIView.AnimationCurve(rawValue: curve) ?? .easeInOut)
                    
                    // Set views with new info
                    strongtoolbarHolder.frame.origin.y = yContainerInputChatView
                    
                    // Commit animations
                    UIView.commitAnimations()
                }
            }
            
        }
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnSave = UIButton.init(type: .custom)
        btnSave?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnSave?.setTitle(NSLocalizedString("done", comment:"").uppercased(), for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnSave?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnSave?.titleLabel?.textAlignment = .right
        btnSave?.contentHorizontalAlignment = .right
        btnSave?.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        navigationView.addSubview(btnSave!)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("task_description", comment: "")
        navigationView.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        bgView = UIScrollView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        self.view.addSubview(bgView!)
        
        let yScreenView : CGFloat = 0
        
        //Title Screen
//        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        let xTitleScreen : CGFloat = pTitleScreen
//        let yTitleScreen : CGFloat = yScreenView
//        let wTitleScreen : CGFloat = wBGView - (xTitleScreen * 2)
//        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
//        let hBottomTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        
//        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
//        lbTitleScreen.textAlignment = .left
//        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
//        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
//        lbTitleScreen.text = NSLocalizedString("task_description", comment: "")
//        bgView!.addSubview(lbTitleScreen)
//        
//        yScreenView += hTitleScreen
//        yScreenView += hBottomTitleScreen
        let webCfg : WKWebViewConfiguration = WKWebViewConfiguration()
        let userController : WKUserContentController = WKUserContentController()
//        let userScript = WKUserScript(
//            source: "redHeader()",
//            injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
//            forMainFrameOnly: true
//        )
        userController.add(self, name: "popupHandler")
        userController.add(self, name: "attributeHandler")
        webCfg.userContentController = userController
        
        
        let pTextViewDescription : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTextViewDescription : CGFloat = 0
        let yTextViewDescription : CGFloat = yScreenView
        let wTextViewDescription : CGFloat = wBGView
        let hTextViewDescription : CGFloat = hBGView - yScreenView - pTextViewDescription
        
        webviewDescription = WKWebView(frame: CGRect(x: xTextViewDescription, y: yTextViewDescription, width: wTextViewDescription, height: hTextViewDescription), configuration: webCfg)
        webviewDescription?.navigationDelegate = self
//        webviewDescription?.configuration = webCfg
        webviewDescription?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        webviewDescription?.tag = DescriptionInfo.tag
//        webviewDescription?.delegate = self
        bgView?.addSubview(webviewDescription!)
        //
//        let bundleUrl = URL(fileURLWithPath: Bundle.main.bundlePath)
//        webviewDescription?.loadHTMLString(strDescription, baseURL: bundleUrl)
//        let url = URL (string: "https://tiptap.scrumpy.io/suggestions")
//        let requestObj = URLRequest(url: url!)
//        webviewDescription?.load(requestObj)
        if let filePath = Bundle.main.url(forResource: "test_son10", withExtension: "htm") {
            let request = URLRequest(url: filePath)
            webviewDescription?.load(request)
        }
        
//        tvDescription = LPlaceholderTextView(frame: CGRect(x: xTextViewDescription, y: yTextViewDescription, width: wTextViewDescription, height: hTextViewDescription))
//        tvDescription?.placeholderText = NSLocalizedString("add_description", comment:"")
//        tvDescription?.placeholderColor = AppColor.NormalColors.GRAY_COLOR
//        tvDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
//        tvDescription?.textColor = AppColor.NormalColors.BLACK_COLOR
//        tvDescription?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
//        tvDescription?.isOpaque = false
//        tvDescription?.delegate = self
//        tvDescription?.returnKeyType = .done
//        bgView?.addSubview(tvDescription!)
//
//        //
//        tvDescription?.becomeFirstResponder()
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if(message.name == "popupHandler") {
            if let dict = message.body as? NSDictionary {
                if let status = dict["status"] as? Int,let message = dict["message"] as? String{
                    if(status == 1){
                        showDropdownView(fromDirection: .top, withText: message)
                    }
                    else{
                        hideDropdownView()
                    }
                }
            }
        }
        else if(message.name == "attributeHandler") {
            if let dict = message.body as? NSDictionary {
                updateToolBarWithButtonName(dictAttribute: dict)
            }
        }
    }
    
    //Finished loading WKWebView
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        setDefaultValue()
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
        //
    
        createToolBarScroll()
        
    }
    
    func bindToViewModel(){
    }
    
    func setDefaultValue(){
        if(self.task != nil){
            if(self.task!.description.count > 0){
                self.setHTML(html: self.task!.description)
            }
        }
    }
    
    @objc func closeAction(){
        self.delegate?.ediTaskDescriptionViewControllerCancel(self, tapCancel: false)
        //
        if(webviewDescription?.isFirstResponder == true){
            webviewDescription?.resignFirstResponder()
        }
        removeNotification_Keyboard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveAction(){
        let scriptSource = "getHTMLContent1();"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                if let strContent = result as? String{
                    self.delegate?.ediTaskDescriptionViewControllerSave(self, doneWith: strContent)
                }
            }
        }
    }
    
    @objc func setHTML(html : String){
//        let html1 = "<p>ewpasadf;<em>mádf</em>;á;df;lmsadf;ms;almdf;à;m;k</p><p>sdfksf</p><p><span class='mention' data-mention-id='be5eb687-f659-4613-bb59-b7f388596f73'>@paint.bsd223@gmail.com</span> </p><p><strong>2323</strong></p><p>3</p><ol><li><p>ssa;dkf;l;lkewrwre</p></li></ol><ul><li><p></p></li></ul>"
//        guard let encodedValue = html.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }
//        var result = html.replace(target: "&", withString: "&amp;")
//        result = result.replace(target: "\"", withString: "&quot;")
//        result = result.replace(target: "'\"'", withString: "&#39;")
//        result = result.replace(target: "<", withString: "&lt;")
//        result = result.replace(target: ">", withString: "&gt;")
        
//        var html1 = html.replace(target: "\"mention\"", withString: "'mention'")
//        html1 = html1.replace(target: "data-mention-id=\"", withString: "data-mention-id=\'")
//        html1 = html1.replace(target: "\">@", withString: "'>@")
//       let content = "\""
        let escapedForJSON = html.replacingOccurrences(of: "\"", with: "\\\"")
        let scriptSource = "setHTMLContent1(\"" + escapedForJSON + "\");"
//        let escapedForJSON = scriptSource.replacingOccurrences(of: "\"", with: "\\\"")
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
            }
        }
    }
    
    func mentionUser(user : User){
//        let scriptSource = "window.testCall(" + user.userId + ",\"" + user.getUserName() + "\");"
        let scriptSource = "selectUserMention(\"" + user.userId + "\",\"" + user.getUserName() + "\");"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
            }
        }
    }
    
   
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
//        if (textView.text.count + (text.count - range.length) > Constant.MaxLength.TaskDescription) {
//            let lengthOfReplacementText = 4 - textView.text.count + range.length
//
//            if (lengthOfReplacementText < 0) {
//                return false
//            }
//
//            var textViewText = textView.text!
//            let newText = textViewText.subString(from: 0, to: lengthOfReplacementText)
//
//            let strFirst = textViewText.subString(from: 0, to: range.location)
//            let strLast = textViewText.subString(from: range.location, to: textViewText.count - 1)
//
//            textViewText = strFirst + newText + strLast
//            textView.text = textViewText;
//            
//            return false
//        }
//        //
//        return true
//    }
    
    func createToolBarScroll() {
        let hToolBarScroll : CGFloat = 44
        let yToolBarScroll : CGFloat = self.view.frame.size.height - hToolBarScroll
            
            
        toolBarScroll = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: hToolBarScroll))
        toolBarScroll?.backgroundColor = .clear
        toolBarScroll?.showsHorizontalScrollIndicator = false
         //
        let backgroundToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: hToolBarScroll))
        backgroundToolbar.autoresizingMask = .flexibleWidth
        
        //Parent holding view
        toolbarHolder = UIView(frame: CGRect(x: 0, y: yToolBarScroll, width: self.view.frame.size.width, height: hToolBarScroll))
        toolbarHolder?.addSubview(toolBarScroll!)
        toolbarHolder?.insertSubview(backgroundToolbar, at: 0)
        
        self.view.addSubview(toolbarHolder!)
        
        //Build the toolbar
        buildToolbar()
    }
    
    func setStateForBoldButton(_ button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.setImage(UIImage(named: "ic_text_bold_normal") , for: .normal)
        }
        else{
            button.setImage(UIImage(named: "ic_text_bold_active") , for: .normal)
        }
    }
    
    func setStateForItalicButton(_ button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.setImage(UIImage(named: "ic_text_italic_normal") , for: .normal)
        }
        else{
            button.setImage(UIImage(named: "ic_text_italic_active") , for: .normal)
        }
    }
    
    func setStateForUnderlineButton(_ button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.setImage(UIImage(named: "ic_text_underline_normal") , for: .normal)
        }
        else{
            button.setImage(UIImage(named: "ic_text_underline_active") , for: .normal)
        }
    }
    
    func setStateForUnorderedList(_ button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.setImage(UIImage(named: "ic_bullet_normal") , for: .normal)
        }
        else{
            button.setImage(UIImage(named: "ic_bullet_active") , for: .normal)
        }
    }
    
    func setStateForOrderedList(_ button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.setImage(UIImage(named: "ic_numbering_normal") , for: .normal)
        }
        else{
            button.setImage(UIImage(named: "ic_numbering_active") , for: .normal)
        }
    }
    
    @objc func setBold(button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.tag = ButtonState.Active
        }
        else{
            button.tag = ButtonState.Normal
        }
        let scriptSource = "setBold();"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                LoggerApp.logInfo("setBold :")
                LoggerApp.logInfo(result!)
            }
        }
        //
        setStateForBoldButton(button)
    }
    
    @objc func setItalic(button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.tag = ButtonState.Active
        }
        else{
            button.tag = ButtonState.Normal
        }
        let scriptSource = "setItalic();"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                LoggerApp.logInfo("setItalic :")
                LoggerApp.logInfo(result!)
            }
        }
        //
        setStateForItalicButton(button)
    }
    
    @objc func setUnderline(button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.tag = ButtonState.Active
        }
        else{
            button.tag = ButtonState.Normal
        }
        let scriptSource = "setUnderline();"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                LoggerApp.logInfo("setUnderline :")
                LoggerApp.logInfo(result!)
            }
        }
        //
        setStateForUnderlineButton(button)
    }
    
    @objc func setUnorderedList(button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.tag = ButtonState.Active
        }
        else{
            button.tag = ButtonState.Normal
        }
        let scriptSource = "setUnorderedList();"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                LoggerApp.logInfo("setUnorderedList :")
                LoggerApp.logInfo(result!)
            }
        }
        //
        setStateForUnorderedList(button)
    }
    
    @objc func setOrderedList(button: UIButton){
        if(button.tag == ButtonState.Normal){
            button.tag = ButtonState.Active
        }
        else{
            button.tag = ButtonState.Normal
        }
        let scriptSource = "setOrderedList();"
        webviewDescription?.evaluateJavaScript(scriptSource) { (result, error) in
            if result != nil {
                LoggerApp.logInfo("setOrderedList :")
                LoggerApp.logInfo(result!)
            }
        }
        //
        setStateForOrderedList(button)
    }
    
    @objc func dismissKeyboard(button: UIButton){
        self.view.endEditing(true)
    }
    
    func itemsForToolbar() -> [UIView] {
        var items : [UIView] = []
        
        let pFirstLeft : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let pItem : CGFloat = 4
        let pLine : CGFloat = 12
        let wButton : CGFloat = 32
        let hToolbar : CGFloat = 44
        
        var xFrame = pFirstLeft
        //
        btnBold = UIButton(frame: CGRect(x: xFrame, y: (hToolbar-wButton)/2, width: wButton, height: wButton))
        btnBold?.tag = ButtonState.Normal
        btnBold?.addTarget(self, action: #selector(setBold(button:)), for: .touchUpInside)
        items.append(btnBold!)
        //
        setStateForBoldButton(btnBold!)
        //
        xFrame = btnBold!.frame.origin.x+btnBold!.frame.size.width+pItem
        //
        btnItalic = UIButton(frame: CGRect(x: xFrame, y: btnBold!.frame.origin.y, width: wButton, height: wButton))
        btnItalic?.tag = ButtonState.Normal
        btnItalic?.addTarget(self, action: #selector(setItalic(button:)), for: .touchUpInside)
        items.append(btnItalic!)
        //
        setStateForItalicButton(btnItalic!)
        //
        xFrame = btnItalic!.frame.origin.x+btnItalic!.frame.size.width+pItem
        //
        btnUnderline = UIButton(frame: CGRect(x: xFrame, y: btnBold!.frame.origin.y, width: wButton, height: wButton))
        btnUnderline?.tag = ButtonState.Normal
        btnUnderline?.addTarget(self, action: #selector(setUnderline(button:)), for: .touchUpInside)
        items.append(btnUnderline!)
        //
        setStateForUnderlineButton(btnUnderline!)
        //
        xFrame = btnUnderline!.frame.origin.x+btnUnderline!.frame.size.width+pItem
        //
        btnUnorderedList = UIButton(frame: CGRect(x: xFrame, y: btnBold!.frame.origin.y, width: wButton, height: wButton))
        btnUnorderedList?.tag = ButtonState.Normal
        btnUnorderedList?.addTarget(self, action: #selector(setUnorderedList(button:)), for: .touchUpInside)
        items.append(btnUnorderedList!)
        //
        setStateForUnorderedList(btnUnorderedList!)
        //
        xFrame = btnUnorderedList!.frame.origin.x+btnUnorderedList!.frame.size.width+pItem
        //
        btnOrderedList = UIButton(frame: CGRect(x: xFrame, y: btnBold!.frame.origin.y, width: wButton, height: wButton))
        btnOrderedList?.tag = ButtonState.Normal
        btnOrderedList?.addTarget(self, action: #selector(setOrderedList(button:)), for: .touchUpInside)
        items.append(btnOrderedList!)
        //
        setStateForOrderedList(btnOrderedList!)
        //
        xFrame = btnOrderedList!.frame.origin.x+btnOrderedList!.frame.size.width+pItem
        
        //
        let wKeyboard : CGFloat = hToolbar
        let btnKeyboard = UIButton(frame: CGRect(x: self.view.frame.size.width-wKeyboard, y: 0, width: wKeyboard, height: wKeyboard))
        btnKeyboard.addTarget(self, action: #selector(dismissKeyboard(button:)), for: .touchUpInside)
        btnKeyboard.setImage(UIImage(named: "ZSSkeyboard.png") , for: .normal)
        items.append(btnKeyboard)
        
        let wLine : CGFloat = 0.6
        let line = UIView(frame: CGRect(x: btnKeyboard.frame.origin.x-wLine, y: 0, width: wLine, height: 44))
        line.backgroundColor = .lightGray
        line.alpha = 0.7
        items.append(line)
        
        wToolbar = self.view.frame.size.width
        
        return items
    }
    
    func barButtonItemDefaultColor() -> UIColor{
        return UIColor(red: CGFloat(0.0)/255, green: CGFloat(122.0)/255, blue: CGFloat(255.0)/255, alpha: 1.0)
    }
    
    func buildToolbar() {
        // Check to see if we have any toolbar items, if not, add them all
        let items = itemsForToolbar()
        // get the width before we add custom buttons
        let toolbarWidth : CGFloat = items.count == 0 ? 0.0 : wToolbar
        //
        for item in items{
            toolbarHolder?.addSubview(item)
        }
        toolbarHolder?.frame.size.width = toolbarWidth
        toolBarScroll?.contentSize = CGSize(width: toolbarHolder?.frame.size.width ?? 0, height: 44)
    }
    
    func updateToolBarWithButtonName(dictAttribute : NSDictionary){
        if let isBold = dictAttribute["bold"] as? Bool,let isItalic = dictAttribute["italic"] as? Bool,let isUnderline = dictAttribute["underline"] as? Bool,let isOrderedList = dictAttribute["orderedList"] as? Bool,let isUnorderedList = dictAttribute["unorderedList"] as? Bool{
            if(isBold == true){
                if(btnBold?.tag == ButtonState.Normal){
                    btnBold?.tag = ButtonState.Active
                    setStateForBoldButton(btnBold!)
                }
            }
            else{
                if(btnBold?.tag == ButtonState.Active){
                    btnBold?.tag = ButtonState.Normal
                    setStateForBoldButton(btnBold!)
                }
            }
            //
            if(isItalic == true){
                if(btnItalic?.tag == ButtonState.Normal){
                    btnItalic?.tag = ButtonState.Active
                    setStateForItalicButton(btnItalic!)
                }
            }
            else{
                if(btnItalic?.tag == ButtonState.Active){
                    btnItalic?.tag = ButtonState.Normal
                    setStateForItalicButton(btnItalic!)
                }
            }
            //
            if(isUnderline == true){
                if(btnUnderline?.tag == ButtonState.Normal){
                    btnUnderline?.tag = ButtonState.Active
                    setStateForUnderlineButton(btnUnderline!)
                }
            }
            else{
                if(btnUnderline?.tag == ButtonState.Active){
                    btnUnderline?.tag = ButtonState.Normal
                    setStateForUnderlineButton(btnUnderline!)
                }
            }
            //
            if(isOrderedList == true){
                if(btnOrderedList?.tag == ButtonState.Normal){
                    btnOrderedList?.tag = ButtonState.Active
                    setStateForOrderedList(btnOrderedList!)
                }
            }
            else{
                if(btnOrderedList?.tag == ButtonState.Active){
                    btnOrderedList?.tag = ButtonState.Normal
                    setStateForOrderedList(btnOrderedList!)
                }
            }
            //
            if(isUnorderedList == true){
                if(btnUnorderedList?.tag == ButtonState.Normal){
                    btnUnorderedList?.tag = ButtonState.Active
                    setStateForUnorderedList(btnUnorderedList!)
                }
            }
            else{
                if(btnUnorderedList?.tag == ButtonState.Active){
                    btnUnorderedList?.tag = ButtonState.Normal
                    setStateForUnorderedList(btnUnorderedList!)
                }
            }
        }
        
        
        
    }
    
    
    
    deinit {
        // Release all resources - perform the deinitialization
        removeNotification_Keyboard()
    }

}

extension EditTaskDescriptionViewController_WV:MLKMenuPopoverPopoverDelegate{
    func showDropdownView(fromDirection direction: LMDropdownViewDirection, withText text: String) {
        //
        if let userTeam = AppSettings.sharedSingleton.userTeam, let strongWebviewDescription = self.webviewDescription{
            //
            var filterUserTeam : [User] = []
            for user in userTeam{
                if(text.count == 0){
                    filterUserTeam.append(user)
                }
                else{
                    if(user.name.range(of: text, options:.caseInsensitive) != nil){
                        filterUserTeam.append(user)
                    }
                }
            }
            //
            if(filterUserTeam.count == 0){
                filterUserTeam.append(UserService.createEmptyUser())
            }
            //
            let paddingTopBottom : CGFloat = 8
            let maxRow : CGFloat = 3
            let heightRow : CGFloat = AppDevice.TableViewRowSize.MemberHeight
            let hMenu : CGFloat = heightRow * (filterUserTeam.count > Int(maxRow) ? maxRow : CGFloat(filterUserTeam.count)) + (paddingTopBottom*2) + 9
            let yMenu : CGFloat = strongWebviewDescription.frame.size.height - hMenu - ( toolbarHolder?.frame.size.height ?? 0)
            let xMenu : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let wMenu : CGFloat = AppDevice.ScreenComponent.Width - (xMenu * 2)
            
            
            let rect = CGRect(x: xMenu, y: yMenu, width: wMenu, height: hMenu)
            if(menuHeaderPopover == nil){
                menuHeaderPopover = MLKMenuPopover(frame: rect)
                //                menuHeaderPopover?.backgroundColor = .red
                menuHeaderPopover?.delegate = self
                menuHeaderPopover?.initView(menuItems:filterUserTeam,search: text)
            }
            else{
                //
                var needChangeFrame : Bool = false
                if(menuHeaderPopover?.menuItems?.count != filterUserTeam.count){
                    needChangeFrame = true
                }
                //
                if(needChangeFrame == true){
                    menuHeaderPopover?.changeFrame(rect: rect)
                }
                //
                menuHeaderPopover?.menuItems = filterUserTeam
                menuHeaderPopover?.search = text
                menuHeaderPopover?.menuItemsTableView?.reloadData()
                //
            }
            //
            if(menuHeaderPopover!.isShowing == false){
                menuHeaderPopover!.showInView(view: bgView!)
            }
            else{
                //                menuHeaderPopover!.hide()
            }
        }
    }
    
    func hideDropdownView(){
        menuHeaderPopover?.hide()
    }
    
    func mlkMenuPopoverPopoverSelected(_ MLKMenuPopover: MLKMenuPopover, didSelectMenuItemAtIndex: Int,item : User) {
        if(item.dataType == .normal){
            self.mentionUser(user: item)
        }
    }
    
    func mlkMenuPopoverPopoverAutoHide(_ MLKMenuPopover: MLKMenuPopover){
        menuHeaderPopover = nil
    }
}



