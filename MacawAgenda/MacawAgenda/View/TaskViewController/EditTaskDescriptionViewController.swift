//
//  EditTaskDescriptionViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/29/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Material

//protocol EditTaskDescriptionViewControllerDelegate : NSObject {
//    func ediTaskDescriptionViewControllerSave(_ ediTaskDescriptionViewController: EditTaskDescriptionViewController, doneWith description: String)
//    func ediTaskDescriptionViewControllerCancel(_ ediTaskDescriptionViewController: EditTaskDescriptionViewController, tapCancel isSave: Bool)
//}

class EditTaskDescriptionViewController: ZSSRichTextEditor {

    //External props
    var task : Task?
    //Views
    var bgView : UIScrollView?
    var btnSave : UIButton?
    var tvDescription : LPlaceholderTextView?
    var menuHeaderPopover : MLKMenuPopover?
    //Events
    weak var delegate: EditTaskDescriptionViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
        setDefaultValue()
        //
        bindToViewModel()
    }
    
    func addNotification(){
        addNotification_Keyboard()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double{
                //
                if(tvDescription?.isFirstResponder == true){
//                    let yTextView : CGFloat = tvDescription!.frame.origin.y
//                    let pTextView : CGFloat = AppDevice.ScreenComponent.ItemPadding
//                    var containerFrame  = tvDescription!.frame
//                    let hFrame : CGFloat = bgView!.frame.size.height - yTextView - pTextView - keyboardBounds.size.height;
//                    if(hFrame != containerFrame.size.height){
//                        containerFrame.size.height = hFrame
//                        // Animations settings
//                        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, options: [.curveEaseOut], animations: {
//                            self.tvDescription?.frame = containerFrame
//                        }, completion: nil)
//                    }
                }
            }
            
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double{
                //
                if(tvDescription?.isFirstResponder == true){
//                    let yTextView : CGFloat = tvDescription!.frame.origin.y
//                    let pTextView : CGFloat = AppDevice.ScreenComponent.ItemPadding
//                    var containerFrame  = tvDescription!.frame
//                    let hFrame : CGFloat = bgView!.frame.size.height - yTextView - pTextView
//                    if(hFrame != containerFrame.size.height){
//                        containerFrame.size.height = hFrame
//                        // Animations settings
//                        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, options: [.curveEaseOut], animations: {
//                            self.tvDescription?.frame = containerFrame
//                        }, completion: nil)
//                    }
                }
            }
            
        }
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnSave = UIButton.init(type: .custom)
        btnSave?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnSave?.setTitle(NSLocalizedString("done", comment:"").uppercased(), for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnSave?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnSave?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnSave?.titleLabel?.textAlignment = .right
        btnSave?.contentHorizontalAlignment = .right
        btnSave?.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        navigationView.addSubview(btnSave!)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("task_description", comment: "")
        navigationView.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - yBGView
        
        bgView = UIScrollView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        self.view.addSubview(bgView!)
        
        let yScreenView : CGFloat = 0
        
        //Title Screen
//        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        let xTitleScreen : CGFloat = pTitleScreen
//        let yTitleScreen : CGFloat = yScreenView
//        let wTitleScreen : CGFloat = wBGView - (xTitleScreen * 2)
//        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
//        let hBottomTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        
//        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
//        lbTitleScreen.textAlignment = .left
//        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
//        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
//        lbTitleScreen.text = NSLocalizedString("task_description", comment: "")
//        bgView!.addSubview(lbTitleScreen)
//        
//        yScreenView += hTitleScreen
//        yScreenView += hBottomTitleScreen
        
        let pTextViewDescription : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTextViewDescription : CGFloat = pTextViewDescription
        let yTextViewDescription : CGFloat = yScreenView
        let wTextViewDescription : CGFloat = wBGView
        let hTextViewDescription : CGFloat = hBGView - yScreenView - pTextViewDescription
        
//        tvDescription = LPlaceholderTextView(frame: CGRect(x: xTextViewDescription, y: yTextViewDescription, width: wTextViewDescription, height: hTextViewDescription))
//        tvDescription?.placeholderText = NSLocalizedString("add_description", comment:"")
//        tvDescription?.placeholderColor = AppColor.NormalColors.GRAY_COLOR
//        tvDescription?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
//        tvDescription?.textColor = AppColor.NormalColors.BLACK_COLOR
//        tvDescription?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
//        tvDescription?.isOpaque = false
//        tvDescription?.delegate = self
//        tvDescription?.returnKeyType = .done
//        bgView?.addSubview(tvDescription!)
//
//        //
//        tvDescription?.becomeFirstResponder()
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
//        createContentView(yHeader: yScreen)
    }
    
    func bindToViewModel(){
    }
    
    func setDefaultValue(){
        if(self.task != nil){
            if(self.task!.description.count > 0){
//                self.tvDescription?.text = self.task!.description
                // HTML Content to set in the editor
//                NSString *html = @"<div contenteditable=\"true\" tabindex=\"0\" class=\"ProseMirror\"><p class="">ewpasadf;<em>mádf</em>;á;df;lmsadf;ms;almdf;à;m;k</p><p>sdfksf</p><p><span class=\"mention\" data-mention-id=\"be5eb687-f659-4613-bb59-b7f388596f73\" contenteditable=\"false\">@paint.bsd223@gmail.com</span> </p><p><strong>2323</strong></p><p>3</p><ol><li><p>ssa;dkf;l;lkewrwre</p></li></ol><ul><li><p><br></p></li></ul></div>";
                
//                let strDescription = self.task!.description
                
                let strDescription = "<p>ewpasadf;<em>mádf</em>;á;df;lmsadf;ms;almdf;à;m;k</p>\n<p>sdfksf</p>\n<p><span style=\"color: #0080FF;font-size: 15px;font-weight: bold;\"  data-mention-id=\"be5eb687-f659-4613-bb59-b7f388596f73\" contenteditable=\"false\">@paint.bsd223@gmail.com</span> \n</p>\n<p><strong>2323</strong>\n</p>\n<p>3</p>\n<ol>\n    <li>\n        <p>ssa;dkf;l;lkewrwre</p>\n    </li>\n</ol>\n<ul>\n    <li>\n        <p></p>\n    </li>\n</ul>"
                
                //Set Custom CSS
                let customCSS = "strong { font-family: 'LatoBold' } .mention {contenteditable: false;color: #0080FF;font-size: 15px;font-weight: bold}"
                self.setCSS(customCSS)
                
                self.alwaysShowToolbar = true
                self.receiveEditorDidChangeEvents = true
                
                // Export HTML
//                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Export" style:UIBarButtonItemStylePlain target:self action:@selector(exportHTML)];
                
                // Set the base URL if you would like to use relative links, such as to images.
                self.baseURL = URL(string: "http://www.zedsaid.com")
                self.shouldShowKeyboard = false
                // Set the HTML contents of the editor
                self.placeholder = NSLocalizedString("add_description", comment:"")
                self.setHTML(strDescription)
            }
        }
    }
    
    @objc func closeAction(){
        self.delegate?.ediTaskDescriptionViewControllerCancel(self, tapCancel: false)
        //
        if(tvDescription?.isFirstResponder == true){
            tvDescription?.resignFirstResponder()
        }
        removeNotification_Keyboard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveAction(){
        let strHTML = self.getHTML()
//        print("html: \(strHTML)")
        
//        self.delegate?.ediTaskDescriptionViewControllerSave(self, doneWith: tvDescription!.text)
        self.selectRange(3, withEnd: 4)
    }
    
    override func editorDidChange(withText text: String!, andHTML html: String!) {
        
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
//        if (textView.text.count + (text.count - range.length) > Constant.MaxLength.TaskDescription) {
//            let lengthOfReplacementText = 4 - textView.text.count + range.length
//
//            if (lengthOfReplacementText < 0) {
//                return false
//            }
//
//            var textViewText = textView.text!
//            let newText = textViewText.subString(from: 0, to: lengthOfReplacementText)
//
//            let strFirst = textViewText.subString(from: 0, to: range.location)
//            let strLast = textViewText.subString(from: range.location, to: textViewText.count - 1)
//
//            textViewText = strFirst + newText + strLast
//            textView.text = textViewText;
//            
//            return false
//        }
//        //
//        return true
//    }
    
    deinit {
        // Release all resources - perform the deinitialization
        removeNotification_Keyboard()
    }

}

extension EditTaskDescriptionViewController:MLKMenuPopoverPopoverDelegate{
    override func showMentionView(_ text: String!) {
        var newString : String?
        if(text.count > 1){
            newString = text.subString(from: 1, to: text.count-1)
        }
        else{
            newString = ""
        }
        if let strongNewString = newString{
            showDropdownView(fromDirection: .top,withText: strongNewString)
        }
        
    }
    
    func showDropdownView(fromDirection direction: LMDropdownViewDirection, withText text: String) {
        //
        if let userTeam = AppSettings.sharedSingleton.userTeam{
            //
            var filterUserTeam : [User] = []
            for user in userTeam{
                if(text.count == 0){
                    filterUserTeam.append(user)
                }
                else{
                    if(user.name.range(of: text, options:.caseInsensitive) != nil){
                        filterUserTeam.append(user)
                    }
                }
            }
            //
            if(filterUserTeam.count == 0){
                filterUserTeam.append(UserService.createEmptyUser())
            }
            //
            let paddingTopBottom : CGFloat = 8
            let maxRow : CGFloat = 3
            let heightRow : CGFloat = AppDevice.TableViewRowSize.MemberHeight
            let hMenu : CGFloat = heightRow * (filterUserTeam.count > Int(maxRow) ? maxRow : CGFloat(filterUserTeam.count)) + (paddingTopBottom*2) + 9
            let yMenu : CGFloat = self.editorView.frame.size.height - hMenu
            let wMenu : CGFloat = 260
            let xMenu : CGFloat = (AppDevice.ScreenComponent.Width-wMenu)/2
            
            
            let rect = CGRect(x: xMenu, y: yMenu, width: wMenu, height: hMenu)
            if(menuHeaderPopover == nil){
                menuHeaderPopover = MLKMenuPopover(frame: rect)
//                menuHeaderPopover?.backgroundColor = .red
                menuHeaderPopover?.delegate = self
                menuHeaderPopover?.initView(menuItems:filterUserTeam)
            }
            else{
                //
                var needChangeFrame : Bool = false
                if(menuHeaderPopover?.menuItems?.count != filterUserTeam.count){
                    needChangeFrame = true
                }
                //
                if(needChangeFrame == true){
                    menuHeaderPopover?.changeFrame(rect: rect)
                }
                //
                menuHeaderPopover?.menuItems = filterUserTeam
                menuHeaderPopover?.menuItemsTableView?.reloadData()
                //
            }
            //
            if(menuHeaderPopover!.isShowing == false){
                menuHeaderPopover!.showInView(view: self.editorView)
            }
            else{
//                menuHeaderPopover!.hide()
            }
        }
    }
    
    func mlkMenuPopoverPopoverSelected(_ MLKMenuPopover: MLKMenuPopover, didSelectMenuItemAtIndex: Int,item : User) {
        if(item.dataType == .normal){
            let html = "<span style=\"color: #0080FF;font-size: 15px;font-weight: bold;\"  data-mention-id=\"be5eb687-f659-4613-bb59-b7f388596f73\" contenteditable=\"false\">"+item.name+"</span>"
            self.insertHTML(html)
        }
        
    }
    
    func mlkMenuPopoverPopoverAutoHide(_ MLKMenuPopover: MLKMenuPopover){
        menuHeaderPopover = nil
    }
}
