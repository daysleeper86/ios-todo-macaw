//
//  TaskAssigneeSelectedViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/29/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Material

protocol AssigneeSelectedViewControllerDelegate : NSObject {
    func assigneeSelectedViewControllerSave(_ assigneeSelectedViewController: TaskAssigneeSelectedViewController, doneWith user: User)
    func assigneeSelectedViewControllerCancel(_ assigneeSelectedViewController: TaskAssigneeSelectedViewController)
}

class TaskAssigneeSelectedViewController: BaseViewController {
    //External props
    var task : Task?
    var arrayUsers : [User] = []
    var navigationTitle : String = NSLocalizedString("assignee", comment: "")
    var selected : Int = 0
    
    //Views
    var navigationView : CustomNavigationView?
    var tbMembers =  UITableView(frame: .zero)
    
    //Data
    var yScreen_Scroll : CGFloat = 0
    static let memberAssigneeCellIdentifier : String = "memberAssigneeCellIdentifier"
    
    var subjectProjectId : BehaviorSubject<String>?
    //Events
    weak var delegate: AssigneeSelectedViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        if(task != nil){
            setDefaultValue()
        }
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    

    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose : UIButton = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_close_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView?.addSubview(btnClose)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView!.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = navigationTitle
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = AppDevice.ScreenComponent.Height - yTableMember
        tbMembers.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbMembers.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbMembers.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbMembers.dataSource = self
        tbMembers.delegate = self
        tbMembers.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbMembers.register(MemberAssigneeItemViewCell.self, forCellReuseIdentifier: TaskAssigneeSelectedViewController.memberAssigneeCellIdentifier)
        self.view.addSubview(tbMembers)
        
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableMember
        let tbMemberHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: 0))
        tbMemberHeaderView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 4.0
        
        let lbTitleScreen = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = navigationTitle
        tbMemberHeaderView.addSubview(lbTitleScreen)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //
        let xTitleNote : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yTitleNote : CGFloat = yViewScreen
        let wTitleNote : CGFloat = wHeaderTableView - (xTitleNote * 2)
        let hTitleNote : CGFloat = 24
        let hBottomTitleNote : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        let lbTitleNote = UILabel(frame: CGRect(x: xTitleNote, y: yTitleNote, width: wTitleNote, height: hTitleNote))
        lbTitleNote.layer.cornerRadius = 4.0
        lbTitleNote.layer.masksToBounds = true
        lbTitleNote.textAlignment = .left
        lbTitleNote.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleNote.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        lbTitleNote.text = NSLocalizedString("select_a_member", comment: "").uppercased()
        tbMemberHeaderView.addSubview(lbTitleNote)
        
        yViewScreen += hTitleNote
        yViewScreen += hBottomTitleNote
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbMemberHeaderView.frame.size.height = hHeaderTableView
        tbMembers.tableHeaderView = tbMemberHeaderView
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    func bindToViewModel(){
        //
        if(task != nil){
            let projectController = ProjectController.sharedAPI
            let projectViewModel = ProjectViewModel(
                inputNeedUsers: (
                    projectId: subjectProjectId?.asObservable(),
                    projectController: projectController
                )
            )
            
            let userAndUnassignee
                = projectViewModel.users?
                    .flatMap {users -> Observable<[User]> in
                        var newUsers = users
                        if(UserService.checkHolderUser(users: users) == false){
                            //
                            var userUnassisnee = User(userId: Constant.keyHolderUnassignedId, dataType: .normal)
                            userUnassisnee.name = NSLocalizedString("unassignee", comment: "")
                            newUsers.insert(userUnassisnee, at: 0)
                        }
                        return Observable.just(newUsers)
                    }
                    .share(replay: 1)
            
            userAndUnassignee?.subscribe({ [weak self] users  in
                guard let strongSelf = self, let strongTask = strongSelf.task, let strongUsers = users.element else { return }
                for i in 0 ..< strongUsers.count{
                    let user = strongUsers[i]
                    if(strongTask.assignee.count > 0 && user.userId == strongTask.assignee){
                        strongSelf.selected = i
                        //
                        break
                    }
                }
                //
                strongSelf.arrayUsers = strongUsers
                strongSelf.tbMembers.reloadData()
            })
                .disposed(by: disposeBag)
        }
        
        tbMembers.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
            }.disposed(by: disposeBag)
    }
    
    func setDefaultValue(){
        //
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        subjectProjectId = BehaviorSubject<String>(value: projectId)
        
    }
    
    @objc func closeAction(){
        self.delegate?.assigneeSelectedViewControllerCancel(self)
        //
        self.dismiss(animated: true, completion: nil)
    }

}

extension TaskAssigneeSelectedViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  TaskAssigneeSelectedViewController.memberAssigneeCellIdentifier, for: indexPath) as? MemberAssigneeItemViewCell else {
            return MemberAssigneeItemViewCell()
        }
        let element = arrayUsers[indexPath.row]
        //
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        var selected : Bool = false
        if(self.selected == indexPath.row){
            selected = true
        }
        cell.setValueForCell(user: element,selected: selected,search: "")
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let heightRow : CGFloat = AppDevice.TableViewRowSize.MemberHeight
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //
        let element = self.arrayUsers[indexPath.row]
        //
        self.delegate?.assigneeSelectedViewControllerSave(self, doneWith: element)
    }
}


