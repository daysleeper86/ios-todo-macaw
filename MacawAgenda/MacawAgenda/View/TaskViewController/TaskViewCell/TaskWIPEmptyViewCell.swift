//
//  TaskEmptyViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/3/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Nantes

protocol TaskWIPEmptyViewCellDelegate : NSObject {
    func taskWIPEmptyViewCellNextAction(_ taskWIPEmptyViewCell: TaskWIPEmptyViewCell)
}

class TaskWIPEmptyViewCell: UITableViewCell {
    //Views
    var backgroundViewCell : UIView?
    var imvAvatar : UIImageView?
    var lbTitleName : NantesLabel?
    //Event
    weak var delegate: TaskWIPEmptyViewCellDelegate?
    //
    let GoToNext : String = "gotoNext"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskWIPHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        // Title name
        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
        let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
        let hTitleName : CGFloat = backgroundViewCell!.frame.size.height
        let yTitleName : CGFloat = 0
        
        lbTitleName = NantesLabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.linkAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.NormalColors.BLUE_COLOR,
            NSAttributedString.Key.font: UIFont(name: FontNames.Lato.MULI_BOLD, size: 16) ?? UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        ]
        lbTitleName?.delegate = self
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 2
        lbTitleName?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 16)
        backgroundViewCell?.addSubview(lbTitleName!)
        //
        backgroundViewCell?.frame.size.height = lbTitleName!.frame.origin.y + lbTitleName!.frame.size.height + pTitleName
        //
        resetAttributeTextLink()
    }
    
    func setDarkModeView(_ darkMode: Bool){
        var bgColor : UIColor!
        var titleColor : UIColor!
        if(darkMode == true){
            bgColor = AppColor.MicsColors.DARK_MENU_COLOR
//            titleColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
            titleColor = AppColor.NormalColors.WHITE_COLOR
        }
        else{
            bgColor = AppColor.NormalColors.WHITE_COLOR
            titleColor = AppColor.NormalColors.BLACK_COLOR
        }
        setBackGroundColor(color: bgColor)
        lbTitleName?.textColor = titleColor
        //
        resetAttributeTextLink()
    }
    
    func resetAttributeTextLink(){
        //Data
        let emptyData = NSLocalizedString("no_running_task", comment: "") + "\n" + NSLocalizedString("start_next_task", comment: "")
        let hereData = NSLocalizedString("here", comment: "")
        let fullData = emptyData + " " + hereData
        lbTitleName?.text = fullData
        //
        if let url = URL(string: GoToNext){
            lbTitleName?.addLink(to: url, withRange: NSRange(location: fullData.count-hereData.count, length: hereData.count))
        }
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
}

extension TaskWIPEmptyViewCell: NantesLabelDelegate{
    // Link handling
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        if(link.absoluteString == GoToNext){
            self.delegate?.taskWIPEmptyViewCellNextAction(self)
        }
    }
}
