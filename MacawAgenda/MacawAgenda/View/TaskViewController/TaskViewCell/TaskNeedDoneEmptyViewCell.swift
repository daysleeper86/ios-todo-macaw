//
//  TaskEmptyViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/3/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Nantes


class TaskNeedDoneEmptyViewCell: UITableViewCell {
    //Views
    var backgroundViewCell : UIView?
    var imvAvatar : UIImageView?
    var lbTitleName : UILabel?
    var itemWidth : CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
    }
    
    func initView(itemWidth : CGFloat,contentPadding: CGFloat){
        self.itemWidth = itemWidth
        
        if(backgroundViewCell == nil){
            // Background View
            let xBackgroundView : CGFloat = 0
            let yBackgroundView : CGFloat = 0
            let wBackgroundView : CGFloat = itemWidth
            let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
            
            backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
            backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
            self.contentView.addSubview(backgroundViewCell!)
            
            // Title name
            let xTitleName : CGFloat = contentPadding
            let wTitleName : CGFloat = wBackgroundView - ( xTitleName * 2)
            let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
            let yTitleName : CGFloat = (hBackgroundView-hTitleName)/2
            
            lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
            lbTitleName?.textAlignment = .left
            lbTitleName?.numberOfLines = 1
            lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
            lbTitleName?.text = NSLocalizedString("you_picked_all_the_task", comment: "")
            backgroundViewCell?.addSubview(lbTitleName!)
        }
    }
    
    func setDarkModeView(_ darkMode: Bool){
        var bgColor : UIColor!
        var titleColor : UIColor!
        if(darkMode == true){
            bgColor = AppColor.MicsColors.DARK_MENU_COLOR
            titleColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        }
        else{
            bgColor = AppColor.NormalColors.WHITE_COLOR
            titleColor = AppColor.NormalColors.BLACK_COLOR
        }
        setBackGroundColor(color: bgColor)
        lbTitleName?.textColor = titleColor
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
}
