//
//  TaskEmptyViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/3/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class TaskEmptyViewCell: UITableViewCell {
    //Views
    var backgroundViewCell : UIView?
    var imvAvatar : UIImageView?
    var lbTitleName : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        
        // Avatar
        let wAvatar : CGFloat = 230
        let hAvatar : CGFloat = 100
        let xAvatar : CGFloat = (wBackgroundView - wAvatar ) / 2
        let yAvatar : CGFloat = 60
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.image = UIImage(named: "ic_empty_task")
        backgroundViewCell?.addSubview(imvAvatar!)
        
        // Title name
        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = pTitleName
        let wTitleName : CGFloat = wBackgroundView - (xTitleName * 2)
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight * 2
        let pTopTitleName : CGFloat = 24
        let yTitleName : CGFloat = yAvatar + hAvatar + pTopTitleName
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .center
        lbTitleName?.numberOfLines = 2
        lbTitleName?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        backgroundViewCell?.addSubview(lbTitleName!)
        //
        backgroundViewCell?.frame.size.height = lbTitleName!.frame.origin.y + lbTitleName!.frame.size.height + pTitleName
    }
    
    func setValueForCell(title: String){
        lbTitleName?.text = NSLocalizedString("no_result_found_for", comment: "") + " \"" + title + "\""
    }
}
