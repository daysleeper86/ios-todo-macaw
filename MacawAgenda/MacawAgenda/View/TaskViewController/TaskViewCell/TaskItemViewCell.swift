//
//  TaskItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit


class TaskItemViewCell: SwipeTableViewCell {
    
    //Views
    var backgroundViewCell : UIView?
    var imvAvatar : FAShimmerImageView?
    var btnCheckbox : FAShimmerButtonView?
    var lbTitleName : FAShimmerLabelView?
    var lbTitleDueDate : FAShimmerLabelView?
    var btnPriority : FAShimmerButtonView?
    //Data
    var task : Task?
    //Static
    static let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
    static let maxWidth : CGFloat = 100
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.TaskHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = wBackgroundView - pAvatar - wAvatar
        let yAvatar : CGFloat = (hBackgroundView-hAvatar)/2
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.isAccessibilityElement = true
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.layer.masksToBounds = true
        backgroundViewCell?.addSubview(imvAvatar!)
        
        // Avatar
        let pPriority : CGFloat = 4
        let wPriority : CGFloat = 24
        let hPriority : CGFloat = wPriority
        let xPriority : CGFloat = xAvatar - pPriority - wPriority
        let yPriority : CGFloat = (hBackgroundView-hPriority)/2
        
        btnPriority = FAShimmerButtonView.init(type: .custom)
        btnPriority?.frame = CGRect(x: xPriority, y: yPriority, width: wPriority, height: hPriority)
        backgroundViewCell?.addSubview(btnPriority!)
        
        // Checkbox
        let wCheckbox : CGFloat = 24
        let hCheckbox : CGFloat = wCheckbox
        let xCheckbox : CGFloat = 16
        let yCheckbox : CGFloat = (hBackgroundView-hCheckbox)/2
        
        btnCheckbox = FAShimmerButtonView.init(type: .custom)
        btnCheckbox?.frame = CGRect(x: xCheckbox, y: yCheckbox, width: wCheckbox, height: hCheckbox)
        backgroundViewCell?.addSubview(btnCheckbox!)
        
        // Title name
        let xTitleName : CGFloat = xCheckbox + wCheckbox + TaskItemViewCell.pTitleName
        let wTitleName : CGFloat = xAvatar - xTitleName - TaskItemViewCell.pTitleName
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let yTitleName : CGFloat = (hBackgroundView - hTitleName ) / 2
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.isAccessibilityElement = true
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 1
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        backgroundViewCell?.addSubview(lbTitleName!)
        
        // Title duedate
        lbTitleDueDate = FAShimmerLabelView(frame: lbTitleName!.frame)
        lbTitleDueDate?.textAlignment = .left
        lbTitleDueDate?.numberOfLines = 0
        lbTitleDueDate?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleDueDate?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        lbTitleDueDate?.text = ""
        backgroundViewCell?.addSubview(lbTitleDueDate!)
        
    }
    
    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
        btnCheckbox?.setImage(nil, for: .normal)
        btnPriority?.setImage(nil, for: .normal)
        lbTitleDueDate?.text = ""
        //
        guard let strongLBTitleName = lbTitleName, let strongIMVAvatar = imvAvatar else { return }
        strongLBTitleName.frame.size.width = strongIMVAvatar.frame.origin.x - strongLBTitleName.frame.origin.x - TaskItemViewCell.pTitleName
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        btnCheckbox?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        imvAvatar?.startShimmering()
        lbTitleName?.startShimmering()
        btnCheckbox?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                btnCheckbox?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopShimmering()
                btnCheckbox?.stopShimmering()
            }
        }
    }
    
    func startProcessingFinishTask(){
        backgroundViewCell?.startShimmering()
        btnCheckbox?.isUserInteractionEnabled = false
    }
    
    func stopProcessingFinishTask(){
        if(backgroundViewCell?.isShimmering() == true){
            backgroundViewCell?.stopShimmering()
            btnCheckbox?.isUserInteractionEnabled = true
        }
    }
    
    func stopProcessingNewTask(){
        if(lbTitleName?.isShimmering() == true){
            lbTitleName?.stopShimmering()
        }
    }
    
    func setValueForCell(task: Task,strSearch: String,index: Int, isCanFinished: Bool){
        if(task.dataType != .holder){
            stopShimmering_Cell()
            //
            self.task = task
            stateForFinishButton()
            //
            var name = ""
            if(task.taskNumber.count > 0){
                name = "#" + task.taskNumber + " - " + task.name
            }
            else{
                name = task.name
            }
        stateForTitleNameAndDueDate(name,strSearch,task.duedate,task.completed,task.priority)
            //
            if(task.dataType == .processing){
                self.lbTitleName?.startShimmering()
            }
            else{
                self.lbTitleName?.stopShimmering()
            }
            //
            loadAssigneeAvatar()
            //
            if(isCanFinished == true){
                self.btnCheckbox?.alpha = 1.0
                self.btnCheckbox?.isUserInteractionEnabled = true
            }
            else{
                self.btnCheckbox?.alpha = 0.4
                self.btnCheckbox?.isUserInteractionEnabled = false
            }
            //
            setAccessibilityIdentifier(index: index)
        }
        else{
            startShimmering_Cell()
        }
        //
        stopProcessingFinishTask()
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    func stateForFinishButton(){
        if let strongTask = self.task{
            //
            var accessibilityLabel = ""
            if(strongTask.completed == false){
                self.btnCheckbox?.setImage(UIImage(named: "ic_uncheck"), for: .normal)
                accessibilityLabel = AccessiblityId.STATUS_UNCHECK
            }
            else{
                self.btnCheckbox?.setImage(UIImage(named: "ic_list_checked"), for: .normal)
                accessibilityLabel = AccessiblityId.STATUS_CHECKED
            }
            //
            self.btnCheckbox?.accessibilityLabel = accessibilityLabel
        }
    }
    
    func stateForDueDate(dueDate : Date , finished : Bool){
        if(finished == false){
            let daysBetweenDates = dueDate.daysBetweenDateAndNow(nowDate: Date())
            if(daysBetweenDates == 0){
                self.lbTitleDueDate?.textColor = AppColor.NormalColors.LIGHT_ORANGE_COLOR
            }
            else if(daysBetweenDates > 0){
                self.lbTitleDueDate?.textColor = AppColor.NormalColors.LIGHT_RED_COLOR
            }
            else{
                self.lbTitleDueDate?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
            }
        }
        else{
            self.lbTitleDueDate?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        }
    }
    
    func stateForTitleNameAndDueDate(_ name : String,_ search: String, _ dueDate : Date?,_ finished: Bool,_ priority: Bool){
        self.lbTitleName?.text = name
        Util.setAttributeStringForSearch(lbText: self.lbTitleName!, withSearch: search, fontSearch: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14), colorSearch: AppColor.NormalColors.BLUE_COLOR, fontNormal: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14), colorNormal: AppColor.NormalColors.BLACK_COLOR)
        //
        if(priority == true){
            self.btnPriority?.setImage(UIImage(named: "ic_priority"), for: .normal)
            self.btnPriority?.isHidden = false
        }
        else{
            self.btnPriority?.isHidden = true
        }
        //
        if let strongDueDate = dueDate{
            let strMonth = strongDueDate.monthString()
            let strDay = strongDueDate.dayString()
            let day = strDay + " " + strMonth
            self.lbTitleDueDate?.text = day
            //
            let sizeDate : CGSize = day.getTextSizeWithString(TaskItemViewCell.maxWidth,self.lbTitleDueDate!.font)
            
            let wTitleDate : CGFloat = sizeDate.width
            let xTitleDate : CGFloat = (self.btnPriority?.isHidden == false ? self.btnPriority!.frame.origin.x : imvAvatar!.frame.origin.x) - wTitleDate - TaskItemViewCell.pTitleName
            self.lbTitleDueDate?.frame.origin.x = xTitleDate
            self.lbTitleDueDate?.frame.size.width = wTitleDate
            
            let xTitleName : CGFloat = self.lbTitleName!.frame.origin.x
            let wTitleName : CGFloat = xTitleDate - xTitleName - TaskItemViewCell.pTitleName
            self.lbTitleName?.frame.origin.x = xTitleName
            self.lbTitleName?.frame.size.width = wTitleName
            
            stateForDueDate(dueDate: strongDueDate, finished: finished)
        }
        else{
            //
            let xTitleDate : CGFloat = (self.btnPriority?.isHidden == false ? self.btnPriority!.frame.origin.x : imvAvatar!.frame.origin.x)
            let xTitleName : CGFloat = self.lbTitleName!.frame.origin.x
            let wTitleName : CGFloat = xTitleDate - xTitleName - TaskItemViewCell.pTitleName
            self.lbTitleName?.frame.origin.x = xTitleName
            self.lbTitleName?.frame.size.width = wTitleName
            self.lbTitleDueDate?.text = ""
        }
    }
    
    func loadAssigneeAvatar(){
        if let strongTask = self.task{
            var user = User(userId: strongTask.assignee, dataType: .normal)
            user.name = strongTask.getAssigneeName()
            stateForAvatar(user,task: strongTask)
        }
    }
    
    func stateForAvatar(_ user : User, task: Task){
        imvAvatar?.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID.count > 0 && task.checkAssigneeNotFound() == true){
//        if(strUserID.count > 0){
            let strURL = Util.parseURLAvatarImageFireStore(strUserID)
            imvAvatar?.sd_setImage(with: URL(string: strURL), placeholderImage: nil, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongSelf = self, let strongObject = strongSelf.task else { return }
                if let imageValue = image{
                    //Current userId
                    let strCurrentUserID = strongObject.assignee
                    if(strCurrentUserID == strUserID){
                        if(imageValue.size.width > 0){
                            strongSelf.imvAvatar?.backgroundColor = .clear
                            strongSelf.imvAvatar?.removeLabelTextIfNeed()
                        }
                        else{
                            let strUserName = strongObject.getAssigneeName()
                            strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                }
                else{
                    //Current userId
                    let strCurrentUserID = strongObject.assignee
                    if(strCurrentUserID == strUserID){
                        let strUserName = strongObject.getAssigneeName()
                        strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
            })
        }
        else{
            imvAvatar?.backgroundColor = .clear
            imvAvatar?.removeLabelTextIfNeed()
            imvAvatar?.image = UIImage(named: "ic_no_assignee")
        }
    }
    
    func setAccessibilityIdentifier(index: Int){
        let strIndex = String(index + 1)
        //
        btnCheckbox?.accessibilityIdentifier = AccessiblityId.BTN_FINISH + strIndex
        btnPriority?.accessibilityIdentifier = AccessiblityId.BTN_PRIORITY + strIndex
        lbTitleName?.accessibilityIdentifier = AccessiblityId.LB_TASKNAME + strIndex
        imvAvatar?.accessibilityIdentifier = AccessiblityId.IMG_ASSIGNEE + strIndex
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
