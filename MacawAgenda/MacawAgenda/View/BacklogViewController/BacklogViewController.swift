//
//  BacklogViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import FirebaseFirestore
import RxSwift
import Moya
import Material

class BacklogViewController: BaseViewController,UISearchBarDelegate,JCActionSheetDelegate,InputTaskInfoViewDelegate {
    //View
    var navigationView : CustomNavigationView?
    var tbBacklogHeaderView : UIView?
    var tbBacklog : UITableView = UITableView(frame: .zero)
    var inputTaskInfoView : InputTaskInfoView?
    //Search
    var searchBarView : UIView?
    var searchBar : UISearchBar?
    var viewOverlay: UIView?
    //Data
    let taskCellIdentifier : String = "taskCellIdentifier"
    let emptyTaskCellIdentifier : String = "emptyTaskCellIdentifier"
    var yScreen_Scroll : CGFloat = 0
    
    //ViewModel
    var viewModel : TaskViewModel?
    var iFilter : Int = Constant.TaskFilter.TaskID.allTask.rawValue
    var subjectFilter = BehaviorSubject<Int>(value: Constant.TaskFilter.TaskID.allTask.rawValue)
    var uploadingTasks : [Task] = []
    var subjectUploadingTasks = BehaviorSubject<[Task]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
        setupUISearchBar()
        //
        bindToViewModel()
        //
        trackOpenBacklog()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //
        let appDelegate : AppDelegate = AppDelegate().sharedInstance()
        if(appDelegate.isGoToPush == true){
            if let remoteNotificationData = appDelegate.remoteNotificationData{
                let dataType = Util.getPushNotificationType(remoteNotificationData: remoteNotificationData)
                if(dataType == .newCommentTask || dataType == .assignTask){
                    if let roomId = remoteNotificationData[SettingString.RoomId] as? String{
                        let messageId = remoteNotificationData[SettingString.MessageId] as? String
                        let messageName = remoteNotificationData[SettingString.RoomName] as? String ?? ""
                        let userId = remoteNotificationData[SettingString.UserId] as? String ?? ""
                        var task = Task(taskId: roomId, name: messageName, dataType: .processing)
                        task.assignee = userId
                        self.gotoTaskDetail(task: task, view: tbBacklog, index: -1, messageId: messageId)
                    }
                }
            }
        }
    }
    
    func trackOpenBacklog(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        Tracker.trackOpenBacklog(userId: userId)
    }
    
    func addNotification(){
        addNotification_Keyboard()
        addNotification_UpdateUserTeamInfo()
    }
    
    func addNotification_Keyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardBounds = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double{
                //
                if let inputTaskInfoView = inputTaskInfoView{
                    if((inputTaskInfoView.isFirstResponder() == true)){
                        inputTaskInfoView.showPicker(keyboardBounds: keyboardBounds, duration: NSNumber(value: duration), isDismiss: true, bindAddTask:true, bindAddAgenda: "",bindUpdateTask: "")
                    }
                }
                else{
                    if let searchBar = searchBar{
                        if((searchBar.isFirstResponder == true)){
                            activate(searchBar, true)
                        }
                    }
                }
            }
            
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
    }
    
    func addNotification_UpdateUserTeamInfo(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(teamAgenda_UpdateInfo(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TEAM_USER_INFO),
                                               object: nil)
    }
    
    //Notification for task detail
    @objc func teamAgenda_UpdateInfo(notification: Notification) {
        reloadAllVisibleCell()
    }
    
    func reloadAllVisibleCell(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TaskItemViewCell{
                taskCell.loadAssigneeAvatar()
            }
        }
    }
    
    func removeNotification_Keyboard(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeNotification(){
        removeNotification_Keyboard()
        //
        NotificationCenter.default.removeObserver(self)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        //Close Button
//        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
//        let xClose : CGFloat = pClose
//        let yClose : CGFloat = yContentHeaderView
//        let hClose : CGFloat = hContentHeaderView
//        let wClose : CGFloat = hClose
//
//        let btnClose = IconButton.init(type: .custom)
//        btnClose.accessibilityIdentifier = AccessiblityId.BTN_BACK
//        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
//        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
//        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
//        navigationView?.addSubview(btnClose)
        
        //Action Button
        let pFilter : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yFilter : CGFloat = yContentHeaderView
        let wFilter : CGFloat = hContentHeaderView
        let hFilter = wFilter
        let xFilter = wHeaderView - wFilter - pFilter
        
        let btnFilter = IconButton.init(type: .custom)
        btnFilter.accessibilityIdentifier = AccessiblityId.BTN_FILTER
        btnFilter.frame = CGRect(x: xFilter, y: yFilter, width: wFilter, height: hFilter)
        btnFilter.setImage(UIImage(named: "ic_menu_filter"), for: .normal)
        btnFilter.addTarget(self, action: #selector(filterAction), for: .touchUpInside)
        navigationView?.addSubview(btnFilter)
        
        let wSearch : CGFloat = wFilter
        let hSearch : CGFloat = wSearch
        let xSearch : CGFloat = xFilter - wSearch
        let ySearch : CGFloat = yFilter
        
        let btnSearch = IconButton.init(type: .custom)
        btnSearch.accessibilityIdentifier = AccessiblityId.BTN_SEARCH
        btnSearch.frame = CGRect(x: xSearch, y: ySearch, width: wSearch, height: hSearch)
        btnSearch.setImage(UIImage(named: "ic_menu_search"), for: .normal)
        btnSearch.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        navigationView?.addSubview(btnSearch)
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
//        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let xTitleHeader : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = xSearch - xTitleHeader - pTitleHeader
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("backlog", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
//        navigationView?.showHideTitleNavigation(isShow: true)
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = AppDevice.ScreenComponent.Width
        let hTableBacklog : CGFloat = AppDevice.ScreenComponent.Height - AppDevice.ScreenComponent.TabbarHeight - yTableBacklog
        tbBacklog.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.accessibilityIdentifier = AccessiblityId.LIST_BACKLOG
        tbBacklog.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbBacklog.register(TaskItemViewCell.self, forCellReuseIdentifier: taskCellIdentifier)
        tbBacklog.register(TaskEmptyViewCell.self, forCellReuseIdentifier: emptyTaskCellIdentifier)
        self.view.addSubview(tbBacklog)
        
        //
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableBacklog
        tbBacklogHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: 0))
        tbBacklogHeaderView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 4.0
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("backlog", comment: "")
        tbBacklogHeaderView?.addSubview(lbTitleScreen)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        let xAddTaskView : CGFloat = 0
        let yAddTaskView : CGFloat = yViewScreen
        let wAddTaskView : CGFloat = AppDevice.ScreenComponent.Width
        let hAddTaskView : CGFloat = AppDevice.TableViewRowSize.AddNewTaskHeight
        let pBottomAddTaskView : CGFloat = 0
        //
        let addTaskView = TaskAddNewView(frame: CGRect(x: xAddTaskView, y: yAddTaskView, width: wAddTaskView, height: hAddTaskView))
        addTaskView.setValueForTitle(title: NSLocalizedString("create_a_task_for_your_team", comment: ""))
        tbBacklogHeaderView?.addSubview(addTaskView)
        //Tap Add Task Event
        let btnAddTaskView = RaisedButton.init(type: .custom)
        btnAddTaskView.accessibilityIdentifier = AccessiblityId.BTN_ADD_TASK
        btnAddTaskView.frame = CGRect(x: 0, y: 0, width: wAddTaskView, height: hAddTaskView)
        btnAddTaskView.pulseColor = AppColor.MicsColors.LINE_COLOR
        btnAddTaskView.backgroundColor = .clear
        btnAddTaskView.addTarget(self, action: #selector(didTapAddNewTask), for: .touchUpInside)
        addTaskView.addSubview(btnAddTaskView)
        
        yViewScreen += hAddTaskView
        yViewScreen += pBottomAddTaskView
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbBacklogHeaderView?.frame.size.height = hHeaderTableView
        tbBacklog.tableHeaderView = tbBacklogHeaderView!
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    func setupUISearchBar(){
        let hSearchView : CGFloat = 44
        let hStatusBar : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let pSearchBarView : CGFloat = 10
        let hSearchBarView : CGFloat = hSearchView + hStatusBar + pSearchBarView
        
        searchBarView = UIView(frame: CGRect(x: 0, y: -hSearchBarView, width: AppDevice.ScreenComponent.Width, height: hSearchBarView))
        searchBarView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        searchBar = UISearchBar(frame: CGRect(x: 0, y: hStatusBar, width: searchBarView!.frame.size.width, height: hSearchView))
        searchBar?.accessibilityIdentifier = AccessiblityId.TF_SEARCH
        searchBar?.autocapitalizationType = .none
        searchBar?.searchBarStyle = .minimal
        searchBar?.delegate = self
        searchBar?.placeholder = NSLocalizedString("search", comment: "")
        searchBar?.tintColor = AppColor.NormalColors.BLUE_COLOR
        searchBar?.showsCancelButton = true
        searchBarView?.addSubview(searchBar!)
        
        viewOverlay = UIView(frame: CGRect(x: 0.0, y: hSearchBarView, width: AppDevice.ScreenComponent.Width, height: AppDevice.ScreenComponent.Height))
        viewOverlay?.accessibilityIdentifier = AccessiblityId.VIEW_OVERPLAY
        viewOverlay?.backgroundColor = AppColor.NormalColors.BLACK_COLOR
        viewOverlay?.alpha = 0
        viewOverlay?.autoresizingMask = .flexibleWidth
        viewOverlay?.autoresizingMask = .flexibleHeight
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapOverplaySearch))
        viewOverlay?.addGestureRecognizer(tapGesture)
        
        self.view.addSubview(searchBarView!)
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func searchAction(){
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseIn], animations: {
            let hSearchView : CGFloat = 44
            let hStatusBar : CGFloat = AppDevice.ScreenComponent.StatusHeight
            let pSearchBarView : CGFloat = 10
            let hSearchBarView : CGFloat = hSearchView + hStatusBar + pSearchBarView
            
            self.searchBarView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: hSearchBarView)
            self.activate(self.searchBar!,true)
            self.searchBar?.becomeFirstResponder()
        }, completion: nil)
    }
    
    @objc func filterAction(){
        let actionSheet = JCActionSheet(title: NSLocalizedString("filter_by", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("cancel", comment: ""), cancelTextColor: AppColor.NormalColors.BLACK_COLOR, destructiveButtonTitle: nil, otherButtonTitles:  [Constant.TaskFilter.allTalk[Constant.keyValue] as! String,Constant.TaskFilter.myTask[Constant.keyValue] as! String,Constant.TaskFilter.priorityTask[Constant.keyValue] as! String,Constant.TaskFilter.completedTask[Constant.keyValue] as! String], textColor: AppColor.NormalColors.BLUE_COLOR, checkedButtonIndex: iFilter, checkedButtonTextColor: AppColor.NormalColors.BLUE_COLOR)
        actionSheet.view.accessibilityIdentifier = AccessiblityId.SHEET_FILTER
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func checkPermission(_ task: Task) -> Bool{
        var isEditPermission : Bool = false
        //
        let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectOwnedId = AppSettings.sharedSingleton.project?.owned.userId ?? ""
        if(myUserId.count > 0){
            if(myUserId == projectOwnedId || myUserId == task.assignee){
                isEditPermission = true
            }
        }
        return isEditPermission
    }
    
    func stopProcessingForAllVisibleCell(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TaskItemViewCell{
                taskCell.stopProcessingFinishTask()
            }
        }
    }
    
    func getVisibleCellFromAgendaTask(taskId: String) -> TaskItemViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : TaskItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TaskItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromAgendaTask(taskId: taskId){
            visibleCell.stopProcessingFinishTask()
        }
    }
    
    func stopProcessingNewTaskForAllVisibleCell(){
        uploadingTasks.removeAll()
        subjectUploadingTasks.onNext(uploadingTasks)
    }
    
    func trackFinishTaskInCollaborate(taskId: String,fromCollaborate : Bool){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        if(fromCollaborate == true){
            Tracker.trackFinishTaskInCollaborate(taskid: taskId, userId: userId, wip: true)
        }
        else{
            Tracker.trackFinishTaskInFocus(taskid: taskId, userId: userId, wip: true)
        }
    }
    
    func updateTaskFinishChanged(task: Task){
        let agendaTaskId : String = task.checkIsTodayAgenda()
        //
        let taskId = task.taskId
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        var taskAPIViewModel : TaskAPIViewModel?
        // Update task finish
        if(agendaTaskId.count > 0){
            let finished = task.completed == true ? Constant.TaskStatus.kTaskStatusUncompleted.rawValue : Constant.TaskStatus.kTaskStatusCompleted.rawValue
            // Update task finish
            taskAPIViewModel = TaskAPIViewModel(
                inputUpdateTaskAgendaStatus: (
                    taskId: taskId,
                    projectId: projectId,
                    agendaId: agendaTaskId,
                    status: BehaviorSubject<Int>(value: finished),
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    controller: taskController
                )
            )
        }
        else{
            let finished = !task.completed
            //
            taskAPIViewModel = TaskAPIViewModel(
                inputUpdateTaskFinished: (
                    taskId: taskId,
                    projectId: projectId,
                    finished: BehaviorSubject(value: finished).asObservable(),
                    loginTaps: BehaviorSubject(value: ()).asObservable(),
                    controller: taskController
                )
            )
        }
        taskAPIViewModel?.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            self?.view.makeToast(NSLocalizedString("update_task_successfully", comment: ""), duration: 1.5, position: .center)
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                        }
                        self?.trackFinishTaskInCollaborate(taskId: reponseAPI.data, fromCollaborate: false)
                    }
                    //
                    self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func addNewTask(name: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let viewModelCreate = TaskAPIViewModel(
            inputCreateTask: (
                projectId: projectId,
                name: BehaviorSubject<String>(value: name),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: taskController
            )
        )
        
        viewModelCreate.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    //
                    self?.stopProcessingNewTaskForAllVisibleCell()
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.makeToast(message:NSLocalizedString("add_task_successfully", comment: ""), duration: 0.5, position: .top)
                        }
                        else{
                            var message = NSLocalizedString("add_task_unsuccessfully", comment: "")
                            if(reponseAPI.code != 0){
                                if(reponseAPI.message.count > 0){
                                    message = reponseAPI.message
                                }
                            }
                            self?.makeToast(message:message, duration: 0.5, position: .top)
                        }
                    }
                    //
                    self?.stopProcessingNewTaskForAllVisibleCell()
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func bindToViewModel(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        let taskController = TaskController.sharedAPI
        self.viewModel = TaskViewModel(
            inputSearchAndSort: (
                search: searchBar!.rx.text.orEmpty.debounce(0.5, scheduler: MainScheduler.instance)
                    .distinctUntilChanged().asObservable(),
                sort:subjectFilter.asObservable(),
                taskController: taskController,
                dataSource : subjectUploadingTasks.asObservable(),
                userId: userId,
                projectId: projectId,
                duedate: nil
            )
        )
        
        self.viewModel?.tasks?.bind(to: tbBacklog.rx.items){[weak self] (table, index, element) -> UITableViewCell in
            guard let strongSelf = self else { return UITableViewCell()}
            if element.taskId.count == 0 && element.name.count == 0 {
                guard let cell = table.dequeueReusableCell(withIdentifier:  strongSelf.emptyTaskCellIdentifier, for: IndexPath.init(row: index, section: 0)) as? TaskEmptyViewCell else {
                    return TaskEmptyViewCell()
                }
                //
                var strSearch = strongSelf.searchBar?.text ?? ""
                if(strSearch.count == 0){
                    strSearch = Util.getFilterTitle(filter: strongSelf.iFilter)
                }
                //
                cell.setValueForCell(title: strSearch)
                return cell
            }else{
                guard let cell = table.dequeueReusableCell(withIdentifier:  strongSelf.taskCellIdentifier, for: IndexPath.init(row: index, section: 0)) as? TaskItemViewCell else {
                    return TaskItemViewCell()
                }
                cell.selectionStyle = .none
                cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
//                let isCanFinish = Util.checkPermissionEditTask(element.assignee)
                let isCanFinish = false
                //Check valid search
                var strSearch = strongSelf.searchBar?.text ?? ""
                if(Util.validateTaskName(strSearch).isValid == false){
                    strSearch = ""
                }
                cell.setValueForCell(task: element, strSearch: strSearch, index:index, isCanFinished: isCanFinish)
                //
                cell.btnCheckbox?.tag = index
                cell.btnCheckbox?.addTarget(self, action: #selector(strongSelf.checkboxAction(sender:)), for: .touchUpInside)
                
                return cell
            }
            
            }.disposed(by: disposeBag)
        
        tbBacklog.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                DispatchQueue.main.async {
                    guard let taskCell = self?.tbBacklog.cellForRow(at: indexPath) as? TaskItemViewCell, let task = taskCell.task  else { return }
                    //
                    if(task.dataType == .normal){
                        self?.gotoTaskDetail(task: task, view: taskCell.contentView, index: indexPath.row,messageId: nil)
                    }
                }
            }).disposed(by: disposeBag)
        
        
        tbBacklog.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
            }.disposed(by: disposeBag)
    }
    
    func gotoTaskDetail(task: Task, view: UIView, index: Int, messageId: String?){
        var heroId = ""
        var needLoadDetail = false
        if(index == -1){
            heroId = "tableview"
            needLoadDetail = true
        }
        else{
            heroId = "cell\(index)"
        }
        view.hero.id = heroId
        //
        let taskDetailView = TaskDetailViewController(nibName: nil, bundle: nil)
        taskDetailView.isFromBacklog = true
        taskDetailView.taskDetailData = task
        taskDetailView.needLoadDetail = needLoadDetail
        taskDetailView.messageId = messageId
        taskDetailView.hero.isEnabled = true
        taskDetailView.tbBacklog.hero.id = heroId
        taskDetailView.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        
        self.navigationController?.present(taskDetailView, animated: true, completion: nil)
    }
    
    @objc func checkboxAction(sender: UIButton){
        let index = sender.tag
        guard let taskItemCell = tbBacklog.cellForRow(at: IndexPath(row: index, section: 0)) as? TaskItemViewCell, let task = taskItemCell.task  else { return}
        if(task.valid() == true){
            taskItemCell.startProcessingFinishTask()
            //
            self.updateTaskFinishChanged(task: task)
        }
    }
    
    @objc func didTapAddNewTask(){
        let content = ""
        showAddTaskView(content: content)
    }
    
    @objc func didTapOverplaySearch(){
        searchBarCancelButtonClicked(searchBar!)
    }
    
    func filterResultsToOriginal(){
        
    }
    
    func filterResultsWithSearchTerm(_ searchText : String){
        
    }
    
    func activate(_ searchBar: UISearchBar,_ active: Bool){
        self.tbBacklog.allowsSelection = !active
        self.tbBacklog.isScrollEnabled = !active
        if (!active) {
            viewOverlay?.removeFromSuperview()
        } else{
            if((viewOverlay?.superview) == nil){
                viewOverlay?.alpha = 0
                self.view.addSubview(viewOverlay!)
                UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveLinear], animations: {
                    self.viewOverlay?.alpha = 0.6
                }, completion: nil)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if (searchText.count == 0) {
            filterResultsToOriginal()
            activate(searchBar, true)
        }
        else {
            filterResultsWithSearchTerm(searchText)
            activate(searchBar, false)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
            let hSearchView : CGFloat = 44
            let hStatusBar : CGFloat = AppDevice.ScreenComponent.StatusHeight
            let pSearchBarView : CGFloat = 10
            let hSearchBarView : CGFloat = hSearchView + hStatusBar + pSearchBarView
            //
            self.searchBar?.text = ""
            self.filterResultsToOriginal()
            self.searchBarView?.frame = CGRect(x: 0, y: -hSearchBarView , width: self.view.frame.size.width, height: hSearchBarView)
            self.activate(searchBar,false)
            self.searchBar?.resignFirstResponder()
        }, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        activate(searchBar, false)
        self.searchBar?.resignFirstResponder()
    }
    
    func actionSheet(_ actionSheet: JCActionSheet, clickedButtonAt buttonIndex: Int) {
        //        self.currentCheckedIndex = buttonIndex;
        //        self.viewModel!.getAgendaByMe().asObservable().
        //            .disposed(by: disposeBag)
        if(buttonIndex != iFilter){
            iFilter = buttonIndex
            self.subjectFilter.onNext(iFilter)
        }
    }
    
    func actionSheetCancel(_ actionSheet: JCActionSheet) {
        //do something with cancel action
    }
    
    func actionSheetDetructive(_ actionSheet: JCActionSheet) {
        //do something with detructive action
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //        let task = arrayTask[indexPath.row]
    }
    
    func showAddTaskView(content : String){
        if(inputTaskInfoView == nil){
            inputTaskInfoView = InputTaskInfoView(frame: CGRect(x: 0, y: 0, width: AppDevice.ScreenComponent.Width, height: AppDevice.ScreenComponent.Height))
            inputTaskInfoView?.delegate = self
        }
        //
        self.view.window?.addSubview(inputTaskInfoView!)
        inputTaskInfoView?.setContent(content:content)
        inputTaskInfoView?.becomeResponder()
    }
    
    func inputTaskInfoViewAdd(_ inputTaskInfoView: InputTaskInfoView, addNew withName: String) {
        let task = Task(taskId: "1", name: withName, dataType: .processing)
        //
        uploadingTasks.insert(task, at: 0)
        subjectUploadingTasks.onNext(uploadingTasks)
        //
        addNewTask(name: withName)
    }
    
    func inputTaskInfoViewUpdate(_ inputTaskInfoView: InputTaskInfoView, update withName: String, taskId: String){
        
    }
    
    func inputTaskInfoViewCancel(_ inputTaskInfoView: InputTaskInfoView, tapCancel isSave: Bool) {
        
    }
    
    deinit {
        // Release all resources - perform the deinitialization
        removeNotification()
    }
    
}
