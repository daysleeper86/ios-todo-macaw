//
//  FocusModeEditorViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/15/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Moya

class FocusModeEditorViewController: BaseViewController,UITableViewDelegate,SwipeTableViewCellDelegate {
    //Views
    var tbBacklog : UITableView = UITableView(frame: .zero)
    var viewLine : UIView?
    //Data
    let needtoDoneSection : Int = 1
    let doneSection : Int = 2
    var agendaId : String = ""
    var agendaViewModel : AgendaViewModel?
    var taskViewModel : TaskViewModel?
    var dataSource: RxTableViewSectionedReloadDataSource<TaskSection>?
    //Constants
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    var subjectDate = BehaviorSubject<Date>(value: AppSettings.sharedSingleton.agenda_today?.date ?? Date())
    static let emptyTaskWIPCellIdentifier : String = "emptyTaskWIPCellIdentifier"
    static let emptyTaskNeedDoneCellIdentifier : String = "emptyTaskNeedDoneCellIdentifier"
    static let taskNotActionCellIdentifier : String = "taskNotActionCellIdentifier"
    
    
    //Callback
    var exitCallback: ((_ isExit: Bool) -> Void)?
    var changeCallback: ((_ isOn: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        changeThemeData()
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func initView(){
        self.view.isOpaque = false
        //        self.view.isUserInteractionEnabled = false
        self.view.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //Background View
        let yScrollView : CGFloat = AppDevice.IPhoneType.isIphoneX ? AppDevice.ScreenComponent.ItemPadding : 0
        let hScrollView : CGFloat = AppDevice.ScreenComponent.Height - yScrollView
        let bgView = UIView(frame: CGRect(x: 0, y: yScrollView, width: AppDevice.ScreenComponent.Width, height: hScrollView))
        bgView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.view.addSubview(bgView)
        
//        var yHeader : CGFloat = AppDevice.ScreenComponent.StatusHeight
        var yHeader : CGFloat = 0
        let hHeader : CGFloat = 44
        
        let headerView = UIView(frame: CGRect(x: 0, y: yHeader, width: bgView.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        bgView.addSubview(headerView)
        
        let pButtonExit : CGFloat = 8
        let wButtonExit : CGFloat = 60
        let hButtonExit = headerView.frame.size.height
//        let xButtonExit = headerView.frame.size.width - wButtonExit - pButtonExit
        let xButtonExit = pButtonExit
        let yButtonExit : CGFloat = 0
        
        
        let btnExit = UIButton.init(type: .custom)
        btnExit.frame = CGRect(x: xButtonExit, y: yButtonExit, width: wButtonExit, height: hButtonExit)
        btnExit.addTarget(self, action: #selector(exitAction), for: .touchUpInside)
        headerView.addSubview(btnExit)
        
        let pSwitch : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wSwitch : CGFloat = AppDevice.SubViewFrame.SwitchWidth
        let hSwitch : CGFloat = AppDevice.SubViewFrame.SwitchHeight
        let xSwitch : CGFloat = headerView.frame.size.width - wSwitch - pSwitch
        let ySwitch : CGFloat = 0
        
        let btnDarkMode = UIButton.init(type: .custom)
        btnDarkMode.frame = CGRect(x: xSwitch, y: ySwitch, width: wSwitch, height: hSwitch)
        btnDarkMode.addTarget(self, action: #selector(darkModeAction), for: .touchUpInside)
        headerView.addSubview(btnDarkMode)
        
        
        //
        yHeader += hHeader
        //
        let xViewLine : CGFloat = 0
        let yViewLine : CGFloat = yHeader
        let wViewLine : CGFloat = bgView.frame.size.width
        let hViewLine : CGFloat = 1
        
        viewLine = UIView(frame: CGRect(x: xViewLine, y: yViewLine, width: wViewLine, height: hViewLine))
        bgView.addSubview(viewLine!)
        
        yHeader += hViewLine
        //
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = bgView.frame.size.width
        let hTableBacklog : CGFloat = bgView.frame.size.height - yTableBacklog
        tbBacklog.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbBacklog.register(TaskWIPEmptyViewCell.self, forCellReuseIdentifier: FocusModeEditorViewController.emptyTaskWIPCellIdentifier)
        tbBacklog.register(TaskNeedDoneEmptyViewCell.self, forCellReuseIdentifier: FocusModeEditorViewController.emptyTaskNeedDoneCellIdentifier)
        tbBacklog.register(TeamAgendaTaskNotActionItemViewCell.self, forCellReuseIdentifier: FocusModeEditorViewController.taskNotActionCellIdentifier)
        tbBacklog.register(TeamAgendaTaskItemViewCell.self, forCellReuseIdentifier: TeamAgendaPlanningBacklogViewController.taskCellIdentifier)
        
        
        bgView.addSubview(tbBacklog)
        
    }
    
    @objc func changeValue(_ sender: UISwitch) {
        let isOn = sender.isOn
        self.changeCallback?(isOn)
    }
    
    func changeThemeData(){
        //
        var backgroundViewColor : UIColor!
        var lineColor : UIColor!
        //
        if(AppSettings.sharedSingleton.isDarkMode == true){
            backgroundViewColor = AppColor.MicsColors.DARK_MENU_COLOR
            lineColor = AppColor.MicsColors.LINE_COLOR_DARK
        }
        else{
            backgroundViewColor = AppColor.NormalColors.WHITE_COLOR
            lineColor = AppColor.MicsColors.LINE_COLOR
        }
        //
        viewLine?.backgroundColor = lineColor
        tbBacklog.backgroundColor = backgroundViewColor
        tbBacklog.reloadData()
    }
    
    @objc func exitAction(){
        self.dismiss(animated: true, completion: {
            self.exitCallback?(true)
        })
    }
    
    @objc func darkModeAction(){
        self.changeCallback?(true)
    }
    
    @objc func exitAction1(){
        dismiss(animated: true,completion: nil)
    }
    
    
    func bindToViewModel(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        func getOnlyIdFromTask(talks : [Task]){
            var ids : [String] = []
            for i in 0..<talks.count{
                let task = talks[i]
                ids.append(task.taskId)
            }
        }
        
        
        let agendaController = AgendaController.sharedAPI
        let taskController = TaskController.sharedAPI
        
        self.agendaViewModel = AgendaViewModel(
            inputUserId: (
                agendaController: agendaController,
                userId: subjectUser.asObservable(),
                projectId:projectId,
                duedate: subjectDate.asObservable(),
                needSubscrible: true
            )
        )
        
        let agendaData = self.agendaViewModel?.agendas?
            .share(replay:1)
        agendaData?.subscribe({ [weak self] agendas  in
            guard let strongAgendas = agendas.element else { return }
            if(strongAgendas.count > 0){
                let agenda = strongAgendas[0]
                if(agenda.valid() == true){
                    self?.agendaId = agenda.agendaId
                }
            }
        })
            .disposed(by: disposeBag)
        
        let taskInfo =
            agendaData!
                .flatMapLatest { agendas -> Observable<[String : Any]> in
                    var taskInfo : [String : Any] = [:]
                    if(agendas.count > 0){
                        let agenda = agendas[0]
                        if(agenda.empty() == true){
                            taskInfo = [
                                "isEmpty": true,
                                "taskId": [],
                                "userId": "",
                                "findType": Constant.FindType.findIn,
                            ]
                        }
                        else{
                            var ids : [String] = []
                            for task in agenda.tasks {
                                ids.append(task.taskId)
                            }
                            let userId = agenda.owned.userId
                            taskInfo = [
                                "taskId": ids,
                                "userId": userId,
                                "findType": Constant.FindType.findIn,
                            ]
                            //
                            if(ids.count == 0){
                                taskInfo["isEmpty"] = true
                            }
                        }
                    }
                    return Observable.just(taskInfo)
        }
        
        
        self.taskViewModel = TaskViewModel(
            inputTaskAgenda: (
                taskInfo: taskInfo,
                taskController: taskController,
                dataSource : Observable.just([]),
                projectId: projectId,
                findOther: false,
                needEmptyData:true
            )
        )
        
        
        let tasksAndSection = self.taskViewModel!.tasks!
            .flatMapLatest{ tasks -> Observable<[TaskSection]> in
                var workInProgress : [Task] = []
                var needToDone : [Task] = []
                var hasDone : [Task] = []
                if(TaskService.checkHolderTask(tasks: tasks) == true){
                    let firstTask = tasks[0]
                    workInProgress.append(firstTask)
                    //
                    for task in tasks{
                        hasDone.append(task)
                        needToDone.append(task)
                    }
                }
                else{
                    for task in tasks{
                        if(task.dataType == .normal){
                            if(task.completed == true){
                                hasDone.append(task)
                            }
                            else{
                                if(task.wip == false){
                                    needToDone.append(task)
                                }
                            }
                            //
                            if(task.wip == true){
                                workInProgress.append(task)
                            }
                        }
                    }
                    //
                    if(workInProgress.count == 0){
                        let task = TaskService.createEmptyTask()
                        workInProgress.append(task)
                    }
                }
                //
                var titleNeedToDone = NSLocalizedString("need_to_be_done", comment: "")
                titleNeedToDone = titleNeedToDone + " (" + String(needToDone.count) + ")"
                if(needToDone.count == 0){
                    let task = TaskService.createEmptyTask()
                    needToDone.append(task)
                }
                //
                var titleHasDone = NSLocalizedString("done", comment: "")
                if(hasDone.count > 0){
                    titleHasDone = titleHasDone + " (" + String(hasDone.count) + ")"
                }
                //
                return Observable.just(
                    [
                        TaskSection(header: NSLocalizedString("work_in_progress", comment: "").uppercased(), items: workInProgress),
                        TaskSection(header: titleNeedToDone.uppercased(), items: needToDone),
                        TaskSection(header: titleHasDone.uppercased(), items: hasDone)
                    ])
        }
        
        
        //Datasource
        let dataSource = RxTableViewSectionedReloadDataSource<TaskSection>(
            configureCell: {(_ ,table, indexPath, element) in
                if(element.dataType == .empty){
                    if(indexPath.section == self.needtoDoneSection){
                        guard let cell = table.dequeueReusableCell(withIdentifier:  FocusModeEditorViewController.emptyTaskNeedDoneCellIdentifier, for: indexPath) as? TaskNeedDoneEmptyViewCell else {
                            return TaskNeedDoneEmptyViewCell()
                        }
                        let itemWidth = table.frame.size.width
                        let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                        cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                        cell.selectionStyle = .none
                        //
                        cell.setDarkModeView(AppSettings.sharedSingleton.isDarkMode)
                        //
                        return cell
                    }
                    else{
                        guard let cell = table.dequeueReusableCell(withIdentifier:  FocusModeEditorViewController.emptyTaskWIPCellIdentifier, for: indexPath) as? TaskWIPEmptyViewCell else {
                            return TaskWIPEmptyViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.delegate = self
                        //
                        cell.setDarkModeView(AppSettings.sharedSingleton.isDarkMode)
                        //
                        return cell
                    }
                }
                else{
                    if(element.completed == false){
                        guard let cell = table.dequeueReusableCell(withIdentifier:  FocusModeEditorViewController.taskNotActionCellIdentifier, for: indexPath) as? TeamAgendaTaskNotActionItemViewCell else {
                            return TeamAgendaTaskNotActionItemViewCell()
                        }
                        //
                        let itemWidth = table.frame.size.width
                        let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                        cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                        cell.selectionStyle = .none
                        //
                        cell.delegate = self
                        
                        cell.setValueForCell(task: element)
                        //
                        var titleColor : UIColor!
                        var darkMode = false
                        if(AppSettings.sharedSingleton.isDarkMode == true){
                            darkMode = true
                            //
                            if(indexPath.section == 0){
                                titleColor = AppColor.NormalColors.WHITE_COLOR
                            }
                            else{
                                titleColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
                            }
                        }
                        else{
                            titleColor = AppColor.NormalColors.BLACK_COLOR
                        }
                        cell.setDarkModeView(darkMode, titleColor:titleColor)
                        //
                        return cell
                    }
                    else{
                        guard let cell = table.dequeueReusableCell(withIdentifier:  TeamAgendaPlanningBacklogViewController.taskCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
                            return TeamAgendaTaskItemViewCell()
                        }
                        //
                        let itemWidth = table.frame.size.width
                        let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                        
                        cell.initView(itemWidth: itemWidth,contentPadding : contentPadding)
                        cell.selectionStyle = .none
                        cell.setValueForCell(task: element, isCanFinished: true)
                        //
                        cell.btnCheckbox?.tag = indexPath.row
                        cell.btnCheckbox?.addTarget(self, action: #selector(self.checkboxAction(sender:)), for: .touchUpInside)
                        //
                        var cellColor : UIColor!
                        if(AppSettings.sharedSingleton.isDarkMode == true){
                            cellColor = AppColor.MicsColors.DARK_MENU_COLOR
                        }
                        else{
                            cellColor = AppColor.NormalColors.WHITE_COLOR
                        }
                        cell.setBackGroundColor(color: cellColor)
                        //
                        return cell
                    }
                }
        },
            titleForHeaderInSection: { dataSource, sectionIndex in
                return dataSource.sectionModels[sectionIndex].header
            }
        )
        
        self.dataSource = dataSource
        
        tasksAndSection
            .bind(to: tbBacklog.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        
        tbBacklog.rx
            .modelSelected(Task.self)
            .subscribe(onNext:  { element in
                if(element.taskId.count > 0 && element.name.count > 0){
                    let taskDetailView = TaskDetailViewController(nibName: nil, bundle: nil)
                    taskDetailView.taskDetailData = element
                    self.navigationController?.pushViewController(taskDetailView, animated: true)
                }
            })
            .disposed(by: disposeBag)
        
        
        tbBacklog.rx
            .setDelegate(self)
            .disposed(by : disposeBag)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionData = self.dataSource?[section] else { return nil }
        var headerView : UIView?
        var isRenderHeader : Bool = false
        if(section == needtoDoneSection){
            isRenderHeader = true
        }
        else{
            if(sectionData.items.count > 0){
                isRenderHeader = true
            }
        }
        if (isRenderHeader == true){
            let wHeader : CGFloat = tableView.frame.size.width
            let hHeader : CGFloat = 48
            
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: wHeader, height: hHeader))
            
            //
            let pTopTitleName : CGFloat = 45
            let xTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
            let wTitleName : CGFloat = wHeader - (xTitleName * 2)
            let hTitleName : CGFloat = 36
            
            let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: pTopTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.text = sectionData.header
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
            headerView?.addSubview(lbTitleName)
            
            //
            var backgroundViewColor : UIColor!
            var titleHeaderColor : UIColor!
            if(AppSettings.sharedSingleton.isDarkMode == true){
                backgroundViewColor = AppColor.MicsColors.DARK_MENU_COLOR
                //
                titleHeaderColor = AppColor.NormalColors.BLUE_COLOR
            }
            else{
                backgroundViewColor = AppColor.NormalColors.WHITE_COLOR
                titleHeaderColor = AppColor.NormalColors.DARK_COLOR
            }
            headerView?.backgroundColor = backgroundViewColor
            lbTitleName.textColor = titleHeaderColor
        }
        //
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var heightRow : CGFloat = 0
        guard let sectionData = self.dataSource?[indexPath.section] else { return heightRow }
        let element = sectionData.items[indexPath.row]
        if(element.taskId.count == 0 && element.name.count == 0){
            heightRow = AppDevice.TableViewRowSize.TaskWIPHeight
        }
        else{
            heightRow = AppDevice.TableViewRowSize.TaskHeight
        }
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var hHeader : CGFloat = 0
        guard let sectionData = self.dataSource?[section] else { return hHeader }
        if(sectionData.items.count > 0){
            hHeader = 45 + 36
        }
        return hHeader
    }
    
    func getVisibleCellFromTask(taskId: String) -> TeamAgendaTaskNotActionItemViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : TeamAgendaTaskNotActionItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromTask(taskId: taskId){
            visibleCell.stopProcessing()
        }
    }
    
    func stopProcessingForAllVisibleCell(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                taskCell.stopProcessing()
            }
        }
    }
    
    func updateTaskFinishChanged(taskId: String, completed: Bool){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        // Update task finish
        let finished = completed == true ? Constant.TaskStatus.kTaskStatusCompleted.rawValue : Constant.TaskStatus.kTaskStatusUncompleted.rawValue
        
        let viewModelFinished = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaId,
                status: BehaviorSubject<Int>(value: finished),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: taskController
            )
        )
        //
        viewModelFinished.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                        }
                        //
                        self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func updateWIPTask(taskId: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let taskViewModel = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaId,
                status: BehaviorSubject<Int>(value: Constant.TaskStatus.kTaskStatusWIP.rawValue),
                loginTaps: BehaviorSubject<Void>(value: ()),
                controller: taskController
            )
        )
        //
        
        taskViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
//                            DispatchQueue.main.async {
//                                self?.view.makeToast(NSLocalizedString("add_task_to_agenda_successfully", comment: ""), duration: 1.5, position: .center, completion: { didTap in
//                                })
//                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self?.makeToast(message:NSLocalizedString("add_task_to_agenda_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                            }
                        }
                        //
                        self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        guard let sectionData = self.dataSource?[indexPath.section] else { return nil }
        let element = sectionData.items[indexPath.row]
        
        var workAction : SwipeAction?
        if(element.wip == false){
            workAction = SwipeAction(style: .default, title: NSLocalizedString("working_on_it", comment: "")) { [weak self] action, indexPath in
                // handle action by updating model with deletion
                guard let strongSelf = self else { return }
                if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                    taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR)
                    //
                    taskCell.hideSwipe(animated: true)
                    //
                    strongSelf.updateWIPTask(taskId: task.taskId)
                }
            }
        }
        else{
            workAction = SwipeAction(style: .default, title: NSLocalizedString("mask_as_done", comment: "")) { [weak self] action, indexPath in
                // handle action by updating model with deletion
                guard let strongSelf = self else { return }
                if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                    taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR)
                    //
                    taskCell.hideSwipe(animated: true)
                    //
                    strongSelf.updateTaskFinishChanged(taskId: task.taskId,completed: true)
                }
            }
        }
        //
        workAction!.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        workAction!.textColor = AppColor.NormalColors.WHITE_COLOR
        workAction!.backgroundColor = AppColor.NormalColors.BLUE_COLOR

        return [workAction!]
    }

    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 170
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        //
        var swipeColor : UIColor!
        if(AppSettings.sharedSingleton.isDarkMode == true){
            swipeColor = AppColor.MicsColors.DISABLE_DARK_COLOR
        }
        else{
            swipeColor = AppColor.MicsColors.DISABLE_COLOR
        }
        cell.setBackGroundColor(color: swipeColor)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        //
        var swipeColor : UIColor!
        if(AppSettings.sharedSingleton.isDarkMode == true){
            swipeColor = AppColor.MicsColors.DARK_MENU_COLOR
        }
        else{
            swipeColor = AppColor.NormalColors.WHITE_COLOR
        }
        cell.setBackGroundColor(color: swipeColor)
    }
    
    @objc func checkboxAction(sender: UIButton){
        let index = sender.tag
        //
        let indexPath = IndexPath(row: index, section: doneSection)
        if let taskCell = tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskItemViewCell, let task = taskCell.task{
            taskCell.startProcessingFinishTask()
            //
            updateTaskFinishChanged(taskId: task.taskId,completed: false)
        }
    }
    
    deinit {
        // Release all resources
        // perform the deinitialization
        exitCallback = nil
        changeCallback = nil
    }
    
}

extension FocusModeEditorViewController : TaskWIPEmptyViewCellDelegate{
    func taskWIPEmptyViewCellNextAction(_ taskWIPEmptyViewCell: TaskWIPEmptyViewCell){
        //
        guard let sectionData = self.dataSource?[needtoDoneSection] else { return }
        let needToDoneTask : [Task] = sectionData.items
        if(needToDoneTask.count == 0){
            self.makeToast(message:NSLocalizedString("no_found_task_need_to_be_done", comment: ""), duration: 2.0, position: .top)
        }
        else{
            let task = needToDoneTask[0]
            if(task.valid() == true){
                self.updateWIPTask(taskId: task.taskId)
            }
        }
    }
}
