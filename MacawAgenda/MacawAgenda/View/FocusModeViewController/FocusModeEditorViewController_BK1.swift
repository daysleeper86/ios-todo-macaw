//
//  FocusModeEditorViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/15/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Moya
import Material
//import TableViewDragger

class FocusModeEditorViewController_BK1: BaseViewController,UITableViewDataSource,UITableViewDelegate,SwipeTableViewCellDelegate {
    //Views
    var dragger: TableViewDragger!
    var dragging : Bool = false
    var needReload : Bool = false
    var needChanged : Bool = false
    var sectionChanged : Int = 0
    var tbBacklog : UITableView = UITableView(frame: .zero)
    var rootScroll : UIScrollView = UIScrollView(frame: .zero)
    //Data
    let wipSection : Int = 0
    let needtoDoneSection : Int = 1
    var agendaId : String = ""
    var agendaViewModel : AgendaViewModel?
    var taskViewModel : TaskViewModel?
//    var dataSource: RxTableViewSectionedReloadDataSource<TaskSection>?
    //Constants
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    var subjectDate = BehaviorSubject<Date>(value: Date())
    static let emptyTaskWIPCellIdentifier : String = "emptyTaskWIPCellIdentifier"
    static let emptyTaskNeedDoneCellIdentifier : String = "emptyTaskNeedDoneCellIdentifier"
    static let taskNotActionCellIdentifier : String = "taskNotActionCellIdentifier"
    
    //
    var dataSourceTask : [[String:Any]] = [
        [SettingString.Title:NSLocalizedString("work_in_progress", comment: "").uppercased(),SettingString.Tasks:[]],
        [SettingString.Title:NSLocalizedString("need_to_be_done", comment: "").uppercased(),SettingString.Tasks:[]],
        [SettingString.Title:NSLocalizedString("done", comment: "").uppercased(),SettingString.Tasks:[]]
    ]
    var dataSourceTempTask : [[String:Any]] = [
    ]
    //Callback
    var exitCallback: ((_ isExit: Bool) -> Void)?
    var tapCallback: ((_ isExit: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func initView(){
        self.view.isOpaque = false
        //        self.view.isUserInteractionEnabled = false
        self.view.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //Background View
        let yScrollView : CGFloat = AppDevice.IPhoneType.isIphoneX ? AppDevice.ScreenComponent.ItemPadding : 0
        let hScrollView : CGFloat = AppDevice.ScreenComponent.Height - yScrollView
        let bgView = UIView(frame: CGRect(x: 0, y: yScrollView, width: AppDevice.ScreenComponent.Width, height: hScrollView))
        bgView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.view.addSubview(bgView)
        
//        var yHeader : CGFloat = AppDevice.ScreenComponent.StatusHeight
        var yHeader : CGFloat = 0
        let hHeader : CGFloat = 44
        
        let headerView = UIView(frame: CGRect(x: 0, y: yHeader, width: bgView.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        headerView.isUserInteractionEnabled = true
        bgView.addSubview(headerView)
        
        //Tap Background Event
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapHeaderAction))
        headerView.addGestureRecognizer(tapGesture)
        
        let pButtonExit : CGFloat = 8
        let wButtonExit : CGFloat = 60
        let hButtonExit = headerView.frame.size.height
        let xButtonExit = headerView.frame.size.width - wButtonExit - pButtonExit
        let yButtonExit : CGFloat = 0


        let btnExit = UIButton.init(type: .custom)
        btnExit.frame = CGRect(x: xButtonExit, y: yButtonExit, width: wButtonExit, height: hButtonExit)
        btnExit.addTarget(self, action: #selector(exitAction), for: .touchUpInside)
        headerView.addSubview(btnExit)
        
        //
        yHeader += hHeader
        //
        let xViewLine : CGFloat = 0
        let yViewLine : CGFloat = yHeader
        let wViewLine : CGFloat = bgView.frame.size.width
        let hViewLine : CGFloat = 1
        
        let viewLine = UIView(frame: CGRect(x: xViewLine, y: yViewLine, width: wViewLine, height: hViewLine))
        viewLine.backgroundColor = AppColor.MicsColors.LINE_COLOR
        bgView.addSubview(viewLine)
        
        yHeader += hViewLine
        //
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = bgView.frame.size.width
        let hTableBacklog : CGFloat = bgView.frame.size.height - yTableBacklog
        
        rootScroll.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        bgView.addSubview(rootScroll)
        
        tbBacklog.frame = CGRect(x: 0, y: 0, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
//        tbBacklog.bounces = true
        tbBacklog.dataSource = self
        tbBacklog.delegate = self
        tbBacklog.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbBacklog.register(TaskWIPEmptyViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.emptyTaskWIPCellIdentifier)
        tbBacklog.register(TaskNeedDoneEmptyViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.emptyTaskNeedDoneCellIdentifier)
        tbBacklog.register(TeamAgendaTaskNotActionItemViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.taskNotActionCellIdentifier)
        tbBacklog.register(TeamAgendaTaskItemViewCell.self, forCellReuseIdentifier: TeamAgendaPlanningBacklogViewController.taskCellIdentifier)
        rootScroll.addSubview(tbBacklog)
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tbBacklog.frame.size.width, height: 20))
        footerView.backgroundColor = .white
        tbBacklog.tableFooterView = footerView
        
        dragger = TableViewDragger(tableView: tbBacklog)
        dragger.availableHorizontalScroll = true
        dragger.dataSource = self
        dragger.delegate = self
//        dragger.alphaForCell = 0.7
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(scrollView == rootScroll){
            print("scrollView")
        }
        else{
            print("tableView")
        }
//        if targetContentOffset.pointee.y < scrollView.contentOffset.y {
//            // it's going up
//            tbBacklog.isUserInteractionEnabled = true
//        } else {
//            // it's going down
//            tbBacklog.isUserInteractionEnabled = false
//        }
        
    }
    
    @objc func exitAction(){
//        self.dismiss(animated: false, completion: {
//            self.exitCallback?(true)
//        })
        let indexPath = IndexPath(row: 0, section: needtoDoneSection)
        let newIndexPath = IndexPath(row: 0, section: wipSection)
        let sectionData = dataSourceTask[indexPath.section]
        if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let item = tasksData[indexPath.row]
            //
            tasksData.remove(at: indexPath.row)
            dataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
            
            //
            let sectionDataNew = dataSourceTask[newIndexPath.section]
            if var tasksDataNew = sectionDataNew[SettingString.Tasks] as? [Any]{
//                tasksDataNew.remove(at: 0)
                tasksDataNew.insert(item, at: newIndexPath.row)
                dataSourceTask[newIndexPath.section][SettingString.Tasks] = tasksDataNew
            }
            CATransaction.begin()
                tbBacklog.beginUpdates()
            CATransaction.setCompletionBlock {[weak self] in
                if let strongSelf = self, var strongDataSourceTask = self?.dataSourceTask{
                    let sectionData1 = strongDataSourceTask[strongSelf.wipSection]
                    // Code to be executed upon completion
                    let newIndexPath1 = IndexPath(row: 1, section: strongSelf.wipSection)
                    if var tasksDataNew1 = sectionData1[SettingString.Tasks] as? [Any]{
                        tasksDataNew1.remove(at: newIndexPath1.row)
                        
//                        tasksDataNew.insert(item, at: newIndexPath.row)
                        strongDataSourceTask[newIndexPath1.section][SettingString.Tasks] = tasksDataNew1
                        strongSelf.dataSourceTask = strongDataSourceTask
                        strongSelf.tbBacklog.deleteRows(at: [newIndexPath1], with: .none)
                    }
                }
                
                
            }
            tbBacklog.moveRow(at: indexPath, to: newIndexPath)
            tbBacklog.endUpdates()
            CATransaction.commit()
            
            //
            //
//            tbBacklog.deleteRows(at: [newIndexPath], with: .none)
        }
        
    }
    
    @objc func tapHeaderAction(){
        self.tapCallback?(true)
    }
    
    
    
    @objc func exitAction1(){
        dismiss(animated: true,completion: nil)
    }
    
    
    func bindToViewModel(){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        func getOnlyIdFromTask(talks : [Task]){
            var ids : [String] = []
            for i in 0..<talks.count{
                let task = talks[i]
                ids.append(task.taskId)
            }
        }
        
        
        let agendaController = AgendaController.sharedAPI
        let taskController = TaskController.sharedAPI
        
        self.agendaViewModel = AgendaViewModel(
            inputUserId: (
                agendaController: agendaController,
                userId: subjectUser.asObservable(),
                projectId:projectId,
                duedate: subjectDate.asObservable(),
                needSubscrible: true
            )
        )
        
        let agendaData = self.agendaViewModel?.agendas?
            .share(replay:1)
        agendaData?.subscribe({ [weak self] agendas  in
            guard let strongAgendas = agendas.element else { return }
            if(strongAgendas.count > 0){
                let agenda = strongAgendas[0]
                if(agenda.valid() == true){
                    self?.agendaId = agenda.agendaId
                }
            }
        })
            .disposed(by: disposeBag)
        
        let taskInfo =
            agendaData!
                .flatMapLatest { agendas -> Observable<[String : Any]> in
                    var taskInfo : [String : Any] = [:]
                    if(agendas.count > 0){
                        let agenda = agendas[0]
                        if(agenda.empty() == true){
                            taskInfo = [
                                "isEmpty": true,
                                "taskId": [],
                                "userId": "",
                                "findType": Constant.FindType.findIn,
                            ]
                        }
                        else{
                            var ids : [String] = []
                            for task in agenda.tasks {
                                ids.append(task.taskId)
                            }
                            let userId = agenda.owned.userId
                            taskInfo = [
                                "taskId": ids,
                                "userId": userId,
                                "findType": Constant.FindType.findIn,
                            ]
                            //
                            if(ids.count == 0){
                                taskInfo["isEmpty"] = true
                            }
                        }
                    }
                    return Observable.just(taskInfo)
        }
        
        
        self.taskViewModel = TaskViewModel(
            inputTaskAgenda: (
                taskInfo: taskInfo,
                taskController: taskController,
                dataSource : Observable.just([]),
                projectId: projectId,
                findOther: false,
                needEmptyData:true
            )
        )
        
        
//        let tasksAndSection = self.taskViewModel!.tasks!
//            .flatMapLatest{ tasks -> Observable<[TaskSection]> in
//                var workInProgress : [Task] = []
//                var needToDone : [Task] = []
//                var hasDone : [Task] = []
//                if(TaskService.checkHolderTask(tasks: tasks) == true){
//                    let firstTask = tasks[0]
//                    workInProgress.append(firstTask)
//                    //
//                    for task in tasks{
//                        hasDone.append(task)
//                        needToDone.append(task)
//                    }
//                }
//                else{
//                    for task in tasks{
//                        if(task.dataType == .normal){
//                            if(task.completed == true){
//                                hasDone.append(task)
//                            }
//                            else{
//                                if(task.wip == false){
//                                    needToDone.append(task)
//                                }
//                            }
//                            //
//                            if(task.wip == true){
//                                workInProgress.append(task)
//                            }
//                        }
//                    }
//                    //
//                    if(workInProgress.count == 0){
//                        let task = TaskService.createEmptyTask()
//                        workInProgress.append(task)
//                    }
//                }
//                //
//                var titleNeedToDone = NSLocalizedString("need_to_be_done", comment: "")
//                if(needToDone.count > 0){
//                    titleNeedToDone = titleNeedToDone + " (" + String(needToDone.count) + ")"
//                }
//                var titleHasDone = NSLocalizedString("done", comment: "")
//                if(hasDone.count > 0){
//                    titleHasDone = titleHasDone + " (" + String(hasDone.count) + ")"
//                }
//                //
//                return Observable.just(
//                    [
//                        TaskSection(header: NSLocalizedString("work_in_progress", comment: "").uppercased(), items: workInProgress),
//                        TaskSection(header: titleNeedToDone.uppercased(), items: needToDone),
//                        TaskSection(header: titleHasDone.uppercased(), items: hasDone)
//                    ])
//        }
        
        self.taskViewModel!.tasks!.subscribe({ [weak self] tasksData  in
            guard let strongSelf = self else { return }
            var workInProgress : [Task] = []
            var needToDone : [Task] = []
            var hasDone : [Task] = []
            if let tasks = tasksData.element{
                if(TaskService.checkHolderTask(tasks: tasks) == true){
                    let firstTask = tasks[0]
                    workInProgress.append(firstTask)
                    //
                    for task in tasks{
                        hasDone.append(task)
                        needToDone.append(task)
                    }
                }
                else{
                    for task in tasks{
                        if(task.dataType == .normal){
                            if(task.completed == true){
                                hasDone.append(task)
                            }
                            else{
                                if(task.wip == false){
                                    needToDone.append(task)
                                }
                            }
                            //
                            if(task.wip == true){
                                workInProgress.append(task)
                            }
                        }
                    }
                    //
                    if(workInProgress.count == 0){
                        let task = TaskService.createEmptyTask()
                        workInProgress.append(task)
                    }
                }
            }
            //
            var titleNeedToDone = NSLocalizedString("need_to_be_done", comment: "")
            titleNeedToDone = titleNeedToDone + " (" + String(needToDone.count) + ")"
            if(needToDone.count == 0){
                let task = TaskService.createEmptyTask()
                needToDone.append(task)
            }
            //
            var titleHasDone = NSLocalizedString("done", comment: "")
            if(hasDone.count > 0){
                titleHasDone = titleHasDone + " (" + String(hasDone.count) + ")"
            }
            
            let dataSource = [
                [SettingString.Title:NSLocalizedString("work_in_progress", comment: "").uppercased(),SettingString.Tasks:workInProgress],
                [SettingString.Title:titleNeedToDone.uppercased(),SettingString.Tasks:needToDone],
                [SettingString.Title:titleHasDone.uppercased(),SettingString.Tasks:hasDone]
            ]
            //
            if(strongSelf.dragging == false){
                //
                strongSelf.dataSourceTask = dataSource
                //
                strongSelf.tbBacklog.reloadData()
            }
            else{
                strongSelf.dataSourceTempTask = dataSource
                strongSelf.needReload = true
            }
        })
            .disposed(by: disposeBag)
        
        
        
        
        //Datasource
//        let dataSource = RxTableViewSectionedReloadDataSource<TaskSection>(
//            configureCell: {(_ ,table, indexPath, element) in
//                if(element.taskId.count == 0 && element.name.count == 0){
//                    guard let cell = table.dequeueReusableCell(withIdentifier:  FocusModeEditorViewController.emptyTaskWIPCellIdentifier, for: indexPath) as? TaskWIPEmptyViewCell else {
//                        return TaskWIPEmptyViewCell()
//                    }
//                    cell.selectionStyle = .none
//                    cell.delegate = self
//                    return cell
//                }
//                else{
//                    if(element.completed == false){
//                        guard let cell = table.dequeueReusableCell(withIdentifier:  FocusModeEditorViewController.taskNotActionCellIdentifier, for: indexPath) as? TeamAgendaTaskNotActionItemViewCell else {
//                            return TeamAgendaTaskNotActionItemViewCell()
//                        }
//                        //
//                        let itemWidth = table.frame.size.width
//                        let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
//                        cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
//                        cell.selectionStyle = .none
//                        //
//                        cell.delegate = self
//
//                        cell.setValueForCell(task: element)
//
//                        return cell
//                    }
//                    else{
//                        guard let cell = table.dequeueReusableCell(withIdentifier:  TeamAgendaPlanningBacklogViewController.taskCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
//                            return TeamAgendaTaskItemViewCell()
//                        }
//                        //
//                        let itemWidth = table.frame.size.width
//                        let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
//
//                        cell.initView(itemWidth: itemWidth,contentPadding : contentPadding)
//                        cell.selectionStyle = .none
//                        cell.setValueForCell(task: element, isCanFinished: false)
//                        return cell
//                    }
//                }
//        },
//            titleForHeaderInSection: { dataSource, sectionIndex in
//                return dataSource.sectionModels[sectionIndex].header
//            }
//        )
//
//        self.dataSource = dataSource
        
//        tasksAndSection
//            .bind(to: tbBacklog.rx.items(dataSource: dataSource))
//            .disposed(by: disposeBag)
//
//
//        tbBacklog.rx
//            .modelSelected(Task.self)
//            .subscribe(onNext:  { element in
//                if(element.taskId.count > 0 && element.name.count > 0){
//                    let taskDetailView = TaskDetailViewController(nibName: nil, bundle: nil)
//                    taskDetailView.taskDetailData = element
//                    self.navigationController?.pushViewController(taskDetailView, animated: true)
//                }
//            })
//            .disposed(by: disposeBag)
        
        
//        tbBacklog.rx
//            .setDelegate(self)
//            .disposed(by : disposeBag)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionData = dataSourceTask[indexPath.section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let element = tasksData[indexPath.row]
            if(element.dataType == .empty){
                if(indexPath.section == needtoDoneSection){
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.emptyTaskNeedDoneCellIdentifier, for: indexPath) as? TaskNeedDoneEmptyViewCell else {
                        return TaskNeedDoneEmptyViewCell()
                    }
                    let itemWidth = tableView.frame.size.width
                    let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                    cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                    cell.selectionStyle = .none
                    return cell
                }
                else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.emptyTaskWIPCellIdentifier, for: indexPath) as? TaskWIPEmptyViewCell else {
                        return TaskWIPEmptyViewCell()
                    }
                    cell.selectionStyle = .none
                    cell.delegate = self
                    return cell
                }
            }
            else{
                if(element.completed == false){
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.taskNotActionCellIdentifier, for: indexPath) as? TeamAgendaTaskNotActionItemViewCell else {
                        return TeamAgendaTaskNotActionItemViewCell()
                    }
                    //
                    let itemWidth = tableView.frame.size.width
                    let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                    cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                    cell.selectionStyle = .none
                    //
                    cell.delegate = self
                    
                    cell.setValueForCell(task: element)
                    
                    return cell
                }
                else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamAgendaPlanningBacklogViewController.taskCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
                        return TeamAgendaTaskItemViewCell()
                    }
                    //
                    let itemWidth = tableView.frame.size.width
                    let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                    
                    cell.initView(itemWidth: itemWidth,contentPadding : contentPadding)
                    cell.selectionStyle = .none
                    cell.setValueForCell(task: element, isCanFinished: false)
                    return cell
                }
            }
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamAgendaPlanningBacklogViewController.taskCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
                return TeamAgendaTaskItemViewCell()
            }
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceTask.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionData = dataSourceTask[section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            return tasksData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionData = dataSourceTask[section]
        var headerView : UIView?
        var isRenderHeader : Bool = false
        if(section == needtoDoneSection){
            isRenderHeader = true
        }
        else{
            if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
                if(tasksData.count > 0){
                    isRenderHeader = true
                }
            }
        }
        if (isRenderHeader == true){
            let wHeader : CGFloat = tableView.frame.size.width
            let hHeader : CGFloat = 48
            
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: wHeader, height: hHeader))
            headerView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
            //
            let pTopTitleName : CGFloat = 45
            let xTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
            let wTitleName : CGFloat = wHeader - (xTitleName * 2)
            let hTitleName : CGFloat = 36
            
            let titleHeader = sectionData[SettingString.Title] as? String
            let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: pTopTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.text = titleHeader
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
            lbTitleName.textColor = AppColor.NormalColors.BLACK_COLOR
            headerView?.addSubview(lbTitleName)
        }
        //
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var heightRow : CGFloat = 0
        let sectionData = dataSourceTask[indexPath.section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let element = tasksData[indexPath.row]
            if(element.taskId.count == 0 && element.name.count == 0){
                heightRow = AppDevice.TableViewRowSize.TaskWIPHeight
            }
            else{
                heightRow = AppDevice.TableViewRowSize.TaskHeight
            }
        }
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var hHeader : CGFloat = 0
        if(section == needtoDoneSection){
            hHeader = 45 + 36
        }
        else{
            let sectionData = dataSourceTask[section]
            if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
                if(tasksData.count > 0){
                    hHeader = 45 + 36
                }
            }
        }
        return hHeader
    }
    
    func getVisibleCellFromTask(taskId: String) -> TeamAgendaTaskNotActionItemViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : TeamAgendaTaskNotActionItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromTask(taskId: taskId){
            visibleCell.stopProcessing()
        }
    }
    
    func stopProcessingForAllVisibleCell(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                taskCell.stopProcessing()
            }
        }
    }
    
    func updateTaskFinishChanged(taskId: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        // Update task finish
        let viewModelFinished = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaId,
                status: BehaviorSubject<Int>(value: Constant.TaskStatus.kTaskStatusCompleted.rawValue),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: taskController
            )
        )
        //
        viewModelFinished.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                        }
                        else{
                            self?.view.makeToast(NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .center)
                        }
                        //
                        self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func updateWIPTask(taskId: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let taskViewModel = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaId,
                status: BehaviorSubject<Int>(value: Constant.TaskStatus.kTaskStatusWIP.rawValue),
                loginTaps: BehaviorSubject<Void>(value: ()),
                controller: taskController
            )
        )
        //
        
        taskViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.view.makeToast(output.message, duration: 3.0, position: .center)
                    self?.stopProcessingForAllVisibleCell()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            DispatchQueue.main.async {
                                self?.view.makeToast(NSLocalizedString("add_task_to_agenda_successfully", comment: ""), duration: 1.5, position: .center, completion: { didTap in
                                })
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self?.view.makeToast(NSLocalizedString("add_task_to_agenda_unsuccessfully", comment: ""), duration: 1.5, position: .center, completion: { didTap in
                                })
                            }
                        }
                        //
                        self?.stopProcessingTaskCell(taskId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let sectionData = dataSourceTask[indexPath.section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let element = tasksData[indexPath.row]
            
            var workAction : SwipeAction?
            if(element.wip == false){
                workAction = SwipeAction(style: .default, title: NSLocalizedString("working_on_it", comment: "")) { [weak self] action, indexPath in
                    // handle action by updating model with deletion
                    guard let strongSelf = self else { return }
                    if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                        taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR)
                        //
                        taskCell.hideSwipe(animated: true)
                        //
                        strongSelf.updateWIPTask(taskId: task.taskId)
                    }
                }
            }
            else{
                workAction = SwipeAction(style: .default, title: NSLocalizedString("mask_as_done", comment: "")) { [weak self] action, indexPath in
                    // handle action by updating model with deletion
                    guard let strongSelf = self else { return }
                    if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                        taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR)
                        //
                        taskCell.hideSwipe(animated: true)
                        //
                        strongSelf.updateTaskFinishChanged(taskId: task.taskId)
                    }
                }
            }
            //
            workAction!.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
            workAction!.textColor = AppColor.NormalColors.WHITE_COLOR
            workAction!.backgroundColor = AppColor.NormalColors.BLUE_COLOR
            
            return [workAction!]
        }
        return nil
    }

    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 170
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.MicsColors.DISABLE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.NormalColors.WHITE_COLOR)
    }
    
    deinit {
        // Release all resources
        // perform the deinitialization
        exitCallback = nil
        tapCallback = nil
    }
    
}

extension FocusModeEditorViewController_BK1 : TaskWIPEmptyViewCellDelegate{
    func taskWIPEmptyViewCellNextAction(_ taskWIPEmptyViewCell: TaskWIPEmptyViewCell){
        //
        let sectionData = dataSourceTask[needtoDoneSection]
        if let needToDoneTask = sectionData[SettingString.Tasks] as? [Task]{
            if(needToDoneTask.count > 0){
                let task = needToDoneTask[0]
                if(task.valid() == true){
                    self.updateWIPTask(taskId: task.taskId)
                }
            }
            else{
                self.view.makeToast(NSLocalizedString("no_found_task_need_to_be_done", comment: ""), duration: 2.0, position: .center)
            }
        }
        else{
            self.view.makeToast(NSLocalizedString("no_found_task_need_to_be_done", comment: ""), duration: 2.0, position: .center)
        }
    }
}

extension FocusModeEditorViewController_BK1 : TableViewDraggerDelegate,TableViewDraggerDataSource{
    func dragger(_ dragger: TableViewDragger, shouldDragAt indexPath: IndexPath) -> Bool{
        if(indexPath.section == needtoDoneSection){
            let sectionData = dataSourceTask[indexPath.section]
            if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
                let item = tasksData[indexPath.row]
                if(item.dataType == .normal){
                    return true
                }
            }
        }
        return false
    }
    
    func dragger(_ dragger: TableViewDragger, moveDraggingAt indexPath: IndexPath, newIndexPath: IndexPath) -> Bool {
        let sectionData = dataSourceTask[indexPath.section]
        if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let item = tasksData[indexPath.row]
            //
            tasksData.remove(at: indexPath.row)
            dataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
            //
            let sectionDataNew = dataSourceTask[newIndexPath.section]
            if var tasksDataNew = sectionDataNew[SettingString.Tasks] as? [Any]{
                tasksDataNew.insert(item, at: newIndexPath.row)
                dataSourceTask[newIndexPath.section][SettingString.Tasks] = tasksDataNew
            }
            tbBacklog.moveRow(at: indexPath, to: newIndexPath)
        }
        //
        if(indexPath.section != newIndexPath.section){
            needChanged = true
            //
            sectionChanged = newIndexPath.section
        }
        else{
            needChanged = false
        }
        
        return true
    }
    
    func dragger(_ dragger: TableViewDragger, willBeginDraggingAt indexPath: IndexPath){
//        let cell = tbBacklog.cellForRow(at: indexPath)
//        if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
//            taskCell.backgroundViewCell?.backgroundColor = UIColor(hexString: "#EBF6FF")
//        }
        //
        dragging = true
    }
    
    func dragger(_ dragger: TableViewDragger, didEndDraggingAt indexPath: IndexPath){
        dragging = false
        //
        if(needChanged == true){
            
        }
        else{
            if(needReload == true){
                dataSourceTask = dataSourceTempTask
                //
                tbBacklog.reloadData()
                //
                needReload = false
            }
        }
    }
    
//    func dragger(_ dragger: TableViewDragger, cellForRowAt indexPath: IndexPath) -> UIView?{
//        var cellView : UIView?
//        let cell = tbBacklog.cellForRow(at: indexPath)
//        if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
//            taskCell.backgroundViewCell?.backgroundColor = UIColor(hexString: "#EBF6FF")
//            cellView = taskCell.backgroundViewCell
//        }
//        return cellView
//    }
    
//    optional func dragger(_ dragger: TableViewDragger, cellForRowAt indexPath: IndexPath) -> UIView? {
//
//    }
//
//    optional func dragger(_ dragger: TableViewDragger, indexPathForDragAt indexPath: IndexPath) -> IndexPath{
//
//    }
}
