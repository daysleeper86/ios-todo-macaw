//
//  FocusModeViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/15/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import SkeletonView
import Nantes
import Material
//import ARNTransitionAnimator
import DTGradientButton
import NVActivityIndicatorView

class FocusModeViewController: BaseViewController {
    //Views
    private var viewNeedToDone : UIView?
    private var bgView : UIView?
    private var switchDarkMode : UISwitch?
    private var lbTitleDarkMode : UILabel?
    private var btnExit : UIButton?
    private var viewWorkInProgress : UIView?
    private var lbTitleProgress = UILabel(frame: .zero)
    private var lbTitleContentProgress = NantesLabel(frame: .zero)
    private var lbTitleNeedToDone : UILabel?
    private var imvNeedToDone : UIImageView?
    private var viewContentProgress : UIView?
    private var btnDoneProgress : RaisedButton?
    private var btnError : UIButton?
    private var viewAnimation : NVActivityIndicatorView?
    private var focusModeEditorNavigation : DTOverlayController?
    private var focusModeEditorView : FocusModeEditorViewControllerNew?
    
    //Data
    static let wipSection : Int = 0
    static let needtoDoneSection : Int = 1
    static let doneSection : Int = 2
    var dataSourceTask : [[String:Any]] = [
        [SettingString.Title:NSLocalizedString("work_in_progress", comment: "").uppercased(),SettingString.Tasks:[]],
        [SettingString.Title:NSLocalizedString("need_to_be_done", comment: "").uppercased(),SettingString.Tasks:[]],
        [SettingString.Title:NSLocalizedString("done", comment: "").uppercased(),SettingString.Tasks:[]]
    ]
    var dataSourceTempTask : [[String:Any]] = [
    ]
    var wipTask = TaskService.createEmptyTask()
    var needToDoneTask : [Task] = []
    var dragging : Bool = false{
        didSet {
            focusModeEditorView?.dragging = dragging
        }
    }
    var needReload : Bool = false
    var needSwapWIP : Bool = false
    let holderHeight : CGFloat = 150
    let GoToNext : String = "gotoNext"
    
    //ViewModel
    var agendaId : String = ""
    var agendaViewModel : AgendaViewModel?
    var taskViewModel : TaskViewModel?
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    var subjectDate = BehaviorSubject<Date>(value: AppSettings.sharedSingleton.agenda_today?.date ?? Date())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
        //
        changeThemeData()
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Background View
        let yScrollView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hScrollView : CGFloat = AppDevice.ScreenComponent.Height - yScrollView
        bgView = UIView(frame: CGRect(x: 0, y: yScrollView, width: AppDevice.ScreenComponent.Width, height: hScrollView))
        bgView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(bgView!)
        
        let pScreen : CGFloat = 0
        let yScreen = pScreen
        //Header View
        var yHeader = yScreen
        let hHeader : CGFloat = 44
        let pBottomHeader : CGFloat = 0
        
        let headerView = UIView(frame: CGRect(x: 0, y: yHeader, width: bgView!.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        bgView!.addSubview(headerView)
        
        let pButtonExit : CGFloat = 8
        let wButtonExit : CGFloat = 60
        let hButtonExit = headerView.frame.size.height
        let xButtonExit = pButtonExit
        let yButtonExit : CGFloat = 0
        
        btnExit = UIButton.init(type: .custom)
        btnExit?.frame = CGRect(x: xButtonExit, y: yButtonExit, width: wButtonExit, height: hButtonExit)
        btnExit?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        btnExit?.titleLabel?.textAlignment = .center
        btnExit?.setTitleColor(AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, for: .normal)
        btnExit?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnExit?.setTitle(NSLocalizedString("exit", comment: "").uppercased(), for: .normal)
        btnExit?.addTarget(self, action: #selector(exitAction), for: .touchUpInside)
        headerView.addSubview(btnExit!)
        
        let pMore : CGFloat = 3
        let pSwitch : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wSwitch : CGFloat = AppDevice.SubViewFrame.SwitchWidth
        let hSwitch : CGFloat = AppDevice.SubViewFrame.SwitchHeight
        let xSwitch : CGFloat = headerView.frame.size.width - wSwitch - pSwitch
        let ySwitch : CGFloat = (hHeader-hSwitch)/2 + pMore
        
        switchDarkMode = UISwitch(frame: CGRect(x: xSwitch, y: ySwitch, width: wSwitch, height: hSwitch))
        switchDarkMode?.layer.cornerRadius = hSwitch / 2
        switchDarkMode?.onTintColor = UIColor(hexString: "#F2F2F2")
        switchDarkMode?.addTarget(self, action: #selector(changeValue(_:)), for: .valueChanged)
        //
        if(AppSettings.sharedSingleton.isDarkMode == true){
            switchDarkMode?.thumbTintColor = AppColor.MicsColors.DISABLE_DARK_COLOR
        }
        else{
            switchDarkMode?.thumbTintColor = AppColor.NormalColors.DARK_THEME_COLOR
        }
        headerView.addSubview(switchDarkMode!)
        
        let wTitleDarkMode : CGFloat = 100
        let pTitleDarkMode : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleDarkMode : CGFloat = xSwitch - wTitleDarkMode - pTitleDarkMode
        let yTitleDarkMode : CGFloat = 0 + pMore
        let hTitleDarkMode : CGFloat = headerView.frame.size.height
        lbTitleDarkMode = UILabel(frame: CGRect(x: xTitleDarkMode, y: yTitleDarkMode, width: wTitleDarkMode, height: hTitleDarkMode))
        lbTitleDarkMode?.textAlignment = .right
        lbTitleDarkMode?.textColor = AppColor.MicsColors.BLUE_DARK_COLOR
        lbTitleDarkMode?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        headerView.addSubview(lbTitleDarkMode!)
        
        yHeader += hHeader
        yHeader += pBottomHeader
    
        //
        let xViewNeedToDone : CGFloat = 0
        let hViewNeedToDone : CGFloat = 44
        let yViewNeedToDone : CGFloat = bgView!.frame.size.height - hViewNeedToDone
        let wViewNeedToDone : CGFloat = bgView!.frame.size.width
        
        viewNeedToDone = UIView(frame: CGRect(x: xViewNeedToDone, y: yViewNeedToDone, width: wViewNeedToDone, height: hViewNeedToDone))
        bgView!.addSubview(viewNeedToDone!)
        
        let pTitleNeedToDone : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
        let xTitleNeedToDone : CGFloat = pTitleNeedToDone
        let yTitleNeedToDone : CGFloat = 0
        let wTitleNeedToDone : CGFloat = viewNeedToDone!.frame.size.width - (xTitleNeedToDone * 2)
        let hTitleNeedToDone : CGFloat = viewNeedToDone!.frame.size.height
        
        lbTitleNeedToDone = UILabel(frame: CGRect(x: xTitleNeedToDone, y: yTitleNeedToDone, width: wTitleNeedToDone, height: hTitleNeedToDone))
        lbTitleNeedToDone?.textAlignment = .left
        lbTitleNeedToDone?.textColor = AppColor.NormalColors.DARK_COLOR
        lbTitleNeedToDone?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        viewNeedToDone?.addSubview(lbTitleNeedToDone!)
        
        let pImageNeedToDone : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wImageNeedToDone : CGFloat = 24
        let xImageNeedToDone : CGFloat = viewNeedToDone!.frame.size.width - pImageNeedToDone - pImageNeedToDone
        let yImageNeedToDone : CGFloat = (viewNeedToDone!.frame.size.height - wImageNeedToDone) / 2
        
        imvNeedToDone = UIImageView(frame: CGRect(x: xImageNeedToDone, y: yImageNeedToDone, width: wImageNeedToDone, height: wImageNeedToDone))
        viewNeedToDone?.addSubview(imvNeedToDone!)
        
        let btnNeedToDone = UIButton.init(type: .custom)
        btnNeedToDone.frame = CGRect(x: 0, y: 0, width: viewNeedToDone!.frame.size.width, height: viewNeedToDone!.frame.size.height)
        btnNeedToDone.addTarget(self, action: #selector(needToDoneAction), for: .touchUpInside)
        viewNeedToDone?.addSubview(btnNeedToDone)
        
        //Content View
        let xContentProgress : CGFloat = 0
        let yContentProgress : CGFloat = yHeader
        let wContentProgress : CGFloat = bgView!.frame.size.width
        let hContentProgress : CGFloat = bgView!.frame.size.height - headerView.frame.size.height - viewNeedToDone!.frame.size.height
        
        viewWorkInProgress = UIView(frame: CGRect(x: xContentProgress, y: yContentProgress, width: wContentProgress, height: hContentProgress))
        bgView!.addSubview(viewWorkInProgress!)
    }
    
    func changeThemeData(){
        //
        var backgroundViewColor : UIColor!
        var valueDarkMode : Bool!
        var offColor : UIColor!
        var titleDarkMode : String!
        var titleDarkModeColor : UIColor!
        var exitButtonNormalColor : UIColor!
        var exitButtonSelectedColor : UIColor!
        var imageNeedToDone : String!
        var titleNeedToDoneColor : UIColor!
        var titleProgress : UIColor!
        var titleContentProgress : UIColor!
        //
        if(AppSettings.sharedSingleton.isDarkMode == true){
            backgroundViewColor = AppColor.MicsColors.DARK_MENU_COLOR
            valueDarkMode = false
            offColor = UIColor.black
            titleDarkMode = NSLocalizedString("dark", comment: "")
            titleDarkModeColor = AppColor.MicsColors.BLUE_DARK_COLOR
            exitButtonNormalColor = AppColor.NormalColors.GRAY_COLOR
            exitButtonSelectedColor = AppColor.NormalColors.WHITE_COLOR
            titleNeedToDoneColor = AppColor.NormalColors.BLUE_COLOR
            imageNeedToDone = "ic_arrow_up_dark"
            titleProgress = AppColor.NormalColors.BLUE_COLOR
            titleContentProgress = AppColor.NormalColors.WHITE_COLOR
            //
            UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
        }
        else{
            backgroundViewColor = AppColor.NormalColors.WHITE_COLOR
            valueDarkMode = true
            offColor = UIColor(hexString: "#F2F2F2")
            titleDarkMode = NSLocalizedString("light", comment: "")
            titleDarkModeColor = AppColor.NormalColors.DARK_THEME_COLOR
            exitButtonNormalColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
            exitButtonSelectedColor = AppColor.NormalColors.BLACK_COLOR
            titleNeedToDoneColor = AppColor.NormalColors.DARK_COLOR
            imageNeedToDone = "ic_arrow_up"
            titleProgress = AppColor.NormalColors.DARK_COLOR
            titleContentProgress = AppColor.NormalColors.BLACK_COLOR
            //
            if #available(iOS 13.0, *) {
                UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.setStatusBarStyle(.default, animated: true)
            }
        }
        //
        if(self.dragging == true){
            titleContentProgress = AppColor.NormalColors.GRAY_1_COLOR
        }
        //
        self.view.backgroundColor = backgroundViewColor
        bgView?.backgroundColor = backgroundViewColor
        switchDarkMode?.isOn = valueDarkMode
        lbTitleDarkMode?.text = titleDarkMode
        lbTitleDarkMode?.textColor = titleDarkModeColor
        btnExit?.setTitleColor(exitButtonNormalColor, for: .normal)
        btnExit?.setTitleColor(exitButtonSelectedColor, for: .highlighted)
        lbTitleNeedToDone?.textColor = titleNeedToDoneColor
        imvNeedToDone?.image = UIImage(named: imageNeedToDone)
        
        lbTitleProgress.textColor = titleProgress
        lbTitleContentProgress.textColor = titleContentProgress
        //
        setContentView(nil,isLoading: false)
        //
        focusModeEditorView?.changeThemeData()
        
        switchDarkMode?.tintColor = offColor
        switchDarkMode?.backgroundColor = offColor
    }
    
    @objc func changeValue(_ sender: UISwitch) {
        let isOn = sender.isOn
        var themeId = ""
        if(isOn == false){
            themeId = Constant.ThemeTypes.dark
        }
        else{
            themeId = Constant.ThemeTypes.light
        }
        AppSettings.sharedSingleton.saveThemeAccount(themeId)
        //
        changeThemeData()
    }
    
    func resetValue_Cell(){
        lbTitleContentProgress.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        lbTitleContentProgress.showAnimatedGradientSkeleton()
    }
    
    func stopShimmering_Cell(){
        lbTitleContentProgress.stopSkeletonAnimation()
        lbTitleContentProgress.hideSkeleton()
    }
    
    func setContentView(_ task : Task?, isLoading: Bool){
        //
        let taskData = task ?? wipTask
        let pContentProgress : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
        
        if(viewContentProgress == nil){
            viewContentProgress = UIView(frame: CGRect(x: 0, y: 0, width: viewWorkInProgress!.frame.size.width, height: viewWorkInProgress!.frame.size.height))
            viewWorkInProgress?.addSubview(viewContentProgress!)
            
            var yHeader : CGFloat = 0
    
            let pTitleProgress : CGFloat = pContentProgress
            let xTitleProgress : CGFloat = pTitleProgress
            let yTitleProgress : CGFloat = yHeader
            let wTitleProgress : CGFloat = viewWorkInProgress!.frame.size.width - (xTitleProgress * 2)
            let hTitleProgress : CGFloat = 20
            let pBottomTitleProgress : CGFloat = 8
            
            lbTitleProgress.frame = CGRect(x: xTitleProgress, y: yTitleProgress, width: wTitleProgress, height: hTitleProgress)
            lbTitleProgress.textAlignment = .left
            lbTitleProgress.numberOfLines = 0
            lbTitleProgress.textColor = AppColor.NormalColors.DARK_COLOR
            lbTitleProgress.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
            lbTitleProgress.text = NSLocalizedString("work_in_progress", comment: "").uppercased()
            viewContentProgress?.addSubview(lbTitleProgress)
            
            let sizeTitleProgress : CGSize = lbTitleProgress.text!.getTextSizeWithString(lbTitleProgress.frame.size.width,lbTitleProgress.font)
            let widthTitleProgress : CGFloat = sizeTitleProgress.width
            //
            let pSpinnerView : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let xSpinnerView : CGFloat = lbTitleProgress.frame.origin.x + widthTitleProgress + pSpinnerView
            let wSpinnerView : CGFloat = 24.0
            let hSpinnerView : CGFloat = wSpinnerView
            let ySpinnerView : CGFloat = lbTitleProgress.frame.origin.y - 2
            
            if(btnError == nil){
                //
                btnError = UIButton(type: .custom)
                btnError?.frame = CGRect(x: xSpinnerView, y: ySpinnerView, width: wSpinnerView, height: hSpinnerView)
                btnError?.addTarget(self, action: #selector(errorAction(sender:)), for: .touchUpInside)
                btnError?.setImage(UIImage(named: "ic_error_failed"), for: .normal)
                btnError?.isHidden = true
                viewContentProgress?.addSubview(btnError!)
            }
            else{
                btnError?.frame.origin.x = xSpinnerView
                btnError?.frame.origin.y = ySpinnerView
            }
            
            yHeader += hTitleProgress
            yHeader += pBottomTitleProgress
            
            let pTitleContentProgress : CGFloat = pContentProgress
            let xTitleContentProgress : CGFloat = pTitleContentProgress
            let yTitleContentProgress : CGFloat = yHeader
            let wTitleContentProgress : CGFloat = viewWorkInProgress!.frame.size.width - (xTitleContentProgress * 2)
            let hTitleContentProgress : CGFloat = holderHeight
            
            lbTitleContentProgress.frame = CGRect(x: xTitleContentProgress, y: yTitleContentProgress, width: wTitleContentProgress, height: hTitleContentProgress)
            lbTitleContentProgress.delegate = self
            lbTitleContentProgress.linkAttributes = [
                NSAttributedString.Key.foregroundColor: AppColor.NormalColors.BLUE_COLOR,
                NSAttributedString.Key.font: UIFont(name: FontNames.Lato.MULI_BOLD, size: 32) ?? UIFont.systemFont(ofSize: 32, weight: UIFont.Weight.bold)
            ]
            lbTitleContentProgress.textAlignment = .left
            lbTitleContentProgress.numberOfLines = 0
            lbTitleContentProgress.textColor = AppColor.NormalColors.BLACK_COLOR
            lbTitleContentProgress.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 32)
            lbTitleContentProgress.isSkeletonable = true
            viewContentProgress?.addSubview(lbTitleContentProgress)
        }
        
        var yHeader : CGFloat = lbTitleContentProgress.frame.origin.y
        let pBottomTitleContentProgress : CGFloat = AppDevice.ScreenComponent.ItemPadding
        //
        var taskName : String = ""
        if(taskData.name.count > 0){
            if(taskData.taskNumber.count > 0){
                taskName = "#" + taskData.taskNumber + " - " + taskData.name
            }
            else{
                taskName = taskData.name
            }
        }
        
        if(taskName.count > 0){
            //
            stopShimmering_Cell()
            //
            if(taskData.taskId.count == 0){
                var titleContentProgress : UIColor!
                if(AppSettings.sharedSingleton.isDarkMode == true){
                    titleContentProgress = AppColor.NormalColors.WHITE_COLOR
                }
                else{
                    titleContentProgress = AppColor.NormalColors.BLACK_COLOR
                }
                lbTitleContentProgress.textColor = titleContentProgress
                //Data
                let emptyData = NSLocalizedString("no_running_task", comment: "") + "\n" + NSLocalizedString("start_next_task", comment: "")
                let hereData = NSLocalizedString("here", comment: "")
                let fullData = emptyData + " " + hereData
                //
                taskName = fullData
                lbTitleContentProgress.text = taskName
                //
                if let url = URL(string: GoToNext){
                    lbTitleContentProgress.addLink(to: url, withRange: NSRange(location: fullData.count-hereData.count, length: hereData.count))
                }
            }
            else{
                //
                if(self.dragging == true){
                    lbTitleContentProgress.textColor = AppColor.NormalColors.GRAY_1_COLOR
                }
                else{
                    var titleContentProgress : UIColor!
                    if(AppSettings.sharedSingleton.isDarkMode == true){
                        titleContentProgress = AppColor.NormalColors.WHITE_COLOR
                    }
                    else{
                        titleContentProgress = AppColor.NormalColors.BLACK_COLOR
                    }
                    lbTitleContentProgress.textColor = titleContentProgress
                }
                lbTitleContentProgress.text = taskName
                
            }
            let sizeTitleContentProgress : CGSize = taskName.getTextSizeWithString(lbTitleContentProgress.frame.size.width,lbTitleContentProgress.font)
            let hTitleContentProgress : CGFloat = sizeTitleContentProgress.height
            lbTitleContentProgress.frame.size.height = hTitleContentProgress
            
            yHeader += hTitleContentProgress
            yHeader += pBottomTitleContentProgress
        }
        else{
            startShimmering_Cell()
        }
        
        if(taskData.taskId.count > 0 && taskData.name.count > 0){
            let yButtonDone :CGFloat = yHeader
            if(btnDoneProgress == nil){
                let pButtonDone : CGFloat = pContentProgress
                let xButtonDone :CGFloat = pButtonDone
                let wButtonDone :CGFloat = 120
                let hButtonDone :CGFloat = 36
                
                btnDoneProgress = RaisedButton.init(type: .custom)
                btnDoneProgress?.pulseColor = .white
                btnDoneProgress?.frame = CGRect(x: xButtonDone, y: yButtonDone, width: wButtonDone, height: hButtonDone)
                btnDoneProgress?.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
                btnDoneProgress?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
                btnDoneProgress?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
                btnDoneProgress?.titleLabel?.textAlignment = .left
                btnDoneProgress?.layer.cornerRadius = 4.0
                btnDoneProgress?.layer.masksToBounds = true
                btnDoneProgress?.setTitle(NSLocalizedString("mask_as_done", comment:""), for: .normal)
                viewContentProgress?.addSubview(btnDoneProgress!)
                
                //
                let wViewAnimation : CGFloat = 20
                let hViewAnimation : CGFloat = wViewAnimation
                let xViewAnimation : CGFloat = wButtonDone + AppDevice.ScreenComponent.ItemPadding
                let yViewAnimation : CGFloat = (hButtonDone - hViewAnimation ) / 2
                viewAnimation = NVActivityIndicatorView(frame: CGRect(x: xViewAnimation, y: yViewAnimation, width: wViewAnimation, height: hViewAnimation),
                                                        type: .ballClipRotate)
                viewAnimation?.color = AppColor.NormalColors.DARK_THEME_COLOR
                viewAnimation?.isHidden = true
                viewAnimation?.startAnimating()
                btnDoneProgress?.addSubview(viewAnimation!)
            }
            else{
                btnDoneProgress?.frame.origin.y = yButtonDone
            }
            //
            stateForDoneButton(loading: isLoading,holder: isLoading)
            //
            btnDoneProgress?.isHidden = false
            //
            let hViewContentProgress : CGFloat = btnDoneProgress!.frame.origin.y + btnDoneProgress!.frame.size.height
            let yViewContentProgress : CGFloat = (viewWorkInProgress!.frame.size.height - hViewContentProgress ) / 2
            
            viewContentProgress?.frame.origin.y = yViewContentProgress
            viewContentProgress?.frame.size.height = hViewContentProgress
        }
        else{
            btnDoneProgress?.isHidden = true
            //
            let hViewContentProgress : CGFloat = lbTitleContentProgress.frame.origin.y + lbTitleContentProgress.frame.size.height
            let yViewContentProgress : CGFloat = (viewWorkInProgress!.frame.size.height - hViewContentProgress ) / 2 - 44
            
            viewContentProgress?.frame.origin.y = yViewContentProgress
            viewContentProgress?.frame.size.height = hViewContentProgress
            //
            btnError?.isHidden = true
        }
    }
    
    @objc func exitAction(){
        if #available(iOS 13.0, *) {
            UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.setStatusBarStyle(.default, animated: true)
        }
        //
        focusModeEditorView?.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func needToDoneAction(){
        //
        if(focusModeEditorView == nil){
            focusModeEditorView  = FocusModeEditorViewControllerNew(nibName: nil, bundle: nil)
            focusModeEditorView?.exitCallback = {[weak self] (isExit) -> Void in
                guard let strongSelf = self else { return }
                strongSelf.focusModeEditorNavigation?.dismiss(animated: true, completion: nil)
            }
            focusModeEditorView?.changeCallback = {[weak self] (on) -> Void in
                guard let strongSwitch = self?.switchDarkMode else { return }
                let isOn = !strongSwitch.isOn
                strongSwitch.isOn = isOn
                strongSwitch.sendActions(for: .valueChanged)
            }
            
            focusModeEditorView?.dragging = self.dragging
            focusModeEditorView?.updateTaskFinishCallback = {[weak self] (taskId,completed) -> Void in
                guard let strongSelf = self else { return }
                strongSelf.updateTaskFinishChanged(taskId: taskId,completed: completed)
            }
            focusModeEditorView?.updateWIPTaskCallback = {[weak self] (task) -> Void in
                guard let strongSelf = self else { return }
                strongSelf.wipTask = task
                strongSelf.dragging = true
                strongSelf.updateWIPTask(taskId: task.taskId)
                //
            }
            focusModeEditorView?.retryCancelCallback = {[weak self] (state) -> Void in
                guard let strongSelf = self else { return }
                strongSelf.updateWIPAction(action: state)
            }
            //
            if(wipTask.errorType == true){
                var cloneDataSourceTask = dataSourceTask
                let indexPath = IndexPath(row: 0, section: FocusModeViewController.needtoDoneSection)
                let newIndexPath = IndexPath(row: 0, section: FocusModeViewController.wipSection)
                
                let sectionData = cloneDataSourceTask[indexPath.section]
                if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
                    var item = tasksData[0]
                    item.wip = true
                    item.errorType = wipTask.errorType
                    tasksData.remove(at: indexPath.row)
                    cloneDataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
                    //
                    let sectionDataNew = cloneDataSourceTask[newIndexPath.section]
                    if var tasksDataNew = sectionDataNew[SettingString.Tasks] as? [Task]{
                        let itemNew = tasksDataNew[newIndexPath.row]
                        tasksDataNew.removeAll()
                        if(itemNew.dataType != .empty){
                            //
                            tasksData.insert(itemNew, at: indexPath.row)
                            cloneDataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
                        }
                        else{
                            
                        }
                        tasksDataNew.insert(item, at: newIndexPath.row)
                        cloneDataSourceTask[newIndexPath.section][SettingString.Tasks] = tasksDataNew
                    }
                    //
                    if(tasksData.count == 0){
                        let task = TaskService.createEmptyTask()
                        tasksData.append(task)
                        //
                        cloneDataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
                    }
                }
                focusModeEditorView?.dataSourceTaskItem = cloneDataSourceTask
            }
            else{
                focusModeEditorView?.dataSourceTaskItem = self.dataSourceTask
            }
        }
        //
        let navigationController = UINavigationController(rootViewController: focusModeEditorView!)
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.view.backgroundColor = .clear
        //
        focusModeEditorNavigation = DTOverlayController(viewController: navigationController,viewNotPan: [UIView(frame: .zero)])
        focusModeEditorNavigation?.isHandleHidden = true
        focusModeEditorNavigation?.overlayHeight = .dynamic(1.0)
        focusModeEditorNavigation?.handleSize =  .zero
        if let presentationController = focusModeEditorNavigation?.presentationController  as? DTOverlayPresentationController{
            presentationController.dimminView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        }
        present(focusModeEditorNavigation!, animated: true, completion: nil)
    }
    
    @objc func doneAction(){
        updateTaskFinishChanged(taskId: wipTask.taskId,completed: true)
    }
    
    func startUpdateWIP(retry: Bool){
        self.setContentView(nil,isLoading: true)
        if(focusModeEditorView == nil){
            self.updateWIPTask(taskId: wipTask.taskId)
        }
        else{
            if(retry == true){
                focusModeEditorView?.retry_WIP(index: 0)
            }
            else{
                focusModeEditorView?.moveToWIPAction(fromRow: 0, toRow: 0, task: wipTask)
            }
        }
    }
    
    func updateWIPAction(action: Int){
        if(action == 0){
            startUpdateWIP(retry: true)
        }
        else{
            needSwapWIP = false
            //
            let sectionData = dataSourceTask[FocusModeViewController.wipSection]
            if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
                let task = tasksData[0]
                wipTask = task
            }
            self.dragging = false
            //
            resetValueForView()
            focusModeEditorView?.dataSourceTaskItem = self.dataSourceTask
        }
    }
    
    @objc func errorAction(sender: UIButton){
        //
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("try_again",comment:""), style: .default, handler: {[weak self] (action) in
            //
            self?.updateWIPAction(action: 0)
        }))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("remove",comment:""), style: .destructive, handler: {[weak self] (action) in
            //
            self?.updateWIPAction(action: 1)
        }))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel",comment:""), style: .cancel))
        self.present(actionSheet, animated: true)
        
    }
    
    func updateTaskFinishChanged(taskId: String, completed: Bool){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        // Update task finish
        let finished = completed == true ? Constant.TaskStatus.kTaskStatusCompleted.rawValue : Constant.TaskStatus.kTaskStatusUncompleted.rawValue
        
        let viewModelFinished = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaId,
                status: BehaviorSubject<Int>(value: finished),
                loginTaps: BehaviorSubject(value: ()).asObservable(),
                controller: taskController
            )
        )
        //
        viewModelFinished.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                self?.stateForDoneButton(loading: loading,holder: false)
            })
            .disposed(by: disposeBag)
        //
        viewModelFinished.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToastSystem(message:output.message, duration: 3.0, position: .top)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            if let strongEditor = self?.focusModeEditorNavigation{
                                if(strongEditor.presentingViewController == nil){
                                    self?.needToDoneAction()
                                }
                            }
                            else{
                                self?.needToDoneAction()
                            }
                        }
                        else{
                            self?.makeToastSystem(message:NSLocalizedString("update_task_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                        }
                    }
                }
            }
            //
            self?.focusModeEditorView?.processedTaskFinish(loginResult: loginResult)
        }).disposed(by: disposeBag)
    }
    
    func stateForDoneButton(loading : Bool, holder: Bool){
        var wButtonDone : CGFloat = 0
        //
        btnDoneProgress?.isEnabled = !loading
        if(loading == false){
            wButtonDone = 120
            viewAnimation?.stopAnimating()
            viewAnimation?.isHidden = true
            //
            if(self.dragging == false){
                let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
                btnDoneProgress?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
                btnDoneProgress?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
                btnDoneProgress?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
                //
                btnError?.isHidden = true
            }
            else{
                //
                let gradientColor = [UIColor(red: CGFloat(248.0)/255, green: CGFloat(248.0)/255, blue: CGFloat(248.0)/255, alpha: 1.0), UIColor(red: CGFloat(225.0)/255, green: CGFloat(234.0)/255, blue: CGFloat(238.0)/255, alpha: 1.0)]
                btnDoneProgress?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.normal)
                btnDoneProgress?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.highlighted)
                btnDoneProgress?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
                //
                btnError?.isHidden = false
                if(wipTask.errorType == true){
                    btnDoneProgress?.isEnabled = false
                }
            }
        }
        else{
            btnError?.isHidden = true
            //
            if(holder == false){
                wButtonDone = 176
                //
                viewAnimation?.startAnimating()
                viewAnimation?.isHidden = false
            }
            else{
                wButtonDone = 120
                viewAnimation?.stopAnimating()
                viewAnimation?.isHidden = true
            }
            //
            let gradientColor = [UIColor(red: CGFloat(248.0)/255, green: CGFloat(248.0)/255, blue: CGFloat(248.0)/255, alpha: 1.0), UIColor(red: CGFloat(225.0)/255, green: CGFloat(234.0)/255, blue: CGFloat(238.0)/255, alpha: 1.0)]
            btnDoneProgress?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.normal)
            btnDoneProgress?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.highlighted)
            btnDoneProgress?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
        }
        btnDoneProgress?.frame.size.width = wButtonDone
    }
    
    func checkNeedSwapWIP() -> Bool{
        var swapWIP = false
        let indexPath = IndexPath(row: 0, section: FocusModeViewController.wipSection)
        let newIndexPath = IndexPath(row: 0, section: FocusModeViewController.needtoDoneSection)
        
        let sectionData = dataSourceTask[indexPath.section]
        if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let sectionDataNew = dataSourceTask[newIndexPath.section]
            if var tasksDataNew = sectionDataNew[SettingString.Tasks] as? [Task]{
                if(tasksData.count > 0 && tasksDataNew.count > 0){
                    let item = tasksData[0]
                    let itemNew = tasksDataNew[0]
                    if(item.dataType == .normal && itemNew.dataType == .normal){
                        swapWIP =  true
                    }
                }
            }
        }
        return swapWIP
    }
    
    func updateWIPTask(taskId: String){
        needSwapWIP = true
        //
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let taskController = TaskAPIController(provider: accountProvider)
        //
        let taskViewModel = TaskAPIViewModel(
            inputUpdateTaskAgendaStatus: (
                taskId: taskId,
                projectId: projectId,
                agendaId: agendaId,
                status: BehaviorSubject<Int>(value: Constant.TaskStatus.kTaskStatusWIP.rawValue),
                loginTaps: BehaviorSubject<Void>(value: ()),
                controller: taskController
            )
        )
        //
        taskViewModel.processingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.lbTitleContentProgress.startShimmering()
                }
                else{
                    self?.lbTitleContentProgress.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        //
        taskViewModel.processFinished?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToastSystem(message:output.message, duration: 3.0, position: .top)
                    self?.lbTitleContentProgress.stopShimmering()
                    //
                    self?.wipTask.errorType = true
                    self?.setContentView(nil, isLoading: false)
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                self?.makeToastSystem(message:NSLocalizedString("add_task_to_agenda_unsuccessfully", comment: ""), duration: 1.5, position: .top)
                                self?.wipTask.errorType = true
                                self?.setContentView(nil, isLoading: false)
                            }
                        }
                        //
                        self?.lbTitleContentProgress.stopShimmering()
                    }
                }
                
            }
            self?.focusModeEditorView?.processedWIPTask(loginResult: loginResult)
        }).disposed(by: disposeBag)
    }
    
    func bindToViewModel(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        
        func getOnlyIdFromTask(talks : [Task]){
            var ids : [String] = []
            for i in 0..<talks.count{
                let task = talks[i]
                ids.append(task.taskId)
            }
        }
        
        
        let agendaController = AgendaController.sharedAPI
        let taskController = TaskController.sharedAPI
        
        self.agendaViewModel = AgendaViewModel(
            inputUserId: (
                agendaController: agendaController,
                userId: subjectUser.asObservable(),
                projectId:Observable.just(projectId),
                duedate: subjectDate.asObservable(),
                needSubscrible: true
            )
        )
        
        let agendaData = self.agendaViewModel?.agendas?
               .share(replay:1)
        
        agendaData?.subscribe({ [weak self] agendas  in
            guard let strongAgendas = agendas.element else { return }
            if(strongAgendas.count > 0){
                let agenda = strongAgendas[0]
                if(agenda.valid() == true){
                    self?.agendaId = agenda.agendaId
                }
            }
        })
            .disposed(by: disposeBag)
        
        let taskInfo =
                agendaData!
                .flatMapLatest { agendas -> Observable<[String : Any]> in
                    var taskInfo : [String : Any] = [:]
                    if(agendas.count > 0){
                        let agenda = agendas[0]
                        if(agenda.empty() == true){
                            taskInfo = [
                                "isEmpty": true,
                                "taskId": [],
                                "userId": userId,
                                "findType": Constant.FindType.findIn,
                            ]
                        }
                        else{
                            var ids : [String] = []
                            for task in agenda.tasks {
                                ids.append(task.taskId)
                            }
                            let userId = agenda.owned.userId
                            //
                            taskInfo = [
                                "taskId": ids,
                                "userId": userId,
                                "findType": Constant.FindType.findIn,
                            ]
                            //
                            if(ids.count == 0){
                                taskInfo["isEmpty"] = true
                            }
                        }
                    }
                    return Observable.just(taskInfo)
        }
        
        
        self.taskViewModel = TaskViewModel(
            inputTaskAgenda: (
                taskInfo: taskInfo,
                taskController: taskController,
                dataSource : Observable.just([]),
                projectId: projectId,
                findOther: false,
                needEmptyData:true
            )
        )
        
        let tasksData = self.taskViewModel?.tasks?.share(replay: 1)
        tasksData?.subscribe({ [weak self] tasksData  in
            guard let strongSelf = self, let strongElement = tasksData.element else { return }
            var workInProgress : [Task] = []
            var needToDone : [Task] = []
            var hasDone : [Task] = []
            var wipTask : Task = TaskService.createEmptyTask()
            if(strongElement.count  > 0){
                if let tasks = tasksData.element{
                    if(TaskService.checkHolderTask(tasks: tasks) == true){
                        let holderTask = tasks[0]
                        workInProgress.append(holderTask)
                        for task in tasks{
                            hasDone.append(task)
                            needToDone.append(task)
                        }
                        //
                        wipTask = holderTask
                    }
                    else{
                        if let wipTaskTemp = TaskService.checkWIPTask(tasks: strongElement){
                            wipTask = wipTaskTemp
                        }
                        else{
                            wipTask.name = NSLocalizedString("no_running_task", comment: "")
                        }
                        //
                        for task in tasks{
                            if(task.dataType == .normal){
                                if(task.completed == true){
                                    hasDone.append(task)
                                }
                                else{
                                    if(task.wip == false){
                                        needToDone.append(task)
                                    }
                                }
                                //
                                if(task.wip == true){
                                    workInProgress.append(task)
                                }
                            }
                        }
                        //
                        if(workInProgress.count == 0){
                            workInProgress.append(wipTask)
                        }
                    }
                }
                //
                var titleNeedToDone = NSLocalizedString("need_to_be_done", comment: "")
                titleNeedToDone = titleNeedToDone + " (" + String(needToDone.count) + ")"
                if(needToDone.count == 0){
                    let task = TaskService.createEmptyTask()
                    needToDone.append(task)
                }
                //
                var titleHasDone = NSLocalizedString("done", comment: "")
                if(hasDone.count > 0){
                    titleHasDone = titleHasDone + " (" + String(hasDone.count) + ")"
                }
                //
                let dataSource = [
                    [SettingString.Title:NSLocalizedString("work_in_progress", comment: "").uppercased(),SettingString.Tasks:workInProgress],
                    [SettingString.Title:titleNeedToDone.uppercased(),SettingString.Tasks:needToDone],
                    [SettingString.Title:titleHasDone.uppercased(),SettingString.Tasks:hasDone]
                ]
                //
                var changed : Bool = false
                if(strongSelf.dragging == false){
                    changed = true
                }
                else{
                    if(strongSelf.wipTask.dataType == .normal){
                        //
                        strongSelf.dragging = false
                        changed = true
                    }
                }
                if(changed == true){
                    if(strongSelf.dragging == false){
                        var pendingSwap : Bool = true
                        if(strongSelf.needSwapWIP == false){
                            pendingSwap = false
                        }
                        else{
                            if(wipTask.dataType == .normal){
                                if(wipTask.taskId == strongSelf.wipTask.taskId){
                                    pendingSwap = false
                                    strongSelf.needSwapWIP = false
                                }
                                //
                            }
                        }
                        //
                        if(pendingSwap == false){
                            //
                            self?.wipTask = wipTask
                            self?.needToDoneTask = needToDone
                            //
                            self?.resetValueForView()
                            //
                            strongSelf.dataSourceTask = dataSource
                            strongSelf.dataSourceTempTask = dataSource
                            self?.resetValueForView()
                            //Passing data
                            strongSelf.focusModeEditorView?.dataSourceTaskItem = strongSelf.dataSourceTask
                        }
                        
                    }
                    else{
                        if(strongSelf.checkPropertyNeedChange(tasks1: strongSelf.needToDoneTask, tasks2: needToDone)){
                            strongSelf.dataSourceTempTask = dataSource
                            strongSelf.needReload = true
                        }
                        
                    }
                }
            }
        })
            .disposed(by: disposeBag)
    }
    
    func resetValueForView(){
        //
        var numNeedToDone = self.needToDoneTask.count
        if(self.needToDoneTask.count == 1){
            let task = self.needToDoneTask[0]
            if(task.dataType == .empty){
                numNeedToDone = 0
            }
        }
        
        let strNeedToDone = NSLocalizedString("need_to_be_done", comment: "").uppercased()
        lbTitleNeedToDone?.text = strNeedToDone + " (" + String(numNeedToDone) + ")"
        //
        if let strongLbTitleName = lbTitleNeedToDone, let strongViewNeedToDone = viewNeedToDone{
            let xTitleName : CGFloat = strongLbTitleName.frame.origin.x
            let wTitleName : CGFloat = strongViewNeedToDone.frame.size.width - xTitleName
            let wCurrentSize = strongLbTitleName.text!.getTextSizeWithString(wTitleName, strongLbTitleName.font)
            strongLbTitleName.frame.size.width = wCurrentSize.width
            //
            let padding = AppDevice.ScreenComponent.ItemPadding
            imvNeedToDone?.frame.origin.x = strongLbTitleName.frame.origin.x + strongLbTitleName.frame.size.width + padding
        }
        
        //
        setContentView(nil,isLoading: false)
    }
    
    func checkPropertyNeedChange(tasks1: [Task], tasks2: [Task]) -> Bool{
        var needChange : Bool = false
        if(tasks1.count == 0 && tasks1.count == tasks2.count){
            needChange = true
        }
        if(tasks1.count != tasks2.count){
            needChange = true
        }
        else{
            for task1 in tasks1{
                if let task2 = task1.checkIn(tasks: tasks2){
                    if(task1.name != task2.name || task1.completed != task2.completed || task1.wip != task2.wip){
                        needChange = true
                        break
                    }
                }
                else{
                    needChange = true
                    break
                }
            }
            //
            if(needChange == false){
                for task2 in tasks2{
                    if let task1 = task2.checkIn(tasks: tasks1){
                        if(task1.name != task2.name || task1.completed != task2.completed || task1.wip != task2.wip){
                            needChange = true
                            break
                        }
                    }
                    else{
                        needChange = true
                        break
                    }
                }
            }
        }
        return needChange
    }
}

extension FocusModeViewController: NantesLabelDelegate{
    // Link handling
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        if(link.absoluteString == GoToNext){
            if(self.needToDoneTask.count == 0){
                self.makeToastSystem(message:NSLocalizedString("no_found_task_need_to_be_done", comment: ""), duration: 2.0, position: .top)
            }
            else{
                let task = self.needToDoneTask[0]
                if(task.valid() == true){
                    self.dragging = true
                    self.wipTask = task
                    startUpdateWIP(retry: false)
                }
                else{
                    self.makeToastSystem(message:NSLocalizedString("no_found_task_need_to_be_done", comment: ""), duration: 2.0, position: .top)
                }
            }
        }
    }
}

