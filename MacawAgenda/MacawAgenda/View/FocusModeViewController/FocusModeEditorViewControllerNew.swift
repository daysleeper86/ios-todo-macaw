//
//  FocusModeEditorViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/15/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Moya

class FocusModeEditorViewControllerNew: BaseViewController {
    //Views
    var tbBacklog : UITableView = UITableView(frame: .zero)
    var viewLine : UIView?
    var lbTitleHeaderNeedToDone : UILabel?
    
    //Data
    var wipTask = TaskService.createEmptyTask()
    var agendaId : String = ""
    static let emptyTaskWIPCellIdentifier : String = "emptyTaskWIPCellIdentifier"
    static let emptyTaskNeedDoneCellIdentifier : String = "emptyTaskNeedDoneCellIdentifier"
    static let taskNotActionCellIdentifier : String = "taskNotActionCellIdentifier"
    var dragging : Bool = false
    var needReload : Bool = false
    var needChanged : Bool = false
    var dataSourceTask : [[String:Any]] = [
        [SettingString.Title:NSLocalizedString("work_in_progress", comment: "").uppercased(),SettingString.Tasks:[]],
        [SettingString.Title:NSLocalizedString("need_to_be_done", comment: "").uppercased(),SettingString.Tasks:[]],
        [SettingString.Title:NSLocalizedString("done", comment: "").uppercased(),SettingString.Tasks:[]]
        ]
    var dataSourceTaskItem : [[String:Any]] = []{
        didSet {
            dataSourceTask = dataSourceTaskItem
            // Update the view.
            setDataView()
        }
    }
    
    //ViewModels
    var subjectUser = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.userId ?? "")
    var subjectDate = BehaviorSubject<Date>(value: AppSettings.sharedSingleton.agenda_today?.date ?? Date())
    
    //Callback
    var exitCallback: ((_ isExit: Bool) -> Void)?
    var changeCallback: ((_ isOn: Bool) -> Void)?
    var updateTaskFinishCallback: ((_ taskId: String,_ completed: Bool) -> Void)?
    var updateWIPTaskCallback: ((_ task: Task) -> Void)?
    var retryCancelCallback: ((_ state: Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        //
        changeThemeData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func initView(){
        self.view.isOpaque = false
        self.view.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //Background View
        let yScrollView : CGFloat = AppDevice.IPhoneType.isIphoneX ? AppDevice.ScreenComponent.ItemPadding : 0
        let hScrollView : CGFloat = AppDevice.ScreenComponent.Height - yScrollView
        let bgView = UIView(frame: CGRect(x: 0, y: yScrollView, width: AppDevice.ScreenComponent.Width, height: hScrollView))
        bgView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.view.addSubview(bgView)
        
//        var yHeader : CGFloat = AppDevice.ScreenComponent.StatusHeight
        var yHeader : CGFloat = 0
        let hHeader : CGFloat = 44
        
        let headerView = UIView(frame: CGRect(x: 0, y: yHeader, width: bgView.frame.size.width, height: hHeader))
        headerView.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        bgView.addSubview(headerView)
        
        let pButtonExit : CGFloat = 8
        let wButtonExit : CGFloat = 60
        let hButtonExit = headerView.frame.size.height
//        let xButtonExit = headerView.frame.size.width - wButtonExit - pButtonExit
        let xButtonExit = pButtonExit
        let yButtonExit : CGFloat = 0
        
        let btnExit = UIButton.init(type: .custom)
        btnExit.frame = CGRect(x: xButtonExit, y: yButtonExit, width: wButtonExit, height: hButtonExit)
        btnExit.addTarget(self, action: #selector(exitAction), for: .touchUpInside)
        headerView.addSubview(btnExit)
        
        let pSwitch : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wSwitch : CGFloat = AppDevice.SubViewFrame.SwitchWidth
        let hSwitch : CGFloat = AppDevice.SubViewFrame.SwitchHeight
        let xSwitch : CGFloat = headerView.frame.size.width - wSwitch - pSwitch
        let ySwitch : CGFloat = 0
        
        let btnDarkMode = UIButton.init(type: .custom)
        btnDarkMode.frame = CGRect(x: xSwitch, y: ySwitch, width: wSwitch, height: hSwitch)
        btnDarkMode.addTarget(self, action: #selector(darkModeAction), for: .touchUpInside)
        headerView.addSubview(btnDarkMode)
    
        //
        yHeader += hHeader
        //
        let xViewLine : CGFloat = 0
        let yViewLine : CGFloat = yHeader
        let wViewLine : CGFloat = bgView.frame.size.width
        let hViewLine : CGFloat = 1
        
        viewLine = UIView(frame: CGRect(x: xViewLine, y: yViewLine, width: wViewLine, height: hViewLine))
        bgView.addSubview(viewLine!)
        
        yHeader += hViewLine
        //
        let xTableBacklog : CGFloat = 0
        let yTableBacklog : CGFloat = yHeader
        let wTableBacklog : CGFloat = bgView.frame.size.width
        let hTableBacklog : CGFloat = bgView.frame.size.height - yTableBacklog - 36
        tbBacklog.frame = CGRect(x: xTableBacklog, y: yTableBacklog, width: wTableBacklog, height: hTableBacklog)
        tbBacklog.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tbBacklog.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbBacklog.dataSource = self
        tbBacklog.delegate = self
        tbBacklog.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbBacklog.register(TaskWIPEmptyViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.emptyTaskWIPCellIdentifier)
        tbBacklog.register(TaskNeedDoneEmptyViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.emptyTaskNeedDoneCellIdentifier)
        tbBacklog.register(TeamAgendaTaskNotActionItemViewCell.self, forCellReuseIdentifier: FocusModeEditorViewControllerNew.taskNotActionCellIdentifier)
        tbBacklog.register(TeamAgendaTaskItemViewCell.self, forCellReuseIdentifier: TeamAgendaPlanningBacklogViewController.taskCellIdentifier)
        
        
        bgView.addSubview(tbBacklog)
        
    }
    
    func setDataView(){
        tbBacklog.reloadData()
    }
    
    @objc func changeValue(_ sender: UISwitch) {
        let isOn = sender.isOn
        self.changeCallback?(isOn)
    }
    
    func changeThemeData(){
        //
        var backgroundViewColor : UIColor!
        var lineColor : UIColor!
        //
        if(AppSettings.sharedSingleton.isDarkMode == true){
            backgroundViewColor = AppColor.MicsColors.DARK_MENU_COLOR
            lineColor = AppColor.MicsColors.LINE_COLOR_DARK
        }
        else{
            backgroundViewColor = AppColor.NormalColors.WHITE_COLOR
            lineColor = AppColor.MicsColors.LINE_COLOR
        }
        //
        viewLine?.backgroundColor = lineColor
        tbBacklog.backgroundColor = backgroundViewColor
        tbBacklog.reloadData()
    }
    
    @objc func exitAction(){
        self.exitCallback?(true)
    }
    
    @objc func darkModeAction(){
        self.changeCallback?(true)
    }
    
    func getVisibleCellFromTask(taskId: String) -> UITableViewCell?{
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : UITableViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
            else if let taskCell = cell as? TeamAgendaTaskItemViewCell{
                if(taskCell.task?.taskId == taskId){
                    visibleCell = taskCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingTaskCell(taskId: String){
        if let visibleCell = getVisibleCellFromTask(taskId: taskId){
            if let taskCell = visibleCell as? TeamAgendaTaskNotActionItemViewCell{
                taskCell.stopProcessing()
            }
            else if let taskCell = visibleCell as? TeamAgendaTaskItemViewCell{
                taskCell.stopProcessingFinishTask()
            }
        }
    }
    
    func stopProcessingForAllVisibleCell_NoDone(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
                taskCell.stopProcessing()
            }
        }
    }
    
    func stopProcessingForAllVisibleCell_HasDone(){
        let paths = tbBacklog.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbBacklog.cellForRow(at: path)
            if let taskCell = cell as? TeamAgendaTaskItemViewCell{
                taskCell.stopProcessingFinishTask()
            }
        }
    }
    
    func processedWIPTask(loginResult: LoginResult){
        switch loginResult {
        case .failed(let output):
            let appDelegate = AppDelegate().sharedInstance()
            if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                self.stopProcessingForAllVisibleCell_NoDone()
                //
                self.updateFailedStateWIP(state: true)
            }
        case .ok(let output):
            if let reponseAPI = output as? ResponseAPI{
                if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                    if(reponseAPI.code == 200){
                        
                    }
                    else{
                        self.updateFailedStateWIP(state: true)
                    }
                    //
                    self.stopProcessingTaskCell(taskId: reponseAPI.data)
                }
            }
        }
    }
    
    func processedTaskFinish(loginResult: LoginResult){
        switch loginResult {
        case .failed(let output):
            let appDelegate = AppDelegate().sharedInstance()
            if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                self.stopProcessingForAllVisibleCell_NoDone()
                self.stopProcessingForAllVisibleCell_HasDone()
            }
            
        case .ok(let output):
            if let reponseAPI = output as? ResponseAPI{
                if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                    //
                    self.stopProcessingTaskCell(taskId: reponseAPI.data)
                }
            }
        }
    }
    
    @objc func checkboxAction(sender: UIButton){
        let index = sender.tag
        //
        if(self.dragging == false){
            let indexPath = IndexPath(row: index, section: FocusModeViewController.doneSection)
            if let taskCell = tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskItemViewCell, let task = taskCell.task{
                taskCell.startProcessingFinishTask(startAnimationRow: !AppSettings.sharedSingleton.isDarkMode)
                //
                self.updateTaskFinishCallback?(task.taskId, false)
            }
        }
    }
    
    @objc func errorAction(sender: UIButton){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("try_again",comment:""), style: .default, handler: {[weak self] (action) in
            let index = sender.tag
            //
            guard let strongSelf = self else { return }
            if(strongSelf.dragging == true){
                strongSelf.retryCancelCallback?(0)
                strongSelf.retry_WIP(index: index)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("remove",comment:""), style: .destructive, handler: {[weak self] (action) in
            //
            guard let strongSelf = self else { return }
            strongSelf.retryCancelCallback?(1)
        }))
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel",comment:""), style: .cancel, handler: {(action) in
            
        }))
        self.present(actionSheet, animated: true)
        
    }
    
    func retry_WIP(index: Int){
        let indexPath = IndexPath(row: index, section: FocusModeViewController.wipSection)
        if let taskCell = tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
            //
            updateFailedStateWIP(state: false)
            startUpdateWIP(task: task)
        }
    }
    
    func moveToWIPAction(fromRow: Int, toRow: Int, task: Task){
        //
        let indexPath = IndexPath(row: fromRow, section: FocusModeViewController.needtoDoneSection)
        let newIndexPath = IndexPath(row: toRow, section: FocusModeViewController.wipSection)
        let sectionData = dataSourceTask[indexPath.section]
        if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
            var item = tasksData[indexPath.row]
            tasksData.remove(at: indexPath.row)
            dataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
            
            //
            var emptyData : Bool = false
            let sectionDataNew = dataSourceTask[newIndexPath.section]
            if var tasksDataNew = sectionDataNew[SettingString.Tasks] as? [Task]{
                let itemNew = tasksDataNew[newIndexPath.row]
                //
                if(itemNew.dataType == .empty){
                    emptyData = true
                }
                else{
//                    tasksDataNew.remove(at: newIndexPath.row)
                }
                //                tasksDataNew.remove(at: 0)
                item.wip = true
                tasksDataNew.insert(item, at: newIndexPath.row)
                dataSourceTask[newIndexPath.section][SettingString.Tasks] = tasksDataNew
            }
            CATransaction.begin()
            tbBacklog.beginUpdates()
            CATransaction.setCompletionBlock {[weak self] in
                self?.deleteEmptyRow(task: task, emptyData: emptyData,newIndex: indexPath.row)
            }
            tbBacklog.moveRow(at: indexPath, to: newIndexPath)
            tbBacklog.endUpdates()
            CATransaction.commit()
            
        }
    }
    
    func deleteEmptyRow(task: Task,emptyData: Bool,newIndex: Int){
        CATransaction.begin()
        tbBacklog.beginUpdates()
        CATransaction.setCompletionBlock {[weak self] in
            //
            guard let strongSelf = self else { return }
            let indexPath = IndexPath(row: 0, section: FocusModeViewController.wipSection)
            if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell{
                //
                self?.setNormalColorCell(cell: taskCell)
            }
            strongSelf.startUpdateWIP(task: task)
        }
        //
        if(emptyData == true){
            let sectionData = dataSourceTask[FocusModeViewController.wipSection]
            let newIndexPath = IndexPath(row: 1, section: FocusModeViewController.wipSection)
            if var tasksDataNew = sectionData[SettingString.Tasks] as? [Any]{
                tasksDataNew.remove(at: newIndexPath.row)
                //
                dataSourceTask[newIndexPath.section][SettingString.Tasks] = tasksDataNew
                tbBacklog.deleteRows(at: [newIndexPath], with: .fade)
            }
            //
            let sectionData1 = dataSourceTask[FocusModeViewController.needtoDoneSection]
            if var tasksDataNew1 = sectionData1[SettingString.Tasks] as? [Any]{
                var titleNeedToDone = NSLocalizedString("need_to_be_done", comment: "")
                titleNeedToDone = titleNeedToDone + " (" + String(tasksDataNew1.count) + ")"
                //
                lbTitleHeaderNeedToDone?.text = titleNeedToDone
                //
                if(tasksDataNew1.count == 0){
                    let newIndexPath1 = IndexPath(row: 0, section: FocusModeViewController.needtoDoneSection)
                    //
                    let task = TaskService.createEmptyTask()
                    tasksDataNew1.append(task)
                    //
                    dataSourceTask[newIndexPath1.section][SettingString.Tasks] = tasksDataNew1
                    tbBacklog.insertRows(at: [newIndexPath1], with: .top)
                }
            }
            
        }
        else{
            let sectionData = dataSourceTask[FocusModeViewController.wipSection]
            let indexPath = IndexPath(row: 1, section: FocusModeViewController.wipSection)
            if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
                let item = tasksData[indexPath.row]
                tasksData.remove(at: indexPath.row)
                dataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
                //
                let newIndexPath = IndexPath(row: newIndex, section: FocusModeViewController.needtoDoneSection)
                let sectionDataNew = dataSourceTask[newIndexPath.section]
                if var tasksDataNew = sectionDataNew[SettingString.Tasks] as? [Task]{
                    tasksDataNew.insert(item, at: newIndexPath.row)
                    dataSourceTask[newIndexPath.section][SettingString.Tasks] = tasksDataNew
                }
                tbBacklog.moveRow(at: indexPath, to: newIndexPath)
            }
        }
        
        tbBacklog.endUpdates()
        CATransaction.commit()
    }
    
    func startUpdateWIP(task: Task){
        //
        let indexPath = IndexPath(row: 0, section: FocusModeViewController.wipSection)
        if let taskCell = tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell{
            taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR, startAnimationRow:  !AppSettings.sharedSingleton.isDarkMode
            )
        }
        self.updateWIPTaskCallback?(task)
        
    }
    
    func updateFailedStateWIP(state: Bool){
        let sectionData = dataSourceTask[FocusModeViewController.wipSection]
        let indexPath = IndexPath(row: 0, section: FocusModeViewController.wipSection)
        if var tasksData = sectionData[SettingString.Tasks] as? [Task]{
            var item = tasksData[indexPath.row]
            item.errorType = state
            //
            tasksData[indexPath.row] = item
            dataSourceTask[indexPath.section][SettingString.Tasks] = tasksData
            //
            tbBacklog.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    
    deinit {
        // Release all resources
        // perform the deinitialization
        exitCallback = nil
        changeCallback = nil
        updateTaskFinishCallback = nil
        updateWIPTaskCallback = nil
        retryCancelCallback = nil
    }
    
}

extension FocusModeEditorViewControllerNew: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionData = dataSourceTask[indexPath.section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let element = tasksData[indexPath.row]
            if(element.dataType == .empty){
                if(indexPath.section == FocusModeViewController.needtoDoneSection){
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.emptyTaskNeedDoneCellIdentifier, for: indexPath) as? TaskNeedDoneEmptyViewCell else {
                        return TaskNeedDoneEmptyViewCell()
                    }
                    let itemWidth = tableView.frame.size.width
                    let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                    cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                    cell.selectionStyle = .none
                    //
                    cell.setDarkModeView(AppSettings.sharedSingleton.isDarkMode)
                    //
                    return cell
                }
                else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.emptyTaskWIPCellIdentifier, for: indexPath) as? TaskWIPEmptyViewCell else {
                        return TaskWIPEmptyViewCell()
                    }
                    cell.selectionStyle = .none
                    cell.delegate = self
                    //
                    cell.setDarkModeView(AppSettings.sharedSingleton.isDarkMode)
                    //
                    return cell
                }
            }
            else{
                if(element.completed == false){
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  FocusModeEditorViewControllerNew.taskNotActionCellIdentifier, for: indexPath) as? TeamAgendaTaskNotActionItemViewCell else {
                        return TeamAgendaTaskNotActionItemViewCell()
                    }
                    //
                    let itemWidth = tableView.frame.size.width
                    let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                    cell.initView(itemWidth: itemWidth, contentPadding: contentPadding)
                    cell.selectionStyle = .none
                    //
                    cell.delegate = self
                    //
                    cell.setValueForCell(task: element)
                    //
                    cell.errorButton?.tag = indexPath.row
                    cell.errorButton?.addTarget(self, action: #selector(self.errorAction(sender:)), for: .touchUpInside)
                    //
                    var titleColor : UIColor!
                    var darkMode = false
                    if(AppSettings.sharedSingleton.isDarkMode == true){
                        darkMode = true
                        //
                        if(indexPath.section == 0){
                            titleColor = AppColor.NormalColors.WHITE_COLOR
                        }
                        else{
                            titleColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
                        }
                    }
                    else{
                        titleColor = AppColor.NormalColors.BLACK_COLOR
                    }
                    cell.setDarkModeView(darkMode, titleColor:titleColor)
                    //
                    return cell
                }
                else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamAgendaPlanningBacklogViewController.taskCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
                        return TeamAgendaTaskItemViewCell()
                    }
                    //
                    let itemWidth = tableView.frame.size.width
                    let contentPadding = AppDevice.ScreenComponent.ItemPadding * 2
                    
                    cell.initView(itemWidth: itemWidth,contentPadding : contentPadding)
                    cell.selectionStyle = .none
                    cell.setValueForCell(task: element, isCanFinished: true)
                    //
                    cell.btnCheckbox?.tag = indexPath.row
                    cell.btnCheckbox?.addTarget(self, action: #selector(self.checkboxAction(sender:)), for: .touchUpInside)
                    //
                    setNormalColorCell(cell: cell)
                    //
                    return cell
                }
            }
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  TeamAgendaPlanningBacklogViewController.taskCellIdentifier, for: indexPath) as? TeamAgendaTaskItemViewCell else {
                return TeamAgendaTaskItemViewCell()
            }
            //
            //
            setNormalColorCell(cell: cell)
            //
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceTask.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionData = dataSourceTask[section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            return tasksData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionData = dataSourceTask[section]
        var headerView : UIView?
        var isRenderHeader : Bool = false
        if(section == FocusModeViewController.needtoDoneSection){
            isRenderHeader = true
        }
        else{
            if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
                if(tasksData.count > 0){
                    isRenderHeader = true
                }
            }
        }
        if (isRenderHeader == true){
            let wHeader : CGFloat = tableView.frame.size.width
            let hHeader : CGFloat = 48
            
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: wHeader, height: hHeader))
            headerView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
            //
            let pTopTitleName : CGFloat = 45
            let xTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
            let wTitleName : CGFloat = wHeader - (xTitleName * 2)
            let hTitleName : CGFloat = 36
            
            let titleHeader = sectionData[SettingString.Title] as? String
            let lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: pTopTitleName, width: wTitleName, height: hTitleName))
            lbTitleName.text = titleHeader
            lbTitleName.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
            headerView?.addSubview(lbTitleName)
            //
            if(section == FocusModeViewController.needtoDoneSection){
                lbTitleHeaderNeedToDone = lbTitleName
            }
            //
            var backgroundViewColor : UIColor!
            var titleHeaderColor : UIColor!
            if(AppSettings.sharedSingleton.isDarkMode == true){
                backgroundViewColor = AppColor.MicsColors.DARK_MENU_COLOR
                //
                titleHeaderColor = AppColor.NormalColors.BLUE_COLOR
            }
            else{
                backgroundViewColor = AppColor.NormalColors.WHITE_COLOR
                titleHeaderColor = AppColor.NormalColors.DARK_COLOR
            }
            headerView?.backgroundColor = backgroundViewColor
            lbTitleName.textColor = titleHeaderColor
        }
        //
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var heightRow : CGFloat = 0
        let sectionData = dataSourceTask[indexPath.section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let element = tasksData[indexPath.row]
            if(element.dataType == .empty){
                heightRow = AppDevice.TableViewRowSize.TaskWIPHeight
            }
            else{
                heightRow = AppDevice.TableViewRowSize.TaskHeight
            }
        }
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var hHeader : CGFloat = 0
        if(section == FocusModeViewController.needtoDoneSection){
            hHeader = 45 + 36
        }
        else{
            let sectionData = dataSourceTask[section]
            if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
                if(tasksData.count > 0){
                    hHeader = 45 + 36
                }
            }
        }
        return hHeader
    }
}
extension FocusModeEditorViewControllerNew: SwipeTableViewCellDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let sectionData = dataSourceTask[indexPath.section]
        if let tasksData = sectionData[SettingString.Tasks] as? [Task]{
            let element = tasksData[indexPath.row]
            
            var workAction : SwipeAction?
            if(element.wip == false){
                workAction = SwipeAction(style: .default, title: NSLocalizedString("working_on_it", comment: "")) { [weak self] action, indexPath in
                    // handle action by updating model with deletion
                    guard let strongSelf = self else { return }
                    if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
//                        taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR)
                        if(strongSelf.dragging == false){
                            //
                            taskCell.hideSwipe(animated: true)
                            strongSelf.setNormalColorCell(cell: taskCell)
                            //
                            strongSelf.moveToWIPAction(fromRow: indexPath.row, toRow: 0, task: task)
                        }
                        else{
                            //
                            taskCell.hideSwipe(animated: true)
                        }
                    }
                }
            }
            else{
                workAction = SwipeAction(style: .default, title: NSLocalizedString("mask_as_done", comment: "")) { [weak self] action, indexPath in
                    // handle action by updating model with deletion
                    guard let strongSelf = self else { return }
                    if let taskCell = strongSelf.tbBacklog.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell, let task = taskCell.task{
                        if(strongSelf.dragging == false){
                            taskCell.startProcessing(color: AppColor.NormalColors.BLUE_COLOR, startAnimationRow:  !AppSettings.sharedSingleton.isDarkMode)
                            //
                            taskCell.hideSwipe(animated: true)
                            //
                            strongSelf.updateTaskFinishCallback?(task.taskId,true)
                        }
                        else{
                            //
                            taskCell.hideSwipe(animated: true)
                        }
                    }
                }
            }
            //
            workAction!.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
            workAction!.textColor = AppColor.NormalColors.WHITE_COLOR
            workAction!.backgroundColor = AppColor.NormalColors.BLUE_COLOR
            
            return [workAction!]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 170
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        //
        var swipeColor : UIColor!
        if(AppSettings.sharedSingleton.isDarkMode == true){
            swipeColor = AppColor.MicsColors.DISABLE_DARK_COLOR
        }
        else{
            swipeColor = AppColor.MicsColors.DISABLE_COLOR
        }
        cell.setBackGroundColor(color: swipeColor)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? TeamAgendaTaskNotActionItemViewCell else { return }
        //
        setNormalColorCell(cell: cell)
    }
    
    func setNormalColorCell(cell: UITableViewCell){
        //
        var swipeColor : UIColor!
        if(AppSettings.sharedSingleton.isDarkMode == true){
            swipeColor = AppColor.MicsColors.DARK_MENU_COLOR
        }
        else{
            swipeColor = AppColor.NormalColors.WHITE_COLOR
        }
        //
        if let taskCell = cell as? TeamAgendaTaskNotActionItemViewCell{
            taskCell.setBackGroundColor(color: swipeColor)
        }
        else if let taskCell = cell as? TeamAgendaTaskItemViewCell{
            taskCell.setBackGroundColor(color: swipeColor)
        }
        
        
    }
}

extension FocusModeEditorViewControllerNew : TaskWIPEmptyViewCellDelegate{
    func taskWIPEmptyViewCellNextAction(_ taskWIPEmptyViewCell: TaskWIPEmptyViewCell){
        //
        let sectionData = dataSourceTask[FocusModeViewController.needtoDoneSection]
        var emptyNeedToDone : Bool = true
        //
        if let needToDoneTask = sectionData[SettingString.Tasks] as? [Task]{
            if(needToDoneTask.count > 0){
                let task = needToDoneTask[0]
                if(task.valid() == true){
                    emptyNeedToDone = false
                    moveToWIPAction(fromRow: 0, toRow: 0, task: task)
                }
            }
        }
        //
        if(emptyNeedToDone == true){
            self.makeToastSystem(message:NSLocalizedString("no_found_task_need_to_be_done", comment: ""), duration: 2.0, position: .top)
        }
    }
}

