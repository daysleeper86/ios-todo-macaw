//
//  BaseViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import NVActivityIndicatorView

class BaseViewController: UIViewController {

    let disposeBag = DisposeBag()
    private var mbProgress : MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func presentHUDWating(color : UIColor?,yFrame : CGFloat,showAnimation : Bool, isLockScreen : Bool) {
        // do a post request and return post data
        DispatchQueue.main.async {
            // Run UI Updates or call completion block
            if(self.mbProgress == nil){
                if(isLockScreen == true){
                    self.mbProgress = MBProgressHUD(view:self.view.window)
                }
                else{
                    self.mbProgress = MBProgressHUD(view:self.view)
                }
                self.mbProgress?.labelText = ""
                self.mbProgress?.dimBackground = true
                self.mbProgress?.color = UIColor.clear
                self.mbProgress?.activityIndicatorColor = UIColor.lightGray
            }
            if let strongProgress = self.mbProgress{
                strongProgress.yOffset_ABS = yFrame;
                if(color != nil){
                    strongProgress.activityIndicatorColor = color
                }
                self.view.addSubview(strongProgress)
                self.view.bringSubviewToFront(strongProgress)
                strongProgress.show(showAnimation)
            }
        }
        
    }
    
    func dismissHUDWating(animation : Bool){
        DispatchQueue.main.async {
            // Run UI Updates or call completion block
            self.mbProgress?.hide(animation)
        }
        
    }
    
    func makeToast(message: String,duration: TimeInterval, position: ToastPosition){
        var toastStyle = ToastStyle()
        toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
        toastStyle.cornerRadius = 4.0
        toastStyle.messageAlignment = .center
        toastStyle.messageFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14)
        toastStyle.backgroundColor = AppColor.MicsColors.TOAST_LIGHT_COLOR
        toastStyle.messageColor = AppColor.NormalColors.WHITE_COLOR
        toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
        self.view.window?.makeToast(message, duration: duration, position: position, style: toastStyle)
    }
    
    func makeToastSystem(message: String,duration: TimeInterval, position: ToastPosition){
        var toastStyle = ToastStyle()
        toastStyle.verticalPadding = AppDevice.ScreenComponent.ItemPadding
        toastStyle.cornerRadius = 4.0
        toastStyle.messageAlignment = .center
        toastStyle.messageFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14)
        toastStyle.fixedWidth = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
        //
        if(AppSettings.sharedSingleton.isDarkMode == true){
            toastStyle.backgroundColor = AppColor.MicsColors.TOAST_DARK_COLOR
            toastStyle.messageColor = AppColor.NormalColors.BLACK_COLOR
        }
        else{
            toastStyle.backgroundColor = AppColor.MicsColors.TOAST_LIGHT_COLOR
            toastStyle.messageColor = AppColor.NormalColors.WHITE_COLOR
        }
        //
        AppDelegate().sharedInstance().window?.makeToast(message, duration: duration, position: position, style: toastStyle)
    }
}

extension BaseViewController : NVActivityIndicatorViewable{
    func startLoading(message: String){
        if(NVActivityIndicatorPresenter.sharedInstance.isAnimating == false){
            let size = CGSize(width: 80, height: 80)
            startAnimating(size, message: message + "...", messageFont: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 20 ),type: .ballClipRotate, fadeInAnimation: nil)
        }
    }
    
    func stopLoading(){
        if(NVActivityIndicatorPresenter.sharedInstance.isAnimating == true){
            stopAnimating(nil)
        }
    }
}
