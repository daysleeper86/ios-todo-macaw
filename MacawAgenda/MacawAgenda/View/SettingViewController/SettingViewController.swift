//
//  SettingViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Firebase
import RxFirebaseAuthentication
import FirebaseAuth
import Material
import Moya
import RxSwift

class SettingViewController: BaseViewController {
    
    //Views
    private var tbSettings : UITableView?
    private var userView : UIView!
    private var imvAvatar : UIImageView?
    private var lbTitleName : UILabel?
    private var lbTitleEmail : UILabel?
    private var lbTitleUpgrade : UILabel?
    
    //Data
    private let settingCellIdentifier : String = "settingCellIdentifier"
    private let orangnizationProfileCell : String = "orangnizationProfileCell"
    private let arraySettings : [[String : Any]] = [
    [Constant.keyId:Constant.SettingMenu.kSettingMenuChangePass,SettingString.Icon:"ic_menu_change_password",SettingString.Title:NSLocalizedString("change_password", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLACK_COLOR],
        [Constant.keyId:Constant.SettingMenu.kSettingMenuNotification,SettingString.Icon:"ic_menu_settings",SettingString.Title:NSLocalizedString("notifications", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLACK_COLOR],
        [Constant.keyId:Constant.SettingMenu.kSettingMenuInvite,SettingString.Icon:"ic_menu_invite",SettingString.Title:NSLocalizedString("invite_member", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLUE_COLOR],
        
        [Constant.keyId:Constant.SettingMenu.kSettingMenuOrangnization,SettingString.Icon:"ic_menu_invite",SettingString.Title:NSLocalizedString("invite_member", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLUE_COLOR],
        [Constant.keyId:Constant.SettingMenu.kSettingMenuSignout,SettingString.Icon:"ic_menu_signout",SettingString.Title:NSLocalizedString("sign_out", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLACK_COLOR]
        
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        addNotification()
        // Do any additional setup after loading the view.
        initView()
        //
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func addNotification(){
        addNotification_UpdateProfile()
        addNotification_UpdateProject()
        addNotification_UpdateOrangnization()
    }
    
    func addNotification_UpdateProfile(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProfile(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_USER_INFO),
                                               object: nil)
    }
    
    @objc func updateProfile(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProfileUpdateInfoType{
                if(updatedType == .kProfileUpdateAvatar){
                    loadAvatar()
                }
                else if(updatedType == .kProfileUpdateInfo){
                    loadInfo()
                }
                else if(updatedType == .kProfileUpdateAll){
                    loadUserInfo()
                }
            }
        }
    }
    
    func addNotification_UpdateProject(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProject(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_PLANNING_TYPE),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProject(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_INFO),
                                               object: nil)
    }
    
    @objc func updateProject(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProjectUpdateInfoType{
                if(updatedType == .kProjecUpdatePlanningType){
                    reloadProjectPlanningType()
                }
                else if(updatedType == .kProjecUpdateInfo){
                    tbSettings?.reloadData()
                }
            }
        }
    }
    
    func addNotification_UpdateOrangnization(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateOrangnization(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_ORANGNIZATION_INFO),
                                               object: nil)
    }
    
    @objc func updateOrangnization(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let orangnization = dictionary[ModelDataKeyString.COLLECTION_ORANGNIZATION] as? Orangnization{
                tbSettings?.reloadData()
            }
        }
    }
    
    func removeNotification(){
        NotificationCenter.default.removeObserver(self)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let wButton : CGFloat = 120
        let hButton : CGFloat = hContentHeaderView
        let pBack : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xBack : CGFloat = pBack
        let yBack : CGFloat = yContentHeaderView
        
        let btnBack = IconButton.init(type: .custom)
        btnBack.frame = CGRect(x: xBack, y: yBack, width: wButton, height: hButton)
        btnBack.contentHorizontalAlignment = .left
        btnBack.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnBack.setTitle(" " + NSLocalizedString("collaborate", comment: ""), for: .normal)
        btnBack.setTitleColor(AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, for: .normal)
        btnBack.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        btnBack.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationView.addSubview(btnBack)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xBGView : CGFloat = 0
        let yBGView : CGFloat = yHeader
        let wBGView : CGFloat = AppDevice.ScreenComponent.Width
        let hBGView : CGFloat = AppDevice.ScreenComponent.Height - AppDevice.ScreenComponent.TabbarHeight - yBGView
        
        let bgView = UIView(frame: CGRect(x: xBGView, y: yBGView, width: wBGView, height: hBGView))
        bgView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(bgView)
        
        var yViewScreen : CGFloat = 0
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = wBGView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 4.0
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("settings", comment: "")
        bgView.addSubview(lbTitleScreen)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        let xTableSetting : CGFloat = 0
        let yTableSetting : CGFloat = yViewScreen
        let wTableSetting : CGFloat = bgView.frame.size.width
        let hTableSetting : CGFloat = bgView.frame.size.height - yTableSetting
        tbSettings = UITableView(frame: CGRect(x: xTableSetting, y: yTableSetting, width: wTableSetting, height: hTableSetting))
        tbSettings?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.dataSource = self
        tbSettings?.delegate = self
        tbSettings?.register(SettingItemViewCell.self, forCellReuseIdentifier: settingCellIdentifier)
        tbSettings?.register(TeamOrangnizationProfileCell.self, forCellReuseIdentifier: orangnizationProfileCell)
        bgView.addSubview(tbSettings!)
    }
    
    func initView(){
        //
//        let yScreen : CGFloat = createHeaderView()
        let yScreen : CGFloat = AppDevice.ScreenComponent.StatusHeight
        createContentView(yHeader: yScreen)
    }
    
    func bindToViewModel(){
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadUserInfo(){
        loadAvatar()
        loadInfo()
    }
    
    func loadAvatar(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let userName = AppSettings.sharedSingleton.account?.name ?? ""
        var user = User(userId: userId, dataType: .normal)
        user.name = userName
        //
        imvAvatar?.loadAvatarForUser(user: user)
    }
    
    func loadInfo(){
        let userName = AppSettings.sharedSingleton.account?.name ?? ""
        let userEmail = AppSettings.sharedSingleton.account?.email ?? ""
        lbTitleName?.text = userName
        lbTitleEmail?.text = userEmail
        //
        reloadProjectPlanningType()
    }
    
    func reloadProjectPlanningType(){
        //
        let projectUserPlanningType = AppSettings.sharedSingleton.project?.projectUserPlanType ?? .kProjectUserPlanFree
        let orangnizationUserPlanningType = AppSettings.sharedSingleton.orangnization?.orangnizationUserPlanType ?? .kProjectUserPlanFree
        if(projectUserPlanningType == .kProjectUserPlanFree && orangnizationUserPlanningType == .kProjectUserPlanFree){
            lbTitleUpgrade?.removeFromSuperview()
        }
        else{
            if let strongLbTitleName = lbTitleName{
                var stringTitleUpgrade = ""
                if(orangnizationUserPlanningType == .kProjectUserPlanTrial || projectUserPlanningType == .kProjectUserPlanTrial){
                    stringTitleUpgrade = NSLocalizedString("trial", comment: "")
                }
                else{
                    stringTitleUpgrade = NSLocalizedString("pro", comment: "")
                }
                //
                let xTitleName : CGFloat = strongLbTitleName.frame.origin.x
                var wTitleName : CGFloat = userView.frame.size.width - xTitleName
                let wCurrentSize = strongLbTitleName.text!.getTextSizeWithString(wTitleName, strongLbTitleName.font)
                let wTitleUpgrade : CGFloat = 36
                let hTitleUpgrade : CGFloat = 24
                let yTitleUpgrade = strongLbTitleName.frame.origin.y - 3
                var xTitleUpgrade : CGFloat = 0
                if(xTitleName + wCurrentSize.width + wTitleUpgrade > userView.frame.size.width){
                    xTitleUpgrade = userView.frame.size.width - wTitleUpgrade
                    wTitleName = xTitleUpgrade - 8 - xTitleName
                }
                else{
                    wTitleName = wCurrentSize.width
                    xTitleUpgrade = xTitleName + wTitleName + 8
                }
                lbTitleName?.frame.size.width = wTitleName
                //
                if(lbTitleUpgrade == nil){
                    lbTitleUpgrade = UILabel(frame: CGRect(x: xTitleUpgrade, y: yTitleUpgrade, width: wTitleUpgrade, height: hTitleUpgrade))
                    lbTitleUpgrade?.accessibilityIdentifier = AccessiblityId.LB_USERTYPE
                    lbTitleUpgrade?.textAlignment = .center
                    lbTitleUpgrade?.textColor = AppColor.NormalColors.LIGHT_ORANGE_COLOR
                    lbTitleUpgrade?.layer.cornerRadius = 2
                    lbTitleUpgrade?.layer.borderWidth = 1
                    lbTitleUpgrade?.layer.borderColor = AppColor.NormalColors.LIGHT_ORANGE_COLOR.cgColor
                    lbTitleUpgrade?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 12)
                }
                else{
                    lbTitleUpgrade?.frame = CGRect(x: xTitleUpgrade, y: yTitleUpgrade, width: wTitleUpgrade, height: hTitleUpgrade)
                }
                lbTitleUpgrade?.text = stringTitleUpgrade
                userView.addSubview(lbTitleUpgrade!)
            }
        }
    }
    
    @objc func didTapUser(){
        let editProfileView = EditProfileViewController(nibName: nil, bundle: nil)
        self.navigationController?.pushViewController(editProfileView, animated: true)
    }
    
    @objc func changeAction(){
        DispatchQueue.main.async {
            let teamOrangnizationSelected  = TeamOrangnizationTeamSelectedViewController(nibName: nil, bundle: nil)
            teamOrangnizationSelected.delegate = self
            let overlayController = DTOverlayController(viewController: teamOrangnizationSelected,viewNotPan: [teamOrangnizationSelected.tbTeam])
            overlayController.overlayHeight = .dynamic(AppDevice.ScreenComponent.OverlayHeight1)
            overlayController.handleVerticalSpace = AppDevice.ScreenComponent.OverlayHandler
            overlayController.isHandleHidden = true
//            overlayController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            
            self.navigationController?.present(overlayController, animated: true, completion: nil)
        }
    }
    
    @objc func manageAction(){
        DispatchQueue.main.async {
            self.gotoOrangnization()
        }
    }
    
    func stateForUpdateOrangnization(enable: Bool){
        let indexPath = IndexPath(row: Constant.SettingMenu.kSettingMenuOrangnization.rawValue, section: 0)
        if let orangnizationCell = tbSettings?.cellForRow(at: indexPath) as? TeamOrangnizationProfileCell{
            if(enable == false){
                orangnizationCell.isUserInteractionEnabled =  false
                orangnizationCell.startShimmering_Cell()
            }
            else{
                orangnizationCell.isUserInteractionEnabled =  true
                orangnizationCell.stopShimmering_Cell()
            }
        }
    }
    
    func updateDefaultProjectChanged(_ projectId: String){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let accountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let accountController = AccountAPIController(provider: accountProvider)
        
        let accountViewModel = AccountAPIViewModel(
            inputUpdateProfileDefaultProject: (
                userId: userId,
                projectId: BehaviorSubject<String>(value: projectId),
                controller: accountController
            )
        )
        
        //
        accountViewModel.signingIn
            .subscribe(onNext: {[weak self] loading  in
                self?.stateForUpdateOrangnization(enable: !loading)
            })
            .disposed(by: disposeBag)
        //
        accountViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stateForUpdateOrangnization(enable: true)
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.makeToast(message:NSLocalizedString("change_team_successfully", comment: ""), duration: 3.0, position: .top)
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("change_team_unsuccessfully", comment: ""), duration: 3.0, position: .top)
                        }
                        //
                        self?.stateForUpdateOrangnization(enable: true)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func gotoOrangnization(){
        let teamOrangnizationViewController = TeamOrangnizationViewController(nibName: nil, bundle: nil)
        self.navigationController?.pushViewController(teamOrangnizationViewController, animated: true)
    }
    
    deinit {
        removeNotification()
    }
    
}

extension SettingViewController : TeamOrangnizationTeamSelectedViewControllerDelegate{
    func teamOrangnizationTeamSelectedViewControllerChange(_ teamOrangnizationTeamSelectedViewController: TeamOrangnizationTeamSelectedViewController, doneWith project: Project){
        //
        updateDefaultProjectChanged(project.projectId)
    }
}

extension SettingViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let hHeader : CGFloat = AppDevice.SubViewFrame.SettingUserHeaderPaddingTop + AppDevice.SubViewFrame.SettingUserHeaderHeight + AppDevice.SubViewFrame.SettingUserHeaderPaddingBottom
        return hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let xHeader : CGFloat = 0
        let yHeader : CGFloat = 0
        let wHeader : CGFloat = tableView.frame.size.width - (xHeader * 2)
        let hHeader : CGFloat = AppDevice.SubViewFrame.SettingUserHeaderPaddingTop + AppDevice.SubViewFrame.SettingUserHeaderHeight + AppDevice.SubViewFrame.SettingUserHeaderPaddingBottom
        
        let headerView = UIView(frame: CGRect(x: xHeader, y: yHeader, width: wHeader, height: hHeader))
        headerView.isUserInteractionEnabled = true
        headerView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Tap Background Event
        let btnHeaderView = RaisedButton.init(type: .custom)
        btnHeaderView.accessibilityIdentifier = AccessiblityId.BTN_USER
        btnHeaderView.frame = CGRect(x: 0, y: 0, width: wHeader, height: hHeader)
        btnHeaderView.pulseColor = AppColor.MicsColors.LINE_COLOR
        btnHeaderView.backgroundColor = .clear
        btnHeaderView.addTarget(self, action: #selector(didTapUser), for: .touchUpInside)
        headerView.addSubview(btnHeaderView)
        
        let xUserView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yUserView : CGFloat = AppDevice.SubViewFrame.SettingUserHeaderPaddingTop
        let wUserView : CGFloat = headerView.frame.size.width - (xUserView * 2)
        let hUserView : CGFloat = AppDevice.SubViewFrame.SettingUserHeaderHeight
        userView = UIView(frame: CGRect(x: xUserView, y: yUserView, width: wUserView, height: hUserView))
        userView.isUserInteractionEnabled = false
        headerView.addSubview(userView)
        
        let wAvatar : CGFloat = 46
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = 0
        let yAvatar : CGFloat = 0
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.isAccessibilityElement = true
        imvAvatar?.accessibilityIdentifier = AccessiblityId.IMG_USERAVATAR
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.contentMode = .scaleAspectFill
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        userView.addSubview(imvAvatar!)
        
        //Title Screen
        let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleName : CGFloat = xAvatar + wAvatar + pTitleName
        let yTitleName : CGFloat = 0
        let wTitleName : CGFloat = userView.frame.size.width - xTitleName
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let pBottomTitleName : CGFloat = 4
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.accessibilityIdentifier = AccessiblityId.LB_USERNAME
        lbTitleName?.textAlignment = .left
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        userView.addSubview(lbTitleName!)
        
        //Title Screen
        let xTitleEmail : CGFloat = xTitleName
        let yTitleEmail : CGFloat = yTitleName + hTitleName + pBottomTitleName
        let wTitleEmail : CGFloat = wTitleName
        let hTitleEmail : CGFloat = AppDevice.SubViewFrame.TitleHeight
        
        
        lbTitleEmail = UILabel(frame: CGRect(x: xTitleEmail, y: yTitleEmail, width: wTitleEmail, height: hTitleEmail))
        lbTitleEmail?.accessibilityIdentifier = AccessiblityId.LB_EMAIL
        lbTitleEmail?.textAlignment = .left
        lbTitleEmail?.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleEmail?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        userView.addSubview(lbTitleEmail!)
        
        //
        loadUserInfo()
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let setting = arraySettings[indexPath.row]
        let key = setting[Constant.keyId] as? Constant.SettingMenu ?? .kSettingMenuSignout
        //
        if(key == .kSettingMenuOrangnization){
            //
            let orangnization = AppSettings.sharedSingleton.orangnization
            //
            var hRow : CGFloat = 0
            hRow += AppDevice.SubViewFrame.OrangnizationCellPadding
            hRow += 1
            hRow += AppDevice.SubViewFrame.OrangnizationCellPadding
            if(orangnization != nil){
                hRow += AppDevice.SubViewFrame.OrangnizationIconWidth
                hRow += AppDevice.ScreenComponent.ItemPadding
            }
            hRow += AppDevice.ScreenComponent.NormalHeight
            hRow += AppDevice.SubViewFrame.OrangnizationCellPadding
            hRow += 1
            
            return hRow
        }
        return AppDevice.TableViewRowSize.SettingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let setting = arraySettings[indexPath.row]
        let key = setting[Constant.keyId] as? Constant.SettingMenu ?? .kSettingMenuSignout
        //
        if(key == .kSettingMenuOrangnization){
            let orangnization = AppSettings.sharedSingleton.orangnization
            let project = AppSettings.sharedSingleton.project
            //
            let cell = TeamOrangnizationProfileCell()
            cell.backgroundColor = AppColor.NormalColors.WHITE_COLOR
            cell.selectionStyle = .none
            let accessibilityIdentifier = AccessiblityId.TBCELL_ORANGNIZATION
            cell.accessibilityIdentifier = accessibilityIdentifier
            cell.initView(orangnization: orangnization, project: project)
            //
            cell.btnChange?.addTarget(self, action: #selector(changeAction), for: .touchUpInside)
            cell.btnManage?.addTarget(self, action: #selector(manageAction), for: .touchUpInside)
            cell.btnHeaderView?.addTarget(self, action: #selector(manageAction), for: .touchUpInside)
            //
            return cell
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier:  self.settingCellIdentifier, for: indexPath) as? SettingItemViewCell else {
                return SettingItemViewCell()
            }
            cell.selectionStyle = .none
            let setting = arraySettings[indexPath.row]
            let key = setting[Constant.keyId] as? Constant.SettingMenu ?? .kSettingMenuSignout
            //
            var accessibilityIdentifier = ""
            if(key == .kSettingMenuChangePass){
                accessibilityIdentifier = AccessiblityId.TBCELL_CHANGEPASS
            }
            else if(key == .kSettingMenuNotification){
                accessibilityIdentifier = AccessiblityId.TBCELL_NOTIFICATION
            }
            else if(key == .kSettingMenuInvite){
                accessibilityIdentifier = AccessiblityId.TBCELL_INVITE
            }
                
            else if(key == .kSettingMenuSignout){
                accessibilityIdentifier = AccessiblityId.TBCELL_LOGOUT
            }
            //
            cell.accessibilityIdentifier = accessibilityIdentifier
            cell.setValueForCell(setting: setting)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //
        let setting = arraySettings[indexPath.row]
        let key = setting[Constant.keyId] as? Constant.SettingMenu ?? .kSettingMenuSignout
        
        if(key == .kSettingMenuChangePass){
            let changePasswordView = ChangePasswordViewController(nibName: nil, bundle: nil)
            self.navigationController?.pushViewController(changePasswordView, animated: true)
        }
        else if(key == .kSettingMenuNotification){
            let settingNotificationView = SettingNotificationViewController(nibName: nil, bundle: nil)
            self.navigationController?.pushViewController(settingNotificationView, animated: true)
        }
        else if(key == .kSettingMenuInvite){
            let inviteMemberView = InviteMemberViewController(nibName: nil, bundle: nil)
            inviteMemberView.viewType = .update
            inviteMemberView.navigationType = .push
            self.navigationController?.pushViewController(inviteMemberView, animated: true)
        }
        else if(key == .kSettingMenuSignout){
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("sign_out",comment:""), style: .destructive, handler: { (action) in
                //
                Util.signOutCurrentAccount()
                //
                AppDelegate().sharedInstance().setupOnboardingView()
            }))
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel",comment:""), style: .cancel))
            present(actionSheet, animated: true)
        }
        
        
    }
}
