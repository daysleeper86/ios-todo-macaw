//
//  ChangePasswordViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import UserNotifications
import Material

class SettingNotificationViewController: BaseViewController{
    //View
    private var tbSettings : UITableView?
    private var keyboardBounds : CGRect?
    private var btnContinue : UIButton?
    //Data
    private let arraySettings : [[String : Any]] = [
    [Constant.keyId:Constant.SettingMenuNotification.kSettingMenuPushNotification,SettingString.Title:NSLocalizedString("push_notifications", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLACK_COLOR],
        [Constant.keyId:Constant.SettingMenuNotification.kSettingMenuEmailNotification,SettingString.Title:NSLocalizedString("email_notifications", comment: ""),SettingString.TitleColor:AppColor.NormalColors.BLACK_COLOR],
    ]
    private let settingNotificationCellIdentifier : String = "settingNotificationCellIdentifier"
    
    //ViewModel
    private var emailStatus = AppSettings.sharedSingleton.account?.emailStatus ?? false
    private var notificationStatus = AppSettings.sharedSingleton.account?.notificationStatus ?? false
    private var subjectDeviceToken = BehaviorSubject<String>(value: AppSettings.sharedSingleton.account?.deviceToken ?? "")
    private var subjectEmailStatus = BehaviorSubject<Bool>(value: AppSettings.sharedSingleton.account?.emailStatus ?? false)
    private var subjectNotificationStatus = BehaviorSubject<Bool>(value: AppSettings.sharedSingleton.account?.notificationStatus ?? false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        checkPermission()
        //
        initView()
        //
        bindToViewModel()
    }
    
    func addNotification() {
        //
        addNotification_ReceiveToken()
        addNotification_SettingNotification()
    }
    
    func addNotification_ReceiveToken(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(receiveToken(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_RECEIVE_DEVICE_TOKEN),
                                               object: nil)
    }
    
    @objc func receiveToken(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let token = dictionary[Constant.keyValue] as? String{
                subjectDeviceToken.onNext(token)
                //
                notificationStatus = true
                tbSettings?.reloadData()
                //
                subjectNotificationStatus.onNext(notificationStatus)
            }
        }
    }
    
    func addNotification_SettingNotification(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(settingNotification(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_SETTING_NOTIFICATION),
                                               object: nil)
    }
    
    @objc func settingNotification(notification: Notification) {
        emailStatus = AppSettings.sharedSingleton.account?.emailStatus ?? false
        notificationStatus = AppSettings.sharedSingleton.account?.notificationStatus ?? false
        tbSettings?.reloadData()
        //
        subjectEmailStatus.onNext(emailStatus)
        subjectNotificationStatus.onNext(notificationStatus)
    }
    
    func removeNotification(){
        NotificationCenter.default.removeObserver(self)
    }
    
    func checkPermission(){
        AppDelegate().sharedInstance().registerForPushNotifications()
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        let navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView)
        
        //Close Button
        let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
        let xClose : CGFloat = pClose
        let yClose : CGFloat = yContentHeaderView
        let hClose : CGFloat = hContentHeaderView
        let wClose : CGFloat = hClose
        
        let btnClose = IconButton.init(type: .custom)
        btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
        btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        navigationView.addSubview(btnClose)
        
        //Action Button
        let wAction : CGFloat = 80
        let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let hAction = hContentHeaderView
        let yAction = yContentHeaderView
        let xAction = navigationView.frame.size.width - wAction - pAction
        
        btnContinue = UIButton.init(type: .custom)
        btnContinue?.accessibilityIdentifier = AccessiblityId.BTN_SAVE
        btnContinue?.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
        btnContinue?.setTitle(NSLocalizedString("save", comment:"").uppercased(), for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
        btnContinue?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
        btnContinue?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
        btnContinue?.titleLabel?.textAlignment = .right
        btnContinue?.contentHorizontalAlignment = .right
        btnContinue?.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        navigationView.addSubview(btnContinue!)
        
        
        //Title Screen
        let pTitleHeader : CGFloat = 8
        let xTitleHeader : CGFloat = xClose + wClose + pTitleHeader
        let yTitleHeader : CGFloat = yContentHeaderView
        let wTitleHeader : CGFloat = navigationView.frame.size.width - (xTitleHeader * 2)
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("notifications", comment: "")
        navigationView.titleView(titleView: lbTitleHeader)
        navigationView.showHideTitleNavigation(isShow: true)
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let pTopSetting : CGFloat = 36
        let xTableSetting : CGFloat = 0
        let yTableSetting : CGFloat = yHeader
        let wTableSetting : CGFloat = AppDevice.ScreenComponent.Width
        let hTableSetting : CGFloat = AppDevice.ScreenComponent.Height - yTableSetting
        
        tbSettings = UITableView(frame: CGRect(x: xTableSetting, y: yTableSetting, width: wTableSetting, height: hTableSetting))
        tbSettings?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbSettings?.dataSource = self
        tbSettings?.delegate = self
        tbSettings?.register(SettingNotificationItemViewCell.self, forCellReuseIdentifier: settingNotificationCellIdentifier)
        self.view.addSubview(tbSettings!)
        
        let headerTableView = UIView(frame: CGRect(x: 0, y: 0, width: wTableSetting, height: pTopSetting))
        headerTableView.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        tbSettings?.tableHeaderView = headerTableView
    }
    
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    func enableDoneButton(_ valid: Bool){
        btnContinue?.isEnabled = valid
        if(valid == true){
            btnContinue?.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for:.normal)
        }
        else{
            btnContinue?.setTitleColor(AppColor.MicsColors.LINE_VIEW_NORMAL, for:.normal)
        }
    }
    
    func bindToViewModel(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectAccountProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let accountController = AccountAPIController(provider: projectAccountProvider)
        let observableTap = btnContinue!.rx.tap.asObservable()
        let accountViewModel = AccountAPIViewModel(
            inputUpdateSettingUser: (
                userId: userId,
                emailStatus: subjectEmailStatus.asObservable(),
                notificationStatus: subjectNotificationStatus.asObservable(),
                notificationToken:subjectDeviceToken.asObservable(),
                loginTaps: observableTap,
                controller: accountController
            )
        )
        //
        accountViewModel.signingIn
            .subscribe(onNext: {[weak self] loading  in
                self?.enableDoneButton(!loading)
                //
                if(loading == true){
                    self?.startLoading(message: NSLocalizedString("setting", comment: ""))
                }
                else{
                    self?.stopLoading()
                }
            })
            .disposed(by: disposeBag)
        //
        accountViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message:output.message, duration: 3.0, position: .top)
                    self?.stopLoading()
                }
                
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            self?.makeToast(message:NSLocalizedString("setting_notifications_successfully", comment: ""), duration: 3.0, position: .top)
                        }
                        else{
                            self?.makeToast(message:NSLocalizedString("setting_notifications_unsuccessfully", comment: ""), duration: 3.0, position: .top)
                        }
                        //
                        self?.stopLoading()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func didTapBackground(){
        
    }
    
    @objc func closeAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func continueAction(){
        
    }
    
    @objc func changeValue(_ sender: UISwitch) {
        let index = sender.tag
        let setting = arraySettings[index]
        if let keyId = setting[Constant.keyId] as? Constant.SettingMenuNotification{
            if(keyId == .kSettingMenuEmailNotification){
                let isOn = sender.isOn
                subjectEmailStatus.onNext(isOn)
            }
            else{
                let isOn = sender.isOn
                subjectNotificationStatus.onNext(isOn)
            }
        }
    }
    
    deinit {
        removeNotification()
    }
}

extension SettingNotificationViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AppDevice.TableViewRowSize.SettingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  self.settingNotificationCellIdentifier, for: indexPath) as? SettingNotificationItemViewCell else {
            return SettingNotificationItemViewCell()
        }
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        var accessibilityString = ""
        let setting = arraySettings[indexPath.row]
        var status : Bool = false
        if let keyId = setting[Constant.keyId] as? Constant.SettingMenuNotification{
            if(keyId == .kSettingMenuEmailNotification){
                status = emailStatus
                accessibilityString = AccessiblityId.SWITCH_EMAIL
            }
            else{
                status = notificationStatus
                accessibilityString = AccessiblityId.SWITCH_NOTIFICATION
            }
        }
        cell.setValueForCell(setting: setting,status: status)
        //
        cell.switchNotification?.tag = indexPath.row
        cell.accessibilityIdentifier = accessibilityString
        cell.switchNotification?.addTarget(self, action: #selector(changeValue(_:)), for: .valueChanged)
        
        return cell
    }
    
}
