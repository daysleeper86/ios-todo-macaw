//
//  SettingItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class SettingItemViewCell: UITableViewCell {
    
    //Views
    private var backgroundViewCell : UIView?
    private var imvAvatar : UIImageView?
    private var lbTitleName : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (AppDevice.ScreenComponent.ItemPadding * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.SettingHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let pAvatar : CGFloat = 0
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = pAvatar
        let yAvatar : CGFloat = (hBackgroundView-hAvatar)/2
        
        imvAvatar = UIImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.layer.masksToBounds = true
        backgroundViewCell?.addSubview(imvAvatar!)
        
        // Title name
        let pTitleName : CGFloat = 8
        let xTitleName : CGFloat = xAvatar + wAvatar + pTitleName
        let yTitleName : CGFloat = 0
        let wTitleName : CGFloat = wBackgroundView - xTitleName
        let hTitleName : CGFloat = hBackgroundView
        
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 1
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        backgroundViewCell?.addSubview(lbTitleName!)
    }
    
    func setValueForCell(setting : [String : Any]){
        imvAvatar?.image = UIImage(named: setting[SettingString.Icon] as? String ?? "")
        lbTitleName?.text = setting[SettingString.Title] as? String ?? ""
        lbTitleName?.textColor = setting[SettingString.TitleColor] as? UIColor ?? AppColor.NormalColors.BLACK_COLOR
        
    }

}
