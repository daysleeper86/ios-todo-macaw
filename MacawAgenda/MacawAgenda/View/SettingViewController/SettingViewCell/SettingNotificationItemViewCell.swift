//
//  SettingNotificationItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class SettingNotificationItemViewCell: UITableViewCell {
    
    //Views
    private var backgroundViewCell : UIView?
    private var lbTitleName : UILabel?
    var switchNotification : UISwitch?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.SettingHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let pSwitch : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wSwitch : CGFloat = AppDevice.SubViewFrame.SwitchWidth
        let hSwitch : CGFloat = AppDevice.SubViewFrame.SwitchHeight
        let xSwitch : CGFloat = wBackgroundView - wSwitch
        let ySwitch : CGFloat = (hBackgroundView-hSwitch)/2
        
        switchNotification = UISwitch(frame: CGRect(x: xSwitch, y: ySwitch, width: wSwitch, height: hSwitch))
        switchNotification?.onTintColor = AppColor.NormalColors.BLUE_COLOR
        backgroundViewCell?.addSubview(switchNotification!)
        
        // Title name
        let pTitleName : CGFloat = 0
        let xTitleName : CGFloat = pTitleName
        let yTitleName : CGFloat = 0
        let wTitleName : CGFloat = xSwitch - xTitleName - pSwitch
        let hTitleName : CGFloat = hBackgroundView
        
        lbTitleName = UILabel(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 1
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        backgroundViewCell?.addSubview(lbTitleName!)
    }
    
    func setValueForCell(setting : [String : Any], status: Bool){
        lbTitleName?.text = setting[SettingString.Title] as? String ?? ""
        lbTitleName?.textColor = setting[SettingString.TitleColor] as? UIColor ?? AppColor.NormalColors.BLACK_COLOR
        //
        switchNotification?.isOn = status
    }
    
}
