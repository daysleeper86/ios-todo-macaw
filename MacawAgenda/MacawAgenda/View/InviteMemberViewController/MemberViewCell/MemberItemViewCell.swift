//
//  MemberItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import SkeletonView
import NVActivityIndicatorView

class MemberItemViewCell: SwipeTableViewCell {
    //Views
    private var backgroundViewCell : UIView?
    private var spinnerView: NVActivityIndicatorView?
    private var imvAvatar : FAShimmerImageView?
    private var lbTitleName : FAShimmerLabelView?
    private var lbTitleSubName : FAShimmerLabelView?
    
    //Data
    static let pTitleName : CGFloat = AppDevice.ScreenComponent.ItemPadding
    var user = UserService.createEmptyUser()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 0
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.MemberHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.addSubview(backgroundViewCell!)
        
        let pSpinnerView : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wSpinnerView : CGFloat = 24.0
        let hSpinnerView : CGFloat = wSpinnerView
        let ySpinnerView : CGFloat = (hBackgroundView - hSpinnerView ) / 2
        let xSpinnerView : CGFloat = wBackgroundView - pSpinnerView - wSpinnerView
        let spinnerFrame = CGRect(x: xSpinnerView, y: ySpinnerView, width: wSpinnerView, height: hSpinnerView)
        spinnerView = NVActivityIndicatorView(frame: spinnerFrame,
                                                            type: .ballClipRotate)
        spinnerView?.color = AppColor.NormalColors.LIGHT_RED_COLOR
        spinnerView?.isHidden = true
        backgroundViewCell?.addSubview(spinnerView!)
        // Avatar
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = pAvatar
        let yAvatar : CGFloat = (hBackgroundView-hAvatar)/2
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.image = LocalImage.UserPlaceHolder
        backgroundViewCell?.addSubview(imvAvatar!)
        
        // Title name
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let xTitleName : CGFloat = xAvatar + wAvatar + pAvatar
        let yTitleName : CGFloat = yAvatar
        let wTitleName : CGFloat = wBackgroundView - xTitleName - MemberItemViewCell.pTitleName
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        lbTitleName?.isSkeletonable = true
        backgroundViewCell?.addSubview(lbTitleName!)
        
        let yTitleSubName : CGFloat = yTitleName + hTitleName
        
        lbTitleSubName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleSubName, width: wTitleName, height: hTitleName))
        lbTitleSubName?.textAlignment = .left
        lbTitleSubName?.numberOfLines = 0
        lbTitleSubName?.textColor = AppColor.NormalColors.LIGHT_GRAY_COLOR
        lbTitleSubName?.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 12)
        lbTitleSubName?.isSkeletonable = true
        backgroundViewCell?.addSubview(lbTitleSubName!)
    }
    
    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
        lbTitleSubName?.text = ""
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        imvAvatar?.startShimmering()
        
        lbTitleName?.showAnimatedSkeleton(usingColor: AppColor.MicsColors.SHIMMER_COLOR, animation: nil)
        lbTitleSubName?.showAnimatedSkeleton(usingColor: AppColor.MicsColors.SHIMMER_COLOR, animation: nil)
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleSubName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopSkeletonAnimation()
                lbTitleSubName?.stopSkeletonAnimation()
                lbTitleName?.hideSkeleton()
                lbTitleSubName?.hideSkeleton()
            }
        }
    }
    
    func startProcessing(){
        backgroundViewCell?.startShimmering()
        //
        spinnerView?.isHidden = false
        spinnerView?.startAnimating()
        //
        if let strongSpinnerView = spinnerView, let strongTitleName = lbTitleName{
            let wTitleName : CGFloat = strongSpinnerView.frame.origin.x - strongTitleName.frame.origin.x - MemberItemViewCell.pTitleName
            strongTitleName.frame.size.width = wTitleName
        }
        
    }
    
    func stopProcessing(){
        if(spinnerView?.isHidden == false){
            backgroundViewCell?.stopShimmering()
            //
            spinnerView?.isHidden = true
            spinnerView?.stopAnimating()
            //
            if let strongBackgroundViewCell = backgroundViewCell, let strongTitleName = lbTitleName{
                let wTitleName : CGFloat = strongBackgroundViewCell.frame.size.width - strongTitleName.frame.origin.x - MemberItemViewCell.pTitleName
                strongTitleName.frame.size.width = wTitleName
            }
        }
    }
    
    func setBackGroundColor(color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    func setValueForCell(user: User){
        self.user = user
        //
        if(self.user.dataType == .normal){
            stopShimmering_Cell()
            //
            stateForAvatar(self.user)
            //
            lbTitleName?.text = self.user.name
            //
            if(self.user.email.count > 0){
                lbTitleSubName?.text = self.user.email
            }
            else{
                lbTitleSubName?.text = self.user.username
            }
            //
        }
        else{
            startShimmering_Cell()
        }
        //
        stopProcessing()
    }
    
    func stateForAvatar(_ user : User){
        //Cancel current request
        imvAvatar?.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID == Constant.keyHolderUnassignedId){
            imvAvatar?.image = UIImage(named: "ic_no_assignee")
        }
        else{
            let strUserID = user.userId
            if(strUserID.count > 0){
                imvAvatar?.sd_cancelCurrentImageLoad()
                //
                let strURL = Util.parseURLAvatarImageFireStore(strUserID)
                imvAvatar?.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                    guard let strongSelf = self else { return }
                    if let imageValue = image{
                        //Current userId
                        let strCurrentUserID = strongSelf.user.userId
                        if(strCurrentUserID == strUserID){
                            if(imageValue.size.width > 0){
                                strongSelf.imvAvatar?.backgroundColor = .clear
                                strongSelf.imvAvatar?.removeLabelTextIfNeed()
                            }
                            else{
                                let strUserName = strongSelf.user.name
                                strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                            }
                        }
                    }
                    else{
                        //Current userId
                        let strCurrentUserID = strongSelf.user.userId
                        if(strCurrentUserID == strUserID){
                            let strUserName = strongSelf.user.name
                            strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                })
            }
            else{
                let strUserName = user.name
                imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
            }
        }
    }
}
