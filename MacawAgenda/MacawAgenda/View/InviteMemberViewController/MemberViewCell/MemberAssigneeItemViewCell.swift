//
//  MemberItemViewCell.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit

class MemberAssigneeItemViewCell: UITableViewCell {
    //Views
    private var backgroundViewCell : UIView?
    private var imvAvatar : FAShimmerImageView?
    private var lbTitleName : FAShimmerLabelView?
    private var imvSelected : FAShimmerImageView?
    //Data
    var user = UserService.createEmptyUser()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        initView()
    }
    
    func initView(){
        // Background View
        let xBackgroundView : CGFloat = 8
        let yBackgroundView : CGFloat = 0
        let wBackgroundView : CGFloat = AppDevice.ScreenComponent.Width - (xBackgroundView * 2)
        let hBackgroundView : CGFloat = AppDevice.TableViewRowSize.MemberHeight
        
        backgroundViewCell = UIView(frame: CGRect(x: xBackgroundView, y: yBackgroundView, width: wBackgroundView, height: hBackgroundView))
        backgroundViewCell?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        backgroundViewCell?.layer.cornerRadius = 4.0
        backgroundViewCell?.layer.masksToBounds = true
        self.addSubview(backgroundViewCell!)
        
        // Avatar
        let pAvatar : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wAvatar : CGFloat = AppDevice.SubViewFrame.AvatarIconWidth
        let hAvatar : CGFloat = wAvatar
        let xAvatar : CGFloat = pAvatar
        let yAvatar : CGFloat = (hBackgroundView-hAvatar)/2
        
        imvAvatar = FAShimmerImageView(frame: CGRect(x: xAvatar, y: yAvatar, width: wAvatar, height: hAvatar))
        imvAvatar?.layer.cornerRadius = hAvatar/2
        imvAvatar?.layer.masksToBounds = true
        imvAvatar?.image = LocalImage.UserPlaceHolder
        backgroundViewCell?.addSubview(imvAvatar!)
        
        //Image selected
        let pImageSelected : CGFloat = AppDevice.ScreenComponent.ItemPadding
//        let wImageSelected : CGFloat = AppDevice.SubViewFrame.AvatarIconSmallWidth
//        let hImageSelected : CGFloat = wImageSelected
        let wImageSelected : CGFloat = 13
        let hImageSelected : CGFloat = 8
        let xImageSelected : CGFloat = wBackgroundView - wImageSelected - pImageSelected
        let yImageSelected : CGFloat = (hBackgroundView-hImageSelected)/2
        
        imvSelected = FAShimmerImageView(frame: CGRect(x: xImageSelected, y: yImageSelected, width: wImageSelected, height: hImageSelected))
        backgroundViewCell?.addSubview(imvSelected!)
        
        // Title name
        let pTitleName : CGFloat = pAvatar
        let hTitleName : CGFloat = AppDevice.SubViewFrame.TitleHeight
        let xTitleName : CGFloat = xAvatar + wAvatar + pAvatar
        let yTitleName : CGFloat = (hBackgroundView-hTitleName)/2
        let wTitleName : CGFloat = xImageSelected - xTitleName - pTitleName
        
        lbTitleName = FAShimmerLabelView(frame: CGRect(x: xTitleName, y: yTitleName, width: wTitleName, height: hTitleName))
        lbTitleName?.textAlignment = .left
        lbTitleName?.numberOfLines = 0
        lbTitleName?.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleName?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        backgroundViewCell?.addSubview(lbTitleName!)
    }
    
    func resetValue_Cell(){
        imvAvatar?.image = nil
        lbTitleName?.text = ""
        imvSelected?.image = nil
    }
    
    func startShimmering_Cell(){
        resetValue_Cell()
        //
        imvAvatar?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        lbTitleName?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        imvSelected?.backgroundColor = AppColor.MicsColors.SHIMMER_COLOR
        
        imvAvatar?.startShimmering()
        lbTitleName?.startShimmering()
        imvSelected?.startShimmering()
    }
    
    func stopShimmering_Cell(){
        if let isShimmering = imvAvatar?.isShimmering(){
            if(isShimmering == true){
                //
                imvAvatar?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                lbTitleName?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                imvSelected?.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
                
                imvAvatar?.stopShimmering()
                lbTitleName?.stopShimmering()
                imvSelected?.stopShimmering()
            }
        }
    }
    
    func setBackGroundColor(_ color: UIColor){
        backgroundViewCell?.backgroundColor = color
    }
    
    func setValueForCell(user: User, selected: Bool,search: String){
        self.user = user
        //
        if(self.user.dataType == .normal){
            stopShimmering_Cell()
            //
            stateForAvatar(self.user)
            //
            stateForTitleName(self.user.name,search)
            //
            imvSelected?.isHidden = !selected
            //
            if(selected == true){
                imvSelected?.image = UIImage(named: "ic_item_selected_clear")
                setBackGroundColor(AppColor.MicsColors.BLUE_LIGHT_1_COLOR)
            }
            else{
                setBackGroundColor(AppColor.NormalColors.WHITE_COLOR)
            }
        }
        else if(self.user.dataType == .empty){
            imvAvatar?.image = UIImage(named: "ic_default_avatar")
            //
            lbTitleName?.text = NSLocalizedString("no_users_found", comment: "")
            imvSelected?.isHidden = true
        }
        else{
            startShimmering_Cell()
        }
    }
    
    func stateForTitleName(_ name : String,_ search: String){
        lbTitleName?.text = name
        //
        self.lbTitleName?.text = name
        Util.setAttributeStringForSearch(lbText: self.lbTitleName!, withSearch: search, fontSearch: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14), colorSearch: AppColor.NormalColors.BLUE_COLOR, fontNormal: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14), colorNormal: AppColor.NormalColors.BLACK_COLOR)
    }
    
    func hideAvatar(isHide: Bool){
        imvAvatar?.isHidden = isHide
    }
    
    func stateForAvatar(_ user : User){
        //Cancel current request
        imvAvatar?.sd_cancelCurrentImageLoad()
        //
        let strUserID = user.userId
        if(strUserID == Constant.keyHolderUnassignedId){
            imvAvatar?.image = UIImage(named: "ic_no_assignee")
        }
        else{
            let strUserID = user.userId
            if(strUserID.count > 0){
                imvAvatar?.sd_cancelCurrentImageLoad()
                //
                let strURL = Util.parseURLAvatarImageFireStore(strUserID)
                imvAvatar?.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                    guard let strongSelf = self else { return }
                    if let imageValue = image{
                        //Current userId
                        let strCurrentUserID = strongSelf.user.userId
                        if(strCurrentUserID == strUserID){
                            if(imageValue.size.width > 0){
                                strongSelf.imvAvatar?.backgroundColor = .clear
                                strongSelf.imvAvatar?.removeLabelTextIfNeed()
                            }
                            else{
                                let strUserName = strongSelf.user.name
                                strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                            }
                        }
                    }
                    else{
                        //Current userId
                        let strCurrentUserID = strongSelf.user.userId
                        if(strCurrentUserID == strUserID){
                            let strUserName = strongSelf.user.name
                            strongSelf.imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                        }
                    }
                })
            }
            else{
                let strUserName = user.name
                imvAvatar?.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
            }
        }
    }
}
