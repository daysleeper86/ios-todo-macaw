//
//  BacklogViewController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import RxDataSources
import NVActivityIndicatorView
import Material
import DTGradientButton

class InviteMemberViewController: BaseViewController {
    //External props
    var viewType : Constant.ViewTypeAddUpdateInsert = .add
    var navigationType : Constant.NavigationTypeAddViewController = .push
    
    //View
    private var navigationView : CustomNavigationView?
    private var tbMemberHeaderView : UIView?
    private var tfEmailID : FloatingLabelTextField?
    private var tbMember : UITableView = UITableView(frame: .zero)
    private var btnSendInvite : RaisedButton?
    
    //Data
    private var arrayUsers : [User] = []
    private let myUserId = AppSettings.sharedSingleton.account?.userId ?? ""
    private var yScreen_Scroll : CGFloat = 0
    static let memberCellIdentifier : String = "memberCellIdentifier"
    private var isRemovePermission : Bool = false
    private var processingUser : User?
    
    //ViewModel
    private var subjectProjectId : BehaviorSubject<String>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addNotification()
        //
        initView()
        //
        setDefaultValue()
        //
        bindToViewModel()
    }
    
    func addNotification(){
        addNotification_UpdateProject()
    }
    
    func addNotification_UpdateProject(){
        // Listen for upload avatar notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProject(notification:)),
                                               name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_PROJECT_OWNER_TYPE),
                                               object: nil)
    }
    
    @objc func updateProject(notification: Notification) {
        if let dictionary = notification.object as? NSDictionary{
            if let updatedType = dictionary[Constant.keyValue] as? Constant.ProjectUpdateInfoType{
                if(updatedType == .kProjecUpdateOwner){
                    checkPermission()
                    //
                    tbMember.reloadData()
                }
            }
        }
    }
    
    func removeNotification(){
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func createHeaderView() -> CGFloat{
        var yScreen : CGFloat = 0
        //Title Screen
        let xHeaderView : CGFloat = 0
        let yHeaderView : CGFloat = 0
        let wHeaderView : CGFloat = AppDevice.ScreenComponent.Width
        let yContentHeaderView : CGFloat = AppDevice.ScreenComponent.StatusHeight
        let hContentHeaderView : CGFloat = AppDevice.ScreenComponent.NavigationHeight
        let hHeaderView : CGFloat = yContentHeaderView + hContentHeaderView
        let pBottomHeaderView : CGFloat = 0
        
        navigationView = CustomNavigationView(frame: CGRect(x: xHeaderView, y: yHeaderView, width: wHeaderView, height: hHeaderView))
        navigationView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        self.view.addSubview(navigationView!)
        
        let pTitleHeader : CGFloat = 8
        var xTitleHeader : CGFloat = 0
        var wTitleHeader : CGFloat = 0
        
        if(viewType == .add){
            //Close Button
            //Action Button
            let wAction : CGFloat = 150
            let pAction : CGFloat = AppDevice.ScreenComponent.ItemPadding
            let hAction = hContentHeaderView
            let yAction = yContentHeaderView
            let xAction = navigationView!.frame.size.width - wAction - pAction
            
            let btnLaunch = UIButton.init(type: .custom)
            btnLaunch.accessibilityIdentifier = AccessiblityId.BTN_LAUNCH
            btnLaunch.frame = CGRect(x: xAction, y: yAction, width: wAction, height: hAction)
            btnLaunch.setTitle(NSLocalizedString("just_launch", comment:"").uppercased(), for: .normal)
            btnLaunch.setTitleColor(AppColor.NormalColors.BLUE_COLOR, for: .normal)
            btnLaunch.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .highlighted)
            btnLaunch.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 15)
            btnLaunch.addTarget(self, action: #selector(launchAction), for: .touchUpInside)
            btnLaunch.titleLabel?.textAlignment = .right
            btnLaunch.contentHorizontalAlignment = .right
            navigationView?.addSubview(btnLaunch)
            
            //
            xTitleHeader = 0
            wTitleHeader = xAction - (xTitleHeader * 2)
        }
        else{
            //Close Button
            let pClose : CGFloat = AppDevice.ScreenComponent.LeftButtonPadding
            let xClose : CGFloat = pClose
            let yClose : CGFloat = yContentHeaderView
            let hClose : CGFloat = hContentHeaderView
            let wClose : CGFloat = hClose
            
            let btnClose = IconButton.init(type: .custom)
            btnClose.accessibilityIdentifier = AccessiblityId.BTN_BACK
            btnClose.frame = CGRect(x: xClose, y: yClose, width: wClose, height: hClose)
            btnClose.setImage(UIImage(named: "ic_back_black"), for: .normal)
            btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
            navigationView?.addSubview(btnClose)
            
            //
            xTitleHeader = xClose + wClose + pTitleHeader
            wTitleHeader = navigationView!.frame.size.width - (xTitleHeader * 2)
        }
        
        //Title Screen
        let yTitleHeader : CGFloat = yContentHeaderView
        let hTitleHeader : CGFloat = hContentHeaderView
        
        let lbTitleHeader : UILabel = UILabel(frame: CGRect(x: xTitleHeader, y: yTitleHeader, width: wTitleHeader, height: hTitleHeader))
        lbTitleHeader.textAlignment = .center
        lbTitleHeader.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleHeader.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 20)
        lbTitleHeader.text = NSLocalizedString("invite_member", comment: "")
        navigationView?.titleView(titleView: lbTitleHeader)
        
        //
        yScreen += hHeaderView
        yScreen += pBottomHeaderView
        
        return yScreen
    }
    
    func createContentView(yHeader :  CGFloat){
        let xTableMember : CGFloat = 0
        let yTableMember : CGFloat = yHeader
        let wTableMember : CGFloat = AppDevice.ScreenComponent.Width
        let hTableMember : CGFloat = AppDevice.ScreenComponent.Height - yTableMember
        tbMember.frame = CGRect(x: xTableMember, y: yTableMember, width: wTableMember, height: hTableMember)
        tbMember.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        tbMember.separatorColor = AppColor.NormalColors.CLEAR_COLOR
        tbMember.dataSource = self
        tbMember.delegate = self
        tbMember.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        tbMember.register(MemberItemViewCell.self, forCellReuseIdentifier: InviteMemberViewController.memberCellIdentifier)
        self.view.addSubview(tbMember)
        
        var yViewScreen : CGFloat = 0
        //Header View
        let wHeaderTableView : CGFloat = wTableMember
        tbMemberHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: wHeaderTableView, height: 0))
        tbMemberHeaderView?.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        //Title Screen
        let pTitleScreen : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let xTitleScreen : CGFloat = pTitleScreen
        let yTitleScreen : CGFloat = yViewScreen
        let wTitleScreen : CGFloat = wHeaderTableView - (xTitleScreen * 2)
        let hTitleScreen : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let hBottomTitleScreen : CGFloat = 4.0
        
        let lbTitleScreen : UILabel = UILabel(frame: CGRect(x: xTitleScreen, y: yTitleScreen, width: wTitleScreen, height: hTitleScreen))
        lbTitleScreen.textAlignment = .left
        lbTitleScreen.textColor = AppColor.NormalColors.BLACK_COLOR
        lbTitleScreen.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 24)
        lbTitleScreen.text = NSLocalizedString("invite_member", comment: "")
        tbMemberHeaderView?.addSubview(lbTitleScreen)
        
        yViewScreen += hTitleScreen
        yViewScreen += hBottomTitleScreen
        
        //Title Description
        let xTitleDescription : CGFloat = xTitleScreen
        let yTitleDescription : CGFloat = yViewScreen
        let wTitleDescription : CGFloat = wTitleScreen
        let hTitleDescription : CGFloat = AppDevice.ScreenComponent.NormalHeight
        let pBottomTitleDescription : CGFloat = AppDevice.ScreenComponent.ItemPadding
        
        let lbTitleDescription : UILabel = UILabel(frame: CGRect(x: xTitleDescription, y: yTitleDescription, width: wTitleDescription, height: hTitleDescription))
        lbTitleDescription.textAlignment = .left
        lbTitleDescription.numberOfLines = 0
        lbTitleDescription.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitleDescription.font = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14)
        lbTitleDescription.text = NSLocalizedString("invite_people_to_collaborate", comment: "")
        tbMemberHeaderView?.addSubview(lbTitleDescription)
        
        yViewScreen += hTitleDescription
        yViewScreen += pBottomTitleDescription
        
        let paddingTopBottomTextField : CGFloat = 12
        yViewScreen += paddingTopBottomTextField
        
        //Email
        let xFieldEmail : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let yFieldEmail : CGFloat = yViewScreen
        let wFieldEmail : CGFloat = tbMemberHeaderView!.frame.size.width - (xFieldEmail * 2)
        let hFieldEmail : CGFloat = 50
        let pBottomFieldEmail : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
        
        tfEmailID = FloatingLabelTextField(frame: CGRect(x: xFieldEmail, y: yFieldEmail, width: wFieldEmail, height: hFieldEmail))
        tfEmailID?.isAccessibilityElement = true
        tfEmailID?.accessibilityIdentifier = AccessiblityId.TF_EMAIL
        tfEmailID?.autocapitalizationType = .none
        tfEmailID?.placeholder = NSLocalizedString("enter_email_address", comment: "")
        tfEmailID?.placeholderColor = UIColor(hexString: "#b2bac0")
        tfEmailID?.title = NSLocalizedString("email_address", comment: "").uppercased()
        tbMemberHeaderView?.addSubview(tfEmailID!)
        
        yViewScreen += hFieldEmail
        yViewScreen += paddingTopBottomTextField
        yViewScreen += pBottomFieldEmail
        
        let xButtonSend : CGFloat = xFieldEmail
        let wButtonSend : CGFloat = wFieldEmail
        let hButtonSend : CGFloat = AppDevice.ScreenComponent.ButtonHeight
        let yButtonSend : CGFloat = yViewScreen
        let pBottomButtonSend : CGFloat = AppDevice.ScreenComponent.ItemPadding * 2
        
        btnSendInvite = RaisedButton.init(type: .custom)
        btnSendInvite?.accessibilityIdentifier = AccessiblityId.BTN_INVITE
        btnSendInvite?.pulseColor = .white
        btnSendInvite?.frame = CGRect(x: xButtonSend, y: yButtonSend, width: wButtonSend, height: hButtonSend)
        btnSendInvite?.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        btnSendInvite?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
        btnSendInvite?.titleLabel?.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        btnSendInvite?.titleLabel?.textAlignment = .center
        btnSendInvite?.layer.cornerRadius = 4.0
        btnSendInvite?.layer.masksToBounds = true
//        btnSendInvite?.backgroundColor = AppColor.NormalColors.BLUE_COLOR
        btnSendInvite?.layer.shadowColor = UIColor(red: 0.09, green: 0.17, blue: 0.3, alpha: 0.4).cgColor
        btnSendInvite?.layer.shadowOffset = CGSize(width: 0.0, height: 8.0)
        btnSendInvite?.layer.shadowRadius = 16;
        btnSendInvite?.layer.shadowOpacity = 1;
        
        btnSendInvite?.setTitle(NSLocalizedString("send_invitation", comment:"").uppercased(), for: .normal)
        tbMemberHeaderView?.addSubview(btnSendInvite!)
        
        yViewScreen += hButtonSend
        yViewScreen += pBottomButtonSend
        
        //Mics Update
        yScreen_Scroll = lbTitleScreen.frame.origin.y + lbTitleScreen.frame.size.height
        let hHeaderTableView : CGFloat = yViewScreen
        tbMemberHeaderView?.frame.size.height = hHeaderTableView
        tbMember.tableHeaderView = tbMemberHeaderView!
    }
    
    func initView(){
        self.view.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        let yScreen : CGFloat = createHeaderView()
        createContentView(yHeader: yScreen)
    }
    
    func setDefaultValue(){
        //
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        subjectProjectId = BehaviorSubject<String>(value: projectId)
        //
        checkPermission()
    }
    
    func checkPermission(){
        let userId = AppSettings.sharedSingleton.account?.userId ?? ""
        let projectOwnedId = AppSettings.sharedSingleton.project?.owned.userId ?? ""
        if(userId.count > 0 && userId == projectOwnedId){
            isRemovePermission = true
        }
        if(isRemovePermission == false){
            let orangnizationOwnedId = AppSettings.sharedSingleton.orangnization?.owner.userId ?? ""
            if(myUserId.count > 0 && orangnizationOwnedId.count > 0){
                if(myUserId == orangnizationOwnedId){
                    isRemovePermission = true
                }
            }
        }
    }
    
    @objc func closeAction(){
        if(navigationType == .push){
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @objc func launchAction(){
        AppDelegate().sharedInstance().setupWelcomeView()
    }
    
    @objc func sendAction(){
    }

    func removeUser(userId: String){
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let userController = UserAPIController(provider: projectProvider)
        let userViewModel = UserAPIViewModel(
            inputRemoveMember: (
                projectId: projectId,
                userId: BehaviorSubject<String>(value: userId),
                userAPIController: userController
            )
        )
        //
        userViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message: output.message,duration: 3.0,position: .top)
                    self?.stopProcessingForAllVisibleCell()
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            let message = NSLocalizedString("remove_member_successfully", comment: "")
                            self?.makeToast(message: message,duration: 4.0,position: .top)
                        }
                        else{
                            let message = NSLocalizedString("remove_member_unsuccessfully", comment: "")
                            self?.makeToast(message: message,duration: 3.0,position: .top)
                        
                        }
                        //
                        self?.stopProcessingUserCell(userId: reponseAPI.data)
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func getVisibleCellFromUser(userId: String) -> MemberItemViewCell?{
        let paths = tbMember.indexPathsForVisibleRows
        guard let strongPaths = paths else { return nil }
        var visibleCell : MemberItemViewCell?
        
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbMember.cellForRow(at: path)
            if let memberCell = cell as? MemberItemViewCell{
                if(memberCell.user.userId == userId){
                    visibleCell = memberCell
                    break
                }
            }
        }
        //
        return visibleCell
    }
    
    func stopProcessingUserCell(userId: String){
        if let visibleCell = getVisibleCellFromUser(userId: userId){
            visibleCell.stopProcessing()
        }
    }
    
    func stopProcessingForAllVisibleCell(){
        let paths = tbMember.indexPathsForVisibleRows
        guard let strongPaths = paths else { return }
        //  For getting the cells themselves
        for path in strongPaths{
            let cell = tbMember.cellForRow(at: path)
            if let memberCell = cell as? MemberItemViewCell{
                memberCell.stopProcessing()
            }
        }
    }
    
    func bindToViewModel(){
        let orangnizationId = AppSettings.sharedSingleton.orangnization?.orangnizationId ?? ""
        let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
        //
        let token = AppSettings.sharedSingleton.account?.token ?? ""
        let authPlugin = AccessTokenPlugin { token }
        let projectProvider = MoyaProvider<AppAPI>(plugins: [NetworkLoggerPlugin(verbose: true),authPlugin])
        let userController = UserAPIController(provider: projectProvider)
        let observableTap = btnSendInvite!.rx.tap.asObservable()
        let userViewModel = UserAPIViewModel(
            inputInviteMember: (
                oid: orangnizationId,
                projectId: projectId,
                email: tfEmailID!.rx.text.orEmpty.asObservable(),
                loginTaps: observableTap,
                userAPIController: userController
            )
        )
        // bind results to  {
        userViewModel.loginEnabled?
            .subscribe(onNext: { [weak self] valid  in
                self?.btnSendInvite?.isEnabled = valid
                if(valid == true){
                    let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
                    self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
                    self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
                    
                    self?.btnSendInvite?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
                }
                else{
                    let gradientColor = [UIColor(red: CGFloat(248.0)/255, green: CGFloat(248.0)/255, blue: CGFloat(248.0)/255, alpha: 1.0), UIColor(red: CGFloat(225.0)/255, green: CGFloat(234.0)/255, blue: CGFloat(238.0)/255, alpha: 1.0)]
                    self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.normal)
                    self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.highlighted)
                    self?.btnSendInvite?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
                }
            })
            .disposed(by: disposeBag)
        //
        userViewModel.signingIn?
            .subscribe(onNext: {[weak self] loading  in
                if(loading == true){
                    self?.tfEmailID?.isUserInteractionEnabled = false
                    self?.tfEmailID?.startShimmering()
                }
                else{
                    self?.tfEmailID?.isUserInteractionEnabled = true
                    self?.tfEmailID?.stopShimmering()
                }
            })
            .disposed(by: disposeBag)
        //
        userViewModel.signedIn?.subscribe(onNext: { [weak self] loginResult in
            switch loginResult {
            case .failed(let output):
                let appDelegate = AppDelegate().sharedInstance()
                if(appDelegate.alertSessionExpiredIfNeed(code: output.code) == false){
                    self?.makeToast(message: output.message,duration: 3.0,position: .top)
                    //
                    self?.tfEmailID?.isUserInteractionEnabled = true
                    self?.tfEmailID?.stopShimmering()
                    self?.tfEmailID?.text = ""
                }
            case .ok(let output):
                if let reponseAPI = output as? ResponseAPI{
                    if(reponseAPI.status == AppURL.APIStatusString.SUCCESS.rawValue){
                        if(reponseAPI.code == 200){
                            let message = NSLocalizedString("invite", comment: "") + " '" + reponseAPI.data + "' "  + NSLocalizedString("invite_member_successfully", comment: "")
                            self?.makeToast(message:message, duration: 3.0, position: .top)
                            //
                            if let email = self?.tfEmailID?.text{
                                let userId = AppSettings.sharedSingleton.account?.userId ?? ""
                                let userName = AppSettings.sharedSingleton.account?.name ?? ""
                                let projectId = AppSettings.sharedSingleton.project?.projectId ?? ""
                                Tracker.trackInviteSend(userid: userId, name: userName, projectId: projectId, email: email)
                            }
                        }
                        else{
                            var message = NSLocalizedString("failed_to_invite", comment: "")  + " '" + reponseAPI.data + "' "  + NSLocalizedString("invite_member_unsuccessfully", comment: "")
                            if(reponseAPI.code != 0){
                                if(reponseAPI.message.count > 0){
                                    message = NSLocalizedString("failed_to_invite", comment: "")  + " '" + reponseAPI.data + "' " + NSLocalizedString("to_project", comment: "")  + reponseAPI.message
                                }
                            }
                            self?.makeToast(message:message, duration: 3.0, position: .top)
                        }
                        //
                        self?.tfEmailID?.isUserInteractionEnabled = true
                        self?.tfEmailID?.stopShimmering()
                        self?.tfEmailID?.text = ""
                        self?.tfEmailID?.resignFirstResponder()
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
        let projectController = ProjectController.sharedAPI
        let projectViewModel = ProjectViewModel(
            inputNeedUsers: (
                projectId: subjectProjectId?.asObservable(),
                projectController: projectController
            )
        )
        
        projectViewModel.users?
            .subscribe(onNext:  {[weak self] users in
                self?.arrayUsers = users
                self?.tbMember.reloadData()
                //
                if(UserService.checkHolderUser(users: users) == false){
                    var maxInvite : Int = 0
                    if let maxInviteOrangnization = AppSettings.sharedSingleton.orangnization?.orangnizationUserMaxInvite{
                        maxInvite = maxInviteOrangnization
                    }
                    else if let maxInviteProject = AppSettings.sharedSingleton.project?.projectUserMaxInvite{
                        maxInvite = maxInviteProject
                    }
                    if(maxInvite > 0){
                        if(users.count >= maxInvite){
                            let gradientColor = [UIColor(red: CGFloat(248.0)/255, green: CGFloat(248.0)/255, blue: CGFloat(248.0)/255, alpha: 1.0), UIColor(red: CGFloat(225.0)/255, green: CGFloat(234.0)/255, blue: CGFloat(238.0)/255, alpha: 1.0)]
                            self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.normal)
                            self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toLeft, for: UIControl.State.highlighted)
                            self?.btnSendInvite?.setTitleColor(AppColor.NormalColors.BLACK_COLOR, for: .normal)
                            //
                            self?.tfEmailID?.text = ""
                            self?.tfEmailID?.isUserInteractionEnabled = false
                            self?.tfEmailID?.placeholder = NSLocalizedString("maximun_users_in_this_project", comment: "")
                            self?.tfEmailID?.placeholderColor = AppColor.NormalColors.LIGHT_RED_COLOR
                        }
                        else{
                            let gradientColor = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
                            self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.normal)
                            self?.btnSendInvite?.setGradientBackgroundColors(gradientColor, direction: DTImageGradientDirection.toTopRight, for: UIControl.State.highlighted)
                            
                            self?.btnSendInvite?.setTitleColor(AppColor.NormalColors.WHITE_COLOR, for: .normal)
                            //
                            self?.tfEmailID?.isUserInteractionEnabled = true
                            self?.tfEmailID?.placeholder = NSLocalizedString("enter_email_address", comment: "")
                            self?.tfEmailID?.placeholderColor = UIColor(hexString: "#b2bac0")
                        }
                    }
                    else{
                        
                    }
                }
            })
            .disposed(by: disposeBag)
        
        
        tbMember.rx.contentOffset.subscribe { [weak self] in
            let yOffset = $0.element?.y ?? 0
            let yScreen_Scroll = self?.yScreen_Scroll ?? 0
            var isShow : Bool = false
            if(yOffset > yScreen_Scroll){
                isShow = true
            }
            self?.navigationView?.showHideTitleNavigation(isShow: isShow)
            }.disposed(by: disposeBag)
    }
    
    deinit {
        removeNotification()
    }
    
}


extension InviteMemberViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let hHeader : CGFloat = 36
        return hHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let hFrame : CGFloat = 36
        let headerView_Section = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: hFrame))
        headerView_Section.backgroundColor = AppColor.NormalColors.WHITE_COLOR
        
        //Title
        let xTitle : CGFloat = AppDevice.ScreenComponent.ItemPadding
        let wTitle : CGFloat = headerView_Section.frame.size.width - (xTitle * 2)
        
        let lbTitle : UILabel = UILabel(frame: CGRect(x: xTitle, y: 0, width: wTitle, height: hFrame))
        lbTitle.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        lbTitle.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 16)
        lbTitle.textColor = AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR
        lbTitle.numberOfLines = 0
        headerView_Section.addSubview(lbTitle)
        
        //
        var title = NSLocalizedString("member_list", comment: "")
        if(UserService.checkHolderUser(users: arrayUsers) == false){
            title = title + " (" + String(arrayUsers.count) + ") "
        }
        lbTitle.text = title
        
        return headerView_Section
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let cellHeight : CGFloat = AppDevice.TableViewRowSize.MemberHeight
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier:  InviteMemberViewController.memberCellIdentifier, for: indexPath) as? MemberItemViewCell else {
            return MemberItemViewCell()
        }
        cell.selectionStyle = .none
        cell.backgroundColor = AppColor.NormalColors.CLEAR_COLOR
        //
        let element = arrayUsers[indexPath.row]
        //
        if(self.isRemovePermission == true && element.valid() && element.userId != self.myUserId){
            cell.delegate = self
        }
        else{
            cell.delegate = nil
        }
        //
        cell.setValueForCell(user: element)
        //
        cell.accessibilityIdentifier = AccessiblityId.TBCELL_ITEM + String(indexPath.row)
        return cell
        
    }
}

extension InviteMemberViewController: SwipeTableViewCellDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let strSpace = "  "
        let strTitleRemove = NSLocalizedString("remove", comment: "") + strSpace
        let removeAction = SwipeAction(style: .default, title: strTitleRemove) { [weak self] action, indexPath in
            // handle action by updating model with deletion
            guard let strongSelf = self else { return }
            let user = strongSelf.arrayUsers[indexPath.row]
            //
            strongSelf.processingUser = user
            //
            let title = NSLocalizedString("are_you_sure_you_want_to_remove",comment:"")
            let name = user.name
            let strSpace = "\n"
            let titleMessage  = title + strSpace + name
            //
            let attributedString = NSMutableAttributedString(string: titleMessage)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.MicsColors.TITLE_BOLD_DESIGN_COLOR, range: NSRange(location: 0, length: titleMessage.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: title.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold), range: NSRange(location: title.count + strSpace.count, length: name.count))
            
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            actionSheet.setValue(attributedString, forKey: "attributedTitle")
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("remove",comment:""), style: .destructive, handler: {[weak self] (action) in
                //
                guard let strongSelf = self,let strongProcessingUser = strongSelf.processingUser else { return }
                if let memberCell = strongSelf.tbMember.cellForRow(at: indexPath) as? MemberItemViewCell{
                    memberCell.startProcessing()
                    //
                    memberCell.hideSwipe(animated: true)
                    //
                    strongSelf.removeUser(userId: strongProcessingUser.userId)
                }
            }))
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel",comment:""), style: .cancel, handler: {[weak self] (action) in
                //
                guard let strongSelf = self else { return }
                if let memberCell = strongSelf.tbMember.cellForRow(at: indexPath) as? MemberItemViewCell{
                    memberCell.hideSwipe(animated: true)
                }
            }))
            self?.present(actionSheet, animated: true)
        }
        // customize the action appearance
        removeAction.font = UIFont(name: FontNames.Lato.MULI_BOLD, size: 14)
        removeAction.textColor = AppColor.NormalColors.WHITE_COLOR
        removeAction.backgroundColor = AppColor.NormalColors.LIGHT_RED_COLOR
        removeAction.image = UIImage(named: "ic_remove_task")
        removeAction.imageTitleOrientation = .horizontal
        
        return [removeAction]
        
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .border
        options.buttonVerticalAlignment = .center
        options.minimumButtonWidth = 140
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        guard let cell = tableView.cellForRow(at: indexPath) as? MemberItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.MicsColors.DISABLE_COLOR)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        guard let strongIndexPath = indexPath, let cell = tableView.cellForRow(at: strongIndexPath) as? MemberItemViewCell else { return }
        cell.setBackGroundColor(color: AppColor.NormalColors.WHITE_COLOR)
    }
    
}
