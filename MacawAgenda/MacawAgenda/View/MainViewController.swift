//
//  MainViewController1.swift
//  MacawAgenda
//
//  Created by tranquangson on 9/27/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

public class ExampleNavigationController: UINavigationController {
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UIBarButtonItem.appearance()
        appearance.setBackButtonTitlePositionAdjustment(UIOffset.init(horizontal: 0.0, vertical: -60), for: .default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.barTintColor = UIColor.init(red: 250/255.0, green: 250/255.0, blue: 250/255.0, alpha: 0.8)
        #if swift(>=4.0)
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0), NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)]
        #elseif swift(>=3.0)
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)];
        #endif
        self.navigationBar.tintColor = UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0)
        self.navigationItem.title = ""
    }
}


class MainViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        MainViewController1.customIrregularityStyle(delegate:nil)
    }
    
    static func customIrregularityStyle(delegate: UITabBarControllerDelegate?) -> ExampleNavigationController {
        let tabBarController = ESTabBarController()
        tabBarController.delegate = delegate
        tabBarController.title = "Irregularity"
        tabBarController.tabBar.shadowImage = UIImage(named: "transparent")
        tabBarController.tabBar.backgroundImage = UIImage(named: "background_dark")
        
        let teamAgenda = TeamAgendaViewController(nibName: nil, bundle: nil)
        
        let backlog = BacklogViewController(nibName: nil, bundle: nil)
        
        let notification = NotificationViewController(nibName: nil, bundle: nil)
        
        let setting = SettingViewController(nibName: nil, bundle: nil)
        
        
        teamAgenda.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: NSLocalizedString("collaborate", comment: ""), image: UIImage(named: "ic_tab_collaborate"), selectedImage: UIImage(named: "ic_tab_collaborate"))
        backlog.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: NSLocalizedString("backlog", comment: ""), image: UIImage(named: "ic_tab_backlog2"), selectedImage: UIImage(named: "ic_tab_backlog2"))
//        empty.tabBarItem = ESTabBarItem.init(ExampleIrregularityContentView(), title: nil, image: UIImage(named: "focusmode"), selectedImage: UIImage(named: "focusmode"))
        notification.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: NSLocalizedString("notifications", comment: ""), image: UIImage(named: "ic_tab_notification"), selectedImage: UIImage(named: "ic_tab_notification"))
        setting.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: NSLocalizedString("account", comment: ""), image: UIImage(named: "ic_tab_account"), selectedImage: UIImage(named: "ic_tab_account"))
        
        tabBarController.viewControllers = [teamAgenda, backlog, notification, setting]
        
        let tabGradientView = UIView(frame: tabBarController.tabBar.bounds)
        tabGradientView.backgroundColor = UIColor.white
        tabGradientView.translatesAutoresizingMaskIntoConstraints = false;
        tabBarController.tabBar.addSubview(tabGradientView)
        tabBarController.tabBar.sendSubviewToBack(tabGradientView)
        tabGradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabGradientView.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabGradientView.layer.shadowRadius = 2.0
        tabGradientView.layer.shadowColor = UIColor.gray.cgColor
        tabGradientView.layer.shadowOpacity = 0.3
        tabBarController.tabBar.clipsToBounds = false
        tabBarController.tabBar.backgroundImage = UIImage()
        tabBarController.tabBar.shadowImage = UIImage()
        
        let navigationController = ExampleNavigationController.init(rootViewController: tabBarController)
        return navigationController
    }
}
