//
//  ModelSection.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/23/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import RxDataSources

//Task section
struct TaskSection {
    var header: String
    var items: [Task]
}
extension TaskSection : SectionModelType {
    typealias Item = Task
    
    var identity: String {
        return header
    }
    
    init(original: TaskSection, items: [Task]) {
        self = original
        self.items = items
    }
}

//Activity section
struct MessageSection {
    var header: String
    var items: [Message]
}
extension MessageSection : SectionModelType {
    typealias Item = Message
    
    var identity: String {
        return header
    }
    
    init(original: MessageSection, items: [Message]) {
        self = original
        self.items = items
    }
}

//User section
struct UserSection {
    var header: String
    var items: [User]
}
extension UserSection : SectionModelType {
    typealias Item = User
    
    var identity: String {
        return header
    }
    
    init(original: UserSection, items: [User]) {
        self = original
        self.items = items
    }
}
