enum LoginResult {
    case ok(output : Any?)
    case failed(output: (code: Int, message:String))
    
}
extension LoginResult: Equatable {}

func == (lhs: LoginResult, rhs: LoginResult) -> Bool {
    switch (lhs,rhs) {
    case (.ok, .ok):
        return true
    case (.failed(let x), .failed(let x1))
        where x.code == x1.code:
        return true
    default:
        return false
    }
}
