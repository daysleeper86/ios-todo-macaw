//
//  File.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/12/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import UIKit

public typealias AACompletionVoid  = (() -> ())
public typealias AACompletionAny   = ((Any) -> ())

struct AppDevice {
    
    struct ScreenComponent {
        static let NormalHeight : CGFloat =  44
        static let NavigationHeight : CGFloat =  44
        static let ToolbarHeight : CGFloat =  44
        static let HomeHeight : CGFloat =  34
        static let StatusHeight : CGFloat =  IPhoneType.isIphoneX ? 44 : 20
        static let StatusPadding : CGFloat =  28
        static let TabbarHeight : CGFloat =  IPhoneType.isIphoneX ? 49 + HomeHeight : 49
        static let ButtonHeight : CGFloat =  44
        static let LeftButtonPadding : CGFloat =  8
        static let RightButtonPadding : CGFloat =  8
        static let ItemPadding : CGFloat =  16
        static let Width : CGFloat = UIScreen.main.bounds.size.width
        static let Height : CGFloat =  UIScreen.main.bounds.size.height
        static let MaxWidthHeight : CGFloat =  max(Width,Height)
        static let MinWidthHeight : CGFloat =  min(Width,Height)
        static let HeightWithoutNavigation : CGFloat =  UIScreen.main.bounds.size.height
        static let OverlayHeight : CGFloat =  0.85
        static let OverlayHeight1 : CGFloat =  0.95
        static let OverlayHandler : CGFloat =  30.0
    }
    
    struct DeviceType{
        static let isIpad = UIDevice.current.userInterfaceIdiom == .pad
        static let isIphone = UIDevice.current.userInterfaceIdiom == .phone
        static let isRetina = UIScreen.main.scale >= 2
    }
    
    struct IPhoneType{
        static let isIphone4orLess = DeviceType.isIphone && ScreenComponent.MaxWidthHeight == 568.0
        static let isIphone5 = DeviceType.isIphone && ScreenComponent.MaxWidthHeight == 568.0
        static let isIphone6or7 = DeviceType.isIphone && ScreenComponent.MaxWidthHeight == 667.0
        static let isIphone6Por7P = DeviceType.isIphone && ScreenComponent.MaxWidthHeight == 736.0
        static let isIphoneX = DeviceType.isIphone && ScreenComponent.MaxWidthHeight == 812.0
        static let isIphoneLargeSize = DeviceType.isIphone && ScreenComponent.MaxWidthHeight == 414.0
    }
    
    struct TableViewRowSize {
        static let TaskHeight : CGFloat =  44
        static let TaskWIPHeight : CGFloat =  68
        static let AddNewTaskHeight : CGFloat =  44
        static let FileHeight : CGFloat =  64
        static let SettingHeight : CGFloat =  48
        static let InputFieldHeight : CGFloat =  95
        static let NotificationHeight : CGFloat =  96
        static let MemberHeight : CGFloat =  50
        static let AlbumHeight : CGFloat =  112
    }
    
    struct SubViewFrame{
        static let TitleHeight : CGFloat =  18.0
        static let LargeTitleHeight : CGFloat =  22.0
        static let AvatarIconX : CGFloat =  20.0
        static let AvatarIconSmallWidth : CGFloat =  24.0
        static let AvatarIconSmallHeight : CGFloat =  24.0
        static let AvatarIconWidth : CGFloat =  36.0
        static let AvatarIconHeight : CGFloat =  36.0
        static let AvatarIconMediumWidth : CGFloat =  44.0
        static let AvatarIconMediumHeight : CGFloat =  44.0
        static let AvatarIconLargeWidth : CGFloat =  72.0
        static let AvatarIconLargeHeight : CGFloat =  72.0
        static let AvatarIconLargeLargeWidth : CGFloat =  96.0
        static let AvatarIconLargeLargeHeight : CGFloat =  96.0
        static let AlbumIconWidth : CGFloat =  80.0
        static let AlbumIconHeight : CGFloat =  80.0
        static let OrangnizationIconWidth : CGFloat =  48.0
        static let OrangnizationIconHeight : CGFloat =  48.0
        static let OrangnizationCellPadding : CGFloat =  24.0
        
        static let UserHorizontalItemWidth : CGFloat =  AvatarIconWidth
        static let UserHorizontalItemHeight : CGFloat =  AvatarIconHeight + ( AppDevice.ScreenComponent.ItemPadding * 2)
        
        static let SettingUserHeaderHeight : CGFloat =  48
        static let SettingUserHeaderPaddingTop : CGFloat =  AppDevice.ScreenComponent.ItemPadding
        static let SettingUserHeaderPaddingBottom : CGFloat =  AppDevice.ScreenComponent.ItemPadding / 2
        static let SettingProfileHeaderHeight : CGFloat =  170
        static let SettingProfileHeaderPaddingTop : CGFloat =  AppDevice.ScreenComponent.ItemPadding * 2
        static let SettingProfileHeaderPaddingBottom : CGFloat = 48
        
        static let EditUserHeaderHeight : CGFloat =  68
        static let EditUserHeaderPaddingTop : CGFloat =  36
        
        static let SwitchWidth : CGFloat =  51
        static let SwitchHeight : CGFloat =  31
        
        static let FloatingLabelTextFieldTopBottomPadding : CGFloat =  12
        static let FloatingLabelTextFieldHeight : CGFloat =  50
    }
    
    
    static let TabbarCount : Int =  4
    struct TabbarIndex  {
        static let Collaborate = 0
        static let Backlog = 1
        static let Notifications = 2
        static let Account = 3
    }
    
    struct Configuration {
        //Dev and Dis
        static let isDistribution = true
    }
}

struct AppURL {
    struct APIDomains {
        static let Dev = "https://api.macaw-dev.tk/v1"
//        static let Dev = "http://34.83.161.132/v1"
//        static let Dev = "http://35.199.144.76/v1"
        static let Dis = "https://api.github.com/search/repositories?q="
        static let UAT = "http://test-UAT.com"
        static let Local = "192.145.1.1"
        static let QA = "testAddress.qa.com"
        static let FireStorageBucket = "macaw-beta.appspot.com"
        static let FireStorageImageURL = "https://firebasestorage.googleapis.com/v0/b/macaw-beta.appspot.com/o/"
        static let FireStorageImageMediaType = "?alt=media"
        static let FireStorageImageType = "image/jpeg"
        static let FireStorageFileFolder = "uploads"
        static let FireStorageAvatarFolder = "avatars"
        static let Folder_User_Chatting = "Chatting"
        
//        static let ServerAPI_Dev = "http://192.168.1.19:7070/v1"
        static let ServerAPI_Dev = "https://api.macaw-dev.tk/v1"
        static let ServerAPI_Dis = "https://api.macaw-app.com/v1"
        static let ServerAPI = (AppDevice.Configuration.isDistribution == true ? ServerAPI_Dis : ServerAPI_Dev)
        
        static let PrefixCollectionDev = ""
        static let PrefixCollectionDis = "prod_"
        static let PrefixCollection = (AppDevice.Configuration.isDistribution == true ? PrefixCollectionDis : PrefixCollectionDev)
        
        static let PrefixUserNameDev = "dev_"
        static let PrefixUserNameDis = ""
        static let PrefixUserName = (AppDevice.Configuration.isDistribution == true ? PrefixUserNameDis : PrefixUserNameDev)
        
        static let ImageURL_Dev = "https://beta3.macaw-dev.ga/"
        static let ImageURL_Dis = "https://workplace.macaw-app.com/"
        static let ImageURL = (AppDevice.Configuration.isDistribution == true ? ImageURL_Dis : ImageURL_Dev)
        static let ImageURL_Avatar = ImageURL + "avatar/"
        static let ImageURL_Logo = ImageURL + "logo/"
    }
    
    struct ExternalKey {
        static let GoogleSignIn = "38074605768-d65pbvmtljfrpmpi8uk8e2bh1464qenk.apps.googleusercontent.com"
        static let KeyChainAccessService = "com.macaw.todo.security.ios"
        static let AnalyticKey = "qVFReu9gIHtIE8XnaHJTfrdy8TR3LlWl"
    }
    
    
    
    
    private struct APIRoutes {
        static let API_CHECK_USER_EMAIL = "/check-email"
        static let API_CHECK_FORGOT_PASS = "/forgot-password"
        static let API_REQUEST_LOGIN = "/login"
        static let API_REQUEST_GOOGLE_LOGIN = "/login-google"
        static let API_REQUEST_SIGNUP = "/register"
        static let API_GET_ALL_USER = "/user"
        static let API_POST_CREATE_NEW_USER = "/user/create"
        static let API_POST_CHANGE_PASS = "/change-password"
        static let API_PUT_UPDATE_USER = "/user/update/"
        static let API_PUT_DELETE_USER = "/user/destroy/"
        static let API_USER = "/user/"
        static let API_PUT_UPDATE_USER_PROFILE = "/change-profile"
        static let API_PUT_UPDATE_WELCOME = "/update-welcome"
        static let API_PUT_UPDATE_TOKEN = "/push-notification"
        //Project
        static let API_GET_ALL_PROJECT = "/projects"
        static let API_CREATE_PROJECT = "/projects"
        static let API_LEAVE_PROJECT = "/projects/leave/"
        static let API_CLIENT_PROJECT = "/client"
        static let API_UPDATE_PROJECT = "/projects/"
        static let API_GET_PROJECT_FILE = "/files"
        static let API_READ = "/read"
        //Tasks
        static let API_CREATE_TASK = "/tasks/"
        static let API_ARCHIVE_TASK = "/tasks/"
        static let API_UPDATE_TASK = "/tasks/"
        static let API_CONVERSATION = "/conversations/"
        static let API_CONVERSATION_START = "/start-discussion"
        static let API_CONVERSATION_REQUEST = "/request-discussion"
        
        static let API_QUESTION = "/questions/"
        static let API_ANSWER = "/answer"
        static let API_VOTE = "/vote"
        //
        static let API_NOTIFICATION = "/notifications"
        static let API_ALL = "/all"
        
        //Subfix
        static let API_ARCHIVE = "/archive"
        static let API_STATUS = "/status"
        //Message
        static let API_GET_ALL_MESSAGE = "/messages"
        static let API_SEND_TEXT_MESSAGE = "/messages"
        static let API_SEND_TEXT_ATTACHMENT = "/messages/attachments"
        //
        static let API_GEN_OTP = "/v2.1/genotp"
        static let API_LOGIN_ACCOUNT_OTHER_APP = "/v2.1/genotp"
        static let API_CHECK_VERSION = "/v2.1/app/checkCurrentVersion"
        static let API_UPDATE_CONTACT = "/v2/contact/setContact"
        static let API_ADD_CONTACT = "/v2/contact/addContact"
        static let API_GET_CONTACT = "/v2/contact/getContact"
        static let API_REMOVE_CONTACT = "/v2/contact/removeContact"
        static let API_GET_USER_INFO = "/v2/user/getUserInfo"
        static let API_UPDATE_USER_INFO = "/v2/user/setUserInfo"
        static let API_UPLOAD_MEDIA = "/v2/upload/file"
        static let API_UPLOAD_AVATAR = "/v2/upload/avatar"
        static let API_UPLOAD_PHOTO = "/v2/upload/publicImage"
        static let API_DOWNLOAD_MEDIA = "/v2/download"
        static let API_SET_UNREAD_MESSAGE = "/message/setUnreadNumber"
        static let API_INVITE_USE_APP = "/v2/sms/sendInviteSms"
    }
    
    enum APIStatusCode: Int {
        case MOYA_REQUEST_TIMEOUT = 6
        case CONTINUE = 100
        case OK = 200
        case CREATED = 201
        case ACCEPTED = 202
        case NO_CONTENT = 204
        case BAD_REQUEST = 400
        case UNAUTHORIZED = 401
        case FORBIDDEN = 403
        case NOT_FOUND = 404
        case NOT_ACCEPTABLE = 406
        case REQUEST_TIMEOUT = 408
        case CONFLICT = 409
        case REQUEST_ENTITY_TOO_LARGE = 413
        case UNSUPPORTED_MEDIA_TYPE = 415
        case UNPROCESSABLE_ENTITY = 422
        case TOO_MANY_REQUESTS = 429
        case INTERNAL_SERVER_ERROR = 500
        case NOT_IMPLEMENTED = 501
        case BAD_GATEWAY = 502
        case SERVICE_UNAVAILABLE = 503
        case GATEWAY_TIMEOUT = 504
    }
    
    enum APIStatusString: String {
        case SUCCESS = "success"
        case FAIL = "fail"
    }
    
}


struct FontNames {
    static let LatoName = "Lato"
    struct Lato {
        static let MULI_REGULAR = "Lato-Regular"
        static let MULI_LIGHT = "Lato-Light"
        static let MULI_LIGHT_ITALIC = "Lato-LightItalic"
        static let MULI_BOLD = "Lato-Bold"
        static let MULI_ITALIC = "Lato-Italic"
        static let MULI_BOLD_ITALIC = "Lato-BoldItalic"
        static let MULI_HAIRLINE = "Lato-Hairline"
        static let MULI_HAIRLINE_ITALIC = "Lato-HairlineItalic"
        static let MULI_BLACK = "Lato-Black"
        static let MULI_BLACK_ITALIC = "Lato-BlackItalic"
    }
}

struct EventCode {
    static let MODEL_EVENT_CODE_SUCCESS = 0
    static let MODEL_EVENT_CODE_FAILURE = -1
}

struct ButtonState {
    static let Normal = 0
    static let Active = 1
}


struct responseMessage{
    static let errorCode = "errorCode"
    static let result = "result"
}

struct AppColor {
    private struct Alphas {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }

    struct NormalColors {
        static let NORMAL_COLOR = UIColor.darkGray
        static let WHITE_COLOR = UIColor.white
        static let RED_COLOR = UIColor.red
        static let YELLOW_COLOR = UIColor(red: CGFloat(246.0)/255, green: CGFloat(159.0)/255, blue: CGFloat(41.0)/255, alpha: Alphas.Opaque)
        static let LIGHT_YELLOW_COLOR = UIColor(red: CGFloat(238.0)/255, green: CGFloat(237.0)/255, blue: CGFloat(168.0)/255, alpha: Alphas.Opaque)
        static let BLACK_COLOR = UIColor(red: CGFloat(31.0)/255, green: CGFloat(37.0)/255, blue: CGFloat(50.0)/255, alpha: Alphas.Opaque)
        static let CLEAR_COLOR = UIColor.clear
        static let BG_VIEW_COLOR = UIColor(red: CGFloat(250.0)/255, green: CGFloat(250.0)/255, blue: CGFloat(250.0)/255, alpha: Alphas.Opaque)
        static let MAIN_COLOR = UIColor(hexString: "#00bcd4")
        static let BLUE_COLOR = UIColor(red: CGFloat(0.0)/255, green: CGFloat(128.0)/255, blue: CGFloat(255.0)/255, alpha: Alphas.Opaque)
        static let GREEN_COLOR = UIColor(red: CGFloat(124.0)/255, green: CGFloat(177.0)/255, blue: CGFloat(37.0)/255, alpha: Alphas.Opaque)
        static let GRAY_COLOR = UIColor(red: CGFloat(222.0)/255, green: CGFloat(222.0)/255, blue: CGFloat(222.0)/255, alpha: Alphas.Opaque)
        static let GRAY_1_COLOR = UIColor(red: CGFloat(140.0)/255, green: CGFloat(140.0)/255, blue: CGFloat(140.0)/255, alpha: Alphas.Opaque)
        static let LIGHT_BLUE_COLOR = UIColor(red: CGFloat(116.0)/255, green: CGFloat(155.0)/255, blue: CGFloat(190.0)/255, alpha: Alphas.Opaque)
        static let LIGHT_GRAY_COLOR = UIColor(red: CGFloat(183.0)/255, green: CGFloat(188.0)/255, blue: CGFloat(196.0)/255, alpha: Alphas.Opaque)
        static let LIGHT_RED_COLOR = UIColor(red: CGFloat(250.0)/255, green: CGFloat(80.0)/255, blue: CGFloat(80.0)/255, alpha: Alphas.Opaque)
        static let LIGHT_ORANGE_COLOR = UIColor(red: CGFloat(253.0)/255, green: CGFloat(165.0)/255, blue: CGFloat(59.0)/255, alpha: Alphas.Opaque)
        static let DARK_COLOR = UIColor(hexString: "#595959")
        static let DARK_THEME_COLOR = UIColor(hexString: "#334054")
    }

    struct MicsColors {
        static let ONBOARDING_BACKGROUND_COLOR = UIColor(red: CGFloat(239.0)/255, green: CGFloat(249.0)/255, blue: CGFloat(255.0)/255, alpha: Alphas.Opaque)
        static let CHATTING_TITLE_NAME_COLOR = UIColor(red: CGFloat(32.0)/255, green: CGFloat(46.0)/255, blue: CGFloat(67.0)/255, alpha: Alphas.Opaque)
        static let TITLE_BOLD_DESIGN_COLOR = UIColor(red: CGFloat(149.0)/255, green: CGFloat(149.0)/255, blue: CGFloat(149.0)/255, alpha: Alphas.Opaque)
        static let LINE_VIEW_NORMAL = UIColor(red: CGFloat(216.0)/255, green: CGFloat(216.0)/255, blue: CGFloat(216.0)/255, alpha: Alphas.Opaque)
        static let DARK_MENU_COLOR = UIColor(red: CGFloat(31.0)/255, green: CGFloat(37.0)/255, blue: CGFloat(50.0)/255, alpha: Alphas.Opaque)
        static let DARK_LIGHT_ITEM_COLOR = UIColor(red: CGFloat(43.0)/255, green: CGFloat(49.0)/255, blue: CGFloat(62.0)/255, alpha: Alphas.Opaque)
        static let DARK_LIGHT_MENU_COLOR = UIColor(red: CGFloat(51.0)/255, green: CGFloat(64.0)/255, blue: CGFloat(83.0)/255, alpha: Alphas.Opaque)
        static let FILE_MESSAGE_BORDER_COLOR = UIColor(red: CGFloat(237.0)/255, green: CGFloat(237.0)/255, blue: CGFloat(237.0)/255, alpha: Alphas.Opaque)
        static let SHIMMER_COLOR = UIColor(red: CGFloat(170.0)/255, green: CGFloat(170.0)/255, blue: CGFloat(170.0)/255, alpha: 0.18)
        static let LINE_COLOR = UIColor(hexString: "#eef1f4")
        static let LINE_COLOR_DARK = UIColor(hexString: "#526480")
        
        
        static let BLUE_LIGHT_COLOR = UIColor(red: CGFloat(213.0)/255, green: CGFloat(235.0)/255, blue: CGFloat(255.0)/255, alpha: Alphas.Opaque)
        static let BLUE_LIGHT_1_COLOR = UIColor(red: CGFloat(230.0)/255, green: CGFloat(243.0)/255, blue: CGFloat(255.0)/255, alpha: Alphas.Opaque)
        static let BLUE_LIGHT_2_COLOR = UIColor(red: CGFloat(128.0)/255, green: CGFloat(169.0)/255, blue: CGFloat(209.0)/255, alpha: Alphas.Opaque)
        static let BLUE_DARK_COLOR = UIColor(red: CGFloat(161.0)/255, green: CGFloat(194.0)/255, blue: CGFloat(227.0)/255, alpha: 0.8)
        
        static let DISABLE_COLOR = UIColor(red: CGFloat(245.0)/255, green: CGFloat(245.0)/255, blue: CGFloat(245.0)/255, alpha: Alphas.Opaque)
        static let DISABLE_DARK_COLOR = UIColor(hexString: "#526480")
        
        //Toast
        static let TOAST_DARK_COLOR = UIColor(hexString: "#E8E8E8")
        static let TOAST_LIGHT_COLOR = UIColor(hexString: "#1F2532")
    }
    
    struct RandomColors {
        static let COLOR_1 = "#ea477f"
        static let COLOR_2 = "#dc5c60"
        static let COLOR_3 = "#825cc2"
        static let COLOR_4 = "#aa4dbc"
        static let COLOR_5 = "#fd7453"
        static let COLOR_6 = "#41aa9c"
        static let COLOR_7 = "#fda93d"
        static let COLOR_8 = "#42b6f5"
        static let COLOR_9 = "#6670bf"
        static let COLOR_10 = "#8f756c"
    }
    
//    struct RandomGradientColors {
//        static let COLOR_1 = [UIColor(hexString: "5B4AF5"), UIColor(hexString: "26D4FF")]
//        static let COLOR_2 = "#dc5c60"
//        static let COLOR_3 = "#825cc2"
//        static let COLOR_4 = "#aa4dbc"
//        static let COLOR_5 = "#fd7453"
//        static let COLOR_6 = "#41aa9c"
//        static let COLOR_7 = "#fda93d"
//        static let COLOR_8 = "#42b6f5"
//        static let COLOR_9 = "#6670bf"
//        static let COLOR_10 = "#8f756c"
//    }
}

struct LocalImage {
    static let ImagePlaceHolder = "image_placeholder.png"
    
    struct UserImage{
        static let Avatar = "ic_default_avatar.png"
        static let Avatar_Small = "ic_user_small.png"
        static let Company = "ic_default_logo.png"
    }
    
    static let FilePlaceHolder : UIImage = UIImage(named:ImagePlaceHolder)!
    static let UserPlaceHolder : UIImage = UIImage(named: UserImage.Avatar)!
    
    enum imageExtension : String {
        case jpg = "jpg"
        case jpeg = "jpeg"
        case png = "png"
        case ico = "ico"
        
        static let allValues = [jpg, jpeg, png, ico]
    }
    
    enum fileExtension : String {
        case pdf = "pdf"
        case doc = "doc"
        case docx = "docx"
        case excel = "xls"
        case excelx = "xlsx"
        case ppt = "ppt"
        case pptx = "pptx"
        
        static let allValues = [pdf, doc,docx, excel, excelx,ppt,pptx]
    }
   
    struct ChattingImage{
        static let Close = "ic_attach_close"
        static let Image = "ic_attach_image"
        static let Camera = "ic_chatting_camera"
        static let Contact = "ic_attach_contact"
        static let Buzz = "ic_attach_buzz"
        static let File = "ic_attach_file"
        static let Fail = "btn_resent.png"
        static let Typing = "ic_typing-bubble"
        
        struct Send{
            static let Selected = "ic_chatting_send_selected_new"
            static let NotSelected = "ic_chatting_send_new"
        }
        struct Mention{
            static let Selected = "ic_mention_selected"
            static let NotSelected = "ic_mention"
        }
        struct Attach{
            static let Selected = "ic_chatting_attachment_selected_new"
            static let NotSelected = "ic_chatting_attachment_new"
        }
    }
    
    struct IconImage{
        static let Menu = "ic_menu.png"
        static let Seperator = "separator_cell.png"
        static let Attach = "ic_conversation_attach"
    }
    
    struct ContactIcon{
        static let Message = "ic_contact_message.png"
        static let Invite = "ic_invite_app.png"
        static let Invite_Success = "ic_invite_app_successful.png"
        static let Invite_Disable = "ic_invite_app_disable.png"
    }
    
    struct SpinIcon{
        static let On = "ic_spin.png"
        static let Off = "ic_no_spin.png"
    }
    
    struct PinIcon{
        static let Off = "ic_pin_off.png"
        static let On = "ic_pin_on.png"
    }
    
    struct MarkIcon{
        static let Mark_On = "ic_mark.png"
        static let Mark_Off = "ic_no_mark.png"
    }
    
    struct FileIcon{
        static let pdf = "ic_file_pdf.png"
        static let doc = "ic_file_word.png"
        static let excel = "ic_file_excel.png"
        static let ppt = "ic_file_powerpoint.png"
        static let txt = "ic_file_text.png"
    }
}



struct Constant {
    static let keyId = "keyId"
    static let keyValue = "keyValue"
    
    static let keyHolderUnassignedId = "HolderUnassignedId"
    
    struct UTCTimes{
        static let Ireland = 7
        static let Vn = 7
    }
    
    enum NetworkType: Int {
        case kNetworkTypeNon = 0
        case kNetworkTypeConnect
        case kNetworkTypeDisConnect
    }
    
    enum GenderType: Int {
        case kGenderTypeMale = 0
        case kGenderTypeFemale
    }
    
    enum ChangeContactType: Int {
        case kChangeContactTypeInsert = 0
        case kChangeContactTypeChange
        case kChangeContactTypeDelete
        case kChangeContactTypeAll
        case kGetContactAll
    }
    
    enum MessagePosition: Int {
        case none = 0
        case senderFirstEnd
        case senderFirstFirst
        case senderCenter
        case senderEnd
        case receiverFirstEnd
        case receiverFirst
        case receiverCenter
        case receiverEnd
    }
    
    enum MessageDirection: Int {
        case left = 0
        case right
    }
    
    enum MessageState: Int {
        case kMessageStateNon = 0
        case kMessageStateSending
        case kMessageStateFail
        case kMessageStateSent
        case kMessageStateDelivery
        case kMessageStateUploadFailed
        case kMessageStateUploading
        case kMessageStateDownloadFailed
        case kMessageStateDownloading
        case kMessageStateSeen
    }
    
    enum MessageType: Int {
        case kMessageTypeNon = 0
        case kMessageTypeImage
        case kMessageTypeText
        case kMessageTypeFile
        case kMessageTypeInfoJoinedPerson
        case kMessageTypeInfoAddPerson
        case kMessageTypeInfoRemovePerson
        case kMessageTypeInfoLeaveGroup
        case kMessageTypeInfoTimeStamp
        case kMessageTypeInfoCreateTask
        case kMessageTypeNoneComment
    }
    
    
    enum UserState: Int {
        case kUserStateOnline = 0
        case kUserStateOffline
        case kUserStateBusy
        case kUserStateAway
    }
    
    enum SettingMenu: Int {
        case kSettingMenuChangePass = 0
        case kSettingMenuNotification
        case kSettingMenuInvite
        case kSettingMenuOrangnization
        case kSettingMenuSignout
    }
    
    enum SettingMenuNotification: Int {
        case kSettingMenuPushNotification = 0
        case kSettingMenuEmailNotification
    }
    
    enum ProfileMenu: Int {
        case kProfileMenuName = 0
        case kProfileMenuEmail
        case kProfileMenuTeamName
        case kProfileMenuTeamType
        case kProfileMenuTeamSize
        case kProfileMenuTeamLeader
    }
    
    enum ProfileUpdateInfoType: Int {
        case kProfileUpdateNone = 0
        case kProfileUpdateAvatar
        case kProfileUpdateInfo
        case kProfileUpdateAll
        case kProfileUpdateSettingNotification
    }
    
    enum ProjectUpdateInfoType: Int {
        case kProjecUpdateNone = 0
        case kProjecUpdateInfo
        case kProjecUpdatePlanningType
        case kProjecUpdateOwner
    }
    
    enum ProjectUserPlanType: Int {
        case kProjectUserPlanFree = 0
        case kProjectUserPlanTrial
        case kProjectUserPlanPro
    }
    
    enum AuthenAccountState: Int {
        case kAuthenAccountStateNon = 0
        case kAuthenAccountStateConnecting
        case kAuthenAccountStateConnected
        case kAuthenAccountStateFailed
    }
    
    @objc enum DataModelType: Int {
        case empty = 0
        case holder
        case normal
        case processing
        case failed
        case other
    }
    
   
    enum JsonMessageType: Int {
        case agenda = 0
        case room
        case notification
    }
    
    struct ChattingView {
        static let Bubble_Info_Text_Font_Large : UIFont = UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15)!
        //
        static let Padding_Chatting_Cell : CGFloat = 16
        static let Padding_Top_Chatting_Cell : CGFloat = 8
        static let Padding_Bottom_Chatting_Cell : CGFloat = 8
        static let Padding_Info_Chatting_Cell : CGFloat = 10
        static let MaxWidth : CGFloat = AppDevice.ScreenComponent.Width
        static let Width_Info_Message : CGFloat = MaxWidth
        static let Width_Info_Message_Frame_Display : CGFloat = Width_Info_Message - (Padding_Chatting_Cell * 2)
        static let Buble_TextView_Avatar_MaxWidth : CGFloat = MaxWidth - (AppDevice.SubViewFrame.AvatarIconMediumWidth + Padding_Chatting_Cell * 2 + Padding_Chatting_Cell)
        static let Buble_TextView_Avatar_MaxHeight : CGFloat = AppDevice.SubViewFrame.TitleHeight * 2
        static let Left_Content_Insets : UIEdgeInsets = UIEdgeInsets(top: 10.0, left: 13.0, bottom: 10.0, right: 13.0)
        static let Right_Content_Insets : UIEdgeInsets = UIEdgeInsets(top: 10.0, left: 13.0, bottom: 10.0, right: 13.0)
        
        static let Height_Input_Container_Message : CGFloat = 76.0
        static let Height_Input_TextView_Message : CGFloat = 44.0
    }
    
    struct RoomTypes{
        static let task = "dis"
        static let attachment  = "attachment"
        static let agenda  = "agenda"
        static let document  = "doc"
    }
    
    struct ThemeTypes{
        static let light = "light"
        static let dark  = "dark"
    }
    
    struct TaskFilter{
        enum TaskID: Int {
            case allTask = 0
            case myTask
            case priorityTask
            case completedTask
            case myUncompletedTask
            case allUncompletedTask
        }
        
        
        static let allTalk : [String : Any] = [keyId: TaskID.allTask,keyValue: NSLocalizedString("all_talks", comment: "")]
        static let myTask : [String : Any] = [keyId: TaskID.allTask,keyValue: NSLocalizedString("my_talks", comment: "")]
        static let priorityTask : [String : Any] = [keyId: TaskID.allTask,keyValue: NSLocalizedString("priority_talks", comment: "")]
        static let completedTask : [String : Any] = [keyId: TaskID.allTask,keyValue: NSLocalizedString("completed_talks", comment: "")]
    }
    
    struct CollectionItemAgenda{
        static let Width : CGFloat = 200 * (AppDevice.DeviceType.isIpad ? 2 : 1)
        static let Height : CGFloat = 252 * (AppDevice.DeviceType.isIpad ? 2 : 1)
        static let LargeWidth : CGFloat = 320 * (AppDevice.DeviceType.isIpad ? 2 : 1)
    }
    
    struct Tags{
        static let LabelNameImageView : Int = 113
    }
    
    enum ViewTypeAddUpdateInsert: Int {
        case add = 0
        case update
        case insert
    }
    
    enum NavigationTypeAddViewController: Int {
        case push = 0
        case present
    }
    
    enum FindType: Int {
        case findIn = 0
        case findOut
    }
    
    struct MaxLength {
        static let Name : Int = 50
        static let Task : Int = 80
        static let Password : Int = 50
        static let RandomKey : Int = 20
        static let Message : Int = 500
        static let TaskDescription : Int = 500
    }
    
    struct MinLength {
        static let Name : Int = 2
        static let Task : Int = 2
        static let Password : Int = 8
        static let Message : Int = 2
    }
    
    enum AlertType: Int {
        case kAlertTypeConfirm = 0
        case kAlertTypeImage
        case kAlertTypeSettingCamera
        case kAlertTypeSettingPhotoAlbum
    }
    
    enum TaskStatus: Int {
        case kTaskStatusUncompleted = 0
        case kTaskStatusWIP = 1
        case kTaskStatusCompleted = 2
    }
    
    enum PushNotificationDataType: Int {
        case emptyNotification = 0
        case newCommentAgenda
        case planTask
        case assignTask
        case newCommentTask
    }
}

struct NotificationTypeKeyString {
    static let NotificationType_AddTask = "at"
    static let NotificationType_RemoveUser = "ru"
    static let NotificationType_AddUser = "au"
    static let NotificationType_RenameTask = "ctn"
    static let NotificationType_RemoveComment = "rmd"
    static let NotificationType_AddMessage = "normal"
    static let NotificationType_commentTask = "commentTask"
    static let NotificationType_AddMessageAttachment = "normal_attachment"
    static let NotificationType_AddFile = "att"
    static let NotificationType_AddAssignee = "assignee"
    static let NotificationType_Mention = "mention"
    static let NotificationType_TaskPriority = "task-priority"
    static let NotificationType_TaskUnpriority = "task-unpriority"
    static let NotificationType_TaskDuedate = "task-duedate"
    static let NotificationType_TaskDescription = "task-desc"
    static let NotificationType_TaskCompleted = "task-u-status-completed"
    static let NotificationType_TaskUncompleted = "task-u-status-uncompleted"
    static let NotificationType_TaskStatus = "task-u-status"
    static let NotificationType_TaskUnassignee = "task-unassignee"
    static let NotificationType_TaskLabel = "addLabelToTask"
    static let NotificationType_SunmitAgenda = "submitAgenda"
    static let NotificationType_DoneAgenda = "doneAgenda"
    static let NotificationType_CompletedAllTasksAgenda = "AgendaCompletedAllTasks"
    static let NotificationType_CommentAgenda = "commentAgenda"
    static let NotificationType_ChangeTask = "changeTask"
    static let NotificationType_Other = "other"
}

struct NotificationCenterKey{
    static let NOTIFICATION_RETRY_NETWORK = "NOTIFICATION_RETRY_NETWORK"
    static let NOTIFICATION_RETRY_WIP = "NOTIFICATION_RETRY_WIP"
    static let NOTIFICATION_CANCEL_WIP = "NOTIFICATION_CANCEL_WIP"
    static let NOTIFICATION_ERROR_WIP = "NOTIFICATION_ERROR_WIP"
    static let NOTIFICATION_LOADING_ERROR_WIP = "NOTIFICATION_LOADING_ERROR_WIP"
    //
    static let MWPHOTO_LOADING_DID_END_NOTIFICATION = "MWPHOTO_LOADING_DID_END_NOTIFICATION"
    static let MWPHOTO_PROGRESS_NOTIFICATION = "MWPHOTO_PROGRESS_NOTIFICATION"
    static let NOTIFICATION_DOWNLOADING_PROGRESS = "NOTIFICATION_DOWNLOADING_PROGRESS"
    
    static let NOTIFICATION_UPLOADING_PROGRESS = "NOTIFICATION_UPLOADING_PROGRESS"
    static let NOTIFICATION_UPLOAD_FAIL = "NOTIFICATION_UPLOAD_FAIL"
    static let NOTIFICATION_UPLOAD_SUCCESSFULLY = "NOTIFICATION_UPLOAD_SUCCESSFULLY"
    
    static let NOTIFICATION_UPLOADING_API_PROGRESS = "NOTIFICATION_UPLOADING_API_PROGRESS"
    static let NOTIFICATION_UPLOAD_API_FAIL = "NOTIFICATION_UPLOAD_API_FAIL"
    static let NOTIFICATION_UPLOAD_API_SUCCESSFULLY = "NOTIFICATION_UPLOAD_API_SUCCESSFULLY"
    
    static let NOTIFICATION_UPLOAD_PROCESSING_AVATAR = "NOTIFICATION_UPLOAD_PROCESSING_AVATAR"
    static let NOTIFICATION_UPLOAD_FAIL_AVATAR = "NOTIFICATION_UPLOAD_FAIL_AVATAR"
    static let NOTIFICATION_UPLOAD_SUCCESSFULLY_AVATAR = "NOTIFICATION_UPLOAD_SUCCESSFULLY_AVATAR"
    //
    static let NOTIFICATION_UPDATE_USER_INFO = "NOTIFICATION_UPDATE_USER_INFO"
    static let NOTIFICATION_UPDATE_TEAM_USER_INFO = "NOTIFICATION_UPDATE_TEAM_USER_INFO"
    static let NOTIFICATION_UPDATE_SIGNLE_USER_INFO = "NOTIFICATION_UPDATE_SIGNLE_USER_INFO"
    static let NOTIFICATION_UPDATE_TASK_ITEM = "NOTIFICATION_UPDATE_TASK_ITEM"
    static let NOTIFICATION_DELETE_TASK_ITEM = "NOTIFICATION_DELETE_TASK_ITEM"
    static let NOTIFICATION_UPDATE_PROJECT_INFO = "NOTIFICATION_UPDATE_PROJECT_INFO"
    static let NOTIFICATION_UPDATE_PROJECT_PLANNING_TYPE = "NOTIFICATION_UPDATE_PROJECT_PLANNING_TYPE"
    static let NOTIFICATION_UPDATE_PROJECT_OWNER_TYPE = "NOTIFICATION_UPDATE_PROJECT_PLANNING_TYPE"
    //
    static let NOTIFICATION_UPDATE_ORANGNIZATION_INFO = "NOTIFICATION_UPDATE_ORANGNIZATION_INFO"
    //
    static let NOTIFICATION_RECEIVE_DEVICE_TOKEN = "NOTIFICATION_RECEIVE_DEVICE_TOKEN"
    //
    static let NOTIFICATION_SETTING_NOTIFICATION = "NOTIFICATION_SETTING_NOTIFICATION"
    static let NOTIFICATION_NUMBER_READ_NOTIFICATION = "NOTIFICATION_NUMBER_READ_NOTIFICATION"
    static let NOTIFICATION_NEW_DATA_NOTIFICATION = "NOTIFICATION_NEW_DATA_NOTIFICATION"
    static let NOTIFICATION_NEW_REALTIME_DATA_NOTIFICATION = "NOTIFICATION_NEW_REALTIME_DATA_NOTIFICATION"
}

struct UserInfoKeyString {
    static let MY_USERID_KEY = "ID"
    static let MY_USERNAME_KEY = "USERNAME"
    static let MY_NAME_KEY = "NAME"
    static let MY_TEAMNAME_KEY = "TEAMNAME"
    static let MY_PHONE_KEY = "PHONE"
    static let MY_EMAIL_KEY = "EMAIL"
    static let MY_EMAIL_VERIFIED_KEY = "EMAIL_VERIFIED"
    static let MY_FIRSTNAME_KEY = "FIRSTNAME"
    static let MY_MIDDLENAME_KEY = "MIDDLENAME"
    static let MY_LASTNAME_KEY = "LASTNAME"
    static let MY_TOKEN_KEY_API = "TOKEN_API"
    static let MY_TOKEN_KEY_SUB = "TOKEN_SUB"
    static let MY_DEVICE_TOKEN_KEY = "DEVICE_TOKEN"
    static let MY_PASSWORD_KEY = "PASSWORD"
    static let MY_LOCAL_AVATAR_KEY = "LOCALAVATAR"
    static let MY_URL_AVATAR_KEY = "URLAVATAR"
    static let MY_LASTCHANGE_AVATAR_KEY = "LASTCHANGEAVATAR"
    static let MY_ACCOUNT_TYPE_KEY = "ACCOUNTTYPE"
    static let MY_CREATED_AT_KEY = "CREATEDAT"
    static let MY_IS_LOGIN_KEY = "ISLOGIN"
    static let MY_IS_LOGIN_OTHER_SYSTEM_KEY = "ISLOGINOTHERSYSTEM"
    static let MY_IS_LOGIN_OTHER_SYSTEM_CHANGE_PASS_KEY = "ISLOGINOTHERSYSTEMCHANGEPASS"
    static let MY_IS_WELCOME_KEY = "ISWELCOMEKEY"
    static let MY_IS_EMAIL_STATUS_KEY = "ISEMAILSTATUSKEY"
    static let MY_IS_NOTIFICATION_STATUS_KEY = "ISNOTIFICATIONSTATUSKEY"
    static let MY_IS_SEND_NOTIFICATION_KEY = "ISSENDNOTIFICATION"
    static let MY_NUMBER_UNREAD_NOTIFICATION = "NUMBERUNREADNOTIFICATION"
    static let MY_UTC_OFFSET = "MYUTCOFFSET"
    static let MY_STATUS_KEY = "STATUS"
    static let MY_THEME_KEY = "THEME"
}

struct SettingsProjectKeyString {
    static let MY_SETTING_PROJECT_ID_KEY = "MY_SETTING_PROJECT_ID_KEY";
    static let MY_SETTING_PROJECT_NAME_KEY = "MY_SETTING_PROJECT_NAME_KEY";
    static let MY_SETTING_PROJECT_TEAMTYPE_KEY = "MY_SETTING_PROJECT_TEAMTYPE_KEY";
    static let MY_SETTING_PROJECT_TEAMSIZE_KEY = "MY_SETTING_PROJECT_TEAMSIZE_KEY";
    static let MY_SETTING_OWNER_ID_KEY = "MY_SETTING_OWNER_ID_KEY";
    static let MY_SETTING_OWNER_NAME_KEY = "MY_SETTING_OWNER_NAME_KEY";
    static let MY_SETTING_PROJECT_PLAINING_TYPE_KEY = "MY_SETTING_PROJECT_PLAINING_TYPE_KEY";
    static let MY_SETTING_PROJECT_PLAINING_INVITE_KEY = "MY_SETTING_PROJECT_PLAINING_INVITE_KEY";
    static let MY_SETTING_PROJECT_UTC_OFFSET = "MY_SETTING_PROJECT_UTC_OFFSET";
}

struct SettingsOrangnizationKeyString {
    static let MY_SETTING_ORANGNIZATION_ID_KEY = "MY_SETTING_ORANGNIZATION_ID_KEY";
    static let MY_SETTING_ORANGNIZATION_NAME_KEY = "MY_SETTING_ORANGNIZATION_NAME_KEY";
    static let MY_SETTING_ORANGNIZATION_PLAN_TYPE_KEY = "MY_SETTING_ORANGNIZATION_PLAN_TYPE_KEY";
    static let MY_SETTING_ORANGNIZATION_MAX_INVITE_KEY = "MY_SETTING_ORANGNIZATION_MAX_INVITE_KEY";
}

struct ModelDataKeyString {
    static let COLLECTION_MESSAGE = "rocketchat_message";
    static let COLLECTION_FILE = "collection_file";
    static let COLLECTION_TASK = "collection_task";
    static let COLLECTION_USER = "collection_user";
    static let COLLECTION_NOTIFICATION = "collection_notification";
    static let COLLECTION_ORANGNIZATION = "collection_orangnization";
}

struct SettingString {
    static let Icon = "icon";
    static let Title = "title";
    static let Message = "message";
    static let TitleColor = "titleColor";
    static let Progress = "progress";
    static let Photo = "photo";
    static let UserId = "userId";
    static let ImageName = "imageName";
    static let ImageData = "imageData";
    static let ImageSource = "imageSource";
    static let FileId = "fileId";
    static let Tasks = "tasks";
    static let Loading = "loading";
    static let Action = "action";
    static let Date = "date";
    static let NotificationType = "type";
    static let ProjectId = "pid";
    static let AgendaId = "aid";
    static let RoomId = "rid";
    static let RoomName = "RoomName";
    static let RoomType = "RoomType";
    static let MessageId = "mId";
    static let NotificationId = "NotificationId";
}




