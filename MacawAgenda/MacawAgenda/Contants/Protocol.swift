//
//  Protocol.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import RxSwift
import RxCocoa

enum ValidationResult {
    case ok(message: String)
    case empty
    case validating
    case failed(message: String)
}


protocol EmailValidationService {
    func validateExitUsername(_ email: String) -> Observable<ValidationResult>
    func validateRegexEmail(_ email: String) -> ValidationResult
}

protocol FirestoreTaskAPI {
    //Get once
    func getAllTasks(projectId: String, userId: String, search: String, sort: Int, duedate : Date?) -> Observable<[Task]>
    func getTaskByIds(projectId: String, userId: String, ids: [String], findType : Constant.FindType, byMe: Bool,needEmptyData: Bool) -> Observable<[Task]>
    func getTaskById(taskId: String) -> Observable<[Task]>
    //Subcribe realtime
    func subscribleAllTasks(projectId: String, userId: String, search: String, sort: Int, duedate : Date?) -> Observable<[Task]>
    func subscribleTaskByIds(projectId: String, userId: String, ids: [String], findType : Constant.FindType, byMe: Bool,needEmptyData: Bool,needSortByTimeStamp: Bool) -> Observable<[Task]>
    func subscribleTaskById(taskId: String) -> Observable<[Task]>
}

protocol FirestoreAgendaAPI {
    //Get once
    func getAllAgendaByTeam(projectId : String, userId : [User], duedate : Date, sortUserId : String,otherEmpty: Bool) -> Observable<[Agenda]>
    func getAllAgendaById(projectId : String,userId : String, duedate : Date) -> Observable<[Agenda]>
    //Subcribe realtime
    func subscribleAllAgendaByTeam(projectId : String, userId : [User], duedate : Date, sortUserId : String,otherEmpty: Bool) -> Observable<[Agenda]>
    func subscribleAllAgendaById(projectId : String,userId : String, duedate : Date) -> Observable<[Agenda]>
    func subscribleAllTodayAgendaByProject(projectId : String, duedate : Date) -> Observable<[Agenda]>
}

protocol FirestoreFileAPI {
    //Get once
    func getAllFileOfTaskId(taskId: String) -> Observable<[File]>
    //Subcribe realtime
    func subscribleAllFileOfTaskId(taskId: String) -> Observable<[File]>
}

protocol FirestoreMessageAPI {
    //Get once
    func getAllMessageOfTaskId(taskId: String) -> Observable<[Message]>
    func getAllMessageOfAgendaId(roomId: String) -> Observable<[Message]>
    //Subcribe realtime
    func subscribleAllMessageOfTaskId(taskId: String) -> Observable<[Message]>
    func subscribleAllMessageOfAgendaId(roomId: String) -> Observable<[Message]>
}

protocol FirestoreActivityAPI {
    //Get once
    func getAllActivityOfRoomId(roomId: String) -> Observable<[Activity]>
    //Subcribe realtime
    func subscribleAllActivityOfRoomId(roomId: String) -> Observable<[Activity]>
}

protocol FirestoreRoomAPI {
    //Get once
    func getRoomOfAgenda(projectId: String,_ userId: String,_ duedate : Date) -> Observable<[Room]>
    //Subcribe realtime
    func subscribleRoomOfAgenda(projectId: String,_ userId: String,_ duedate : Date) -> Observable<[Room]>
}

protocol FirestoreOrangnizationAPI {
    //Get once
    func getOrangnizationById(orangnizationId: String) -> Observable<[Orangnization]>
    func getOrangnizationByUserId(userId: String) -> Observable<[Orangnization]>
    //Subcribe realtime
    func subscribleOrangnizationById(orangnizationId: String) -> Observable<[Orangnization]>
    func subscribleOrangnizationByUserId(userId: String) -> Observable<[Orangnization]>
//    func subscribleProjectById(projectId: String) -> Observable<[Project]>
//    func subscribleUsersByProjectId(projectId: String) -> Observable<[User]>
}

protocol FirestoreProjectAPI {
    //Get once
    func getProjectById(projectId: String) -> Observable<[Project]>
    func getUsersByProjectId(projectId: String) -> Observable<[User]>
    //Subcribe realtime
    func subscribleProjectById(projectId: String) -> Observable<[Project]>
    func subscribleUsersByProjectId(projectId: String) -> Observable<[User]>
    func subscribleProjectByUserId(userId: String,orangnizationId: String) -> Observable<[Project]>
}

protocol FirestoreNotificationAPI {
    //Get once
    func getNotificationByUserId(projectId: String,userId: String)  -> Observable<[NotificationData]>
    func getNotificationNumberByUserId(projectId: String, userId: String) -> Observable<Counter>
    //Subcribe realtime
    func subscribleNotificationByUserId(projectId: String,userId: String)  -> Observable<[NotificationData]>
}

protocol FirestoreUserAPI {
    //Get once
    func getUserInfo(_ userId: String) -> Observable<User>
    func getMyUserInfo(_ userId: String) -> Observable<Account>
    //Subcribe realtime
    func subscribleUserInfo(_ userId: String) -> Observable<User>
    func subscribleMyUserInfo(_ userId: String) -> Observable<Account>
}


extension ValidationResult {
    var isValid: Bool {
        switch self {
        case .ok:
            return true
        default:
            return false
        }
    }
}

