//
//  AccessiblityIdConstants.swift
//  MacawAgenda
//
//  Created by tranquangson on 9/6/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

struct AccessiblityId {
    //Normal
    static let BTN_BACK = "btn_Back"
    static let BTN_CANCEL = "btn_Cancel"
    static let BTN_SAVE = "btn_Save"
    static let BTN_CHANGE = "btn_Change"
    static let VIEW_OVERPLAY = "view_Overplay"
    static let LB_DATE = "lb_Date"
    
    //Onboarding
    static let SCROLLVIEW_CONTENT = "srv_Content"
    static let BTN_LOGIN_GOOGLE = "btn_LoginGoogle"
    static let BTN_LOGIN_EMAIL = "btn_LoginEmail"
    
    //Acccounts
    static let LB_USERNAME = "lb_Username"
    static let TF_PASSSWORD = "tf_Password"
    static let LB_ERROR_PASSSWORD = "lb_ErrorPassword"
    static let TF_EMAIL = "tf_Email"
    static let LB_ERROR_EMAIL = "lb_ErrorEmail"
    static let BTN_LOGIN = "btn_Login"
    static let BTN_SIGNUP = "btn_Signup"
    static let BTN_RESET = "btn_Reset"
    static let BTN_INPUTEMAIL = "btn_InputEmail"
    static let BTN_FORGET = "btn_Forget"
    static let LB_EMAIL = "lb_Email"
    static let TF_NAME = "tf_Name"
    static let LB_ERROR_NAME = "lb_ErrorName"
    static let LB_CONTROL_NAVIGATE = "lb_ControlNavigate"
    static let LB_CONTROL_RESEND = "lb_ControlResend"
    static let BTN_USER = "btn_User"
    
    
    //Team info
    static let LB_TEAMSIZE = "lb_TeamSize"
    static let BTN_SELECTTEAMSIZE = "btn_SelectTeamSize"
    static let LB_TEAMTYPE = "tf_TeamType"
    static let BTN_SELECTTEAMTYPE = "btn_SelectTeamType"
    static let TF_TEAMNAME = "tf_TeamName"
    static let BTN_PLANNING = "btn_Planning"
    static let BTN_CREATETEAM = "btn_CreateTeam"
    
    //Setting
    static let SWITCH_EMAIL = "switch_Email"
    static let SWITCH_NOTIFICATION = "switch_Notification"
    static let TBCELL_CHANGEPASS = "tbCell_ChangePass"
    static let TBCELL_NOTIFICATION = "tbCell_Notification"
    static let TBCELL_INVITE = "tbCell_Invite"
    static let TBCELL_ORANGNIZATION = "tbCell_Orangnization"
    static let TBCELL_LOGOUT = "tbCell_Logout"
    
    static let TBCELL_ITEM = "tbCell_Item"
    
    //Invite
    static let BTN_LAUNCH = "btn_Launch"
    static let BTN_INVITE = "btn_Invite"
    
    //User
    static let IMG_USERAVATAR = "img_UserAvatar"
    static let LB_USERTYPE = "lb_UserType"
    
    //Backlog
    static let BTN_SEARCH = "btn_Search"
    static let BTN_FILTER = "btn_Filter"
    static let BTN_ADD_TASK = "btn_AddNewTask"
    static let LIST_BACKLOG = "list_Backlog"
    static let TF_SEARCH = "tf_Search"
    static let SHEET_FILTER = "sheet_Filter"
    static let ACTION_SHEET_CANCEL = "actionsheet_Cancel"
    static let ACTION_SHEET_DESTRUCTIVE = "actionsheet_Destructive"
    static let ACTION_SHEET_OTHER = "actionsheet_Other"
    //Team Agenda
    static let BTN_BACKLOG = "btn_Backlog"
    static let BTN_NOTIFICATION = "btn_Notification"
    static let LB_NOTIFICATION = "lb_Notification"
    static let BTN_DISCUSSION = "btn_Discussion"
    static let BTN_FOCUS_MODE = "btn_FocusMode"
    static let BTN_PROFILE = "btn_Profile"
    static let IMG_PROFILE = "img_Profile"
    static let SWITCH_DATE = "switch_Date"
    static let BTN_TODAY = "btn_Today"
    static let BTN_YESTERDAY = "btn_Yesterday"
    static let LB_TITLE_CHART = "lb_TitleChart"
    static let CHART_TEAM_AGENDA = "chart_TeamAgenda"
    static let LIST_TEAM_AGENDA = "list_TeamAgenda"
    static let SCROLLVIEW_TEAM_AGENDA = "srv_TeamAgenda"
    
    //Team Orangnization
    static let SCROLLVIEW_TEAM_ORANGNIZATION = "srv_TeamOrangnization"
    static let BTN_MANAGE = "btn_Manage"
    static let BTN_TEAM_MANAGE = "btn_TeamManage"
    
    //Task Item
    static let TF_TASKNAME = "tf_TaskName"
    static let BTN_FINISH = "btn_Finish"
    static let BTN_PRIORITY = "btn_Priority"
    static let LB_TASKNAME = "lb_TaskName"
    static let IMG_ASSIGNEE = "img_Assignee"
    static let STATUS_CHECKED = "stt_Checked"
    static let STATUS_UNCHECK = "stt_UnCheck"
}
