//
//  String+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import UIKit

import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    func md5Value() -> String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        
        if let d = self.data(using: .utf8) {
            _ = d.withUnsafeBytes { body -> String in
                CC_MD5(body.baseAddress, CC_LONG(d.count), &digest)
                
                return ""
            }
        }
        
        return (0 ..< length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
    
    func subString(from: Int, to: Int) -> String {
        if(from > to || from < 0 || to < 0 || from > self.count - 1 || to > self.count - 1){return self}
        let startIndex = self.index(self.startIndex, offsetBy: from)
        let endIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[startIndex...endIndex])
    }
    
    func getTextSizeWithString(_ maxWidth: CGFloat, _ fontSize: UIFont?) -> CGSize {
        if (self.count == 0)
        {
            return CGSize(width: 0, height: 0)
        }
        
        return NSString(string: self).boundingRect(
            with: CGSize(width: maxWidth, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: fontSize!],
            context: nil).size
        
    }
    
    func isImageExtension() -> Bool{
        let strExtension = self.lowercased()
        if(strExtension.range(of: LocalImage.imageExtension.jpg.rawValue) != nil || strExtension.range(of: LocalImage.imageExtension.jpeg.rawValue) != nil || strExtension.range(of: LocalImage.imageExtension.png.rawValue) != nil || strExtension.range(of: LocalImage.imageExtension.ico.rawValue) != nil){
            return true
        }
        return false
    }
    
    func getIconForFile() -> String{
        var strIcon = ""
        //
        let pdfType = LocalImage.fileExtension.pdf.rawValue
        let docType = LocalImage.fileExtension.doc.rawValue
        let docxType = LocalImage.fileExtension.docx.rawValue
        let excelType = LocalImage.fileExtension.excel.rawValue
        let excelxType = LocalImage.fileExtension.excelx.rawValue
        let pptType = LocalImage.fileExtension.ppt.rawValue
        let pptxType = LocalImage.fileExtension.pptx.rawValue
        
        if(self.range(of: pdfType) != nil){
            strIcon = LocalImage.FileIcon.pdf
        }
        else if(self.range(of: docType) != nil || self.range(of: docxType) != nil){
            strIcon = LocalImage.FileIcon.doc
        }
        else if(self.range(of: excelType) != nil || self.range(of: excelxType) != nil){
            strIcon = LocalImage.FileIcon.excel
        }
        else if(self.range(of: pptType) != nil  || self.range(of: pptxType) != nil){
            strIcon = LocalImage.FileIcon.ppt
        }
        else{
            strIcon = LocalImage.FileIcon.txt
        }
        return strIcon;
    }
    
    func indices(of occurrence: String) -> [Int] {
        var indices = [Int]()
        var position = startIndex
        while let range = range(of: occurrence, options:.caseInsensitive, range: position..<endIndex, locale: nil) {
            let i = distance(from: startIndex,
                             to: range.lowerBound)
            indices.append(i)
            let offset = occurrence.distance(from: occurrence.startIndex,
                                             to: occurrence.endIndex) - 1
            guard let after = index(range.lowerBound,
                                    offsetBy: offset,
                                    limitedBy: endIndex) else {
                                        break
            }
            position = index(after: after)
        }
        return indices
    }
    
    func ranges(of searchString: String) -> [Range<String.Index>] {
        let _indices = indices(of: searchString)
        let count = searchString.count
        return _indices.map({ index(startIndex, offsetBy: $0)..<index(startIndex, offsetBy: $0+count) })
    }
    
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString)
    }
    
    func aa_localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    var aa_htmlToAttributed: NSAttributedString? {
        guard
            let data = self.data(using: .utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [
                NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
                ], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    var aa_noHtml: String {
        guard let data = self.data(using: .utf8) else { return self }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }
        
        return attributedString.string
    }
    
    var withoutHtmlTags: String {
      return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}

