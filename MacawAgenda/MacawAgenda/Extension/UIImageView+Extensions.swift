//
//  UIImageView+Extensions.swift
//  RxExample
//
//  Created by carlos on 28/5/15.
//  Copyright © 2015 Krunoslav Zaher. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func makeRoundedCorners(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    func makeRoundedCorners() {
        self.makeRoundedCorners(self.frame.size.height / 2)
    }
    
    func setDefaultAvatarIfNeed() {
        self.image = LocalImage.UserPlaceHolder
    }
    
    func loadAvatarForUser(user : User) {
        let strUserID = user.userId
        let strUserName = user.name
        //
        if(strUserID.count > 0){
            self.sd_imageIndicator = SDWebImageActivityIndicator.white
            //
            let strURL = Util.parseURLAvatarImageFireStore(strUserID)
            self.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongAvatar = self else { return }
                if let imageValue = image{
                    if(imageValue.size.width > 0){
                        strongAvatar.backgroundColor = .clear
                        strongAvatar.removeLabelTextIfNeed()
                    }
                    else{
                        strongAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
                else{
                    strongAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                }
            })
        }
        else{
            self.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
        }
    }
    
    func loadAvatarForOrangnization(orangnization : Orangnization) {
        let strName = orangnization.orangnizationName
        let strAvatar = orangnization.orangnizationAvatar
        //
        if(strAvatar.count > 0){
            self.sd_imageIndicator = SDWebImageActivityIndicator.white
            //
            let strURL = strAvatar
            self.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongAvatar = self else { return }
                if let imageValue = image{
                    if(imageValue.size.width > 0){
                        strongAvatar.backgroundColor = .clear
                        strongAvatar.removeLabelTextIfNeed()
                    }
                    else{
                        strongAvatar.setLabelTextForAvatar(image: nil, text: strName, identify: strName)
                    }
                }
                else{
                    strongAvatar.setLabelTextForAvatar(image: nil, text: strName, identify: strName)
                }
            })
        }
        else{
            self.setLabelTextForAvatar(image: nil, text: strName, identify: strName)
        }
    }
    
    static func clearCache(for link : String?){
        if let strongLink = link{
            SDImageCache.shared.removeImage(forKey: strongLink, fromDisk: true, withCompletion: nil)
        }
    }
    
    func removeLabelTextIfNeed(){
        if let labelText = self.viewWithTag(Constant.Tags.LabelNameImageView) as? UILabel{
            //Clear label
            labelText.removeFromSuperview()
        }
    }
    
    func setLabelTextForAvatar(image: UIImage?, text: String, identify: String){
        if(image != nil){
            removeLabelTextIfNeed()
            //Clear background
            self.backgroundColor = .clear
            self.image = image
        }
        else if(text.count > 0){
            self.image = nil
            self.backgroundColor = Util.getColorFromString(string: identify)
            //
            let defaultFont = self.frame.size.width > 15.0 ? UIFont(name: FontNames.Lato.MULI_REGULAR, size: self.frame.size.width / 2 - 5.0) : UIFont(name: FontNames.Lato.MULI_REGULAR, size: 5.0)
            var lbText : UILabel?
            if let labelText = self.viewWithTag(Constant.Tags.LabelNameImageView) as? UILabel{
                lbText = labelText
                lbText?.frame.size.width = self.frame.size.width
                lbText?.frame.size.height = self.frame.size.height
            }
            else{
                lbText = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
                lbText?.tag = Constant.Tags.LabelNameImageView
                lbText?.backgroundColor = .clear
                lbText?.font = defaultFont
                lbText?.textAlignment = .center
                lbText?.textColor = .white
                self.addSubview(lbText!)
            }
            lbText?.font = self.frame.size.width > 15.0 ? UIFont(name: FontNames.Lato.MULI_REGULAR, size: self.frame.size.width / 2 - 5.0) : UIFont(name: FontNames.Lato.MULI_REGULAR, size: 5.0)
            lbText?.text = Util.shortname2Char(name: text)
        }
        else{
            removeLabelTextIfNeed()
            //Clear background
            self.backgroundColor = .clear
            self.image = LocalImage.UserPlaceHolder
        }
    }
    
    func setLabelTextForAvatar_Large(image: UIImage?, text: String, identify: String){
        if(image != nil){
            removeLabelTextIfNeed()
            //Clear background
            self.backgroundColor = .clear
            self.image = image
        }
        else if(text.count > 0){
            self.image = nil
            self.backgroundColor = Util.getColorFromString(string: identify)
            //
            let defaultFont = UIFont(name: FontNames.Lato.MULI_BOLD, size: self.frame.size.width / 2)
            var lbText : UILabel?
            if let labelText = self.viewWithTag(Constant.Tags.LabelNameImageView) as? UILabel{
                lbText = labelText
                lbText?.frame.size.width = self.frame.size.width
                lbText?.frame.size.height = self.frame.size.height
            }
            else{
                lbText = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
                lbText?.tag = Constant.Tags.LabelNameImageView
                lbText?.backgroundColor = .clear
                lbText?.font = defaultFont
                lbText?.textAlignment = .center
                lbText?.textColor = .white
                self.addSubview(lbText!)
            }
            lbText?.font = defaultFont
            lbText?.text = Util.shortname1Char(name: text)
        }
        else{
            removeLabelTextIfNeed()
            //Clear background
            self.backgroundColor = .clear
            self.image = LocalImage.UserPlaceHolder
        }
    }
    
    func stateForAvatar(_ user : User){
        let strUserID = user.userId
        let strUserName = user.name
        //
        if(strUserID.count > 0){
            let strURL = Util.parseURLAvatarImageFireStore(strUserID)
            self.sd_setImage(with: URL(string: strURL), placeholderImage: LocalImage.UserPlaceHolder, completed:{[weak self] (image, error, cacheType, imageURL) in
                guard let strongAvatar = self else { return }
                if let imageValue = image{
                    if(imageValue.size.width > 0){
                        strongAvatar.backgroundColor = .clear
                        strongAvatar.removeLabelTextIfNeed()
                    }
                    else{
                        strongAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                    }
                }
                else{
                    strongAvatar.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
                }
            })
        }
        else{
            self.setLabelTextForAvatar(image: nil, text: strUserName, identify: strUserName)
        }
    }
}
