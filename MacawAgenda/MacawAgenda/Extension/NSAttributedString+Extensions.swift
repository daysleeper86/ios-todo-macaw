//
//  NSAttributedString+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

extension StringProtocol {
    func aa_nsRange(from range: Range<Index>) -> NSRange {
        return .init(range, in: self)
    }
}

extension NSAttributedString {
    convenience init(aa_withText text: String, keywords: String, color: UIColor) throws {
        let allString = NSMutableAttributedString(string: text)
        
        keywords.components(separatedBy: " ").forEach({ (word) in
            let string = NSMutableAttributedString(string: text)
            if let range = string.string.range(of: word, options: .caseInsensitive) {
                let nsRange = string.string.aa_nsRange(from: range)
                (string.string as NSString).substring(with: nsRange)
                allString.addAttributes([
                    NSAttributedString.Key.foregroundColor: color as Any
                    ], range: NSRange(location: nsRange.location, length: word.count))
            }
        })
        self.init(attributedString: allString)
    }
}
