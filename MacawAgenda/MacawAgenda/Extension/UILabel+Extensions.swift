//
//  UILabel+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

extension UILabel {
    func aa_getHeight(for string: String) -> CGFloat {
        let textStorage = NSTextStorage(string: string)
        let textContainter = NSTextContainer(size: CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainter)
        textStorage.addLayoutManager(layoutManager)
        textStorage.addAttribute(NSAttributedString.Key.font, value: font as Any, range: NSMakeRange(0, textStorage.length))
        textContainter.lineFragmentPadding = 0.0
        layoutManager.glyphRange(for: textContainter)
        return layoutManager.usedRect(for: textContainter).size.height
    }
    
    func aa_changeColor(of searchString: String, with color: UIColor) {
        do { attributedText = try NSAttributedString(aa_withText: self.text ?? "", keywords: searchString, color: color) }
        catch {
        }
    }
}
