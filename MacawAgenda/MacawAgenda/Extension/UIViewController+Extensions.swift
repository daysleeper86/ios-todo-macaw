//
//  UIViewController+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

extension UIViewController {
    var aa_topViewController: UIViewController {
        switch self {
        case is UINavigationController:
            return (self as! UINavigationController).visibleViewController?.aa_topViewController ?? self
        case is UITabBarController:
            return (self as! UITabBarController).selectedViewController?.aa_topViewController ?? self
        default:
            return presentedViewController?.aa_topViewController ?? self
        }
    }
    
    class func aa_replaceRootViewController (
        to viewController: UIViewController,
        animated: Bool = true,
        duration: TimeInterval = 0.5,
        options: UIView.AnimationOptions = .transitionFlipFromRight,
        _ completion: AACompletionVoid? = nil) {
        
        let keyWindow = UIApplication.shared.keyWindow!
        
        guard animated else {
            keyWindow.rootViewController = viewController
            completion?()
            return
        }
        
        UIView.transition(with: keyWindow, duration: duration, options: options, animations: {
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            keyWindow.rootViewController = viewController
            UIView.setAnimationsEnabled(oldState)
        }, completion: { _ in
            completion?()
        })
    }
}
