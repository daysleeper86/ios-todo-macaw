//
//  UIView+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

extension UIView {
    func aa_fadeIn(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }

    func aa_fadeOut(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    func aa_startGlowing(color: UIColor, radius: CGFloat = 1) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = radius

        let animation = CABasicAnimation(keyPath: #keyPath(CALayer.shadowOpacity))
        animation.duration = 1
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = .greatestFiniteMagnitude
        self.layer.add(animation, forKey: "animateOpacity")

    }
    
    func aa_addTopBorderWithColor(color: UIColor, width: CGFloat, paddingX: CGFloat, paddingY: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: paddingX, y: paddingY, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func aa_addRightBorderWithColor(color: UIColor, width: CGFloat, paddingX: CGFloat, paddingY: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: (self.frame.size.width - width) + paddingX, y: paddingY, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func aa_addBottomBorderWithColor(color: UIColor, width: CGFloat, paddingX: CGFloat, paddingY: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: paddingX, y: (self.frame.size.height - width) + paddingY, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func aa_addLeftBorderWithColor(color: UIColor, width: CGFloat, paddingX: CGFloat, paddingY: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: paddingX, y: paddingY, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func aa_addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat) {
        self.layoutIfNeeded()
        let strokeColor = strokeColor.cgColor

        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round

        shapeLayer.lineDashPattern = [5,5] // adjust to your liking
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: shapeRect.width, height: shapeRect.height), cornerRadius: self.layer.cornerRadius).cgPath

        self.layer.addSublayer(shapeLayer)
    }
    
    func aa_addTapGesture(_ target: Any, action: Selector) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    func aa_roundCorners(topLeft : Bool, topRight : Bool, bottomLeft : Bool, bottomRight: Bool, strokeColor : UIColor?, lineWidth : CGFloat, radius : CGFloat) -> Void {
        
        if #available(iOS 11.0, *) {
            var corner  = CACornerMask()
            if topLeft {
                corner = corner.union(.layerMinXMinYCorner)
            }
            if topRight {
                corner = corner.union(.layerMaxXMinYCorner)
            }
            if bottomLeft {
                corner = corner.union(.layerMinXMaxYCorner)
            }
            if bottomRight {
                corner = corner.union(.layerMaxXMaxYCorner)
            }
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = corner
        }
        else{
            var corner  = UIRectCorner()
            if topLeft {
                corner = corner.union(.topLeft)
            }
            if topRight {
                corner = corner.union(.topRight)
            }
            if bottomLeft {
                corner = corner.union(.bottomLeft)
            }
            if bottomRight {
                corner = corner.union(.bottomRight)
            }
            
            let maskPath1 = UIBezierPath(roundedRect: bounds,
                                         byRoundingCorners:corner,
                                         cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = bounds
            maskLayer1.path = maskPath1.cgPath
            layer.mask = maskLayer1
        }
    }
    
    var aa_screenshot: UIImage? {
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, 0)
        defer {
            UIGraphicsEndImageContext()
        }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
