//
//  NSDate+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/17/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import UIKit

enum DateRepresentation {
    case second
    case minute
    case hour
    case day
    case week
    case month
    case year
}

extension Date {
    static var dateFormatter: DateFormatter {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }
    
    static func from(year: Int, month: Int, day: Int) -> Date? {
//        let calendar = Calendar(identifier: .gregorian)
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        return calendar.date(from: dateComponents) ?? nil
    }
    
    var yesterday: Date {
        return self.dayBefore
    }
    
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    static var UTCOffset : CGFloat{
        let date = Date()
        let deviceTimeZone = NSTimeZone.system
        let offset : CGFloat = CGFloat(deviceTimeZone.secondsFromGMT(for: date)) / CGFloat(3600.0)
        return offset
    }

    func dayString() -> String {
        var strDay : String = ""
        
        let day = self.day()
        if(day < 10){
            strDay = "0" + String(day)
        }
        else{
            strDay = String(day)
        }
        return strDay
    }
    
    func day() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let day = calendar.component(.day, from: self)
        return day
    }
    
    func hour() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let hour = calendar.component(.hour, from: self)
        return hour
    }
    
    func minute() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let minute = calendar.component(.minute, from: self)
        return minute
    }
    
    func monthString() -> String {
        var strMonth : String = ""
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let month = calendar.component(.month, from: self)
        switch month {
        case 1:
            strMonth = NSLocalizedString("last_mess_january", comment: "")
            break
        case 2:
            strMonth = NSLocalizedString("last_mess_febrary", comment: "")
            break
        case 3:
            strMonth = NSLocalizedString("last_mess_march", comment: "")
            break
        case 4:
            strMonth = NSLocalizedString("last_mess_april", comment: "")
            break
        case 5:
            strMonth = NSLocalizedString("last_mess_may", comment: "")
            break
        case 6:
            strMonth = NSLocalizedString("last_mess_jun", comment: "")
            break
        case 7:
            strMonth = NSLocalizedString("last_mess_july", comment: "")
            break
        case 8:
            strMonth = NSLocalizedString("last_mess_august", comment: "")
            break
        case 9:
            strMonth = NSLocalizedString("last_mess_september", comment: "")
            break
        case 10:
            strMonth = NSLocalizedString("last_mess_october", comment: "")
            break
        case 11:
            strMonth = NSLocalizedString("last_mess_november", comment: "")
            break
        case 12:
            strMonth = NSLocalizedString("last_mess_December", comment: "")
            break
        default:
            strMonth = ""
        }
        return strMonth
    }
    
    func dayMonthYearString() -> String {
        var strDate : String = ""
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let year = calendar.component(.year, from: self)
        
        let strMonth = self.monthString()
        let strDay = self.dayString()
        
        strDate = strMonth + " " + strDay + ", " + String(year)
        return strDate
    }
    
    func since(_ anotherTime: Date,
               in representation: DateRepresentation) -> Double {
        switch representation {
        case .second:   return -timeIntervalSince(anotherTime)
        case .minute:   return -timeIntervalSince(anotherTime) / 60
        case .hour:     return -timeIntervalSince(anotherTime) / (60 * 60)
        case .day:      return -timeIntervalSince(anotherTime) / (24 * 60 * 60)
        case .week:     return -timeIntervalSince(anotherTime) / (7 * 24 * 60 * 60)
        case .month:    return -timeIntervalSince(anotherTime) / (30 * 24 * 60 * 60)
        case .year:     return -timeIntervalSince(anotherTime) / (365 * 24 * 60 * 60)
        }
    }
    
    func hoursBetweenDateAndNow(nowDate : Date) -> Int {
        let distanceBetweenDates: TimeInterval? = nowDate.timeIntervalSince(self)
        let secondsInAnHour: Double = 3600
        let daysBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
        return daysBetweenDates
    }
    
    func daysBetweenDateAndNow(nowDate : Date) -> Int {
        let distanceBetweenDates: TimeInterval? = nowDate.timeIntervalSince(self)
        let secondsInDays: Double = 86400
        let hoursBetweenDates = Int((distanceBetweenDates! / secondsInDays))
        return hoursBetweenDates
    }
    
    func currentTimeInMiliseconds() -> Int {
        let since1970 = self.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    func dateToMiliseconds() -> Int {
        let since1970 = self.timeIntervalSince1970
        return Int(since1970 / 1000)
    }
    
    func formatInt() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let day = calendar.component(.day, from: self)
        let month = calendar.component(.month, from: self)
        let year = calendar.component(.year, from: self)
        
        var strDay : String = ""
        if(day < 10){
            strDay = "0" + String(day)
        }
        else{
            strDay = String(day)
        }
        
        var strMonth : String = ""
        if(month < 10){
            strMonth = "0" + String(month)
        }
        else{
            strMonth = String(month)
        }
        
        let strDate = "\(year)\(strMonth)\(strDay)"
        return Int(strDate) ?? 0
    }
    
    
    
    func isSameDay(_ date : Date) -> Bool{
        return self.dayMonthYearString() == date.dayMonthYearString()
    }
    
    func isToday() -> Bool{
        return self.isSameDay(Date())
    }
    
    func isYesterday() -> Bool{
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents([.era, .year , .month , .day], from: Date())
        let today = calendar.date(from: components)
        let tmpDate = today?.addingTimeInterval(-86400.0)
        
        let otherComponents = calendar.dateComponents([.era, .year , .month , .day], from: self)
        let otherDate = calendar.date(from: otherComponents)
        return tmpDate?.isSameDay(otherDate ?? Date()) ?? false
        
    }
    
    func isWeek() -> Bool{
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents([.era, .year , .month , .day], from: Date())
        let today = calendar.date(from: components)
        let thisWeek = today?.addingTimeInterval(-604800.0)
        
        let components1 = calendar.dateComponents([.era, .year , .month , .day], from: self)
        let otherDate = calendar.date(from: components1)
        
        if(otherDate?.compare(today ?? Date()) == .orderedAscending){
            if(otherDate?.compare(thisWeek ?? Date()) == .orderedDescending){
                return true
            }
        }
    
        return false
    }
    
    
    func getTime(format : String) -> String{
        let dateFormatter = Date.dateFormatter
        dateFormatter.dateFormat = format
        let formattedDateString = dateFormatter.string(from: self)
        return formattedDateString
    }
    
    func timeStampHourMinute() -> String{
        let nightorday = self.getTime(format: "a")
        let hourminute  = self.getTime(format: "HH:mm")
        let timelabel = hourminute + " " + nightorday
        return timelabel
    }

    
    func dayWeek(isFullText : Bool) -> String{
        var strDay : String = ""
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let otherComponents = calendar.dateComponents([.weekday], from: self)
        let weekdayNumber = otherComponents.weekday
        
        switch weekdayNumber {
        case 1:
            strDay = NSLocalizedString("last_mess_sunday", comment: "")
            break
        case 2:
            strDay = NSLocalizedString("last_mess_monday", comment: "")
            break
        case 3:
            strDay = NSLocalizedString("last_mess_tuesday", comment: "")
            break
        case 4:
            strDay = NSLocalizedString("last_mess_wednesday", comment: "")
            break
        case 5:
            strDay = NSLocalizedString("last_mess_thursday", comment: "")
            break
        case 6:
            strDay = NSLocalizedString("last_mess_friday", comment: "")
            break
        case 7:
            strDay = NSLocalizedString("last_mess_saturday", comment: "")
            break
        default:
            strDay = ""
        }
        
        return strDay
    }
    
    
    func timeStampForChattingHeader(showMinute: Bool) -> String{
        var timelabel : String = ""
        if(self.isToday()){
            let currentDate = Date()
            let timeOffset = currentDate.offsetLong(from: self) // "9M"
            timelabel = timeOffset
//            print("Time ne" + timeOffset)
//
//            let distanceBetweenDates : TimeInterval  = Double(currentDate.dateToMiliseconds()) - Double(self.dateToMiliseconds())
//            let seconds : Double = 3600
//            let hoursBetweenDates : Double = distanceBetweenDates / seconds
//
//            if (hoursBetweenDates >= 1) {
//                if ((1 <= hoursBetweenDates && hoursBetweenDates <= 1.5) || hoursBetweenDates == 0) {
//                    timelabel = NSLocalizedString("an_hour_ago", comment: "")
//                }
//                else if (hoursBetweenDates <= 12) {
//                    timelabel = String(hoursBetweenDates) + " " + NSLocalizedString("hours_ago", comment: "")
//                }
//            }
//            else {
//                let seconds : Double = 60
//                let hoursBetweenDates : Double = distanceBetweenDates / seconds
//                if (hoursBetweenDates > 0) {
//                    if (hoursBetweenDates > 1) {
//                         timelabel = String(hoursBetweenDates) + " " + NSLocalizedString("minutes_ago", comment: "")
//                    }
//                    else {
//                        timelabel = NSLocalizedString("a_minute_ago", comment: "")
//                    }
//                }
//                else { //neu nho hon 1 phut
//                    let secondBetweenDates : Double = distanceBetweenDates
//                    if (secondBetweenDates + 1 > 1) {
//                        timelabel = String(hoursBetweenDates + 1) + " " + NSLocalizedString("seconds_ago", comment: "")
//                    }
//                    else {
////                        timelabel = String(hoursBetweenDates + 1) + " " + NSLocalizedString("second_ago", comment: "")
//                        timelabel = NSLocalizedString("second_ago", comment: "")
//                    }
//                }
//            }
//            timelabel = NSLocalizedString("history_view_today_time",comment:"")
        }
        else{
            if (self.isYesterday()){
                timelabel = NSLocalizedString("history_view_yesterday_time",comment:"")
            }
            else if (self.isWeek()) {
                timelabel = self.dayWeek(isFullText:true)
            }
            else{
                timelabel = self.dayMonthYearString()
            }
            //
            if(showMinute == true){
                let strMinute = self.timeStampHourMinute()
                timelabel = timelabel + " " + strMinute
            }
            
        }
       
        
        return timelabel
    }
    
    func toTimeStampFireStore() -> String{
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents([.era, .year , .month , .day, .hour, .minute], from: self)
        if let date = calendar.date(from: components) {
            return (DateFormatter.localizedString(
                from: date,
                dateStyle: .medium,
                timeStyle: .medium))
        }
        return ""
    }
    
    
    func dateForUTC(utcOffset: Int) -> Date{
        // 1) Get the current TimeZone's seconds from GMT. Since I am in Chicago this will be: 60*60*5 (18000)
        //        let timezoneOffset =  TimeZone.current.secondsFromGMT()
        //        let timezoneOffset =  TimeZone.current.secondsFro GMT(60*60*7)
        if let timezoneOffset = TimeZone(secondsFromGMT: 60*60*utcOffset)?.secondsFromGMT(){
            // 2) Get the current date (GMT) in seconds since 1970. Epoch datetime.
            let epochDate = self.timeIntervalSince1970
            // 3) Perform a calculation with timezoneOffset + epochDate to get the total seconds for the
            //    local date since 1970.
            //    This may look a bit strange, but since timezoneOffset is given as -18000.0, adding epochDate and timezoneOffset
            //    calculates correctly.
            let timezoneEpochOffset = (epochDate + Double(timezoneOffset))
            // 4) Finally, create a date using the seconds offset since 1970 for the local date.
            let timeZoneOffsetDate = Date(timeIntervalSince1970: timezoneEpochOffset)
            //
            return timeZoneOffsetDate
        }
        return Date()
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    func offsetLong(from date: Date) -> String {
        if years(from: date)   > 0 { return years(from: date) > 1 ? "\(years(from: date)) years ago" : "\(years(from: date)) year ago" }
        if months(from: date)  > 0 { return months(from: date) > 1 ? "\(months(from: date)) months ago" : "\(months(from: date)) month ago" }
        if weeks(from: date)   > 0 { return weeks(from: date) > 1 ? "\(weeks(from: date)) weeks ago" : "\(weeks(from: date)) week ago"   }
        if days(from: date)    > 0 { return days(from: date) > 1 ? "\(days(from: date)) days ago" : "\(days(from: date)) day ago" }
        if hours(from: date)   > 0 { return hours(from: date) > 1 ? "\(hours(from: date)) hours ago" : "\(hours(from: date)) hour ago"   }
        if minutes(from: date) > 0 { return minutes(from: date) > 1 ? "\(minutes(from: date)) minutes ago" : "\(minutes(from: date)) minute ago" }
        if seconds(from: date) > 0 { return seconds(from: date) > 1 ? "\(seconds(from: date)) seconds ago" : "\(seconds(from: date)) second ago" }
        return ""
    }
}

