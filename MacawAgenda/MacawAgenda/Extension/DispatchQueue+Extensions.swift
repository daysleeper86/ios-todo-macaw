//
//  DispatchQueue+Extensions.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

extension DispatchQueue {
    
    class func aa_performBackground(delay: Double = 0.0,
                              background: AACompletionVoid? = nil,
                              completion: AACompletionVoid? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}
