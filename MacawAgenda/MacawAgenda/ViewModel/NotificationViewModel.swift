//
//  NotificationViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class NotificationViewModel {
    // Inputs
    let notificationController: NotificationController
    
    //Results
    var notifications: Observable<[NotificationData]>?
    var counter: Observable<Counter>?
    static let emptyNotification = NotificationService.createEmptyNotificationData()
    
    init(input: (
        projectId: String,
        userId: Observable<String>,
        notificationController: NotificationController
        )) {
        self.notificationController = input.notificationController
        //
        let notificationHolder = NotificationService.createHolderAgenda(numHolder: 5)
        self.notifications = input.userId
            .flatMapLatest { userId -> Observable<[NotificationData]> in
                return self.notificationController.subscribleNotificationByUserId(projectId: input.projectId, userId: userId)
                    .retry(3)
                    .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                    .startWith(notificationHolder) // clears results on new search term
                    .catchErrorJustReturn([NotificationViewModel.emptyNotification])
        }
        .share(replay: 1)
    }
    
    init(inputGetCountNotification: (
        projectId: Observable<String>,
        userId: Observable<String>,
        notificationController: NotificationController
        )) {
        self.notificationController = inputGetCountNotification.notificationController
        //
        let projectAndUser =
            Observable
                .combineLatest(inputGetCountNotification.projectId, inputGetCountNotification.userId) { (projectId: $0, userId: $1) }
                .distinctUntilChanged { (old: (projectId: String, userId: String),
                    new: (projectId: String, userId: String)) in
                    return old.projectId == new.projectId && old.userId == new.userId
        }
        //
        let counterEmpty = Counter(count: 0, dataType: .empty)
        self.counter = projectAndUser
            .flatMapLatest { projectId,userId -> Observable<Counter> in
                if(projectId.count > 0 && userId.count > 0){
                    return self.notificationController.getNotificationNumberByUserId(projectId: projectId, userId: userId)
                        .retry(3)
                        .retryOnBecomesReachable(counterEmpty, reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                        .catchErrorJustReturn(counterEmpty)
                }
                else{
                    return Observable.just(counterEmpty)
                }
        }
    }
    
    init(inputSubscribleCountNotification: (
        projectId: String,
        userId: Observable<String>,
        notificationController: NotificationController
        )) {
        self.notificationController = inputSubscribleCountNotification.notificationController
        //
        let counterEmpty = Counter(count: 0, dataType: .empty)
        self.counter = inputSubscribleCountNotification.userId
            .flatMapLatest { userId -> Observable<Counter> in
                return self.notificationController.subscribleNotificationNumberByUserId(projectId: inputSubscribleCountNotification.projectId, userId: userId)
                    .retry(3)
                    .retryOnBecomesReachable(counterEmpty, reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                    .catchErrorJustReturn(counterEmpty)
        }
    }
    
}
