//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class FileAPIViewModel {
    // Inputs
    var fileAPIController: FileAPIController?
    
    // Results
    var processingIn: Observable<Bool>?
    var processFinised: Observable<LoginResult>?
    
    init(inputSendFileUpload: (
        projectId: String,
        taskId: Observable<String>,
        fileId: String,
        dictFile: [String:Any],
        loginTaps: Observable<Void>,
        fileAPIController: FileAPIController
        )) {
        self.fileAPIController = inputSendFileUpload.fileAPIController
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinised = inputSendFileUpload.loginTaps
            .withLatestFrom(inputSendFileUpload.taskId)
            .flatMapLatest { taskId in
                return inputSendFileUpload.fileAPIController.sendFileUpload(projectId: inputSendFileUpload.projectId,taskId: taskId, fileId: inputSendFileUpload.fileId, dictFile: inputSendFileUpload.dictFile, tracker: processingIn)
        }
    }
}
