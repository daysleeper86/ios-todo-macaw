//
//  ProjectViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import RxSwiftUtilities

class ProjectViewModel {
    // Inputs
    var projectController: ProjectController?
    
    // Results
    var projects: Observable<[Project]>?
    var users: Observable<[User]>?
    static let emptyProject = ProjectService.createEmptyProject()
    
    init(inputNeedProjects: (
        projectId: Observable<String>?,
        projectController: ProjectController,
        needSubscrible: Bool
        )) {
        self.projectController = inputNeedProjects.projectController
        //
        if(inputNeedProjects.projectId != nil){
            self.projects = inputNeedProjects.projectId!
                .flatMapLatest {[weak self] projectId -> Observable<[Project]> in
                    if let strongSelf = self, let strongProjectControlelr = strongSelf.projectController {
                        if(inputNeedProjects.needSubscrible == true){
                            return strongProjectControlelr.subscribleProjectById(projectId: projectId)
                                .retry(3)
                                .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                                .startWith([]) // clears results on new search term
                                .catchErrorJustReturn([])
                        }
                        else{
                            return strongProjectControlelr.getProjectById(projectId: projectId)
                                .retry(3)
                                .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                                .startWith([]) // clears results on new search term
                                .catchErrorJustReturn([])
                        }
                    }
                    else{
                        return Observable.just([])
                    }
            }
            .share(replay: 1)
        }
    }
    
    init(inputNeedUsers: (
        projectId: Observable<String>?,
        projectController: ProjectController
        )) {
        self.projectController = inputNeedUsers.projectController
        //
        if(inputNeedUsers.projectId != nil){
            let userHolder = UserService.createHolderUser(numHolder: 5)
            self.users = inputNeedUsers.projectId!
                .flatMapLatest {[weak self] projectId -> Observable<[User]> in
                    if let strongSelf = self, let strongProjectControlelr = strongSelf.projectController {
                        return strongProjectControlelr.subscribleUsersByProjectId(projectId: projectId)
                            .retry(3)
                            .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith(userHolder) // clears results on new search term
                            .catchErrorJustReturn([])
                    }
                    else{
                        return Observable.just([])
                    }
                }
                .share(replay: 1)
        }
    }
    
    init(inputUserNeedProjects: (
        userId: Observable<String>?,
        orangnizationId: String,
        projectController: ProjectController,
        dataSource : Observable <[Project]>
        )) {
        self.projectController = inputUserNeedProjects.projectController
        //
        if(inputUserNeedProjects.userId != nil){
            let projectHolder = ProjectService.createHolderProject(numHolder: 3)
            let dataDest = inputUserNeedProjects.userId!
                .flatMapLatest {[weak self] userId -> Observable<[Project]> in
                    if let strongSelf = self, let strongProjectControlelr = strongSelf.projectController {
                        return strongProjectControlelr.subscribleProjectByUserId(userId: userId,orangnizationId: inputUserNeedProjects.orangnizationId)
                            .retry(3)
                            .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith(projectHolder) // clears results on new search term
                            .catchErrorJustReturn([])
                    }
                    else{
                        return Observable.just([])
                    }
                }
            
            self.projects = Observable.combineLatest(inputUserNeedProjects.dataSource, dataDest) { (dataSource: $0, dataDest: $1) }
                .distinctUntilChanged { (old: (dataSource: [Project], dataDest: [Project]),
                    new: (dataSource: [Project], dataDest: [Project])) in
                    return (ProjectService.checkHolderProject(projects: old.dataDest) == true
                        && ProjectService.checkHolderProject(projects: new.dataDest) ==  true)
                }
                .flatMapLatest { dataSource, dataDest -> Observable<[Project]> in
                    if(dataSource.count == 0){
                        return Observable.just(dataDest)
                    }
                    else{
                        var resultSource : [Project]
                        if(ProjectService.checkEmptyProject(projects: dataDest) == false){
                            resultSource = dataSource + dataDest
                        }
                        else{
                            resultSource = dataSource
                        }
                        return Observable.just(resultSource)
                    }
                }.share(replay: 1)
        }
    }
}
