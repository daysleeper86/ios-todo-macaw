//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class TaskAPIViewModel {
    // Inputs
    var taskAPIController: TaskAPIController?
    
    // Results
    var validatedName: Observable<ValidationResult>?
    var processingIn: Observable<Bool>?
    var processEnabled: Observable<Bool>?
    var processFinished: Observable<LoginResult>?
    
    init(inputCreateTask: (
        projectId: String,
        name: Observable<String>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputCreateTask.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        validatedName = inputCreateTask.name
            .map { name in
                return Util.validateTaskName(name)
        }
        
        //
        processEnabled = Observable.combineLatest(
            validatedName!,
            processingIn.asObservable())
        { name, signingIn in
            name.isValid &&
                !signingIn
            }
            .distinctUntilChanged()
        .share(replay: 1)
        
//        let validatedRequest =
//            Observable
//                .combineLatest(inputCreateTask.name, processEnabled!) { (name: $0, enable: $1) }
//                .distinctUntilChanged { (old: (name: String, enable: Bool),
//                    new: (name: String, enable: Bool)) in
//                    return old.name == new.name
//        }
        
        //
        processFinished = inputCreateTask.loginTaps
            .withLatestFrom(inputCreateTask.name)
            .flatMapLatest { name -> Observable<LoginResult> in
                if(name.count > 0){
                    return inputCreateTask.controller.createTask(projectId: inputCreateTask.projectId, name: name,tracker: processingIn)
                }
                else{
                    return Observable.just(LoginResult.ok(output: nil))
                }
        }
    }

    init(inputCreateTaskAndUpdateAgenda: (
        projectId: String,
        agendaId: String,
        name: Observable<String>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputCreateTaskAndUpdateAgenda.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        validatedName = inputCreateTaskAndUpdateAgenda.name
            .map { name in
                return Util.validateTaskName(name)
        }
        
        //
        processEnabled = Observable.combineLatest(
            validatedName!,
            processingIn.asObservable())
        { name, signingIn in
            name.isValid &&
                !signingIn
            }
            .distinctUntilChanged()
            .share(replay: 1)
        
        processFinished = inputCreateTaskAndUpdateAgenda.loginTaps
            .withLatestFrom(inputCreateTaskAndUpdateAgenda.name)
            .flatMapLatest { name -> Observable<LoginResult> in
                if(name.count > 0){
                    return inputCreateTaskAndUpdateAgenda.controller.createTaskAndUpdateAgenda(projectId: inputCreateTaskAndUpdateAgenda.projectId, name: name, agendaId: inputCreateTaskAndUpdateAgenda.agendaId, tracker: processingIn)
                }
                else{
                    return Observable.just(LoginResult.ok(output: nil))
                }
        }
    }
    

    
    
    init(inputUpdateTaskDeleted: (
        taskId: String,
        projectId: String,
        deleted: Observable<Bool>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskDeleted.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskDeleted.loginTaps
            .withLatestFrom(inputUpdateTaskDeleted.deleted)
            .flatMapLatest { deleted in
                return inputUpdateTaskDeleted.controller.updateTaskDeleted(taskId: inputUpdateTaskDeleted.taskId,projectId: inputUpdateTaskDeleted.projectId, deleted: deleted,tracker: processingIn)
        }
    }
    
    init(inputUpdateTaskAgendaStatus: (
        taskId: String,
        projectId: String,
        agendaId: String,
        status: Observable<Int>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskAgendaStatus.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskAgendaStatus.loginTaps
            .withLatestFrom(inputUpdateTaskAgendaStatus.status)
            .flatMapLatest { status in
                return inputUpdateTaskAgendaStatus.controller.updateTaskAgendaStatus(taskId: inputUpdateTaskAgendaStatus.taskId,projectId: inputUpdateTaskAgendaStatus.projectId, agendaId: inputUpdateTaskAgendaStatus.agendaId, status: status,tracker: processingIn)
        }
    }
    
    init(inputUpdateTaskFinished: (
        taskId: String,
        projectId: String,
        finished: Observable<Bool>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskFinished.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskFinished.loginTaps
            .withLatestFrom(inputUpdateTaskFinished.finished)
            .flatMapLatest { finished in
                return inputUpdateTaskFinished.controller.updateTaskFinished(taskId: inputUpdateTaskFinished.taskId,projectId: inputUpdateTaskFinished.projectId, finished: finished,tracker: processingIn)
            }
    }
    
    init(inputUpdateTaskPriority: (
        taskId: String,
        projectId: String,
        priority: Observable<Bool>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskPriority.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskPriority.loginTaps
            .withLatestFrom(inputUpdateTaskPriority.priority)
            .flatMapLatest { priority in
                return inputUpdateTaskPriority.controller.updateTaskPriority(taskId: inputUpdateTaskPriority.taskId, projectId: inputUpdateTaskPriority.projectId, priority: priority, tracker: processingIn)
            }
    }
    
    init(inputUpdateTaskName: (
        taskId: String,
        projectId: String,
        name: Observable<String>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskName.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        validatedName = inputUpdateTaskName.name
            .map { name in
                return Util.validateTaskName(name)
        }
        //
        processFinished = inputUpdateTaskName.loginTaps
            .withLatestFrom(inputUpdateTaskName.name)
            .flatMapLatest { name in
                return inputUpdateTaskName.controller.updateTaskName(taskId: inputUpdateTaskName.taskId, projectId: inputUpdateTaskName.projectId, name: name, tracker: processingIn)
        }
    }
    
    init(inputUpdateTaskDuedate: (
        taskId: String,
        projectId: String,
        duedate: Observable<Date>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskDuedate.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskDuedate.loginTaps
            .withLatestFrom(inputUpdateTaskDuedate.duedate)
            .flatMapLatest { duedate in
                return inputUpdateTaskDuedate.controller.updateTaskDuedate(taskId: inputUpdateTaskDuedate.taskId, projectId: inputUpdateTaskDuedate.projectId, duedate: duedate, tracker: processingIn)
        }
    }
    
    init(inputUpdateTaskAssignee: (
        taskId: String,
        projectId: String,
        assignee: Observable<String>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskAssignee.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskAssignee.loginTaps
            .withLatestFrom(inputUpdateTaskAssignee.assignee)
            .flatMapLatest { assignee in
                return inputUpdateTaskAssignee.controller.updateTaskAssignee(taskId: inputUpdateTaskAssignee.taskId, projectId: inputUpdateTaskAssignee.projectId, assignee: assignee, tracker: processingIn)
        }
    }
    
    init(inputUpdateTaskDescription: (
        taskId: String,
        projectId: String,
        description: Observable<String>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskDescription.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputUpdateTaskDescription.loginTaps
            .withLatestFrom(inputUpdateTaskDescription.description)
            .flatMapLatest { description in
                return inputUpdateTaskDescription.controller.updateTaskDescription(taskId: inputUpdateTaskDescription.taskId, projectId: inputUpdateTaskDescription.projectId, description: description, tracker: processingIn)
        }
    }
    
    init(inputUpdateTaskFollower: (
        taskId: String,
        projectId: String,
        follower: Observable<String>,
        addOrRemove: Observable<Int>,
        loginTaps: Observable<Void>,
        controller: TaskAPIController
        )) {
        self.taskAPIController = inputUpdateTaskFollower.controller
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        let followerAndAction = Observable.combineLatest(inputUpdateTaskFollower.follower, inputUpdateTaskFollower.addOrRemove) { (follower: $0, addOrRemove: $1) }
        
        processFinished = inputUpdateTaskFollower.loginTaps
            .withLatestFrom(followerAndAction)
            .flatMapLatest { followerAndAction in
                return inputUpdateTaskFollower.controller.updateTaskFollower(taskId: inputUpdateTaskFollower.taskId, projectId: inputUpdateTaskFollower.projectId, followerId:followerAndAction.follower,addOrRemove:followerAndAction.addOrRemove, tracker: processingIn)
        }
    }
    
}
