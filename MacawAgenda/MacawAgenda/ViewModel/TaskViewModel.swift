//
//  AccountViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class TaskViewModel {
    
    // Inputs
    let taskController: FirestoreTaskAPI
    
    // Results
    var tasks: Observable<[Task]>?
    var otherTasks: Observable<[Task]>?
    static let emptyTask = TaskService.createEmptyTask()
    
    init(inputSearchAndSort: (
        search: Observable<String>?,
        sort: Observable<Int>?,
        taskController: FirestoreTaskAPI,
        dataSource : Observable <[Task]>,
        projectId: String,
        userId: String,
        duedate: Date?
        )) {
        self.taskController = inputSearchAndSort.taskController
        
        let validateSearch : Observable<String> =
            inputSearchAndSort.search!
                .map{search in
                    return search.count > 1 ? search : ""
        }
        
        let searchAndSort =
            Observable
                .combineLatest(validateSearch, inputSearchAndSort.sort!) { (search: $0, sort: $1) }
                .distinctUntilChanged { (old: (search: String, sort: Int),
                    new: (search: String, sort: Int)) in
                    return old.search == new.search && old.sort == new.sort
        }
        
        let dataDest = searchAndSort
            .flatMapLatest { search, sort -> Observable<[Task]> in
                let taskHolder = TaskService.createHolderTask(numHolder: 5)
                return self.taskController.subscribleAllTasks(projectId: inputSearchAndSort.projectId,userId:inputSearchAndSort.userId,search:search,sort:sort,duedate: inputSearchAndSort.duedate)
                    .retry(3)
                    .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                    .startWith(taskHolder) // clears results on new search term
                    .catchErrorJustReturn([TaskViewModel.emptyTask])
        }
        
        self.tasks =
            Observable.combineLatest(inputSearchAndSort.dataSource, dataDest) { (dataSource: $0, dataDest: $1) }
                .distinctUntilChanged { (old: (dataSource: [Task], dataDest: [Task]),
                    new: (dataSource: [Task], dataDest: [Task])) in
                    return (TaskService.checkHolderTask(tasks: old.dataDest) == true
                        && TaskService.checkHolderTask(tasks: new.dataDest) ==  true)
                }
                .flatMapLatest { dataSource, dataDest -> Observable<[Task]> in
                    if(dataSource.count == 0){
                        return Observable.just(dataDest)
                    }
                    else{
                        var resultSource : [Task]
                        if(TaskService.checkEmptyTask(tasks: dataDest) == false){
                            resultSource = dataSource + dataDest
                        }
                        else{
                            resultSource = dataSource
                        }
                        return Observable.just(resultSource)
                    }
                }.share(replay: 1)
    }
    
    
    init(inputTaskAgenda: (
        taskInfo: Observable<[String : Any]>,
        taskController: FirestoreTaskAPI,
        dataSource : Observable <[Task]>,
        projectId: String,
        findOther : Bool,
        needEmptyData: Bool
        )) {
        
        self.taskController = inputTaskAgenda.taskController
        
        let taskHolder = TaskService.createHolderTask(numHolder: 3)
        let dataDest = inputTaskAgenda.taskInfo
            .flatMapLatest { taskInfo -> Observable<[Task]> in
                let isEmpty = taskInfo["isEmpty"] as? Bool ?? false
                let clearEmptyData = taskInfo["clearEmptyData"] as? Bool ?? false
                let userID = taskInfo["userId"] as? String ?? ""
                let tasksID = taskInfo["taskId"] as? [String] ?? []
                let findType = taskInfo["findType"] as? Constant.FindType ?? Constant.FindType.findIn
                //
                var validedQuery : Bool = false
                if(userID.count > 0){
                    if(inputTaskAgenda.findOther == false){
                        if(tasksID.count > 0){
                            validedQuery = true
                        }
                    }
                    else{
                        if(isEmpty == false){
                            validedQuery = true
                        }
                    }
                }
                //
                if(validedQuery == true){
                    return self.taskController.subscribleTaskByIds(projectId:inputTaskAgenda.projectId, userId:userID,ids:tasksID,findType:findType,byMe:true,needEmptyData: inputTaskAgenda.needEmptyData,needSortByTimeStamp: true)
                        .retry(3)
                        .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                        .catchErrorJustReturn([])
                }
                else{
                    if(isEmpty == true){
                        if(clearEmptyData == false){
                            return Observable.just([TaskViewModel.emptyTask])
                        }
                        else{
                            return Observable.just([])
                        }
                    }
                    else{
                        return Observable.just(taskHolder)
                    }
                }
        }
        
        self.tasks =
            Observable.combineLatest(inputTaskAgenda.dataSource, dataDest) { (dataSource: $0, dataDest: $1) }
                .distinctUntilChanged { (old: (dataSource: [Task], dataDest: [Task]),
                    new: (dataSource: [Task], dataDest: [Task])) in
                    return (TaskService.checkHolderTask(tasks: old.dataDest) == true
                        && TaskService.checkHolderTask(tasks: new.dataDest) ==  true)
                }
                .flatMapLatest { dataSource, dataDest -> Observable<[Task]> in
                    if(dataSource.count == 0){
                        return Observable.just(dataDest)
                    }
                    else{
                        var resultSource : [Task]
                        if(TaskService.checkEmptyTask(tasks: dataDest) == false){
                            resultSource = dataSource + dataDest
                        }
                        else{
                            resultSource = dataSource
                        }
                        return Observable.just(resultSource)
                    }
                }
        
        if(inputTaskAgenda.findOther == true){
            self.otherTasks = inputTaskAgenda.taskInfo
                .flatMapLatest { taskInfo -> Observable<[Task]> in
                    let userID = taskInfo["userId"] as? String ?? ""
                    let tasksID = taskInfo["taskId"] as? [String] ?? []
                    let findType = taskInfo["findType"] as? Constant.FindType ?? Constant.FindType.findIn
                    //
                    var validedQuery : Bool = false
                    if(userID.count > 0){
                        if(inputTaskAgenda.findOther == false){
                            if(tasksID.count > 0){
                                validedQuery = true
                            }
                        }
                        else{
                            validedQuery = true
                        }
                    }
                    if(validedQuery == true){
                        return self.taskController.subscribleTaskByIds(projectId: inputTaskAgenda.projectId, userId:userID,ids:tasksID,findType:findType,byMe: false,needEmptyData: inputTaskAgenda.needEmptyData,needSortByTimeStamp: true)
                            .retry(3)
                            .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith(taskHolder) // clears results on new search term
                            .catchErrorJustReturn([])
                    }
                    else{
                        return Observable.just([])
                    }
            }
        }
    }
    
    init(inputTaskId: (
        taskID: String,
        taskName: String,
        loginTaps: Observable<Void>,
        taskController: FirestoreTaskAPI
        )) {
        
        self.taskController = inputTaskId.taskController
        
        let taskHolder = Task(taskId: inputTaskId.taskID, name: inputTaskId.taskName, dataType: .holder)
        self.tasks = inputTaskId.loginTaps
            .flatMapLatest { _ -> Observable<[Task]> in
                return self.taskController.getTaskById(taskId:inputTaskId.taskID)
                    .retry(3)
                    .startWith([taskHolder])
                    .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                    .catchErrorJustReturn([])
        }
    }
    
//    init(inputSingleTaskId: (
//        taskID: String,
//        taskName: String,
//        loginTaps: Observable<Void>,
//        taskController: FirestoreTaskAPI
//        )) {
//
//        self.taskController = inputSingleTaskId.taskController
//
//        let taskHolder = Task(taskId: inputSingleTaskId.taskID, name: inputSingleTaskId.taskName, dataType: .holder)
//        self.tasks = inputSingleTaskId.loginTaps
//            .flatMapLatest { _ -> Observable<[Task]> in
//                return self.taskController.getTaskById(taskId:inputSingleTaskId.taskID)
//                    .retry(3)
//                    .startWith([taskHolder])
//                    .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
//                    .catchErrorJustReturn([])
//        }
//    }
    
    init(inputSingleTaskId: (
        taskID: String,
        loginTaps: Observable<Void>,
        taskController: FirestoreTaskAPI
        )) {

        self.taskController = inputSingleTaskId.taskController
        //
        self.tasks = inputSingleTaskId.loginTaps
            .flatMapLatest { _ -> Observable<[Task]> in
                return self.taskController.subscribleTaskById(taskId:inputSingleTaskId.taskID)
                    .retry(3)
                    .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                    .catchErrorJustReturn([])
        }
    }

    
}
