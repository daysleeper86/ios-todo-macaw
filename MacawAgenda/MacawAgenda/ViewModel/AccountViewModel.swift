//
//  AccountViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import FirebaseAuth
import RxSwiftUtilities

class AccountViewModel {
    // Inputs
    fileprivate var controller: AccountAPIController?
    
    // Results
    var validatedEmail: Observable<Bool>?
    //
    var validatedName: Observable<ValidationResult>?
    //
    var validatedPassword: Observable<ValidationResult>?
    //
    var validatedNewPassword: Observable<ValidationResult>?
    //
    var validatedConfirmPassword: Observable<ValidationResult>?
    //
    var loginEnabled: Observable<Bool>?
    // Is signing process in progress
    let signingIn: Observable<Bool>
    // Has user signed in
    var signupInMyServer: Observable<LoginResult>?
    var signedIn: Observable<LoginResult>?
    
    init(inputEmailInput: (
        email: Observable<String>,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        
        self.controller = inputEmailInput.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        validatedEmail = inputEmailInput.email
            .map { email in
                return Util.isValidEmail(email)
            }
            .share(replay: 1)
        
        
        loginEnabled = Observable.combineLatest(
            validatedEmail!,
            signingIn.asObservable()
        )   { email, signingIn in
            email &&
                !signingIn
            }
            .distinctUntilChanged()
        
        signedIn = inputEmailInput.loginTaps
            .withLatestFrom(inputEmailInput.email)
            .flatMapLatest { email -> Observable<Event<ResponseAPICheckEmail>> in
                return inputEmailInput.controller.checkAccount(email: email,tracker: signingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
            }
    }
    
    init(inputSignUpEmail: (
        email: Observable<String>,
        name: Observable<String>?,
        password: Observable<String>?,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        self.controller = inputSignUpEmail.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        validatedName = inputSignUpEmail.name!
            .map { name in
                return Util.validateUserName(name)
        }
        
        validatedPassword = inputSignUpEmail.password!
            .map { password in
                return Util.validatePassword(password)
        }
        let emailNameAndPassword = Observable.combineLatest(inputSignUpEmail.email, inputSignUpEmail.name!, inputSignUpEmail.password!) { (email: $0, name: $1, password: $2) }
        
        loginEnabled = Observable.combineLatest(
            validatedName!,
            validatedPassword!,
            signingIn.asObservable())
        { name, password, signingIn in
            name.isValid &&
                password.isValid &&
                !signingIn
            }
            .distinctUntilChanged()
        
        signupInMyServer = inputSignUpEmail.loginTaps
            .withLatestFrom(emailNameAndPassword)
            .flatMap { pair  -> Observable<Event<Account>> in
                return inputSignUpEmail.controller.signUp(email: pair.email, name: pair.name, password: pair.password, tracker: signingIn)
            }
            .map { accountInfo in
                if(accountInfo.isCompleted == false){
                    if let account = accountInfo.element{
                        if(account.valid() == false){
                            let strMessage = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                            let statusCode : Int = AppURL.APIStatusCode.INTERNAL_SERVER_ERROR.rawValue
                            return LoginResult.failed(output: (statusCode,strMessage))
                        }
                        else{
                            return LoginResult.ok(output: account)
                        }
                    }
                    else{
                        return Util.parseErrorResult(errorResult: accountInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    init(inputLoginEmail: (
        email: Observable<String>,
        password: Observable<String>?,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        self.controller = inputLoginEmail.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        
        validatedPassword = inputLoginEmail.password!
            .map { password in
                return Util.validatePassword(password)
        }
        
        loginEnabled = Observable.combineLatest(
            validatedPassword!,
            signingIn.asObservable()
        )   { password, signingIn in
            password.isValid &&
                !signingIn
            }
            .distinctUntilChanged()
        
        let emailAndPassword = Observable.combineLatest(inputLoginEmail.email, inputLoginEmail.password!) { (email: $0, password: $1) }
        
        let signedInMyServer : Observable<LoginResult> = inputLoginEmail.loginTaps
            .withLatestFrom(emailAndPassword)
            .flatMapLatest { pair  -> Observable<Event<Account>> in
                return inputLoginEmail.controller.signIn(email: pair.email, password: pair.password, tracker: signingIn)
            }
            .map { accountInfo in
                if(accountInfo.isCompleted == false){
                    if let account = accountInfo.element{
                        if(account.valid() == false){
                            let strMessage = NSLocalizedString("invalid_email_or_password", comment: "")
                            let statusCode : Int = 0
                            return LoginResult.failed(output: (statusCode,strMessage))
                        }
                        else{
                            return LoginResult.ok(output: account)
                        }
                    }
                    else{
                        return Util.parseErrorResult(errorResult: accountInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
            }
        
        let appFireStore = AppFirestore.sharedAPI
        signedIn = signedInMyServer
            .flatMap { loginResultAPI -> Observable<LoginResult> in
                var accountAPI : Account?
                switch loginResultAPI {
                case .ok(let output):
                    accountAPI = output as? Account
                default:
                    break
                }
                if let strongAccountAPI = accountAPI {
                    return appFireStore.signIn(username: strongAccountAPI.email, password: strongAccountAPI.password)
                        .retry(3)
                        .trackActivity(signingIn)
                        .materialize()
                        .map { authResultEvent in
                            if(authResultEvent.isCompleted == false){
                                let loginResult = Util.parseAuthenFireStoreResult(authDataResult: authResultEvent.element, errorResult: authResultEvent.error)
                                var account : Account?
                                switch loginResult {
                                case .ok(let output):
                                    account = output as? Account
                                default:
                                    break
                                }
                                if account != nil {
                                    return LoginResult.ok(output: strongAccountAPI)
                                }
                                else{
                                    return loginResult
                                }
                            }
                            else{
                                return LoginResult.ok(output: nil)
                            }
                    }
                }
                else{
                    return Observable.just(loginResultAPI)
                }
            }
            .share(replay: 1)
    }
    
    init(inputGoogleSignIn: (
        loginInfos: Observable<[String:Any]>,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        self.controller = inputGoogleSignIn.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        let signedInMyServer : Observable<LoginResult> = inputGoogleSignIn.loginTaps
            .withLatestFrom(inputGoogleSignIn.loginInfos)
            .flatMapLatest { infos  -> Observable<Event<Account>> in
                let email = infos[UserInfoKeyString.MY_EMAIL_KEY] as? String ?? ""
                let name = infos[UserInfoKeyString.MY_NAME_KEY] as? String ?? ""
                let userId = infos[UserInfoKeyString.MY_USERID_KEY] as? String ?? ""
                let urlImage = infos[UserInfoKeyString.MY_URL_AVATAR_KEY] as? String ?? ""
                let accessToken = infos[UserInfoKeyString.MY_TOKEN_KEY_API] as? String ?? ""
                return inputGoogleSignIn.controller.signInGoogle(email: email, name: name, image: urlImage,userId: userId, token: accessToken, tracker: signingIn)
            }
            .map { accountInfo in
                if(accountInfo.isCompleted == false){
                    if let account = accountInfo.element{
                        if(account.valid() == false){
                            let strMessage = NSLocalizedString("something_went_wrong", comment: "") + " ." + NSLocalizedString("please_try_again", comment: "")
                            let statusCode : Int = 0
                            return LoginResult.failed(output: (statusCode,strMessage))
                        }
                        else{
                            return LoginResult.ok(output: account)
                        }
                    }
                    else{
                        return Util.parseErrorResult(errorResult: accountInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
            }
        
        let appFireStore = AppFirestore.sharedAPI
        signedIn = signedInMyServer
            .flatMap { loginResultAPI -> Observable<LoginResult> in
                var accountAPI : Account?
                switch loginResultAPI {
                case .ok(let output):
                    accountAPI = output as? Account
                default:
                    break
                }
                if let strongAccountAPI = accountAPI {
                    return appFireStore.signIn(username: strongAccountAPI.email, password: strongAccountAPI.password)
                        .retry(3)
                        .trackActivity(signingIn)
                        .materialize()
                        .map { authResultEvent in
                            if(authResultEvent.isCompleted == false){
                                let loginResult = Util.parseAuthenFireStoreResult(authDataResult: authResultEvent.element, errorResult: authResultEvent.error)
                                var account : Account?
                                switch loginResult {
                                case .ok(let output):
                                    account = output as? Account
                                default:
                                    break
                                }
                                if account != nil {
                                    return LoginResult.ok(output: strongAccountAPI)
                                }
                                else{
                                    return loginResult
                                }
                            }
                            else{
                                return LoginResult.ok(output: nil)
                            }
                        }
                }
                else{
                    return Observable.just(loginResultAPI)
                }
            }
            .share(replay: 1)
    }
    
    init(inputReLoginFireStore: (
        loginInfos: Observable<Account>,
        loginTaps: Observable<Void>
        )) {
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        let appFireStore = AppFirestore.sharedAPI
        signedIn = inputReLoginFireStore.loginTaps
                .withLatestFrom(inputReLoginFireStore.loginInfos)
                .flatMapLatest { account -> Observable<LoginResult> in
                    return appFireStore.signIn(username: account.email, password: account.password)
                        .retry(3)
                        .trackActivity(signingIn)
                        .materialize()
                        .map { authResultEvent in
                            if(authResultEvent.isCompleted == false){
                                let loginResult = Util.parseAuthenFireStoreResult(authDataResult: authResultEvent.element, errorResult: authResultEvent.error)
                                var account : Account?
                                switch loginResult {
                                case .ok(let output):
                                    account = output as? Account
                                default:
                                    break
                                }
                                if account != nil {
                                    return LoginResult.ok(output: account)
                                }
                                else{
                                    return loginResult
                                }
                            }
                            else{
                                return LoginResult.ok(output: nil)
                            }
                    }
                }
                .share(replay: 1)
    }
}
