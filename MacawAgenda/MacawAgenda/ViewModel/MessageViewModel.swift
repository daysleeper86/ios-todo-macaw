//
//  MessageViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class MessageViewModel {
    // Inputs
    let messageController: MessageController
    
    // Results
    var messages: Observable<[Message]>?
    
    init(inputNeedTaskId: (
        taskId: Observable<String>?,
        messageController: MessageController
        )) {
        self.messageController = inputNeedTaskId.messageController
        //
        if(inputNeedTaskId.taskId != nil){
            let messageHolder = MessageService.createHolderMessage(numHolder: 3)
            self.messages = inputNeedTaskId.taskId!
                .flatMapLatest { taskId -> Observable<[Message]> in
                    if(taskId.count > 0){
                        return self.messageController.subscribleAllMessageOfTaskId(taskId: taskId)
                            .retry(3)
                            .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith(messageHolder) // clears results on new search term
                            .catchErrorJustReturn([MessageService.messageNoneComment])
                    }
                    else{
                        return Observable.just([MessageService.messageNoneComment])
                    }
                    
            }.share(replay: 1)
        }
    }
    
    init(inputNeedAgendaRoomId: (
        agendaRoomId: Observable<Room>,
        messageController: MessageController
        )) {
        self.messageController = inputNeedAgendaRoomId.messageController
        //
        let messageHolder = MessageService.createHolderMessage(numHolder: 5)
        self.messages = inputNeedAgendaRoomId.agendaRoomId
            .flatMapLatest { roomId -> Observable<[Message]> in
                if(roomId.roomId.count > 0){
                    if(roomId.dataType == .holder){
                        return Observable.just(messageHolder)
                    }
                    else{
                        return self.messageController.subscribleAllMessageOfAgendaId(roomId: roomId.roomId)
                            .retry(3)
                            .retryOnBecomesReachable([MessageService.emptyMessage], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith(messageHolder) // clears results on new search term
                            .catchErrorJustReturn([MessageService.emptyMessage])
                    }
                }
                else{
                    if(roomId.dataType == .holder){
                        return Observable.just(messageHolder)
                    }
                    else if(roomId.dataType == .empty){
                        return Observable.just([MessageService.emptyMessage])
                    }
                    else{
                        return Observable.just([])
                    }
                }
            }.share(replay: 1)
    }
}
