//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class AgendaAPIViewModel {
    // Inputs
    var agendaAPIController: AgendaAPIController?
    
    // Results
    var signingIn: Observable<Bool>?
    var processEnabled: Observable<Bool>?
    var processFinished: Observable<LoginResult>?
    
    init(inputCreateAgenda: (
        projectId: Observable<String>,
        loginTaps: Observable<Void>,
        agendaAPIController: AgendaAPIController
        )) {
        self.agendaAPIController = inputCreateAgenda.agendaAPIController
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        processEnabled = signingIn.asObservable()
            .map { signingIn in
                return !signingIn
        }
        //
        let projectAndprocessEnabled = Observable.combineLatest(inputCreateAgenda.projectId, processEnabled!) { (projectId: $0, processEnabled: $1) }
        //
        processFinished = inputCreateAgenda.loginTaps
            .withLatestFrom(projectAndprocessEnabled)
            .flatMapLatest { projectId,processEnabled -> Observable<LoginResult> in
                if(processEnabled == true){
                    if(projectId.count > 0){
                        return inputCreateAgenda.agendaAPIController.createAgenda(projectId: projectId, tracker: signingIn)
                    }
                    else{
                        return Observable.just(LoginResult.ok(output: nil))
                    }
                }
                else{
                    return Observable.just(LoginResult.ok(output: nil))
                }
        }
    }
    
    init(inputUpdateAgendaDonePlanning: (
        projectId: String,
        agendaId: String,
        hasTask:Observable<Bool>,
        status: Int,
        loginTaps: Observable<Void>,
        agendaAPIController: AgendaAPIController
        )) {
        self.agendaAPIController = inputUpdateAgendaDonePlanning.agendaAPIController
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        processEnabled = Observable.combineLatest(
            inputUpdateAgendaDonePlanning.hasTask,
            signingIn.asObservable()
        )   { hasTask, signingIn in
            hasTask &&
                !signingIn
            }
            .distinctUntilChanged()
        //
        processFinished = inputUpdateAgendaDonePlanning.loginTaps
            .flatMapLatest { 
                return inputUpdateAgendaDonePlanning.agendaAPIController.updateAgendaDonePlanning(projectId: inputUpdateAgendaDonePlanning.projectId, agendaId: inputUpdateAgendaDonePlanning.agendaId, status: inputUpdateAgendaDonePlanning.status, tracker: signingIn)
        }
    }
    
    init(inputUpdateAgendaAddOrRemoveTask: (
        projectId: String,
        addOrRemove: Int,
        agendaId: Observable<String>,
        taskId: Observable<String>,
        loginTaps: Observable<Void>,
        agendaAPIController: AgendaAPIController
        )) {
        self.agendaAPIController = inputUpdateAgendaAddOrRemoveTask.agendaAPIController
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        let validatedAgenda = inputUpdateAgendaAddOrRemoveTask.agendaId
            .map { agendaId in
                return Util.validateName(agendaId)
        }
        //
        processEnabled = Observable.combineLatest(
            validatedAgenda,
            signingIn.asObservable()
        )   { validatedAgenda, signingIn in
            validatedAgenda.isValid &&
                !signingIn
            }
            .distinctUntilChanged()
        //
        let agendaAndTask = Observable.combineLatest(inputUpdateAgendaAddOrRemoveTask.agendaId, inputUpdateAgendaAddOrRemoveTask.taskId) { (agendaId: $0, taskId: $1) }
        //
        processFinished = inputUpdateAgendaAddOrRemoveTask.loginTaps
            .withLatestFrom(agendaAndTask)
            .flatMapLatest { agendaAndTask in
                return inputUpdateAgendaAddOrRemoveTask.agendaAPIController.updateAgendaAddOrRemoveTask(projectId: inputUpdateAgendaAddOrRemoveTask.projectId, agendaId: agendaAndTask.agendaId, taskId: agendaAndTask.taskId,addOrRemove:inputUpdateAgendaAddOrRemoveTask.addOrRemove, tracker: signingIn)
        }
    }
}
