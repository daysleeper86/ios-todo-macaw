//
//  ProjectViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import RxSwiftUtilities

class ProjectAPIViewModel {
    // Inputs
    var projectAPIController: ProjectAPIController?
    
    // Results
    var validatedName: Observable<ValidationResult>?
    var processEnabled: Observable<Bool>?
    var processingIn: Observable<Bool>?
    var processFinished: Observable<LoginResult>?
    
    init(inputCreateProject: (
        name: Observable<String>,
        teamType: Observable<String>,
        teamSize: Observable<Int>,
        loginTaps: Observable<Void>,
        controller: ProjectAPIController
        )) {
        self.projectAPIController = inputCreateProject.controller
        
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        
        validatedName = inputCreateProject.name
            .map { name in
                return Util.validateProjectName(name)
        }
        
        processEnabled = Observable.combineLatest(
            validatedName!,
            inputCreateProject.teamType,
            processingIn.asObservable()
        )   { name, teamType,processingIn in
            name.isValid &&
                teamType.count > 0 &&
                !processingIn
            }
            .distinctUntilChanged()
        
        let projectInfo =
            Observable
                .combineLatest(inputCreateProject.name,inputCreateProject.teamType, inputCreateProject.teamSize) { (name: $0, teamType: $1, teamSize: $2) }
                .distinctUntilChanged { (old: (name: String, teamType: String, teamSize: Int),
                    new: (name: String, teamType: String, teamSize: Int)) in
                    return old.name == new.name && old.teamType == new.teamType && old.teamSize == new.teamSize
        }
        
        processFinished = inputCreateProject.loginTaps
            .withLatestFrom(projectInfo)
            .flatMapLatest {projectInfo -> Observable<Event<Project>> in
                return inputCreateProject.controller.createProject(name: projectInfo.name,teamType: projectInfo.teamType,teamSize: projectInfo.teamSize, tracker: processingIn)
            }
            .map { projectInfo in
                if(projectInfo.isCompleted == false){
                    if let project = projectInfo.element{
                        return LoginResult.ok(output: project)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: projectInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    init(inputUpdateProject: (
        projectId: String,
        oldName: Observable<String>,
        name: Observable<String>,
        loginTaps: Observable<Void>,
        controller: ProjectAPIController
        )) {
        self.projectAPIController = inputUpdateProject.controller
        
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        
        validatedName = inputUpdateProject.name
            .map { name in
                return Util.validateProjectName(name)
        }
        .share(replay: 1)
        
        processEnabled = Observable.combineLatest(
            validatedName!,
            inputUpdateProject.name,
            inputUpdateProject.oldName,
            processingIn.asObservable()
        )   { validName,name,oldName,processingIn in
                validName.isValid &&
                name != oldName &&
                !processingIn
            }
            .distinctUntilChanged()
        
        processFinished = inputUpdateProject.loginTaps
            .withLatestFrom(Observable.combineLatest(inputUpdateProject.name, processEnabled!) { (name: $0, valid: $1) })
            .flatMapLatest{ nameAndValid -> Observable<Event<ResponseAPI>> in
                if(nameAndValid.valid == true){
                    return inputUpdateProject.controller.updateProject(projectId:inputUpdateProject.projectId, name: nameAndValid.name, tracker: processingIn)
                }
                else{
                    //Simulator
                    let eventResponseEvent : Event<ResponseAPI> = Event<ResponseAPI>.completed
                    return Observable.just(eventResponseEvent)
                }
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
        .share(replay: 1)
    }
    
    init(inputCreateOrangnizationProject: (
        orangnizationId: String,
        name: Observable<String>,
        teamLeader: Observable<String>,
        loginTaps: Observable<Void>,
        controller: ProjectAPIController
        )) {
        self.projectAPIController = inputCreateOrangnizationProject.controller
        
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        
        validatedName = inputCreateOrangnizationProject.name
            .map { name in
                return Util.validateProjectName(name)
        }
        
        processEnabled = Observable.combineLatest(
            validatedName!,
            inputCreateOrangnizationProject.teamLeader,
            processingIn.asObservable()
        )   { name, teamLeader,processingIn in
            name.isValid &&
                teamLeader.count > 0 &&
                !processingIn
            }
            .distinctUntilChanged()
        
        let projectInfo =
            Observable
                .combineLatest(inputCreateOrangnizationProject.name,inputCreateOrangnizationProject.teamLeader) { (name: $0, teamLeader: $1) }
                .distinctUntilChanged { (old: (name: String, teamLeader: String),
                    new: (name: String, teamLeader: String)) in
                    return old.name == new.name && old.teamLeader == new.teamLeader
        }
        
        processFinished = inputCreateOrangnizationProject.loginTaps
            .withLatestFrom(projectInfo)
            .flatMapLatest {projectInfo -> Observable<Event<ResponseAPI>> in
                return inputCreateOrangnizationProject.controller.createOrangnizationProject(oid: inputCreateOrangnizationProject.orangnizationId, name: projectInfo.name,teamLeader: projectInfo.teamLeader, tracker: processingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
}
