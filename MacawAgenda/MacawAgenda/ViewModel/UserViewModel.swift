//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class UserViewModel {
    // Inputs
    var userController: UserController?
    
    // Results
    var account: Observable<Account>?
    var user: Observable<User>?
    
    init(inputMyAccount: (
        userId: Observable<String>?,
        subscrible: Bool,
        userController: UserController
        )) {
        self.userController = inputMyAccount.userController
        //
        if(inputMyAccount.userId != nil){
            if(inputMyAccount.subscrible == true){
                self.account = inputMyAccount.userId!
                    .flatMapLatest { userId -> Observable<Account> in
                        return inputMyAccount.userController.subscribleMyUserInfo(userId)
                            .retry(3)
                            .retryOnBecomesReachable(UserService.emptyAccount, reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .catchErrorJustReturn(UserService.emptyAccount)
                }
            }
            else{
                self.account = inputMyAccount.userId!
                    .flatMapLatest { userId -> Observable<Account> in
                        return inputMyAccount.userController.getMyUserInfo(userId)
                            .retry(3)
                            .retryOnBecomesReachable(UserService.emptyAccount, reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .catchErrorJustReturn(UserService.emptyAccount)
                }
            }
        }
        
    }
    
    init(inputUserInfo: (
        userId: Observable<String>?,
        userController: UserController
        )) {
        self.userController = inputUserInfo.userController
        //
        if(inputUserInfo.userId != nil){
            self.user = inputUserInfo.userId!
                .flatMapLatest { userId -> Observable<User> in
                    return inputUserInfo.userController.subscribleUserInfo(userId)
                        .retry(3)
                        .retryOnBecomesReachable(UserService.emptyUser, reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                        .catchErrorJustReturn(UserService.emptyUser)
            }
        }
    }
}
