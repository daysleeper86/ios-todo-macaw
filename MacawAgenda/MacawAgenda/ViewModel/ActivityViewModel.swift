//
//  ActivityViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class ActivityViewModel {
    // Inputs
    let activityController: ActivityController
    
    // Results
    var activities: Observable<[Activity]>?
    static let emptyActivity = ActivityService.createEmptyActivity()
    
    init(input: (
        roomId: Observable<Room>,
        activityController: ActivityController
        )) {
        self.activityController = input.activityController
        //
        let activityHolder = ActivityService.createHolderActivity(numHolder: 5)
        self.activities =
            input.roomId
                .flatMapLatest { roomId -> Observable<[Activity]> in
                    if(roomId.roomId.count > 0){
                        return self.activityController.subscribleAllActivityOfRoomId(roomId: roomId.roomId)
                            .retry(3)
                            .retryOnBecomesReachable([ActivityViewModel.emptyActivity], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith(activityHolder) // clears results on new search term
                            .catchErrorJustReturn([ActivityViewModel.emptyActivity])
                    }
                    else{
                        if(roomId.dataType == .holder){
                            return Observable.just(activityHolder)
                        }
                        else if(roomId.dataType == .empty){
                            return Observable.just([ActivityViewModel.emptyActivity])
                        }
                        else{
                            return Observable.just([])
                        }
                    }
                    
        }
    }
}
