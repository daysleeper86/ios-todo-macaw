//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class MessageAPIViewModel {
    // Inputs
    var messageAPIController: MessageAPIController?
    
    // Results
    var validatedContent: Observable<ValidationResult>?
    var processingIn: Observable<Bool>?
    var processEnabled: Observable<Bool>?
    var processFinished: Observable<LoginResult>?
    
    init(inputSendMessageRoomAgenda: (
        projectId: String,
        roomId: Observable<Room>,
        mentions: Observable<[String]>,
        content: Observable<String>,
        loginTaps: Observable<Void>,
        messageAPIController: MessageAPIController
        )) {
        self.messageAPIController = inputSendMessageRoomAgenda.messageAPIController
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        validatedContent = inputSendMessageRoomAgenda.content
            .map { content in
                return Util.validateMessageContent(content)
        }
        let validatedRoomId = inputSendMessageRoomAgenda.roomId
            .map { roomId in
                return Util.validateContent(roomId.roomId)
        }
        //
        processEnabled = Observable.combineLatest(
            validatedContent!,
            validatedRoomId,
            processingIn.asObservable()
        )   { content, roomId, processingIn in
                content.isValid &&
                roomId.isValid &&
                !processingIn
            }
            .distinctUntilChanged()
        //
        let roomIdAndContent = Observable.combineLatest(inputSendMessageRoomAgenda.roomId, inputSendMessageRoomAgenda.content,inputSendMessageRoomAgenda.mentions) { (room: $0, content: $1, mentions: $2) }
        
        processFinished = inputSendMessageRoomAgenda.loginTaps
            .withLatestFrom(roomIdAndContent)
            .flatMapLatest { roomIdAndContent in
                return inputSendMessageRoomAgenda.messageAPIController.sendMessageToRoomAgenda(projectId:  inputSendMessageRoomAgenda.projectId, roomId: roomIdAndContent.room.roomId,mentions:roomIdAndContent.mentions,content: roomIdAndContent.content,tracker: processingIn)
        }
    }
    
    init(inputSendMessageRoomTask: (
        projectId: String,
        roomId: Observable<String>?,
        mentions: Observable<[String]>,
        content: Observable<String>,
        loginTaps: Observable<Void>,
        messageAPIController: MessageAPIController
        )) {
        self.messageAPIController = inputSendMessageRoomTask.messageAPIController
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        validatedContent = inputSendMessageRoomTask.content
            .map { content in
                return Util.validateMessageContent(content)
        }
        //
        if(inputSendMessageRoomTask.roomId != nil){
            let validatedRoomId = inputSendMessageRoomTask.roomId!
                .map { roomId in
                    return Util.validateContent(roomId)
            }
            //
            processEnabled = Observable.combineLatest(
                validatedContent!,
                validatedRoomId,
                processingIn.asObservable()
            )   { content, roomId, processingIn in
                content.isValid &&
                    roomId.isValid &&
                    !processingIn
                }
                .distinctUntilChanged()
            //
            let roomIdAndContent = Observable.combineLatest(inputSendMessageRoomTask.roomId!, inputSendMessageRoomTask.content, inputSendMessageRoomTask.mentions) { (roomId: $0, content: $1, mentions: $2) }
            //
            processFinished = inputSendMessageRoomTask.loginTaps
                .withLatestFrom(roomIdAndContent)
                .flatMapLatest { roomIdAndContent in
                    return inputSendMessageRoomTask.messageAPIController.sendMessageToRoomTask(projectId:  inputSendMessageRoomTask.projectId, roomId: roomIdAndContent.roomId,mentions:roomIdAndContent.mentions, content: roomIdAndContent.content,tracker: processingIn)
            }
        }
    }
}
