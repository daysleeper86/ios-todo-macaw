//
//  ProjectViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import RxSwiftUtilities

class OrangnizationViewModel {
    // Inputs
    var orangnizationController: OrangnizationController?

    // Results
    var orangnizations: Observable<[Orangnization]>?
    
    static let emptyOrangnization = OrangnizationService.createEmptyOrangnization()
    
    init(inputOrangnizationById: (
        orangnizationId: Observable<String>?,
        orangnizationController: OrangnizationController
        )) {
        self.orangnizationController = inputOrangnizationById.orangnizationController
        //
        if(inputOrangnizationById.orangnizationId != nil){
            self.orangnizations = inputOrangnizationById.orangnizationId!
                .flatMapLatest {[weak self] orangnizationId -> Observable<[Orangnization]> in
                    if let strongSelf = self, let strongOrangnizationController = strongSelf.orangnizationController {
                        return strongOrangnizationController.subscribleOrangnizationById(orangnizationId: orangnizationId)
                            .retry(3)
                            .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith([]) // clears results on new search term
                            .catchErrorJustReturn([OrangnizationViewModel.emptyOrangnization])
                    }
                    else{
                        return Observable.just([OrangnizationViewModel.emptyOrangnization])
                    }
            }
            .share(replay: 1)
        }
    }

    init(inputOrangnizationByUserId: (
        userId: Observable<String>?,
        orangnizationController: OrangnizationController
        )) {
        self.orangnizationController = inputOrangnizationByUserId.orangnizationController
        //
        if(inputOrangnizationByUserId.userId != nil){
            self.orangnizations = inputOrangnizationByUserId.userId!
                .flatMapLatest {[weak self] userId -> Observable<[Orangnization]> in
                    if let strongSelf = self, let strongOrangnizationController = strongSelf.orangnizationController {
                        return strongOrangnizationController.subscribleOrangnizationByUserId(userId: userId)
                            .retry(3)
                            .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                            .startWith([]) // clears results on new search term
                            .catchErrorJustReturn([OrangnizationViewModel.emptyOrangnization])
                    }
                    else{
                        return Observable.just([OrangnizationViewModel.emptyOrangnization])
                    }
                }
                .share(replay: 1)
        }
    }
}
