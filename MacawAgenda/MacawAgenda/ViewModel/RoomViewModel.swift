//
//  RoomViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class RoomViewModel {
    // Input
    let roomController: RoomController
    //Task
    var rooms: Observable<[Room]>?
    // Is signing process in progress
    init(input: (
        projectId: String,
        userId: Observable<String>,
        date: Date,
        roomController: RoomController
        )) {
        self.roomController = input.roomController
        let roomHolder = RoomService.createHolderRoom(numHolder: 1)
        self.rooms = input.userId
            .flatMapLatest { userId -> Observable<[Room]> in
                if(userId.count > 0){
                    return self.roomController.subscribleRoomOfAgenda(projectId: input.projectId, userId, input.date)
                        .retry(3)
                        .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                        .startWith(roomHolder) // clears results on new search term
                        .catchErrorJustReturn([])
                }
                else{
                    return Observable.just([])
                }
        }
        .share(replay: 1)
        
    }
}
