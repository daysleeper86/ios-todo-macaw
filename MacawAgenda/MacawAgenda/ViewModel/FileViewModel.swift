//
//  FileViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class FileViewModel {
    // Inputs
    let fileController: FileController
    
    // Results
    var files: Observable<[File]>?
    
    init(input: (
        taskId: Observable<String>,
        dataSource : Observable <[File]>,
        fileController: FileController
        )) {
        self.fileController = input.fileController
        //
        
        let dataDest = input.taskId
            .flatMapLatest { taskId -> Observable<[File]> in
                if(taskId.count > 0){
                    let fileHolder = FileService.createHolderFile(numHolder: 3)
                    return self.fileController.subscribleAllFileOfTaskId(taskId: taskId)
                        .retry(3)
                        .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                        .startWith(fileHolder) // clears results on new search term
                        .catchErrorJustReturn([])
                }
                else{
                    return Observable.just([])
                }
        }
        
        self.files =
            Observable.combineLatest(input.dataSource, dataDest) { (dataSource: $0, dataDest: $1) }
                .distinctUntilChanged { (old: (dataSource: [File], dataDest: [File]),
                    new: (dataSource: [File], dataDest: [File])) in
                    return (FileService.checkHolderFile(files: old.dataDest) == true
                        && FileService.checkHolderFile(files: new.dataDest) ==  true)
                }
                .flatMapLatest { dataSource, dataDest -> Observable<[File]> in
                    if(dataSource.count == 0){
                        return Observable.just(dataDest)
                    }
                    else{
                        let resultSource = dataSource + dataDest
                        return Observable.just(resultSource)
                    }
            }.share(replay: 1)
    }
}
