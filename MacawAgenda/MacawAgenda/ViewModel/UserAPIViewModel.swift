//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class UserAPIViewModel {
    // Input
    var userAPIController: UserAPIController?
    // Input
    var validatedEmail: Observable<Bool>?
    var signingIn: Observable<Bool>?
    var loginEnabled: Observable<Bool>?
    var signedIn: Observable<LoginResult>?
    
    init(inputInviteMember: (
        oid: String,
        projectId: String,
        email: Observable<String>,
        loginTaps: Observable<Void>,
        userAPIController: UserAPIController
        )) {
        self.userAPIController = inputInviteMember.userAPIController
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        validatedEmail = inputInviteMember.email
            .map { email in
                return Util.isValidEmail(email)
        }
        //
        loginEnabled = Observable.combineLatest(
            validatedEmail!,
            signingIn.asObservable()
        )   { email, signingIn in
            email &&
                !signingIn
            }
            .distinctUntilChanged()
        //
        signedIn = inputInviteMember.loginTaps
            .withLatestFrom(inputInviteMember.email)
            .flatMapLatest { email -> Observable<LoginResult> in
                if(inputInviteMember.oid.count > 0){
                    //
                    let userId = Util.getUserIdFrom(email: email)
                    if(userId.count == 0){
                        return inputInviteMember.userAPIController.inviteUser(projectId: inputInviteMember.projectId, email: email, tracker: signingIn)
                    }
                    else{
                        return inputInviteMember.userAPIController.inviteUserOrangnization(oid:inputInviteMember.oid, projectId: inputInviteMember.projectId,email: email, userId: userId, tracker: signingIn)
                    }
                }
                else{
                    return inputInviteMember.userAPIController.inviteUser(projectId: inputInviteMember.projectId, email: email, tracker: signingIn)
                }
        }
    }
    
    
    init(inputRemoveMember: (
        projectId: String,
        userId: Observable<String>,
        userAPIController: UserAPIController
        )) {
        self.userAPIController = inputRemoveMember.userAPIController
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        self.signedIn = inputRemoveMember.userId
            .flatMapLatest { userId in
                return inputRemoveMember.userAPIController.removeUser(projectId: inputRemoveMember.projectId, userId: userId,tracker: signingIn)
            }
    }
    
    init(inputForgetPass: (
        email: Observable<String>,
        loginTaps: Observable<Void>,
        userAPIController: UserAPIController
        )) {
        self.userAPIController = inputForgetPass.userAPIController
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        validatedEmail = inputForgetPass.email
            .map { email in
                return Util.isValidEmail(email)
        }
        //
        loginEnabled = Observable.combineLatest(
            validatedEmail!,
            signingIn.asObservable()
        )   { email, signingIn in
            email &&
                !signingIn
            }
            .distinctUntilChanged()
        //
        signedIn = inputForgetPass.loginTaps
            .withLatestFrom(inputForgetPass.email)
            .flatMapLatest { email in
                return inputForgetPass.userAPIController.forgetPassword(email: email,tracker: signingIn)
        }
    }
}
