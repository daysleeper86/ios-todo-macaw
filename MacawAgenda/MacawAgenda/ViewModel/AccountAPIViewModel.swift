//
//  AccountViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import FirebaseAuth
import RxSwiftUtilities

class AccountAPIViewModel {
    // Input
    var validatedName: Observable<ValidationResult>?
    //
    var validatedPassword: Observable<ValidationResult>?
    //
    var validatedNewPassword: Observable<ValidationResult>?
    //
    var validatedConfirmPassword: Observable<ValidationResult>?
    //
    var loginEnabled: Observable<Bool>?
    // Is signing process in progress
    let signingIn: Observable<Bool>
    // Has user signed in
    var signedIn: Observable<LoginResult>?
    //
    fileprivate var controller: AccountAPIController?
    
    init(inputChangePass: (
        userId: String,
        password: Observable<String>?,
        newPassword: Observable<String>,
        confirmNewPassword: Observable<String>,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        self.controller = inputChangePass.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        validatedPassword = inputChangePass.password?
            .map { password in
                return Util.validatePassword(password)
            }
            .share(replay: 1)
        
        validatedNewPassword = inputChangePass.newPassword
            .map { password in
                return Util.validatePassword(password)
            }
            .share(replay: 1)
        
        let validConfirmPassword = inputChangePass.confirmNewPassword
            .map { password in
                return Util.validatePassword(password)
            }
            .share(replay: 1)
        
        validatedConfirmPassword = Observable.combineLatest(inputChangePass.newPassword, inputChangePass.confirmNewPassword,validConfirmPassword) {(newPassword: $0, confirmNewPassword: $1,validatePassword: $2)}
            .flatMapLatest { pair -> Observable<ValidationResult> in
                if(pair.validatePassword.isValid == false){
                    return Observable.just(pair.validatePassword)
                }
                return Observable.just(Util.validateNewAndConfirmPassword(pair.newPassword, pair.confirmNewPassword))
            }
            .share(replay: 1)
        
        let oldAndNewPassword = Observable.combineLatest(inputChangePass.password ?? Observable.just(""), inputChangePass.newPassword) { (oldPassword: $0, newPassword: $1) }
        
        if(validatedPassword != nil){
            loginEnabled = Observable.combineLatest(
                validatedPassword!,
                validatedNewPassword!,
                validatedConfirmPassword!,
                signingIn.asObservable()){
                    password, newPassword, confirmPassword, signingIn
                    in
                    password.isValid &&
                        newPassword.isValid &&
                        confirmPassword.isValid &&
                        !signingIn
                }
                .distinctUntilChanged()
        }
        else{
            loginEnabled = Observable.combineLatest(
                validatedNewPassword!,
                validatedConfirmPassword!,
                signingIn.asObservable()){
                    newPassword, confirmPassword, signingIn
                    in
                    newPassword.isValid &&
                        confirmPassword.isValid &&
                        !signingIn
                }
                .distinctUntilChanged()
        }
        
        
        signedIn = inputChangePass.loginTaps
            .withLatestFrom(oldAndNewPassword)
            .flatMapLatest { pair  -> Observable<Event<ResponseAPI>> in
                return inputChangePass.controller.changePass(userId: inputChangePass.userId, currentPass: pair.oldPassword, newPass: pair.newPassword, tracker: signingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    init(inputUpdateProfileName: (
        userId: String,
        oldName: Observable<String>,
        name: Observable<String>,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        self.controller = inputUpdateProfileName.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        validatedName = inputUpdateProfileName.name
            .map { name in
                return Util.validateUserName(name)
        }
        .share(replay: 1)
        
        loginEnabled = Observable.combineLatest(
            validatedName!,
            inputUpdateProfileName.name,
            inputUpdateProfileName.oldName,
            signingIn.asObservable()
        )   { validName,name,oldName,signingIn in
            validName.isValid &&
                name != oldName &&
                !signingIn
            }
            .distinctUntilChanged()
        
        signedIn = inputUpdateProfileName.loginTaps
            .withLatestFrom(Observable.combineLatest(inputUpdateProfileName.name, loginEnabled!) { (name: $0, valid: $1) })
            .flatMapLatest{ nameAndValid -> Observable<Event<ResponseAPI>> in
                if(nameAndValid.valid == true){
                    return inputUpdateProfileName.controller.updateProfileName(userId: inputUpdateProfileName.userId, name: nameAndValid.name, tracker: signingIn)
                }
                else{
                    //Simulator
                    let eventResponseEvent : Event<ResponseAPI> = Event<ResponseAPI>.completed
                    return Observable.just(eventResponseEvent)
                }
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
        .share(replay: 1)
    }
    
    init(inputUpdateSettingUser: (
        userId: String,
        emailStatus: Observable<Bool>,
        notificationStatus: Observable<Bool>,
        notificationToken: Observable<String>,
        loginTaps: Observable<Void>,
        controller: AccountAPIController
        )) {
        
        self.controller = inputUpdateSettingUser.controller
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        let settingInfo = Observable.combineLatest(inputUpdateSettingUser.emailStatus, inputUpdateSettingUser.notificationStatus,inputUpdateSettingUser.notificationToken) {(emailStatus: $0, notificationStatus: $1,notificationToken: $2)}
        //
        signedIn = inputUpdateSettingUser.loginTaps
            .withLatestFrom(settingInfo)
            .flatMapLatest{ settingInfo -> Observable<Event<ResponseAPI>> in
                return inputUpdateSettingUser.controller.updateSettingUser(userId: inputUpdateSettingUser.userId, emailStatus: settingInfo.emailStatus, notificationStatus: settingInfo.notificationStatus, notificationToken: settingInfo.notificationToken, tracker: signingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
            }
            .share(replay: 1)
    }
    
    init(inputUpdateProfileAvatar: (
        userId: String,
        changedAvatar: Observable<Bool>,
        controller: AccountAPIController
        )) {
        
        self.controller = inputUpdateProfileAvatar.controller
        
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        self.signedIn = inputUpdateProfileAvatar.changedAvatar
            .flatMapLatest { changedAvatar in
                return inputUpdateProfileAvatar.controller.updateProfileAvatar(userId: inputUpdateProfileAvatar.userId, lastChangedAvatar:changedAvatar, tracker: signingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
        
    }
    
    init(inputUpdateProfileDefaultProject: (
        userId: String,
        projectId: Observable<String>,
        controller: AccountAPIController
        )) {
        
        self.controller = inputUpdateProfileDefaultProject.controller
        
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        self.signedIn = inputUpdateProfileDefaultProject.projectId
            .flatMapLatest { projectId in
                return inputUpdateProfileDefaultProject.controller.updateProfileDefaultProject(userId: inputUpdateProfileDefaultProject.userId, projectId:projectId, tracker: signingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
        
    }
    
    init(inputUpdateProfileActiveState: (
        userId: Observable<String>,
        state: Bool,
        controller: AccountAPIController
        )) {
        
        self.controller = inputUpdateProfileActiveState.controller
        
        //
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        //
        self.signedIn = inputUpdateProfileActiveState.userId
            .flatMapLatest { userId in
                return inputUpdateProfileActiveState.controller.updateProfileActiveState(userId: userId, state:inputUpdateProfileActiveState.state, tracker: signingIn)
            }
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
        
    }
    
    init(inputGetUserInfo: (
        userId: Observable<String>,
        controller: AccountAPIController
        )) {
        self.controller = inputGetUserInfo.controller
        
        let signingIn = ActivityIndicator()
        self.signingIn = signingIn.asObservable()
        
        signedIn = inputGetUserInfo.userId
            .flatMapLatest{ userId -> Observable<Event<Account>> in
                return inputGetUserInfo.controller.getUserInfo(tracker: signingIn)
            }
            .map { accountInfo in
                if(accountInfo.isCompleted == false){
                    if let account = accountInfo.element{
                        return LoginResult.ok(output: account)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: accountInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
        
    }
    
}
