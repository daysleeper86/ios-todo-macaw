//
//  UserViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

class NotificationAPIViewModel {
    // Input
    var notificationAPIController: NotificationAPIController?
    // Input
    var processingIn: Observable<Bool>?
    var processFinished: Observable<LoginResult>?
    
    init(inputReadNotification: (
        projectId: String,
        notificationId: String,
        loginTaps: Observable<Void>,
        notificationAPIController: NotificationAPIController
        )) {
        self.notificationAPIController = inputReadNotification.notificationAPIController
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputReadNotification.loginTaps
            .flatMapLatest {
                return inputReadNotification.notificationAPIController.readNotifition(projectId: inputReadNotification.projectId, notificationId: inputReadNotification.notificationId, tracker: processingIn)
        }
    }
    
    init(inputReadAllNotification: (
        projectId: String,
        loginTaps: Observable<Void>,
        notificationAPIController: NotificationAPIController
        )) {
        self.notificationAPIController = inputReadAllNotification.notificationAPIController
        //
        let processingIn = ActivityIndicator()
        self.processingIn = processingIn.asObservable()
        //
        processFinished = inputReadAllNotification.loginTaps
            .flatMapLatest {
                return inputReadAllNotification.notificationAPIController.readAllNotification(projectId: inputReadAllNotification.projectId, tracker: processingIn)
        }
    }
}
