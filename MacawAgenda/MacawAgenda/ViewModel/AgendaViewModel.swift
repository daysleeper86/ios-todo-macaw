//
//  AgendaViewModel.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class AgendaViewModel {
    // Inputs
    fileprivate let agendaController: AgendaController
    
    // Result
    var agendas: Observable<[Agenda]>?
    static let emptyAgenda = AgendaService.createEmptyAgenda()
    
    init(inputUserId: (
        agendaController: AgendaController,
        userId: Observable<String>,
        projectId: Observable<String>,
        duedate: Observable<Date>,
        needSubscrible: Bool
        )) {
        self.agendaController = inputUserId.agendaController
        
        let userAndDate =
            Observable
                .combineLatest(inputUserId.userId, inputUserId.duedate,inputUserId.projectId) { (userId: $0, duedate: $1, projectId: $2) }
                .distinctUntilChanged { (old: (userId: String, duedate: Date, projectId: String),
                    new: (userId: String, duedate: Date, projectId: String)) in
                    return old.userId == new.userId && old.duedate.dayMonthYearString() == new.duedate.dayMonthYearString() && old.projectId == new.projectId
        }
        
        self.agendas =
            userAndDate
                .flatMapLatest { userId, duedate, projectId -> Observable<[Agenda]> in
                    if(projectId.count > 0 && userId.count > 0){
                        if(inputUserId.needSubscrible == true){
                            return self.agendaController.subscribleAllAgendaById(projectId: projectId,userId:userId,duedate:duedate)
                                .retry(3)
                                .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                                .startWith([]) // clears results on new search term
                                .catchErrorJustReturn([])
                        }
                        else{
                            return self.agendaController.getAllAgendaById(projectId: projectId,userId:userId,duedate:duedate)
                                .retry(3)
                                .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                                .startWith([]) // clears results on new search term
                                .catchErrorJustReturn([])
                        }
                    }
                    else{
                        return Observable.just([])
                    }
        }
    }
    
    init(inputUserTeam: (
        agendaController: AgendaController,
        userId: Observable<[User]>,
        projectId: String,
        duedate: Observable<Date>,
        sortUserId: String,
        otherEmpty: Bool
        )) {
        self.agendaController = inputUserTeam.agendaController
        
        let agendaHolder = AgendaService.createHolderAgenda(numHolder: 1)
        let userAndDate = Observable
            .combineLatest(inputUserTeam.userId, inputUserTeam.duedate)  { (userId: $0, duedate: $1) }
            .distinctUntilChanged {
                (old: (userId: [User], duedate: Date),
                new: (userId: [User], duedate: Date)) in
                return Util.userToString(users: old.userId) == Util.userToString(users: new.userId) && old.duedate == new.duedate
        }
        
        self.agendas = userAndDate
            .flatMapLatest { userId, duedate -> Observable<[Agenda]> in
                if(userId.count > 0){
                    return self.agendaController.subscribleAllAgendaByTeam(projectId:inputUserTeam.projectId, userId:userId,duedate:duedate,sortUserId: inputUserTeam.sortUserId,otherEmpty: inputUserTeam.otherEmpty)
                        .retry(3)
                        .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                        .startWith(agendaHolder) // clears results on new search term
                        .catchErrorJustReturn([])
                }
                else{
                    return Observable.just(agendaHolder)
                }
        }
    }
    
    init(inputProjectInfo: (
        agendaController: AgendaController,
        projectId: String,
        duedate: Observable<Date>
        )) {
        self.agendaController = inputProjectInfo.agendaController
        
        self.agendas = inputProjectInfo.duedate
            .flatMapLatest { duedate -> Observable<[Agenda]> in
                return self.agendaController.subscribleAllTodayAgendaByProject(projectId: inputProjectInfo.projectId, duedate: duedate)
                    .retry(3)
                    .retryOnBecomesReachable([], reachabilityService: Dependencies.sharedDependencies.reachabilityService)
                    .startWith([]) // clears results on new search term
                    .catchErrorJustReturn([])
        }
    }
}
