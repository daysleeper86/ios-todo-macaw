//
//  AgendaController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

struct AgendaController : FirestoreAgendaAPI{
    let db : Firestore
    static let sharedAPI = AgendaController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    //
    func Query_AgendaByDuedate(_ userId: String, _ projectId: String,_ duedate : Date) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "agenda"
        let collection = collectionPrefix + collectionName
        //
//        let utcDate = duedate.dateForUTC(utcOffset:  AppSettings.sharedSingleton.account?.utcOffset ?? 0)
        //
        let iDate = duedate.formatInt()
        let query = db.collection(collection)
            .whereField("tsday", isEqualTo: iDate)
            .whereField("pid", isEqualTo: projectId)
            .whereField("u._id", isEqualTo: userId)
        return query
    }
    
    private func Query_TeamAgendaByDuedate(_ projectId: String,_ duedate : Date) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "agenda"
        let collection = collectionPrefix + collectionName
        //
//        let utcDate = duedate.dateForUTC(utcOffset:  AppSettings.sharedSingleton.account?.utcOffset ?? 0)
        //
        let iDate = duedate.formatInt()
        let query = db.collection(collection)
            .whereField("tsday", isEqualTo: iDate)
            .whereField("pid", isEqualTo: projectId)
        return query
    }
    
    func Query_AgendaByProject(_ projectId: String,_ duedate : Date) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "agenda"
        let collection = collectionPrefix + collectionName
        //
//        let utcDate = duedate.dateForUTC(utcOffset:  AppSettings.sharedSingleton.account?.utcOffset ?? 0)
        //
        let iDate = duedate.formatInt()
        let query = db.collection(collection)
            .whereField("tsday", isEqualTo: iDate)
            .whereField("pid", isEqualTo: projectId)
        return query
    }
    
    private func getAgendaByDuedate (userId: String, projectId: String,duedate : Date) -> Observable<QuerySnapshot>{
        let query = Query_AgendaByDuedate(userId,projectId,duedate)
        return query.rx.getDocuments()
    }
    
    private func getTeamAgendaByDuedate (projectId: String,duedate : Date) -> Observable<QuerySnapshot>{
        let query = Query_TeamAgendaByDuedate(projectId,duedate)
        return query.rx.getDocuments()
    }
    
    
    func getAllAgendaByTeam(projectId : String, userId : [User], duedate : Date, sortUserId : String,otherEmpty: Bool) -> Observable<[Agenda]> {
        return
            getTeamAgendaByDuedate(projectId: projectId, duedate: duedate)
                .flatMapLatest { querySnapshot -> Observable<[Agenda]> in
                    let arrayAgenda = AgendaService.documentToAgenda(querySnapshot: querySnapshot)
                    return AgendaService.filterWithUserId(ids: userId, arrayAgenda: arrayAgenda,date: duedate ,sortUserId: sortUserId, otherEmpty: otherEmpty)
                }
    }
    
    func getAllAgendaById(projectId : String,userId : String, duedate : Date) -> Observable<[Agenda]> {
        return
            getAgendaByDuedate(userId: userId, projectId: projectId, duedate: duedate)
                .flatMapLatest { querySnapshot -> Observable<[Agenda]> in
                    var arrayAgenda = AgendaService.documentToAgenda(querySnapshot: querySnapshot)
                    if(arrayAgenda.count == 0){
                        arrayAgenda.append(AgendaViewModel.emptyAgenda)
                    }
                    return Observable.just(arrayAgenda)
                }
    }
    
    func subscribleAllAgendaByTeam(projectId : String, userId : [User], duedate : Date, sortUserId : String,otherEmpty: Bool) -> Observable<[Agenda]>{
        let query = Query_TeamAgendaByDuedate(projectId,duedate)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleAllAgendaByTeam :")
        LoggerApp.logInfo(userId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Agenda]> in
            let arrayAgenda = AgendaService.documentToAgenda(querySnapshot: querySnapshot)
            return AgendaService.filterWithUserId(ids: userId, arrayAgenda: arrayAgenda,date: duedate ,sortUserId: sortUserId, otherEmpty: otherEmpty)
        }
    }
    
    func subscribleAllAgendaById(projectId : String,userId : String, duedate : Date) -> Observable<[Agenda]>{
        let query = Query_AgendaByDuedate(userId,projectId,duedate)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleAllAgendaById :" + projectId)
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Agenda]> in
            var arrayAgenda = AgendaService.documentToAgenda(querySnapshot: querySnapshot)
            if(arrayAgenda.count == 0){
                arrayAgenda.append(AgendaViewModel.emptyAgenda)
            }
            return Observable.just(arrayAgenda)
        }
    }
    
    func subscribleAllTodayAgendaByProject(projectId : String, duedate : Date) -> Observable<[Agenda]>{
        let query = Query_AgendaByProject(projectId, duedate)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleAllTodayAgendaByProject :" + projectId)
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Agenda]> in
            let arrayAgenda = AgendaService.documentToAgenda(querySnapshot: querySnapshot)
            return Observable.just(arrayAgenda)
        }
    }
}

