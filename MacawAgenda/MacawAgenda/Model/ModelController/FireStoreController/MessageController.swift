//
//  MessageController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class MessageController : FirestoreMessageAPI{
    let db : Firestore
    static let sharedAPI = MessageController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_MessageOfTask(_ taskId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection + "/" + taskId + "/messages")
            .order(by: "ts", descending: true)
        return query
    }
    
    private func Query_MessageOfAgenda(_ roomId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection + "/" + roomId + "/messages")
            .order(by: "ts", descending: true)
        return query
    }
    
    private func getQueryMessageOfTask(taskId: String) -> Observable<QuerySnapshot>{
        let query = Query_MessageOfTask(taskId)
        return query.rx.getDocuments()
    }
    
    private func getQueryMessageOfAgenda(roomId: String) -> Observable<QuerySnapshot>{
        let query = Query_MessageOfAgenda(roomId)
        return query.rx.getDocuments()
    }
    
    func getAllMessageOfTaskId(taskId: String) -> Observable<[Message]> {
        return
            getQueryMessageOfTask(taskId:taskId)
                .flatMapLatest { querySnapshot -> Observable<[Message]> in
                    let arrayMessage = MessageService.documentToMessageTaskRoom(querySnapshot: querySnapshot)
                    return Observable.just(arrayMessage)
        }
    }
    
    func subscribleAllMessageOfTaskId(taskId: String) -> Observable<[Message]>{
        let query = Query_MessageOfTask(taskId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Message]> in
            let arrayMessage = MessageService.documentToMessageTaskRoom(querySnapshot: querySnapshot)
            return Observable.just(arrayMessage)
        }
    }
    
    //Agenda
    func getAllMessageOfAgendaId(roomId: String) -> Observable<[Message]>{
        return
            getQueryMessageOfAgenda(roomId:roomId)
                .flatMapLatest { querySnapshot -> Observable<[Message]> in
                    let arrayMessage = MessageService.documentToMessageAgendaRoom(querySnapshot: querySnapshot)
                    return Observable.just(arrayMessage)
        }
    }
    
    func subscribleAllMessageOfAgendaId(roomId: String) -> Observable<[Message]>{
        let query = Query_MessageOfAgenda(roomId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleAllMessageOfAgendaId :" + roomId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Message]> in
            let arrayMessage = MessageService.documentToMessageAgendaRoom(querySnapshot: querySnapshot)
            return Observable.just(arrayMessage)
        }
    }
    
}


