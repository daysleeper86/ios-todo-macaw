//
//  ProjectController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class NotificationController : FirestoreNotificationAPI{
    
    let db : Firestore
    static let sharedAPI = NotificationController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_NotificationByUserId(_ projectId: String,_ userId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "notifications"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
//            .whereField("project._id", isEqualTo: projectId)
            .whereField("users", arrayContains: userId)
        return query
    }
    
    private func Query_NotificationNumberByUserId(_ projectId: String,_ userId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "notifications"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
//            .whereField("project._id", isEqualTo: projectId)
            .whereField("unread", arrayContains: userId)
        return query
    }
    
    private func getQueryNotificationByUserId(projectId: String,userId: String) -> Observable<QuerySnapshot>{
        let query = Query_NotificationByUserId(projectId,userId)
        return query.rx.getDocuments()
    }
    
    private func getQueryNotificationNumberByUserId(projectId: String,userId: String) -> Observable<QuerySnapshot>{
        let query = Query_NotificationNumberByUserId(projectId,userId)
        return query.rx.getDocuments()
    }
    
    func getNotificationByUserId(projectId: String, userId: String) -> Observable<[NotificationData]> {
        return getQueryNotificationNumberByUserId(projectId:projectId,userId:userId)
            .flatMapLatest { querySnapshot -> Observable<[NotificationData]> in
                let arrayNotification = NotificationService.documentToNotification(querySnapshot: querySnapshot,userId: userId)
                return Observable.just(arrayNotification)
        }
    }
    
    func getNotificationNumberByUserId(projectId: String, userId: String) -> Observable<Counter> {
        LoggerApp.logInfo("getNotificationNumberByUserId :" + userId)
        return getQueryNotificationNumberByUserId(projectId:projectId,userId:userId)
            .flatMapLatest { querySnapshot -> Observable<Counter> in
                let counter = Counter(count: querySnapshot.count, dataType: .normal)
                return Observable.just(counter)
        }
    }
    
    func subscribleNotificationByUserId(projectId: String,userId: String)  -> Observable<[NotificationData]>{
        let query = Query_NotificationByUserId(projectId,userId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleNotificationByUserId :" + userId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[NotificationData]> in
            querySnapshot.documentChanges.forEach { diff in
                if (diff.type == .added) {
                    if(NotificationStored.sharedSingleton.isGetData == true){
                        let notification = NotificationData(jsonDict: diff.document.data(),userId : userId)
                        if(notification.valid() == true){
                            if(notification.createdDate > NotificationStored.sharedSingleton.lastTimeSynced){
                                NotificationStored.sharedSingleton.lastTimeSynced = notification.createdDate
                                //
                                DispatchQueue.main.async {
                                    let dict = NSDictionary(objects: [notification], forKeys: [ModelDataKeyString.COLLECTION_NOTIFICATION as NSCopying])
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_NEW_REALTIME_DATA_NOTIFICATION), object: dict)
                                }
                            }
                        }
                    }
                }
            }
            
            let arrayNotification = NotificationService.documentToNotification(querySnapshot: querySnapshot,userId: userId)
            return Observable.just(arrayNotification)
        }
    }
    
    func subscribleNotificationNumberByUserId(projectId: String, userId: String) -> Observable<Counter> {
        let query = Query_NotificationNumberByUserId(projectId,userId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleNotificationByUserId :" + userId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<Counter> in
            let counter = Counter(count: querySnapshot.count, dataType: .normal)
            return Observable.just(counter)
        }
    }
}


