//
//  AppFirestore.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/19/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseAuth

struct AppFirestore {
    let auth : Auth
    static let sharedAPI = AppFirestore(
        auth: Auth.auth()
    )
    init(auth: Auth) {
        self.auth = auth
    }
    func signIn(username: String, password: String) -> Observable<AuthDataResult>{
        let email = AppURL.APIDomains.PrefixUserName + username
        let salt = email + ".uEjJF8QOun.macaw"
        let hashMD5 = salt.md5Value()
        return auth.rx.signIn(withEmail: email, password: hashMD5)
    }
}


