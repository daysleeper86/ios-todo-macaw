//
//  ProjectController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class ProjectController : FirestoreProjectAPI{
    let db : Firestore
    static let sharedAPI = ProjectController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_ProjectById(_ projectId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "projects"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("_id", isEqualTo: projectId)
        return query
    }
    
    private func Query_ProjectByUserId(_ userId: String,_ orangnizationId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "projects"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("usercheck", arrayContains: userId)
            .whereField("oid", isEqualTo: orangnizationId)
//            .order(by: "ts", descending: true)
        return query
    }
    
    private func getQueryProjectById(projectId: String) -> Observable<QuerySnapshot>{
        let query = Query_ProjectById(projectId)
        return query.rx.getDocuments()
    }
    
    func getProjectById(projectId: String) -> Observable<[Project]> {
        return
            getQueryProjectById(projectId:projectId)
                .flatMapLatest { querySnapshot -> Observable<[Project]> in
                    let arrayProject = ProjectService.documentToProject(querySnapshot: querySnapshot)
                    return Observable.just(arrayProject)
        }
    }
    
    func getUsersByProjectId(projectId: String) -> Observable<[User]> {
        return
            getQueryProjectById(projectId:projectId)
                .flatMapLatest { querySnapshot -> Observable<[User]> in
                    var users : [User] = []
                    //
                    let arrayProject = ProjectService.documentToProject(querySnapshot: querySnapshot)
                    if(arrayProject.count > 0){
                        let firstProject = arrayProject[0]
                        if(firstProject.dataType == .normal){
                            users = firstProject.users
                        }
                    }
                    return Observable.just(users)
        }
    }
    
    func subscribleProjectById(projectId: String) -> Observable<[Project]>{
        let query = Query_ProjectById(projectId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Project]> in
            let arrayProject = ProjectService.documentToProject(querySnapshot: querySnapshot)
            return Observable.just(arrayProject)
        }
    }
    
    func subscribleUsersByProjectId(projectId: String) -> Observable<[User]>{
        let query = Query_ProjectById(projectId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleUsersByProjectId :" + projectId)
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[User]> in
            var users : [User] = []
            //
            let arrayProject = ProjectService.documentToProject(querySnapshot: querySnapshot)
            if(arrayProject.count > 0){
                let firstProject = arrayProject[0]
                if(firstProject.dataType == .normal){
                    users = firstProject.users
                }
//                //Update user info if need
//                for i in 0..<users.count{
//                    let user = users[i]
//                    if let userTeam = AppSettings.sharedSingleton.userTeam{
//                        for user1 in userTeam{
//                            if(user.userId == user1.userId){
//                                if(user.name != user1.name){
//                                    users[i] = user1
//                                    break
//                                }
//                            }
//                        }
//                    }
//                }
//                //
            }
            return Observable.just(users)
        }
    }
    
    func subscribleProjectByUserId(userId: String,orangnizationId: String) -> Observable<[Project]>{
        let query = Query_ProjectByUserId(userId,orangnizationId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleProjectByUserId :" + userId)
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Project]> in
            let arrayProject = ProjectService.documentToProject(querySnapshot: querySnapshot)
            return Observable.just(arrayProject)
        }
    }
}


