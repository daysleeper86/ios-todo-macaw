//
//  MessageController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class ActivityController : FirestoreActivityAPI{
    let db : Firestore
    static let sharedAPI = ActivityController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_ActivityOfRoom(_ roomId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "activities"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("rid", isEqualTo: roomId)
        return query
    }
    
    private func getQueryMessageOfTask(roomId: String) -> Observable<QuerySnapshot>{
        let query = Query_ActivityOfRoom(roomId)
        return query.rx.getDocuments()
    }
    
    func getAllActivityOfRoomId(roomId: String) -> Observable<[Activity]> {
        return
            getQueryMessageOfTask(roomId:roomId)
                .flatMapLatest { querySnapshot -> Observable<[Activity]> in
                    let arrayActivity = ActivityService.documentToActivity(querySnapshot: querySnapshot)
                    return Observable.just(arrayActivity)
        }
    }
    
    func subscribleAllActivityOfRoomId(roomId: String) -> Observable<[Activity]>{
        let query = Query_ActivityOfRoom(roomId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleAllActivityOfRoomId :" + roomId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Activity]> in
            let arrayActivity = ActivityService.documentToActivity(querySnapshot: querySnapshot)
            return Observable.just(arrayActivity)
        }
    }
}


