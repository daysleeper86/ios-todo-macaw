//
//  MessageController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class RoomController : FirestoreRoomAPI{
    let db : Firestore
    static let sharedAPI = RoomController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_RoomOfAgenda(projectId: String, userId: String, duedate : Date) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        //
//        let utcDate = duedate.dateForUTC(utcOffset:  AppSettings.sharedSingleton.account?.utcOffset ?? 0)
        let iDate = duedate.formatInt()
        let query = db.collection(collection)
            .whereField("usercheck", arrayContains: userId)
            .whereField("pid", isEqualTo: projectId)
            .whereField("t", isEqualTo: Constant.RoomTypes.agenda)
            .whereField("tsday", isEqualTo: iDate)
        return query
    }
    
    private func getQueryRoomOfAgenda(projectId: String,userId: String,duedate : Date) -> Observable<QuerySnapshot>{
        let query = Query_RoomOfAgenda(projectId:projectId,userId:userId,duedate:duedate)
        return query.rx.getDocuments()
    }
    
    func getRoomOfAgenda(projectId: String,_ userId: String,_ duedate : Date) -> Observable<[Room]> {
        return
            getQueryRoomOfAgenda(projectId:projectId,userId:userId,duedate:duedate)
                .flatMapLatest { querySnapshot -> Observable<[Room]> in
                    let arrayRoom = RoomService.documentToRoom(querySnapshot: querySnapshot)
                    return Observable.just(arrayRoom)
        }
    }
    
    func subscribleRoomOfAgenda(projectId: String,_ userId: String,_ duedate : Date) -> Observable<[Room]>{
        let query = Query_RoomOfAgenda(projectId:projectId,userId:userId,duedate:duedate)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleRoomOfAgenda :" + userId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Room]> in
            let arrayRoom = RoomService.documentToRoom(querySnapshot: querySnapshot)
            return Observable.just(arrayRoom)
        }
    }
}


