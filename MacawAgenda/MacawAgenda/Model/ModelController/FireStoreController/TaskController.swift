//
//  TaskController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class TaskController : FirestoreTaskAPI{
    let db : Firestore
    static let sharedAPI = TaskController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    func Query_AllTask(_ userId: String, _ projectId: String,_ search : String?,_ duedate : Date?) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        
        var query = db.collection(collection)
//            .whereField("usercheck", arrayContains: userId)
            .whereField("pid", isEqualTo: projectId)
            .whereField("t", isEqualTo: Constant.RoomTypes.task)
        if(search!.count > 0){
//            query = query.whereField("name", isGreaterThan: search ??  "")
            query = query.whereField("completed", isEqualTo: false)
        }
        else{
            query = query.whereField("completed", isEqualTo: false)
        }
        if(duedate != nil){
            query = query.whereField("duedate", isEqualTo: duedate!)
        }
        return query
    }
    
    func Query_TaskFilter(_ userId: String, _ projectId: String,_ filter : Constant.TaskFilter.TaskID) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        
        var query = db.collection(collection)
            .whereField("pid", isEqualTo: projectId)
            .whereField("t", isEqualTo: Constant.RoomTypes.task)
        
        if(filter == .myTask){
            query = query.whereField("assignee", arrayContains: userId)
        }
        else if(filter == .priorityTask){
//            query = query.whereField("usercheck", arrayContains: userId)
//                .whereField("priority", isEqualTo: true)
            query = query.whereField("priority", isEqualTo: 1)
        }
        else if(filter == .completedTask){
//            query = query.whereField("usercheck", arrayContains: userId)
//                .whereField("completed", isEqualTo: true)
            query = query.whereField("completed", isEqualTo: true)
        }
        else if(filter == .myUncompletedTask){
            query = query.whereField("assignee", arrayContains: userId)
                .whereField("completed", isEqualTo: false)
        }
        else if(filter == .allUncompletedTask){
            query = query.whereField("completed", isEqualTo: false)
        }
        return query
    }
    
    func Query_TaskId(_ taskId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        
        let query = db.collection(collection)
            .whereField("_id", isEqualTo: taskId)
        return query
    }
    
    
    private func getAllTask(userId: String, projectId: String,search : String?,duedate : Date?) -> Observable<QuerySnapshot>{
        let query = Query_AllTask(userId,projectId,search,duedate)
        return query.rx.getDocuments()
    }

    private func getTaskFilter(userId: String, projectId: String,filter : Constant.TaskFilter.TaskID) -> Observable<QuerySnapshot>{
        let query = Query_TaskFilter(userId,projectId,filter)
        return query.rx.getDocuments()
    }
    
    private func getTaskId(taskId: String) -> Observable<QuerySnapshot>{
        let query = Query_TaskId(taskId)
        return query.rx.getDocuments()
    }
    
    
    
    func getAllTasks(projectId: String, userId: String, search: String, sort: Int, duedate : Date?) -> Observable<[Task]> {
        if(sort == Constant.TaskFilter.TaskID.allTask.rawValue){
            return
                getAllTask(userId:userId,projectId:projectId,search:search, duedate:duedate)
                    .flatMapLatest {querySnapshot -> Observable<[Task]> in
                        let arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
                        return TaskService.filterSearch(search: search, arrayTask: arrayTask)
                    }
        }
        else{
            return
                getTaskFilter(userId:userId,projectId:projectId, filter:Constant.TaskFilter.TaskID(rawValue: sort)!)
                    .flatMapLatest { querySnapshot -> Observable<[Task]> in
                        var arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
                        if(arrayTask.count == 0){
                            arrayTask.append(TaskViewModel.emptyTask)
                        }
                        return Observable.just(arrayTask)
                    }
        }
    }
    
    func getTaskById(taskId: String) -> Observable<[Task]> {
        return
            getTaskId(taskId: taskId)
                .flatMapLatest { querySnapshot -> Observable<[Task]> in
                    let arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
                    return Observable.just(arrayTask)
        }
    }
    
    func getTaskByIds(projectId: String, userId: String, ids: [String], findType : Constant.FindType, byMe: Bool,needEmptyData: Bool) -> Observable<[Task]> {
        var iFilter : Constant.TaskFilter.TaskID
        if(byMe == true){
            iFilter = (findType == .findIn ? .myTask : .myUncompletedTask)
        }
        else{
            iFilter = .allUncompletedTask
        }
        return
            getTaskFilter(userId:userId,projectId:projectId, filter:iFilter)
                .flatMapLatest { querySnapshot -> Observable<[Task]> in
                    let arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
                    return TaskService.filterWithIdByAgenda(ids: ids, findType: findType, byMe: byMe, userMe: userId, arrayTask: arrayTask,needEmptyData: needEmptyData)
                }
    }
    
    func subscribleAllTasks(projectId: String, userId: String, search: String, sort: Int, duedate : Date?) -> Observable<[Task]>{
        var query : Query?
        if(sort == Constant.TaskFilter.TaskID.allTask.rawValue){
            query = Query_AllTask(userId,projectId,search,duedate)
        }
        else{
            query = Query_TaskFilter(userId,projectId,Constant.TaskFilter.TaskID(rawValue: sort)!)
        }
        let querySnapshot : Observable<QuerySnapshot> = query!.rx.listen()
        if(sort == Constant.TaskFilter.TaskID.allTask.rawValue){
            return querySnapshot.flatMapLatest {querySnapshot -> Observable<[Task]> in
                querySnapshot.documentChanges.forEach { diff in
                    if (diff.type == .modified) {
                        let task = Task(jsonDict: diff.document.data())
                        if(task.valid() == true){
                            if(task.deactive == 1){
                                DispatchQueue.main.async {
                                    let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DELETE_TASK_ITEM), object: dict)
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TASK_ITEM), object: dict)
                                }
                            }
                        }
                    }
                    else if (diff.type == .removed) {
                        let task = Task(jsonDict: diff.document.data())
                        if(task.valid() == true && task.deactive == 1){
                            DispatchQueue.main.async {
                                let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DELETE_TASK_ITEM), object: dict)
                            }
                        }
                    }
                }
                //
                LoggerApp.logInfo("subscribleAllTasks: " + userId)
                
                var arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
                arrayTask.sort(by: { (firstItem: Task, lastItem: Task) -> Bool in
                    let firstTaskNumber = Int(firstItem.taskNumber) ?? 0
                    let lastTaskNumber = Int(lastItem.taskNumber) ?? 0
                    return firstTaskNumber > lastTaskNumber
                })
                return TaskService.filterSearch(search: search, arrayTask: arrayTask)
            }
        }
        else{
            return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Task]> in
                querySnapshot.documentChanges.forEach { diff in
                    if (diff.type == .modified) {
                        let task = Task(jsonDict: diff.document.data())
                        if(task.valid() == true){
                            DispatchQueue.main.async {
                                let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TASK_ITEM), object: dict)
                            }
                        }
                    }
                    else if (diff.type == .removed) {
                        let task = Task(jsonDict: diff.document.data())
                        if(task.valid() == true){
                            DispatchQueue.main.async {
                                let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DELETE_TASK_ITEM), object: dict)
                            }
                        }
                    }
                }
                //
                LoggerApp.logInfo("subscribleAllTasks: " + userId)
                
                var arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
                if(arrayTask.count == 0){
                    arrayTask.append(TaskViewModel.emptyTask)
                }
                else{
                    arrayTask.sort(by: { (firstItem: Task, lastItem: Task) -> Bool in
                        let firstTaskNumber = Int(firstItem.taskNumber) ?? 0
                        let lastTaskNumber = Int(lastItem.taskNumber) ?? 0
                        return firstTaskNumber > lastTaskNumber
                    })
                }
                return Observable.just(arrayTask)
            }
        }
    }
    
    func subscribleTaskByIds(projectId: String, userId: String, ids: [String], findType : Constant.FindType, byMe: Bool,needEmptyData: Bool,needSortByTimeStamp: Bool) -> Observable<[Task]> {
        var iFilter : Constant.TaskFilter.TaskID
        if(byMe == true){
            iFilter = (findType == .findIn ? .myTask : .myUncompletedTask)
        }
        else{
            iFilter = .allUncompletedTask
        }
        let query = Query_TaskFilter(userId,projectId,iFilter)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Task]> in
            querySnapshot.documentChanges.forEach { diff in
                if (diff.type == .modified) {
                    let task = Task(jsonDict: diff.document.data())
                    if(task.valid() == true){
                        DispatchQueue.main.async {
                            let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_TASK_ITEM), object: dict)
                        }
                    }
                }
                else if (diff.type == .removed) {
                    let task = Task(jsonDict: diff.document.data())
                    if(task.valid() == true){
                        DispatchQueue.main.async {
                            let dict = NSDictionary(objects: [task], forKeys: [ModelDataKeyString.COLLECTION_TASK as NSCopying])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_DELETE_TASK_ITEM), object: dict)
                        }
                    }
                }
            }
            var arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
            arrayTask.sort(by: { (firstItem: Task, lastItem: Task) -> Bool in
                let firstTaskNumber = Int(firstItem.taskNumber) ?? 0
                let lastTaskNumber = Int(lastItem.taskNumber) ?? 0
                return firstTaskNumber > lastTaskNumber
            })
            LoggerApp.logInfo("subscribleTaskByIds: ")
            LoggerApp.logInfo(ids)
            return TaskService.filterWithIdByAgenda(ids: ids, findType: findType, byMe: byMe, userMe: userId, arrayTask: arrayTask,needEmptyData: needEmptyData)
        }
    }
    
    func subscribleTaskById(taskId: String) -> Observable<[Task]>{
        let query = Query_TaskId(taskId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Task]> in
            let arrayTask = TaskService.documentToTask(querySnapshot: querySnapshot)
            return Observable.just(arrayTask)
        }
    }
}
