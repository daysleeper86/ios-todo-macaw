//
//  ProjectController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class OrangnizationController : FirestoreOrangnizationAPI{
    let db : Firestore
    static let sharedAPI = OrangnizationController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_OrangnizationById(_ orangnizationId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "organizations"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("_id", isEqualTo: orangnizationId)
        return query
    }
    
    private func Query_OrangnizationByUserId(_ userId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "organizations"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("usercheck", arrayContains: userId)
        return query
    }
    
    private func getQueryOrangnizationById(orangnizationId: String) -> Observable<QuerySnapshot>{
        let query = Query_OrangnizationById(orangnizationId)
        return query.rx.getDocuments()
    }
    
    private func getQueryOrangnizationByUserId(userId: String) -> Observable<QuerySnapshot>{
        let query = Query_OrangnizationByUserId(userId)
        return query.rx.getDocuments()
    }
    
    func getOrangnizationById(orangnizationId: String) -> Observable<[Orangnization]> {
        return
            getQueryOrangnizationById(orangnizationId:orangnizationId)
                .flatMapLatest { querySnapshot -> Observable<[Orangnization]> in
                    let arrayOrangnization = OrangnizationService.documentToOrangnization(querySnapshot: querySnapshot)
                    return Observable.just(arrayOrangnization)
        }
    }
    
    func getOrangnizationByUserId(userId: String) -> Observable<[Orangnization]> {
        return
            getQueryOrangnizationByUserId(userId: userId)
                .flatMapLatest { querySnapshot -> Observable<[Orangnization]> in
                    let arrayOrangnization = OrangnizationService.documentToOrangnization(querySnapshot: querySnapshot)
                    return Observable.just(arrayOrangnization)
        }
    }
    
    
    
    
    func subscribleOrangnizationById(orangnizationId: String) -> Observable<[Orangnization]>{
        let query = Query_OrangnizationById(orangnizationId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleOrangnizationById :" + orangnizationId)
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Orangnization]> in
            let arrayOrangnization = OrangnizationService.documentToOrangnization(querySnapshot: querySnapshot)
            return Observable.just(arrayOrangnization)
        }
    }
    
    func subscribleOrangnizationByUserId(userId: String) -> Observable<[Orangnization]>{
        let query = Query_OrangnizationByUserId(userId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleOrangnizationByUserId :" + userId)
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[Orangnization]> in
            let arrayOrangnization = OrangnizationService.documentToOrangnization(querySnapshot: querySnapshot)
            return Observable.just(arrayOrangnization)
        }
    }
}


