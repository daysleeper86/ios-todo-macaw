//
//  MessageController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import RxFirebase
import FirebaseFirestore

class UserController : FirestoreUserAPI{
    let db : Firestore
    static let sharedAPI = UserController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_UserInfo(userId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "users"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("_id", isEqualTo: userId)
        return query
    }
    
    private func getQueryUserInfo(userId: String) -> Observable<QuerySnapshot>{
        let query = Query_UserInfo(userId:userId)
        return query.rx.getDocuments()
    }
    
    func getUserInfo(_ userId: String) -> Observable<User> {
        return
            getQueryUserInfo(userId:userId)
                .flatMapLatest { querySnapshot -> Observable<User> in
                    let arrayUser = UserService.documentToUser(querySnapshot: querySnapshot)
                    let firstUser = arrayUser[0]
                    return Observable.just(firstUser)
        }
    }
    
    func getMyUserInfo(_ userId: String) -> Observable<Account>{
        return
            getQueryUserInfo(userId:userId)
                .flatMapLatest { querySnapshot -> Observable<Account> in
                    let arrayUser = UserService.documentToAccount(querySnapshot: querySnapshot)
                    let firstUser = arrayUser[0]
                    return Observable.just(firstUser)
        }
    }
    
    
    func subscribleUserInfo(_ userId: String) -> Observable<User>{
        let query = Query_UserInfo(userId:userId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<User> in
            querySnapshot.documentChanges.forEach { diff in
                if (diff.type == .modified) {
                    let user = User(jsonDict: diff.document.data())
                    if(user.valid() == true){
                        if(user.checkChangeAvatar() ==  true){
                            Util.resetImageData(user.userId)
                        }
                        //
                        DispatchQueue.main.async {
                            //
                            let dict = NSDictionary(objects: [user], forKeys: [ModelDataKeyString.COLLECTION_USER as NSCopying])
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCenterKey.NOTIFICATION_UPDATE_SIGNLE_USER_INFO), object: dict)
                        }
                    }
                }
            }
            let arrayUser = UserService.documentToUser(querySnapshot: querySnapshot)
            let firstUser = arrayUser[0]
            return Observable.just(firstUser)
        }
    }
    
    func subscribleMyUserInfo(_ userId: String) -> Observable<Account>{
        let query = Query_UserInfo(userId:userId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleMyUserInfo :" + userId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<Account> in
            //
            let arrayUser = UserService.documentToAccount(querySnapshot: querySnapshot)
            let firstUser = arrayUser[0]
            return Observable.just(firstUser)
        }
    }
}


