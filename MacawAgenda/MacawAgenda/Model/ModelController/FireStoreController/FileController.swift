//
//  FileController.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import RxSwift
import RxFirebase
import FirebaseFirestore

class FileController : FirestoreFileAPI{
    let db : Firestore
    static let sharedAPI = FileController(
        db: Firestore.firestore()
    )
    init(db: Firestore) {
        self.db = db
    }
    
    private func Query_FileOfTask(_ taskId: String) -> Query{
        let collectionPrefix = AppURL.APIDomains.PrefixCollection
        let collectionName = "rooms"
        let collection = collectionPrefix + collectionName
        //
        let query = db.collection(collection)
            .whereField("associate", arrayContains: taskId)
//            .whereField("u._id", isEqualTo: AppSettings.sharedSingleton.account?.userId ?? "")
            .whereField("complete", isEqualTo: true)
            .whereField("t", isEqualTo: Constant.RoomTypes.attachment)
        return query
    }
    
    private func getQueryFileOfTask(taskId: String) -> Observable<QuerySnapshot>{
        let query = Query_FileOfTask(taskId)
        return query.rx.getDocuments()
    }
    
    func getAllFileOfTaskId(taskId: String) -> Observable<[File]> {
        return
            getQueryFileOfTask(taskId:taskId)
                .flatMapLatest { querySnapshot -> Observable<[File]> in
                    let arrayFile = FileService.documentToFile(querySnapshot: querySnapshot)
                    return Observable.just(arrayFile)
        }
    }
    
    func subscribleAllFileOfTaskId(taskId: String) -> Observable<[File]>{
        let query = Query_FileOfTask(taskId)
        let querySnapshot : Observable<QuerySnapshot> = query.rx.listen()
        //
        LoggerApp.logInfo("subscribleAllFileOfTaskId :" + taskId)
        return querySnapshot.flatMapLatest { querySnapshot -> Observable<[File]> in
            let arrayFile = FileService.documentToFile(querySnapshot: querySnapshot)
            return Observable.just(arrayFile)
        }
    }
    
}

