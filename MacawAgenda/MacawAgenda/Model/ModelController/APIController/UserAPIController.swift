//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct UserAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Invite User
    func inviteUser(projectId: String,email: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.inviteUser(projectId: projectId,email: email))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = email
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func inviteUserOrangnization(oid: String, projectId: String,email: String,userId: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.inviteUserOrangnization(oid: oid, projectId: projectId, userId: userId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = email
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    //Remove User
    func removeUser(projectId: String,userId: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.removeUser(projectId: projectId,userId: userId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = userId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    //Forget
    func forgetPassword(email: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.forgetPassword(email: email))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = email
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
}


