//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct NotificationAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Read one notification
    func readNotifition(projectId: String,notificationId: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.readNotification(projectId: projectId,notificationId: notificationId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = notificationId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    //Read all notification
    func readAllNotification(projectId: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.readAllNotification(projectId: projectId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }

}


