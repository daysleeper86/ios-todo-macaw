//
//  File.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/18/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Alamofire
import FirebaseFirestore

class DefaultAlamofireManager: Alamofire.SessionManager {
    static let sharedManager: DefaultAlamofireManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 30 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 30 // as seconds, you can set your resource timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return DefaultAlamofireManager(configuration: configuration)
    }()
}

enum AppAPI {
    //Account
    case signIn(username: String, password: String)
    case signInGoogle(email: String,name: String,image: String,userId: String,token: String)
    case signUp(username: String, name: String, password: String)
    case checkEmail(email: String)
    case forgetPassword(email: String)
    case updateSettingUser(userId: String,emailStatus: Bool,notificationStatus: Bool, notificationToken: String)
    case updateProfileName(userId: String,name: String)
    case updateProfileAvatar(userId: String,lastChangedAvatar: Bool)
    case updateProfileDefaultProject(userId: String,projectId: String)
    case updateProfileActiveState(userId: String, active : Bool)
    case changePass(userId: String,currentPass: String, newPass: String)
    //Agenda
    case createAgenda(projectId: String)
    case updateAgendaDonePlanning(projectId: String, agendaId: String, status: Int)
    case updateAgendaAddTask(projectId: String, agendaId: String, taskId: String)
    case updateAgendaRemoveTask(projectId: String, agendaId: String, taskId: String)
    //Project
    case createProject(name: String,teamType: String, teamSize: Int)
    case createOrangnizationProject(oid: String, name: String,teamLeader: String)
    
    case updateProject(projectId: String,name: String)
    //Task
    case createTask(projectId: String, name: String)
    case createTaskAndUpdateAgenda(projectId: String, name: String, agendaId: String)
    case updateTaskInfo(taskId: String,projectId: String, name: String, duedate: String, assignee: String, description: String)
    case updateTaskDeleted(taskId: String,projectId: String, deleted: Bool)
    case updateTaskFinished(taskId: String,projectId: String, finished: Bool)
    case updateTaskPriority(taskId: String,projectId: String, priority: Bool)
    case updateTaskName(taskId: String,projectId: String, name: String)
    case updateTaskDuedate(taskId: String,projectId: String, duedate: Date)
    case updateTaskAssignee(taskId: String,projectId: String, assignee: String)
    case updateTaskDescription(taskId: String,projectId: String, description: String)
    case updateTaskFollower(taskId: String,projectId: String, followerId: String)
    case removeTaskFollower(taskId: String,projectId: String, followerId: String)
    case updateTaskAgendaStatus(taskId: String,projectId: String, agendaId: String, status: Int)
    //Message
    case sendMessageRoomTask(projectId: String, roomId: String,mentions:[String], content: String)
    case sendMessageRoomAgenda(projectId: String, roomId: String,mentions:[String], content: String)
    //Notification
    case readNotification(projectId: String, notificationId: String)
    case readAllNotification(projectId: String)
    //User
    case inviteUser(projectId: String,email: String)
    case inviteUserOrangnization(oid: String, projectId: String,userId: String)
    case removeUser(projectId: String,userId: String)
    case getSetting
    //File
    case sendFileUpload(dictFile: [String:Any])
}

extension AppAPI: TargetType, AccessTokenAuthorizable {
    
    var path: String {
        switch self {
            //Account
        case .signIn(_, _):
            return "/auth/login"
        case .signInGoogle(_, _, _, _, _):
            return "/auth/login/google"
        case .signUp(_, _, _):
            return "/auth/register"
        case .checkEmail(_):
            return "/auth/existsemail"
        case .forgetPassword(_):
            return "/users/password"
        case .updateSettingUser(_,_,_,_),
             .updateProfileName(_,_),
             .updateProfileAvatar(_,_),
             .updateProfileDefaultProject(_,_),
             .updateProfileActiveState(_,_):
            return "/users"
        case .changePass(_):
            return "/users/password"
            //Agenda
        case .createAgenda(_),
             .updateAgendaDonePlanning(_, _, _):
            return "/agendas"
        case .updateAgendaAddTask(_, _, _),
             .createTaskAndUpdateAgenda(_, _, _),
             .updateAgendaRemoveTask(_, _, _),
             .updateTaskAgendaStatus(_, _, _, _):
            return "/agendas/tasks"
            //Project
        case .createProject(_, _, _),
             .updateProject(_, _):
            return "/projects"
            
        case .createOrangnizationProject(_,_,_):
            return "/organizations/projects"
            
            //Task
        case .createTask(_, _),
             .updateTaskDeleted(_, _, _),
             .updateTaskFinished(_, _, _),
             .updateTaskPriority(_, _, _),
             .updateTaskName(_, _, _),
             .updateTaskDuedate(_, _, _),
             .updateTaskAssignee(_, _, _),
             .updateTaskDescription(_, _, _),
             .updateTaskInfo(_, _, _, _, _, _):
            return "/tasks"
        case .updateTaskFollower(_, _, _),
             .removeTaskFollower(_, _, _):
            return "/tasks/followers"
            //Task
        case .sendMessageRoomTask(_, _,_,_),
             .sendMessageRoomAgenda(_, _, _,_):
            return "/messages"
            //Notification
        case .readNotification(_, _),
             .readAllNotification(_):
            return "/notifications"
            //User
        case .inviteUser(_, _),
             .inviteUserOrangnization(_, _,_),
             .removeUser(_, _):
            return "/projects/users"
            
        case .getSetting:
            return "/settings"
        case .sendFileUpload(_):
            return "/uploads"
        }
    }
    
    
    var task: Moya.Task {
        var extra = ["source":"ios","language":"en","key":""]
        //
        switch self {
            //Account
        case .signIn(let username, let password):
            let data = ["email": username,"password": password]
            extra["action"] = "signIn"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .signInGoogle(let username, let name, _, let userId, let token):
            let data : [String:Any] = ["email": username,"name": name,"userId": userId,"gg_access_token": token,"utcOffset": Date.UTCOffset]
            extra["action"] = "signInGoogle"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .signUp(let username, let name, let password):
            let data : [String:Any] = ["email": username,"name": name,"password": password,"utcOffset": Date.UTCOffset]
            extra["action"] = "signUp"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .checkEmail(let email):
            let data = ["email": email]
            extra["action"] = "checkEmail"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .forgetPassword(let email):
            let data = ["email": email]
            extra["action"] = "forgetPassword"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateSettingUser(let userId,let emailStatus,let notificationStatus,let notificationToken):
            var data : [String:Any] = ["_id": userId]
            data["email_notification"] = emailStatus
            data["push_notification"] = notificationStatus
            if(notificationToken.count > 0){
                data["push_token"] = notificationToken
            }
            extra["action"] = "updateSettingUser"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateProfileName(let userId,let name):
            var data : [String:Any] = ["_id": userId]
            data["name"] = name
            let extra = ["action": "updateProfile"]
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateProfileAvatar(let userId,let lastChangedAvatar):
            var data : [String:Any] = ["_id": userId]
            data["lastChangedAvatar"] = lastChangedAvatar
            extra["action"] = "updateProfile"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateProfileDefaultProject(let userId,let projectId):
            var data : [String:Any] = ["_id": userId]
            data["defaultProject"] = projectId
            extra["action"] = "updateProfile"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateProfileActiveState(let userId, let active):
            var data : [String:Any] = ["_id": userId]
            data["mobile_bg"] = active
            extra["action"] = "updateProfile"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .changePass(let userId,let currentPass, let newPass):
            var data : [String:Any] = ["_id":userId,"password":newPass]
            if(currentPass.count > 0){
                data["password_current"] = currentPass
            }
            extra["action"] = "changePass"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            //
        case .createAgenda(let projectId):
//            var data : [String:Any] = ["pid": projectId,"utcOffset": Date.UTCOffset]
            let data = ["pid": projectId]
            extra["action"] = "createAgenda"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateAgendaDonePlanning(let projectId, let agendaId, let status):
            let data : [String:Any] = ["pid": projectId,"agenda_id": agendaId,"status":status]
            extra["action"] = "updateAgendaDonePlanning"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        
        case .updateAgendaAddTask(let projectId, let agendaId, let taskId),
             .updateAgendaRemoveTask(let projectId, let agendaId, let taskId):
            let data : [String:Any] = ["pid": projectId,"agenda_id": agendaId,"task_id":taskId]
            extra["action"] = "updateAgendaAddTask"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
                
        case .createTaskAndUpdateAgenda(let projectId, let name, let agendaId):
            let subData : [String:Any] = ["pid": projectId,"name": name]
            let data : [String:Any] = ["pid": projectId,"agenda_id": agendaId,"task":subData]
            extra["action"] = "createTaskAndUpdateAgenda"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            
            //Project
        case .createProject(let name,let teamType,let teamSize):
            let data : [String:Any] = ["name": name,"ownerRole": teamType,"size": teamSize]
            extra["action"] = "createProject"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            
        case .createOrangnizationProject(let oid,let name,let teamLeader):
            let data : [String:Any] = ["_id": oid,"name": name,"uid": teamLeader]
            extra["action"] = "createOrangnizationProject"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            
        case .updateProject(let projectId,let name):
            let data : [String:Any] = ["_id": projectId,"name": name]
            extra["action"] = "updateProject"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            //Task
        case .createTask(let projectId,let name):
            let data : [String:Any] = ["pid": projectId,"name": name]
            extra["action"] = "createTask"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskFinished(let taskId,let projectId,let finished):
            //status ( 0 = Uncomplete , 1 - WIP , 2 - Finish)
            var status : Int = Constant.TaskStatus.kTaskStatusUncompleted.rawValue
            if(finished == true){
                status = Constant.TaskStatus.kTaskStatusCompleted.rawValue
            }
            let data : [String:Any] = ["_id": taskId,"pid": projectId,"status": status]
            extra["action"] = "updateTaskFinished"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskDeleted(let taskId,let projectId,let deleted):
            var isDel : Int = 0
            if(deleted == true){
                isDel = 1
            }
            let data : [String:Any] = ["_id": taskId,"pid": projectId,"isdel": isDel]
            extra["action"] = "updateTask-Deleted"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskPriority(let taskId,let projectId,let priority):
            var data : [String:Any] = ["_id": taskId,"pid": projectId]
            if(priority == true){
                data["priority"] = 1
            }
            else{
                data["priority"] = 0
            }
            extra["action"] = "updateTask-Priority"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskName(let taskId,let projectId,let name):
            var data : [String:Any] = ["_id": taskId,"pid": projectId]
            data["name"] = name
            extra["action"] = "updateProject-Name"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskDuedate(let taskId,let projectId,let duedate):
            var data : [String:Any] = ["_id": taskId,"pid": projectId]
            data["duedate"] = duedate.currentTimeInMiliseconds()
            extra["action"] = "updateProject-Duedate"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskAssignee(let taskId,let projectId,let assignee):
            var data : [String:Any] = ["_id": taskId,"pid": projectId]
            var arrayAssignee : [String] = []
            if(assignee != Constant.keyHolderUnassignedId){
                arrayAssignee.append(assignee)
            }
            data["assignee"] = arrayAssignee
            extra["action"] = "updateProject-Assignee"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskDescription(let taskId,let projectId,let description):
            var data : [String:Any] = ["_id": taskId,"pid": projectId]
            data["description"] = description
            extra["action"] = "updateProject-Description"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskInfo(let taskId,let projectId,let name,let duedate,let assignee,let description):
            var data : [String:Any] = ["_id": taskId,"pid": projectId]
            if(name.count > 0){
                data["name"] = name
            }
            if(duedate.count > 0){
                data["duedate"] = duedate
            }
            if(assignee.count > 0){
                data["assignee"] = assignee
            }
            if(description.count > 0){
                data["description"] = description
            }
            extra["action"] = "updateProject"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            
        case .updateTaskFollower(let taskId,let projectId,let followerId),
            .removeTaskFollower(let taskId,let projectId,let followerId):
            let data : [String:Any] = ["_id": taskId,"pid": projectId,"followerid": followerId]
            extra["action"] = "updateTaskFollower"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .updateTaskAgendaStatus(let taskId,let projectId,let agendaId, let status):
            //status = 1 -> Task is WIP
            var data : [String:Any] = ["task_id": taskId,"pid": projectId,"agenda_id": agendaId]
            data["task_data"] = ["status": status]
            extra["action"] = "updateTaskAgendaStatus"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            //Message
        case .sendMessageRoomTask(let projectId,let roomId,let mentions,let content):
            let data : [String:Any] = ["rid": roomId,"pid": projectId,"mentions": mentions,"msg": content]
            extra["action"] = "sendMessageRoomTask"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .sendMessageRoomAgenda(let projectId,let roomId,let mentions,let content):
            let data : [String:Any] = ["rid": roomId,"pid": projectId,"mentions": mentions,"msg": content]
            extra["action"] = "sendMessageRoomAgenda"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            //Notification
        case .readNotification(let projectId,let notificationId):
            let data : [String:Any] = ["pid": projectId,"_id": notificationId]
            extra["action"] = "readNotification"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .readAllNotification(let projectId):
//            let data : [String:Any] = ["pid": projectId]
            let data : [String:Any] = [:]
            extra["action"] = "readAllNotification"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            //User
        case .inviteUser(let projectId,let email):
            let data = ["pid": projectId,"email": email]
            extra["action"] = "inviteUser"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .inviteUserOrangnization(let oid, let projectId,let userId):
            let data = ["oid": oid,"pid": projectId,"userId": userId]
            extra["action"] = "inviteUserOrangnization"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
            
        case .removeUser(let projectId,let userId):
            let data = ["pid": projectId,"userId": userId]
            extra["action"] = "removeUser"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        case .getSetting:
            let data : [String:Any] = [:]
            extra["action"] = "getSetting"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: URLEncoding.default)
            
        case .sendFileUpload(let dictFile):
            let data = dictFile
            extra["action"] = "sendFileUpload"
            return .requestParameters(parameters: ["data": data,"extra": extra], encoding: JSONEncoding.default)
        }
    }
    var headers: [String : String]? {
        switch self {
        case .getSetting:
            return [
                "Content-Type": "application/json",
            ]
        default:
            return [
                "Content-Type": "application/json",
            ]
        }
    }
    
    var baseURL: URL {
        return URL(string: AppURL.APIDomains.ServerAPI)!
    }
    
    var method: Moya.Method {
        switch self {
        case .checkEmail(_),
             .forgetPassword(_),
             .signIn(_, _),
             .signInGoogle(_, _, _, _, _),
             .signUp(_, _, _),
             .createProject(_, _, _),
             .createOrangnizationProject(_,_, _),
             .createTask(_, _),
             .sendMessageRoomTask(_, _, _,_),
             .sendMessageRoomAgenda(_, _, _,_),
             .updateTaskFollower(_, _, _),
             .createAgenda(_),
             .updateAgendaAddTask(_, _, _),
             .inviteUser(_, _),
             .sendFileUpload(_):
            return .post
        case .updateTaskInfo(_, _, _, _, _, _),
             .updateTaskDeleted(_, _, _),
             .updateTaskFinished(_, _, _),
             .updateTaskPriority(_, _, _),
             .updateTaskName(_, _, _),
             .updateTaskDuedate(_, _, _),
             .updateTaskAssignee(_, _, _),
             .updateTaskDescription(_, _, _),
             .updateTaskAgendaStatus(_, _, _,_),
             .changePass(_,_, _),
             .updateSettingUser(_,_,_,_),
             .updateProfileName(_, _),
             .updateProfileAvatar(_, _),
             .updateProfileDefaultProject(_, _),
             .updateProfileActiveState(_, _),
             .updateAgendaDonePlanning(_, _, _),
             .readNotification(_,_),
             .readAllNotification(_),
             .updateProject(_, _):
            return .put
        case .removeTaskFollower(_, _, _),
             .updateAgendaRemoveTask(_, _, _),
             .removeUser(_, _):
            return .delete
            
        case .inviteUserOrangnization(_, _,_),
             .createTaskAndUpdateAgenda(_, _, _):
            
            return .patch
        case .getSetting:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .signIn(_, _),
             .signInGoogle(_, _, _, _, _),
             .signUp(_, _, _),
             .changePass(_,_, _),
             .createProject(_, _, _),
             .createOrangnizationProject(_,_, _),
             .inviteUser(_, _),
             .inviteUserOrangnization(_, _,_),
             .updateSettingUser(_,_,_,_),
             .updateProfileName(_, _),
             .updateProfileAvatar(_, _),
             .updateProfileDefaultProject(_, _),
             .updateProfileActiveState(_, _),
             .updateProject(_, _),
             .createTask(_, _),
             .updateTaskInfo(_, _, _, _, _, _),
             .updateTaskDeleted(_, _, _),
             .updateTaskFinished(_, _, _),
             .updateTaskPriority(_, _, _),
             .updateTaskName(_, _, _),
             .updateTaskDuedate(_, _, _),
             .updateTaskAssignee(_, _, _),
             .updateTaskDescription(_, _, _),
             .updateTaskFollower(_, _, _),
             .removeTaskFollower(_, _, _),
             .updateTaskAgendaStatus(_, _, _,_),
             .sendMessageRoomTask(_, _, _,_),
             .sendMessageRoomAgenda(_, _, _,_),
             .createAgenda(_),
             .updateAgendaDonePlanning(_, _, _),
             .updateAgendaAddTask(_, _, _),
             .createTaskAndUpdateAgenda(_, _, _),
             .updateAgendaRemoveTask(_, _, _),
             .readNotification(_,_),
             .readAllNotification(_),
             .removeUser(_, _),
             .sendFileUpload(_):
            return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
        case .checkEmail(_),
             .forgetPassword(_):
            return Util.fromJSONFile("checkAccount")
        case .getSetting:
            return Util.fromJSONFile("getSetting")
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .signIn(_, _),
             .signInGoogle(_, _, _, _, _),
             .signUp(_, _, _),
             .changePass(_,_, _),
             .createProject(_, _, _),
             .createOrangnizationProject(_,_, _),
             .inviteUser(_, _),
             .inviteUserOrangnization(_, _,_),
             .createTask(_, _),
             .updateTaskInfo(_, _, _, _, _, _),
             .updateTaskDeleted(_, _, _),
             .updateTaskFinished(_, _, _),
             .updateTaskPriority(_, _, _),
             .updateTaskName(_, _, _),
             .updateTaskDuedate(_, _, _),
             .updateTaskAssignee(_, _, _),
             .updateTaskDescription(_, _, _),
             .updateTaskFollower(_, _, _),
             .removeTaskFollower(_, _, _),
             .updateTaskAgendaStatus(_, _, _,_),
             .updateSettingUser(_,_,_,_),
             .updateProfileName(_, _),
             .updateProfileAvatar(_, _),
             .updateProfileDefaultProject(_, _),
             .updateProfileActiveState(_, _),
             .updateProject(_, _),
             .sendMessageRoomTask(_, _, _,_),
             .sendMessageRoomAgenda(_, _, _,_),
             .createAgenda(_),
             .updateAgendaDonePlanning(_, _, _),
             .updateAgendaAddTask(_, _, _),
             .createTaskAndUpdateAgenda(_, _, _),
             .updateAgendaRemoveTask(_, _, _),
             .readNotification(_,_),
             .readAllNotification(_),
             .removeUser(_, _),
             .sendFileUpload(_):
            return [
                    "data": [
                        "email": "phuong20190808.21@local.com",
                        "password": "phuong1234"
                    ],
                    "extra": [
                        "action": "hihi"
                    ]
            ]
        case .checkEmail(let email),
             .forgetPassword(let email):
            return ["email": email]
        case .getSetting:
            return [:]
        }
    }
    
    
    var validationType: ValidationType {
        return .none
    }
    
    var authorizationType: AuthorizationType {
        switch self {
        case .changePass,
             .createProject,
             .createOrangnizationProject(_,_, _),
             .updateProject,
             .createTask(_, _),
             .updateTaskInfo(_, _, _, _, _, _),
             .updateTaskDeleted(_, _, _),
             .updateTaskFinished(_, _, _),
             .updateTaskPriority(_, _, _),
             .updateTaskName(_, _, _),
             .updateTaskDuedate(_, _, _),
             .updateTaskAssignee(_, _, _),
             .updateTaskDescription(_, _, _),
             .updateTaskFollower(_, _, _),
             .removeTaskFollower(_, _, _),
             .updateTaskAgendaStatus(_, _, _,_),
             .inviteUser(_, _),
             .inviteUserOrangnization(_, _,_),
             .removeUser(_, _),
             .updateSettingUser(_,_,_,_),
             .updateProfileName(_, _),
             .updateProfileAvatar(_, _),
             .updateProfileDefaultProject(_, _),
             .updateProfileActiveState(_, _),
             .sendMessageRoomTask(_, _, _,_),
             .sendMessageRoomAgenda(_, _, _,_),
             .createAgenda(_),
             .updateAgendaDonePlanning(_, _, _),
             .updateAgendaAddTask(_, _, _),
             .createTaskAndUpdateAgenda(_, _, _),
             .updateAgendaRemoveTask(_, _, _),
             .readNotification(_,_),
             .readAllNotification(_),
             .getSetting,
             .sendFileUpload(_):
            return .bearer
        default:
            return .none
        }
    }
    
}
