//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct TaskAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Create project if need
    func createTask(projectId: String,name: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.createTask(projectId:projectId ,name: name))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func createTaskAndUpdateAgenda(projectId: String,name: String,agendaId: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.createTaskAndUpdateAgenda(projectId: projectId, name: name, agendaId: agendaId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPIDictionary.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskDeleted(taskId: String,projectId: String,deleted: Bool, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskDeleted(taskId:taskId, projectId: projectId, deleted: deleted))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    
    //Update task
    
    func updateTaskAgendaStatus(taskId: String,projectId: String,agendaId: String, status: Int,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskAgendaStatus(taskId:taskId, projectId: projectId,agendaId: agendaId, status: status))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    
    func updateTaskFinished(taskId: String,projectId: String, finished: Bool,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskFinished(taskId:taskId, projectId: projectId, finished: finished))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskPriority(taskId: String,projectId: String, priority: Bool,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskPriority(taskId:taskId, projectId: projectId, priority: priority))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskName(taskId: String,projectId: String, name: String,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskName(taskId:taskId, projectId: projectId, name: name))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskDuedate(taskId: String,projectId: String, duedate: Date,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskDuedate(taskId:taskId, projectId: projectId, duedate: duedate))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskAssignee(taskId: String,projectId: String, assignee: String,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskAssignee(taskId:taskId, projectId: projectId, assignee: assignee))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskDescription(taskId: String,projectId: String, description: String,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskDescription(taskId:taskId, projectId: projectId, description: description))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskFollower(taskId: String,projectId: String, followerId: String,addOrRemove: Int, tracker: ActivityIndicator) -> Observable<LoginResult> {
        var response : Single<Response>?
        if(addOrRemove == 1){
            response = self.provider.rx.request(.updateTaskFollower(taskId:taskId, projectId: projectId, followerId: followerId))
        }
        else{
            response = self.provider.rx.request(.removeTaskFollower(taskId:taskId, projectId: projectId, followerId: followerId))
        }
        return
            response!
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateTaskInfo(taskId: String,projectId: String, name: String, duedate: String, assignee: String, description: String,tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateTaskInfo(taskId:taskId, projectId: projectId, name: name, duedate: duedate, assignee: assignee, description: description))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = taskId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
}

