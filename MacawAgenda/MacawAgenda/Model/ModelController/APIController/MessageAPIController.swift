//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct MessageAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Send message to agenda
    func sendMessageToRoomAgenda(projectId: String,roomId: String,mentions: [String],content: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.sendMessageRoomAgenda(projectId: projectId, roomId: roomId, mentions:mentions,content: content))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = roomId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    
    //Send message to room
    func sendMessageToRoomTask(projectId: String,roomId: String,mentions: [String],content: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.sendMessageRoomTask(projectId: projectId, roomId: roomId, mentions:mentions, content: content))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        response.data = roomId
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
}


