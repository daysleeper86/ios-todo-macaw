//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct ProjectAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Create project if need
    func createProject(name: String,teamType: String,teamSize: Int, tracker: ActivityIndicator) -> Observable<Event<Project>> {
        return self.provider.rx.request(.createProject(name: name,teamType: teamType,teamSize: teamSize))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: Project.self)
            .materialize()
    }
    
    //Create project if need
    func createOrangnizationProject(oid: String,name: String,teamLeader: String, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.createOrangnizationProject(oid: oid, name: name,teamLeader: teamLeader))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    //Update project
    func updateProject(projectId: String, name: String,tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.updateProject(projectId: projectId, name: name))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
}

