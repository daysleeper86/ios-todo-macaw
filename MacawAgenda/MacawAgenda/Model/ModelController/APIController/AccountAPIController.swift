//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct AccountAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Check account
    func checkAccount(email: String, tracker: ActivityIndicator) -> Observable<Event<ResponseAPICheckEmail>> {
        let emailFormated = email.lowercased()
        //
        return self.provider.rx.request(.checkEmail(email: emailFormated))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .mapJSON()
            .mapObject(type: ResponseAPICheckEmail.self)
            .materialize()
    }
    
    //Sign-in normal
    func signIn(email: String,password: String, tracker: ActivityIndicator) -> Observable<Event<Account>> {
        let emailFormated = email.lowercased()
        //
        return self.provider.rx.request(.signIn(username: emailFormated, password: password))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: Account.self)
            .materialize()
    }
    
    //Sign-in Google
    func signInGoogle(email: String,name: String,image: String,userId: String,token: String, tracker: ActivityIndicator) -> Observable<Event<Account>> {
        let emailFormated = email.lowercased()
        //
        return self.provider.rx.request(.signInGoogle(email: emailFormated,name: name,image: image,userId: userId,token: token))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: Account.self)
            .materialize()
    }
    
    //Sign-up normal
    func signUp(email: String,name: String,password: String, tracker: ActivityIndicator) -> Observable<Event<Account>> {
        let emailFormated = email.lowercased()
        //
        return self.provider.rx.request(.signUp(username: emailFormated, name: name, password: password))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: Account.self)
            .materialize()
    }
    
    //Change Password
    func changePass(userId: String,currentPass: String,newPass: String, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.changePass(userId: userId,currentPass: currentPass,newPass: newPass))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    //Change Password
    func updateProfileName(userId: String,name: String, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.updateProfileName(userId: userId, name: name))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    func updateProfileAvatar(userId: String,lastChangedAvatar: Bool, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.updateProfileAvatar(userId: userId,lastChangedAvatar: lastChangedAvatar))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    func updateProfileDefaultProject(userId: String,projectId: String, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.updateProfileDefaultProject(userId: userId,projectId: projectId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    func updateProfileActiveState(userId: String,state: Bool, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.updateProfileActiveState(userId: userId,active: state))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    func updateSettingUser(userId: String,emailStatus: Bool,notificationStatus: Bool,notificationToken: String, tracker: ActivityIndicator) -> Observable<Event<ResponseAPI>> {
        return self.provider.rx.request(.updateSettingUser(userId: userId,emailStatus:emailStatus,notificationStatus: notificationStatus,notificationToken: notificationToken))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
    }
    
    //Get user info
    func getUserInfo(tracker: ActivityIndicator) -> Observable<Event<Account>> {
        return self.provider.rx.request(.getSetting)
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: Account.self)
            .materialize()
    }
}


