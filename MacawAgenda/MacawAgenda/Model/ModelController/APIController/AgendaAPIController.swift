//
//  UserController.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxSwiftUtilities

struct AgendaAPIController {
    
    let provider: MoyaProvider<AppAPI>
    
    init(provider: MoyaProvider<AppAPI>){
        self.provider = provider;
    }
    
    //Create project if need
    func createAgenda(projectId: String, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.createAgenda(projectId:projectId))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPIAgendas.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateAgendaDonePlanning(projectId: String,agendaId: String, status: Int, tracker: ActivityIndicator) -> Observable<LoginResult> {
        return self.provider.rx.request(.updateAgendaDonePlanning(projectId:projectId, agendaId: agendaId, status:status))
            .asObservable()
            .retry(3)
            .trackActivity(tracker)
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .mapObject(type: ResponseAPI.self)
            .materialize()
            .map { responseInfo in
                if(responseInfo.isCompleted == false){
                    if let response = responseInfo.element{
                        return LoginResult.ok(output: response)
                    }
                    else{
                        return Util.parseErrorResult(errorResult: responseInfo.error)
                    }
                }
                else{
                    return LoginResult.ok(output: nil)
                }
        }
    }
    
    func updateAgendaAddOrRemoveTask(projectId: String,agendaId: String, taskId: String,addOrRemove: Int, tracker: ActivityIndicator) -> Observable<LoginResult> {
        var response : Single<Response>?
        if(addOrRemove == 1){
            response = self.provider.rx.request(.updateAgendaAddTask(projectId:projectId, agendaId: agendaId, taskId:taskId))
        }
        else{
            response = self.provider.rx.request(.updateAgendaRemoveTask(projectId:projectId, agendaId: agendaId, taskId:taskId))
        }
        return
            response!
                .asObservable()
                .retry(3)
                .trackActivity(tracker)
                .filterSuccessfulStatusCodes()
                .mapJSON()
                .mapObject(type: ResponseAPI.self)
                .materialize()
                .map { responseInfo in
                    if(responseInfo.isCompleted == false){
                        if let response = responseInfo.element{
                            response.data = taskId
                            return LoginResult.ok(output: response)
                        }
                        else{
                            return Util.parseErrorResult(errorResult: responseInfo.error)
                        }
                    }
                    else{
                        return LoginResult.ok(output: nil)
                    }
        }
        
    }
}

