
//
//  File.swift
//  MacawAgenda
//
//  Created by tranquangson on 10/14/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

class NotificationStored{
    static let sharedSingleton = NotificationStored()
    
    var notifications : [NotificationData] = []
    var isGetData : Bool = false
    var lastTimeSynced : Date = Date()
    
    private init() {
    }
    
    func storeNotifications(_ newNotifications : [NotificationData]){
        NotificationStored.sharedSingleton.notifications = newNotifications
        //
        if(NotificationService.checkHolderNotification(notifications: newNotifications) == false){
            if(isGetData == false){
                NotificationStored.sharedSingleton.isGetData = true
            }
        }
        //
        if(newNotifications.count > 0){
            let lastNotification = newNotifications[newNotifications.count-1]
            NotificationStored.sharedSingleton.lastTimeSynced = lastNotification.createdDate
        }
    }
}
