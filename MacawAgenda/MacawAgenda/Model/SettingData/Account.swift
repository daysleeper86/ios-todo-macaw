//
//  Account.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore
import ObjectMapper

class Account: Mappable {
    var provider: String = ""
    var userId: String = ""
    var token: String = ""
    var name: String = ""
    var email: String = ""
    var userName: String = ""
    var password: String = ""
    var phone: String = ""
    var avatar: String = ""
    var lastChangedAvatar : Date = Date()
    var numberUnread: Int = 0
    var loginSocial: Bool = false
    var loginSocial_ChangePass: Bool = false
    var defaultProject: String = ""
    var utcOffset : Int = 0
    //Setting
    var deviceToken : String = ""
    var emailStatus: Bool = false
    var notificationStatus: Bool = false
    var hasSetting: Bool = false
    var theme : String = "light"
    var isBackground : Bool = false
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        let json = map.JSON
        if let data = json["data"] as? [String:Any] {
            parseDataWithJsonFormat(jsonDict: data)
        }
    }
    
    init(){
        
    }
    
    func parseDataWithJsonFormat(jsonDict: [String: Any]){
        if let userToken = jsonDict["token"] as? String{
            self.token = userToken
        }
        //
        if let userId = jsonDict["_id"] as? String{
            self.userId = userId
        }
        if let userEmails = jsonDict["emails"] as? [[String:Any]]{
            if(userEmails.count > 0){
                let firstItem = userEmails[0]
                if let email = firstItem["address"] as? String{
                    self.email = email
                }
            }
        }
        if let name = jsonDict["name"] as? String{
            self.name = name
        }
        if let userName = jsonDict["username"] as? String{
            self.userName = userName
        }
        if let services = jsonDict["services"] as? [String:Any]{
            if let password = services["password"] as? [String:Any]{
                if let bcrypt = password["bcrypt"] as? String{
                    self.password = bcrypt
                }
            }
        }
        if let defaultProject = jsonDict["defaultProject"] as? String{
            self.defaultProject = defaultProject
        }
        if let utcOffset = jsonDict["utcOffset"] as? Int{
            self.utcOffset = utcOffset
        }
        if let timestamp = jsonDict["lastChangedAvatar"] as? Timestamp{
            lastChangedAvatar = timestamp.dateValue()
        }
        //
        if let email_notification = jsonDict["email_notification"] as? Bool{
            emailStatus = email_notification
            //
            hasSetting = true
        }
        if let push_notification = jsonDict["push_notification"] as? Bool{
            notificationStatus = push_notification
            //
            hasSetting = true
        }
        if let settings = jsonDict["settings"] as? [String:Any]{
            if let theme = settings["theme"] as? String{
                self.theme = theme
            }
        }
    }
    
    init(jsonDict: [String: Any]) {
        parseDataWithJsonFormat(jsonDict: jsonDict)
    }
}

extension Account {
    //Task validation
    func valid() -> Bool {
        if(self.userId.count == 0 || self.name.count == 0){
            return false
        }
        return true
    }
}


