//
//  Setting.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import KeychainAccess

class AppSettings{
    static let sharedSingleton = AppSettings()
    
    var account : Account?
    var orangnization : Orangnization?
    var project : Project?
    var taskAgendaIds : [[String:Any]]?
    var userTeam : [User]?
    var agenda_today : Agenda?
    var agenda_yesterday : Agenda?
    
    private init() {
        let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
        //Setting
        let projectId = UserDefaults.standard.string(forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_ID_KEY) ?? nil
        let projectName = UserDefaults.standard.string(forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_NAME_KEY) ?? nil
        let projectTeamType = UserDefaults.standard.string(forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_TEAMTYPE_KEY) ?? nil
        let projectTeamSize = UserDefaults.standard.integer(forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_TEAMSIZE_KEY)
        let ownedId = UserDefaults.standard.string(forKey: SettingsProjectKeyString.MY_SETTING_OWNER_ID_KEY) ?? nil
        let ownedName = UserDefaults.standard.string(forKey: SettingsProjectKeyString.MY_SETTING_OWNER_NAME_KEY) ?? nil
        let projectPlanType = try? keychain.get(SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_TYPE_KEY)
        let projectMaxInvite = try? keychain.get(SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_INVITE_KEY)
        let projectUTCOffset = UserDefaults.standard.integer(forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_UTC_OFFSET)
        
        if(projectId != nil){
            let project = Project(projectId: projectId ?? "", name: projectName ?? "", dataType: .normal)
            project.teamType = projectTeamType ?? ""
            project.teamSize = projectTeamSize
            //User
            var user = User(userId: ownedId ?? "", dataType: .normal)
            user.name = ownedName ?? ""
            project.owned = user
            //
            if let valueProjectPlanType = projectPlanType{
                if let iPlanType = Int(valueProjectPlanType){
                    project.projectUserPlanType = Constant.ProjectUserPlanType(rawValue: iPlanType) ?? .kProjectUserPlanFree
                }
            }
            if let valueProjectMaxInvite = projectMaxInvite{
                if let iMaxInvite = Int(valueProjectMaxInvite){
                    project.projectUserMaxInvite = iMaxInvite
                }
            }
            project.utcOffset = (projectUTCOffset == 0 ? Constant.UTCTimes.Vn : Constant.UTCTimes.Ireland)
            //
            self.project = project
        }
        
        //Account
        let userId = try? keychain.get(UserInfoKeyString.MY_USERID_KEY)
        let token = try? keychain.get(UserInfoKeyString.MY_TOKEN_KEY_API)
        let name = try? keychain.get(UserInfoKeyString.MY_NAME_KEY)
        let password = try? keychain.get(UserInfoKeyString.MY_PASSWORD_KEY)
        let deviceToken = try? keychain.get(UserInfoKeyString.MY_DEVICE_TOKEN_KEY)
        
        let email = UserDefaults.standard.string(forKey: UserInfoKeyString.MY_EMAIL_KEY)
        let lastChangeAvatar = UserDefaults.standard.object(forKey: UserInfoKeyString.MY_LASTCHANGE_AVATAR_KEY) as? Date
        var emailStatus = true
        if(UserDefaults.standard.object(forKey: UserInfoKeyString.MY_IS_EMAIL_STATUS_KEY) != nil){
            emailStatus = UserDefaults.standard.bool(forKey: UserInfoKeyString.MY_IS_EMAIL_STATUS_KEY)
        }
        var notificationStatus = true
        if(UserDefaults.standard.object(forKey: UserInfoKeyString.MY_IS_NOTIFICATION_STATUS_KEY) != nil){
            notificationStatus = UserDefaults.standard.bool(forKey: UserInfoKeyString.MY_IS_NOTIFICATION_STATUS_KEY)
        }
        let numberUnread = UserDefaults.standard.integer(forKey: UserInfoKeyString.MY_NUMBER_UNREAD_NOTIFICATION)
        let utcOffset = UserDefaults.standard.integer(forKey: UserInfoKeyString.MY_UTC_OFFSET)
        
        let theme = UserDefaults.standard.string(forKey: UserInfoKeyString.MY_THEME_KEY)
        
        if(userId != nil){
            let account = Account()
            account.userId = userId ?? ""
            account.token = token ?? ""
            account.name = name ?? ""
            account.email = email ?? ""
            account.numberUnread = numberUnread
            account.utcOffset = utcOffset
            account.lastChangedAvatar = lastChangeAvatar ?? Date()
            account.password = password ?? ""
            //
            account.emailStatus = emailStatus
            account.notificationStatus = notificationStatus
            account.deviceToken = deviceToken ?? ""
            account.theme = theme ?? ""
            self.account = account
        }
        
        //
        let orangnizationId = UserDefaults.standard.string(forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_ID_KEY) ?? nil
        let orangnizationName = UserDefaults.standard.string(forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_NAME_KEY) ?? nil
        let orangnizationPlanType = UserDefaults.standard.string(forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_PLAN_TYPE_KEY) ?? nil
        let orangnizationMaxInvite = UserDefaults.standard.string(forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_MAX_INVITE_KEY) ?? nil
        if(orangnizationId != nil){
            let orangnization = Orangnization(orangnizationId: orangnizationId ?? "", orangnizationName: orangnizationName ?? "", dataType: .normal)
            if let valueProjectPlanType = orangnizationPlanType{
                if let iPlanType = Int(valueProjectPlanType){
                    orangnization.orangnizationUserPlanType = Constant.ProjectUserPlanType.init(rawValue: iPlanType) ?? .kProjectUserPlanFree
                }
            }
            if let valueProjectMaxInvite = orangnizationMaxInvite{
                if let iMaxInvite = Int(valueProjectMaxInvite){
                    orangnization.orangnizationUserMaxInvite = iMaxInvite
                }
            }
            //
            self.orangnization = orangnization
        }
    }
    
    func saveOrangnization(_ orangnization : Orangnization?)
    {
        AppSettings.sharedSingleton.orangnization = orangnization
        //
        let userDefaults = UserDefaults.standard
        
        var orangnizationId : String?
        var orangnizationName : String?
        var orangnizationPlanType : Int?
        var orangnizationMaxUser : Int?
        
        if(AppSettings.sharedSingleton.orangnization != nil)
        {
            orangnizationId = AppSettings.sharedSingleton.orangnization!.orangnizationId
            orangnizationName = AppSettings.sharedSingleton.orangnization!.orangnizationName
            orangnizationPlanType = AppSettings.sharedSingleton.orangnization!.orangnizationUserPlanType.rawValue
            orangnizationMaxUser = AppSettings.sharedSingleton.orangnization!.orangnizationUserMaxInvite
        }
        else{
            orangnizationId = nil
            orangnizationName = nil
            orangnizationPlanType = nil
            orangnizationMaxUser = nil
        }
        
        userDefaults.set(orangnizationId, forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_ID_KEY)
        userDefaults.set(orangnizationName, forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_NAME_KEY)
        userDefaults.set(orangnizationPlanType, forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_PLAN_TYPE_KEY)
        userDefaults.set(orangnizationMaxUser, forKey: SettingsOrangnizationKeyString.MY_SETTING_ORANGNIZATION_MAX_INVITE_KEY)
    }
    
    func saveProject(_ project : Project?)
    {
        AppSettings.sharedSingleton.project = project
        //
        let userDefaults = UserDefaults.standard
        
        var projectId : String?
        var projectName : String?
        var projectTeamType : String?
        var projectTeamSize : Int?
        var ownedId : String?
        var ownedName : String?
        let projectPlanType : String?
        let projectMaxInvite : String?
        var projectUTCOffset : Int?
        
        if(AppSettings.sharedSingleton.project != nil)
        {
            projectId = AppSettings.sharedSingleton.project!.projectId
            projectName = AppSettings.sharedSingleton.project!.name
            projectTeamType = AppSettings.sharedSingleton.project!.teamType
            projectTeamSize = AppSettings.sharedSingleton.project!.teamSize
            ownedId = AppSettings.sharedSingleton.project!.owned.userId
            ownedName = AppSettings.sharedSingleton.project!.owned.name
            projectPlanType = String(AppSettings.sharedSingleton.project!.projectUserPlanType.rawValue)
            projectMaxInvite = String(AppSettings.sharedSingleton.project!.projectUserMaxInvite)
            projectUTCOffset = AppSettings.sharedSingleton.project!.utcOffset
        }
        else{
            projectId = nil
            projectName = nil
            projectTeamType = nil
            projectTeamSize = 0
            ownedId = nil
            ownedName = nil
            projectPlanType = nil
            projectMaxInvite = nil
            projectUTCOffset = 0
        }
        
        userDefaults.set(projectId, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_ID_KEY)
        userDefaults.set(projectName, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_NAME_KEY)
        userDefaults.set(projectTeamType, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_TEAMTYPE_KEY)
        userDefaults.set(projectTeamSize, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_TEAMSIZE_KEY)
        userDefaults.set(ownedId, forKey: SettingsProjectKeyString.MY_SETTING_OWNER_ID_KEY)
        userDefaults.set(ownedName, forKey: SettingsProjectKeyString.MY_SETTING_OWNER_NAME_KEY)
        userDefaults.set(projectUTCOffset, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_UTC_OFFSET)
        userDefaults.synchronize()
        
        //Security
        let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
        do {
            if(projectPlanType != nil){
                try keychain.set(projectPlanType!, key: SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_TYPE_KEY)
            }
            else{
                try keychain.remove(SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_TYPE_KEY)
            }
            //
            if(projectMaxInvite != nil){
                try keychain.set(projectMaxInvite!, key: SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_INVITE_KEY)
            }
            else{
                try keychain.remove(SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_TYPE_KEY)
                try keychain.remove(SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_INVITE_KEY)
            }
        }
        catch let error {
            print("Failed saveProject: " + error.localizedDescription)
        }
    }
    
    func saveAccount(_ account : Account?){
        AppSettings.sharedSingleton.account = account
        //
        if(AppSettings.sharedSingleton.account != nil)
        {
            //Security
            let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
            do {
                if let userId = AppSettings.sharedSingleton.account?.userId{
                    if(userId.count > 0){
                        try keychain.set(userId, key: UserInfoKeyString.MY_USERID_KEY)
                    }
                }
                if let userToken = AppSettings.sharedSingleton.account?.token{
                    if(userToken.count > 0){
                        try keychain.set(userToken, key: UserInfoKeyString.MY_TOKEN_KEY_API)
                    }
                }
                if let userName = AppSettings.sharedSingleton.account?.name{
                    if(userName.count > 0){
                        try keychain.set(userName, key: UserInfoKeyString.MY_NAME_KEY)
                    }
                }
                if let userPassword = AppSettings.sharedSingleton.account?.password{
                    if(userPassword.count > 0){
                        try keychain.set(userPassword, key: UserInfoKeyString.MY_PASSWORD_KEY)
                    }
                }
                if let userDeviceToken = AppSettings.sharedSingleton.account?.deviceToken{
                    if(userDeviceToken.count > 0){
                        try keychain.set(userDeviceToken, key: UserInfoKeyString.MY_DEVICE_TOKEN_KEY)
                    }
                }
                
            }
            catch let error {
                print("Failed saveAccount: " + error.localizedDescription)
            }
            //Normal
            let userDefaults = UserDefaults.standard
            if let userEmail = AppSettings.sharedSingleton.account?.email{
                if(userEmail.count > 0){
                    userDefaults.set(userEmail, forKey: UserInfoKeyString.MY_EMAIL_KEY)
                }
            }
            if let userNumberUnread = AppSettings.sharedSingleton.account?.numberUnread{
                if(userNumberUnread > 0){
                    userDefaults.set(userNumberUnread, forKey: UserInfoKeyString.MY_NUMBER_UNREAD_NOTIFICATION)
                }
            }
            if let userUTCOffset = AppSettings.sharedSingleton.account?.utcOffset{
                if(userUTCOffset > 0){
                    userDefaults.set(userUTCOffset, forKey: UserInfoKeyString.MY_UTC_OFFSET)
                }
            }
            if let userLastChangedAvatar = AppSettings.sharedSingleton.account?.lastChangedAvatar{
                userDefaults.set(userLastChangedAvatar, forKey: UserInfoKeyString.MY_LASTCHANGE_AVATAR_KEY)
            }
            if let theme = AppSettings.sharedSingleton.account?.theme{
                userDefaults.set(theme, forKey: UserInfoKeyString.MY_THEME_KEY)
            }
            userDefaults.synchronize()
        }
        else{
            //Security
            let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
            do {
                try keychain.remove(UserInfoKeyString.MY_USERID_KEY)
                try keychain.remove(UserInfoKeyString.MY_TOKEN_KEY_API)
                try keychain.remove(UserInfoKeyString.MY_NAME_KEY)
                try keychain.remove(UserInfoKeyString.MY_PASSWORD_KEY)
                try keychain.remove(UserInfoKeyString.MY_DEVICE_TOKEN_KEY)
            }
            catch let error {
                print("Failed saveAccount: " + error.localizedDescription)
            }
            //Normal
            let userDefaults = UserDefaults.standard
            userDefaults.set(nil, forKey: UserInfoKeyString.MY_EMAIL_KEY)
            userDefaults.set(nil, forKey: UserInfoKeyString.MY_TEAMNAME_KEY)
            userDefaults.set(0, forKey: UserInfoKeyString.MY_NUMBER_UNREAD_NOTIFICATION)
            userDefaults.set(0, forKey: UserInfoKeyString.MY_UTC_OFFSET)
            userDefaults.set(nil, forKey: UserInfoKeyString.MY_LASTCHANGE_AVATAR_KEY)
            userDefaults.set(nil, forKey: UserInfoKeyString.MY_THEME_KEY)
            userDefaults.synchronize()
        }
    }
    
    func saveMiscAccount(_ numberUnread : Int?, name: String?, password: String?, deviceToken: String?){
        if(AppSettings.sharedSingleton.account != nil){
            if let userName = name{
                AppSettings.sharedSingleton.account!.name = userName
                let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
                do {
                    if(userName.count > 0){
                        try keychain.set(userName, key: UserInfoKeyString.MY_NAME_KEY)
                    }
                }
                catch let error {
                    print("Failed saveMiscAccount - userName : " + error.localizedDescription)
                }
            }
            if let userPassword = password{
                AppSettings.sharedSingleton.account!.password = userPassword
                let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
                do {
                    if(userPassword.count > 0){
                        try keychain.set(userPassword, key: UserInfoKeyString.MY_PASSWORD_KEY)
                    }
                }
                catch let error {
                    print("Failed saveMiscAccount - userPassword : " + error.localizedDescription)
                }
            }
            if let userNumberUnread = numberUnread{
                AppSettings.sharedSingleton.account!.numberUnread = userNumberUnread
                //Normal
                let userDefaults = UserDefaults.standard
                userDefaults.set(userNumberUnread, forKey: UserInfoKeyString.MY_NUMBER_UNREAD_NOTIFICATION)
                userDefaults.synchronize()
            }
            
            if let userDeviceToken = deviceToken{
                AppSettings.sharedSingleton.account!.deviceToken = userDeviceToken
                //Normal
                let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
                do {
                    if(userDeviceToken.count > 0){
                        try keychain.set(userDeviceToken, key: UserInfoKeyString.MY_DEVICE_TOKEN_KEY)
                    }
                }
                catch let error {
                    print("Failed saveMiscAccount - userDeviceToken : " + error.localizedDescription)
                }
            }
        }
    }
    
    func saveNameUser(_ name: String){
        if(AppSettings.sharedSingleton.account != nil){
            AppSettings.sharedSingleton.account!.name = name
            //Normal
            let userDefaults = UserDefaults.standard
            userDefaults.set(AppSettings.sharedSingleton.account!.name, forKey: UserInfoKeyString.MY_NAME_KEY)
            userDefaults.synchronize()
        }
    }
    
    func saveSettingUser(emailStatus: Bool,notificationStatus: Bool){
        if(AppSettings.sharedSingleton.account != nil){
            AppSettings.sharedSingleton.account!.emailStatus = emailStatus
            AppSettings.sharedSingleton.account!.notificationStatus = notificationStatus
            //Normal
            let userDefaults = UserDefaults.standard
            userDefaults.set(AppSettings.sharedSingleton.account!.emailStatus, forKey: UserInfoKeyString.MY_IS_EMAIL_STATUS_KEY)
            userDefaults.set(AppSettings.sharedSingleton.account!.notificationStatus, forKey: UserInfoKeyString.MY_IS_NOTIFICATION_STATUS_KEY)
            userDefaults.synchronize()
        }
    }
    
    func saveIdProject(_ projectId: String){
        if(AppSettings.sharedSingleton.project != nil){
            AppSettings.sharedSingleton.project!.projectId = projectId
            //Normal
            let userDefaults = UserDefaults.standard
            userDefaults.set(AppSettings.sharedSingleton.project!.projectId, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_ID_KEY)
            userDefaults.synchronize()
        }
    }
    
    func saveNameProject(_ name: String){
        if(AppSettings.sharedSingleton.project != nil){
            AppSettings.sharedSingleton.project!.name = name
            //Normal
            let userDefaults = UserDefaults.standard
            userDefaults.set(AppSettings.sharedSingleton.project!.name, forKey: SettingsProjectKeyString.MY_SETTING_PROJECT_NAME_KEY)
            userDefaults.synchronize()
        }
    }
    
    func saveThemeAccount(_ theme: String){
        if(AppSettings.sharedSingleton.account != nil){
            AppSettings.sharedSingleton.account!.theme = theme
            //Normal
            let userDefaults = UserDefaults.standard
            userDefaults.set(AppSettings.sharedSingleton.account!.theme, forKey: UserInfoKeyString.MY_THEME_KEY)
            userDefaults.synchronize()
        }
    }
    
    func savePlanningTypeProject(_ projectPlanningType: Constant.ProjectUserPlanType){
        if(AppSettings.sharedSingleton.project != nil){
            AppSettings.sharedSingleton.project!.projectUserPlanType = projectPlanningType
            //Security
            let keychain = Keychain(service: AppURL.ExternalKey.KeyChainAccessService)
            do {
                let projectPlanType = String(AppSettings.sharedSingleton.project!.projectUserPlanType.rawValue)
                try keychain.set(projectPlanType, key: SettingsProjectKeyString.MY_SETTING_PROJECT_PLAINING_TYPE_KEY)
            }
            catch let error {
                 print("Failed savePlanningTypeProject : " + error.localizedDescription)
            }
        }
    }
    
    
    
    func saveTaskAgendaIds(_ taskAgendaIds : [[String:Any]]){
        AppSettings.sharedSingleton.taskAgendaIds = taskAgendaIds
    }
    
    func addUserToTeamInfoIfNeed(_ newUser : User){
        if(AppSettings.sharedSingleton.userTeam == nil){
            AppSettings.sharedSingleton.userTeam = []
        }
        //
        var needAdded : Bool = true
        for i in 0..<AppSettings.sharedSingleton.userTeam!.count{
            let user = AppSettings.sharedSingleton.userTeam![i]
            if(user.userId == newUser.userId){
                if(user.name != newUser.name || user.lastChangedAvatar != newUser.lastChangedAvatar){
                    AppSettings.sharedSingleton.userTeam![i] = newUser
                }
                //
                needAdded = false
            }
        }
        if(needAdded == true){
            AppSettings.sharedSingleton.userTeam?.append(newUser)
        }
    }
    
    func saveUserToTeamInfoIfNeed(_ newUsers : [User]){
        AppSettings.sharedSingleton.userTeam = newUsers
    }
    
    func saveTodayAgenda(_ agenda : Agenda){
        AppSettings.sharedSingleton.agenda_today = agenda
    }
    
    func saveYesterdayAgenda(_ agenda : Agenda){
        AppSettings.sharedSingleton.agenda_yesterday = agenda
    }
    
    var isDarkMode : Bool {
        let themeId = AppSettings.sharedSingleton.account?.theme ?? ""
        if(themeId == Constant.ThemeTypes.dark){
            return true
        }
        else{
            return false
        }
    }
}
