//
//  Account.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseAPI: Mappable {
    var status: String = ""
    var code: Int = 200
    var data: String = ""
    var message: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        let json = map.JSON
        if let responseStatus = json["status"] as? String{
            self.status = responseStatus
        }
        if let responseMessage = json["message"] as? String{
            self.message = responseMessage
        }
        if let responseError = json["error"] as? [String:Any]{
            if let responseCode = responseError["code"] as? Int{
                self.code = responseCode
            }
            if let responseMessage = responseError["message"] as? String{
                self.message = responseMessage
            }
        }
        if let responseData = json["data"] as? [String:Any]{
            if let data = responseData["_id"] as? String{
                self.data = data
            }
        }
    }
    
    init(){
        
    }
}

class ResponseAPIDictionary: Mappable {
    var status: String = ""
    var code: Int = 200
    var data: [String: Any] = [:]
    var message: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        let json = map.JSON
        if let responseStatus = json["status"] as? String{
            self.status = responseStatus
        }
        if let responseMessage = json["message"] as? String{
            self.message = responseMessage
        }
        if let responseError = json["error"] as? [String:Any]{
            if let responseCode = responseError["code"] as? Int{
                self.code = responseCode
            }
            if let responseMessage = responseError["message"] as? String{
                self.message = responseMessage
            }
        }
        if let responseData = json["data"] as? [String:Any]{
            if let taskId = responseData["_id"] as? String{
                self.data["taskId"] = taskId
            }
            if let taskNumber = responseData["tasknumber"] as? Int{
                self.data["tasknumber"] = taskNumber
            }
        }
    }
    
    init(){
        
    }
}

class ResponseAPICheckEmail: Mappable {
    var status: String = ""
    var code: Int = 200
    var data: String = ""
    var message: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        let json = map.JSON
        if let responseStatus = json["status"] as? String{
            self.status = responseStatus
        }
        if let responseMessage = json["message"] as? String{
            self.message = responseMessage
        }
        if let responseError = json["error"] as? [String:Any]{
            if let responseCode = responseError["code"] as? Int{
                self.code = responseCode
            }
            if let responseMessage = responseError["message"] as? String{
                self.message = responseMessage
            }
        }
        if let responseData = json["data"] as? [String:Any]{
            if let data = responseData["_id"] as? String{
                self.data = data
            }
            //Invited
            if let invited = responseData["invited"] as? Bool{
                if(invited == true){
                    self.code = 0
                }
            }
        }
    }
    
    init(){
        
    }
}

class ResponseAPIAgendas: Mappable {
    var agenda_today: Agenda = Agenda(agendaId: "", dataType: .empty)
    var agenda_yesterday: Agenda = Agenda(agendaId: "", dataType: .empty)
    var status: String = ""
    var code: Int = 200
    var message: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        let json = map.JSON
        if let responseStatus = json["status"] as? String{
            self.status = responseStatus
        }
        if let responseMessage = json["message"] as? String{
            self.message = responseMessage
        }
        if let responseError = json["error"] as? [String:Any]{
            if let responseCode = responseError["code"] as? Int{
                self.code = responseCode
            }
            if let responseMessage = responseError["message"] as? String{
                self.message = responseMessage
            }
        }
        if let responseData = json["data"] as? [String:Any]{
            var arrayAgendas : [Agenda] = []
            if let prevAgendasData = responseData["prevAgendas"] as? [[String:Any]]{
                for agendaData in prevAgendasData{
                    let agendaItem = Agenda(jsonDict: agendaData)
                    if(agendaItem.valid() == true){
                        arrayAgendas.append(agendaItem)
                    }
                }
            }
            if(arrayAgendas.count == 0){
                let firstAgendaItem = Agenda(jsonDict: responseData)
                if(firstAgendaItem.valid() == true){
                    arrayAgendas.append(firstAgendaItem)
                }
            }
            
            arrayAgendas = AgendaService.sortAgendasByTSDay(agendas: arrayAgendas)
            if(arrayAgendas.count > 0){
                self.agenda_today = arrayAgendas[0]
                if(arrayAgendas.count > 1){
                    self.agenda_yesterday = arrayAgendas[1]
                }
                else{
                    self.agenda_yesterday.date = self.agenda_today.date.yesterday
                    self.agenda_yesterday.dataType = .empty
                }
            }
            else{
                self.agenda_today.dataType = .empty
                //
                self.agenda_yesterday.date = self.agenda_today.date.yesterday
                self.agenda_yesterday.dataType = .empty
            }
        }
    }
    
    init(){
        
    }
}


