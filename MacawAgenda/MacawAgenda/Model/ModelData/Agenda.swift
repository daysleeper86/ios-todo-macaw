//
//  Agenda.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Agenda{
    var agendaId : String = ""
    var tasks: [Task] = []
    var date : Date = Date()
    var tsday : Int = 0
    var owned : User = UserService.createEmptyUser()
    var status : Int = 0
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(agendaId: String,dataType: Constant.DataModelType) {
        self.agendaId = agendaId
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any]) {
        self.agendaId = jsonDict["_id"] as? String ?? ""
        if let taskcheck = jsonDict["taskcheck"] as? [String]{
            for item in taskcheck {
                let task = Task(taskId: item, name: "", dataType: .normal)
                tasks.append(task)
            }
        }
        if let timestamp = jsonDict["ts"] as? Timestamp{
            date = timestamp.dateValue()
        }
        else{
            if let timestamp = jsonDict["ts"] as? String{
                date = Util.dateForRFC3339DateTimeString(rfc3339DateTimeString: timestamp) ?? Date()
            }
        }
        if let tsday = jsonDict["tsday"] as? Int{
            self.tsday = tsday
        }
        if let ownedData = jsonDict["u"] as? [String: Any]{
            let userId = ownedData["_id"] as? String ?? ""
            let name = ownedData["name"] as? String ?? ""
            //
            owned = User(userId: userId, dataType: .normal)
            owned.name = name
        }
        self.status = jsonDict["status"] as? Int ?? 0
    }
}

extension Agenda {
    //Task validation
    func valid() -> Bool {
        if(self.agendaId.count == 0){
            return false
        }
        return true
    }
    
    func empty() -> Bool{
        return !self.valid()
    }
    
    //Check Agenda in array [User]
    func checkOwned(ids : [User]) -> Bool{
        if(ids.count > 0){
            for user in ids {
                if(self.owned.userId == user.userId){
                    return true
                }
            }
        }
        return false
    }
    
    //Check Agenda in array [User]
    func checkExistTask(task : Task) -> Bool{
        if(self.tasks.count > 0){
            for taskItem in self.tasks {
                if(taskItem.taskId == task.taskId){
                    return true
                }
            }
        }
        return false
    }
    
    func getWIPTask() -> Task?{
        var taskResult : Task?
        for task in self.tasks{
            if(task.wip == true){
                taskResult = task
                break
            }
        }
        return taskResult
    }
    
    func checkWIPTask() -> Bool{
        return (self.getWIPTask() != nil)
    }
    
}


