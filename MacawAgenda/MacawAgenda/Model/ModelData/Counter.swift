//
//  Counter.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/25/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

struct Counter{
    var count : Int = 0
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(count: Int, dataType: Constant.DataModelType) {
        self.count = count
        self.dataType = dataType
    }
}
