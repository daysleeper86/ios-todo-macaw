//
//  FileRealm.swift
//  MacawAgenda
//
//  Created by tranquangson on 8/5/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import RealmSwift

class FileRealm : Object{
    @objc dynamic var fileId: String = ""
    @objc dynamic var name : String = ""
    @objc dynamic var file_type : String = ""
    @objc dynamic var local_link : String = ""
    @objc dynamic var path :  String = ""
    @objc dynamic var url :  String = ""
    @objc dynamic var size : Double = 0
    @objc dynamic var uploadedAt : Date = Date()
    @objc dynamic var associate :  String = ""
    @objc dynamic var taskId : String = ""
    //Replece User with params
    @objc dynamic var userId : String = ""
    @objc dynamic var userName : String = ""
    @objc dynamic var userEmail : String = ""
        
    //File manager
    @objc dynamic var progressDownload : Double = 0
    @objc dynamic var progressUpload : Double = 0
    //Type
    @objc dynamic var dataType : Constant.DataModelType = .normal
}
