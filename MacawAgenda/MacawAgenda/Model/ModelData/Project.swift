//
//  Project.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore
import ObjectMapper

class Project{
    var projectId : String = ""
    var name : String = ""
    var teamType : String = ""
    var teamSize : Int = 0
    var projectUserPlanType : Constant.ProjectUserPlanType = .kProjectUserPlanFree
    var projectUserMaxInvite : Int = 0
    var labels : [Label] = []
    var createdTime : Date = Date()
    var users : [User] = []
    var owned : User = User()
    var utcOffset : Int = 0
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(projectId: String, name: String,dataType: Constant.DataModelType) {
        self.projectId = projectId
        self.name = name
        self.dataType = dataType
    }
    
    required init?(map: Map) {
    }
    
    init(jsonDict: [String: Any]) {
        parseDataWithJsonFormat(jsonDict: jsonDict)
    }
    
    func parseDataWithJsonFormat(jsonDict: [String: Any]){
        //
        self.projectId = jsonDict["_id"] as? String ?? ""
        self.name = jsonDict["name"] as? String ?? ""
        self.teamType = jsonDict["ownerRole"] as? String ?? ""
        self.teamSize = jsonDict["size"] as? Int ?? 0
        if let labelsData = jsonDict["labels"] as? [[String: Any]]{
            for jsonItem in labelsData {
                let label = Label(jsonDict: jsonItem)
                labels.append(label)
            }
        }
        if let timestamp = jsonDict["ts"] as? Timestamp{
            createdTime = timestamp.dateValue()
        }
        else{
            if let timestamp = jsonDict["ts"] as? String{
                let date = Util.dateForRFC3339DateTimeString(rfc3339DateTimeString: timestamp) ?? Date()
                createdTime = date
            }
        }
        if let ownedData = jsonDict["u"] as? [String: Any]{
            let userId = ownedData["_id"] as? String ?? ""
            let name = ownedData["name"] as? String ?? ""
            //
            owned = User(userId: userId, dataType: .normal)
            owned.name = name
        }
        
        if let userData = jsonDict["users"] as? [[String: Any]]{
            for jsonItem in userData {
                let userId = jsonItem["_id"] as? String ?? ""
                let name = jsonItem["name"] as? String ?? ""
                let username = jsonItem["username"] as? String ?? ""
                let email = username
                //
                var user = User(userId: userId, dataType: .normal)
                user.name = name
                user.username = username
                user.email = email
                //
                users.append(user)
            }
        }
        
        if let userPlan = jsonDict["plan"] as? [String: Any]{
            let planType = userPlan["type"] as? Int ?? 0
            self.projectUserPlanType = Constant.ProjectUserPlanType.init(rawValue: planType) ?? .kProjectUserPlanFree
            let maxUsers = userPlan["maxUsers"] as? Int ?? 0
            self.projectUserMaxInvite = maxUsers
        }
        
        if let utcOffset = jsonDict["utcOffset"] as? Int{
            self.utcOffset = utcOffset
        }
        
    }
}

extension Project: Mappable {
    func mapping(map: Map) {
        let json = map.JSON
        if let data = json["data"] as? [String:Any] {
            parseDataWithJsonFormat(jsonDict: data)
        }
    }
}

extension Project {
    //Task validation
    func valid() -> Bool {
        if(self.projectId.count == 0 || self.name.count == 0){
            return false
        }
        return true
    }
    //Get only ids
    func getUserIDs() -> String {
        let ids = Util.userToString(users: self.users)
        return ids
    }
    
    func checkExist(userId: String) -> Bool {
        var exist = false
        if(self.users.count > 0){
            for user in self.users {
                let userIdItem = user.userId
                if(userIdItem == userId){
                    exist = true
                    break;
                }
            }
        }
        return exist
    }
}

