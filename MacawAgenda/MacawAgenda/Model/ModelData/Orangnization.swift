//
//  Orangnization.swift
//  MacawAgenda
//
//  Created by tranquangson on 9/30/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import UIKit
import FirebaseFirestore

class Orangnization {
    var orangnizationId : String = ""
    var orangnizationAvatar : String = ""
    var orangnizationName : String = ""
    var orangnizationUserPlanType : Constant.ProjectUserPlanType = .kProjectUserPlanFree
    var orangnizationUserMaxInvite : Int = 0
    var projects : [Project] = []
    var createdTime : Date?
    var owner : User = UserService.createEmptyUser()
    var users : [User] = []
    
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(orangnizationId: String, orangnizationName: String,dataType: Constant.DataModelType) {
        self.orangnizationId = orangnizationId
        self.orangnizationName = orangnizationName
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any]) {
        parseDataWithJsonFormat(jsonDict: jsonDict)
    }
    
    func parseDataWithJsonFormat(jsonDict: [String: Any]){
        //
        self.orangnizationId = jsonDict["_id"] as? String ?? ""
        self.orangnizationName = jsonDict["name"] as? String ?? ""
        self.orangnizationAvatar = jsonDict["avatar"] as? String ?? ""
        if let projectIds = jsonDict["projectcheck"] as? [String]{
            for projectId in projectIds {
                let project = Project(projectId: projectId, name: "", dataType: .normal)
                projects.append(project)
            }
        }
        if let timestamp = jsonDict["ts"] as? Timestamp{
            createdTime = timestamp.dateValue()
        }
        else{
            if let timestamp = jsonDict["ts"] as? String{
                createdTime = Util.dateForRFC3339DateTimeString(rfc3339DateTimeString: timestamp) ?? Date()
            }
        }
        if let ownedData = jsonDict["u"] as? [String: Any]{
            let userId = ownedData["_id"] as? String ?? ""
            let name = ownedData["name"] as? String ?? ""
            //
            owner = User(userId: userId, dataType: .normal)
            owner.name = name
        }
        if let userData = jsonDict["users"] as? [[String: Any]]{
            for jsonItem in userData {
                let userId = jsonItem["_id"] as? String ?? ""
                let name = jsonItem["name"] as? String ?? ""
                let email = jsonItem["email"] as? String ?? ""
                let username = email
                //
                var user = User(userId: userId, dataType: .normal)
                user.name = name
                user.username = username
                user.email = email
                //
                users.append(user)
            }
        }
        
        if let userPlan = jsonDict["plan"] as? [String: Any]{
            let planType = userPlan["type"] as? Int ?? 0
            self.orangnizationUserPlanType = Constant.ProjectUserPlanType.init(rawValue: planType) ?? .kProjectUserPlanFree
            let maxUsers = userPlan["maxUsers"] as? Int ?? 0
            self.orangnizationUserMaxInvite = maxUsers
        }
    }
}

extension Orangnization {
    //Task validation
    func valid() -> Bool {
        if(self.orangnizationId.count == 0 || self.orangnizationName.count == 0){
            return false
        }
        return true
    }
    //Get only ids
    func getUserIDs() -> String {
        let ids = Util.userToString(users: self.users)
        return ids
    }
}
