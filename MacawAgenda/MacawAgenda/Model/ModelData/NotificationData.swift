//
//  Notification.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore

public struct NotificationData{
    var notificationId : String = ""
    var projectId : String = ""
    var type : String = ""
    var isRead : Bool = true
    var createdDate : Date = Date()
    var owner : User = User()
    var message : Message = MessageService.createEmptyMessage()
    var room = RoomService.createEmptyRoom()
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(notificationId: String,dataType: Constant.DataModelType) {
        self.notificationId = notificationId
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any], userId: String) {
        self.notificationId = jsonDict["_id"] as? String ?? ""
        self.type = jsonDict["t"] as? String ?? ""
        if let timestamp = jsonDict["ts"] as? Timestamp{
            createdDate = timestamp.dateValue()
        }
        else{
            if let timestamp = jsonDict["ts"] as? String{
                createdDate = Util.dateForRFC3339DateTimeString(rfc3339DateTimeString: timestamp) ?? Date()
            }
        }
        self.message = Message(jsonDict: jsonDict, messageType: .notification)
        if let roomData = jsonDict["room"] as? [String: Any]{
            self.room.roomId = roomData["rid"] as? String ?? ""
            self.room.name = roomData["name"] as? String ?? ""
            self.room.roomType = roomData["t"] as? String ?? Constant.RoomTypes.agenda
            self.room.dataType = .normal
        }
        //
        if(self.room.roomId.count == 0){
            if let subData = jsonDict["sub"] as? [String: Any]{
                self.room.roomId = subData["rid"] as? String ?? ""
                self.room.name = subData["name"] as? String ?? ""
                self.room.roomType = subData["t"] as? String ?? Constant.RoomTypes.agenda
                self.room.dataType = .normal
            }
        }
        
        if let projectData = jsonDict["project"] as? [String: Any]{
            self.projectId = projectData["_id"] as? String ?? ""
        }
        //
        if let unreadUsers = jsonDict["unread"] as? [String]{
            self.isRead = self.checkRead(unreadUsers: unreadUsers, myUsers: userId)
        }
        //
        if let userData = jsonDict["u"] as? [String: Any]{
            let userId = userData["_id"] as? String ?? ""
            let name = userData["name"] as? String ?? ""
            //
            self.owner = User(userId: userId, dataType: .normal)
            self.owner.name = name
        }
        else{
            if(self.message.user.userId.count > 0){
                self.owner = self.message.user
            }
        }
    }
}

extension NotificationData {
    //Task validation
    func valid() -> Bool {
        if(self.notificationId.count == 0){
            return false
        }
        return true
    }
    
    func checkRead(unreadUsers : [String], myUsers : String) -> Bool{
        if(unreadUsers.count > 0){
            for userItem in unreadUsers {
                if(userItem == myUsers){
                    return false
                }
            }
        }
        return true
    }
}
