//
//  User.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/13/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxDataSources
import SwiftyJSON
import FirebaseFirestore

struct User : IdentifiableType,Equatable{
    typealias Identity = String
    var identity : Identity { return userId }
    
    var userId : String = ""
    var name : String = ""
    var username : String = ""
    var info : String = ""
    var email : String = ""
    var status : Bool = false
    var lastChangedAvatar : Date = Date()
    var isChangePass: Bool = false
    var isWelcome : Bool = false
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(){
        
    }
    
    init(userId: String,dataType: Constant.DataModelType) {
        self.userId = userId
        self.dataType = dataType
    }
    
    init(userId: String, name: String, company: String, email: String,
         status: Bool, isChangePass: Bool, isWelcome: Bool) {
        self.userId = userId
        self.name = name
        self.email = email
        self.status = status
        self.isChangePass = isChangePass
        self.isWelcome = isWelcome
    }
    
    init(jsonDict: [String: Any]) {
        if let userId = jsonDict["_id"] as? String{
            self.userId = userId
        }
        if let userEmails = jsonDict["emails"] as? [[String:Any]]{
            if(userEmails.count > 0){
                let firstItem = userEmails[0]
                if let email = firstItem["address"] as? String{
                    self.email = email
                }
            }
        }
        if let name = jsonDict["name"] as? String{
            self.name = name
        }
        //
        if let timestamp = jsonDict["lastChangedAvatar"] as? Timestamp{
            lastChangedAvatar = timestamp.dateValue()
        }
    }
}

extension User {
    //Task validation
    func valid() -> Bool {
        if(self.userId.count == 0 || self.name.count == 0){
            return false
        }
        return true
    }
    
    func getUserName() -> String {
        var name = ""
        if let currentUsers = AppSettings.sharedSingleton.userTeam{
            if(currentUsers.count > 0){
                for user in currentUsers {
                    let nameItem = user.userId
                    if(nameItem == self.userId){
                        name = user.name
                        break;
                    }
                }
            }
        }
        else{
            name = self.name
        }
        //
        return name
    }
    
    func checkChangeAvatar() -> Bool{
        var changed = false
        if let currentUsers = AppSettings.sharedSingleton.userTeam{
            if(currentUsers.count > 0){
                for user in currentUsers {
                    if(user.lastChangedAvatar != self.lastChangedAvatar){
                        changed = true
                        break;
                    }
                }
            }
        }
        return changed
    }
}

func ==(lhs: User, rhs:User) -> Bool {
    return lhs.userId == rhs.userId
}


