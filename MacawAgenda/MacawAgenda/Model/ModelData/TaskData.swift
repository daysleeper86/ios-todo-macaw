//
//  Task.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore
import RxDataSources
import ObjectMapper

class TaskData{
    
    var taskId : String = ""
    var name : String = ""
    var taskNumber : String = ""
    var assignee : String = ""
    var completed : Bool = false
    var priority : Bool = false
    var wip : Bool = false
    var duedate : Date?
    var finishdate : Date?
    var description : String = ""
    var users : [User] = []
    //Type
    var dataType : Constant.DataModelType = .normal
    
    required init?(map: Map) {
    }
    
    init(jsonDict: [String: Any]) {
        parseDataWithJsonFormat(jsonDict: jsonDict)
    }
    
    func parseDataWithJsonFormat(jsonDict: [String: Any]){
        //
        self.taskId = jsonDict["_id"] as? String ?? ""
        self.name = jsonDict["name"] as? String ?? ""
        let iTaskNumber = jsonDict["tasknumber"] as? Int ?? 0
        if(iTaskNumber > 0){
            self.taskNumber = String(iTaskNumber)
        }
        else{
            self.taskNumber = jsonDict["tasknumber"] as? String ?? ""
        }
        //
        if let assignees = jsonDict["assignee"] as? [String]{
            if(assignees.count > 0){
                self.assignee = assignees[0]
            }
        }
        if let descriptionTask = jsonDict["description"] as? String{
            description = descriptionTask
        }
        if let timestamp = jsonDict["duedate"] as? Timestamp{
            duedate = timestamp.dateValue()
        }
        if let userData = jsonDict["users"] as? [[String: Any]]{
            for jsonItem in userData {
                let userId = jsonItem["_id"] as? String ?? ""
                let name = jsonItem["name"] as? String ?? ""
                //
                var user = User(userId: userId, dataType: .normal)
                user.name = name
                users.append(user)
            }
        }
        self.completed = jsonDict["completed"] as? Bool ?? false
        if let timestamp = jsonDict["finishdate"] as? Timestamp{
            finishdate = timestamp.dateValue()
        }
        self.priority = jsonDict["priority"] as? Bool ?? false
        self.wip = jsonDict["wip"] as? Bool ?? false
    }
}

extension TaskData: Mappable {
    func mapping(map: Map) {
        let json = map.JSON
        if let data = json["data"] as? [String:Any] {
            parseDataWithJsonFormat(jsonDict: data)
        }
    }
}

extension TaskData {
    //Task validation
    func valid() -> Bool {
        if(self.taskId.count == 0 || self.name.count == 0 || self.taskNumber.count == 0){
            return false
        }
        return true
    }
}


