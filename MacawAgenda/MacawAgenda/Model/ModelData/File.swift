//
//  File.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/24/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore

public class File : NSObject{
    var fileId : String = ""
    var name : String = ""
    var file_type : String = ""
    var local_link : String = ""
    var path :  String = ""
    var url :  String = ""
    var pathURL :  String = ""
    var size : Double = 0
    var uploadedAt : Date = Date()
    var associate :  String = ""
    var taskId : String = ""
    var width : Double = 0
    var height : Double = 0
    var user : User = UserService.createEmptyUser()
    //File manager
    var progressDownload : Double = 0
    var progressUpload : Double = 0
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(fileId: String, name: String,dataType: Constant.DataModelType) {
        self.fileId = fileId
        self.name = name
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any]) {
        //
        self.fileId = jsonDict["_id"] as? String ?? ""
        self.name = jsonDict["name"] as? String ?? ""
        self.file_type = jsonDict["type"] as? String ?? ""
        self.path = jsonDict["path"] as? String ?? ""
        self.url = jsonDict["url"] as? String ?? ""
        if let identifyMap = jsonDict["identify"] as? [String: Any]{
            if let sizeMap = identifyMap["size"] as? [String: Any]{
                let width = sizeMap["width"] as? Double ?? 0
                let height = sizeMap["height"] as? Double ?? 0
                //
                self.width = width
                self.height = height
            }
        }
        self.size = jsonDict["size"] as? Double ?? 0
        if let timestamp = jsonDict["duedate"] as? Timestamp{
            self.uploadedAt = timestamp.dateValue()
        }
        if let associate = jsonDict["associate"] as? [String]{
            if(associate.count > 0){
                self.associate = associate[0]
            }
        }
        if let userData = jsonDict["u"] as? [String: Any]{
            let userId = userData["_id"] as? String ?? ""
            let name = userData["name"] as? String ?? ""
            //
            self.user = User(userId: userId, dataType: .normal)
            self.user.name = name
        }
        if let fireBaseStorage = jsonDict["FireBaseStorage"] as? [String: Any]{
            if let path = fireBaseStorage["path"] as? String{
                self.pathURL = Util.parseURLImageFireStore(path.URLEscapedString)
            }
        }
    }
}

extension File {
    //Task validation
    func valid() -> Bool {
        if(self.fileId.count == 0 || self.name.count == 0){
            return false
        }
        return true
    }
}

