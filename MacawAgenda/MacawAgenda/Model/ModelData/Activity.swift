//
//  Message.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/12/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Activity{
    var activityId : String = ""
    var createdAt : Date = Date()
    var message : Message = MessageService.createEmptyMessage()
    //Type
    var dataType : Constant.DataModelType = .normal
    
    //Inits
    init(activityId: String,dataType: Constant.DataModelType) {
        self.activityId = activityId
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any]) {
        //
        self.activityId = jsonDict["_id"] as? String ?? ""
        self.message = Message(jsonDict: jsonDict, messageType: .agenda)
        if let timestamp = jsonDict["ts"] as? Timestamp{
            self.createdAt = timestamp.dateValue()
        }
        else{
            if let timestamp = jsonDict["ts"] as? String{
                self.createdAt = Util.dateForRFC3339DateTimeString(rfc3339DateTimeString: timestamp) ?? Date()
            }
        }
    }
}

extension Activity {
    //Task validation
    func valid() -> Bool {
        if(self.activityId.count == 0){
            return false
        }
        return true
    }
}
