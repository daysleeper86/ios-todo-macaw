//
//  Message.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/12/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Message{
    var messageId : String = ""
    var content : String = ""
    var oldContent : String = ""
    var createdAt : Date = Date()
    var hidden : Bool = false
    var messageType : Constant.MessageType = .kMessageTypeNoneComment
    var messageHeight : CGFloat = Constant.ChattingView.Buble_TextView_Avatar_MaxHeight
    var user : User = User()
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(messageId: String, content: String, type : Constant.MessageType,dataType: Constant.DataModelType) {
        self.messageId = messageId
        self.content = content
        self.messageType = type
        self.dataType = dataType
        //
        if(self.content.count > 0){
            let height = self.content.getTextSizeWithString(Constant.ChattingView.Width_Info_Message_Frame_Display, Constant.ChattingView.Bubble_Info_Text_Font_Large).height
            self.messageHeight = height
        }
    }
    
    
    init(jsonDict: [String: Any], messageType : Constant.JsonMessageType) {
        //
        if(messageType == .room){
            self.messageId = jsonDict["_id"] as? String ?? ""
            self.content = jsonDict["msg"] as? String ?? ""
            if let timestamp = jsonDict["ts"] as? Timestamp{
                self.createdAt = timestamp.dateValue()
            }
            if let userData = jsonDict["u"] as? [String: Any]{
                let userId = userData["_id"] as? String ?? ""
                let name = userData["name"] as? String ?? ""
                //
                self.user = User(userId: userId, dataType: .normal)
                self.user.name = name
            }
            self.hidden = jsonDict["_hidden"] as? Bool ?? false
            
        }
        else if(messageType == .agenda){
            if let msg  = jsonDict["msg"] as? [String: Any]{
                self.messageId = msg["_id"] as? String ?? ""
                self.content = msg["msg"] as? String ?? ""
                if let timestamp = msg["ts"] as? Timestamp{
                    self.createdAt = timestamp.dateValue()
                }
            }
            if let userData = jsonDict["u"] as? [String: Any]{
                let userId = userData["_id"] as? String ?? ""
                let name = userData["name"] as? String ?? ""
                //
                self.user = User(userId: userId, dataType: .normal)
                self.user.name = name
            }
            self.hidden = jsonDict["_hidden"] as? Bool ?? false
        }
        else if(messageType == .notification){
            //
            if let msg  = jsonDict["msg"] as? [String: Any]{
                self.messageId = msg["_id"] as? String ?? ""
                self.content = msg["msg"] as? String ?? ""
                self.oldContent = msg["oldName"] as? String ?? ""
                if let timestamp = msg["ts"] as? Timestamp{
                    self.createdAt = timestamp.dateValue()
                }
                //
                if let userData = msg["u_dest"] as? [String: Any]{
                    let userId = userData["_id"] as? String ?? ""
                    let name = userData["name"] as? String ?? ""
                    //
                    self.user = User(userId: userId, dataType: .normal)
                    self.user.name = name
                }
                else if let userData = msg["u"] as? [String: Any]{
                    let userId = userData["_id"] as? String ?? ""
                    let name = userData["name"] as? String ?? ""
                    //
                    self.user = User(userId: userId, dataType: .normal)
                    self.user.name = name
                }
            }
            //
            if(self.user.userId.count == 0){
                if let userData = jsonDict["u"] as? [String: Any]{
                    let userId = userData["_id"] as? String ?? ""
                    let name = userData["name"] as? String ?? ""
                    //
                    self.user = User(userId: userId, dataType: .normal)
                    self.user.name = name
                }
            }
            
            self.hidden = jsonDict["_hidden"] as? Bool ?? false
        }
        
        self.messageType = .kMessageTypeText
        if(content.count > 0){
            if(messageType != .notification){
                self.content = Util.formatMessageHTMLContent(content: content)
            }
            //
            let height = self.content.getTextSizeWithString(Constant.ChattingView.Buble_TextView_Avatar_MaxWidth, Constant.ChattingView.Bubble_Info_Text_Font_Large).height
            self.messageHeight = height
        }
    }
}

extension Message {
    //Task validation
    func valid() -> Bool {
        if(self.messageId.count == 0 || self.content.count == 0 || self.hidden == true){
            return false
        }
        return true
    }
}
