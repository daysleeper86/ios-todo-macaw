//
//  Task.swift
//  MacawAgenda
//
//  Created by tranquangson on 6/20/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore
import RxDataSources

struct Task : IdentifiableType,Equatable{
    typealias Identity = String
    var identity : Identity { return taskId }
    
    var taskId : String = ""
    var name : String = ""
    var taskNumber : String = ""
    var assignee : String = ""
    var completed : Bool = false
    var priority : Bool = false
    var isFollowing : Bool = false
    var wip : Bool = false
    var deactive : Int = 0
    var duedate : Date?
    var finishdate : Date?
    var description : String = ""
    var users : [User] = []
    var followers : [User] = []
    //Type
    var dataType : Constant.DataModelType = .normal
    var errorType : Bool = false
    
    init(){
    }
    
    init(taskId: String, name: String,dataType: Constant.DataModelType) {
        self.taskId = taskId
        self.name = name
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any]) {
        //
        self.taskId = jsonDict["_id"] as? String ?? ""
        self.name = jsonDict["name"] as? String ?? ""
        let iTaskNumber = jsonDict["tasknumber"] as? Int ?? 0
        if(iTaskNumber > 0){
            self.taskNumber = String(iTaskNumber)
        }
        else{
            self.taskNumber = jsonDict["tasknumber"] as? String ?? ""
        }
        //
        if let assignees = jsonDict["assignee"] as? [String]{
            if(assignees.count > 0){
                self.assignee = assignees[0]
            }
        }
        if let descriptionTask = jsonDict["description"] as? String{
            description = descriptionTask
        }
        if let timestamp = jsonDict["duedate"] as? Timestamp{
            duedate = timestamp.dateValue()
        }
        if let userData = jsonDict["users"] as? [[String: Any]]{
            for jsonItem in userData {
                let userId = jsonItem["_id"] as? String ?? ""
                let name = jsonItem["name"] as? String ?? ""
                //
                var user = User(userId: userId, dataType: .normal)
                user.name = name
                users.append(user)
            }
        }
        if let userFollowing = jsonDict["followers"] as? [[String: Any]]{
            for jsonItem in userFollowing {
                let userId = jsonItem["_id"] as? String ?? ""
                let name = jsonItem["name"] as? String ?? ""
                //
                var user = User(userId: userId, dataType: .normal)
                user.name = name
                followers.append(user)
            }
        }
        if let isDel = jsonDict["isdel"] as? Int{
            self.deactive = isDel
        }
        if let status = jsonDict["status"] as? Int{
            if(status == Constant.TaskStatus.kTaskStatusCompleted.rawValue){
                self.completed = true
            }
            else{
                self.completed = false
                if(status == Constant.TaskStatus.kTaskStatusWIP.rawValue){
                    self.wip = true
                }
                else{
                    self.wip = false
                }
            }
        }
        if let timestamp = jsonDict["finishdate"] as? Timestamp{
            finishdate = timestamp.dateValue()
        }
        //
        if let priority = jsonDict["priority"] as? Int{
            self.priority = Bool(exactly: priority as NSNumber) ?? false
        }
        else if let priority = jsonDict["priority"] as? Bool{
            self.priority = priority
        }
    }
}

extension Task {
    //Task validation
    func valid() -> Bool {
        if(self.taskId.count == 0 || self.name.count == 0 || self.taskNumber.count == 0){
            return false
        }
        return true
    }
    
    //Get assignee name
    func getAssigneeName() -> String {
        var name = ""
        var currentUsers : [User]?
        if AppSettings.sharedSingleton.userTeam != nil{
            currentUsers = AppSettings.sharedSingleton.userTeam
        }
        else{
            currentUsers = self.users
        }
        if let strongCurrentUsers = currentUsers{
            if(strongCurrentUsers.count > 0){
                for user in strongCurrentUsers {
                    let nameItem = user.userId
                    if(nameItem == self.assignee){
                        name = user.name
                        break;
                    }
                }
            }
        }
        //
        return name
    }
    
    func checkFollowing(myUserId: String) -> Bool {
        var following = false
        if(self.followers.count > 0){
            for user in self.followers {
                let userIdItem = user.userId
                if(userIdItem == myUserId){
                    following = true
                    break;
                }
            }
        }
        return following
    }
    
    //Check in array [Task]
    func checkIn(ids : [String]) -> Bool{
        if(ids.count > 0){
            for id in ids {
                if(id == self.taskId){
                    return true
                }
            }
        }
        return false
    }
    
    func checkIn(tasks : [Task]) -> Task?{
        if(tasks.count > 0){
            for task in tasks {
                if(task.taskId == self.taskId){
                    return task
                }
            }
        }
        return nil
    }
    
    //Check assignee by Id
    func taskBy(id : String) -> Bool{
        return self.assignee == id ? true : false
    }
    
    func checkIsTodayAgenda() -> String{
        var agendaTaskId : String = ""
        if let agendaTaskInfo = AppSettings.sharedSingleton.taskAgendaIds{
            if(agendaTaskInfo.count > 0){
                for agendaTask in agendaTaskInfo{
                    if let taskIds = agendaTask[Constant.keyValue] as? [String], let agendaId = agendaTask[Constant.keyId] as? String{
                        if(self.checkIn(ids: taskIds)){
                            agendaTaskId = agendaId
                            break
                        }
                    }
                }
            }
        }
        return agendaTaskId
    }
    
    func checkAssigneeNotFound() -> Bool{
        var exist : Bool = false
        if let currentUsers = AppSettings.sharedSingleton.userTeam{
            if(currentUsers.count > 0){
                for user in currentUsers {
                    let nameItem = user.userId
                    if(nameItem == self.assignee){
                        exist = true
                        break;
                    }
                }
            }
        }
        return exist
    }
}

func ==(lhs: Task, rhs:Task) -> Bool {
    return lhs.taskId == rhs.taskId
}


