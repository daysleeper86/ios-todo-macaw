//
//  Agenda.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/4/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Room{
    var roomId : String = ""
    var lastMsg: String = ""
    var numMsg: Int = 0
    var name :  String = ""
    var roomType : String = Constant.RoomTypes.agenda
    var createdDate : Date = Date()
    //Type
    var dataType : Constant.DataModelType = .normal
    
    init(roomId: String,dataType: Constant.DataModelType) {
        self.roomId = roomId
        self.dataType = dataType
    }
    
    init(jsonDict: [String: Any]) {
        self.roomId = jsonDict["_id"] as? String ?? ""
        self.lastMsg = jsonDict["lastmsg"] as? String ?? ""
        self.numMsg = jsonDict["msgs"] as? Int ?? 0
        self.name = jsonDict["name"] as? String ?? ""
        self.roomType = jsonDict["t"] as? String ?? ""
        if let timestamp = jsonDict["tsday"] as? Timestamp{
            self.createdDate = timestamp.dateValue()
        }
    }
}

extension Room {
    //Task validation
    func valid() -> Bool {
        if(self.roomId.count == 0){
            return false
        }
        return true
    }
}


