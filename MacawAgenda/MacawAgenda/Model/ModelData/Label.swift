//
//  Label.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation

struct Label{
    var labelId : String = ""
    var name : String = ""
    var color : String = ""
    
    init(labelId: String, name: String) {
        self.labelId = labelId
        self.name = name
    }
    
    init(jsonDict: [String: Any]) {
        //
        self.labelId = jsonDict["_id"] as? String ?? ""
        self.name = jsonDict["label"] as? String ?? ""
        self.color = jsonDict["color"] as? String ?? ""
    }
}
