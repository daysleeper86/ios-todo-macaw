//
//  AgendaService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class AgendaService{
    
    static func sortAgendasById(agendas: [Agenda], userId: String) -> [Agenda]{
        var newAgendas = agendas
        for index in 0..<newAgendas.count{
            let agenda = newAgendas[index]
            if(agenda.owned.userId == userId){
                newAgendas.remove (at: index)
                newAgendas.insert(agenda, at: 0)
                //
                break
            }
        }
        return newAgendas
    }
    
    static func sortAgendasByTSDay(agendas: [Agenda]) -> [Agenda]{
        var newAgendas : [Agenda] = []
        for index in 0 ..< agendas.count{
            var agenda = agendas[index]
            let year = agenda.tsday/10000
            let month = (agenda.tsday % 10000) / 100
            let day = (agenda.tsday % 100)
            if let date = Date.from(year: year, month: month, day: day){
                agenda.date = date
            }
            newAgendas.append(agenda)
        }
        //
        newAgendas.sort(by: { $0.date.compare($1.date) == .orderedDescending })
        return newAgendas
    }
    
    
    static func documentToAgenda(querySnapshot: QuerySnapshot) -> [Agenda] {
        var arrayAgenda : [Agenda] = []
        for document in querySnapshot.documents {
            let agenda = Agenda(jsonDict: document.data())
            if(agenda.valid() == true){
                arrayAgenda.append(agenda)
            }
        }
        return arrayAgenda
    }
    
    static func checkHolderAgenda(agendas : [Agenda]) -> Bool{
        var isHolder : Bool = false
        for agenda in agendas{
            if(agenda.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }
    
    static func filterWithUserId(ids: [User], arrayAgenda : [Agenda],date: Date, sortUserId: String, otherEmpty : Bool) -> Observable<[Agenda]> {
        var arrayResultAgenda : [Agenda] = []
        //
        for var agendaItem in arrayAgenda {
            if(agendaItem.checkOwned(ids: ids) == true){
                agendaItem.date = date
                arrayResultAgenda.append(agendaItem)
            }
        }
        //
        if(otherEmpty == true){
            var idsNotAgenda : [User] = []
            for user in ids {
                var exist : Bool = false
                let n : Int = arrayResultAgenda.count
                for i in  0 ..< n {
                    let agendaItem = arrayResultAgenda[i]
                    if(agendaItem.owned.userId == user.userId){
                        exist = true
                        break;
                    }
                }
                if(exist == false){
                    idsNotAgenda.append(user)
                }
            }
            for user in idsNotAgenda {
                var agenda = Agenda(agendaId: "", dataType: .empty)
                agenda.owned = user
                agenda.date = date
                arrayResultAgenda.append(agenda)
            }
        }
        //
        if(arrayResultAgenda.count == 0){
            arrayResultAgenda.append(AgendaViewModel.emptyAgenda)
        }
        else{
            arrayResultAgenda = AgendaService.sortAgendasById(agendas: arrayResultAgenda, userId: sortUserId)
        }
        //
        return Observable.just(arrayResultAgenda)
    }
    
    static func combineTaskOfAgenda(arrayTask: [[Task]], arrayAgenda : [Agenda]) -> Observable<[Agenda]> {
        var arrayResultAgenda : [Agenda] = arrayAgenda
        if(arrayTask.count > 0){
            for i in 0..<arrayTask.count{
                let tasks = arrayTask[i]
                if(tasks.count > 0){
                    let firstTask = tasks[0]
                    for j in 0..<arrayAgenda.count{
                        var agendaItem = arrayAgenda[j]
                        if(agendaItem.checkExistTask(task: firstTask) == true){
                            agendaItem.tasks = tasks
                            arrayResultAgenda[j] = agendaItem
                            break
                        }
                    }
                }
                
            }
        }
        return Observable.just(arrayResultAgenda)
    }
    
    static func checkAgendaInfoBetweenTwoArray(lhs: [Agenda], rhs: [Agenda]) -> Bool{
        let isHolderLhs = AgendaService.checkHolderAgenda(agendas: lhs)
        let isHolderRhs = AgendaService.checkHolderAgenda(agendas: rhs)
        if(isHolderLhs == true && isHolderRhs == true){
            return true
        }
        else if(isHolderLhs == true || isHolderRhs == true){
            return false
        }
        else{
            if(lhs.count != rhs.count){
                return false
            }
            else{
                for agenda1 in lhs{
                    for agenda2 in rhs{
                        if(agenda1.agendaId == agenda2.agendaId){
                            if(agenda1.tasks.count != agenda2.tasks.count){
                                return false
                            }
                            else{
                                for task in agenda1.tasks {
                                    if task.checkIn(tasks: agenda2.tasks) == nil{
                                        return false
                                    }
                                }
                                
                            }
                        }
                    }
                }
                return true
            }
        }
    }
    
    static func createHolderAgenda(numHolder: Int) -> [Agenda] {
        var agendaHolder : [Agenda] = []
        for i in 1...numHolder{
            let agendaId = String(i)
            let agendaItem = Agenda(agendaId: agendaId, dataType: .holder)
            //
            agendaHolder.append(agendaItem)
        }
        return agendaHolder
    }
    
    static func createEmptyAgenda() -> Agenda {
        return Agenda(agendaId: "", dataType: .empty)
    }
    
}
