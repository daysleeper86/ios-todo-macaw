//
//  ProjectService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class NotificationService{
    static func documentToNotification(querySnapshot: QuerySnapshot, userId: String) -> [NotificationData] {
        var arrayNotification : [NotificationData] = []
        for document in querySnapshot.documents {
            let notification = NotificationData(jsonDict: document.data(),userId : userId)
            if(notification.valid() == true){
                arrayNotification.append(notification)
            }
        }
        //
        if(arrayNotification.count == 0){
            arrayNotification.append(NotificationViewModel.emptyNotification)
        }
        else{
            arrayNotification.sort(by: { $0.createdDate.compare($1.createdDate) == .orderedDescending })
        }
        return arrayNotification
    }
    
    static func formatAttributedString(notification: NotificationData,user: User) -> NSMutableAttributedString{
        let myUserId = user.userId
        var attributedString : NSMutableAttributedString
        //
        var strOwner : String = ""
//        if(notification.message.user.userId == myUserId){
//            strOwner = NSLocalizedString("you", comment: "")
//        }
//        else{
//            strOwner = notification.message.user.name
//        }
        //
        if(notification.owner.userId.count > 0){
            if(notification.owner.userId == myUserId){
                strOwner = NSLocalizedString("you", comment: "")
            }
            else{
                strOwner = notification.owner.name
            }
        }
        else{
            if(notification.message.user.userId == myUserId){
                strOwner = NSLocalizedString("you", comment: "")
            }
            else{
                strOwner = notification.message.user.name
            }
        }
        
        var strType : String = ""
        var strContent = notification.message.content
        var strMessage : String = ""
        
        if(notification.type == NotificationTypeKeyString.NotificationType_AddFile ||
            notification.type == NotificationTypeKeyString.NotificationType_AddTask ||
            notification.type == NotificationTypeKeyString.NotificationType_RemoveComment ||
            notification.type == NotificationTypeKeyString.NotificationType_Other
            ){
            if(notification.type == NotificationTypeKeyString.NotificationType_AddFile){
                strType = NSLocalizedString("add_new_attachment", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_AddTask){
                strType = NSLocalizedString("added_new_task", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_RemoveComment){
                strType = NSLocalizedString("deleted", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_Other){
                strType = NSLocalizedString("deleted", comment: "")
                //
                if(notification.room.name.count > 0){
                    strContent = notification.room.name
                }
            }
            //
            let strTask : String = strContent
            
            strMessage = strOwner + " " + strType + " " + strTask
            
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: strOwner.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: strOwner.count+1, length: strType.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: strOwner.count+strType.count+2, length: strTask.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: strOwner.count+strType.count+2, length: strTask.count))
        }
        else if(notification.type == NotificationTypeKeyString.NotificationType_TaskCompleted ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskUncompleted ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskStatus
            ){
            let strType1 : String = NSLocalizedString("marked", comment: "")
            var strType2 : String = ""
            if(notification.type == NotificationTypeKeyString.NotificationType_TaskCompleted){
                strType2 = NSLocalizedString("as_completed", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_TaskUncompleted){
                strType2 = NSLocalizedString("as_incomplete", comment: "")
            }
            else{
                strType2 = NSLocalizedString("as", comment: "") + " " + strContent
                //
                if(notification.room.name.count > 0){
                    strContent = notification.room.name
                }
            }
            //
            let strTask : String = strContent
            strMessage = strOwner + " " + strType1 + " " + strTask + " " + strType2
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: strOwner.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: strOwner.count+1, length: strType.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: strOwner.count+strType.count+2, length: strTask.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: strOwner.count+strType.count+2, length: strTask.count))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 14) ?? UIFont.systemFont(ofSize: 14), range: NSRange(location: strOwner.count+strType1.count+strTask.count+3, length: strType2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: strOwner.count+strType1.count+strTask.count+3, length: strType2.count))
        }
        else if(notification.type == NotificationTypeKeyString.NotificationType_ChangeTask
            ){
            let strType1 : String = NSLocalizedString("changed_agenda_for", comment: "")
            let strType2 : String = NSLocalizedString("to", comment: "")
            //
            let strTask1 = NSLocalizedString("from", comment: "")
            let strTask2 = NSLocalizedString("to", comment: "")
            //
            strMessage = strOwner + " " + strType1 + " " + strTask1 + " " + strType2 + " " + strTask2
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: strOwner.count))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: strOwner.count+strType1.count+2, length: strTask1.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: strOwner.count+strType1.count+2, length: strTask1.count))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: strOwner.count+strType1.count+strTask1.count+3, length: strType2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: strOwner.count+strType1.count+strTask1.count+3, length: strType2.count))
            
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: strOwner.count+strType1.count+strTask1.count+strType2.count+4, length: strTask2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: strOwner.count+strType1.count+strTask1.count+strType2.count+4, length: strTask2.count))
        }
        else if(notification.type == NotificationTypeKeyString.NotificationType_RenameTask){
            let strType1 : String = NSLocalizedString("re-named", comment: "")
            let strType2 : String = NSLocalizedString("to", comment: "")
            //
            let strTask1 = notification.message.content
            let strTask2 = notification.message.oldContent
            //
            strMessage = strOwner + " " + strType1 + " " + strTask1 + " " + strType2 + " " + strTask2
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: strOwner.count))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: strOwner.count+strType1.count+2, length: strTask1.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: strOwner.count+strType1.count+2, length: strTask1.count))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: strOwner.count+strType1.count+strTask1.count+3, length: strType2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: strOwner.count+strType1.count+strTask1.count+3, length: strType2.count))
            
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: strOwner.count+strType1.count+strTask1.count+strType2.count+4, length: strTask2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: strOwner.count+strType1.count+strTask1.count+strType2.count+4, length: strTask2.count))
        }
        
        else if(notification.type == NotificationTypeKeyString.NotificationType_AddAssignee || notification.type == NotificationTypeKeyString.NotificationType_TaskUnassignee){
            var strType1 : String = ""
            if(notification.type == NotificationTypeKeyString.NotificationType_AddAssignee){
                strType1 = NSLocalizedString("changed_the_assignee_to", comment: "")
            }
            else{
                strType1 = NSLocalizedString("changed_the_assignee_to", comment: "")
            }
            
            let strType2 : String = NSLocalizedString("on", comment: "")
            //
            let myUserName = notification.message.user.name.count > 0 ? notification.message.user.name : user.name
            let strTask1 = myUserName
//            let strTask2 = notification.message.content
            let strTask2 = notification.room.name
            //
            strMessage = strOwner + " " + strType1 + " " + strTask1 + " " + strType2 + " " + strTask2
            //
            var lenghtUserName1 : Int = 0
            var lenghtUserName2 : Int = 0
//            var lenghtFull : Int = 0
            if(strOwner.containsEmoji == false){
                lenghtUserName1 = strOwner.count
            }
            else{
                lenghtUserName1 = strOwner.utf16.count
            }
            //
            if(strTask1.containsEmoji == false){
                lenghtUserName2 = strTask1.count
            }
            else{
                lenghtUserName2 = strTask1.utf16.count
            }
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: lenghtUserName1))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: 0, length: lenghtUserName1))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: lenghtUserName1+strType1.count+2, length: lenghtUserName2))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: lenghtUserName1+strType1.count+2, length: lenghtUserName2))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: lenghtUserName1+strType1.count+lenghtUserName2+3, length: strType2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: lenghtUserName1+strType1.count+lenghtUserName2+3, length: strType2.count))
            
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: lenghtUserName1+strType1.count+lenghtUserName2+strType2.count+4, length: strTask2.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: lenghtUserName1+strType1.count+lenghtUserName2+strType2.count+4, length: strTask2.count))
        }
        else if(notification.type == NotificationTypeKeyString.NotificationType_DoneAgenda ||
            notification.type == NotificationTypeKeyString.NotificationType_CommentAgenda || notification.type == NotificationTypeKeyString.NotificationType_SunmitAgenda){
            if(notification.type == NotificationTypeKeyString.NotificationType_DoneAgenda || notification.type == NotificationTypeKeyString.NotificationType_CompletedAllTasksAgenda){
                strType = NSLocalizedString("done_all_tasks_in_agenda", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_CommentAgenda){
                strType = NSLocalizedString("added_new_comment_to_team_agenda", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_SunmitAgenda){
                strType = NSLocalizedString("has_finish_planning_for_agenda", comment: "")
            }
            //
            let strType1 : String = NSLocalizedString("for", comment: "")
            let strType2 : String = notification.createdDate.dayMonthYearString()
            //
            strMessage = strOwner + " " + strType + " " + strType1 + " " + strType2
            var lenghtUserName1 : Int = 0
            if(strOwner.containsEmoji == false){
                lenghtUserName1 = strOwner.count
            }
            else{
                lenghtUserName1 = strOwner.utf16.count
            }
            
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: lenghtUserName1))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: 0, length: lenghtUserName1))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_REGULAR, size: 15) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: lenghtUserName1+1, length: strType.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLACK_COLOR, range: NSRange(location: lenghtUserName1+1, length: strType.count))
        }
        else if(notification.type == NotificationTypeKeyString.NotificationType_AddMessage || notification.type == NotificationTypeKeyString.NotificationType_commentTask || notification.type == NotificationTypeKeyString.NotificationType_Mention ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskPriority  ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskUnpriority ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskDuedate ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskDescription ||
            notification.type == NotificationTypeKeyString.NotificationType_TaskLabel){
            if(notification.type == NotificationTypeKeyString.NotificationType_Mention){
                strType = NSLocalizedString("mentioned_you_in", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_commentTask){
                strType = NSLocalizedString("added_new_comment_on", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_TaskPriority){
                strType = NSLocalizedString("prioritized", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_TaskUnpriority){
                strType = NSLocalizedString("unprioritized", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_TaskDuedate){
                strType = NSLocalizedString("changed_due_date", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_TaskDescription){
                strType = NSLocalizedString("changed_description_of", comment: "")
            }
            else if(notification.type == NotificationTypeKeyString.NotificationType_TaskLabel){
                strType = NSLocalizedString("added_label_on", comment: "")
            }
            else{
                strType = NSLocalizedString("added_new_comment_on", comment: "")
            }
            
            //
            if(notification.room.name.count > 0){
                strContent = notification.room.name
            }
            let strTask : String = strContent
            strMessage = strOwner + " " + strType + " " + strTask
            //
            var lenghtUserName1 : Int = 0
            var lenghtUserName2 : Int = 0
            if(strOwner.containsEmoji == false){
                lenghtUserName1 = strOwner.count
            }
            else{
                lenghtUserName1 = strOwner.utf16.count
            }
            //
            if(strTask.containsEmoji == false){
                lenghtUserName2 = strTask.count
            }
            else{
                lenghtUserName2 = strTask.utf16.count
            }
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: lenghtUserName1))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: 0, length: lenghtUserName1))
            
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: lenghtUserName1+1, length: strType.count))
            
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: lenghtUserName1+strType.count+2, length: lenghtUserName2))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: lenghtUserName1+strType.count+2, length: lenghtUserName2))
        }
        else{
            strType = NSLocalizedString("added_new_comment_on", comment: "")
            //
            if(notification.room.name.count > 0){
                strContent = notification.room.name
            }
            let strTask : String = strContent
            strMessage = strOwner + " " + strType + " " + strTask
            //
            var lenghtUserName1 : Int = 0
            var lenghtUserName2 : Int = 0
            if(strOwner.containsEmoji == false){
                lenghtUserName1 = strOwner.count
            }
            else{
                lenghtUserName1 = strOwner.utf16.count
            }
            //
            if(strTask.containsEmoji == false){
                lenghtUserName2 = strTask.count
            }
            else{
                lenghtUserName2 = strTask.utf16.count
            }
            //
            attributedString = NSMutableAttributedString(string: strMessage)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 15) ?? UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 0, length: lenghtUserName1))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_ITALIC, size: 15) ?? UIFont.italicSystemFont(ofSize: 15), range: NSRange(location: lenghtUserName1+1, length: strType.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.NormalColors.BLUE_COLOR, range: NSRange(location: lenghtUserName1+strType.count+2, length: lenghtUserName2))
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: FontNames.Lato.MULI_BOLD, size: 14) ?? UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: lenghtUserName1+strType.count+2, length: lenghtUserName2))
        }
        
        return attributedString
    }
    
    static func checkHolderNotification(notifications : [NotificationData]) -> Bool{
        var isHolder : Bool = false
        for notification in notifications{
            if(notification.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }
    
    static func isHasUnreadNotification(notifications : [NotificationData]) -> Bool{
        var isHasUnread : Bool = false
        for notification in notifications{
            if(notification.isRead == false){
                isHasUnread = true
                break
            }
        }
        return isHasUnread
    }
    
    static func createHolderAgenda(numHolder: Int) -> [NotificationData] {
        var notificationHolder : [NotificationData] = []
        for i in 1...numHolder{
            let notificationId = String(i)
            let notificationItem = NotificationData(notificationId: notificationId,dataType: .holder)
            //
            notificationHolder.append(notificationItem)
        }
        return notificationHolder
    }
    
    static func createEmptyNotificationData() -> NotificationData {
        return NotificationData(notificationId: "",dataType: .empty)
    }
    
}

