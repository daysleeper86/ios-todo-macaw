//
//  TaskService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class TaskService{
    static func sortTasksByWIP(tasks: [Task]) -> [Task]{
        var newTasks = tasks
        for index in 0..<newTasks.count{
            let task = newTasks[index]
            if(task.wip == true){
                newTasks.remove (at: index)
                newTasks.insert(task, at: 0)
                //
                break
            }
        }
        return newTasks
    }
    
    static func documentToTask(querySnapshot: QuerySnapshot) -> [Task] {
        var arrayTask : [Task] = []
        for document in querySnapshot.documents {
            let task = Task(jsonDict: document.data())
            if(task.valid() == true && task.deactive == 0){
                arrayTask.append(task)
            }
        }
        return arrayTask
    }
    
    
    static func mockDataToTask(mockData: [[String : Any]]) -> [Task] {
        var arrayTask : [Task] = []
        for mockItem in mockData{
            let task = Task(jsonDict: mockItem)
            if(task.valid() == true){
                arrayTask.append(task)
            }
        }
        return arrayTask
    }
    
    static func countNumComplete(arrayTask : [Task]) -> Int {
        var count : Int = 0
        for task in arrayTask {
            if(task.completed == true){
                count = count + 1
            }
        }
        return count
    }
    
    static func filterSearch(search: String,arrayTask : [Task]) -> Observable<[Task]> {
        var arrayResultTask : [Task] = []
        //
        let isSearch = search.count > 1 ? true : false
        let isValid = Util.isValidTaskNameSearch(search)
        //
        for task in arrayTask {
            if(isSearch == true){
                let name : String?
                if(isValid == true){
                    name = "#" + task.taskNumber
                }
                else{
                    name = task.name
                }
                //
                if(name!.range(of: search, options:.caseInsensitive) != nil){
                    arrayResultTask.append(task)
                }
            }
            else{
                arrayResultTask.append(task)
            }
        }
        //
        if(arrayResultTask.count == 0){
            arrayResultTask.append(TaskViewModel.emptyTask)
        }
        //
        return Observable.just(arrayResultTask)
    }
    
    static func filterWithIdByAgenda(ids: [String], findType : Constant.FindType, byMe: Bool, userMe : String? , arrayTask : [Task], needEmptyData: Bool) -> Observable<[Task]> {
        var arrayResultTask : [Task] = []
        //
        for task in arrayTask {
            if(byMe == true){
                if(findType == .findIn){
                    if(task.checkIn(ids: ids) == true){
                        arrayResultTask.append(task)
                    }
                }
                else{
                    if(task.checkIn(ids: ids) == false){
                        arrayResultTask.append(task)
                    }
                }
            }
            else{
                if(task.taskBy(id: userMe!) == false){
                    arrayResultTask.append(task)
                }
            }
        }
        //
        if(arrayResultTask.count == 0){
            if(needEmptyData == true){
                arrayResultTask.append(TaskViewModel.emptyTask)
            }
        }
        else{
            arrayResultTask = TaskService.sortTasksByWIP(tasks: arrayResultTask)
        }
        //
        return Observable.just(arrayResultTask)
    }
    
    static func checkWIPTask(tasks : [Task]) -> Task?{
        var wipTask : Task?
        for task in tasks{
            if(task.wip == true){
                wipTask = task
                break
            }
        }
        return wipTask
    }
    
    static func checkEmptyTask(tasks : [Task]) -> Bool{
        var isEmpty : Bool = false
        if(tasks.count == 1){
            for task in tasks{
                if(task.dataType == .empty){
                    isEmpty = true
                    break
                }
            }
        }
        return isEmpty
    }
    
    static func checkHolderTask(tasks : [Task]) -> Bool{
        var isHolder : Bool = false
        for task in tasks{
            if(task.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }

    
    static func createHolderTask(numHolder: Int) -> [Task] {
        var taskHolder : [Task] = []
        for i in 1...numHolder{
            let taskId = String(i)
            let taskItem = Task(taskId: taskId, name: "Holder " + taskId, dataType: .holder)
            //
            taskHolder.append(taskItem)
        }
        return taskHolder
    }
    
    static func createEmptyTask() -> Task {
        return Task(taskId: "", name: "", dataType: .empty)
    }
}
