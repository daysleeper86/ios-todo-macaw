//
//  FileService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class FileService{
    static func documentToFile(querySnapshot: QuerySnapshot) -> [File] {
        var arrayFile : [File] = []
        for document in querySnapshot.documents {
            let file = File(jsonDict: document.data())
            if(file.valid() == true){
                arrayFile.append(file)
            }
        }
        return arrayFile
    }
    
    static func checkHolderFile(files : [File]) -> Bool{
        var isHolder : Bool = false
        for file in files{
            if(file.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }
    
    static func createHolderFile(numHolder: Int) -> [File] {
        var fileHolder : [File] = []
        for i in 1...numHolder{
            let fileId = String(i)
            let fileItem = File(fileId: fileId, name: "File " + fileId ,dataType: .holder)
            //
            fileHolder.append(fileItem)
        }
        return fileHolder
    }
    
    
}

