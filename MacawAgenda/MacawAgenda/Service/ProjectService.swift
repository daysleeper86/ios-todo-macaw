//
//  ProjectService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class ProjectService{
    static func documentToProject(querySnapshot: QuerySnapshot) -> [Project] {
        var arrayProject : [Project] = []
        for document in querySnapshot.documents {
            let project = Project(jsonDict: document.data())
            if(project.valid() == true){
                arrayProject.append(project)
            }
        }
        //
        if(arrayProject.count == 0){
            arrayProject.append(ProjectViewModel.emptyProject)
        }
        else{
            arrayProject.sort(by: { $0.createdTime.compare($1.createdTime) == .orderedDescending })
        }
        return arrayProject
    }
    
    static func resortCurrentProject(projects: [Project], projectId: String) -> [Project]{
        var newProjects = projects
        if(newProjects.count == 1){
            let project = newProjects[0]
            if(project.dataType == .empty){
                newProjects.removeAll()
                if let strongCurrentProject = AppDelegate().sharedInstance().currentProject{
                    newProjects.append(strongCurrentProject)
                }
            }
        }
        else{
            for index in 0..<newProjects.count{
                let project = newProjects[index]
                if(project.projectId == projectId){
                    newProjects.remove (at: index)
                    newProjects.insert(project, at: 0)
                    //
                    break
                }
            }
        }
        return newProjects
    }
    
    static func checkEmptyProject(projects : [Project]) -> Bool{
        var isEmpty : Bool = false
        if(projects.count == 1){
            for project in projects{
                if(project.dataType == .empty){
                    isEmpty = true
                    break
                }
            }
        }
        return isEmpty
    }
    
    static func checkHolderProject(projects : [Project]) -> Bool{
        var isHolder : Bool = false
        for project in projects{
            if(project.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }
    
    static func createHolderProject(numHolder: Int) -> [Project] {
        var projectHolder : [Project] = []
        for i in 1...numHolder{
            let projectId = String(i)
            let projectItem = Project(projectId: projectId, name: "Holder " + projectId, dataType: .holder)
            //
            projectHolder.append(projectItem)
        }
        return projectHolder
    }
    
    static func createEmptyProject() -> Project {
        return Project(projectId: "", name: "", dataType: .empty)
    }
}

