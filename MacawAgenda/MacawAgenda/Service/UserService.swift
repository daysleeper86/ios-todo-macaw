//
//  ProjectService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class UserService{
    static let emptyUser = UserService.createEmptyUser()
    static let emptyAccount = Account()
    
    static func documentToUser(querySnapshot: QuerySnapshot) -> [User] {
        var arrayUser : [User] = []
        for document in querySnapshot.documents {
            let user = User(jsonDict: document.data())
            if(user.valid() == true){
                arrayUser.append(user)
            }
        }
        //
        if(arrayUser.count == 0){
            arrayUser.append(UserService.emptyUser)
        }
        return arrayUser
    }
    
    static func documentToAccount(querySnapshot: QuerySnapshot) -> [Account] {
        var arrayAccount : [Account] = []
        for document in querySnapshot.documents {
            let account = Account(jsonDict: document.data())
            if(account.valid() == true){
                arrayAccount.append(account)
            }
        }
        //
        if(arrayAccount.count == 0){
            arrayAccount.append(UserService.emptyAccount)
        }
        return arrayAccount
    }
    
    static func checkHolderUser(users : [User]) -> Bool{
        var isHolder : Bool = false
        for user in users{
            if(user.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }
    
    static func createHolderUser(numHolder: Int) -> [User] {
        var userHolder : [User] = []
        for i in 1...numHolder{
            let userId = String(i)
            let userItem = User(userId: userId, dataType: .holder)
            //
            userHolder.append(userItem)
        }
        return userHolder
    }
    
    static func createEmptyUser() -> User {
        return User(userId: "", dataType: .empty)
    }
}

