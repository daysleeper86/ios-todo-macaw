//
//  ProjectService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/11/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class OrangnizationService{
    static func documentToOrangnization(querySnapshot: QuerySnapshot) -> [Orangnization] {
        var arrayOrangnization : [Orangnization] = []
        for document in querySnapshot.documents {
            let orangnization = Orangnization(jsonDict: document.data())
            if(orangnization.valid() == true){
                arrayOrangnization.append(orangnization)
            }
        }
        //
        if(arrayOrangnization.count == 0){
            arrayOrangnization.append(OrangnizationViewModel.emptyOrangnization)
        }
        return arrayOrangnization
    }
    
    static func createEmptyOrangnization() -> Orangnization {
        return Orangnization(orangnizationId: "", orangnizationName: "", dataType: .empty)
    }
}

