//
//  MessageService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class ActivityService{
    static func documentToActivity(querySnapshot: QuerySnapshot) -> [Activity] {
        var arrayActivity : [Activity] = []
        for document in querySnapshot.documents {
            let activity = Activity(jsonDict: document.data())
            if(activity.valid() == true){
                arrayActivity.append(activity)
            }
        }
        if(arrayActivity.count == 0){
            arrayActivity.append(ActivityViewModel.emptyActivity)
        }
        else{
            arrayActivity.sort(by: { $0.createdAt.compare($1.createdAt) == .orderedDescending })
        }
        return arrayActivity
    }
    
    static func createHolderActivity(numHolder: Int) -> [Activity] {
        var activityHolder : [Activity] = []
        for i in 1...numHolder{
            let activityId = String(i)
            var activityItem = Activity(activityId: activityId,dataType: .holder)
            activityItem.message.dataType = .holder
            //
            activityHolder.append(activityItem)
        }
        return activityHolder
    }
    
    static func createEmptyActivity() -> Activity {
        return Activity(activityId: "",dataType: .empty)
    }

}
