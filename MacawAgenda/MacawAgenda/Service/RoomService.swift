//
//  MessageService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class RoomService{
    static func documentToRoom(querySnapshot: QuerySnapshot) -> [Room] {
        var arrayRoom : [Room] = []
        for document in querySnapshot.documents {
            let room = Room(jsonDict: document.data())
            if(room.valid() == true){
                arrayRoom.append(room)
            }
        }
        return arrayRoom
    }
    
    static func createHolderRoom(numHolder: Int) -> [Room] {
        var roomHolder : [Room] = []
        for i in 1...numHolder{
            let roomId = String(i)
            let roomItem = Room(roomId: roomId,dataType: .holder)
            //
            roomHolder.append(roomItem)
        }
        return roomHolder
    }
    
    static func createEmptyRoom() -> Room {
        return Room(roomId: "", dataType: .empty)
    }
}
