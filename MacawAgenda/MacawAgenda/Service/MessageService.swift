//
//  MessageService.swift
//  MacawAgenda
//
//  Created by tranquangson on 7/10/19.
//  Copyright © 2019 trananhtuan. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore

class MessageService{
    static let emptyMessage = MessageService.createEmptyMessage()
    static let messageNoneComment = MessageService.createEmptyNoneCommentMessage()
    //
    static func documentToMessageTaskRoom(querySnapshot: QuerySnapshot) -> [Message] {
        var arrayMessage : [Message] = []
        for document in querySnapshot.documents {
            let message = Message(jsonDict: document.data(), messageType: .room)
            if(message.valid() == true){
                arrayMessage.append(message)
            }
        }
        if(arrayMessage.count == 0){
            arrayMessage.append(MessageService.messageNoneComment)
        }
        return arrayMessage
    }
    
    static func documentToMessageAgendaRoom(querySnapshot: QuerySnapshot) -> [Message] {
        var arrayMessage : [Message] = []
        for document in querySnapshot.documents {
            let message = Message(jsonDict: document.data(), messageType: .room)
            if(message.valid() == true){
                arrayMessage.append(message)
            }
        }
        if(arrayMessage.count == 0){
            arrayMessage.append(MessageService.emptyMessage)
        }
        return arrayMessage
    }
    
    static func checkHolderMessage(messages : [Message]) -> Bool{
        var isHolder : Bool = false
        for message in messages{
            if(message.dataType == .holder){
                isHolder = true
                break
            }
        }
        return isHolder
    }
    
    static func createHolderMessage(numHolder: Int) -> [Message] {
        var messageHolder : [Message] = []
        for i in 1...numHolder{
            let messageId = String(i)
            let messageItem = Message(messageId: messageId, content: "Message " + messageId, type: .kMessageTypeText, dataType: .holder)
            //
            messageHolder.append(messageItem)
        }
        return messageHolder
    }
    
    static func createEmptyMessage() -> Message {
        return Message(messageId: "", content: "", type: .kMessageTypeNon, dataType: .empty)
    }
    
    static func createEmptyNoneCommentMessage() -> Message {
        return Message(messageId: Util.getRandomKey(), content: NSLocalizedString("no_comments_yet", comment:""), type: .kMessageTypeNoneComment, dataType: .empty)
    }
    
}
